

# List of base modules of the Exeos Build System:
set( _EBS_VINTERNAL_DEFLIST_MOD
    "_EBSREGISTRY" #

    "SYSCONF" # Detectes host environment, stores config for the target.

    "WORKSPACE"
)


#
function( ebsBaseGetVariableInternalRefName pBaseModule pVarCategory pVarRefName outRefName )

    # Pattern used to compose names of variables:
    # EBS_VAR_${baseModule}_${varCategory}_${varName}
    set( ${outRefName} "EBS_VAR_${pBaseModule}_${pVarCategory}_${pVarRefName}" PARENT_SCOPE )

endfunction()
