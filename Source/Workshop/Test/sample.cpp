
#include <ExsLib/System/DisplayDriver.h>
#include <ExsLib/System/OpenGL.h>
#include <ExsLib/System/Window.h>
#include <GL/gl.h>

using namespace Exs;

#include <Windows.h>

int main()
{
	System::VisualDesc visualDesc;
	visualDesc.config.colorDesc.rgba.redBits = 8;
	visualDesc.config.colorDesc.rgba.greenBits = 8;
	visualDesc.config.colorDesc.rgba.blueBits = 8;
	visualDesc.config.colorDesc.rgba.alphaBits = 8;
	visualDesc.config.depthStencilDesc.depthBufferSize = 24;
	visualDesc.config.depthStencilDesc.stencilBufferSize = 8;
	visualDesc.config.msaaDesc.count = 0;
	visualDesc.config.msaaDesc.quality = 0;
	visualDesc.propertyFlags = System::VisualPropertyFlag_Sys_Doublebuffer;

	auto systemSession = System::CreateSystemSessionState();

	System::DisplayDriverCreateInfo ddci;
	auto displayDriver = System::CreateDisplayDriver( systemSession, ddci );

	auto dsDevices = System::EnumDisplayDevices( displayDriver );
	auto& coreDevice = dsDevices->at( 0 );
	auto displayOutputSize = coreDevice->outputDesc->rect.size;

	System::ReleaseDisplayDriver( displayDriver );

	System::WindowHandle window;
	System::GLSurfaceHandle glsurface;
	System::GLContextHandle glcontext;

	{
		System::GLWindowCreateInfo windowCreateInfo;
		windowCreateInfo.position = System::windowPositionAuto;
		windowCreateInfo.size = System::windowSizeDefault;
		windowCreateInfo.visualDesc = visualDesc;

		System::GLSurfaceCreateInfo surfaceCreateInfo;
		surfaceCreateInfo.visualDesc = visualDesc;

		System::GLCoreContextCreateInfo coreContextCreateInfo;
		coreContextCreateInfo.version.major = 4;
		coreContextCreateInfo.version.minor = 4;
		coreContextCreateInfo.visualDesc = visualDesc;

		auto initState = System::OpenGLAPI::CreateInitState( systemSession );
		auto apiLoadRes = System::OpenGLAPI::InitializeOpenGL( initState );

		System::OpenGLAPI::ReleaseInitState( initState );

		windowCreateInfo.style = System::WindowStyle::Fixed;
		windowCreateInfo.title = "Sample OpenGL Window";

		auto windowRes = System::OpenGLAPI::CreateWindow( systemSession, windowCreateInfo );
		auto surfaceRes = System::OpenGLAPI::CreateWindowSurface( windowRes, surfaceCreateInfo );
		auto contextRes = System::OpenGLAPI::CreateCoreContext( surfaceRes, coreContextCreateInfo );

		window = *windowRes;
		glsurface = *surfaceRes;
		glcontext = *contextRes;
	}

	System::OpenGLAPI::BindContext( glcontext );
	System::WindowAPI::ShowWindow( window );

	auto versionInfo = System::OpenGLAPI::GetOpenGLVersionInfo( glcontext );

	printf( "OpenGL info:\n-- version: %u.%u (%s)\n-- glslver: %s\n-- renderer: %s\n-- vendor: %s\n",
	        versionInfo->apiVersion.major,
	        versionInfo->apiVersion.minor,
	        versionInfo->apiVersionStr.c_str(),
	        versionInfo->glslVersionStr.c_str(),
	        versionInfo->rendererName.c_str(),
	        versionInfo->vendorName.c_str() );

	while ( true )
	{
		MSG msg;

		if ( PeekMessageW( &msg, NULL, 0, 0, PM_REMOVE ) )
		{
			if ( msg.message == WM_CLOSE || msg.message == WM_DESTROY )
			{
				PostQuitMessage( 0 );
			}

			if ( msg.message == WM_QUIT )
			{
				break;
			}

			TranslateMessage( &msg );
			DispatchMessageW( &msg );
		}

		glClearColor( 0.0f, 0.0f, 0.5f, 1.0f );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

		System::OpenGLAPI::SwapContextBuffers( glcontext );

		Sleep( 10 );
	}

	System::OpenGLAPI::UnbindContext( glcontext );
	System::OpenGLAPI::DestroyContext( glcontext );
	System::WindowAPI::DestroyWindow( window );

	return 0;
}
