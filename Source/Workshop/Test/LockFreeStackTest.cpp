
#if 0

#include <Exs/Core/Concrt/Thread.h>
#include <Exs/Core/Concrt/ThreadLauncher.h>
#include <Exs/Core/Concrt/ExternalThreadWrapper.h>

#include <fstream>
#include <iostream>
#include <sstream>

using namespace Exs;
using namespace Exs::Concrt;

enum : Thread_ref_id_t
{
	THRID_LFSTEST_1 = 0x711,
	THRID_LFSTEST_2 = 0x712,
	THRID_LFSTEST_3 = 0x713,
	THRID_LFSTEST_4 = 0x714,
	THRID_LFSTEST_5 = 0x715,

	THRID_0_MAIN = 0x900
};

static const Thread_ref_id_t lfsTestThreads[] = { THRID_LFSTEST_1, THRID_LFSTEST_2, THRID_LFSTEST_3, THRID_LFSTEST_4, THRID_LFSTEST_5 };

class LFSTestThreadBase : public Thread
{
public:
	LFSTestThreadBase( EXS_THREAD_CTOR_PARAMS_DECL, Size_t index )
		: Thread( EXS_THREAD_CTOR_PARAMS_DEF )
		, _index( index )
	{ }

	Size_t GetIndex() const
	{
		return this->_index;
	}

private:
	Size_t _index;
};


using LFSTestThread = AsyncInvokerThread<LFSTestThreadBase>;


template <typename TValue>
void saveValuesToFile( const std::vector<TValue>& values, const std::string& filename )
{
	std::fstream fstr;
	fstr.open( filename, std::ios::out );

	if ( fstr )
	{
		for ( auto value : values )
		{
			fstr << value << "\n";
		}
	}
}


template <typename TValue>
std::vector<TValue> mergeOrderedArrays( const std::vector< std::vector<TValue> >& arrays )
{
	std::vector<TValue> mergedArray;
	std::vector<TValue> mergedTemporary;

	Size_t arraysNum = arrays.size();
	Size_t totalElementsNum = 0;

	for ( Size_t i = 0; i < arraysNum; ++i )
	{
		totalElementsNum += arrays[i].size();
	}

	mergedArray.reserve( totalElementsNum );
	mergedTemporary.reserve( totalElementsNum );

	mergedArray.assign( arrays[0].begin(), arrays[0].end() );

	for ( Size_t arrayIndex = 1; arrayIndex < arraysNum; ++arrayIndex )
	{
		std::swap( mergedArray, mergedTemporary );
		std::merge( mergedTemporary.begin(), mergedTemporary.end(), arrays[arrayIndex].begin(), arrays[arrayIndex].end(), std::back_inserter( mergedArray ) );
		mergedTemporary.clear();
	}

	return mergedArray;
}

#include <stack>
#include <stdx/concurrent_stack.h>


int qqmain()
{
	const Size_t lfsStackSize = 524288;
	const Size_t threadsNum = sizeof( lfsTestThreads ) / sizeof( lfsTestThreads[0] );

	std::vector< std::vector<Uint32> > fetchedValues;
	fetchedValues.resize( threadsNum );

	stdx::concurrent_stack<Uint> lfs;
	std::mutex stdoutMutex;

	for ( int i = lfsStackSize; i > 0; --i )
	{
		lfs.push( i );
	}

	{
		ConcrtSharedStateInitInfo initInfo;
		auto concrtSharedState = CreateConcrtSharedState( initInfo );

		ExternalThreadCreateInfo thrCreateInfo;
		thrCreateInfo.properties.name = "MainThread";
		thrCreateInfo.properties.permissions = ThreadPermission_Default;
		thrCreateInfo.properties.refID = 0x77;
		thrCreateInfo.runFlags = 0;

		auto mainThreadWrapper = CreateMainThreadWrapper<ExternalThread>( concrtSharedState, thrCreateInfo );

		auto lfsTextLambda = [&lfs, &fetchedValues, &stdoutMutex]( LFSTestThread* thr ) -> Result {
			Size_t myIndex = thr->GetIndex();

			while ( !lfs.empty() )
			{
				auto getRes = lfs.fetch();

				if ( getRes )
				{
					fetchedValues[myIndex].push_back( getRes.value );
				}
			}

			saveValuesToFile( fetchedValues[myIndex], thr->GetName() + std::string( ".bin" ) );

			{
				std::unique_lock<std::mutex> sdtoutLock{ stdoutMutex };
				{
					std::cout << thr->GetName() << " has fetched " << fetchedValues[myIndex].size() << " values from the stack \n";
					std::cout.flush();
				}
			}

			for ( auto& value : fetchedValues[myIndex] )
			{
				lfs.push( 0 );
			}

			return ExsResult( RSC_Success );
		};

		ThreadLaunchInfo thrLaunchInfo;
		thrCreateInfo.properties.permissions = ThreadPermission_Default;
		thrCreateInfo.runFlags = ThreadRun_Default;

		for ( Size_t i = 0; i < threadsNum; ++i )
		{
			thrLaunchInfo.properties.name = std::string( "ThreadLFS" ) + std::to_string( lfsTestThreads[i] );
			thrLaunchInfo.properties.refID = lfsTestThreads[i];
			ThreadLauncher::Launch<LFSTestThread>( concrtSharedState, thrLaunchInfo, lfsTextLambda, i );
		}
	}

	auto mergedArray = mergeOrderedArrays( fetchedValues );

	ExsDebugAssert( mergedArray.size() == lfsStackSize );

	for ( Size_t i = 0; i < (lfsStackSize-1); ++i )
	{
		ExsDebugAssert( mergedArray[i] < mergedArray[i+1] );
		ExsDebugAssert( mergedArray[i] == i+1 );
	}

	Size_t lfsValues = 0;

	while ( !lfs.empty() )
	{
		auto res = lfs.fetch();
		ExsDebugAssert( res.value == 0 );

		++lfsValues;
	}

	ExsDebugAssert( lfsValues == lfsStackSize );

	return 0;
}


int rrmain()
{
	const Size_t lfsStackSize = 524288;
	const Size_t threadsNum = sizeof( lfsTestThreads ) / sizeof( lfsTestThreads[0] );

	HANDLE hConsole = GetStdHandle( STD_OUTPUT_HANDLE );

	for ( size_t t = 0; t < 4096; ++t )
	{
		stdx::concurrent_stack<Uint> lfs;

		for ( int i = lfsStackSize; i > 0; --i )
		{
			lfs.push( i );
		}

		SetConsoleTextAttribute( hConsole, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE );
		std::cout << "Test " << t << ": ";

		try
		{
			{
				ConcrtSharedStateInitInfo initInfo;
				auto concrtSharedState = CreateConcrtSharedState( initInfo );

				ExternalThreadCreateInfo thrCreateInfo;
				thrCreateInfo.properties.name = "MainThread";
				thrCreateInfo.properties.permissions = ThreadPermission_Default;
				thrCreateInfo.properties.refID = 0x77;
				thrCreateInfo.runFlags = 0;

				auto mainThreadWrapper = CreateMainThreadWrapper<ExternalThread>( concrtSharedState, thrCreateInfo );

				auto lfsTextLambda = [&lfs]( LFSTestThread* thr ) -> Result {
					while ( !lfs.empty() )
					{
						lfs.fetch();
					}
					return ExsResult( RSC_Success );
				};

				ThreadLaunchInfo thrLaunchInfo;
				thrCreateInfo.properties.permissions = ThreadPermission_Default;
				thrCreateInfo.runFlags = ThreadRun_Default;

				for ( Size_t i = 0; i < threadsNum; ++i )
				{
					thrLaunchInfo.properties.name = std::string( "ThreadLFS" ) + std::to_string( lfsTestThreads[i] );
					thrLaunchInfo.properties.refID = lfsTestThreads[i];
					ThreadLauncher::Launch<LFSTestThread>( concrtSharedState, thrLaunchInfo, lfsTextLambda, i );
				}
			}

			SetConsoleTextAttribute( hConsole, FOREGROUND_GREEN );
			std::cout << "SUCCEEDED";

		}
		catch ( ... )
		{
			SetConsoleTextAttribute( hConsole, FOREGROUND_RED );
			std::cout << "FAILED";
		}

		SetConsoleTextAttribute( hConsole, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE );
		std::cout << "\n";
	}

	std::vector<Uint32> fetchedValuesNum;
	fetchedValuesNum.resize( threadsNum, 0 );

	std::stack<int> pstck;
	stdx::concurrent_stack<Uint> lfs;
	std::mutex pstckMutex;
	std::mutex stdoutMutex;

	std::chrono::seconds::rep lfsTime = 0;
	std::chrono::seconds::rep pstckTime = 0;

	for ( int i = lfsStackSize; i > 0; --i )
	{
		lfs.push( i );
		pstck.push( i );
	}

	{
		ConcrtSharedStateInitInfo initInfo;
		auto concrtSharedState = CreateConcrtSharedState( initInfo );

		ExternalThreadCreateInfo thrCreateInfo;
		thrCreateInfo.properties.name = "MainThread";
		thrCreateInfo.properties.permissions = ThreadPermission_Default;
		thrCreateInfo.properties.refID = 0x77;
		thrCreateInfo.runFlags = 0;

		auto mainThreadWrapper = CreateMainThreadWrapper<ExternalThread>( concrtSharedState, thrCreateInfo );

		auto lfsTextLambda = [&lfs, &fetchedValuesNum, &lfsTime]( LFSTestThread* thr ) -> Result {
			Size_t myIndex = thr->GetIndex();
			auto& myValuesNum = fetchedValuesNum[myIndex];

			auto b = std::chrono::high_resolution_clock::now();

			while ( !lfs.empty() )
			{
				if ( auto getRes = lfs.fetch() )
				{
					++myValuesNum;
				}
			}

			auto e = std::chrono::high_resolution_clock::now();

			lfsTime += std::chrono::duration_cast<std::chrono::nanoseconds>( e - b ).count();

			return ExsResult( RSC_Success );
		};

		ThreadLaunchInfo thrLaunchInfo;
		thrCreateInfo.properties.permissions = ThreadPermission_Default;
		thrCreateInfo.runFlags = ThreadRun_Default;

		for ( Size_t i = 0; i < threadsNum; ++i )
		{
			thrLaunchInfo.properties.name = std::string( "ThreadLFS" ) + std::to_string( lfsTestThreads[i] );
			thrLaunchInfo.properties.refID = lfsTestThreads[i];
			ThreadLauncher::Launch<LFSTestThread>( concrtSharedState, thrLaunchInfo, lfsTextLambda, i );
		}
	}

	double lfsSeconds = static_cast<double>( lfsTime ) / static_cast<double>( std::nano::den );

	std::cout << "LFS results:\n------------\n";
	size_t i = 0;
	for ( auto& value : fetchedValuesNum )
	{
		std::cout << "Thread" << std::to_string( i++ ) << " has fetched " << value << " values\n";
	}
	std::cout << "Total time spent: " << lfsSeconds << " seconds\n\n";

	fetchedValuesNum.clear();
	fetchedValuesNum.resize( threadsNum, 0 );

	{
		ConcrtSharedStateInitInfo initInfo;
		auto concrtSharedState = CreateConcrtSharedState( initInfo );

		ExternalThreadCreateInfo thrCreateInfo;
		thrCreateInfo.properties.name = "MainThread";
		thrCreateInfo.properties.permissions = ThreadPermission_Default;
		thrCreateInfo.properties.refID = 0x77;
		thrCreateInfo.runFlags = 0;

		auto mainThreadWrapper = CreateMainThreadWrapper<ExternalThread>( concrtSharedState, thrCreateInfo );

		auto pstckTextLambda = [&pstck, &pstckMutex, &fetchedValuesNum, &pstckTime]( LFSTestThread* thr ) -> Result {
			Size_t myIndex = thr->GetIndex();
			auto& myValuesNum = fetchedValuesNum[myIndex];

			auto b = std::chrono::high_resolution_clock::now();

			while ( true )
			{
				{
					std::unique_lock<std::mutex> pstckLock{ pstckMutex };
					{
						if ( !pstck.empty() )
						{
							pstck.top();
							pstck.pop();
							++myValuesNum;
						}
						else
						{
							break;
						}
					}

				}
			}

			auto e = std::chrono::high_resolution_clock::now();

			pstckTime += std::chrono::duration_cast<std::chrono::nanoseconds>( e - b ).count();

			return ExsResult( RSC_Success );
		};

		ThreadLaunchInfo thrLaunchInfo;
		thrCreateInfo.properties.permissions = ThreadPermission_Default;
		thrCreateInfo.runFlags = ThreadRun_Default;

		for ( Size_t i = 0; i < threadsNum; ++i )
		{
			thrLaunchInfo.properties.name = std::string( "ThreadLFS" ) + std::to_string( lfsTestThreads[i] );
			thrLaunchInfo.properties.refID = lfsTestThreads[i];
			ThreadLauncher::Launch<LFSTestThread>( concrtSharedState, thrLaunchInfo, pstckTextLambda, i );
		}
	}

	double pstckSeconds = static_cast<double>( pstckTime ) / static_cast<double>( std::nano::den );

	std::cout << "PSTCK results:\n------------\n";
	i = 0;
	for ( auto& value : fetchedValuesNum )
	{
		std::cout << "Thread" << std::to_string( i++ ) << " has fetched " << value << " values\n";
	}
	std::cout << "Total time spent: " << pstckSeconds << " seconds\n\n";

	return 0;
}

#endif
