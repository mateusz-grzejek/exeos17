
#include <ExsLib/CoreUtils/DbgStreamHelper.h>
#include <ExsLib/System/Thread.h>
#include <ExsLib/System/Window.h>

#include <Exs/Core/Concrt/CoreMessage.h>
#include <Exs/Core/Concrt/Thread.h>
#include <Exs/Core/Concrt/ThreadLauncher.h>
#include <Exs/Core/Concrt/ThreadLocalStorage.h>
#include <Exs/Core/Concrt/ThreadSystemManager.h>
#include <Exs/Core/Concrt/ExternalThreadWrapper.h>

#include <Exs/Core/Concrt/ThreadPool.h>

#include <iostream>
#include <sstream>

#if ( EXS_TARGET_PROPERTY_LIB_PRAGMA_SUPPORTED )
#  pragma comment(lib, "ExsLib.CoreUtils.lib")
#  pragma comment(lib, "ExsLib.System.lib")
#  pragma comment(lib, "ExsCore.lib")
#endif

using namespace Exs;
using namespace Exs::Concrt;


int xxmain()
{
	constexpr Thread_ref_id_t mainThreadRefID = 0x77;

	ConcrtSharedStateInitInfo initInfo;
	auto concrtSharedState = CreateConcrtSharedState( initInfo );

	ExternalThreadCreateInfo thrCreateInfo;
	thrCreateInfo.properties.name = "MainThread";
	thrCreateInfo.properties.permissions = ThreadPermission_Default;
	thrCreateInfo.properties.refID = mainThreadRefID;
	thrCreateInfo.runFlags = 0;

	auto mainThreadWrapper = CreateMainThreadWrapper<ExternalThread>( concrtSharedState, thrCreateInfo );

	return 0;
}


#define MSG_SYSTEM_TEST 1
#define THR_POOL_TEST 0


#if THR_POOL_TEST

enum : Thread_ref_id_t
{
	THRID_0_MAIN = 0x7FF,
	THRID_1_THRPOBSERVER = 0x100
};

class ThreadPoolObserverThread : public Thread
{
public:
	ThreadPoolObserverThread( EXS_THREAD_CTOR_PARAMS_DECL, ThreadPool& threadPool )
		: Thread( EXS_THREAD_CTOR_PARAMS_DEF )
		, _threadPool( &threadPool )
	{ }

private:
	virtual Result Entry() override final
	{
		const Size_t workerThreadsNum = this->_threadPool->GetActiveWorkersNum();

		Size_t progressCounter = 0;
		const Size_t progressCounterMax = 10;

		std::string progressString;
		progressString.reserve( progressCounterMax );

		std::string queueString;

		while ( this->Update() == ThreadUpdateResult::Continue )
		{
			system( "cls" );

			progressString.resize( ( progressCounter % progressCounterMax ) + 1, '.' );

			std::cout << "----------------------------------------\n";
			std::cout << "Thread pool monitor is running" << progressString << "\n";
			std::cout << "----------------------------------------\n";

			auto workQueueSize = this->_threadPool->GetSharedQueueSize();
			std::cout << "Queued items: " << workQueueSize << "\n";

			auto idleWorkersNum = this->_threadPool->GetIdleWorkersNum();
			std::cout << "Idle workers: " << idleWorkersNum << "\n";

			std::cout << "Activity: ";

			for ( Size_t workerIndex = 0; workerIndex < workerThreadsNum; ++workerIndex )
			{
				bool isActive = this->_threadPool->IsWorkerActive( workerIndex );
				std::cout << ( ( workerIndex > 0 ) ? " " : "" ) << '[' << ( isActive ? 'A' : '_' ) << ']';
			}

			std::cout << "\n";
			std::cout << "----------------------------------------\n";

			++progressCounter;

			this->Sleep( Milliseconds( 100 ) );
		}

		return ExsResult( RSC_Success );
	}

private:
	ThreadPool* _threadPool;
};

#include <thread>

int main()
{
	srand( time( nullptr ) );

	auto concrtSharedState = CreateConcrtSharedState( ConcrtSharedStateInitInfo() );

	ExternalThreadCreateInfo thrCreateInfo;
	thrCreateInfo.properties.name = "MainThread";
	thrCreateInfo.properties.permissions = ThreadPermission_Default;
	thrCreateInfo.properties.refID = THRID_0_MAIN;
	thrCreateInfo.runFlags = 0;

	auto mainThreadWrapper = CreateMainThreadWrapper<ExternalThread>( concrtSharedState, thrCreateInfo );

	ThreadPool thrPool( concrtSharedState, "smp_pool" );
	thrPool.Init( 16 );

	ThreadLaunchInfo thrLaunchInfo;
	thrLaunchInfo.properties.permissions = ThreadPermission_Default;
	thrLaunchInfo.runFlags = ThreadRun_Default;

	thrLaunchInfo.properties.name = std::string( "ThreadPoolObserver" );
	thrLaunchInfo.properties.refID = THRID_1_THRPOBSERVER;
	ThreadLauncher::Launch<ThreadPoolObserverThread>( concrtSharedState, thrLaunchInfo, thrPool );

	for ( Size_t i = 0; i < 16; ++i )
	{
		auto timeout = ( rand() % 2000 ) + 100;
		auto info = std::to_string( timeout ) + std::string( "\n" );
		printf( "%s", info.c_str() );

		thrPool.QueueWorkItem( [timeout]() -> void {
			std::this_thread::sleep_for( std::chrono::milliseconds( timeout ) );
		} );

		auto timeout2 = ( rand() % 1600 ) + 100;

		std::this_thread::sleep_for( std::chrono::milliseconds( timeout2 ) );
	}

	return 0;
}

#endif


#if MSG_SYSTEM_TEST

#include <Exs/Core/Concrt/ThreadMessageController.h>

using CustomThread = AsyncInvokerThread<Thread>;

enum : Thread_ref_id_t
{
	THRID_SND_1 = 0x711,
	THRID_SND_2 = 0x712,
	THRID_SND_3 = 0x713,
	THRID_SND_4 = 0x714,
	THRID_SND_5 = 0x715,

	THRID_RCV_1 = 0x811,
	THRID_RCV_2 = 0x812,
	THRID_RCV_3 = 0x813,
	THRID_RCV_4 = 0x814,
	THRID_RCV_5 = 0x815,

	THRID_0_MAIN = 0x900
};

static LightMutex coutmtx;

static const Thread_ref_id_t senderThreads[] = { THRID_SND_1, THRID_SND_2, THRID_SND_3, THRID_SND_4, THRID_SND_5 };
static const Thread_ref_id_t receiverThreads[] = { THRID_RCV_1, THRID_RCV_2, THRID_RCV_3, THRID_RCV_4, THRID_RCV_5 };

#include <thread>

int main()
{
	for ( Uint32 t = 0; t < 1; ++t )
	{
		ConcrtSharedStateInitInfo initInfo;
		auto concrtSharedState = CreateConcrtSharedState( initInfo );

		ExternalThreadCreateInfo thrCreateInfo;
		thrCreateInfo.properties.name = "MainThread";
		thrCreateInfo.properties.permissions = ThreadPermission_Default;
		thrCreateInfo.properties.refID = THRID_0_MAIN;
		thrCreateInfo.runFlags = 0;

		auto mainThreadWrapper = CreateMainThreadWrapper<ExternalThread>( concrtSharedState, thrCreateInfo );

		srand( time( nullptr ) );

		{
			std::unique_lock<LightMutex> lck{ coutmtx };
			{
				std::cout << CurrentThread::GetThreadObject().GetTextInfo() << "\n";
				std::cout.flush();
			}
		}

		auto senderLambda = []( CustomThread* thr ) -> Result {

			{
				std::unique_lock<LightMutex> lck{ coutmtx };
				{
					std::cout << thr->GetTextInfo() << "\n";
					std::cout.flush();
				}
			}

			auto* ctrl = CurrentThread::GetLocalStorage()->messageController;

			for ( Uint32 m = 0; m < 16; ++m )
			{
				CoreSyncMessageSendRequest sndreq;
				sndreq.receiverID = receiverThreads[rand()%5];
				sndreq.sendOptions.set( CoreMessageSendOption_Sync_Wait_For_Inactive_Recipient );
				sndreq.syncWaitTimeoutMs = Milliseconds( 100 );

				auto message = CreateCoreMessage( 0x1111 );
				auto sndres = ctrl->SendMessage( sndreq, message );

				if ( sndres )
				{
					{
						std::unique_lock<LightMutex> lck{ coutmtx };
						{
							std::cout << thr->GetName() << " has sent message, waiting for response...\n";
							std::cout.flush();
						}
					}

					if ( auto* r = message->WaitForResponse() )
					{
						std::unique_lock<LightMutex> lck{ coutmtx };
						{
							std::cout << thr->GetName() << " got response for " << message->GetCode() << ": " << r->GetCode() << "\n";
							std::cout.flush();
						}
					}
					else
					{
						std::unique_lock<LightMutex> lck{ coutmtx };
						{
							std::cout << thr->GetName() << " response wait timeout\n";
							std::cout.flush();
						}
					}
				}
			}

			for ( auto& tid : receiverThreads )
			{
				ctrl->SendSimpleMessage( tid, 0 );
			}

			return ExsResult( RSC_Success );
		};

		auto receiverLambda = []( CustomThread* thr ) -> Result {

			{
				std::unique_lock<LightMutex> lck{ coutmtx };
				{
					std::cout << thr->GetTextInfo() << "\n";
					std::cout.flush();
				}
			}

			Uint32 mend = 0;
			Uint32 messages = 0;
			while ( mend < ( sizeof( senderThreads ) / sizeof( senderThreads[0] ) ) )
			{
				auto* ctrl = CurrentThread::GetLocalStorage()->messageController;
				if ( auto msg = ctrl->WaitForMessage() )
				{
					auto msgCode = msg->GetCode();
					if ( msgCode != 0 )
					{
						{
							std::unique_lock<LightMutex> lck{ coutmtx };
							{
								std::cout << thr->GetName() << " has received message: " << msgCode << ", responding...\n";
								std::cout.flush();
							}
						}

						std::this_thread::sleep_for( std::chrono::milliseconds( ( rand()%6 + 2 ) * 10 ) );

						if ( auto* r = msg->CreateResponse( 444 ) )
						{
							r->SetReady();
						}
					}
					else
					{
						{
							std::unique_lock<LightMutex> lck{ coutmtx };
							{
								std::cout << thr->GetName() << " has received END message (0)" << "\n";
								std::cout.flush();
							}
						}
						++mend;
					}
					++messages;
				}
			}
			std::unique_lock<LightMutex> lck{ coutmtx };
			{
				std::cout << thr->GetName() << " has received total of: >>" << messages << "<<\n";
				std::cout.flush();
			}
			return ExsResult( RSC_Success );
		};

		ThreadLaunchInfo thrLaunchInfo;
		thrCreateInfo.properties.permissions = ThreadPermission_Default;
		thrCreateInfo.runFlags = ThreadRun_Default;

		for ( auto& tid : receiverThreads )
		{
			thrLaunchInfo.properties.name = std::string( "Thread_Rcv_" ) + std::to_string( tid );
			thrLaunchInfo.properties.refID = tid;
			ThreadLauncher::Launch<CustomThread>( concrtSharedState, thrLaunchInfo, receiverLambda );
		}

		for ( auto& tid : senderThreads )
		{
			thrLaunchInfo.properties.name = std::string( "Thread_Snd_" ) + std::to_string( tid );
			thrLaunchInfo.properties.refID = tid;
			ThreadLauncher::Launch<CustomThread>( concrtSharedState, thrLaunchInfo, senderLambda );
		}
	}

	return 0;
}

#endif
