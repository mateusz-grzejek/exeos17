
#ifndef __Exs_ResManager_FontLayer_H__
#define __Exs_ResManager_FontLayer_H__

#include "FontGlyph.h"
#include <unordered_map>


namespace Exs
{

	
	ExsDeclareRSClassObjHandle(ShaderResourceView);
	ExsDeclareRSClassObjHandle(Texture2D);


}


#endif /* __Exs_ResManager_FontLayer_H__ */
