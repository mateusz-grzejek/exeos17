
#ifndef __Exs_ResManager_ImageData_H__
#define __Exs_ResManager_ImageData_H__

#include "ImageBase.h"
#include <stdx/data_array.h>


namespace Exs
{


	struct ImageData
	{
		//
		typedef stdx::dynamic_data_array<Byte> PixelArray;

		//
		ImageDataInfo info;

		//
		PixelArray pixelArray;
	};


}


#endif /* __Exs_ResManager_ImageData_H__ */
