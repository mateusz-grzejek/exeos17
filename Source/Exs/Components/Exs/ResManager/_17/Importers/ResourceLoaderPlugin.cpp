
#include <ExsResManager/Importers/ResourceLoaderPlugin.h>
#include <stdx/string_utils.h>


namespace Exs
{


	ResourceLoaderPlugin::ResourceLoaderPlugin(ResourceLoaderPluginManager* manager)
	: Plugin(manager)
	{ }


	ResourceLoaderPlugin::~ResourceLoaderPlugin()
	{ }


	void ResourceLoaderPlugin::RegisterLoader(const ResourceLoaderRefPtr& loader)
	{
		LoaderEntry loaderEntry;
		loaderEntry.loader = loader;
		loaderEntry.loaderName = loader->GetName();
		loaderEntry.resourceType = loader->GetResourceType();

		this->_loaders.push_back(std::move(loaderEntry));
	}




	ResourceLoaderPluginManager::ResourceLoaderPluginManager(PluginSystem* pluginSystem)
	: PluginManager(pluginSystem, PluginType_Resource_Loader, "ResourceLoaderPluginManager")
	{ }


	ResourceLoaderPluginManager::~ResourceLoaderPluginManager()
	{ }


	void ResourceLoaderPluginManager::OnRegisterPlugin(Plugin* plugin)
	{ }


	void ResourceLoaderPluginManager::OnUnregisterPlugin(Plugin* plugin)
	{ }


}
