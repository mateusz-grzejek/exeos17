
#ifndef __Exs_Core_CoreBase_H__
#define __Exs_Core_CoreBase_H__

#include "_Common/_Include.h"
#include "_Common/CoreConfig.h"
#include "_Common/CoreDefs.h"
#include "_Common/CoreTypes.h"
#include "_Common/CoreEnums.h"
#include "_Common/EventBase.h"
#include "_Common/MessageBase.h"
#include "_Common/SharedState.h"
#include "_Common/TraceCommon.h"
#include "_Common/CoreResultCodes.inl"


namespace Exs
{


	ExsDeclareComponentContextHandle( CoreComponentContext );


}


#endif /* __Exs_Core_CoreBase_H__ */
