
#ifndef __Exs_Core_StaticMemoryAllocator_H__
#define __Exs_Core_StaticMemoryAllocator_H__

#include "MemoryBase.h"


namespace Exs
{


	enum class StaticAllocatorExecutionPolicy : Enum
	{
		Default,

		Thread_Local
	};


	template <class T, StaticAllocatorExecutionPolicy>
	struct StaticAllocatorProxy;

	template <class T>
	struct StaticAllocatorProxy<T, StaticAllocatorExecutionPolicy::Default>
	{

	};

	template <class T>
	struct StaticAllocatorProxy<T, StaticAllocatorExecutionPolicy::Thread_Local>
	{

	};


	template <class T, Size_t N>
	class StaticMemoryAllocator
	{
	public:
		static constexpr Size_t alignment = alignof( T );
	};


}


#endif /* __Exs_Core_StaticMemoryAllocator_H__ */
