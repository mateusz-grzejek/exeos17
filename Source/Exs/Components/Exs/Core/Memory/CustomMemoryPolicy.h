
#ifndef __Exs_Core_CustomMemoryPolicy_H__
#define __Exs_Core_CustomMemoryPolicy_H__

#include "MemoryBase.h"


namespace Exs
{


	template <class AllocatorT>
	class CustomMemoryPolicy
	{
	public:
		static void* operator new( Size_t memSize )
		{
			return AllocatorT::Alloc( memSize );
		}

		static void* operator new[]( Size_t memSize )
		{
			return AllocatorT::Alloc( memSize );
		}

		static void operator delete( void* memPtr ) gnoexcept
		{
			return AllocatorT::Dealloc( memPtr );
		}

		static void operator delete[]( void* memPtr ) gnoexcept
		{
			return AllocatorT::Dealloc( memPtr );
		}

		static void* operator new( Size_t memSize, const SourceLocationInfo& srcInfo )
		{
			return AllocatorT::Alloc( memSize, srcInfo );
		}

		static void* operator new[]( Size_t memSize, const SourceLocationInfo& srcInfo )
		{
			return AllocatorT::Alloc( memSize, srcInfo );
		}

		static void operator delete( void* memPtr, const SourceLocationInfo& srcInfo ) gnoexcept
		{
			return AllocatorT::Dealloc( memPtr, srcInfo );
		}

		static void operator delete[]( void* memPtr, const SourceLocationInfo& srcInfo ) gnoexcept
		{
			return AllocatorT::Dealloc( memPtr, srcInfo );
		}
	};


}


#endif /* __Exs_Core_CustomMemoryPolicy_H__ */
