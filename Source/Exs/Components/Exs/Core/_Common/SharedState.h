
#ifndef __Exs_Core_SharedState_H__
#define __Exs_Core_SharedState_H__


namespace Exs
{


	template <class Mutex>
	struct SharedStateBase
	{
		using MutexType = Mutex;

		/// Lock used to acquire unique access to the data. Every sub-type of shared data is assumed
		/// to be possibly shared in every meaning of this word, including access from multiple threads.
		mutable MutexType accessMutex;
	};


	template <>
	struct SharedStateBase<void>
	{ };


	///<summary>
	///</summary>
	template <class Mutex>
	class ComponentContextBase : public Lockable<Mutex, LockAccess::Relaxed>
	{
	public:
		using MutexType = Mutex;
	};


	template <>
	class ComponentContextBase<void>;


	///<summary>
	/// Common base type for shared state. Shared state is a class/struct which encapsulates some shared data
	/// used by a specific subsystem of the engine's component (one or more). Basically, it serves as a container
	/// for data that is used across certain functionality. For example, CoreThreadSystemState is a shared state
	/// used within threading system. It contains instance of ThreadSystemManager, dispatcher of core messages
	/// and other threading-related objects. CoreThreadSystemState is required by many objects, including Thread,
	/// CoreMessenger and most of the public threading API.
	///
	/// The main reasoning behind shared states is to avoid using singletons - instead of having ThreadSystemManager
	/// singleton, CoreMessageDispatcher singleton, etc., we ewncapsulate those objects in a shared state, which is
	/// created on certain point of initialization and passed to all objects/functions which require this state.
	/// This also makes it easier to control the lifetime of a state - as long as there is at least one active handle,
	/// it will stay alive (so, for example, TSM can be safely used by dameon threads, as every thread object holds
	/// active handle to CoreThreadSystemState).
	///
	/// TODO: mutex.
	///</summary>
	template <class Mutex>
	struct SharedState : public SharedStateBase<Mutex>
	{
	};


	///<summary>
	///</summary>
	template <class Mutex>
	class ComponentContext : public ComponentContextBase<Mutex>
	{
	};


	template <class T>
	using SharedStateHandle = std::shared_ptr<T>;


	template <class T>
	using ComponentContextHandle = std::shared_ptr<T>;


	/// Forward-declares 'type' and typedefs SharedStateHandle{type} as type##Handle. Forward declaration uses 'struct' keyword.
#define ExsDeclareSharedStateHandle(type) \
	struct type; typedef SharedStateHandle<type> type##Handle;


	/// Forward-declares 'type' and typedefs ComponentContextHandle{type} as type##Handle. Forward declaration uses 'class' keyword.
#define ExsDeclareComponentContextHandle(type) \
	class type; typedef ComponentContextHandle<type> type##Handle;


	/*/// Locks component context with SharedMutex for read-only access.
	inline stdx::shared_lock<SharedMutex> LockComponentContextReadOnly( const ComponentContext<SharedMutex>& context )
	{
		return stdx::shared_lock<SharedMutex>{ context.GetLock() };
	}

	/// Locks component context with SharedMutex for read-only access. Returns empty lock if handle does not refer to a context.
	inline stdx::shared_lock<SharedMutex> LockComponentContextReadOnly( const SharedHandle< ComponentContext<SharedMutex> >& contextHandle )
	{
		return contextHandle ? stdx::shared_lock<SharedMutex>{ contextHandle->GetLock() } : stdx::shared_lock<SharedMutex>{ };
	}


	/// Locks component context for read-only access.
	template <typename Mutex>
	inline std::unique_lock<Mutex> LockComponentContextReadOnly( const ComponentContext<Mutex>& context )
	{
		return std::unique_lock<Mutex>{ context.GetLock() };
	}

	/// Locks component context for read-only access. Returns empty lock if handle does not refer to a context.
	template <typename Mutex>
	inline std::unique_lock<Mutex> LockComponentContextReadOnly( const SharedHandle< ComponentContext<Mutex> >& contextHandle )
	{
		return contextHandle ? std::unique_lock<Mutex>{ contextHandle->GetLock() } : stdx::shared_lock<Mutex>{};
	}


	/// Locks component context for read/write access.
	template <typename Mutex>
	inline std::unique_lock<Mutex> LockComponentContextReadWrite( const ComponentContext<Mutex>& context )
	{
		return std::unique_lock<Mutex>{ context.GetLock() };
	}

	/// Locks component context for read/write access. Returns empty lock if handle does not refer to a context.
	template <typename Mutex>
	inline std::unique_lock<Mutex> LockComponentContextReadWrite( const SharedHandle< ComponentContext<Mutex> >& contextHandle )
	{
		return contextHandle ? std::unique_lock<Mutex>{ contextHandle->GetLock() } : stdx::shared_lock<Mutex>{ };
	}


	/// Locks component context with SharedMutex for access, which is specified as template parameter.
	template <SharedMutexAccessType Access>
	inline typename SharedMutexLockType<Access>::Type LockComponentContext( const ComponentContext<SharedMutex>& context )
	{
		using Lock = typename SharedMutexLockType<Access>::Type;
		return Lock{ context.GetLock() };
	}

	/// Locks component context with SharedMutex for access, which is specified as template parameter. Returns empty lock if handle does not refer to a context.
	template <SharedMutexAccessType Access>
	inline typename SharedMutexLockType<Access>::Type LockComponentContext( const SharedHandle< ComponentContext<SharedMutex> >& sharedStateHandle )
	{
		using Lock = typename SharedMutexLockType<Access>::Type;
		return sharedStateHandle ? Lock{ contextHandle->GetLock() } : Lock{ };
	}


	/// Locks component context for access, which is specified as template parameter.
	template <SharedMutexAccessType Access, typename Mutex>
	inline std::unique_lock<Mutex> LockComponentContext( const ComponentContext<Mutex>& context )
	{
		return std::unique_lock<Mutex>{ context.GetLock() };
	}

	/// Locks component context for access, which is specified as template parameter. Returns empty lock if handle does not refer to a context.
	template <SharedMutexAccessType Access, typename Mutex>
	inline std::unique_lock<Mutex> LockComponentContext( const SharedHandle< ComponentContext<Mutex> >& contextHandle )
	{
		return sharedStateHandle ? std::unique_lock<Mutex>{ contextHandle->GetLock() } : std::unique_lock<Mutex>{ };
	}


	/// Locks shared state for read-only access.
	inline stdx::shared_lock<SharedMutex> LockSharedStateReadOnly( const ComponentSubsystemSharedState& sharedState )
	{
		return stdx::shared_lock<SharedMutex>{ sharedState.accessMutex };
	}

	/// Locks shared state (passed via handle) for read-only access. Returns empty lock if handle does not refer to any state.
	inline stdx::shared_lock<SharedMutex> LockSharedStateReadOnly( const SharedHandle<ComponentSubsystemSharedState>& sharedStateHandle )
	{
		return sharedStateHandle ? stdx::shared_lock<SharedMutex>{ sharedStateHandle->accessMutex } : stdx::shared_lock<SharedMutex>{};
	}


	/// Locks shared state for read/write access.
	inline std::unique_lock<SharedMutex> LockSharedStateReadWrite( const ComponentSubsystemSharedState& sharedState )
	{
		return std::unique_lock<SharedMutex>{ sharedState.accessMutex };
	}

	/// Locks shared state (passed via handle) for read/write access. Returns empty lock if handle does not refer to any state.
	inline std::unique_lock<SharedMutex> LockSharedStateReadWrite( const SharedHandle<ComponentSubsystemSharedState>& sharedStateHandle )
	{
		return sharedStateHandle ? std::unique_lock<SharedMutex>{ sharedStateHandle->accessMutex } : std::unique_lock<SharedMutex>{};
	}


	/// Locks shared state for access, which is specified as template parameter. Lock type depends on access.
	template <SharedMutexAccessType Access>
	inline typename SharedMutexLockType<Access>::Type LockSharedState( const ComponentSubsystemSharedState& sharedState )
	{
		using Lock = typename SharedMutexLockType<Access>::Type;
		return Lock{ sharedState.accessMutex };
	}

	/// Locks shared state for access, which is specified as template parameter. Lock type depends on access. Returns empty lock if handle does not refer to any state.
	template <SharedMutexAccessType Access>
	inline typename SharedMutexLockType<Access>::Type LockSharedState( const SharedHandle<ComponentSubsystemSharedState>& sharedStateHandle )
	{
		using Lock = typename SharedMutexLockType<Access>::Type;
		return sharedStateHandle ? Lock{ sharedStateHandle->accessMutex } : Lock{};
	}*/


}


#endif /* __Exs_Core_SharedState_H__ */
