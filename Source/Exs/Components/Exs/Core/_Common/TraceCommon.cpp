
#include "Precomp.h"
#include <Exs/Core/Core.h>


namespace Exs
{
	namespace Internal
	{


		void PrintTraceOutput( const TraceOrigin& traceOrigin, const char* format, va_list argsList );


		void TraceOutputImpl( TraceSeverityLevel severityLevel, TraceCategoryID category, const char* typeName, const char* categoryName, const char* format, ... )
		{
			TraceOrigin traceOrigin{};
			traceOrigin.severityLevel = severityLevel;
			traceOrigin.category = category;
			traceOrigin.typeName = typeName;
			traceOrigin.categoryName = categoryName;

			va_list varArgsList;
			va_start( varArgsList, format );

			PrintTraceOutput( traceOrigin, format, varArgsList );

			va_end( varArgsList );
		}


		void PrintTraceOutput( const TraceOrigin& traceOrigin, const char* format, va_list argsList )
		{
			const Size_t Local_buffer_size = 1024;
			char localBuffer[Local_buffer_size];

			Size_t bufferCapacity = Local_buffer_size;
			char* bufferPtr = &( localBuffer[0] );

			{
				auto printCount = snprintf( bufferPtr, bufferCapacity, "<%s> ", traceOrigin.typeName );
				bufferCapacity -= printCount;
				bufferPtr += printCount;
			}

			{
				auto printCount = snprintf( bufferPtr, bufferCapacity, "[%s] ", traceOrigin.categoryName );
				bufferCapacity -= printCount;
				bufferPtr += printCount;
			}

			{
				auto printCount = vsnprintf( bufferPtr, bufferCapacity, format, argsList );
				bufferCapacity -= printCount;
				bufferPtr += printCount;
			}

			ExsDebugAssert( bufferPtr < &( localBuffer[Local_buffer_size] ) );

			printf( "%s", localBuffer );
		}


	}
}
