
#ifndef __Exs_Core_CoreResultCodes_INL__
#define __Exs_Core_CoreResultCodes_INL__

namespace Exs
{


	enum : Uint16
	{
		RSCT_Common,
		RSCT_Input,
		RSCT_Messaging,
		RSCT_System,
		RSCT_Tasks,
		RSCT_Threads
	};


}


#endif /* __Exs_Core_CoreResultCodes_INL__ */
