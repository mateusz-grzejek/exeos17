
#ifndef __Exs_Core_TraceCommon_H__
#define __Exs_Core_TraceCommon_H__


namespace Exs
{


	class ITraceOutputStream
	{
	public:
		virtual void Print( const char* text ) = 0;
	};


}



#if 1

#define EXS_TRACE_TYPE_INFO          0x0001
#define EXS_TRACE_TYPE_NOTIFICATION  0x0002
#define EXS_TRACE_TYPE_WARNING       0x0004
#define EXS_TRACE_TYPE_ERROR         0x0008
#define EXS_TRACE_TYPE_EXCEPTION     0x0010

#define EXS_TRACE_TYPE_ALL              0xFFFF
#define EXS_TRACE_TYPE_DEFAULT_ENABLED  EXS_TRACE_TYPE_EXCEPTION | EXS_TRACE_TYPE_ERROR


#if !defined( EXS_TRACE_CONFIG_ACTIVE_SEVERITY_LEVELS_MASK )
#  if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
#    define EXS_TRACE_CONFIG_ACTIVE_SEVERITY_LEVELS_MASK  EXS_TRACE_TYPE_ALL
#  else
#    define EXS_TRACE_CONFIG_ACTIVE_SEVERITY_LEVELS_MASK  EXS_TRACE_TYPE_DEFAULT_ENABLED
#  endif
#endif


#define EXS_TRACE_SET_CATEGORY_NAME(category, name) \
	template <> inline const char* GetTraceCategoryName<category>() { return name; }

#define EXS_TRACE_SET_TYPE_NAME(type, name) \
	template <> inline const char* GetTraceSeverityLevelName<type>() { return name; }


namespace Exs
{


	typedef Enum TraceCategoryID;


	enum class TraceSeverityLevel : Enum
	{
		Info = EXS_TRACE_TYPE_INFO,
		Notification = EXS_TRACE_TYPE_NOTIFICATION,
		Warning = EXS_TRACE_TYPE_WARNING,
		Error = EXS_TRACE_TYPE_ERROR,
		Exception = EXS_TRACE_TYPE_EXCEPTION
	};


	enum : TraceCategoryID
	{
		TRC_None,
		TRC_Common,
		TRC_Core_Memory,
		TRC_Core_Messaging,
		TRC_Core_Plugins,
		TRC_Core_System,
		TRC_Core_Threading
	};


	struct TraceOrigin
	{
		TraceSeverityLevel severityLevel;
		TraceCategoryID category;
		const char* typeName;
		const char* categoryName;
	};


	template <TraceCategoryID>
	inline const char* GetTraceCategoryName()
	{
		return "Unknown";
	}


	template <TraceSeverityLevel>
	inline const char* GetTraceSeverityLevelName()
	{
		return "Null";
	}


	template <TraceSeverityLevel>
	inline void TraceOutputFinalizer()
	{ }

	template <>
	inline void TraceOutputFinalizer<TraceSeverityLevel::Error>()
	{
		ExsDebugInterrupt();
	}

	template <>
	inline void TraceOutputFinalizer<TraceSeverityLevel::Exception>()
	{
		ExsDebugInterrupt();
	}


	EXS_TRACE_SET_TYPE_NAME( TraceSeverityLevel::Info, "Inf" );
	EXS_TRACE_SET_TYPE_NAME( TraceSeverityLevel::Notification, "Ntf" );
	EXS_TRACE_SET_TYPE_NAME( TraceSeverityLevel::Warning, "Wrn" );
	EXS_TRACE_SET_TYPE_NAME( TraceSeverityLevel::Error, "Err" );
	EXS_TRACE_SET_TYPE_NAME( TraceSeverityLevel::Exception, "Exc" );


	EXS_TRACE_SET_CATEGORY_NAME( TRC_Common, "Common" );
	EXS_TRACE_SET_CATEGORY_NAME( TRC_Core_Memory, "Core:Memory" );
	EXS_TRACE_SET_CATEGORY_NAME( TRC_Core_Messaging, "Core:Messaging" );
	EXS_TRACE_SET_CATEGORY_NAME( TRC_Core_Plugins, "Core:Plugins" );
	EXS_TRACE_SET_CATEGORY_NAME( TRC_Core_System, "Core:System" );
	EXS_TRACE_SET_CATEGORY_NAME( TRC_Core_Threading, "Core:Threading" );


	namespace Internal
	{

		EXS_LIBAPI_CORE void TraceOutputImpl( TraceSeverityLevel severityLevel, TraceCategoryID category, const char* typeName, const char* categoryName, const char* format, ... );


		template <Uint32 Mask>
		struct TraceOutputFwd
		{
			template <class... Args>
			static void Print( TraceSeverityLevel severityLevel, TraceCategoryID category, const char* typeName, const char* categoryName, const char* format, Args&&... args )
			{
				TraceOutputImpl( severityLevel, category, typeName, categoryName, format, std::forward<Args>( args )... );
			}
		};

		template <>
		struct TraceOutputFwd<0>
		{
			template <class... Args>
			static void Print( TraceSeverityLevel severityLevel, TraceCategoryID category, const char* typeName, const char* categoryName, const char* format, Args&&... args )
			{ }
		};


		template <TraceSeverityLevel Severity, TraceCategoryID CategoryID>
		struct TraceOutputWrapper
		{
			template <class... Args>
			static void Print( const char* format, Args&&... args )
			{
				constexpr Uint32 activeSeverityMask = static_cast< Uint32 >( Severity ) & EXS_TRACE_CONFIG_ACTIVE_SEVERITY_LEVELS_MASK;
				const char* typeName = GetTraceSeverityLevelName<Severity>();
				const char* categoryName = GetTraceCategoryName<CategoryID>();
				TraceOutputFwd<activeSeverityMask>::Print( Severity, CategoryID, typeName, categoryName, format, std::forward<Args>( args )... );
				TraceOutputFinalizer<Severity>();
			}
		};

	}


#if ( EXS_CONFIG_BASE_ENABLE_TRACING )
#  define TraceOutput(severityLevel, categoryID, format, ...) ::Exs::Internal::TraceOutputWrapper<severityLevel, categoryID>::Print(format, ##__VA_ARGS__)
#else
#  define TraceOutput(severityLevel, categoryID, format, ...)
#endif


#define ExsTraceInfo(categoryID, format, ...) \
	TraceOutput(TraceSeverityLevel::Info, categoryID, format, ##__VA_ARGS__)

#define ExsTraceNotification(categoryID, format, ...) \
	TraceOutput(TraceSeverityLevel::Notification, categoryID, format, ##__VA_ARGS__)

#define ExsTraceWarning(categoryID, format, ...) \
	TraceOutput(TraceSeverityLevel::Warning, categoryID, format, ##__VA_ARGS__)

#define ExsTraceError(categoryID, format, ...) \
	TraceOutput(TraceSeverityLevel::Error, categoryID, format, ##__VA_ARGS__)

#define ExsTraceException(categoryID, format, ...) \
	TraceOutput(TraceSeverityLevel::Exception, categoryID, format, ##__VA_ARGS__)


}

#endif


#endif /* __Exs_Core_TraceCommon_H__ */
