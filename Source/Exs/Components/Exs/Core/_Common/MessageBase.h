
#ifndef __Exs_Core_MessageBase_H__
#define __Exs_Core_MessageBase_H__


namespace Exs
{


	/// Type used to store the value of a message code.
	typedef Uint64 Message_code_t;

	/// Type used to store the value of a message class.
	typedef Uint32 Message_class_t;

	/// Type used to store the value of a message UID.
	typedef Uint64 Message_uid_value_t;

	/// Type used to store the value of a message priority.
	typedef Int32 Message_priority_value_t;

	/// Implementation-dependent type which represents message UID (Unique ID). It is
	/// guaranteed to fulfill all requirements of UID-Compatible type (see #TODO: what?).
	typedef Message_uid_value_t MessageUID;

	template <class Message>
	using MessageHandle = std::shared_ptr<Message>;

	template <class Response>
	using MessageResponseHandle = std::shared_ptr<Response>;


	namespace EnumDef
	{

		// Message code pattern: [CCCCCCCC|DDDD|XXXX], where:
		// - D: user specified code ID (16 bits)
		// - X: ext value (may be unused) (16 bits)
		// - C: message class (32 bits)

		// Message class pattern: [KKKK|CCCC], where:
		// - K: control key (below) (16 bits)
		// - C: class ID (16 bits)


		//
		constexpr Uint16 Message_Code_Control_Key = 0x4701;

		//
		inline constexpr Message_class_t DeclareMessageClass( Uint16 clsid )
		{
			return MakeU32W( Message_Code_Control_Key, clsid );
		}

		//
		inline constexpr Message_code_t DeclareMessageCode( Message_class_t msgClass, Uint16 internalID, Uint16 ext = 0 )
		{
			return MakeU64D( msgClass, MakeU32W( internalID, ext ) );
		}

		//
		inline constexpr Message_class_t GetMessageClassFromCode( Message_code_t messageCode )
		{
			return GetU64Dword<0>( messageCode );
		}

		//
		inline constexpr Uint16 GetMessageCodeControlKey( Message_code_t messageCode )
		{
			return GetU32Word<0>( GetMessageClassFromCode( messageCode ) );
		}

		//
		inline constexpr bool ValidateMessageCode( Message_code_t messageCode )
		{
			return GetMessageCodeControlKey( messageCode ) == Message_Code_Control_Key;
		}

	}


	// Common message classes.
	enum : Message_class_t
	{
		// Message is an internal message, used to implement basic functionality of the engine.
		Message_Class_Internal = EnumDef::DeclareMessageClass( 0xE077 ),

		//
		Message_Class_Thread = EnumDef::DeclareMessageClass( 0xE888 ),

		// Message has unspecified class.
		Message_Class_None = EnumDef::DeclareMessageClass( 0 )
	};


	///<summary>
	///</summary>
	enum class MessagePriority : Message_priority_value_t
	{
		//
		Low,

		//
		Default,

		//
		High
	};


	///<summary>
	///</summary>
	struct MessageHeader
	{
		Message_code_t code;

		MessagePriority priority;
	};


	struct MessageResponseHeader
	{
		Message_code_t respondedMessageCode;

		Message_code_t responseCode;
	};


	template <Size_t storageSize, Size_t storageAlignment>
	struct MessagePayload
	{
	public:
		static constexpr auto capacity = storageSize;
		static constexpr auto alignment = storageAlignment;

	public:
		MessagePayload()
			: data( &( _storage ) )
			, dataSize( 0 )
		{ }

	public:
		//
		void* const data;

		//
		Size_t dataSize;

	private:
		//
		typename std::aligned_storage<storageSize, storageAlignment>::type _storage;
	};


	struct MessageFilter
	{
		//
		Message_code_t messageCode;

		MessageFilter( Message_code_t code = 0 )
			: messageCode( code )
		{ }

		operator Message_code_t() const
		{
			return this->messageCode;
		}

		bool IsClass() const
		{
			return EnumDef::GetMessageClassFromCode( this->messageCode ) == this->messageCode;
		}
	};


	struct MessageFilterComp
	{
		bool operator()( const MessageFilter& left, Message_code_t right ) const
		{
			auto leftClass = EnumDef::GetMessageClassFromCode( left.messageCode );
			auto rightClass = EnumDef::GetMessageClassFromCode( right );

			return ( leftClass < rightClass ) || ( ( leftClass == rightClass ) && ( left.messageCode < right ) );
		}

		bool operator()( const MessageFilter& left, const MessageFilter& right ) const
		{
			return ( *this )( left, right.messageCode );
		}
	};


	template <class Header>
	class MessageBase
	{
	public:
		using HeaderType = Header;

	public:
		MessageBase( Message_code_t code, MessagePriority priority )
		{
			this->_header.code = code;
			this->_header.priority = priority;
		}

		Message_code_t GetCode() const
		{
			return this->_header.code;
		}

		Message_class_t GetClass() const
		{
			return EnumDef::GetMessageClassFromCode( this->_header.code );
		}

		MessagePriority GetPriority() const
		{
			return this->_header.priority;
		}

		const HeaderType& GetHeader() const
		{
			return this->_header;
		}

	protected:
		//
		HeaderType _header;
	};


	template <class Message, class Header>
	class MessageRepsonseBase
	{
	public:
		using HeaderType = Header;
		using MessageType = MessageBase<typename Message::HeaderType>;

	public:
		MessageRepsonseBase( Message_code_t respondedMessageCode, Message_code_t responseCode )
		{
			this->_header.respondedMessageCode = respondedMessageCode;
			this->_header.responseCode = responseCode;
		}

		Message_code_t GetCode() const
		{
			return this->_header.responseCode;
		}

		Message_code_t GetRespondedMessageCode() const
		{
			return this->_header.respondedMessageCode;
		}

		const HeaderType& GetHeader() const
		{
			return this->_header;
		}

	protected:
		//
		HeaderType _header;
	};


}


#endif /* __Exs_Core_MessageBase_H__ */
