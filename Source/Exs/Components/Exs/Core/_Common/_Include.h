
#ifndef __Exs_Core_ExternalPrerequisites_H__
#define __Exs_Core_ExternalPrerequisites_H__

#include <ExsLib/CoreUtils/CoreUtils.h>

#include <stdx/assoc_array.h>
#include <stdx/intrusive_ptr.h>

#include <array>
#include <queue>
#include <list>
#include <set>
#include <vector>

#endif /* __Exs_Core_ExternalPrerequisites_H__ */
