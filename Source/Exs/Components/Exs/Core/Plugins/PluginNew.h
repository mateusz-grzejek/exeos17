
#ifndef __Exs_Core_PluginNew_H__
#define __Exs_Core_PluginNew_H__

#include "../Core.h"
#include <unordered_map>


namespace Exs
{


	class Plugin;


	template <class TEventID, class TEventCallback>
	class EventProxy
	{
	public:
		using CallbackList = std::list<TEventCallback>;

		using ListenerMap = std::unordered_map<TEventID, CallbackList>;

	protected:
		template <class... TArgs>
		void Emit( TEventID eventID, TArgs&& ...args )
		{
			auto callbackListIter = this->_listenerMap.find( eventID );

			if ( callbackListIter != this->_listenerMap.end() )
			{
				auto& callbackList = *callbackListIter;
				for( auto& callback : callbackList )
				{
					callback( std::forward<TArgs>( args )... );
				}
			}
		}

		template <class TListener, class TFunction, class... TArgs>
		void RegisterListener( TEventID eventID, TFunction function, TListener& listener, TArgs&& ...args )
		{
			auto callback = std::bind( function, listener, std::forward<TArgs>( args )... );
			auto& callbackList = this->_listenerMap[eventID];
			callbackList.push_back( std::move( callback ) );
		}

		template <class TListener, class TRet, class... TParams, class... TArgs>
		void RegisterListener( TEventID eventID, TRet (TListener::* mfptr)(TParams...), TListener& listener, TArgs&& ...args )
		{
			this->RegisterListener( eventID, mfptr, listener, std::forward<TArgs>( args )... );
		}

	private:
		//
		ListenerMap _listenerMap;
	};


	enum class PluginEventID : Enum
	{
		Load,

		Unload_Request
	};


	class IPluginLoadEventListener
	{
	public:
		virtual void HandleLoadEvent( Plugin& plugin ) = 0;
	};


	class IPluginUnloadRequestEventListener
	{
	public:
		virtual void HandleUnloadRequestEvent( Plugin& plugin ) = 0;
	};


	class Plugin
	{
	public:
		//
		Uint32 GetUseRefCount() const;

	protected:
		//
		Uint32 AddUseRef();

		//
		Uint32 RemoveUseRef();
	};


	class PluginSystem
	{
	public:
		void RegisterLoadListener( IPluginLoadEventListener& listener );
		void RegisterUnloadRequestListener( IPluginUnloadRequestEventListener& listener );
	};


}


#endif /* __Exs_Core_PluginNew_H__ */
