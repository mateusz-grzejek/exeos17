
#ifndef __Exs_Core_ThreadSyncController_H__
#define __Exs_Core_ThreadSyncController_H__

#include "SyncCommon.h"
#include "ThreadSyncCommon.h"


namespace Exs
{
	namespace Concrt
	{


		class ThreadSyncController : public Lockable<LightMutex, LockAccess::Relaxed>
		{
			friend class ThreadSystemManager;
		public:
			struct ThreadEntry
			{
				Thread* thread = nullptr;

				ThreadSharedSyncContext* sharedSyncContext = nullptr;

				std::atomic<bool> isWaiting = ATOMIC_VAR_INIT( false );
			};

			//
			using ThreadSyncContextList = std::array<ThreadEntry, Config::CCRT_THR_Max_Active_Threads_Num>;

			//
			using ActiveWaitList = stdx::assoc_array<Thread*, ThreadActiveWaitInfo>;

		public:
			ThreadSyncController( ConcrtSharedStateHandle concrtSharedState, ThreadSystemManager& threadSystemManager )
				: _concrtSharedState( concrtSharedState )
				, _threadSystemManager( &threadSystemManager )
			{ }

			/**
			 *
			 * @return
			 */
			const ActiveWaitList& GetActiveWaitList() const;

			/**
			 *
			 * @param thread
			 * @return
			 */
			bool IsThreadWaiting( Thread& thread ) const;

		friendapi:
			// @ Thread
			void RegisterThreadSharedSyncContext( Thread& thread, ThreadSharedSyncContext& threadSharedSyncContext );

			// @ Thread
			void UnregisterThreadSharedSyncContext( Thread& thread );

			// @ SyncObjectBase
			void RegisterActiveWait( Thread& thread, SyncObject& syncObject );

			// @ SyncObjectBase
			void UnregisterActiveWait( Thread& thread );

			// @ SyncObjectBase
			ThreadSharedSyncContext* GetCurrentThreadSyncContext();

			// @ SyncObjectBase
			ThreadSharedSyncContext* GetThreadSyncContext( Thread& thread );

		private:
			//
			ThreadEntry& _GetThreadEntry( Thread& thread );

			//
			const ThreadEntry& _GetThreadEntry( Thread& thread ) const;

		private:
			//
			ConcrtSharedStateHandle _concrtSharedState;

			// Pointer to the control manager.
			ThreadSystemManager* _threadSystemManager;

			//
			ThreadSyncContextList _contextList;

			//
			ActiveWaitList _activeWaitList;
		};


		inline const ThreadSyncController::ActiveWaitList& ThreadSyncController::GetActiveWaitList() const
		{
			return this->_activeWaitList;
		}

		inline bool ThreadSyncController::IsThreadWaiting( Thread& thread ) const
		{
			auto& threadEntry = this->_GetThreadEntry( thread );
			return threadEntry.isWaiting.load( std::memory_order_relaxed );
		}

		inline ThreadSharedSyncContext* ThreadSyncController::GetCurrentThreadSyncContext()
		{
			return this->GetThreadSyncContext( CurrentThread::GetThreadObject() );
		}


	}
}


#endif /* __Exs_Core_ThreadSyncController_H__ */
