
#include "Precomp.h"
#include <Exs/Core/Concrt/SyncObjectImpl.h>
#include <Exs/Core/Concrt/Thread.h>


namespace Exs
{
	namespace Concrt
	{


		ThreadSystemManager& SyncObjectImplBase::GetThreadSystemManager( Thread& thread )
		{
			return thread.GetSystemManager();
		}


	}
}
