
#ifndef __Exs_Core_SharedSyncObject_H__
#define __Exs_Core_SharedSyncObject_H__

#include "SyncObjectImpl.h"


namespace Exs
{
	namespace Concrt
	{


		/**
		 *
		 */
		class SharedSyncObjectImpl : public SyncObjectImplBase
		{
		protected:
			/// @see SyncObjectBase::SyncObjectBase( const ConcrtSharedStateHandle& )
			SharedSyncObjectImpl() = default;

			bool OnWaitBeginImpl( ThreadWaitSyncState& threadWaitSyncState )
			{
				threadWaitSyncState.waitingListRef = this->_waitList.insert( this->_waitList.end(), &threadWaitSyncState );

				return true;
			}

			void OnWaitEndImpl( ThreadWaitSyncState& threadWaitSyncState )
			{
				this->_waitList.erase( threadWaitSyncState.waitingListRef );
			}

			template <class TLock>
			Size_t AbortImpl( TLock& lock )
			{
				return this->_SignalAll( lock, SyncObjectSignalFlag_Interrupt );
			}

			template <class TLock>
			bool InterruptImpl( TLock& lock, Thread& thread )
			{
				for ( auto* syncthreadWaitSyncState : this->_waitList )
				{
					if ( syncthreadWaitSyncState->thread == &thread )
					{
						SyncObjectImplBase::SignalThread<TLock>( *syncthreadWaitSyncState, SyncObjectSignalFlag_Interrupt );

						return true;
					}
				}

				return false;
			}

			template <class TLock>
			bool NotifyOneImpl( TLock& lock )
			{
				if ( !this->_waitList.empty() )
				{
					auto* syncthreadWaitSyncState = this->_waitList.front();

					SyncObjectImplBase::SignalThread<TLock>( *syncthreadWaitSyncState, SyncObjectSignalFlag_Notify );

					return true;
				}

				return false;
			}

			template <class TLock>
			Size_t NotifyAllImpl( TLock& lock )
			{
				return this->_SignalAll( lock, SyncObjectSignalFlag_Notify );
			}

		private:
			template <class TLock>
			Size_t _SignalAll( TLock& lock, SyncObjectSignalFlags signalFlags )
			{
				for ( auto* syncthreadWaitSyncState : this->_waitList )
				{
					SyncObjectImplBase::SignalThread<TLock>( *syncthreadWaitSyncState, signalFlags );
				}

				return this->_waitList.size();
			}

		private:
			//
			SyncObjectWaitingThreadList _waitList;
		};


		/**
		 *
		 */
		template <class TMutex = SyncMutex>
		class SharedSyncObject : public SyncObjectImplProxy<TMutex, SharedSyncObjectImpl>
		{
		public:
			using BaseImplProxyType = SyncObjectImplProxy<TMutex, SharedSyncObjectImpl>;

		public:
			/**
			 *
			 * @param concrtSharedState
			 */
			SharedSyncObject()
			: BaseImplProxyType( CurrentThread::GetThreadSystemManager() )
			{ }

			/**
			 *
			 */
			explicit SharedSyncObject( ThreadSystemManager& threadSystemManager )
			: BaseImplProxyType( threadSystemManager )
			{ }

			/**
			 *
			 * @param concrtSharedState
			 */
			explicit SharedSyncObject( const ConcrtSharedStateHandle& concrtSharedState )
			: BaseImplProxyType( concrtSharedState->GetThreadSystemManager() )
			{ }
		};


	}
}


#endif /* __Exs_Core_SharedSyncObject_H__ */

