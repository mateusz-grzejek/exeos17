
#ifndef __Exs_Core_ThreadLauncher_H__
#define __Exs_Core_ThreadLauncher_H__

#include "Thread.h"
#include "ThreadSystemManager.h"


namespace Exs
{
	namespace Concrt
	{


		///<summary>
		/// Contains data used to control the process of launching new threads.
		///</summary>
		struct ThreadLaunchInfo : public ThreadCreateInfo
		{
		};


		///<summary>
		///</summary>
		class ThreadLauncher
		{
		public:
			template <class TThreadType, class... TArgs>
			static ThreadObjectHandle<TThreadType> CreateThread( ConcrtSharedStateHandle concrtSharedState, const ThreadLaunchInfo& launchInfo, TArgs&& ...args )
			{
				return concrtSharedState->threadSystemManager->CreateThreadObject<TThreadType>( launchInfo, std::forward<TArgs>( args )... );
			}

			template <class TThreadType>
			static Result RunThread( ThreadObjectHandle<TThreadType> thread, const ThreadLaunchInfo& launchInfo )
			{
				return Thread::Run( thread, launchInfo.runFlags );
			}

			template <class TThreadType, class... TArgs>
			static ThreadObjectHandle<TThreadType> Launch( ConcrtSharedStateHandle concrtSharedState, const ThreadLaunchInfo& launchInfo, TArgs&& ...args )
			{
				if ( auto threadHandle = CreateThread<TThreadType>( concrtSharedState, launchInfo, std::forward<TArgs>( args )... ) )
				{
					auto runResult = RunThread( threadHandle, launchInfo );

					if ( runResult == RSC_Success )
					{
						return threadHandle;
					}
				}

				return nullptr;
			}
		};


	}
}


#endif /* __Exs_Core_ThreadLauncher_H__ */
