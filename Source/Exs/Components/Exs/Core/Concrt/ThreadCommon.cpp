
#include "Precomp.h"
#include <Exs/Core/Concrt/Thread.h>
#include <Exs/Core/Concrt/ThreadInternal.h>
#include <Exs/Core/Concrt/ThreadLocalStorage.h>
#include <ExsLib/CoreUtils/Exception.h>
#include <stdx/string_utils.h>
#include <sstream>


namespace Exs
{
	namespace Concrt
	{


		ThreadLocalStorage* CurrentThread::GetLocalStorage()
		{
			auto tlsInfo = Internal::GetThreadLocalStorageUnchecked();
			ExsDebugAssert( tlsInfo.second > 0 );

			return tlsInfo.first;
		}


		Thread& CurrentThread::GetThreadObject()
		{
			auto* threadLocalStorage = GetLocalStorage();
			
			if ( threadLocalStorage->internalState.thread == nullptr )
			{
				ExsThrowException( EXC_Internal_Error );
			}
			
			return *( threadLocalStorage->internalState.thread );
		}


		ThreadSystemManager& CurrentThread::GetThreadSystemManager()
		{
			auto& threadObject = GetThreadObject();
			auto& systemManager = threadObject.GetSystemManager();

			return systemManager;
		}


		Thread_ref_id_t CurrentThread::GetRefID()
		{
			auto* threadLocalStorage = GetLocalStorage();
			return threadLocalStorage->internalState.threadUID.GetRefID();
		}




		Thread_ref_id_t ThreadUtils::GetThreadRefID( Thread* thread )
		{
			return thread->GetRefID();
		}


		std::string ThreadUtils::GetUIDString( ThreadUID threadUID )
		{
			std::ostringstream strStream;

			strStream << std::hex << threadUID.internalData.index;
			strStream << std::hex << threadUID.internalData.refID;
			strStream << std::hex << threadUID.internalData.thrObjectPtr;

			return std::string("0x") + stdx::str_make_uppercase( strStream.str() );
		}


	}
}
