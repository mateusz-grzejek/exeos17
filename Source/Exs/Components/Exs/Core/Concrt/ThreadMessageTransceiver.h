
#ifndef __Exs_Core_ThreadMessageTransceiver_H__
#define __Exs_Core_ThreadMessageTransceiver_H__

#include "CoreMessageBase.h"
#include "ThreadMessageController.h"


namespace Exs
{
	namespace Concrt
	{


		class CoreMessageReceiver;
		class CoreMessageSender;
		class CoreMessageSystemRoot;
		class ThreadMessageProxyQueue;


		enum : U32ID
		{
			Thread_Proxy_Queue_Base_ID = 0x47000001
		};


		class ThreadMessageTransceiver
		{
			friend class ThreadMessageController;
			friend class ThreadMessageProxyQueue;
			friend class ThreadSystemManager;

		private:
			//
			struct ThreadProxyQueueDeleter
			{
				void operator()( ThreadMessageProxyQueue* queue );
			};

			using ThreadMessageProxyQueuePtr = std::unique_ptr<ThreadMessageProxyQueue, ThreadProxyQueueDeleter>;

			//
			struct ThreadProxyQueueInfo
			{
				U32ID queueID;

				CoreMessageQueue* messageQueue;

				ThreadMessageProxyQueuePtr queue;

				std::set<MessageFilter> registeredFilters;
			};
			
			using ThreadProxyQueueList = stdx::assoc_array<U32ID, ThreadProxyQueueInfo>;

			using ThreadProxyQueueInfoRef = ThreadProxyQueueList::iterator;

		public:
			ThreadMessageTransceiver( CoreMessageSystemRoot& messageSystemRoot, Thread& thread, ThreadUID threadUID );

			~ThreadMessageTransceiver();

		friendapi:
			// @ Thread
			//
			Result Initialize( bool initReceiver, bool initSender );

			// @ Thread
			//
			Result Register();

			// @ Thread
			//
			void Unregister();

			// @ ThreadMessageController
			// Creates new proxy queue of specified type.
			ThreadMessageProxyQueue* CreateProxyQueue( U32ID queueID );

			// @ ThreadMessageController
			// Creates new proxy queue of specified type.
			ThreadMessageProxyQueue* CreateProxyQueue( const ThreadProxyQueueCreateInfo& createInfo );

			// @ ThreadMessageController
			//
			Result SendMessage( const CoreMessageSendRequest& sendRequest, const CoreMessageHandle& message );

			// @ ThreadMessageController
			//
			CoreSyncMessageSendResult SendMessage( const CoreSyncMessageSendRequest& sendRequest, const CoreMessageHandle& message );

			// @ ThreadMessageController
			// Waits for response for the most recent sync message.
			CoreMessageResponse* WaitForResponse();

			// @ ThreadMessageController
			// Waits for response for the most recent sync message.
			CoreMessageResponse* WaitForResponse( CoreMessage& message );

			// @ ThreadMessageController
			//
			CoreMessageResponse* GetResponse();

			// @ ThreadMessageController
			//
			CoreMessageResponse* GetResponse( CoreMessage& message );

			// @ ThreadMessageController, ThreadMessageProxyQueue
			//
			CoreMessageHandle PeekMessage( ThreadMessageProxyQueue* sourceQueue = nullptr );

			// @ ThreadMessageController, ThreadMessageProxyQueue
			//
			CoreMessageHandle WaitForMessage( ThreadMessageProxyQueue* sourceQueue = nullptr );

			// @ ThreadMessageController, ThreadMessageProxyQueue
			//
			CoreMessageHandle WaitForMessage( const Milliseconds& timeout, ThreadMessageProxyQueue* sourceQueue = nullptr );

			// @ ThreadMessageProxyQueue
			//
			bool AddThreadMessageFilter( MessageFilter messageFilter, ThreadMessageProxyQueue* proxyQueue, bool overwrite );

			// @ ThreadMessageProxyQueue
			// 
			void RemoveThreadMessageFilter( MessageFilter messageFilter, ThreadMessageProxyQueue* proxyQueue = nullptr );

			// @ ThreadMessageProxyQueue
			//
			void ReleaseProxyQueue( ThreadMessageProxyQueue* queue );

			// @ Thread
			//
			ThreadMessageController* GetController();

			// @ ThreadMessageController
			//
			bool IsProxyQueueActive( U32ID queueID ) const;

			// @ ThreadMessageController
			//
			bool IsProxyQueueRegistered( U32ID queueID ) const;

		private:
			//
			ThreadMessageProxyQueue* _AddQueue( U32ID queueID, ThreadMessageProxyQueuePtr&& queue );

			//
			ThreadMessageProxyQueue* _GetQueue( U32ID queueID );

			//
			std::pair<ThreadMessageProxyQueue*, U32ID> _CheckQueueCreateRequest( U32ID queueID );

		private:
			//
			static std::atomic<U32ID> _queueIDGen;

		private:
			//
			CoreMessageSystemRoot* _messageSystemRoot;

			//
			Thread* _thread;

			//
			ThreadUID _threadUID;

			//
			ThreadMessageController _controller;

			//
			std::unique_ptr<CoreMessageReceiver> _receiver;

			//
			std::unique_ptr<CoreMessageSender> _sender;

			//
			ThreadProxyQueueList _threadProxyQueues;
		};


		inline ThreadMessageController* ThreadMessageTransceiver::GetController()
		{
			return &( this->_controller );
		}

		inline bool ThreadMessageTransceiver::IsProxyQueueActive( U32ID queueID ) const
		{
			auto queueInfoRef = this->_threadProxyQueues.find( queueID );
			return ( queueInfoRef != this->_threadProxyQueues.end() ) ? !queueInfoRef->value.registeredFilters.empty() : false;
		}

		inline bool ThreadMessageTransceiver::IsProxyQueueRegistered( U32ID queueID ) const
		{
			auto queueInfoRef = this->_threadProxyQueues.find( queueID );
			return queueInfoRef != this->_threadProxyQueues.end();
		}

	}
}


#endif /* __Exs_Core_ThreadMessageTransceiver_H__ */
