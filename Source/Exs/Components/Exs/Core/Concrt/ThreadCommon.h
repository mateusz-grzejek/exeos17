
#ifndef __Exs_Core_ThreadCommon_H__
#define __Exs_Core_ThreadCommon_H__

#include "ConcrtCommon.h"
#include <stdx/concurrent_data_pool.h>


namespace Exs
{
	namespace Concrt
	{


		struct ThreadLocalStorage;
		struct ThreadSystemRegInfo;
		struct ThreadSharedSyncContext;

		class Thread;
		class ThreadRegistry;
		class ThreadSyncGroup;
		class ThreadSystemManager;

		using ChildThreadList = std::list<Thread*>;

		using ChildThreadRef = ChildThreadList::iterator;


		///
		template <typename Thr>
		using ThreadObjectHandle = std::shared_ptr<Thr>;


		enum class ThreadUpdateResult : Enum
		{
			Continue = 0,

			Exit = 1
		};


		enum ThreadPermissionFlags : Enum
		{
			// Thread is allowed to spawn child threads. If this flag is not set, an attempt to create a new thread will result in error.
			ThreadPermission_Create_Thread = 0x0010,

			ThreadPermission_Msg_Receive = 0x0100,

			ThreadPermission_Msg_Send = 0x0200,

			ThreadPermission_Msg_Send_Receive = ThreadPermission_Msg_Receive | ThreadPermission_Msg_Send,

			ThreadPermission_Default = ThreadPermission_Create_Thread | ThreadPermission_Msg_Send_Receive
		};


		/// Represents properties of a thread object. Properties define capabilities of a thread
		/// and its behavior. Specified on creation, they cannot be changed afterwards.
		enum ThreadRunFlags : Enum
		{
			ThreadRun_Detached = 0x10000,

			ThreadRun_Enable_Parent_Sync = 0x20000,

			ThreadRun_Default = ThreadRun_Enable_Parent_Sync,
		};


		struct ThreadProperties
		{
			//
			std::string name;

			//
			Thread_ref_id_t refID = Thread_Ref_ID_Auto;

			//
			stdx::mask<ThreadPermissionFlags> permissions = ThreadPermission_Default;
		};


		///<summary>
		///</summary>
		struct ThreadCreateInfo
		{
			//
			ThreadProperties properties;

			//
			stdx::mask<ThreadRunFlags> runFlags = ThreadRun_Default;
		};


		///<summary>
		///</summary>
		struct ThreadEnvironmentConfig
		{
			//
			Uint32 hardwareConcurrencyLevel = 0;
		};


		///<summary>
		///</summary>
		class EXS_LIBCLASS_CORE CurrentThread
		{
		public:
			//
			static ThreadLocalStorage* GetLocalStorage();

			//
			static Thread& GetThreadObject();

			//
			static ThreadSystemManager& GetThreadSystemManager();

			//
			static Thread_ref_id_t GetRefID();
		};


		class EXS_LIBCLASS_CORE ThreadUtils
		{
		public:
			//
			static Thread_ref_id_t GetThreadRefID( Thread* thread );


			static std::string GetUIDString( ThreadUID threadUID );
		};


	}
}


#endif /* __Exs_Core_ThreadCommon_H__ */
