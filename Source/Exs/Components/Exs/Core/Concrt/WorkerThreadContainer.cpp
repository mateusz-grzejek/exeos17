
#include <Concrt/WorkerThread.h>
#include <Concrt/WorkerThreadContainer.h>
#include "Precomp.h"


namespace Exs
{
	namespace Concrt
	{


		WorkerThreadContainer::WorkerThreadContainer( ConcrtSharedStateHandle concrtSharedState, WorkerThreadFactory& workerThreadFactory, std::string name )
			: _concrtSharedState( concrtSharedState )
			, _workerThreadFactory( &workerThreadFactory )
			, _name( name )
			, _internalSyncObject( concrtSharedState )
			, _workerThreadsNum( ATOMIC_VAR_INIT(0) )
			, _resizeExitRequestCounter( ATOMIC_VAR_INIT(0) )
			, _resizeControlCounter( 0 )
			, _internalState( 0 )
		{ }


		WorkerThreadContainer::~WorkerThreadContainer()
		{ }


		WorkerThreadHandle WorkerThreadContainer::AddWorker()
		{
			InternalMutexLock internalLock{ this->_internalSyncObject.GetMutex() };
			{
				auto checkResult = this->_CheckAddRequest( internalLock, 1, true, Timeout_Infinite );

				if ( checkResult.first )
				{
					if ( auto resizeResult = this->_Resize( internalLock, checkResult.second, checkResult.second + 1 ) )
					{
						auto threadHandle = resizeResult.threadList.front();

						return threadHandle;
					}
				}
			}

			return nullptr;
		}


		WorkerThreadContainer::WorkerThreadList WorkerThreadContainer::AddWorkers( Size_t threadsNum )
		{
			InternalMutexLock internalLock{ this->_internalSyncObject.GetMutex() };
			{
				auto checkResult = this->_CheckAddRequest( internalLock, threadsNum, true, Timeout_Infinite );

				if ( checkResult.first )
				{
					if ( auto resizeResult = this->_Resize( internalLock, checkResult.second, checkResult.second + threadsNum ) )
					{
						return resizeResult.threadList;
					}
				}
			}

			return WorkerThreadList();
		}


		void WorkerThreadContainer::Reset()
		{
		}


		WorkerThreadContainer::ResizeResult WorkerThreadContainer::Resize( Size_t threadsNum )
		{
			InternalMutexLock internalLock{ this->_internalSyncObject.GetMutex() };
			{
				auto checkResult = this->_CheckResizeRequest( internalLock, threadsNum, true, Timeout_Infinite );

				if ( !checkResult.first )
				{
					return ExsResult( RSC_Err_Failed );
				}

				return this->_Resize( internalLock, checkResult.second, threadsNum );
			}
		}


		WorkerThreadContainer::ResizeResult WorkerThreadContainer::TryResize( Size_t threadsNum )
		{
			InternalMutexLock internalLock{ this->_internalSyncObject.GetMutex() };
			{
				auto checkResult = this->_CheckResizeRequest( internalLock, threadsNum, false, 0 );

				if ( !checkResult.first )
				{
					return ExsResult( RSC_Err_Failed );
				}

				return this->_Resize( internalLock, checkResult.second, threadsNum );
			}
		}


		WorkerThreadContainer::ResizeResult WorkerThreadContainer::TryResizeFor( Size_t threadsNum, const Nanoseconds& timeout )
		{
			InternalMutexLock internalLock{ this->_internalSyncObject.GetMutex() };
			{
				auto checkResult = this->_CheckResizeRequest( internalLock, threadsNum, true, timeout );

				if ( !checkResult.first )
				{
					return ExsResult( RSC_Err_Failed );
				}

				return this->_Resize( internalLock, checkResult.second, threadsNum );
			}
		}


		void WorkerThreadContainer::WaitResizeComplete()
		{
			if ( this->_IsResizeFlagSet() )
			{
				InternalMutexLock internalLock{ this->_internalSyncObject.GetMutex() };
				{
					this->_WaitResizeComplete( internalLock, Timeout_Infinite );
				}
			}
		}


		void WorkerThreadContainer::WaitResizeCompleteFor( const Nanoseconds& timeout )
		{
			if ( this->_IsResizeFlagSet() )
			{
				InternalMutexLock internalLock{ this->_internalSyncObject.GetMutex() };
				{
					this->_WaitResizeComplete( internalLock, timeout );
				}
			}
		}


		std::vector<Worker_thread_index_t> WorkerThreadContainer::GetActiveWorkerIndexList( InternalMutexLock& ) const
		{
			auto activeThreadsNum = this->_workerThreadsNum.load( std::memory_order_acquire );

			std::vector<Worker_thread_index_t> result;
			result.reserve( activeThreadsNum );

			for ( const auto* threadState : this->_workerBaseStateArray )
			{
				if ( threadState->CheckStatus( WorkerThreadStatusFlag_Active ) )
				{
					result.push_back( threadState->GetWorkerIndex() );
				}
			}

			return result;
		}


		std::vector<WorkerThreadHandle> WorkerThreadContainer::GetActiveWorkerThreadList( InternalMutexLock& ) const
		{
			auto activeThreadsNum = this->_workerThreadsNum.load( std::memory_order_acquire );

			std::vector<WorkerThreadHandle> result;
			result.reserve( activeThreadsNum );

			for ( const auto* threadState : this->_workerBaseStateArray )
			{
				if ( threadState->CheckStatus( WorkerThreadStatusFlag_Active ) )
				{
					result.push_back( threadState->GetThreadHandle() );
				}
			}

			return result;
		}


		WorkerThreadHandle WorkerThreadContainer::GetWorkerThread( Worker_thread_index_t workerIndex ) const
		{
			auto& workerState = this->_GetWorkerBaseState( workerIndex );
			return workerState.GetThreadHandle();
		}


		bool WorkerThreadContainer::IsWorkerActive( Worker_thread_index_t workerIndex ) const
		{
			auto& workerState = this->_GetWorkerBaseState( workerIndex );
			return workerState.CheckStatus( WorkerThreadStatusFlag_Active );
		}


		void WorkerThreadContainer::SetBaseState( Worker_thread_index_t arrayIndex, WorkerThreadState& state )
		{
			this->_workerBaseStateArray[arrayIndex] = &state;
		}


		Worker_thread_index_t WorkerThreadContainer::QueryWorkerIndex( const WorkerThread& workerThread )
		{
			return workerThread.GetWorkerIndex();
		}


		bool WorkerThreadContainer::RegisterThread( WorkerThread& workerThread )
		{
			auto workerIndex = workerThread.GetIndex();

			auto& workerState = this->_GetWorkerBaseState( workerIndex );

			InternalMutexLock internalLock{ this->_internalSyncObject.GetMutex() };
			{
				workerState.SetStatusFlags( WorkerThreadStatusFlag_Registered );

				auto newThreadsNum = this->_workerThreadsNum.fetch_add( 1, std::memory_order_relaxed ) + 1;

				--this->_resizeControlCounter;

				this->_OnSizeChange( internalLock, newThreadsNum, true );
			}

			return true;
		}


		void WorkerThreadContainer::UnregisterThread( WorkerThread& workerThread )
		{
			auto workerIndex = workerThread.GetIndex();

			auto& workerState = this->_GetWorkerBaseState( workerIndex );

			InternalMutexLock internalLock{ this->_internalSyncObject.GetMutex() };
			{
				bool resizeExitRequest = workerState.CheckStatus( WorkerThreadStatusFlag_Resize_Exit_Request );

				workerState.UnsetStatusFlags( WorkerThreadStatusFlag_Registered );
				workerState.ResetWorkerData();

				this->ReleaseWorkerIndex( workerIndex );

				auto newThreadsNum = this->_workerThreadsNum.fetch_sub( 1, std::memory_order_relaxed ) - 1;

				if ( resizeExitRequest )
				{
					--this->_resizeControlCounter;
				}

				this->_OnSizeChange( internalLock, newThreadsNum, resizeExitRequest );
			}
		}


		bool WorkerThreadContainer::CheckExitRequest( WorkerThread& workerThread )
		{
			if ( this->_IsResizeFlagSet() )
			{
				for ( auto releaseThreadsNum = this->_resizeExitRequestCounter.load( std::memory_order_relaxed ); releaseThreadsNum > 0; )
				{
					//
					if ( this->_resizeExitRequestCounter.compare_exchange_weak( releaseThreadsNum, releaseThreadsNum - 1, std::memory_order_acq_rel, std::memory_order_relaxed ) )
					{
						auto& workerState = this->_GetWorkerBaseState( workerThread );

						//
						workerState.SetStatusFlags( WorkerThreadStatusFlag_Resize_Exit_Request );

						return true;
					}
				}
			}

			return false;
		}


		void WorkerThreadContainer::SetWorkerStatusFlags( WorkerThread& workerThread, stdx::mask<Worker_thread_status_value_t> flags )
		{
			auto& workerState = this->_GetWorkerBaseState( workerThread );
			workerState.SetStatusFlags( flags );
		}


		void WorkerThreadContainer::UnsetWorkerStatusFlags( WorkerThread& workerThread, stdx::mask<Worker_thread_status_value_t> flags )
		{
			auto& workerState = this->_GetWorkerBaseState( workerThread );
			workerState.UnsetStatusFlags( flags );
		}


		bool WorkerThreadContainer::CheckWorkerStatus( const WorkerThread& workerThread, stdx::mask<Worker_thread_status_value_t> flags ) const
		{
			auto& workerState = this->_GetWorkerBaseState( workerThread );
			return workerState.CheckStatus( flags );
		}


		bool WorkerThreadContainer::CheckWorkerStatusAny( const WorkerThread& workerThread, stdx::mask<Worker_thread_status_value_t> flags ) const
		{
			auto& workerState = this->_GetWorkerBaseState( workerThread );
			return workerState.CheckStatusAny( flags );
		}


		WorkerThreadState& WorkerThreadContainer::_GetWorkerBaseState( Worker_thread_index_t workerIndex ) const
		{
			auto* workerStatePtr = this->_workerBaseStateArray[workerIndex];

			return *workerStatePtr;
		}


		WorkerThreadState& WorkerThreadContainer::_GetWorkerBaseState( const WorkerThread& thread ) const
		{
			auto workerIndex = thread.GetIndex();

			return this->_GetWorkerBaseState( workerIndex );
		}


		std::pair<bool, Size_t> WorkerThreadContainer::_CheckAddRequest( InternalMutexLock& internalLock, Size_t newThreadsNum, bool waitOnResize, const Nanoseconds& waitTimeout )
		{
			std::pair<bool, Size_t> result;
			result.first = false;

			if ( newThreadsNum == 0 )
			{
				return result;
			}

			if ( waitOnResize )
			{
				this->_WaitResizeComplete( internalLock, waitTimeout );
			}

			result.second = this->_workerThreadsNum.load( std::memory_order_relaxed );

			if ( this->_IsResizeFlagSet() || ( newThreadsNum > maxSize - result.second ) )
			{
				return result;
			}

			result.first = true;

			return result;
		}


		std::pair<bool, Size_t> WorkerThreadContainer::_CheckResizeRequest( InternalMutexLock& internalLock, Size_t threadsNum, bool waitOnResize, const Nanoseconds& waitTimeout )
		{
			std::pair<bool, Size_t> result;
			result.first = false;

			if ( waitOnResize )
			{
				this->_WaitResizeComplete( internalLock, waitTimeout );
			}

			result.second = this->_workerThreadsNum.load( std::memory_order_relaxed );

			if ( this->_IsResizeFlagSet() || ( threadsNum > maxSize ) || ( threadsNum == result.second ) )
			{
				return result;
			}

			result.first = true;

			return result;
		}


		WorkerThreadContainer::ResizeResult WorkerThreadContainer::_Resize( InternalMutexLock& internalLock, Size_t currentThreadsNum, Size_t threadsNum )
		{
			ResizeResult resizeResult;

			resizeResult.prevSize = currentThreadsNum;
			resizeResult.result = ExsResult( RSC_Err_Failed );

			//
			this->_SetResizeFlag();

			if ( threadsNum > currentThreadsNum )
			{
				resizeResult.threadList = this->_CreateWorkerThreads( internalLock, threadsNum - currentThreadsNum );
				resizeResult.result = ExsResult( RSC_Success );
			}
			else if ( threadsNum < currentThreadsNum )
			{
				this->_SetExitRequestCounter( internalLock, currentThreadsNum - threadsNum );
				resizeResult.result = ExsResult( RSC_Success );
			}

			return resizeResult;
		}


		WorkerThreadContainer::WorkerThreadList WorkerThreadContainer::_CreateWorkerThreads( InternalMutexLock& internalLock, Size_t threadsNum )
		{
			WorkerThreadList threadList;
			threadList.reserve( threadsNum );

			ThreadLaunchInfo threadLanuchInfo;

			// Async threads are created with default permissions, except they cannot start threads on their own
			// and do not have access to the core messaging system (communication is implemented inside sub-classes
			// and uses direct queues or simillar mechanisms).
			threadLanuchInfo.properties.permissions = ThreadPermission_Default;
			threadLanuchInfo.properties.permissions.unset( ThreadPermission_Create_Thread );
			threadLanuchInfo.properties.permissions.unset( ThreadPermission_Msg_Send_Receive );

			// Worker threads are started with the default policy, except they are not synchronized with parent
			// thread. Synchronization is performed by the pool itself (threads are ordered to finish when pool
			// is closed), so the concrt-level synchronization should be off.
			threadLanuchInfo.runFlags = ThreadRun_Default;
			threadLanuchInfo.runFlags.unset( ThreadRun_Enable_Parent_Sync );

			for ( Size_t threadNum = 0; threadNum < threadsNum; ++threadNum )
			{
				if ( auto workerThreadHandle = this->_CreateThread( threadLanuchInfo ) )
				{
					threadList.push_back( workerThreadHandle );
				}
			}

			this->_resizeControlCounter = threadList.size();

			return threadList;
		}


		WorkerThreadHandle WorkerThreadContainer::_CreateThread( ThreadLaunchInfo& threadLanuchInfo )
		{
			auto workerIndex = this->ReserveWorkerIndex();

			if ( workerIndex != invalidWorkerIndex )
			{
				auto& workerState = this->_GetWorkerBaseState( workerIndex );

				// Name consists of predefined string and index (starting from 0) of that thread.
				threadLanuchInfo.properties.name = this->_name + std::string( ":W" ) + std::to_string( workerIndex );

				if ( auto threadHandle = this->_workerThreadFactory->CreateWorkerThread( this->_concrtSharedState, threadLanuchInfo, *this, workerIndex ) )
				{
					workerState.SetWorkerData( threadHandle, threadHandle->GetRefID(), workerIndex );

					if ( this->_RunWorkerThread( threadHandle, threadLanuchInfo ) )
					{
						return threadHandle;
					}

					workerState.ResetWorkerData();
				}

				this->ReleaseWorkerIndex( workerIndex );
			}

			return nullptr;
		}


		void WorkerThreadContainer::_SetExitRequestCounter( InternalMutexLock& internalLock, Size_t exitCounterValue )
		{
			this->_resizeControlCounter = exitCounterValue;
			this->_resizeExitRequestCounter.store( exitCounterValue, std::memory_order_relaxed );
		}


		void WorkerThreadContainer::_WaitResizeComplete( InternalMutexLock& internalLock, const Nanoseconds& timeout )
		{
			this->_internalSyncObject.WaitFor( internalLock, timeout, [this]() -> bool {
				return !this->_IsResizeFlagSet();
			} );
		}


		void WorkerThreadContainer::_OnSizeChange( InternalMutexLock& internalLock, Size_t threadsNum, bool resizeEvent )
		{
			if ( resizeEvent && ( this->_resizeControlCounter == 0 ) )
			{
				this->_ClearResizeFlag();

				this->_internalSyncObject.NotifyAll( internalLock );
			}
		}


		bool WorkerThreadContainer::_RunWorkerThread( WorkerThreadHandle workerThread, const ThreadLaunchInfo& threadLanuchInfo )
		{
			try
			{
				auto runResult = ThreadLauncher::RunThread(workerThread, threadLanuchInfo);

				if ( runResult == RSC_Success )
				{
					return true;
				}
			}
			catch(...)
			{
			}

			return false;
		}


	}
}
