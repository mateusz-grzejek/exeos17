
#ifndef __Exs_Core_ThreadSyncGroup_H__
#define __Exs_Core_ThreadSyncGroup_H__

#include "SharedSyncObject.h"
#include "ThreadInternal.h"


namespace Exs
{
	namespace Concrt
	{


		class EXS_LIBCLASS_CORE ThreadSyncGroup
		{
		public:
			ThreadSyncGroup( ConcrtSharedStateHandle concrtSharedState, Size_t threadsNum, ThreadExecutionEvent relSyncEvent = ThreadExecutionEvent::None )
				: _concrtSharedState( concrtSharedState )
				, _threadsNum( threadsNum )
				, _registeredThreadsNum( 0 )
				, _relSyncEvent( relSyncEvent )
				, _syncObject( concrtSharedState )
			{ }

			~ThreadSyncGroup()
			{
				if ( this->_relSyncEvent != ThreadExecutionEvent::None )
				{
					this->Wait( this->_relSyncEvent );
				}
			}

			void AddThread( Thread& thread );

			void SubscriveEvent( ThreadExecutionEvent event );

			void Wait( ThreadExecutionEvent event );

			void NotifyOne( Thread& thread, ThreadExecutionEvent event );

		private:
			struct EventState
			{
				Size_t notifyCounter = 0;
			};

			using InternalSyncObject = SharedSyncObject<>;

			using SunscribedEventList = stdx::assoc_array<ThreadExecutionEvent, EventState>;

		private:
			//
			EventState* _GetEventState( ThreadExecutionEvent event );

			//
			bool _CheckNotifyCounter( EventState& eventState );

			//
			bool _CheckNotifyCounter( ThreadExecutionEvent event );

		private:
			//
			ConcrtSharedStateHandle _concrtSharedState;

			//
			const Size_t _threadsNum;

			//
			Size_t _registeredThreadsNum;

			//
			ThreadExecutionEvent _relSyncEvent;

			//
			InternalSyncObject _syncObject;

			//
			SunscribedEventList _subscribedEvents;
		};


	}
}


#endif /* __Exs_Core_ThreadSyncGroup_H__ */
