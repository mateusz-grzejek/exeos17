
#include "Precomp.h"
#include <Exs/Core/Concrt/ThreadMessageController.h>
#include <Exs/Core/Concrt/ThreadMessageTransceiver.h>
#include <Exs/Core/Concrt/CoreMessage.h>


namespace Exs
{
	namespace Concrt
	{


		ThreadMessageController::ThreadMessageController( ThreadMessageTransceiver* transceiver )
			: _transceiver( transceiver )
		{ }


		ThreadMessageController::~ThreadMessageController()
		{ }


		ThreadMessageProxyQueue* ThreadMessageController::CreateProxyQueue( U32ID queueID )
		{
			return this->_transceiver->CreateProxyQueue( queueID );
		}


		ThreadMessageProxyQueue* ThreadMessageController::CreateProxyQueue( const ThreadProxyQueueCreateInfo& createInfo )
		{
			return this->_transceiver->CreateProxyQueue( createInfo );
		}


		bool ThreadMessageController::AddThreadMessageFilter( MessageFilter messageFilter, ThreadMessageProxyQueue* proxyQueue, bool overwrite )
		{
			return this->_transceiver->AddThreadMessageFilter( messageFilter, proxyQueue, overwrite );
		}


		void ThreadMessageController::RemoveThreadMessageFilter( MessageFilter messageFilter, ThreadMessageProxyQueue* proxyQueue )
		{
			return this->_transceiver->RemoveThreadMessageFilter( messageFilter, proxyQueue );
		}


		CoreMessageHandle ThreadMessageController::PeekMessage()
		{
			return this->_transceiver->PeekMessage( nullptr );
		}


		CoreMessageHandle ThreadMessageController::WaitForMessage()
		{
			return this->_transceiver->WaitForMessage( nullptr );
		}


		CoreMessageHandle ThreadMessageController::WaitForMessage( const Milliseconds& timeout )
		{
			return this->_transceiver->WaitForMessage( timeout, nullptr );
		}


		Result ThreadMessageController::SendMessage( ThreadUID threadUID, const CoreMessageHandle& message )
		{
			CoreMessageSendRequest sendRequest;
			sendRequest.receiverID = threadUID.GetRefID();

			return this->_transceiver->SendMessage( sendRequest, message );
		}


		Result ThreadMessageController::SendMessage( Thread_ref_id_t threadRefID, const CoreMessageHandle& message )
		{
			CoreMessageSendRequest sendRequest;
			sendRequest.receiverID = threadRefID;

			return this->_transceiver->SendMessage( sendRequest, message );
		}


		Result ThreadMessageController::SendMessage( const CoreMessageSendRequest& sendRequest, const CoreMessageHandle& message )
		{
			return this->_transceiver->SendMessage( sendRequest, message );
		}


		CoreSyncMessageSendResult ThreadMessageController::SendMessage( const CoreSyncMessageSendRequest& sendRequest, const CoreMessageHandle& message )
		{
			return this->_transceiver->SendMessage( sendRequest, message );
		}


		Result ThreadMessageController::SendSimpleMessage( ThreadUID threadUID, Message_code_t messageCode, MessagePriority priority )
		{
			auto message = CreateCoreMessage( messageCode, priority );

			CoreMessageSendRequest sendRequest;
			sendRequest.receiverID = threadUID.GetRefID();

			return this->SendMessage( sendRequest, message );
		}


		Result ThreadMessageController::SendSimpleMessage( Thread_ref_id_t threadRefID, Message_code_t messageCode, MessagePriority priority )
		{
			auto message = CreateCoreMessage( messageCode, priority );

			CoreMessageSendRequest sendRequest;
			sendRequest.receiverID = threadRefID;

			return this->SendMessage( sendRequest, message );
		}


		CoreMessageResponse* ThreadMessageController::WaitForResponse()
		{
			return this->_transceiver->WaitForResponse();
		}


		CoreMessageResponse* ThreadMessageController::WaitForResponse( CoreMessage& message )
		{
			return this->_transceiver->WaitForResponse( message );
		}


		CoreMessageResponse* ThreadMessageController::GetResponse()
		{
			return this->_transceiver->GetResponse();
		}


		CoreMessageResponse* ThreadMessageController::GetResponse( CoreMessage& message )
		{
			return this->_transceiver->GetResponse( message );
		}


	}
}
