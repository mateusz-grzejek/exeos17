
#ifndef __Exs_Core_ThreadSyncCommon_H__
#define __Exs_Core_ThreadSyncCommon_H__

#include "SyncConditionVariable.h"
#include "ThreadInternal.h"


namespace Exs
{
	namespace Concrt
	{


		enum class ThreadSyncConditionVariableTypeTag : Enum
		{
			Any,

			System,

			Undefined
		};


		struct ThreadSyncConditionVariableContainer
		{
			SyncConditionVariableAny cvAny;

			SyncConditionVariableSystem cvSystem;

			ThreadSyncConditionVariableTypeTag typeTag = ThreadSyncConditionVariableTypeTag::Undefined;
		};


		/**
		 * @brief Per-thread sync context. Contains data and state used for thread wait synchronization, including condition
		 * variables the thread can wait for. This data is stored in the sync controller and assigned to the thread when
		 * it is registered within the threading system.
		 */
		struct ThreadSharedSyncContext
		{
			// Thread's conditon variables.
			ThreadSyncConditionVariableContainer conditionVariableContainer;
		};


		/**
		 * @brief Represents info about an active wait.
		 */
		struct ThreadActiveWaitInfo
		{
			// Thread which is currently waiting (executing cv.Wait/WaitFor()).
			Thread* thread;

			// SyncObject the thread is waiting for.
			SyncObject* syncObject;
		};


		template <class TLock>
		struct ThreadSyncConditionVariableProxy
		{
			static void NotifyOne( ThreadSharedSyncContext& threadSharedSyncContext )
			{
				if ( threadSharedSyncContext.conditionVariableContainer.typeTag != ThreadSyncConditionVariableTypeTag::Any )
				{
					throw 0;
				}

				threadSharedSyncContext.conditionVariableContainer.cvAny.NotifyOne();
			}

			template <class... TArgs>
			static void Wait( ThreadSharedSyncContext& threadSharedSyncContext, TLock& lock, TArgs&& ...args )
			{
				threadSharedSyncContext.conditionVariableContainer.typeTag = ThreadSyncConditionVariableTypeTag::Any;
				threadSharedSyncContext.conditionVariableContainer.cvAny.Wait( lock, std::forward<TArgs>( args )... );
				threadSharedSyncContext.conditionVariableContainer.typeTag = ThreadSyncConditionVariableTypeTag::Undefined;
			}

			template <class... TArgs>
			static bool WaitFor( ThreadSharedSyncContext& threadSharedSyncContext, TLock& lock, TArgs&& ...args )
			{
				threadSharedSyncContext.conditionVariableContainer.typeTag = ThreadSyncConditionVariableTypeTag::Any;
				bool waitResult = threadSharedSyncContext.conditionVariableContainer.cvAny.WaitFor( lock, std::forward<TArgs>( args )... );
				threadSharedSyncContext.conditionVariableContainer.typeTag = ThreadSyncConditionVariableTypeTag::Undefined;

				return waitResult;
			}
		};


		template <>
		struct ThreadSyncConditionVariableProxy<SystemMutexLock >
		{
			static void NotifyOne( ThreadSharedSyncContext& threadSharedSyncContext )
			{
				if ( threadSharedSyncContext.conditionVariableContainer.typeTag != ThreadSyncConditionVariableTypeTag::System )
				{
					throw 0;
				}

				threadSharedSyncContext.conditionVariableContainer.cvSystem.NotifyOne();
			}

			template <class... TArgs>
			static void Wait( ThreadSharedSyncContext& threadSharedSyncContext, SystemMutexLock& lock, TArgs&& ...args )
			{
				threadSharedSyncContext.conditionVariableContainer.typeTag = ThreadSyncConditionVariableTypeTag::System;
				threadSharedSyncContext.conditionVariableContainer.cvSystem.Wait( lock, std::forward<TArgs>( args )... );
				threadSharedSyncContext.conditionVariableContainer.typeTag = ThreadSyncConditionVariableTypeTag::Undefined;
			}

			template <class... TArgs>
			static bool WaitFor( ThreadSharedSyncContext& threadSyncContext, SystemMutexLock& lock, TArgs&& ...args )
			{
				threadSyncContext.conditionVariableContainer.typeTag = ThreadSyncConditionVariableTypeTag::System;
				bool waitResult = threadSyncContext.conditionVariableContainer.cvSystem.WaitFor( lock, std::forward<TArgs>( args )... );
				threadSyncContext.conditionVariableContainer.typeTag = ThreadSyncConditionVariableTypeTag::Undefined;

				return waitResult;
			}
		};


	}
}


#endif /* __Exs_Core_ThreadSyncCommon_H__ */
