
#ifndef __Exs_Core_CoreMessageSystemRoot_H__
#define __Exs_Core_CoreMessageSystemRoot_H__

#include "CoreMessageDispatcher.h"
#include <queue>


namespace Exs
{
	namespace Concrt
	{


		class CoreMessageSender;
		class ThreadSystemManager;


		class CoreMessageSystemRoot
		{
			friend class CoreMessageSender;

		public:
			CoreMessageSystemRoot();

			/// Registers message receiver with specified ID in the dispatcher's registry at the index 'receiverIndex'. Index must be unique
			/// for every registered receiver and must be in rage [0, Config::CCRT_MSG_Max_Registered_Receivers_Num - 1]; It is up to the
			/// client of this class how it organizes/assigns indexes to receivers.
			Result RegisterMessageReceiver( CoreMessageReceiverID receiverID, Uint32 receiverIndex, CoreMessageReceiver& receiver );

			/// Unregisters receiver with specified ID from the messaging system. Receiver will not be able to receive any messages after
			/// it is unregistered. Every message send using such inactive ID, will be enqueued in temporary, per-ID message queue. If 'receiver'
			/// is not NULL, additional validation is performed (whether this pointer matches address of the receiver stored at the index assigned
			/// to the specified ID) and exception is thrown if it fails. If there is no receiver with such ID, request is silently ignored.
			void UnregisterMessageReceiver( CoreMessageReceiverID receiverID, CoreMessageReceiver* receiver = nullptr );

		friendapi:
			// @ CoreMessageSender
			// 
			Result DispatchMessage( const CoreMessageSendRequest& sendRequest, const CoreMessageHandle& message );

		private:
			//
			CoreMessageDispatcher _dispatcher;
		};


	}
}


#endif /* __Exs_Core_CoreMessageSystemRoot_H__ */
