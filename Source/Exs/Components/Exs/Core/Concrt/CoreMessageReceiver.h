
#ifndef __Exs_Core_CoreMessageReceiver_H__
#define __Exs_Core_CoreMessageReceiver_H__

#include "CoreMessageQueue.h"
#include "ThreadSyncObject.h"
#include <stdx/assoc_array.h>


namespace Exs
{
	namespace Concrt
	{


		class CoreMessageDispatcher;
		class CoreMessageSystemRoot;


		/**
		 *
		 */
		class CoreMessageReceiver
		{
			friend class CoreMessageDispatcher;

		public:
			CoreMessageReceiver( CoreMessageSystemRoot& messageSystemRoot, Thread& thread, CoreMessageReceiverID id );

			/**
			 *
			 * @param externalQueue
			 */
			void RegisterExternalQueue( CoreMessageQueue& externalQueue );

			/**
			 *
			 * @param externalQueue
			 */
			void UnregisterExternalQueue( CoreMessageQueue& externalQueue );

			/**
			 *
			 * @param messageFilter
			 * @param externalQueue
			 * @param overwrite
			 * @return
			 */
			bool AddMessageFilter( MessageFilter messageFilter, CoreMessageQueue& externalQueue, bool overwrite );

			/**
			 *
			 * @param messageFilter
			 * @param externalQueue
			 */
			void RemoveMessageFilter( MessageFilter messageFilter, CoreMessageQueue* externalQueue = nullptr );

			/**
			 *
			 * @param sourceQueue
			 * @return
			 */
			CoreMessageHandle PeekMessage( CoreMessageQueue* sourceQueue = nullptr );

			/**
			 *
			 * @param sourceQueue
			 * @return
			 */
			CoreMessageHandle WaitForMessage( CoreMessageQueue* sourceQueue = nullptr );

			/**
			 *
			 * @param timeout
			 * @param sourceQueue
			 * @return
			 */
			CoreMessageHandle WaitForMessage( const Milliseconds& timeout, CoreMessageQueue* sourceQueue = nullptr );

			/**
			 *
			 * @return
			 */
			CoreMessageReceiverID GetID() const;

		friendapi:
			/**
			 * @brief Unchecked push, used by dispatcher to insert cached messages from before the registration.
			 * @caller CoreMessageDispatcher
			 * @param message
			 * @return
			 */
			Result PushMessage( const CoreMessageHandle& message );

			/**
			 * @brief
			 * @caller CoreMessageDispatcher
			 * @param message
			 * @return
			 */
			Result PostMessage( const CoreMessageHandle& message );

		private:
			//
			struct ExternalQueueState
			{
				bool isActive = false;

				CoreMessageQueue* queue = nullptr;
			};

			//
			using InternalSyncObject = ThreadSyncObject<>;

			//
			using MessageFilterMap = stdx::assoc_array<MessageFilter, CoreMessageQueue*, MessageFilterComp>;

			//
			using ExternalQueueStateMap = stdx::assoc_array<CoreMessageQueue*, ExternalQueueState>;

		private:
			///
			CoreMessageQueue* _GetTargetQueue( Message_code_t messageCode, Message_class_t messageClass );

			///
			ExternalQueueState* _GetExternalQueueState( CoreMessageQueue* queue );

		private:
			// Pointer to the root object of the core messaging system.
			CoreMessageSystemRoot* _messageSystemRoot;

			// Pointer to the thread object which owns this receiver.
			Thread* _thread;

			//
			CoreMessageReceiverID _id;

			//
			CoreMessageQueue _defaultQueue;

			//
			InternalSyncObject _syncObject;

			//
			CoreMessageQueue* _currentWaitQueue;

			//
			MessageFilterMap _messageFilterMap;

			//
			ExternalQueueStateMap _externalQueueStateMap;

			//
			LightSharedMutex _queueDataLock;
		};


		inline CoreMessageSenderID CoreMessageReceiver::GetID() const
		{
			return this->_id;
		}


	}
}


#endif /* __Exs_Core_CoreMessageReceiver_H__ */
