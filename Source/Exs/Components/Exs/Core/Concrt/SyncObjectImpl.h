
#ifndef __Exs_Core_SyncObjectImplProxy_H__
#define __Exs_Core_SyncObjectImplProxy_H__

#include "SyncObject.h"


namespace Exs
{
	namespace Concrt
	{


		/**
		 *
		 */
		class EXS_LIBCLASS_CORE SyncObjectImplBase
		{
		protected:
			/**
			 * @brief  Helper methods used to signal the thread.
			 * @tparam TLock Type of the lock (mutex) used by the SyncObject.
			 * @param threadWaitSyncState WaitState created for the thread.
			 * @param signalFlags Signal flags to be set before signalling.
			 */
			template <class TLock>
			static void SignalThread( ThreadWaitSyncState& threadWaitSyncState, SyncObjectSignalFlags signalFlags )
			{
				// Set appropriate flag (Notify/Interrupt). Required for explicit signalling.
				threadWaitSyncState.SetStateFlags( signalFlags );

				// Notify the proper CV. CV to signal is selected using type of the lock used by the SO.
				ThreadSyncConditionVariableProxy<TLock>::NotifyOne( *( threadWaitSyncState.threadSharedSyncContext ) );
			}

			/**
			 * @brief Helper proxy method used to get TreadSystemManager using specified thread object (to avoid #include Thread).
			 * @param thread
			 * @return
			 */
			static ThreadSystemManager& GetThreadSystemManager( Thread& thread );
		};


		/**
		 * @brief
		 * @tparam TMutex Type of mutex which SyncObject is to be used with.
		 * @tparam TImplType Base class which implements required interfaces.
		 */
		template <class TMutex, class TImplType>
		class SyncObjectImplProxy : public SyncObject, public TImplType
		{
		public:
			using Mutex = TMutex;
			using MutexLock = std::unique_lock<TMutex>;

		public:
			template <class... TArgs>
			explicit SyncObjectImplProxy( ThreadSystemManager& threadSystemManager,  TArgs&& ...args )
			: SyncObject( threadSystemManager )
			, TImplType( std::forward<TArgs>( args )... )
			{ }

			virtual Size_t Abort() override final
			{
				std::unique_lock<TMutex> lock{ this->_mutex };
				{
					return this->TImplType::AbortImpl( lock );
				}
			}

			virtual bool Interrupt( Thread& thread ) override final
			{
				std::unique_lock<TMutex> lock{ this->_mutex };
				{
					return this->TImplType::InterruptImpl( lock, thread );
				}
			}

			template <class TLock>
			bool NotifyOne( TLock& lock )
			{
				return this->TImplType::NotifyOneImpl( lock );
			}

			template <class TLock>
			Size_t NotifyAll( TLock& lock )
			{
				return this->TImplType::NotifyAllImpl( lock );
			}

			//
			template <class TLock>
			WaitResult Wait( TLock& lock )
			{
				auto waitPredicate = []( ThreadWaitSyncState& syncContext ) -> bool {
					return syncContext.IsNotifyFlagSet() || syncContext.IsInterruptFlagSet();
				};

				return this->_Wait( nullptr, lock, waitPredicate );
			}

			//
			template <class TLock>
			WaitResult Wait( Thread& currentThread, TLock& lock )
			{
				auto waitPredicate = []( ThreadWaitSyncState& syncContext ) -> bool {
					return syncContext.IsNotifyFlagSet() || syncContext.IsInterruptFlagSet();
				};

				return this->_Wait( &currentThread, lock, waitPredicate );
			}

			//
			template <class TLock, class Predicate>
			WaitResult Wait( TLock& lock, Predicate predicate )
			{
				auto waitPredicate = [&predicate]( ThreadWaitSyncState& syncContext ) -> bool {
					return ( predicate() && syncContext.IsNotifyFlagSet() ) || syncContext.IsInterruptFlagSet();
				};

				return this->_Wait( nullptr, lock, waitPredicate );
			}

			//
			template <class TLock, class Predicate>
			WaitResult Wait( Thread& currentThread, TLock& lock, Predicate predicate )
			{
				auto waitPredicate = [&predicate]( ThreadWaitSyncState& syncContext ) -> bool {
					return ( predicate() && syncContext.IsNotifyFlagSet() ) || syncContext.IsInterruptFlagSet();
				};

				return this->_Wait( &currentThread, lock, waitPredicate );
			}

			//
			template <class TLock, class Duration_t>
			WaitResult WaitFor( TLock& lock, const Duration_t& duration )
			{
				auto waitPredicate = []( ThreadWaitSyncState& syncContext ) -> bool {
					return syncContext.IsNotifyFlagSet() || syncContext.IsInterruptFlagSet();
				};

				return this->_WaitFor( nullptr, lock, duration, waitPredicate );
			}

			//
			template <class TLock, class Duration_t>
			WaitResult WaitFor( Thread& currentThread, TLock& lock, const Duration_t& duration )
			{
				auto waitPredicate = []( ThreadWaitSyncState& syncContext ) -> bool {
					return syncContext.IsNotifyFlagSet() || syncContext.IsInterruptFlagSet();
				};

				return this->_WaitFor( &currentThread, lock, duration, waitPredicate );
			}

			//
			template <class TLock, class Duration_t, class Predicate>
			WaitResult WaitFor( TLock& lock, const Duration_t& duration, Predicate predicate )
			{
				auto waitPredicate = [&predicate]( ThreadWaitSyncState& syncContext ) -> bool {
					return ( predicate() && syncContext.IsNotifyFlagSet() ) || syncContext.IsInterruptFlagSet();
				};

				return this->_WaitFor( nullptr, lock, duration, waitPredicate );
			}

			//
			template <class TLock, class Duration_t, class Predicate>
			WaitResult WaitFor( Thread& currentThread, TLock& lock, const Duration_t& duration, Predicate predicate )
			{
				auto waitPredicate = [&predicate]( ThreadWaitSyncState& syncContext ) -> bool {
					return ( predicate() && syncContext.IsNotifyFlagSet() ) || syncContext.IsInterruptFlagSet();
				};

				return this->_WaitFor( &currentThread, lock, duration, waitPredicate );
			}

			TMutex& GetMutex() const
			{
				return this->_mutex;
			}

		private:
			///
			virtual bool OnWaitBegin( ThreadWaitSyncState& threadWaitSyncState ) override final
			{
				return this->TImplType::OnWaitBeginImpl( threadWaitSyncState );
			}

			///
			virtual void OnWaitEnd( ThreadWaitSyncState& threadWaitSyncState ) override final
			{
				this->TImplType::OnWaitEndImpl( threadWaitSyncState );
			}

			///
			template <class TLock, class TPredicate>
			WaitResult _Wait( Thread* currentThread, TLock& lock, const TPredicate& waitPredicate )
			{
				auto waitResult = WaitResult::Failure;

				ThreadWaitSyncState threadWaitSyncState;
				threadWaitSyncState.thread = currentThread;

				if ( this->BeginWait( threadWaitSyncState ) )
				{
					try
					{
						while ( !waitPredicate( threadWaitSyncState ) )
						{
							ThreadSyncConditionVariableProxy<TLock>::Wait( *( threadWaitSyncState.threadSharedSyncContext ), lock );
						}
					}
					catch ( ... )
					{
						ExsDebugInterrupt();
					}

					this->EndWait( threadWaitSyncState );

					waitResult = this->GetWaitResult( threadWaitSyncState );
				}

				return waitResult;
			}

			///
			template <class TLock, class TDuration, class TPredicate>
			WaitResult _WaitFor( Thread* currentThread, TLock& lock, const TDuration& duration, const TPredicate& waitPredicate )
			{
				auto waitResult = WaitResult::Failure;

				ThreadWaitSyncState threadWaitSyncState;
				threadWaitSyncState.thread = currentThread;

				if ( this->BeginWait( threadWaitSyncState ) )
				{
					bool waitTimeout = false;

					try
					{
						while ( !waitPredicate( threadWaitSyncState ) && !waitTimeout )
						{
							waitTimeout = !ThreadSyncConditionVariableProxy<TLock>::WaitFor( *( threadWaitSyncState.threadSharedSyncContext ), lock, duration );
						}
					}
					catch ( ... )
					{
						ExsDebugInterrupt();
					}

					this->EndWait( threadWaitSyncState );

					waitResult = this->GetWaitResult( threadWaitSyncState, waitTimeout );
				}

				return waitResult;
			}

		private:
			//
			mutable TMutex _mutex;
		};


	}
}


#endif /* __Exs_Core_SyncObjectImplProxy_H__ */

