
#ifndef __Exs_Core_ThreadLocalStorage_H__
#define __Exs_Core_ThreadLocalStorage_H__

#include "ConcrtCommon.h"


namespace Exs
{
	namespace Concrt
	{


		class ThreadMessageController;


		///<summary>
		///</summary>
		struct ThreadLocalStorage
		{
		public:
			//
			struct alignas( 64 ) ControlBlock
			{
				/// Validation key, set after successful initialization of a TLS data.
				Uint32 validationKey;

				/// Represents number of TLS fetches BEFORE initialization.
				Uint32 uncheckedFetchCount;
			};

			//
			struct alignas( 64 ) InternalState
			{
				/// UID of a thread which owns this TLS.
				ThreadUID threadUID;

				/// Pointer to a thread which owns this TLS.
				Thread* thread;
			};

		public:
			//
			ControlBlock controlBlock;

			//
			InternalState internalState;

			//
			ThreadMessageController* messageController;
		};


	}
}


#endif /* __Exs_Core_ThreadLocalStorage_H__ */
