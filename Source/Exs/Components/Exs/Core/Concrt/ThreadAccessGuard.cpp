
#include "Precomp.h"
#include <Exs/Core/Concrt/Thread.h>
#include <Exs/Core/Concrt/ThreadAccessGuard.h>
#include <ExsLib/CoreUtils/Exception.h>


namespace Exs
{
	namespace Concrt
	{


		ThreadAccessGuard::ThreadAccessGuard( const char* funcName )
			: _funcName( funcName )
		{ }


		void ThreadAccessGuard::Enter()
		{
			auto currentThreadRefID = CurrentThread::GetRefID();

			if ( !this->IsThreadAllowed( currentThreadRefID ) )
			{
				this->_RaiseError();
			}
		}


		bool ThreadAccessGuard::TryEnter()
		{
			auto currentThreadRefID = CurrentThread::GetRefID();
			return this->IsThreadAllowed( currentThreadRefID );
		}


		void ThreadAccessGuard::_RaiseError()
		{
			ExsThrowExceptionEx( EXC_Unauthorized_Thread_Access, this->_funcName );
		}


	}
}
