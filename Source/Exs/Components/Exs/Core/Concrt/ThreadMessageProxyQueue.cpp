
#include "Precomp.h"
#include <Exs/Core/Concrt/ThreadMessageProxyQueue.h>
#include <Exs/Core/Concrt/ThreadMessageTransceiver.h>


namespace Exs
{
	namespace Concrt
	{


		ThreadMessageProxyQueue::ThreadMessageProxyQueue( ThreadMessageTransceiver* transceiver, U32ID queueID )
			: _transceiver( transceiver )
			, _id( queueID )
		{ }


		ThreadMessageProxyQueue::~ThreadMessageProxyQueue()
		{ }


		void ThreadMessageProxyQueue::Release()
		{
			this->_transceiver->ReleaseProxyQueue( this );
		}


		CoreMessageHandle ThreadMessageProxyQueue::PeekMessage()
		{
			return this->_transceiver->PeekMessage( this );
		}


		CoreMessageHandle ThreadMessageProxyQueue::WaitForMessage()
		{
			return this->_transceiver->WaitForMessage( this );
		}


		CoreMessageHandle ThreadMessageProxyQueue::WaitForMessage( const Milliseconds& timeout )
		{
			return this->_transceiver->WaitForMessage( timeout, this );
		}


		bool ThreadMessageProxyQueue::AddFilter( MessageFilter messageFilter, bool overwrite )
		{
			return this->_transceiver->AddThreadMessageFilter( messageFilter, this, overwrite );
		}


		void ThreadMessageProxyQueue::RemoveFilter( MessageFilter messageFilter )
		{
			this->_transceiver->RemoveThreadMessageFilter( messageFilter, this );
		}


		bool ThreadMessageProxyQueue::IsActive() const
		{
			return this->_transceiver->IsProxyQueueActive( this->_id );
		}


	}
}
