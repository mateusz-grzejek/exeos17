
#include "Precomp.h"
#include <Exs/Core/Concrt/CoreMessageSystemRoot.h>
#include <Exs/Core/Concrt/ConcrtSharedState.h>
#include <Exs/Core/Concrt/ThreadSystemManager.h>


namespace Exs
{
	namespace Concrt
	{


		CoreMessageSystemRoot::CoreMessageSystemRoot()
			: _dispatcher( this )
		{ }


		Result CoreMessageSystemRoot::RegisterMessageReceiver( CoreMessageReceiverID receiverID, Uint32 receiverIndex, CoreMessageReceiver& receiver )
		{
			return this->_dispatcher.RegisterReceiver( receiverID, receiverIndex, receiver );
		}


		void CoreMessageSystemRoot::UnregisterMessageReceiver( CoreMessageReceiverID receiverID, CoreMessageReceiver* receiver )
		{
			this->_dispatcher.UnregisterReceiver( receiverID, receiver );
		}


		Result CoreMessageSystemRoot::DispatchMessage( const CoreMessageSendRequest& sendRequest, const CoreMessageHandle& message )
		{
			CoreMessageDispatchRequest dispatchRequest;
			dispatchRequest.receiverID = sendRequest.receiverID;
			dispatchRequest.sendOptions = sendRequest.sendOptions;

			return this->_dispatcher.DispatchMessage( dispatchRequest, message );
		}


	}
}
