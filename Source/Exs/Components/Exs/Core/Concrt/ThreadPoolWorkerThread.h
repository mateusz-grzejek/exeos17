
#ifndef __Exs_Core_ThreadPoolWorkerThread_H__
#define __Exs_Core_ThreadPoolWorkerThread_H__

#include <Concrt/WorkerThread.h>
#include "ThreadPoolCommon.h"


namespace Exs
{
	namespace Concrt
	{


		enum : Result_code_value_t
		{
			RSC_Thrp_Worker_Process_Result_No_Input
		};


		class ThreadPoolWorkerThread : public WorkerThread
		{
			EXS_DECLARE_NONCOPYABLE( ThreadPoolWorkerThread );

		public:
			ThreadPoolWorkerThread( EXS_WORKER_THREAD_CTOR_PARAMS_DECL, ThreadPool& threadPool );

			virtual ~ThreadPoolWorkerThread() = default;

		private:
			//
			virtual bool OnInit( const ThreadInitContext& initContext ) override final;

			//
			virtual void OnRelease( const ThreadReleaseContext& releaseContext ) noexcept override final;

			//
			virtual Result Entry() override final;

			//
			ThreadPoolProcessResult ProcessNextItem();

			//
			void ExecuteCallback( ThreadPoolAsyncCallback callback );

			//
			ThreadUpdateResult Update( ThreadPoolProcessResult processResult );

		private:
			//
			ThreadPool* _threadPool;
		};


	}
}


#endif /* __Exs_Core_ThreadPoolWorkerThread_H__ */
