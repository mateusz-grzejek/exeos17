
#ifndef __Exs_Core_SyncObject_H__
#define __Exs_Core_SyncObject_H__

#include "SyncInternal.h"


namespace Exs
{
	namespace Concrt
	{


		/**
		 * @brief Base class for SyncObjects. Contains common logic, shared by all types of SO. Provides also internal API
		 * for interaction with thread objects and few helper methods.
		 */
		class EXS_LIBCLASS_CORE SyncObject
		{
		public:
			/**
			 * @brief Constructor. Initializes threadSystemManager using specified reference.
			 * @param threadSystemManager
			 */
			explicit SyncObject( ThreadSystemManager& threadSystemManager )
			: _threadSystemManager( &threadSystemManager )
			{ }

			/**
			 * @brief Virtual destructor (default).
			 */
			virtual ~SyncObject() = default;

			/**
			 *
			 * @return
			 */
			virtual Size_t Abort() = 0;

			/**
			 *
			 * @param thread
			 * @return
			 */
			virtual bool Interrupt( Thread& thread ) = 0;

		protected:

			///
			bool BeginWait( ThreadWaitSyncState& threadWaitSyncState );

			///
			void EndWait( ThreadWaitSyncState& threadWaitSyncState );

			static WaitResult GetWaitResult( ThreadWaitSyncState& threadWaitSyncState, bool waitTimeout = false );

		private:
			///
			virtual bool OnWaitBegin( ThreadWaitSyncState& threadWaitSyncState ) = 0;

			///
			virtual void OnWaitEnd( ThreadWaitSyncState& threadWaitSyncState ) = 0;

			///
			void _UpdateThreadState( ThreadWaitSyncState& threadWaitSyncState );

			///
			void _ResetThreadState( ThreadWaitSyncState& threadWaitSyncState );

		private:
			//
			ThreadSystemManager* _threadSystemManager;
		};



	}
}


#endif /* __Exs_Core_SyncObject_H__ */
