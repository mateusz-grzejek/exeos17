
#include "Precomp.h"
#include <Exs/Core/Concrt/CoreMessageSender.h>
#include <Exs/Core/Concrt/CoreMessageSystemRoot.h>
#include <Exs/Core/Concrt/CoreMessage.h>
#include <ExsLib/CoreUtils/Exception.h>


namespace Exs
{
	namespace Concrt
	{


		CoreMessageSender::CoreMessageSender( CoreMessageSystemRoot& messageSystemRoot, Thread& thread, CoreMessageSenderID id )
		: _messageSystemRoot( &messageSystemRoot )
		, _thread( &thread )
		, _id( id )
		{
			// this->_stateController.AddTransition( { SenderState::Idle, SenderState::Send } );
			//
			// this->_stateController.AddTransition( { SenderState::Idle, SenderState::Send_Sync_Active } );
			//
			// this->_stateController.AddTransition( { SenderState::Send, SenderState::Idle } );
			//
			// this->_stateController.AddTransition( { SenderState::Send_Sync_Active, SenderState::Send_Sync_Inactive } );
			//
			// this->_stateController.AddTransition( { SenderState::Send_Sync_Active, SenderState::Send_Sync_Wait } );
			//
			// this->_stateController.AddTransition( { SenderState::Send_Sync_Inactive, SenderState::Send_Sync_Wait } );
			//
			// this->_stateController.AddTransition( { SenderState::Send_Sync_Inactive, SenderState::Idle } );
		}


		CoreMessageSendResult CoreMessageSender::SendMessage( const CoreMessageSendRequest& sendRequest, const CoreMessageHandle& message )
		{
			this->_ResetSyncMessageState();

			return this->_DispatchMessage( sendRequest, message );
		}


		CoreSyncMessageSendResult CoreMessageSender::SendMessage( const CoreSyncMessageSendRequest& syncSendRequest, const CoreMessageHandle& message )
		{
			this->_SetSyncMessageState( *message, syncSendRequest );

			CoreMessageSendRequest internalSendRequest = syncSendRequest;

			if ( !syncSendRequest.sendOptions.is_set( CoreMessageSendOption_Sync_Wait_For_Inactive_Recipient ) )
			{
				// If sender does not want to wait for unregistered/inactive receiver, we should discard
				// the message in such case. To achieve this, we add Immediate flag, which will cause
				//
				internalSendRequest.sendOptions.set( CoreMessageSendOption_Immediate );
			}

			auto dispatchResult = this->_DispatchMessage( internalSendRequest, message );

			if ( dispatchResult == RSC_Msg_Message_Discarded )
			{
				this->_ResetSyncMessageState();

				return dispatchResult;
			}

			if ( syncSendRequest.sendOptions.is_set( CoreMessageSendOption_Sync_Wait_Implicit ) )
			{
				this->WaitForResponse( message.get() );
			}

			return ExsResult( RSC_Success );
		}


		CoreMessageResponseResult CoreMessageSender::WaitForResponse( CoreMessage* message )
		{
			if ( !this->_CheckCurrentSyncMessage( message ) )
			{
				if ( message == nullptr )
				{
					return ExsResult( RSC_Msg_Err_No_Sync_Message );
				}
				else
				{
					return ExsResult( RSC_Msg_Err_Invalid_Message );
				}
			}

			SyncMutexLock syncLock{ this->_syncMessageContext.syncObject.GetMutex() };
			{
				// Do the actual wait for the response.
				if ( !this->_WaitForResponse( syncLock ) )
				{
					return ExsResult( RSC_Msg_Response_Wait_Timeout );
				}
			}

			return CoreMessageResponseResult( ExsResult( RSC_Success ), this->_syncMessageContext.response.get() );
		}


		CoreMessageResponseResult CoreMessageSender::GetMessageResponse( CoreMessage* message )
		{
			if ( !this->_CheckCurrentSyncMessage( message ) )
			{
				if ( message == nullptr )
				{
					return ExsResult( RSC_Msg_Err_No_Sync_Message );
				}
				else
				{
					return ExsResult( RSC_Msg_Err_Invalid_Message );
				}
			}

			if ( !this->_IsResponseReady() )
			{
				return ExsResult( RSC_Msg_Response_Not_Ready );
			}

			auto* response = this->_syncMessageContext.response.get();

			return CoreMessageResponseResult( ExsResult( RSC_Success ), response );
		}


		CoreMessageResponse* CoreMessageSender::CreateMessageResponse( CoreMessage& message, Message_code_t responseCode )
		{
			CoreMessageResponse* result = nullptr;

			if ( this->_CheckCurrentSyncMessage( &message ) )
			{
				ExsBeginCriticalSection( this->_syncMessageContext.syncObject.GetMutex() );
				{
					if ( this->_CheckCurrentSyncMessage( &message ) )
					{
						this->_syncMessageContext.response = CreateCoreMessageResponse( message, responseCode );
						result = this->_syncMessageContext.response.get();
					}
				}
				ExsEndCriticalSection();
			}

			return result;
		}


		void CoreMessageSender::SetResponseReady( CoreMessage& message, CoreMessageResponse& response )
		{
			if ( !this->_CheckCurrentSyncMessage( &message ) )
			{
				return;
			}

			SyncMutexLock syncLock{ this->_syncMessageContext.syncObject.GetMutex() };
			{
				if ( this->_syncMessageContext.response.get() != &response )
				{
					ExsThrowException( EXC_Invalid_Operation );
				}

				this->_syncMessageContext.responseReady.store( true, std::memory_order_release );
				this->_syncMessageContext.syncObject.NotifyOne( syncLock );
			}
		}


		void CoreMessageSender::_SetSyncMessageState( CoreMessage& message, const CoreSyncMessageSendRequest& syncSendRequest )
		{
			this->_syncMessageContext.currentSyncMessage.store( &message, std::memory_order_relaxed );
			this->_syncMessageContext.responseReady.store( false, std::memory_order_relaxed );
			this->_syncMessageContext.response.reset();
			this->_syncMessageContext.waitTimeout = syncSendRequest.syncWaitTimeoutMs;
		}


		void CoreMessageSender::_ResetSyncMessageState()
		{
			this->_syncMessageContext.currentSyncMessage.store( nullptr, std::memory_order_release );
		}


		Result CoreMessageSender::_DispatchMessage( const CoreMessageSendRequest& sendRequest, const CoreMessageHandle& message )
		{
			auto dispatchResult = this->_messageSystemRoot->DispatchMessage( sendRequest, message );

			if ( dispatchResult != RSC_Msg_Message_Discarded )
			{
				message->UpdateSenderInfo( this, this->_id );
			}

			return dispatchResult;
		}


		bool CoreMessageSender::_WaitForResponse( SyncMutexLock& lock )
		{
			if ( !this->_IsResponseReady() )
			{
				this->_syncMessageContext.syncObject.WaitFor( *( this->_thread ), lock, this->_syncMessageContext.waitTimeout, [this]() -> bool {
					return this->_IsResponseReady();
				} );

				if ( !this->_IsResponseReady() && this->_IsResponseCreated() )
				{
					this->_syncMessageContext.syncObject.Wait( *( this->_thread ), lock, [this]() -> bool {
						return this->_IsResponseReady();
					} );
				}
			}

			return this->_IsResponseReady();
		}


	}
}
