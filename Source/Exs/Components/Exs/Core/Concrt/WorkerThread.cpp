
#include <Concrt/WorkerThread.h>
#include <Concrt/WorkerThreadContainer.h>
#include "Precomp.h"


namespace Exs
{
	namespace Concrt
	{


		WorkerThread::WorkerThread( EXS_THREAD_CTOR_PARAMS_DECL, WorkerThreadContainer& container, Worker_thread_index_t workerIndex )
		: Thread( EXS_THREAD_CTOR_PARAMS_DEF )
			, _container( &container )
			, _workerIndex( workerIndex )
		{
		}


		ThreadUpdateResult WorkerThread::Update()
		{
			auto updateResult = Thread::Update();

			if ( updateResult != ThreadUpdateResult::Continue )
			{
				return updateResult;
			}

			if ( this->_container->CheckExitRequest( *this ) )
			{
				return ThreadUpdateResult::Exit;
			}

			return ThreadUpdateResult::Continue;
		}


		bool WorkerThread::OnInit( const ThreadInitContext& initContext )
		{
			if ( !Thread::OnInit( initContext ) )
			{
				return false;
			}

			if ( !this->_container->RegisterThread( *this ) )
			{
				return false;
			}

			ExsTraceInfo( TRC_Core_Threading, "Worker thread [%s] (index: %u) has been successfuly initialized.", this->GetName().c_str(), this->GetWorkerIndex() );

			return true;
		}


		void WorkerThread::OnRelease( const ThreadReleaseContext& releaseContext ) noexcept
		{
			ExsTraceInfo( TRC_Core_Threading, "Worker thread [%s] (index: %u) is being released...", this->GetName().c_str(), this->GetWorkerIndex() );

			this->_container->UnregisterThread( *this );

			Thread::OnRelease( releaseContext );
		}


		void WorkerThread::SetWorkerStatusFlags( stdx::mask<Worker_thread_status_value_t> flags )
		{
			this->_container->SetWorkerStatusFlags( *this, flags );
		}


		void WorkerThread::UnsetWorkerStatusFlags( stdx::mask<Worker_thread_status_value_t> flags )
		{
			this->_container->UnsetWorkerStatusFlags( *this, flags );
		}


		bool WorkerThread::CheckWorkerStatus( stdx::mask<Worker_thread_status_value_t> flags ) const
		{
			return this->_container->CheckWorkerStatus( *this, flags );
		}


		bool WorkerThread::CheckWorkerStatusAny( stdx::mask<Worker_thread_status_value_t> flags ) const
		{
			return this->_container->CheckWorkerStatusAny( *this, flags );
		}


	}
}
