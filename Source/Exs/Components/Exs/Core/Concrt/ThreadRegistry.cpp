
#include "Precomp.h"
#include <Exs/Core/Concrt/ThreadRegistry.h>
#include <Exs/Core/Concrt/Thread.h>
#include <Exs/Core/Concrt/ThreadInternal.h>
#include <ExsLib/CoreUtils/Exception.h>


namespace Exs
{
	namespace Concrt
	{


		ThreadRegistry::ThreadRegistry( ConcrtSharedStateHandle concrtSharedState, ThreadSystemManager& threadSystemManger )
			: _concrtSharedState( concrtSharedState )
			, _threadSystemManager( &threadSystemManger )
			, _registeredThreadsNum{ ATOMIC_VAR_INIT( 0 ) }
		{ }


		ThreadRegistry::~ThreadRegistry()
		{ }


		ThreadUID ThreadRegistry::QueryThreadUID( Thread_ref_id_t threadRefID ) const
		{
			// Use explicit find to prevent creation of empty mappings to non-existing UIDs.
			// Altough it might be a possibility to consider, if ThreadUID objects were
			// initialized by default to Thread_UID_Invalid value.

			auto threadUIDRef = this->_refIDMap.find( threadRefID );

			if ( threadUIDRef != this->_refIDMap.end() )
			{
				// Return found UID. No additional checks are performed, because RefID --> UID
				// mapping is done during the registration phase, so here we can keep it simple.

				return threadUIDRef->value;
			}

			return Thread_UID_Invalid;
		}


		bool ThreadRegistry::IsThreadRegistered( ThreadUID threadUID ) const
		{
			// If this function turns to be used very intesively (altough it should not be),
			// an unordered_map<UID, {}> may be intruduced to increase search time. Keep it
			// simple now, though - querying registration state should not be done very often.

			auto threadInfoRef = std::find_if( this->_threadList.begin(), this->_threadList.end(), [threadUID]( const RegisteredThreadInfo& info ) -> bool {
				return info.uid == threadUID;
			} );

			return threadInfoRef != this->_threadList.end();
		}


		bool ThreadRegistry::IsThreadRegistered( Thread_ref_id_t threadRefID ) const
		{
			auto threadUID = this->QueryThreadUID( threadRefID );

			if ( threadUID != Thread_UID_Invalid )
			{
				// If UID is still in the map, check if thread is on the reg list.
				bool result = this->IsThreadRegistered( threadUID );

				// This should never fail - id ref ID is still in the map, thread should also be on the list.
				ExsDebugAssert( result );

				return result;
			}

			return false;
		}


		const ThreadRegistry::RegisteredThreadInfo& ThreadRegistry::GetThreadInfo( ThreadUID threadUID ) const
		{
			const auto* threadInfoPtr = this->_FindThreadInfo( threadUID );

			if ( threadInfoPtr == nullptr )
			{
				throw std::runtime_error( "" );
			}

			ExsDebugAssert( threadInfoPtr->uid == threadUID );
			return *threadInfoPtr;
		}


		const ThreadRegistry::RegisteredThreadInfo& ThreadRegistry::GetThreadInfo( Thread_ref_id_t threadRefID ) const
		{
			const auto* threadInfoPtr = this->_FindThreadInfo( threadRefID );

			if ( threadInfoPtr == nullptr )
			{
				throw std::runtime_error( "" );
			}

			ExsDebugAssert( threadInfoPtr->uid.GetRefID() == threadRefID );
			return *threadInfoPtr;
		}


		const std::string& ThreadRegistry::GetThreadName( ThreadUID threadUID ) const
		{
			const auto& threadInfo = this->GetThreadInfo( threadUID );
			return threadInfo.name;
		}


		const std::string& ThreadRegistry::GetThreadName( Thread_ref_id_t threadRefID ) const
		{
			const auto& threadInfo = this->GetThreadInfo( threadRefID );
			return threadInfo.name;
		}


		void ThreadRegistry::RegisterThread( Thread& thread, const ThreadSystemRegInfo& systemRegInfo )
		{
			auto threadUID = systemRegInfo.threadUID;
			auto threadRefID = threadUID.GetRefID();

			ExsBeginSharedCriticalSectionWithUniqueAccess( this->GetLock() );
			{
				if ( this->IsThreadRegistered( threadUID ) )
				{
					ExsThrowException( EXC_Invalid_Operation );
				}

				RegisteredThreadInfo threadInfo;
				threadInfo.threadObject = &thread;
				threadInfo.name = systemRegInfo.threadName;
				threadInfo.uid = threadUID;

				auto infoReference = this->_threadList.insert( this->_threadList.end(), std::move( threadInfo ) );
				infoReference->_reference = infoReference;

				this->_refIDMap.insert( threadRefID, threadUID );
				this->_registeredThreadsNum.fetch_add( 1, std::memory_order_relaxed );
			}
			ExsEndCriticalSection();
		}


		void ThreadRegistry::UnregisterThread( Thread& thread )
		{
			auto threadUID = thread.GetUID();
			auto threadRefID = threadUID.GetRefID();

			ExsBeginSharedCriticalSectionWithUniqueAccess( this->GetLock() );
			{
				auto threadInfoRef = std::find_if( this->_threadList.begin(), this->_threadList.end(), [threadUID]( const RegisteredThreadInfo& info ) -> bool {
					return info.uid == threadUID;
				} );

				if ( threadInfoRef == this->_threadList.end() )
				{
					ExsThrowException( EXC_Invalid_Operation );
				}

				if ( threadInfoRef->threadObject != &thread )
				{
					ExsThrowException( EXC_Invalid_State );
				}

				auto threadUIDRef = this->_refIDMap.find( threadRefID );

				if ( threadUIDRef == this->_refIDMap.end() )
				{
					ExsThrowException( EXC_Invalid_State );
				}

				this->_threadList.erase( threadInfoRef );
				this->_refIDMap.erase( threadUIDRef );
				this->_registeredThreadsNum.fetch_sub( 1, std::memory_order_relaxed );
			}
			ExsEndCriticalSection();
		}


		const ThreadRegistry::RegisteredThreadInfo* ThreadRegistry::_FindThreadInfo( ThreadUID threadUID ) const
		{
			auto threadInfoRef = std::find_if( this->_threadList.begin(), this->_threadList.end(), [threadUID]( const RegisteredThreadInfo& info ) -> bool {
				return info.uid == threadUID;
			} );

			return ( threadInfoRef != this->_threadList.end() ) ? &( *threadInfoRef ) : nullptr;
		}


		const ThreadRegistry::RegisteredThreadInfo* ThreadRegistry::_FindThreadInfo( Thread_ref_id_t threadRefID ) const
		{
			// NOTE: Do not query thread UID and forward this call to _FindThreadInfo(UID)! This internal method
			// is also used to validate the consistency of the registry, so refID --> UID map should not be used.

			auto threadInfoRef = std::find_if( this->_threadList.begin(), this->_threadList.end(), [threadRefID]( const RegisteredThreadInfo& info ) -> bool {
				return info.uid.GetRefID() == threadRefID;
			} );

			return ( threadInfoRef != this->_threadList.end() ) ? &( *threadInfoRef ) : nullptr;
		}


	}
}
