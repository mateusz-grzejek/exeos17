
#ifndef __Exs_Core_SyncCommon_H__
#define __Exs_Core_SyncCommon_H__

#include "ConcrtCommon.h"


namespace Exs
{
	namespace Concrt
	{


		class SyncObject;


		/**
		 * @brief Default mutex used in sync primitives. Defaults to SystemMutex (std::mutex).
		 */
		using SyncMutex = SystemMutex;

		/**
		 * @brief Default lock for mutex used in sync primitives. Defaults to SystemMutexLock (std::unique_lock<std::mutex>).
		 */
		using SyncMutexLock = SystemMutexLock;


		/**
		 * @brief Represents result codes which may be returned by Wait()/WaitFor() methods of sync objects.
		 */
		enum class WaitResult : Enum
		{
			// Wait could not be performed due to internal error.
			Failure,

			// Object has been signaled using Abort() or Interrupt() method. If a control predicate was passed,
			// it is unspecified what value will be returned by this predicate after wait exits with this code.
			Interrupt,

			// Object has been signaled with NotifyXXX() method. If a control predicate was passed, it is
			// guaranteed, that after wait exits with this code, calling the predicate will return true.
			Signaled,

			// A timeout has occured. Only valid for timed waits.
			Timeout
		};


	}
}


#endif /* __Exs_Core_SyncCommon_H__ */
