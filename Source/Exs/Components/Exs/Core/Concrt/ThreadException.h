
#ifndef __Exs_Core_ThreadException_H__
#define __Exs_Core_ThreadException_H__

#include "ThreadCommon.h"
#include "ExsLib/CoreUtils/Exception.h"


namespace Exs
{
	namespace Concrt
	{


		enum : Enum
		{
			// Reuqest for thread interruption (exit command).
			EXC_Thread_Exit = EnumDef::DeclareExceptionCode( ExceptionType::Interrupt, 0x1000 ),

			// Reuqest for thread interruption (termination command).
			EXC_Thread_Terminate,

			//
			EXC_Thread_Initialization_Error = EnumDef::DeclareExceptionCode( ExceptionType::Runtime, 0x1000 ),

			EXC_Thread_Unauthorized_Access
		};


		class ThreadException : public RuntimeException
		{
		public:
			ThreadException( const ExceptionDesc& exceptionDesc, Thread& originThread ) noexcept
				: RuntimeException( exceptionDesc )
				, _originThread( &originThread )
			{ }

			Thread& GetOriginThread() const
			{
				return *( this->_originThread );
			}

		private:
			Thread* _originThread; // UID of thread which thrown the exception.
		};


		///<summary>
		///</summary>
		class ThreadInitializationErrorException : public ThreadException
		{
		public:
			ThreadInitializationErrorException( const ExceptionDesc& exceptionDesc, Thread& originThread ) noexcept
				: ThreadException( exceptionDesc, originThread )
			{ }
		};


		///<summary>
		///</summary>
		class ThreadInterruptException : public ThreadException
		{
		public:
			ThreadInterruptException( const ExceptionDesc& exceptionDesc, Thread& originThread, Enum interruptCode ) noexcept
				: ThreadException( exceptionDesc, originThread )
				, _interruptCode( interruptCode )
			{ }

			Enum GetInterruptCode() const
			{
				return this->_interruptCode;
			}

		private:
			Enum  _interruptCode;
		};


		///<summary>
		///</summary>
		class ThreadUnauthorizedAccessException : public ThreadException
		{
		public:
			ThreadUnauthorizedAccessException( const ExceptionDesc& exceptionDesc, Thread& originThread, Thread* threadObject ) noexcept
				: ThreadException( exceptionDesc, originThread )
				, _threadObject( threadObject )
			{ }

		private:
			Thread* _threadObject;
		};


	#define ExsThrowThreadException( exceptionCode, ... ) \
		ExsThrowException( exceptionCode, CurrentThread::GetThreadObject(), ##__VA_ARGS__ )

	#define ExsThrowThreadInterruptException( exceptionCode, ... ) \
		ExsThrowThreadException( exceptionCode, 0, ##__VA_ARGS__ )

	#define ExsThrowThreadInterruptExceptionEx( exceptionCode, interruptCode, ... ) \
		ExsThrowThreadException( exceptionCode, interruptCode, ##__VA_ARGS__ )

	#define ExsThrowThreadUnauthorizedAccessException( threadObject, ... ) \
		ExsThrowThreadException( EXC_Thread_Unauthorized_Access, threadObject, ##__VA_ARGS__ )


	}


	EXS_EXCEPTION_SET_CODE_TYPE( Concrt::EXC_Thread_Exit, Concrt::ThreadInterruptException );
	EXS_EXCEPTION_SET_CODE_TYPE( Concrt::EXC_Thread_Terminate, Concrt::ThreadInterruptException );
	EXS_EXCEPTION_SET_CODE_TYPE( Concrt::EXC_Thread_Initialization_Error, Concrt::ThreadInitializationErrorException );
	EXS_EXCEPTION_SET_CODE_TYPE( Concrt::EXC_Thread_Unauthorized_Access, Concrt::ThreadUnauthorizedAccessException );


}


#endif /* __Exs_Core_ThreadException_H__ */
