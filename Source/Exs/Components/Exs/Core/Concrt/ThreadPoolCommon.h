
#ifndef __Exs_Core_ThreadPoolCommon_H__
#define __Exs_Core_ThreadPoolCommon_H__

#include "AsyncWaitCallback.h"
#include "ThreadCommon.h"


namespace Exs
{
	namespace Concrt
	{


		class ThreadPool;
		class ThreadPoolSharedQueue;
		class ThreadPoolWorkerThread;
		class ThreadPoolWorkerThreadFactory;


		//
		using ThreadPoolAsyncCallback = AsyncWaitCallback<void()>;

		//
		using ThreadPoolAsyncResult = ThreadPoolAsyncCallback::AsyncResultType;

		//
		using ThreadPoolWorkerThreadHandle = ThreadObjectHandle<ThreadPoolWorkerThread>;

		//
		using ThreadPoolIdleWorkerList = std::list<ThreadPoolWorkerThread*>;

		//
		using ThreadPoolIdleWorkerRef = ThreadPoolIdleWorkerList::iterator;


		//
		enum class ThreadPoolProcessResult
		{
			Err_No_Input,

			Err_Unknown,

			No_Error
		};


		struct ThreadPoolWorkerState : public WorkerThreadState
		{
			ThreadPoolIdleWorkerRef idleRef;
		};


	}
}


#endif /* __Exs_Core_ThreadPoolCommon_H__ */
