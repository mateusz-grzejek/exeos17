
#include "Precomp.h"
#include <Exs/Core/Concrt/ConcrtSharedState.h>
#include <Exs/Core/Concrt/CoreMessageQueue.h>
#include <Exs/Core/Concrt/CoreMessageReceiver.h>
#include <Exs/Core/Concrt/CoreMessageSender.h>
#include <Exs/Core/Concrt/CoreMessageSystemRoot.h>
#include <Exs/Core/Concrt/ThreadAccessGuard.h>
#include <Exs/Core/Concrt/ThreadMessageController.h>
#include <Exs/Core/Concrt/ThreadMessageProxyQueue.h>
#include <Exs/Core/Concrt/ThreadMessageTransceiver.h>
#include <ExsLib/CoreUtils/Exception.h>


namespace Exs
{
	namespace Concrt
	{


		void ThreadMessageTransceiver::ThreadProxyQueueDeleter::operator()( ThreadMessageProxyQueue* queue )
		{
			delete queue;
		}


		std::atomic<U32ID> ThreadMessageTransceiver::_queueIDGen = ATOMIC_VAR_INIT( Thread_Proxy_Queue_Base_ID );


		ThreadMessageTransceiver::ThreadMessageTransceiver( CoreMessageSystemRoot& messageSystemRoot, Thread& thread, ThreadUID threadUID )
			: _messageSystemRoot( &messageSystemRoot )
			, _thread( &thread )
			, _threadUID( threadUID )
			, _controller( this )
		{ }


		ThreadMessageTransceiver::~ThreadMessageTransceiver()
		{ }


		Result ThreadMessageTransceiver::Initialize( bool initReceiver, bool initSender )
		{
			auto senderReceiverID = this->_threadUID.GetRefID();

			if ( initReceiver )
			{
				this->_receiver = std::make_unique<CoreMessageReceiver>( *( this->_messageSystemRoot ), *( this->_thread ), senderReceiverID );
			}

			if ( initSender )
			{
				this->_sender = std::make_unique<CoreMessageSender>( *( this->_messageSystemRoot ), *( this->_thread ), senderReceiverID );
			}

			return ExsResult( RSC_Success );
		}


		Result ThreadMessageTransceiver::Register()
		{
			if ( !this->_receiver )
			{
				// If owner thread has message receiving functionality disabled, there is
				// nothing to do here (sender is just a proxy and is not registered anywhere).
				return ExsResult( RSC_Success );
			}

			// Thread ref id is used as receiver ID. This gives us nice, unique IDs out of the box.
			auto receiverID = this->_threadUID.GetRefID();

			// Same for the index - we use the same index which was assigned to the thread by the thread manager.
			auto receiverIndex = this->_threadUID.GetIndex();

			return this->_messageSystemRoot->RegisterMessageReceiver( receiverID, receiverIndex, *( this->_receiver ) );
		}


		void ThreadMessageTransceiver::Unregister()
		{
			this->_messageSystemRoot->UnregisterMessageReceiver( this->_receiver->GetID(), this->_receiver.get() );
		}


		ThreadMessageProxyQueue* ThreadMessageTransceiver::CreateProxyQueue( U32ID queueID )
		{
			ExsThreadAccessControlDebugAllow( this->_threadUID.GetRefID() );

			if ( !this->_receiver )
			{
				return nullptr;
			}

			auto queueCheckResult = this->_CheckQueueCreateRequest( queueID );

			if ( queueCheckResult.first == nullptr )
			{
				ThreadMessageProxyQueuePtr queue{ new ThreadMessageProxyQueue( this, queueCheckResult.second ) };
				queueCheckResult.first = this->_AddQueue( queueCheckResult.second, std::move( queue ) );
			}

			return queueCheckResult.first;
		}


		ThreadMessageProxyQueue* ThreadMessageTransceiver::CreateProxyQueue( const ThreadProxyQueueCreateInfo& createInfo )
		{
			ExsThreadAccessControlDebugAllow( this->_threadUID.GetRefID() );

			if ( !this->_receiver )
			{
				return nullptr;
			}

			auto queueCheckResult = this->_CheckQueueCreateRequest( createInfo.queueID );

			if ( queueCheckResult.first == nullptr )
			{
				ThreadMessageProxyQueuePtr queue{ new ThreadMessageProxyQueue( this, queueCheckResult.second ) };
				queueCheckResult.first = this->_AddQueue( queueCheckResult.second, std::move( queue ) );
			}

			return queueCheckResult.first;
		}


		Result ThreadMessageTransceiver::SendMessage( const CoreMessageSendRequest& sendRequest, const CoreMessageHandle& message )
		{
			ExsThreadAccessControlDebugAllow( this->_threadUID.GetRefID() );

			if ( !this->_sender )
			{
				ExsThrowException( EXC_Invalid_Operation );
			}

			return this->_sender->SendMessage( sendRequest, message );
		}


		CoreSyncMessageSendResult ThreadMessageTransceiver::SendMessage( const CoreSyncMessageSendRequest& sendRequest, const CoreMessageHandle& message )
		{
			ExsThreadAccessControlDebugAllow( this->_threadUID.GetRefID() );

			if ( !this->_sender )
			{
				ExsThrowException( EXC_Invalid_Operation );
			}

			return this->_sender->SendMessage( sendRequest, message );
		}


		CoreMessageResponse* ThreadMessageTransceiver::WaitForResponse()
		{
			ExsThreadAccessControlDebugAllow( this->_threadUID.GetRefID() );

			if ( !this->_sender )
			{
				ExsThrowException( EXC_Invalid_Operation );
			}

			auto responseResult = this->_sender->WaitForResponse( nullptr );

			return responseResult.response;
		}


		CoreMessageResponse* ThreadMessageTransceiver::WaitForResponse( CoreMessage& message )
		{
			ExsThreadAccessControlDebugAllow( this->_threadUID.GetRefID() );

			if ( !this->_sender )
			{
				ExsThrowException( EXC_Invalid_Operation );
			}

			auto responseResult = this->_sender->WaitForResponse( &message );

			return responseResult.response;
		}


		CoreMessageResponse* ThreadMessageTransceiver::GetResponse()
		{
			ExsThreadAccessControlDebugAllow( this->_threadUID.GetRefID() );

			if ( !this->_sender )
			{
				ExsThrowException( EXC_Invalid_Operation );
			}

			auto responseResult = this->_sender->GetMessageResponse( nullptr );

			return responseResult.response;
		}


		CoreMessageResponse* ThreadMessageTransceiver::GetResponse( CoreMessage& message )
		{
			ExsThreadAccessControlDebugAllow( this->_threadUID.GetRefID() );

			if ( !this->_sender )
			{
				ExsThrowException( EXC_Invalid_Operation );
			}

			auto responseResult = this->_sender->GetMessageResponse( &message );

			return responseResult.response;
		}


		CoreMessageHandle ThreadMessageTransceiver::PeekMessage( ThreadMessageProxyQueue* sourceQueue )
		{
			ExsThreadAccessControlDebugAllow( this->_threadUID.GetRefID() );

			if ( !this->_receiver )
			{
				ExsThrowException( EXC_Invalid_Operation );
			}

			auto* sourceMessageQueue = ( sourceQueue != nullptr ) ? sourceQueue->GetMessageQueue() : nullptr;

			return this->_receiver->PeekMessage( sourceMessageQueue );
		}


		CoreMessageHandle ThreadMessageTransceiver::WaitForMessage( ThreadMessageProxyQueue* sourceQueue )
		{
			ExsThreadAccessControlDebugAllow( this->_threadUID.GetRefID() );

			if ( !this->_receiver )
			{
				ExsThrowException( EXC_Invalid_Operation );
			}

			auto* sourceMessageQueue = ( sourceQueue != nullptr ) ? sourceQueue->GetMessageQueue() : nullptr;

			return this->_receiver->WaitForMessage( sourceMessageQueue );
		}


		CoreMessageHandle ThreadMessageTransceiver::WaitForMessage( const Milliseconds& timeout, ThreadMessageProxyQueue* sourceQueue )
		{
			ExsThreadAccessControlDebugAllow( this->_threadUID.GetRefID() );

			if ( !this->_receiver )
			{
				ExsThrowException( EXC_Invalid_Operation );
			}

			auto* sourceMessageQueue = ( sourceQueue != nullptr ) ? sourceQueue->GetMessageQueue() : nullptr;

			return this->_receiver->WaitForMessage( timeout, sourceMessageQueue );
		}


		bool ThreadMessageTransceiver::AddThreadMessageFilter( MessageFilter messageFilter, ThreadMessageProxyQueue* proxyQueue, bool overwrite )
		{
			auto queueID = proxyQueue->GetID();
			auto queueInfoIter = this->_threadProxyQueues.find( queueID );

			if ( queueInfoIter != this->_threadProxyQueues.end() )
			{
				ExsThrowException( EXC_Invalid_Operation );
			}

			if ( !this->_receiver->AddMessageFilter( messageFilter, *( queueInfoIter->value.messageQueue ), overwrite ) )
			{
				return false;
			}

			queueInfoIter->value.registeredFilters.insert( messageFilter );

			return true;
		}


		void ThreadMessageTransceiver::RemoveThreadMessageFilter( MessageFilter messageFilter, ThreadMessageProxyQueue* proxyQueue )
		{
			auto queueID = proxyQueue->GetID();
			auto queueInfoIter = this->_threadProxyQueues.find( queueID );

			if ( queueInfoIter == this->_threadProxyQueues.end() )
			{
				ExsThrowException( EXC_Invalid_Operation );
			}

			auto& queueFiltersList = queueInfoIter->value.registeredFilters;
			auto filterIter = queueFiltersList.find( messageFilter );

			if ( filterIter == queueFiltersList.end() )
			{
				ExsThrowException( EXC_Invalid_Operation );
			}

			this->_receiver->RemoveMessageFilter( messageFilter, queueInfoIter->value.messageQueue );

			queueFiltersList.erase( filterIter );
		}


		void ThreadMessageTransceiver::ReleaseProxyQueue( ThreadMessageProxyQueue* queue )
		{
			auto queueID = queue->GetID();
			auto queueInfoIter = this->_threadProxyQueues.find( queueID );

			if ( queueInfoIter == this->_threadProxyQueues.end() )
			{
				ExsThrowException( EXC_Invalid_Operation );
			}

			auto& queueInfo = queueInfoIter->value;

			for ( auto& refID : queueInfo.registeredFilters )
			{
				this->_receiver->RemoveMessageFilter( refID, queueInfoIter->value.messageQueue );
			}

			this->_receiver->UnregisterExternalQueue( *( queueInfo.messageQueue ) );
			this->_threadProxyQueues.erase( queueInfoIter );
		}


		ThreadMessageProxyQueue* ThreadMessageTransceiver::_AddQueue( U32ID queueID, ThreadMessageProxyQueuePtr&& queue )
		{
			ThreadProxyQueueInfo proxyQueueInfo;
			proxyQueueInfo.queueID = queueID;
			proxyQueueInfo.queue = std::move( queue );
			proxyQueueInfo.messageQueue = proxyQueueInfo.queue->GetMessageQueue();

			auto queueInfoIter = this->_threadProxyQueues.insert( queueID, std::move( proxyQueueInfo ) );
			this->_receiver->RegisterExternalQueue( *( proxyQueueInfo.messageQueue ) );

			return queueInfoIter->value.queue.get();
		}


		ThreadMessageProxyQueue* ThreadMessageTransceiver::_GetQueue( U32ID queueID )
		{
			auto queueInfoIter = this->_threadProxyQueues.find( queueID );
			return ( queueInfoIter != this->_threadProxyQueues.end() ) ? queueInfoIter->value.queue.get() : nullptr;
		}


		std::pair<ThreadMessageProxyQueue*, U32ID> ThreadMessageTransceiver::_CheckQueueCreateRequest( U32ID queueID )
		{
			// By default, queue pointer is nullptr and ID is invalid.
			std::pair<ThreadMessageProxyQueue*, U32ID> result{ nullptr, ID32_Invalid };

			if ( ( queueID != 0 ) && ( queueID != ID32_Auto ) )
			{
				// If queue ID has been specified manually (no automatic generation), search for queue with this ID.
				// This check is not required for auto IDs, since they are globally unique (even across different threads).
				result.first = this->_GetQueue( queueID );
			}
			else
			{
				// If ID is auto, leave queue ptr set to NULL, because new ID is being generated. Use static counter for that.
				result.second = _queueIDGen.fetch_add( 1, std::memory_order_relaxed );
			}

			return result;
		}


	}
}
