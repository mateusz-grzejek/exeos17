
#ifndef __Exs_Core_ThreadRegistry_H__
#define __Exs_Core_ThreadRegistry_H__

#include "ThreadCommon.h"
#include <stdx/assoc_array.h>


namespace Exs
{
	namespace Concrt
	{


		class ThreadRegistry : public Lockable<LightSharedMutex, LockAccess::Relaxed>
		{
			friend class ThreadSystemManager;

		public:
			struct RegisteredThreadInfo
			{
				std::string name;
				Thread* threadObject;
				ThreadUID uid;

				// For internal use only!
				std::list<RegisteredThreadInfo>::iterator _reference;
			};

			// List of threads in the registry.
			using ThreadList = std::list<RegisteredThreadInfo>;

			// Map used for mapping ref IDs to UIDs.
			using ThreadRefIDMap = stdx::assoc_array<Thread_ref_id_t, ThreadUID>;

		public:
			ThreadRegistry( ConcrtSharedStateHandle concrtSharedState, ThreadSystemManager& threadSystemManger );
			~ThreadRegistry();

			ThreadUID QueryThreadUID( Thread_ref_id_t threadRefID ) const;

			bool IsThreadRegistered( ThreadUID threadUID ) const;
			bool IsThreadRegistered( Thread_ref_id_t threadRefID ) const;

			const RegisteredThreadInfo& GetThreadInfo( ThreadUID threadUID ) const;
			const RegisteredThreadInfo& GetThreadInfo( Thread_ref_id_t threadRefID ) const;

			const std::string& GetThreadName( ThreadUID threadUID ) const;
			const std::string& GetThreadName( Thread_ref_id_t threadRefID ) const;

			const ThreadList& GetThreadList() const;

		friendapi:
			//
			void RegisterThread( Thread& thread, const ThreadSystemRegInfo& systemRegInfo );

			//
			void UnregisterThread( Thread& thread );

		private:
			const RegisteredThreadInfo* _FindThreadInfo( ThreadUID threadUID ) const;
			const RegisteredThreadInfo* _FindThreadInfo( Thread_ref_id_t threadRefID ) const;

		private:
			//
			ConcrtSharedStateHandle _concrtSharedState;

			// Pointer to the control manager.
			ThreadSystemManager* _threadSystemManager;

			// List of registered threads.
			ThreadList _threadList;

			// RefID --> UID map. Allows ref IDs to be used in external API.
			ThreadRefIDMap _refIDMap;

			// Number of registsred threads.
			std::atomic<Size_t> _registeredThreadsNum;
		};


		inline const ThreadRegistry::ThreadList& ThreadRegistry::GetThreadList() const
		{
			return this->_threadList;
		}


	}
}


#endif /* __Exs_Core_ThreadRegistry_H__ */
