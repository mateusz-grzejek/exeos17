
#include "Precomp.h"
#include <Exs/Core/Concrt/ThreadPoolWorkerThread.h>
#include <Exs/Core/Concrt/ThreadPool.h>


namespace Exs
{
	namespace Concrt
	{


		ThreadPoolWorkerThread::ThreadPoolWorkerThread( EXS_WORKER_THREAD_CTOR_PARAMS_DECL, ThreadPool& threadPool )
		: WorkerThread( EXS_WORKER_THREAD_CTOR_PARAMS_DEF )
			, _threadPool( &threadPool )
		{
		}


		bool ThreadPoolWorkerThread::OnInit( const ThreadInitContext& initContext )
		{
			if ( !WorkerThread::OnInit( initContext ) )
			{
				return false;
			}

			if ( !this->_threadPool->RegisterWorkerThread( *this ) )
			{
				return false;
			}

			return true;
		}


		void ThreadPoolWorkerThread::OnRelease( const ThreadReleaseContext& releaseContext ) noexcept
		{
			this->_threadPool->UnregisterWorkerThread( *this );

			WorkerThread::OnRelease( releaseContext );
		}


		Result ThreadPoolWorkerThread::Entry()
		{
			auto processResult = ThreadPoolProcessResult::No_Error;

			//
			while( this->Update( processResult ) != ThreadUpdateResult::Exit )
			{
				//
				processResult = this->ProcessNextItem();
			}

			return ExsResult( RSC_Success );
		}


		ThreadPoolProcessResult ThreadPoolWorkerThread::ProcessNextItem()
		{
			auto processResult = ThreadPoolProcessResult::Err_Unknown;

			ExsTraceInfo( TRC_Core_Threading, "Worker thread %u waits for item...", this->GetWorkerIndex() );

			if ( auto callback = this->_threadPool->WaitForCallback( *this ) )
			{
				ExsTraceInfo( TRC_Core_Threading, "Fetched item", this->GetWorkerIndex() );

				this->ExecuteCallback( callback );

				processResult = ThreadPoolProcessResult::No_Error;
			}
			else
			{
				processResult = ThreadPoolProcessResult::Err_No_Input;
			}

			return processResult;
		}


		void ThreadPoolWorkerThread::ExecuteCallback( ThreadPoolAsyncCallback callback )
		{
			callback.Run();
		}


		ThreadUpdateResult ThreadPoolWorkerThread::Update( ThreadPoolProcessResult processResult )
		{
			if ( processResult == ThreadPoolProcessResult::Err_No_Input )
			{
				if ( !this->_threadPool->IsActive() )
				{
					return ThreadUpdateResult::Exit;
				}
			}

			return ThreadUpdateResult::Continue;
		}


	}
}
