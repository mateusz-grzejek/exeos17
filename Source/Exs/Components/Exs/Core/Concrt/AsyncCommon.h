
#ifndef __Exs_Core_AsyncCommon_H__
#define __Exs_Core_AsyncCommon_H__

#include "SharedSyncObject.h"
#include <stdx/intrusive_ptr.h>


namespace Exs
{
	namespace Concrt
	{


		///<summary>
		///
		///</summary>
		class AsyncExecutionContext
		{
		public:
			virtual ~AsyncExecutionContext()
			{
				if ( !this->IsReady() )
				{
					this->Wait();
				}
			}

			//
			bool SetReady( SyncMutexLock& syncMutexLock )
			{
				if ( this->IsReady() )
				{
					return false;
				}

				this->_state.set( SyncState_Ready );
				this->_syncObject.NotifyAll( syncMutexLock );

				return true;
			}

			//
			void Wait()
			{
				SyncMutexLock syncLock{ this->GetMutex() };
				{
					this->_syncObject.Wait( syncLock, [this]() -> bool {
						return this->IsReady();
					} );
				}
			}

			//
			template <class TDuration>
			void WaitFor( const TDuration& duration )
			{
				SyncMutexLock syncLock{ this->GetMutex() };
				{
					this->_syncObject.WaitFor( syncLock, duration, [this]() -> bool {
						return this->IsReady();
					} );
				}
			}

			//
			SyncMutex& GetMutex() const
			{
				return this->_syncObject.GetMutex();
			}

			//
			bool IsReady() const
			{
				return this->_state.is_set( SyncState_Ready, std::memory_order_relaxed );
			}

		private:
			enum SyncStateFlags : Enum
			{
				SyncState_Ready = 0x0001
			};

		private:
			//
			SharedSyncObject<> _syncObject;

			//
			stdx::atomic_mask<Uint32> _state;
		};


		class SharedAsyncExecutionContext : public AsyncExecutionContext
		{
		};


		class UniqueAsyncExecutionContext : public AsyncExecutionContext
		{
		};


		///<summary>
		///</summary>
		class AsyncCallState
		{
		public:
			AsyncCallState()
				: _context( nullptr )
			{ }

			explicit operator bool() const
			{
				return this->_context != nullptr;
			}

			//
			bool SetReady( SyncMutexLock& syncMutexLock )
			{
				ExsDebugAssert( this->_context != nullptr );
				return this->_context->SetReady( syncMutexLock );
			}

			//
			void Wait()
			{
				ExsDebugAssert( this->_context != nullptr );
				this->_context->Wait();
			}

			//
			template <class Duration>
			void WaitFor( const Duration& duration )
			{
				ExsDebugAssert( this->_context != nullptr );
				this->_context->WaitFor( duration );
			}

			//
			SyncMutex& GetMutex() const
			{
				ExsDebugAssert( this->_context != nullptr );
				return this->_context->GetMutex();
			}

			//
			bool IsReady() const
			{
				ExsDebugAssert( this->_context != nullptr );
				return this->_context->IsReady();
			}

		protected:
			void _CheckSetRequest()
			{
				if ( this->IsReady() )
				{
					throw 0;
				}
			}

			void _ClearContext()
			{
				this->_context = nullptr;
			}

			void _SetContext( AsyncExecutionContext& context )
			{
				this->_context = &context;
			}

		private:
			AsyncExecutionContext* _context;
		};


		///<summary>
		///</summary>
		class AsyncExceptionProxy : public AsyncCallState
		{
		public:
			AsyncExceptionProxy() = default;

			//
			void SaveCurrentException()
			{
				this->_CheckSetRequest();
				this->_exception = std::current_exception();
			}

			//
			void SetException( std::exception_ptr exception )
			{
				this->_CheckSetRequest();
				this->_exception = exception;
			}

			//
			bool HasException() const
			{
				return this->_exception ? true : false;
			}

		protected:
			//
			void _RethrowException()
			{
				std::rethrow_exception( this->_exception );
			}

		private:
			std::exception_ptr _exception;
		};


		namespace Internal
		{

			template <class R>
			struct AsyncSharedDataWrapper : public stdx::ref_counted_base<stdx::sync_ref_counter>
			{
				SharedAsyncExecutionContext context;

				R value;
			};

			template <>
			struct AsyncSharedDataWrapper<void> : public stdx::ref_counted_base<stdx::sync_ref_counter>
			{
				SharedAsyncExecutionContext context;
			};

		}


		///<summary>
		///</summary>
		class AsyncResult : public AsyncExceptionProxy
		{
		protected:
			using SharedData = Internal::AsyncSharedDataWrapper<void>;

		public:
			AsyncResult()
				: _sharedData( stdx::make_instrusive<SharedData>() )
			{
				this->_SetContext( this->_sharedData->context );
			}

			AsyncResult( std::nullptr_t )
			{ }

			AsyncResult( const UninitializedTag& )
			{ }

			void Release()
			{
				this->_sharedData.reset();
				this->_ClearContext();
			}

		protected:
			stdx::intrusive_ptr<SharedData> _sharedData;
		};


		///<summary>
		///</summary>
		template <class R>
		class AsyncValue : public AsyncExceptionProxy
		{
		private:
			using SharedData = Internal::AsyncSharedDataWrapper<R>;

		public:
			AsyncValue()
				: _sharedData( stdx::make_instrusive<SharedData>() )
			{
				this->_SetContext( this->_sharedData->context );
			}

			AsyncValue( std::nullptr_t )
			{ }

			AsyncValue( const UninitializedTag& tag )
			{ }

			void SetValue( R&& value )
			{
				this->_CheckSetRequest();
				this->_sharedData->value = std::move( value );
			}

			void SetValue( const R& value )
			{
				this->_CheckSetRequest();
				this->_sharedData->value = value;
			}

			const R& GetValue()
			{
				if ( !this->IsReady() )
				{
					this->Wait();
				}

				if ( this->HasException() )
				{
					this->_RethrowException();
				}

				return this->_sharedData->value;
			}

		private:
			stdx::intrusive_ptr<SharedData> _sharedData;
		};


		template <>
		class AsyncValue<void> : public AsyncResult
		{
		public:
			AsyncValue()
			{ }

			AsyncValue( std::nullptr_t )
				: AsyncResult( nullptr )
			{ }

			AsyncValue( const UninitializedTag& tag )
				: AsyncResult( tag )
			{ }

		};


	}
}


#endif /* __Exs_Core_AsyncCommon_H__ */
