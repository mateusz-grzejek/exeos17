
#ifndef __Exs_Core_TaskExecutionPool_H__
#define __Exs_Core_TaskExecutionPool_H__

#include <Concrt/WorkerThreadContainer.h>
#include "TaskCommon.h"
#include <stdx/sorted_array.h>


namespace Exs
{
	namespace Concrt
	{


		class EXS_LIBCLASS_CORE TaskExecutionPool
		{
		public:
			TaskExecutionPool();

			void SetElevatedPriorityParameters( Uint32 thresholdValuePercentage, Uint32 favorCountLimit );

			void SetHeuristicPriorityWeights( Uint32 priorityLevelWeight, Uint32 intensityClassWeight );

		private:
			Uint32 _ComputeHeuristicPriorityValue( Uint32 priorityValue, TaskIntensityClass intensityClass );

			bool _IsElevatedPriorityTask( Uint32 heuristicPriorityValue );

		private:
			struct HeuristicMetrics
			{
				//
				Uint32 elevatedPriorityThresholdValue;

				//
				Uint32 elevatedPriorityFavorCountLimit;

				//
				Uint32 intensityClassWeight;

				//
				Uint32 priorityLevelWeight;
			};

			// List of keys (indices within the pool) of active threads). Indices are from 0 to _threadsNum - 1.
			using ThreadStateIndex = stdx::sorted_array<stdx::concurrent_data_pool_key_t>;

		private:
			//
			HeuristicMetrics _hmetrics;

			//
			Uint32 _heuristicPriorityMaxValue;

			//
			ThreadStateIndex _threadStateIndex;
		};


	}
}


#endif /* __Exs_Core_TaskExecutionPool_H__ */
