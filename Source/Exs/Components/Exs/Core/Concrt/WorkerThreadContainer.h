
#ifndef __Exs_Core_AsyncThreadContainer_H__
#define __Exs_Core_AsyncThreadContainer_H__

#include "SharedSyncObject.h"
#include "ThreadLauncher.h"
#include "WorkerThreadCommon.h"

namespace Exs
{
	namespace Concrt
	{


#define EXS_WORKER_THREAD_FACTORY_PARAMS_DECL \
	ConcrtSharedStateHandle concrtSharedState, const ThreadLaunchInfo& launchInfo, WorkerThreadContainer& container, Worker_thread_index_t workerIndex

#define EXS_WORKER_THREAD_FACTORY_PARAMS_DEF \
	concrtSharedState, launchInfo, container, workerIndex


		///<summary>
		///</summary>
		class WorkerThreadFactory
		{
		public:
			//
			virtual ~WorkerThreadFactory() = default;

			//
			virtual WorkerThreadHandle CreateWorkerThread( EXS_WORKER_THREAD_FACTORY_PARAMS_DECL ) noexcept = 0;
		};


		///<summary>
		///</summary>
		class EXS_LIBCLASS_CORE WorkerThreadContainer
		{
			friend class WorkerThread;

		public:
			using InternalSyncObject = SharedSyncObject<>;
			using InternalMutex = InternalSyncObject::Mutex;
			using InternalMutexLock = InternalSyncObject::MutexLock;
			using WorkerThreadList = std::vector<WorkerThreadHandle>;

		public:
			struct ResizeResult
			{
			public:
				//
				WorkerThreadList threadList;

				//
				Result result;

				//
				Size_t prevSize;

			public:
				ResizeResult()
				: result( RSC_Err_Unknown )
				{ }

				ResizeResult( Result result )
				: result( result )
				{ }

				explicit operator bool() const
				{
					return this->result == RSC_Success;
				}
			};

		public:
			//
			static constexpr Size_t maxSize = Config::CCRT_THR_Max_Thread_Pool_Size;

			//
			static constexpr Worker_thread_index_t maxWorkerIndex = maxSize - 1;

			//
			static constexpr Worker_thread_index_t invalidWorkerIndex = stdx::limits<Worker_thread_index_t>::max_value;

		public:
			WorkerThreadContainer( ConcrtSharedStateHandle concrtSharedState, WorkerThreadFactory& workerThreadFactory, std::string name );

			virtual ~WorkerThreadContainer();

			///
			WorkerThreadHandle AddWorker();

			///
			WorkerThreadList AddWorkers( Size_t threadsNum );

			///
			void Reset();

			/**
			 * @brief Resizes the container by creating new threads or stopping existing ones. If another Resize operation is
			 * in progress, current thread is blocked until it completes.
			 */
			ResizeResult Resize( Size_t threadsNum );

			/**
			 * @brief Resizes the container by creating new threads or stopping existing ones. If another Resize operation is
			 * in progress, function returns immediately. Returns value indicating whether resize was successful.
			 */
			ResizeResult TryResize( Size_t threadsNum );

			/**
			 * @brief Resizes the container by creating new threads or stopping existing ones. If another Resize operation is
			 * in progress, current thread is blocked until it completes or specified timeout elapses.
			 */
			ResizeResult TryResizeFor( Size_t threadsNum, const Nanoseconds& timeout );

			/**
			 * @brief Blocks current thread until resize is completed. If container is not being resized, function returns immediately.
			 */
			void WaitResizeComplete();

			/**
			 * @brief Blocks current thread until resize is completed or specified timeout elapses. If container is not being
			 * resized, function returns immediately.
			 */
			void WaitResizeCompleteFor( const Nanoseconds& timeout );

			///
			InternalMutex& GetInternalMutex() const;

			///
			std::vector<Worker_thread_index_t> GetActiveWorkerIndexList( InternalMutexLock& internalLock ) const;

			///
			std::vector<WorkerThreadHandle> GetActiveWorkerThreadList( InternalMutexLock& internalLock ) const;

			///
			WorkerThreadHandle GetWorkerThread( Worker_thread_index_t workerIndex ) const;

			/// Returns size of the container, i.e. number of threads currently running.
			Size_t Size() const;

			/// Returns true if entry at the specified index contain an active thread object.
			bool IsWorkerActive( Worker_thread_index_t workerIndex ) const;

		protected:
			//
			void SetBaseState( Worker_thread_index_t arrayIndex, WorkerThreadState& state );

			//
			static Worker_thread_index_t QueryWorkerIndex( const WorkerThread& workerThread );

		friendapi:
			//
			bool RegisterThread( WorkerThread& workerThread );

			//
			void UnregisterThread( WorkerThread& workerThread );

			//
			bool CheckExitRequest( WorkerThread& workerThread );

			//
			void SetWorkerStatusFlags( WorkerThread& workerThread, stdx::mask<Worker_thread_status_value_t> flags );

			//
			void UnsetWorkerStatusFlags( WorkerThread& workerThread, stdx::mask<Worker_thread_status_value_t> flags );

			//
			bool CheckWorkerStatus( const WorkerThread& workerThread, stdx::mask<Worker_thread_status_value_t> flags ) const;

			//
			bool CheckWorkerStatusAny( const WorkerThread& workerThread, stdx::mask<Worker_thread_status_value_t> flags ) const;

		private:
			//
			virtual Worker_thread_index_t ReserveWorkerIndex() = 0;

			//
			virtual void ReleaseWorkerIndex( Worker_thread_index_t workerIndex ) = 0;

		private:
			//
			WorkerThreadState& _GetWorkerBaseState( Worker_thread_index_t workerIndex ) const;

			//
			WorkerThreadState& _GetWorkerBaseState( const WorkerThread& workerThread ) const;

			//
			std::pair<bool, Size_t> _CheckAddRequest( InternalMutexLock& internalLock, Size_t newThreadsNum, bool waitOnResize, const Nanoseconds& waitTimeout );

			//
			std::pair<bool, Size_t> _CheckResizeRequest( InternalMutexLock& internalLock, Size_t threadsNum, bool waitOnResize, const Nanoseconds& waitTimeout );

			//
			ResizeResult _Resize( InternalMutexLock& internalLock, Size_t currentThreadsNum, Size_t threadsNum );

			//
			WorkerThreadList _CreateWorkerThreads( InternalMutexLock& internalLock, Size_t threadsNum );

			//
			WorkerThreadHandle _CreateThread( ThreadLaunchInfo& threadLanuchInfo );

			//
			void _SetExitRequestCounter( InternalMutexLock& internalLock, Size_t exitCounterValue );

			//
			void _WaitResizeComplete( InternalMutexLock& internalLock, const Nanoseconds& timeout );

			//
			void _OnSizeChange( InternalMutexLock& internalLock, Size_t threadsNum, bool resizeEvent );

			//
			bool _RunWorkerThread( WorkerThreadHandle workerThread, const ThreadLaunchInfo& threadLanuchInfo );

			//
			bool _SetResizeFlag();

			//
			void _ClearResizeFlag();

			//
			bool _IsResizeFlagSet();

		private:
			enum InternalStateFlags : Enum
			{
				InternalStateFlag_Resize_Active = 0x0400
			};

			// Fixed-sized pool for thread data. When thread is created, it allocates entry from the pool.
			using WorkerBaseStateArray = std::array<WorkerThreadState*, maxSize>;

		private:
			//
			ConcrtSharedStateHandle _concrtSharedState;

			//
			WorkerThreadFactory* _workerThreadFactory;

			//
			std::string _name;

			//
			InternalSyncObject _internalSyncObject;

			//
			WorkerBaseStateArray _workerBaseStateArray;

			//
			std::atomic<Size_t> _workerThreadsNum;

			// Stores number of threads
			std::atomic<Size_t> _resizeExitRequestCounter;

			//
			Size_t _resizeControlCounter;

			//
			stdx::atomic_mask<InternalStateFlags> _internalState;
		};


		inline WorkerThreadContainer::InternalMutex& WorkerThreadContainer::GetInternalMutex() const
		{
			return this->_internalSyncObject.GetMutex();
		}

		inline Size_t WorkerThreadContainer::Size() const
		{
			return this->_workerThreadsNum.load( std::memory_order_relaxed );
		}

		inline bool WorkerThreadContainer::_SetResizeFlag()
		{
			return this->_internalState.test_and_set( InternalStateFlag_Resize_Active );
		}

		inline void WorkerThreadContainer::_ClearResizeFlag()
		{
			this->_internalState.unset( InternalStateFlag_Resize_Active );
		}

		inline bool WorkerThreadContainer::_IsResizeFlagSet()
		{
			return this->_internalState.is_set( InternalStateFlag_Resize_Active );
		}


		///<summary>
		///</summary>
		template <class TWorkerState = WorkerThreadState>
		class WorkerThreadGenericContainer : public WorkerThreadContainer
		{
		public:
			using WorkerState = TWorkerState;

		public:
			WorkerThreadGenericContainer( ConcrtSharedStateHandle concrtSharedState, WorkerThreadFactory& workerThreadFactory, std::string name, Size_t initialSize = 0 )
			: WorkerThreadContainer( concrtSharedState, workerThreadFactory, name )
			{
				this->_InitializeBaseStateArray();

				if ( initialSize > 0 )
				{
					this->Resize( initialSize );
				}
			}

			virtual ~WorkerThreadGenericContainer() = default;

			///
			WorkerState& GetWorkerThreadState( Worker_thread_index_t workerIndex )
			{
				return this->_GetWorkerState( workerIndex );
			}

			///
			const WorkerState& GetWorkerThreadState( Worker_thread_index_t workerIndex ) const
			{
				return this->_GetWorkerState( workerIndex );
			}

			///
			WorkerState& GetWorkerThreadState( const WorkerThread& workerThread )
			{
				return this->_GetWorkerState( workerThread );
			}

			///
			const WorkerState& GetWorkerThreadState( const WorkerThread& workerThread ) const
			{
				return this->_GetWorkerState( workerThread );
			}

		private:
			//
			virtual Worker_thread_index_t ReserveWorkerIndex() override final
			{
				if ( auto stateEntryRef = this->_workerStateArray.acquire() )
				{
					return stateEntryRef.get_key();
				}

				return invalidWorkerIndex;
			}

			//
			virtual void ReleaseWorkerIndex( Worker_thread_index_t workerIndex ) override final
			{
				this->_workerStateArray.release( workerIndex );
			}

		private:
			//
			void _InitializeBaseStateArray()
			{
				for ( Worker_thread_index_t workerIndex = 0; workerIndex <= maxWorkerIndex; ++workerIndex )
				{
					auto& workerState = this->_workerStateArray[workerIndex];
					this->SetBaseState( workerIndex, workerState );
				}
			}

			//
			WorkerState& _GetWorkerState( Worker_thread_index_t workerIndex )
			{
				auto& workerState = this->_workerStateArray[workerIndex];
				return workerState;
			}

			//
			const WorkerState& _GetWorkerState( Worker_thread_index_t workerIndex ) const
			{
				auto& workerState = this->_workerStateArray[workerIndex];
				return workerState;
			}

			//
			WorkerState& _GetWorkerState( const WorkerThread& workerThread )
			{
				auto workerIndex = QueryWorkerIndex( workerThread );
				return this->_GetWorkerState( workerIndex );
			}

			//
			const WorkerState& _GetWorkerState( const WorkerThread& workerThread ) const
			{
				auto workerIndex = QueryWorkerIndex( workerThread );
				return this->_GetWorkerState( workerIndex );
			}

		private:
			// Fixed-sized pool for thread data. When thread is created, it allocates entry from the pool.
			using WorkerStateArray = stdx::concurrent_data_pool<WorkerState, maxSize>;

		private:
			//
			WorkerStateArray _workerStateArray;
		};

	}
}

#endif /* __Exs_Core_AsyncThreadContainer_H__ */
