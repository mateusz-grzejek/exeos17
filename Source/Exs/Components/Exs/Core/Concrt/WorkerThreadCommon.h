
#ifndef __Exs_Core_ThreadAsyncCommon_H__
#define __Exs_Core_ThreadAsyncCommon_H__

#include "AsyncWaitCallback.h"
#include "ThreadCommon.h"


namespace Exs
{
	namespace Concrt
	{


		class WorkerThread;
		class WorkerThreadContainer;


		/// Represents index in the internal array of the pool, assigned to a thread object.
		using Worker_thread_index_t = stdx::concurrent_data_pool_key_t;

		// Value type used to store worker thread status flags.
		using Worker_thread_status_value_t = Enum;

		// Typedef for thread handle for WorkerThread class.
		using WorkerThreadHandle = ThreadObjectHandle<WorkerThread>;


		enum : Worker_thread_status_value_t
		{
			WorkerThreadStatusFlag_Active = 0x0001,
			WorkerThreadStatusFlag_Registered = 0x0002,
			WorkerThreadStatusFlag_Working = 0x0004,
			WorkerThreadStatusFlag_Resize_Exit_Request = 0x8000,
		};


		///<summary>
		///</summary>
		struct WorkerThreadState
		{
			friend class WorkerThreadContainer;

		public:
			//
			WorkerThreadHandle GetThreadHandle() const
			{
				return this->_threadHandle;
			}

			//
			Thread_ref_id_t GetThreadRefID() const
			{
				return this->_refID;
			}

			//
			Worker_thread_index_t GetWorkerIndex() const
			{
				return this->_index;
			}

			//
			bool IsValid() const
			{
				return this->_threadHandle ? true : false;
			}

		friendapi:
			//
			void SetWorkerData( WorkerThreadHandle workerThreadHandle, Thread_ref_id_t workerThreadRefID, Worker_thread_index_t workerIndex )
			{
				this->_threadHandle = workerThreadHandle;
				this->_refID = workerThreadRefID;
				this->_index = workerIndex;
			}

			//
			void ResetWorkerData()
			{
				this->_index = Thread_Index_Invalid;
				this->_refID = Thread_Ref_ID_None;
				this->_threadHandle = nullptr;
			}

			//
			void ResetStatus()
			{
				this->_statusMask.clear();
			}

			//
			void SetStatusFlags( stdx::mask<Worker_thread_status_value_t> flags )
			{
				this->_statusMask.set( flags );
			}

			//
			void UnsetStatusFlags( stdx::mask<Worker_thread_status_value_t> flags )
			{
				this->_statusMask.unset( flags );
			}

			//
			bool CheckStatus( stdx::mask<Worker_thread_status_value_t> flags ) const
			{
				return this->_statusMask.is_set( flags );
			}

			//
			bool CheckStatusAny( stdx::mask<Worker_thread_status_value_t> flags ) const
			{
				return this->_statusMask.is_set_any_of( flags );
			}

		private:
			//
			WorkerThreadHandle _threadHandle;

			//
			Thread_ref_id_t _refID = Thread_Ref_ID_None;

			//
			Worker_thread_index_t _index = Thread_Index_Invalid;

			//
			stdx::atomic_mask<Worker_thread_status_value_t> _statusMask = ATOMIC_VAR_INIT( 0 );
		};


	}
}


#endif /* __Exs_Core_ThreadAsyncCommon_H__ */
