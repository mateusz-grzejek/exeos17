
#pragma once

#ifndef __Exs_Core_ThreadAccessGuard_H__
#define __Exs_Core_ThreadAccessGuard_H__

#include "ThreadCommon.h"


namespace Exs
{
	namespace Concrt
	{


		enum class ThreadAccessPolicy : Enum
		{
			Allow,

			Deny,

			External
		};


		class EXS_LIBCLASS_CORE ThreadAccessGuard
		{
			EXS_DECLARE_NONCOPYABLE( ThreadAccessGuard );

		public:
			ThreadAccessGuard( const char* funcName );

			virtual bool IsThreadAllowed( Thread_ref_id_t threadRefID ) const = 0;

			void Enter();

			bool TryEnter();

		private:
			void _RaiseError();

		private:
			const char* _funcName;
		};


		template <ThreadAccessPolicy>
		class SingleThreadAccessGuard : public ThreadAccessGuard
		{
			EXS_DECLARE_NONCOPYABLE( SingleThreadAccessGuard );

		public:
			SingleThreadAccessGuard( const char* funcName, Thread_ref_id_t threadRefID )
				: ThreadAccessGuard( funcName )
				, _threadRefID( threadRefID )
			{ }

			SingleThreadAccessGuard( const char* funcName, Thread* thread )
				: ThreadAccessGuard(funcName)
				, _threadRefID(ThreadUtils::GetThreadRefID(thread))
			{ }

			virtual bool IsThreadAllowed( Thread_ref_id_t threadRefID ) const override;

		private:
			Thread_ref_id_t _threadRefID;
		};


		template <>
		inline bool SingleThreadAccessGuard<ThreadAccessPolicy::Allow>::IsThreadAllowed( Thread_ref_id_t threadRefID ) const
		{
			return this->_threadRefID == threadRefID;
		}

		template <>
		inline bool SingleThreadAccessGuard<ThreadAccessPolicy::Deny>::IsThreadAllowed( Thread_ref_id_t threadRefID ) const
		{
			return this->_threadRefID != threadRefID;
		}


		template <ThreadAccessPolicy>
		class MultiThreadAccessGuard : public ThreadAccessGuard
		{
			EXS_DECLARE_NONCOPYABLE( MultiThreadAccessGuard );

		public:
			static const Size_t maxThreadsNum = Config::CCRT_THR_Max_Active_Threads_Num / 2;

		public:
			MultiThreadAccessGuard( const char* funcName, const std::initializer_list<Thread_ref_id_t>& threadList )
				: _threadsNum( threadList.size() )
			{
				for ( size_t i = 0; i < this->_threadsNum; ++i )
				{
					this->_threadRefIDList[i] = *( threadList.begin() + i );
				}
			}

			MultiThreadAccessGuard( const char* funcName, const std::initializer_list<Thread*>& threadList )
				: _threadsNum( threadList.size() )
			{
				for ( size_t i = 0; i < this->_threadsNum; ++i )
				{
					this->_threadRefIDList[i] = ThreadUtils::GetThreadRefID( *( threadList.begin() + i ) );
				}
			}

			virtual bool IsThreadAllowed( Thread_ref_id_t threadRefID ) const override;

		private:
			std::array<Thread_ref_id_t, maxThreadsNum> _threadRefIDList;

			Size_t _threadsNum;
		};


		template <>
		inline bool MultiThreadAccessGuard<ThreadAccessPolicy::Allow>::IsThreadAllowed( Thread_ref_id_t threadRefID ) const
		{
			auto threadRefIDIter = std::find( this->_threadRefIDList.begin(), this->_threadRefIDList.end(), threadRefID );
			return threadRefIDIter != this->_threadRefIDList.end();
		}

		template <>
		inline bool MultiThreadAccessGuard<ThreadAccessPolicy::Deny>::IsThreadAllowed( Thread_ref_id_t threadRefID ) const
		{
			auto threadRefIDIter = std::find( this->_threadRefIDList.begin(), this->_threadRefIDList.end(), threadRefID );
			return threadRefIDIter == this->_threadRefIDList.end();
		}




		namespace Internal
		{
		#if ( EXS_CONFIG_BASE_ENABLE_DEBUG_THREAD_ACCESS_CONTROL )

			template <ThreadAccessPolicy Policy>
			inline void RestrictThreadAccess( const char* funcName, Thread_ref_id_t threadRefID )
			{
				SingleThreadAccessGuard<Policy> accessGuard( funcName, threadRefID );
				accessGuard.Enter();
			}

			template <ThreadAccessPolicy Policy>
			inline void RestrictThreadAccess( const char* funcName, Thread* thread )
			{
				SingleThreadAccessGuard<Policy> accessGuard( funcName, thread );
				accessGuard.Enter();
			}

			template <ThreadAccessPolicy Policy>
			inline void RestrictThreadAccess( const char* funcName, const std::initializer_list<Thread_ref_id_t>& threadRefIDList )
			{
				MultiThreadAccessGuard<Policy> accessGuard( funcName, threadRefIDList );
				accessGuard.Enter();
			}

			template <ThreadAccessPolicy Policy>
			inline void RestrictThreadAccess( const char* funcName, const std::initializer_list<Thread*>& threadList )
			{
				MultiThreadAccessGuard<Policy> accessGuard( funcName, threadList );
				accessGuard.Enter();
			}

		#endif
		}


	}
}


#if ( EXS_CONFIG_BASE_ENABLE_DEBUG_THREAD_ACCESS_CONTROL )
#  define ExsThreadAccessControlDebug(threadAccessPolicy, threadList) Internal::RestrictThreadAccess<threadAccessPolicy>(EXS_FUNC, threadList)
#  define ExsThreadAccessControlRuntime(threadAccessPolicy, threadList) Internal::RestrictThreadAccess<threadAccessPolicy>(EXS_FUNC, threadList)
#else
#  define ExsThreadAccessControlDebug(threadAccessPolicy, threadList)
#  define ExsThreadAccessControlRuntime(threadAccessPolicy, threadList) Internal::RestrictThreadAccess<threadAccessPolicy>(EXS_FUNC, threadList)
#endif

#define ExsThreadAccessControlDebugAllow(threadList) ExsThreadAccessControlDebug(ThreadAccessPolicy::Allow, threadList);
#define ExsThreadAccessControlDebugRestrict(threadList) ExsThreadAccessControlDebug(ThreadAccessPolicy::Deny, threadList);

#define ExsThreadAccessControlRuntimeAllow(threadList) ExsThreadAccessControlRuntime(ThreadAccessPolicy::Allow, threadList);
#define ExsThreadAccessControlRuntimeRestrict(threadList) ExsThreadAccessControlRuntime(ThreadAccessPolicy::Deny, threadList);


#endif /* __Exs_Core_ThreadAccessGuard_H__ */
