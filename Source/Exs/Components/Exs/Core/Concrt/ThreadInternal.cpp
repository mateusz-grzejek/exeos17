
#include "Precomp.h"
#include <Exs/Core/Concrt/ThreadInternal.h>
#include <Exs/Core/Concrt/ThreadException.h>
#include <Exs/Core/Concrt/ThreadLocalStorage.h>
#include <Exs/Core/Concrt/ThreadSyncCommon.h>


namespace Exs
{
	namespace Concrt
	{


		ThreadChildRefContainer::ThreadChildRefContainer( Thread& owner )
			: ThreadRuntimeInterface( owner )
			, _childThreadsNum( 0 )
		{ }


		const ChildThreadList& ThreadChildRefContainer::GetChildThreads() const
		{
			return this->_childThreads;
		}


		Size_t ThreadChildRefContainer::GetChildThreadsNum() const
		{
			return this->_childThreadsNum.load( std::memory_order_relaxed );
		}

		Size_t ThreadChildRefContainer::GetChildThreadsNum( std::memory_order loadOrder ) const
		{
			return this->_childThreadsNum.load( loadOrder );
		}


		bool ThreadChildRefContainer::IsChildRegistered( Thread& thread ) const
		{
			auto entryRef = std::find_if( this->_childThreads.begin(), this->_childThreads.end(), [&thread]( const Thread* threadObject ) -> bool {
				return threadObject == &thread;
			} );

			return ( entryRef != this->_childThreads.end() );
		}


		ChildThreadRef ThreadChildRefContainer::AddChild( Thread& childThread )
		{
			if ( this->IsChildRegistered( childThread ) )
			{
				// This function should be used only by the Thread object. Any mismatches in state
				// (double registers, etc.) are logic errors which should be fixed at the source code level.

				ExsThrowExceptionEx( EXC_Invalid_State, "" );
			}

			auto childRef = this->_childThreads.insert( this->_childThreads.end(), &childThread );
			this->_childThreadsNum.fetch_add( 1, std::memory_order_relaxed );

			return childRef;
		}


		void ThreadChildRefContainer::RemoveChild( Thread& childThread, ChildThreadRef childThreadRef )
		{
			if ( *childThreadRef != &childThread )
			{
				//

				ExsThrowExceptionEx( EXC_Invalid_Operation, "" );
			}

			if ( !this->IsChildRegistered( childThread ) )
			{
				// This function should be used only by the Thread object. Any mismatches in state
				// (double frees, etc.) are logic errors which should be fixed at the source code level.

				ExsThrowExceptionEx( EXC_Invalid_State, "" );
			}

			this->_childThreads.erase( childThreadRef );
			this->_childThreadsNum.fetch_sub( 1, std::memory_order_relaxed );
		}




		namespace Internal
		{

			ThreadUID CreateThreadUID( Thread* thread, Thread_index_t thrIndex, Thread_ref_id_t refID )
			{
				ThreadUID result;
				result.internalData.index = thrIndex;

				// If non-zero ref id is specified by the user during thread's creation, use it.
				result.internalData.refID = refID;

				// Otherwise, use the address of the thread object as ref id.
				auto ptrValue = reinterpret_cast<Ptrval_t>( thread );

			#if ( EXS_TARGET_64 )
				// On 64-bit target, shift to remove lower 16 bits and then truncate to keep middle 32 bits only.
				result.internalData.thrObjectPtr = truncate_cast<Uint32>( ptrValue >> 16 );
			#else
				// On 32-bit target, use all 32 bits of the address.
				result.internalData.thrObjectPtr = truncate_cast<Uint32>( ptrValue );
			#endif

				return result;
			}


			std::pair<ThreadLocalStorage*, Uint> GetThreadLocalStorageUnchecked()
			{
				// Ensures, that each TLS is initialized only once.
				static EXS_ATTR_THREAD_LOCAL std::once_flag tlsInitFlag{};

				// Actual TLS data, stored as thread-specific object.
				static EXS_ATTR_THREAD_LOCAL ThreadLocalStorage threadLocalStorage{};

				// TLS is initialized by filling its whole memory with zeros.
				std::call_once( tlsInitFlag, []( ThreadLocalStorage* tls ) -> void {
					memset( tls, 0, sizeof( ThreadLocalStorage ) );
				}, &threadLocalStorage );

				// Increment fetch count and store it in variable to be returned (value befonre incrementation!).
				Uint fetchCount = threadLocalStorage.controlBlock.uncheckedFetchCount++;

				// Return pointer to the TLS and value of the fetch counter.
				return std::pair<ThreadLocalStorage*, Uint>( &threadLocalStorage, fetchCount );
			}


			ThreadLocalStorage* InitializeThreadLocalStorage()
			{
				// Fetch TLS via internal ("unchecked") method to initialize it.
				auto tlsInfo = GetThreadLocalStorageUnchecked();

				// GetThreadLocalStorageUnchecked() also returns fetch counter. At this point, TLS should be
				// fetched for the very first time (value of fetch counter should be 0). Check this before init.

				if ( tlsInfo.second != 0 )
				{
					// If fetch counter is greater than 0, there is a logic error somewhere in code
					// (the TLS cannot be used before its initialization - which is performed here!).

					ExsThrowExceptionEx( EXC_Invalid_State, "TLS fetch counter value mismatch" );
				}

				return tlsInfo.first;
			}


			void ReleaseThreadLocalStorage( ThreadLocalStorage* tls )
			{
				tls->controlBlock.uncheckedFetchCount = 0;
			}

		}


	}
}
