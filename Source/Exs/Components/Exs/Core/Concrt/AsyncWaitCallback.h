
#ifndef __Exs_Core_AsyncWaitCallback_H__
#define __Exs_Core_AsyncWaitCallback_H__

#include "AsyncCommon.h"


namespace Exs
{
	namespace Concrt
	{


		template <class TRes>
		struct AsyncWaitCallbackExecuteProxy
		{
			template <class TCallable>
			static void Execute( const TCallable& callable, AsyncValue<TRes>& asyncResult )
			{
				asyncResult.SetValue( callable() );
			}
		};

		template <>
		struct AsyncWaitCallbackExecuteProxy<void>
		{
			template <class TCallable>
			static void Execute( const TCallable& callable, AsyncValue<void>& )
			{
				callable();
			}
		};


		template <class T>
		class AsyncWaitCallback;


		template <class TRes, class... TArgs>
		class AsyncWaitCallback<TRes( TArgs... )>
		{
		public:
			using FunctionType = std::function<TRes( TArgs... )>;
			using AsyncResultType = AsyncValue<TRes>;

		public:
			AsyncWaitCallback()
				: _result( Tag::uninitialized )
			{ }

			AsyncWaitCallback( std::nullptr_t )
				: _result( Tag::uninitialized )
			{ }

			template <class TCallable>
			AsyncWaitCallback( TCallable callable )
				: _function( std::forward<TCallable>( callable ) )
			{ }

			AsyncWaitCallback( AsyncWaitCallback && ) = default;
			AsyncWaitCallback& operator=( AsyncWaitCallback&& ) = default;

			AsyncWaitCallback( const AsyncWaitCallback & ) = default;
			AsyncWaitCallback& operator=( const AsyncWaitCallback& ) = default;

			explicit operator bool() const
			{
				return this->_function ? true : false;
			}

			void Run()
			{
				if ( this->_function )
				{
					try
					{
						AsyncWaitCallbackExecuteProxy<TRes>::Execute( this->_function, this->_result );
					}
					catch ( ... )
					{
						if ( this->_result )
						{
							this->_result.SaveCurrentException();
						}
					}
				}

				if ( this->_result )
				{
					SyncMutexLock syncLock{ this->_result.GetMutex() };
					{
						this->_result.SetReady( syncLock );
					}
				}
			}

			AsyncValue<TRes> GetResult() const
			{
				return this->_result;
			}

			bool IsEmpty() const
			{
				return !this->_function;
			}

		private:
			//
			std::function<TRes( TArgs... )> _function;

			//
			AsyncValue<TRes> _result;
		};


	}
}


#endif /* __Exs_Core_AsyncWaitCallback_H__ */
