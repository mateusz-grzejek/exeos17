
#ifndef __Exs_Core_CoreMessageSender_H__
#define __Exs_Core_CoreMessageSender_H__

#include "CoreMessageDispatcher.h"
#include "ExclusiveSyncObject.h"


namespace Exs
{
	namespace Concrt
	{


		class CoreMessageSystemRoot;


		enum : Enum
		{
			RSC_Msg_Err_No_Sync_Message,
			RSC_Msg_Err_Invalid_Message,
			RSC_Msg_Response_Not_Ready,
			RSC_Msg_Response_Wait_Timeout
		};


		class CoreMessageSender
		{
			friend class CoreMessage;
			friend class CoreMessageResponse;

		public:
			CoreMessageSender( CoreMessageSystemRoot& messageSystemRoot, Thread& thread, CoreMessageSenderID id );

			CoreMessageSendResult SendMessage( const CoreMessageSendRequest& sendRequest, const CoreMessageHandle& message );

			CoreSyncMessageSendResult SendMessage( const CoreSyncMessageSendRequest& sendRequest, const CoreMessageHandle& message );

			CoreMessageResponseResult WaitForResponse( CoreMessage* message = nullptr );

			CoreMessageResponseResult GetMessageResponse( CoreMessage* message = nullptr );

			CoreMessageSenderID GetID() const;

		friendapi:
			// @ CoreMessage [TAR: !owner]
			CoreMessageResponse* CreateMessageResponse( CoreMessage& message, Message_code_t responseCode = 0 );

			// @ CoreMessageResponse [TAR: !owner]
			void SetResponseReady( CoreMessage& message, CoreMessageResponse& response );

		private:
			//
			void _SetSyncMessageState( CoreMessage& message, const CoreSyncMessageSendRequest& syncSendRequest );

			//
			void _ResetSyncMessageState();

			// Checks specified message against sync message which is currently being sent. If 'message' is not NULL,
			// functions returns boolean value indicating whether it points to the current sync message. If parameter
			// is NULL
			bool _CheckCurrentSyncMessage( CoreMessage* message ) const;

			//
			bool _IsResponseCreated() const;

			//
			bool _IsResponseReady() const;

			//
			Result _DispatchMessage( const CoreMessageSendRequest& syncSendRequest, const CoreMessageHandle& message );

			//
			bool _WaitForResponse( SyncMutexLock& lock );

		private:
			struct SyncMessageContext
			{
				using WaitTimeoutUnit = CoreSyncMessageSendRequest::WaitTimeoutUnit;

				// Sync object - serves as mutex for controlling access to this state
				// and wait condition for synchronization between sender and receiver.
				ExclusiveSyncObject<> syncObject;

				// Wait timeout. Saved, so the sender can know how long to wait if
				// waiting is not done immediately after the message has been dispatched.
				WaitTimeoutUnit waitTimeout;

				// Actual response.
				CoreMessageResponseHandle response;

				// Current sync message. Only response for this message can be retrieved.
				std::atomic<CoreMessage*> currentSyncMessage = ATOMIC_VAR_INIT( nullptr );

				// Flag indicating whether response is ready.
				std::atomic<bool> responseReady = ATOMIC_VAR_INIT( false );
			};

		private:
			//
			CoreMessageSystemRoot* _messageSystemRoot;

			// Pointer to the thread object which owns this receiver.
			Thread* _thread;

			//
			CoreMessageSenderID _id;

			//
			SyncMessageContext _syncMessageContext;
		};


		inline CoreMessageSenderID CoreMessageSender::GetID() const
		{
			return this->_id;
		}

		inline bool CoreMessageSender::_CheckCurrentSyncMessage( CoreMessage* message ) const
		{
			auto* currentSyncMessage = this->_syncMessageContext.currentSyncMessage.load( std::memory_order_relaxed );
			return ( message != nullptr ) ? ( currentSyncMessage == message ) : ( currentSyncMessage != nullptr );
		}

		inline bool CoreMessageSender::_IsResponseCreated() const
		{
			return this->_syncMessageContext.response ? true : false;
		}

		inline bool CoreMessageSender::_IsResponseReady() const
		{
			return this->_syncMessageContext.responseReady.load( std::memory_order_relaxed );
		}

	}
}


#endif /* __Exs_Core_CoreMessageSender_H__ */
