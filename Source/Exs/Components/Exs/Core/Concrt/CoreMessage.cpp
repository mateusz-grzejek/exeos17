
#include "Precomp.h"
#include <Exs/Core/Concrt/CoreMessage.h>
#include <Exs/Core/Concrt/CoreMessageSender.h>


namespace Exs
{
	namespace Concrt
	{


		CoreMessage::CoreMessage( Message_code_t code, MessagePriority priority )
			: MessageBase( code, priority )
			, _sender( nullptr )
			, _response( nullptr )
		{
			this->_header.uid = CoreMessageUIDGen::GenerateGlobalMessageUID( code );
		}


		CoreMessageResponse* CoreMessage::CreateResponse( Message_code_t responseCode )
		{
			if ( this->_sender == nullptr )
			{
				return nullptr;
			}

			return this->_sender->CreateMessageResponse( *this, responseCode );
		}


		CoreMessageResponse* CoreMessage::GetResponse()
		{
			if ( this->_sender == nullptr )
			{
				return nullptr;
			}

			if ( this->_response == nullptr )
			{
				auto responseResult = this->_sender->GetMessageResponse( this );
				this->_response = responseResult.response;
			}

			return this->_response;
		}


		CoreMessageResponse* CoreMessage::WaitForResponse()
		{
			if ( this->_sender == nullptr )
			{
				return nullptr;
			}

			if ( this->_response == nullptr )
			{
				auto responseResult = this->_sender->WaitForResponse( this );
				this->_response = responseResult.response;
			}

			return this->_response;
		}




		CoreMessageResponse::CoreMessageResponse( CoreMessage& message, Message_code_t responseCode )
			: MessageRepsonseBase( message.GetCode(), responseCode )
			, _sender( message.GetSender() )
		{
			this->_header.respondedMessage = &message;
		}


		void CoreMessageResponse::SetReady()
		{
			if ( this->_sender == nullptr )
			{
				return;
			}

			this->_sender->SetResponseReady( *( this->_header.respondedMessage ), *this );
		}


	}
}
