
#ifndef __Exs_Core_SyncConditionVariable_H__
#define __Exs_Core_SyncConditionVariable_H__

#include "SyncCommon.h"
#include <condition_variable>


namespace Exs
{
	namespace Concrt
	{


		/**
		 * @brief Proxy class with unified interface wrapped around actual std::condition_variableXXX. Provides abstraction
		 * layer independent from actual CV type. It is not intended to be inherited, but stored as an internal member.
		 *
		 * @tparam TCondVar The type of the internal condition variable.
		 */
		template <class TCondVar>
		class ConditionVariableProxy
		{
		public:
			ConditionVariableProxy() = default;

			/**
			 * @brief Calls .notify_all() on internal CV.
			 */
			void NotifyAll()
			{
				this->_condVar.notify_all();
			}

			/**
			 * @brief Calls .notify_one() on internal CV.
			 */
			void NotifyOne()
			{
				this->_condVar.notify_one();
			}

			/**
			 * @brief Calls .wait( lock ) on internal CV.
			 */
			template <class TLock>
			void Wait( TLock& lock )
			{
				this->_condVar.wait( lock );
			}

			/**
			 * @brief Calls .wait( lock, predicate ) on internal CV.
			 */
			template <class TLock, class TPredicate>
			void Wait( TLock& lock, TPredicate predicate )
			{
				this->_condVar.wait( lock, predicate );
			}

			/**
			 * @brief Calls .wait( lock, duration ) on internal CV. Duration is an Exs::Durtion object. If duration's value is
			 * set to Timeout_Infinite, wait is performed using wait( lock ) to avoid possible overflows if internal implementation
			 * does wait_until( std::XXX_clock::now() + timeout ).
			 */
			template <class TLock, DurationPeriod tPeriod, typename TDurationRep>
			bool WaitFor( TLock& lock, const Duration<tPeriod, TDurationRep>& duration )
			{
				if ( duration.GetValue() == Timeout_Infinite )
				{
					this->Wait( lock );
					return true;
				}

				auto status = this->_condVar.wait_for( lock, static_cast<typename Duration<tPeriod, TDurationRep>::StdDurationType>( duration ) );

				return status == std::cv_status::no_timeout;
			}

			/**
			 * @brief Calls .wait( lock, duration, predicate ) on internal CV. Duration is an Exs::Durtion object. If the value
			 * of duration is Timeout_Infinite, wait is performed using wait( lock, predicate ) (see note above for explanation).
			 */
			template <class TLock, DurationPeriod tPeriod, typename TDurationRep, class TPredicate>
			bool WaitFor( TLock& lock, const Duration<tPeriod, TDurationRep>& duration, TPredicate predicate )
			{
				if ( duration.GetValue() == Timeout_Infinite )
				{
					this->Wait( lock, predicate );
					return true;
				}

				return this->_condVar.wait_for( lock, static_cast<typename Duration<tPeriod, TDurationRep>::StdDurationType>(duration), predicate );
			}

			/**
			 * @brief Calls .wait( lock, duration, predicate ) on internal CV. Duration is an std::chrono::duration object.
			 * If the value of duration is Timeout_Infinite, wait is performed using wait( lock ) (see note above).
			 */
			template <class TLock, class TDurationRep, class TDurationPeriod>
			bool WaitFor( TLock& lock, const std::chrono::duration<TDurationRep, TDurationPeriod>& duration )
			{
				if ( duration.count() == Timeout_Infinite )
				{
					this->Wait( lock );
					return true;
				}

				auto status = this->_condVar.wait_for( lock, duration );

				return status == std::cv_status::no_timeout;
			}

			/**
			 * @brief Calls .wait( lock, duration, predicate ) on internal CV. Duration is an an std::chrono::duration object.
			 * If the value of duration is Timeout_Infinite, wait is performed using wait( lock, predicate ) (see note above).
			 */
			template <class TLock, class TDurationRep, class TDurationPeriod, class TPredicate>
			bool WaitFor( TLock& lock, const std::chrono::duration<TDurationRep, TDurationPeriod>& duration, TPredicate predicate )
			{
				if ( duration.count() == Timeout_Infinite )
				{
					this->Wait( lock, predicate );
					return true;
				}

				return this->_condVar.wait_for( lock, duration, predicate );
			}

		private:
			// Internal condition variable.
			TCondVar _condVar;
		};


		/**
		 * @brief Base class for condition variables used within SyncObject implementation. Provides implementation of
		 * CV type-independent methods. Inerited by more specialized CVs defined below.
		 *
		 * @tparam TCondVar The type of an underlying CV object, used as template arg for ConditionVariableProxy.
		 */
		template <class TCondVar>
		class SyncConditionVariableBase
		{
		public:
			SyncConditionVariableBase() = default;

			/**
			 * @brief Notifies all threads waiting on this condition variable. Calls .NotifyAll() on internal CV proxy.
			 */
			void NotifyAll()
			{
				this->_condVarProxy.NotifyAll();
			}

			/**
			 * @brief Notifies single thread waiting on this condition variable. Calls .NotifyOne() on internal CV proxy.
			 */
			void NotifyOne()
			{
				this->_condVarProxy.NotifyOne();
			}

		protected:
			// Condition variable proxy for the specified CV type.
			ConditionVariableProxy<TCondVar> _condVarProxy;
		};


		/**
		 * @brief SyncConditionVariable which can be used with any mutex type which is a Lockable type. Uses internally
		 * std::condition_variable_any as the executive CV object.
		 */
		class SyncConditionVariableAny : public SyncConditionVariableBase<std::condition_variable_any>
		{
		public:
			SyncConditionVariableAny() = default;

			template <class TLock>
			void Wait( TLock& lock )
			{
				this->_condVarProxy.Wait( lock );
			}

			template <class TLock, class TPredicate>
			void Wait( TLock& lock, TPredicate predicate )
			{
				this->_condVarProxy.Wait( lock, predicate );
			}

			template <class TLock, class TDuration>
			bool WaitFor( TLock& lock, const TDuration& duration )
			{
				return this->_condVarProxy.WaitFor( lock, duration );
			}

			template <class TLock, class TDuration, class TPredicate>
			bool WaitFor( TLock& lock, const TDuration& duration, TPredicate predicate )
			{
				return this->_condVarProxy.WaitFor( lock, duration, predicate );
			}
		};


		/**
		 * @brief SyncConditionVariable which can be used only with SystemMutex (std::mutex). Uses internally
		 * std::condition_variable as the executive CV object.
		 */
		class SyncConditionVariableSystem : public SyncConditionVariableBase<std::condition_variable>
		{
		public:
			SyncConditionVariableSystem() = default;

			void Wait( SyncMutexLock& lock )
			{
				this->_condVarProxy.Wait( lock );
			}

			template <class TPredicate>
			void Wait( SyncMutexLock& lock, TPredicate predicate )
			{
				this->_condVarProxy.Wait( lock, predicate );
			}

			template <class TDuration>
			bool WaitFor( SyncMutexLock& lock, const TDuration& duration )
			{
				return this->_condVarProxy.WaitFor( lock, duration );
			}

			template <class TDuration, class TPredicate>
			bool WaitFor( SyncMutexLock& lock, const TDuration& duration, TPredicate predicate )
			{
				return this->_condVarProxy.WaitFor( lock, duration, predicate );
			}
		};


	}
}


#endif /* __Exs_Core_SyncConditionVariable_H__ */
