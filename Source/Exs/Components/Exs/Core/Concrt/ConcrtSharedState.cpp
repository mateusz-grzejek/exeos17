
#include "Precomp.h"
#include <Exs/Core/Concrt/ConcrtSharedState.h>
#include <Exs/Core/Concrt/CoreMessageSystemRoot.h>
#include <Exs/Core/Concrt/ThreadSystemManager.h>


namespace Exs
{
	namespace Concrt
	{


		ConcrtSharedStateHandle CreateConcrtSharedState( const ConcrtSharedStateInitInfo& initInfo )
		{
			auto concrtSharedState = std::make_shared<ConcrtSharedState>();

			if ( initInfo.systemSystemSessionState )
			{
				concrtSharedState->systemSystemSessionState = initInfo.systemSystemSessionState;
			}
			else
			{
				concrtSharedState->systemSystemSessionState = System::CreateSystemSessionState();
			}

			concrtSharedState->threadSystemManager = std::make_shared<ThreadSystemManager>( concrtSharedState );
			concrtSharedState->coreMessageSystemRoot = std::make_shared<CoreMessageSystemRoot>();

			return concrtSharedState;
		}


	}
}
