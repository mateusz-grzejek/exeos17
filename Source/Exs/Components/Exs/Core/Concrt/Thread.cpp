
#include "Precomp.h"
#include <Exs/Core/Concrt/Thread.h>
#include <Exs/Core/Concrt/ThreadAccessGuard.h>
#include <Exs/Core/Concrt/ThreadException.h>
#include <Exs/Core/Concrt/ThreadInternal.h>
#include <Exs/Core/Concrt/ThreadLocalStorage.h>
#include <Exs/Core/Concrt/ThreadSyncGroup.h>
#include <Exs/Core/Concrt/ThreadMessageTransceiver.h>
#include <Exs/Core/Concrt/ThreadSystemManager.h>
#include <ExsLib/System/Thread.h>
#include <sstream>

namespace Exs
{
	namespace Concrt
	{


		Thread::Thread( ConcrtSharedStateHandle concrtSharedState, Thread* parent, const ThreadProperties& properties )
			: _concrtSharedState( concrtSharedState )
			, _threadSystemManager( concrtSharedState->threadSystemManager.get() )
			, _refID( properties.refID )
			, _uid( properties.refID, Thread_Index_Invalid )
			, _name( properties.name )
			, _internalSyncObject( *this )
			, _internalExecutionState( 0 )
			, _permissions( properties.permissions )
			, _parent( parent )
			, _localStorage( nullptr )
			, _syncGroup( nullptr )
		{ }


		Thread::~Thread()
		{ }


		bool Thread::Exit()
		{
			InternalMutexLock internalLock{ this->GetInternalMutex() };
			{
				if ( auto* parametersPtr = this->_SetRequest( internalLock, ThreadRequestCode::Exit ) )
				{
					// no parameters for exit request
					( parametersPtr );
					return true;
				}
			}

			return false;
		}


		bool Thread::Terminate()
		{
			InternalMutexLock internalLock{ this->GetInternalMutex() };
			{
				if ( auto* parametersPtr = this->_SetRequest( internalLock, ThreadRequestCode::Terminate ) )
				{
					// no parameters for terminate request
					( parametersPtr );
					return true;
				}
			}

			return false;
		}


		bool Thread::Suspend( bool interruptSleep )
		{
			InternalMutexLock internalLock{ this->GetInternalMutex() };
			{
				if ( auto* parametersPtr = this->_SetRequest( internalLock, ThreadRequestCode::Suspend ) )
				{
					// no parameters for suspend request
					( parametersPtr );
					return true;
				}
			}

			return false;
		}


		bool Thread::Sleep( const Nanoseconds& timeout )
		{
			InternalMutexLock internalLock{ this->GetInternalMutex() };
			{
				if ( auto* parametersPtr = this->_SetRequest( internalLock, ThreadRequestCode::Sleep ) )
				{
					parametersPtr->sleepParams.timeout = timeout;
					return true;
				}
			}

			return false;
		}


		void Thread::Resume()
		{
			SyncMutexLock lock{ this->_internalSyncObject.GetMutex() };
			{
				if ( this->IsSuspended() )
				{
					this->_internalExecutionState.unset( ThreadExecutionStateFlag_Suspended );
					this->_internalSyncObject.NotifyOne( lock );
				}
				else
				{
					auto currentRequestCode = this->_internalRequest.GetCode();

					if ( ( currentRequestCode == ThreadRequestCode::Sleep ) || ( currentRequestCode == ThreadRequestCode::Suspend ) )
					{
						this->_internalRequest.Clear();
					}
				}
			}
		}


		void Thread::Join()
		{
			if ( this->_sysThreadHandle )
			{
				System::ThreadAPI::WaitForThread( this->_sysThreadHandle );
				this->_sysThreadHandle = nullptr;
			}
		}


		bool Thread::WaitForExecutionEvent( InternalMutexLock& internalMutexLock, ThreadExecutionEvent event )
		{
			return this->WaitForStateChange( internalMutexLock, static_cast<ThreadExecutionEvent>( event ), true );
		}


		std::string Thread::GetTextInfo() const
		{
			std::ostringstream strStream;

			auto threadAddress = reinterpret_cast<Uint>( this );
			auto tlsAddress = reinterpret_cast<Uint>( this->_localStorage );

			strStream << "Thread \"" << this->_name << "\" (0x" << std::hex << threadAddress << ")";
			strStream << ", UID: " << ThreadUtils::GetUIDString( this->_uid );
			strStream << ", TLS address = 0x" << std::hex << tlsAddress;

			return strStream.str();
		}


		Result Thread::Run( ThreadObjectHandle<Thread> thread, stdx::mask<ThreadRunFlags> runFlags )
		{
			try
			{
				if ( !thread->_SetActiveFlag() )
				{
					// This means, that 'thread' has already been started
					ExsThrowException( EXC_Invalid_Operation );
				}

				if ( !thread->_AcquireThreadState() )
				{
					ExsThrowException( EXC_Internal_Error );
				}

				if ( !_RunSystemThread( thread, runFlags ) )
				{
					ExsThrowException( EXC_Internal_Error );
				}
			}
			catch ( ... )
			{
				thread->_ResetRunState();

				return ExsResult( RSC_Success );
			}

			// Very important: without this, current thread may finish its execution before new thread gets a chance
			// to register itself (if thread was created with ThreadRun_Enable_Parent_Sync flag). Such threads have
			// their _parent member set to a non-NULL value.

			if ( thread->_parent != nullptr )
			{
				InternalMutexLock internalLock{ thread->GetInternalMutex() };
				{
					// Registration_Complete flag is set after thread registers itself within the thread system
					// and successfuly adds itself to the list of child threads of its parent (see _InitWrapper()).
					thread->WaitForStateChange( internalLock, ThreadExecutionStateFlag_Registered, true );
				}
			}

			return ExsResult( RSC_Success );
		}


		void Thread::SetSyncGroup( ThreadSyncGroup& syncGroup )
		{
			if ( this->IsActive() )
			{

			}

			this->_syncGroup = &syncGroup;
		}


		ThreadUpdateResult Thread::Update()
		{
			if ( this->_ProcessRequestState() == ThreadRequestProcessResult::Exit )
			{
				return ThreadUpdateResult::Exit;
			}

			return ThreadUpdateResult::Continue;
		}


		bool Thread::WaitForStateChange( InternalMutexLock& internalMutexLock, stdx::mask<ThreadExecutionStateFlags> state, bool expected )
		{
			if ( this->_internalExecutionState.is_set( state ) != expected )
			{
				this->_internalSyncObject.Wait( internalMutexLock, [this, state, expected]() -> bool {
					return ( this->_internalExecutionState.is_set( state ) == expected ) || this->_internalExecutionState.is_set( ThreadExecutionStateFlag_Finished );
				} );
			}

			return this->_internalExecutionState.is_set( state ) == expected;
		}


		bool Thread::SetExecutionStateFlag( InternalMutexLock& internalMutexLock, stdx::mask<ThreadExecutionStateFlags> state, bool set )
		{
			if ( this->_internalExecutionState.is_set( state ) != set )
			{
				if ( set )
				{
					this->_internalExecutionState.set( state );
				}
				else
				{
					this->_internalExecutionState.unset( state );
				}

				if ( state.is_set( ThreadExecutionStateFlag_Event_Mask ) )
				{
					//
					this->_internalSyncObject.NotifyAll( internalMutexLock );

					if ( this->_syncGroup != nullptr )
					{
						//
						auto event = static_cast<ThreadExecutionEvent>( state.get() );

						//
						this->_syncGroup->NotifyOne( *this, event );
					}
				}

				return true;
			}

			return false;
		}


		bool Thread::OnInit( const ThreadInitContext& initContext )
		{
			return true;
		}


		void Thread::OnRelease( const ThreadReleaseContext& releaseContext ) noexcept
		{ }


		bool Thread::InitializeExternal( ThreadObjectHandle<Thread> thread )
		{
			try
			{
				if ( !thread->_SetActiveFlag() )
				{
					// This means, that 'thread' has already been started
					ExsThrowException( EXC_Invalid_Operation );
				}

				if ( !thread->_AcquireThreadState() )
				{
					ExsThrowException( EXC_Internal_Error );
				}

				if ( !_InitWrapper( thread, nullptr ) )
				{
					ExsThrowException( EXC_Internal_Error );
				}
			}
			catch ( ... )
			{
				thread->_ResetRunState();

				false;
			}

			return true;
		}


		void Thread::ReleaseExternal( ThreadObjectHandle<Thread> thread )
		{
			_ReleaseWrapper( thread );
		}


		bool Thread::_SetActiveFlag()
		{
			return this->_internalExecutionState.test_and_set( ThreadExecutionStateFlag_Activated );
		}


		bool Thread::_AcquireThreadState()
		{
			// Reserve state for the current thread. Returned value is the UID generated for this thread.
			this->_uid = this->_threadSystemManager->CreateThreadState( *this );

			return this->_uid != Thread_UID_Invalid;
		}


		void Thread::_ReleaseThreadState()
		{
			if ( this->_uid != Thread_UID_Invalid )
			{
				//
				this->_threadSystemManager->ReleaseThreadState( *this );

				this->_uid = Thread_UID_Invalid;
			}
		}


		void Thread::_ResetRunState()
		{
			this->_ReleaseThreadState();

			this->_internalExecutionState.unset( ThreadExecutionStateFlag_Activated );
		}


		void Thread::_CreateDynamicThreadLocalStorage()
		{
			//
			auto* threadLocalStorage = Internal::InitializeThreadLocalStorage();

			threadLocalStorage->internalState.thread = this;
			threadLocalStorage->internalState.threadUID = this->_uid;

			this->_localStorage = threadLocalStorage;
		}


		void Thread::_DestroyDynamicThreadLocalStorage() noexcept
		{
			Internal::ReleaseThreadLocalStorage( this->_localStorage );

			this->_localStorage = nullptr;
		}


		void Thread::_Register()
		{
			ThreadSystemRegInfo systemRegInfo;
			systemRegInfo.threadUID = this->_uid;
			systemRegInfo.threadName = this->_name.c_str();
			systemRegInfo.permissions = this->_permissions;

			this->_threadSystemManager->RegisterThread( *this, systemRegInfo );

			if ( this->_parent != nullptr )
			{
				// If parent is not NULL, child thread is requested to register itself
				// within parent child threads list to allow proper synchronization.

				this->_parentListRef = this->_parent->_AddChildThread( *this );
			}

			{
				InternalMutexLock internalLock{ this->GetInternalMutex() };
				{
					// This is required by the parent thread to properly synchronize with its subthreads.
					// Although synchronization is performed only for threads with Sync_With_Parent flag,
					// state is updated for every thread for concistency (and manual syncs).

					this->SetExecutionStateFlag( internalLock, ThreadExecutionStateFlag_Registered, true );
				}
			}
		}


		void Thread::_Unregister() noexcept
		{
			if ( this->_parent != nullptr )
			{
				this->_parent->_RemoveChildThread( *this, this->_parentListRef );
			}

			this->_threadSystemManager->UnregisterThread( *this );
		}


		void Thread::_Initialize()
		{
			this->_coreSystemData = this->_threadSystemManager->CreateThreadRuntimeObjects( *this );

			if ( !this->_coreSystemData )
			{
				ExsThrowThreadException( EXC_Thread_Initialization_Error );
			}

			this->_localStorage->messageController = this->_coreSystemData.messageController;

			ThreadInitContext initContext;
			initContext.thread = this;
			initContext.localStorage = this->_localStorage;

			if ( !this->OnInit( initContext ) )
			{
				ExsThrowThreadException( EXC_Thread_Initialization_Error );
			}
		}


		void Thread::_Release() noexcept
		{
			ThreadReleaseContext releaseContext;
			releaseContext.thread = this;
			releaseContext.localStorage = this->_localStorage;

			this->OnRelease( releaseContext );

			this->_localStorage->messageController = nullptr;

			this->_threadSystemManager->DestroyThreadRuntimeObjects( *this );

			{
				InternalMutexLock internalLock{ this->GetInternalMutex() };
				{
					this->SetExecutionStateFlag( internalLock, ThreadExecutionStateFlag_Released, true );
				}
			}
		}


		void Thread::_SyncWithChildThreads() noexcept
		{
			if ( this->_coreSystemData.childRefContainer )
			{
				ExsBeginCriticalSection( this->_coreSystemData.childRefContainer->GetLock() );
				{
					const auto& childThreads = this->_coreSystemData.childRefContainer->GetChildThreads();

					if ( !childThreads.empty() )
					{
						for ( auto* childThread : childThreads )
						{
							childThread->Exit();
						}
					}
				}
				ExsEndCriticalSection();

				for ( int64_t c = 0; this->_coreSystemData.childRefContainer->GetChildThreadsNum() > 0; c++ )
				{
					stdx::thr_yield_spinlock( c );
				}
			}
		}


		ChildThreadRef Thread::_AddChildThread( Thread& childThread )
		{
			if ( this == &childThread )
			{
				// It is an error to add thread to its own list of child threads.
				// Since returning empty ref is not a helpful solution, throw an error.

				ExsThrowExceptionEx( EXC_Invalid_Operation, "Thread cannot ba added to its own child thread list." );
			}

			ExsBeginCriticalSection( this->_coreSystemData.childRefContainer->GetLock() );
			{
				return this->_coreSystemData.childRefContainer->AddChild( childThread );
			}
			ExsEndCriticalSection();
		}


		void Thread::_RemoveChildThread( Thread& childThread, ChildThreadRef childThreadRef )
		{
			ExsBeginCriticalSection( this->_coreSystemData.childRefContainer->GetLock() );
			{
				this->_coreSystemData.childRefContainer->RemoveChild( childThread, childThreadRef );
			}
			ExsEndCriticalSection();
		}


		ThreadRequestParameters* Thread::_SetRequest( InternalMutexLock& internalLock, ThreadRequestCode requestCode )
		{
			if ( this->_internalRequest.CheckPriority( requestCode ) )
			{
				this->_internalRequest.Set( requestCode );

				auto& parameters = this->_internalRequest.GetParameters();

				return &parameters;
			}

			return nullptr;
		}


		ThreadRequestProcessResult Thread::_ProcessRequestState()
		{
			if ( this->_internalRequest )
			{
				InternalMutexLock internalLock{ this->GetInternalMutex() };
				{
					if ( this->_internalRequest )
					{
						auto requestCode = this->_internalRequest.GetCode();
						this->_internalRequest.Clear();

						switch ( requestCode )
						{
							case ThreadRequestCode::Sleep:
							{
								auto& parameters = this->_internalRequest.GetParameters();
								this->_SuspendExecution( internalLock, parameters.sleepParams.timeout );
								break;
							}

							case ThreadRequestCode::Suspend:
							{
								this->_SuspendExecution( internalLock, Timeout_Infinite );
								break;
							}

							case ThreadRequestCode::Exit:
							{
								return ThreadRequestProcessResult::Exit;
								break;
							}

							case ThreadRequestCode::Terminate:
							{
								ExsThrowThreadInterruptException( EXC_Thread_Terminate );
								break;
							}

							default:
							{
								break;
							}
						}
					}
				}
			}

			return ThreadRequestProcessResult::None;
		}


		void Thread::_SuspendExecution( InternalMutexLock& internalLock, const Nanoseconds& timeout )
		{
			this->SetExecutionStateFlag( internalLock, ThreadExecutionStateFlag_Suspended );

			if ( timeout.GetValue() == Timeout_Infinite )
			{
				this->_internalSyncObject.Wait( internalLock, [this]() -> bool {
					return !this->IsSuspended();
				} );
			}
			else
			{
				this->_internalSyncObject.WaitFor( internalLock, timeout, [this]() -> bool {
					return !this->IsSuspended();
				} );
			}
		}


		bool Thread::_RunSystemThread( ThreadObjectHandle<Thread> thread, stdx::mask<ThreadRunFlags> runFlags )
		{
			System::ThreadCreateInfo threadCreateInfo;
			threadCreateInfo.name = thread->_name.c_str();
			threadCreateInfo.createFlags = System::ThreadCreateFlag_Run_Detached;

			System::ThreadInterfaceSpec threadInterfaceSpec;
			threadInterfaceSpec.initProc = std::bind( _InitWrapper, thread, std::placeholders::_1 );
			threadInterfaceSpec.entryProc = std::bind( _ExecutionWrapper, thread );
			threadInterfaceSpec.releaseProc = std::bind( _ReleaseWrapper, thread );

			// Create system-level thread. Returned handle is stored only for checking - if we set it here
			// (thread->_sysHandle = ...), it could be possible, that thread would not see it, as we cannot
			// know how threads are scheduled (and this would be a race condition anyway). Instead, we save this
			// handle inside thread initialization wrapper, which receives that handle from the sys-level caller.

			auto result = System::ThreadAPI::CreateThread( thread->_concrtSharedState->systemSystemSessionState, threadCreateInfo, std::move( threadInterfaceSpec ) );

			if ( result.IsErrorSet() )
			{
				// note: may be an internal error or exception (result.IsExceptionSet()).
				return false;
			}

			// log info about created thread?

			return true;
		}


		bool Thread::_InitWrapper( ThreadObjectHandle<Thread> thread, System::ThreadHandle systemThreadHandle )
		{
			// Assignment of the handle is performed here to avoid data races - if this assignment was done
			// in the parent thread, there could be a situation, where thread does not have its own handle set.
			thread->_sysThreadHandle = systemThreadHandle;

			try
			{
				thread->_CreateDynamicThreadLocalStorage();

				thread->_Initialize();

				thread->_Register();
			}
			catch ( const ThreadInitializationErrorException& )
			{
				return false;
			}
			catch ( ... )
			{
				return false;
			}

			return true;
		}


		void Thread::_ExecutionWrapper( ThreadObjectHandle<Thread> thread )
		{
			try
			{
				thread->Entry();
			}
			catch ( const ThreadInterruptException& exception )
			{
				( exception );
			}
			catch ( ... )
			{

			}
		}


		void Thread::_ReleaseWrapper( ThreadObjectHandle<Thread> thread )
		{
			// Wait for registered child threads to complete.
			thread->_SyncWithChildThreads();

			thread->_Unregister();

			thread->_Release();

			thread->_DestroyDynamicThreadLocalStorage();
		}


	}
}
