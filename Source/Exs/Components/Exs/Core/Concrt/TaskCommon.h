
#ifndef __Exs_Core_TaskCommon_H__
#define __Exs_Core_TaskCommon_H__

#include "AsyncWaitCallback.h"
#include "ThreadCommon.h"


namespace Exs
{
	namespace Concrt
	{


		class TaskBase;
		class TaskGroup;
		class TaskExecutorThread;


		//
		using Task_executor_index_t = Thread_index_t;

		//
		using TaskAsyncCallback = AsyncWaitCallback<void()>;

		//
		using TaskAsyncResult = TaskAsyncCallback::AsyncResultType;

		//
		using TaskExecutorThreadHandle = ThreadObjectHandle<TaskExecutorThread>;


		enum class TaskIntensityClass : Uint32
		{
			Class1 = 1,
			Class2 = 2,
			Class3 = 4,
			Class4 = 6,
			Class5 = 10,
			Class6 = 14,

			Max = Class6,

			Min = Class1
		};


		enum class TaskPriorityLevel : Uint32
		{
			Level1 = 10,
			Level2 = 30,
			Level3 = 50,
			Level4 = 80,
			Level5 = 100,

			Max = Level5,

			Min = Level1,

			Default = Level3
		};


		struct TaskExecutorState
		{
		public:
			enum StateFlags : Enum
			{
				State_Registered = 0x8000,

				State_Active = 0x0001
			};

		public:
			//
			TaskExecutorThreadHandle threadHandle;

			//
			Thread_ref_id_t refID = Thread_Ref_ID_None;

			//
			Task_executor_index_t index = Thread_Index_Invalid;

			//
			stdx::atomic_mask<StateFlags> executionState = ATOMIC_VAR_INIT( 0 );
		};


	}
}


#endif /* __Exs_Core_TaskCommon_H__ */
