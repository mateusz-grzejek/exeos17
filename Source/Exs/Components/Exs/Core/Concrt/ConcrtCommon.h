
#ifndef __Exs_Core_ConcrtCommon_H__
#define __Exs_Core_ConcrtCommon_H__

#include "../Core.h"
#include <ExsLib/CoreUtils/Chrono.h>
#include <ExsLib/CoreUtils/Mutex.h>
#include <ExsLib/CoreUtils/SharedMutex.h>


namespace Exs
{
	namespace Config
	{

		enum : Size_t
		{
			/**
			 *
			 */
			CCRT_CFG_Thread_Specific_Data_Alignment = EXS_HW_DESTRUCTIVE_CACHE_INTERFERENCE_SIZE,

			/**
			 *
			 */
			CCRT_THR_Max_Active_Threads_Num = 128,

			/**
			 *
			 */
			CCRT_THR_System_Ref_ID_Base_Value = 0x8000,

			/**
			 *
			 */
			CCRT_THR_Max_Thread_Pool_Size = 8,

			/**
			 *
			 */
			CCRT_TLS_Max_User_Data_Entries_Num = 64,

			/**
			 *
			 */
			CCRT_TLS_Max_User_Release_Callbacks_Num = 72,

			/**
			 *
			 */
			CCRT_TLS_Init_Validation_Control_Key = 0xABCDEF77,

			/**
			 *
			 */
			CCRT_TSK_Default_Intensity_Class_Heuristic_Weight = 35,

			/**
			 *
			 */
			CCRT_TSK_Default_Priority_Level_Heuristic_Weight = 65,
		};

	}
}


namespace Exs
{
	namespace Concrt
	{


		class Thread;
		class ThreadSystemManager;

		ExsDeclareSharedStateHandle( ConcrtSharedState );


		/// Stores index assigned to a thread in the thread registry.
		typedef Uint16 Thread_index_t;

		/// Type which stores thread ref ID - custom, user-defined value used to identify thread objects.
		/// It serves as an addition to runtime-generated UIDs, and may be used everywhere where ThreadUID
		/// is permitted. Useful if threads are to be identified using compile-time constant known in advance.
		typedef Uint16 Thread_ref_id_t;

		/// Represents value of unique ID of a thread object, generated at runtime (during thread initialization).
		typedef U64ID Thread_uid_value_t;


		/// Special values for ref ID.
		enum : Thread_ref_id_t
		{
			//
			Thread_Ref_ID_None = 0,

			//
			Thread_Ref_ID_Auto = stdx::limits<Thread_ref_id_t>::max_value
		};


		/// Special values for ThreadUID.
		enum : Thread_uid_value_t
		{
			// Empty UID which has not been set. Used in addressing means 'all threads'.
			Thread_UID_None = 0,

			// Invalid UID. Represents uninitialized or invalidated thread.
			Thread_UID_Invalid = stdx::limits<Thread_uid_value_t>::max_value
		};


		enum : Thread_index_t
		{
			Thread_Index_Invalid = stdx::limits<Thread_index_t>::max_value,
		};


		///<summary>
		///</summary>
		union ThreadUID
		{
		public:
			struct alignas( sizeof( Thread_uid_value_t ) ) InternalData
			{
				//
				Uint32 thrObjectPtr = 0xCDCDCDCD;

				// Additional 'ref id', customizable on thread creation - 16 bits.
				Thread_ref_id_t refID = 0;

				// Global thread index inside registry - 16 bits.
				Thread_index_t index = Thread_Index_Invalid;
			};

			// Internal data of the UID. Do not access directly!
			InternalData internalData;

			// Numerical value of the UID.
			Thread_uid_value_t value;

		public:
			ThreadUID()
				: internalData()
			{ }

			ThreadUID( Thread_uid_value_t uidValue )
				: value( uidValue )
			{ }

			ThreadUID( Thread_ref_id_t refID, Thread_index_t index )
				: internalData()
			{
				this->internalData.refID = refID;
				this->internalData.index = index;
			}

			ThreadUID( const ThreadUID& origin )
				: value( origin.value )
			{ }

			explicit ThreadUID( const InternalData& internalDataData )
				: internalData( internalDataData )
			{ }

			ThreadUID& operator=( const ThreadUID& rhs )
			{
				this->value = rhs.value;
				return *this;
			}

			ThreadUID& operator=( Thread_uid_value_t rhs )
			{
				this->value = rhs;
				return *this;
			}

			operator Thread_uid_value_t() const
			{
				return this->value;
			}

			Thread_index_t GetIndex() const
			{
				return this->internalData.index;
			}

			Thread_ref_id_t GetRefID() const
			{
				return this->internalData.refID;
			}
		};


		static_assert( sizeof( ThreadUID ) == sizeof( Thread_uid_value_t ), "" );


	}

}


#endif /* __Exs_Core_ConcrtCommon_H__ */
