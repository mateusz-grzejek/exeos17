
#include "Precomp.h"
#include <Exs/Core/Concrt/ThreadSystemManager.h>
#include <Exs/Core/Concrt/Thread.h>
#include <Exs/Core/Concrt/ThreadMessageTransceiver.h>



namespace Exs
{
	namespace Concrt
	{


		ThreadSystemManager::ThreadSystemManager( ConcrtSharedStateHandle concrtSharedState )
			: _concrtSharedState( concrtSharedState )
			, _registry( concrtSharedState, *this )
			, _syncController( concrtSharedState, *this )
			, _systemRefIDValue{ ATOMIC_VAR_INIT( Config::CCRT_THR_System_Ref_ID_Base_Value ) }
		{ }


		ThreadSystemManager::~ThreadSystemManager()
		{ }


		bool ThreadSystemManager::ValidateThreadCreateRequest() const
		{
			auto& currentThread = CurrentThread::GetThreadObject();
			const auto& permissions = currentThread.GetPermissions();

			return permissions.is_set( ThreadPermission_Create_Thread );
		}


		ThreadUID ThreadSystemManager::QueryThreadUID( Thread_ref_id_t threadRefID ) const
		{
			ExsBeginSharedCriticalSectionWithSharedAccess( this->_registry.GetLock() );
			{
				return this->_registry.QueryThreadUID( threadRefID );
			}
			ExsEndCriticalSection();
		}


		Thread& ThreadSystemManager::GetCurrentThread() const
		{
			return CurrentThread::GetThreadObject();
		}


		ThreadSharedSyncContext* ThreadSystemManager::GetThreadSyncContext( Thread& thread )
		{
			auto& threadPersistentData = this->_GetThreadPersistentData( thread );

			return &( threadPersistentData.sharedSyncContext );
		}


		ThreadUID ThreadSystemManager::CreateThreadState( Thread& thread )
		{
			auto threadPersistentDataHandle = this->_AcquireThreadData();

			if ( !threadPersistentDataHandle )
			{
				return Thread_UID_Invalid;
			}

			auto threadRefID = thread.GetRefID();
			auto threadIndex = threadPersistentDataHandle->index;
			auto threadUID = Internal::CreateThreadUID( &thread, threadIndex, threadRefID );

			threadPersistentDataHandle->threadUID = threadUID;
			threadPersistentDataHandle->SetThread( thread );

			return threadUID;
		}


		void ThreadSystemManager::ReleaseThreadState( Thread& thread )
		{
			auto& threadPersistentData = this->_GetThreadPersistentData( thread );

			if ( threadPersistentData.CheckThread( thread ) )
			{
				this->_ReleaseThreadData( threadPersistentData );
			}
		}


		void ThreadSystemManager::RegisterThread( Thread& thread, const ThreadSystemRegInfo& systemRegInfo )
		{
			auto& threadPersistentData = this->_GetThreadPersistentData( thread );

			//
			this->_registry.RegisterThread( thread, systemRegInfo );

			//
			this->_syncController.RegisterThreadSharedSyncContext( thread, threadPersistentData.sharedSyncContext );

		}


		void ThreadSystemManager::UnregisterThread( Thread& thread )
		{
			auto& threadPersistentData = this->_GetThreadPersistentData( thread );

			//
			this->_registry.UnregisterThread( thread );

			//
			this->_syncController.UnregisterThreadSharedSyncContext( thread );

			this->_ReleaseThreadData( threadPersistentData );
		}


		ThreadCoreSystemData ThreadSystemManager::CreateThreadRuntimeObjects( Thread& thread )
		{
			auto& threadPersistentData = this->_GetThreadPersistentData( thread );

			auto& threadPermissions = thread.GetPermissions();

			//
			this->_CreateRuntimeObjects( thread, threadPersistentData, threadPermissions );

			ThreadCoreSystemData coreSystemData;
			coreSystemData.threadObject = &thread;
			coreSystemData.sharedSyncContext = &( threadPersistentData.sharedSyncContext );
			coreSystemData.childRefContainer = threadPersistentData.childRefContainer.get();
			coreSystemData.messageTransceiver = threadPersistentData.messageTransceiver.get();
			coreSystemData.messageController = threadPersistentData.messageTransceiver->GetController();

			return coreSystemData;
		}


		void ThreadSystemManager::DestroyThreadRuntimeObjects( Thread& thread )
		{
			auto& threadPersistentData = this->_GetThreadPersistentData( thread );

			//
			this->_DestroyRuntimeObjects( thread, threadPersistentData );
		}


		ThreadSystemManager::ThreadPersistentData& ThreadSystemManager::_GetThreadPersistentData( Thread& thread )
		{
			auto threadIndex = thread.GetIndex();
			auto& threadPersistentData = this->_threadPersistentDataIndex[threadIndex];

			ExsDebugAssert( threadPersistentData.CheckThread( thread ) );

			return threadPersistentData;
		}


		ThreadSystemManager::ThreadPersistentDataHandle ThreadSystemManager::_AcquireThreadData()
		{
			// Increment control registration counter. Counter has a limit set to max number of threads
			// allowed to run in the system. If this value is reached, new threads cannot be created.
			auto regCounterValue = this->_thrActiveCounter.increment();

			if ( regCounterValue == maxThreadsNum )
			{
				// No new threads may be created - limit already reached.
				return nullptr;
			}

			// Reserve persistent data for the thread.
			auto threadPersistentDataHandle = this->_threadPersistentDataIndex.acquire();

			// Allocation should never fail if reg counter was incremented successfuly.
			ExsDebugAssert( threadPersistentDataHandle );

			threadPersistentDataHandle->index = truncate_cast<Thread_index_t>( threadPersistentDataHandle.get_key() );

			return threadPersistentDataHandle;
		}


		void ThreadSystemManager::_ReleaseThreadData( ThreadPersistentData& threadPersistentData )
		{
			this->_threadPersistentDataIndex.release( threadPersistentData.index );

			threadPersistentData.index = Thread_Index_Invalid;

			this->_thrActiveCounter.decrement();
		}


		void ThreadSystemManager::_CreateRuntimeObjects( Thread& thread, ThreadPersistentData& threadPersistentData, stdx::mask<ThreadPermissionFlags> permissions )
		{
			if ( permissions.is_set( ThreadPermission_Create_Thread ) )
			{
				//
				threadPersistentData.childRefContainer = std::make_unique<ThreadChildRefContainer>( thread );
			}

			if ( permissions.is_set_any_of( ThreadPermission_Msg_Receive | ThreadPermission_Msg_Send ) )
			{
				auto* coreMessageSystemRoot = this->_concrtSharedState->GetCoreMessageSystemRootPtr();

				bool initReceiver = permissions.is_set( ThreadPermission_Msg_Receive );
				bool initSender = permissions.is_set( ThreadPermission_Msg_Send );

				threadPersistentData.messageTransceiver = std::make_unique<ThreadMessageTransceiver>( *coreMessageSystemRoot, thread, thread.GetUID() );

				threadPersistentData.messageTransceiver->Initialize( initReceiver, initSender );

				threadPersistentData.messageTransceiver->Register();
			}
		}


		void ThreadSystemManager::_DestroyRuntimeObjects( Thread& thread, ThreadPersistentData& threadPersistentData )
		{
			if ( threadPersistentData.childRefContainer )
			{
				threadPersistentData.childRefContainer.reset();
			}

			if ( threadPersistentData.messageTransceiver )
			{
				threadPersistentData.messageTransceiver.reset();
			}
		}


	}
}
