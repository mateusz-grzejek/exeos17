
#ifndef __Exs_Core_SyncInternal_H__
#define __Exs_Core_SyncInternal_H__

#include "ThreadSyncCommon.h"


namespace Exs
{
	namespace Concrt
	{


		struct ThreadSharedSyncContext;
		struct ThreadWaitSyncState;

		class SyncObject;

		/**
		 *
		 */
		using SyncObjectWaitingThreadList = std::list<ThreadWaitSyncState*>;

		/**
		 *
		 */
		using SyncObjectWaitingThreadRef = SyncObjectWaitingThreadList::iterator;


		/**
		 * @brief Internal state flags.
		 */
		enum SyncObjectSignalFlags : Enum
		{
			// Indicates that SO was signaled (used to detect spurious wakeups).
			SyncObjectSignalFlag_Control_Bit = 0x0001,

			// Indicates that SO was explicitly notified.
			SyncObjectSignalFlag_Notify = 0x0010 | SyncObjectSignalFlag_Control_Bit,

			// Indicates that SO was interrupted.
			SyncObjectSignalFlag_Interrupt = 0x0020 | SyncObjectSignalFlag_Control_Bit,
		};


		/**
		 * @brief Contains state used by SyncObjects during waiting process.
		 */
		struct ThreadWaitSyncState
		{
		public:
			// Pointer to the sync object which owns this context.
			SyncObject* syncObject = nullptr;

			// Thread object which is synchronized with this context.
			Thread* thread = nullptr;

			// Sync context of the synchronized thread.
			ThreadSharedSyncContext* threadSharedSyncContext = nullptr;

			// Ref (iterator) to the thread's entry on the list of threads waiting for the sync object.
			SyncObjectWaitingThreadRef waitingListRef;

		private:
			// Internal flags
			stdx::atomic_mask<Enum> _internalStateMask = ATOMIC_VAR_INIT( 0 );

		public:
			//
			void SetStateFlags( stdx::mask<Enum> flags )
			{
				this->_internalStateMask.set( flags );
			}

			//
			bool IsStateFlagSet( stdx::mask<Enum> flags ) const
			{
				return this->_internalStateMask.is_set( flags );
			}

			//
			bool IsInterruptFlagSet() const
			{
				return this->_internalStateMask.is_set( SyncObjectSignalFlag_Interrupt );
			}

			//
			bool IsNotifyFlagSet() const
			{
				return this->_internalStateMask.is_set( SyncObjectSignalFlag_Notify );
			}

			//
			bool IsSignalFlagSet() const
			{
				return this->_internalStateMask.is_set( SyncObjectSignalFlag_Control_Bit );
			}
		};


	}
}


#endif /* __Exs_Core_SyncInternal_H__ */
