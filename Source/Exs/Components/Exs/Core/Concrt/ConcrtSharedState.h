
#ifndef __Exs_Core_ConcrtSharedState_H__
#define __Exs_Core_ConcrtSharedState_H__

#include "ConcrtCommon.h"
#include <ExsLib/System/System.h>


namespace Exs
{
	namespace Concrt
	{


		ExsDeclareRefPtrClass( CoreMessageSystemRoot );
		ExsDeclareRefPtrClass( ThreadSystemManager );


		struct ConcrtSharedStateInitInfo
		{
			System::SystemSessionStateHandle systemSystemSessionState;
		};


		struct ConcrtSharedState : public SharedState<void>
		{
		public:
			System::SystemSessionStateHandle systemSystemSessionState;

			CoreMessageSystemRootRefPtr coreMessageSystemRoot;

			ThreadSystemManagerRefPtr threadSystemManager;

		public:
			CoreMessageSystemRoot& GetCoreMessageSystemRoot() const
			{
				return *( this->coreMessageSystemRoot );
			}

			CoreMessageSystemRoot* GetCoreMessageSystemRootPtr() const
			{
				return this->coreMessageSystemRoot.get();
			}

			ThreadSystemManager& GetThreadSystemManager() const
			{
				return *( this->threadSystemManager );
			}

			ThreadSystemManager* GetThreadSystemManagerPtr() const
			{
				return this->threadSystemManager.get();
			}
		};


		EXS_LIBAPI_CORE ConcrtSharedStateHandle CreateConcrtSharedState( const ConcrtSharedStateInitInfo& initInfo );


	}
}


#endif /* __Exs_Core_ConcrtSharedState_H__ */
