
#ifndef __Exs_Core_ThreadMessageProxyQueue_H__
#define __Exs_Core_ThreadMessageProxyQueue_H__

#include "CoreMessageQueue.h"


namespace Exs
{
	namespace Concrt
	{


		class ThreadMessageTransceiver;


		class EXS_LIBCLASS_CORE ThreadMessageProxyQueue
		{
			friend class ThreadMessageTransceiver;

		private:
			ThreadMessageProxyQueue( ThreadMessageTransceiver* transceiver, U32ID queueID );
			~ThreadMessageProxyQueue();

		public:
			void Release();

			/// @ref ThreadMessageTransceiver::PeekMessage
			CoreMessageHandle PeekMessage();

			/// @ref ThreadMessageTransceiver::WaitForMessage
			CoreMessageHandle WaitForMessage();

			/// @ref ThreadMessageTransceiver::WaitForMessage(timeout)
			CoreMessageHandle WaitForMessage( const Milliseconds& timeout );

			/// @ref ThreadMessageTransceiver::AddThreadMessageFilter
			bool AddFilter( MessageFilter messageFilter, bool overwrite = false );

			/// @ref ThreadMessageTransceiver::RemoveThreadMessageFilter
			/// Sets specified filter as inactive. Effectively removes itself from the receiver as queue. If current queue is not set
			/// as the active queue for the specified filter, an exception is thrown.
			void RemoveFilter( MessageFilter messageFilter );

			/// Returns id of the queue.
			U32ID GetID() const;

			///
			bool IsActive() const;

		friendapi:
			// @ ThreadMessageTransceiver
			// Returns internal message queue. 
			CoreMessageQueue* GetMessageQueue();

		private:
			// Pointer to the parent transceiver.
			ThreadMessageTransceiver* _transceiver;

			// ID of the queue.
			U32ID _id;

			// Internal queue with messages.
			CoreMessageQueue _messageQueue;
		};


		inline U32ID ThreadMessageProxyQueue::GetID() const
		{
			return this->_id;
		}

		inline CoreMessageQueue* ThreadMessageProxyQueue::GetMessageQueue()
		{
			return &( this->_messageQueue );
		}


	}
}


#endif /* __Exs_Core_ThreadMessageProxyQueue_H__ */
