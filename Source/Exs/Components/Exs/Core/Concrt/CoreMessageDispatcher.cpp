
#include "Precomp.h"
#include <Exs/Core/Concrt/CoreMessageDispatcher.h>
#include <Exs/Core/Concrt/CoreMessageReceiver.h>
#include <ExsLib/CoreUtils/Exception.h>


namespace Exs
{
	namespace Concrt
	{


		CoreMessageDispatcher::CoreMessageDispatcher( CoreMessageSystemRoot* messageSystemRoot )
			: _messageSystemRoot( messageSystemRoot )
		{ }


		Result CoreMessageDispatcher::RegisterReceiver( CoreMessageReceiverID receiverID, Uint32 receiverIndex, CoreMessageReceiver& receiver )
		{
			ExsBeginSharedCriticalSectionWithUniqueAccess( this->_sharedMutex );
			{
				//
				auto& threadReceiverState = this->_receiverArray[receiverIndex];

				if ( ( threadReceiverState.receiverID != Core_Message_Receiver_ID_None ) || ( threadReceiverState.receiver != nullptr ) )
				{
					return ExsResult( RSC_Err_Invalid_State );
				}

				threadReceiverState.receiver = &receiver;
				threadReceiverState.receiverID = receiverID;

				this->_FetchTemporaryMessageQueue( receiverID, receiver );

				this->_receiverIndexMap[receiverID] = receiverIndex;
			}
			ExsEndCriticalSection();

			return ExsResult( RSC_Success );
		}


		void CoreMessageDispatcher::UnregisterReceiver( CoreMessageReceiverID receiverID, CoreMessageReceiver* receiver )
		{
			ExsBeginSharedCriticalSectionWithUniqueAccess( this->_sharedMutex );
			{
				//
				auto receiverIndexRef = this->_receiverIndexMap.find( receiverID );

				if ( receiverIndexRef == this->_receiverIndexMap.end() )
				{
					throw 0;
				}

				//
				auto& receiverState = this->_receiverArray[receiverIndexRef->value];

				if ( ( receiverState.receiverID == Core_Message_Receiver_ID_None ) || ( receiverState.receiver == nullptr ) )
				{
					ExsThrowException( EXC_Invalid_Operation );
				}

				if ( ( receiverState.receiverID != receiverID ) || ( ( receiver != nullptr ) && ( receiver != receiverState.receiver ) ) )
				{
					ExsThrowException( EXC_Invalid_State );
				}

				receiverState.receiver = nullptr;
				receiverState.receiverID = Core_Message_Receiver_ID_None;

				this->_receiverIndexMap.erase( receiverIndexRef );
			}
			ExsEndCriticalSection();
		}


		void CoreMessageDispatcher::ActivateReceiver( CoreMessageReceiverID receiverID )
		{

		}


		void CoreMessageDispatcher::DeactivateReceiver( CoreMessageReceiverID receiverID )
		{

		}


		Result CoreMessageDispatcher::DispatchMessage( const CoreMessageDispatchRequest& dispatchRequest, const CoreMessageHandle& message )
		{
			ExsBeginSharedCriticalSectionWithSharedAccess( this->_sharedMutex );
			{
				// If immediate flag is set, message is discarded if receiver is not registered or not active.
				bool isImmediateMessage = dispatchRequest.sendOptions.is_set( CoreMessageSendOption_Immediate );

				//
				auto receiverIndex = this->_GetReceiverIndex( dispatchRequest.receiverID );

				if ( receiverIndex != invalidReceiverIndex )
				{
					return this->_DispatchReg( receiverIndex, message, isImmediateMessage );
				}
				else
				{
					return this->_DispatchUnreg( dispatchRequest.receiverID, message, isImmediateMessage );
				}
			}
			ExsEndCriticalSection();
		}


		Result CoreMessageDispatcher::_DispatchReg( Uint32 receiverIndex, const CoreMessageHandle& message, bool isImmediateMessage )
		{
			auto& receiverState = this->_receiverArray[receiverIndex];

			bool receiverActive = !receiverState.stateFlags.is_set( ReceiverState_Inactive, std::memory_order_relaxed );

			if ( receiverActive )
			{
				return receiverState.receiver->PostMessage( message );
			}
			else
			{
				if ( !isImmediateMessage )
				{
					ExsBeginCriticalSection( receiverState.inactiveMessageQueueMutex );
					{
						receiverState.inactiveMessageQueue.push_back( message );
					}
					ExsEndCriticalSection();

					return ExsResult( RSC_Msg_Message_Suspended );
				}
				else
				{
					return ExsResult( RSC_Msg_Receiver_Inactive );
				}
			}
		}


		Result CoreMessageDispatcher::_DispatchUnreg( CoreMessageReceiverID receiverID, const CoreMessageHandle& message, bool isImmediateMessage )
		{
			if ( isImmediateMessage )
			{
				return ExsResult( RSC_Msg_Message_Discarded );
			}

			ExsBeginCriticalSection( this->_temporaryReceiverMessageQueueListMutex );
			{
				auto& queue = this->_temporaryReceiverMessageQueueList[receiverID];
				queue.push_back( message );
			}
			ExsEndCriticalSection();

			return ExsResult( RSC_Success );
		}


		void CoreMessageDispatcher::_FetchTemporaryMessageQueue( CoreMessageReceiverID receiverID, CoreMessageReceiver& receiver )
		{
			auto temporaryQueueRef = this->_temporaryReceiverMessageQueueList.find( receiverID );

			if ( temporaryQueueRef != this->_temporaryReceiverMessageQueueList.end() )
			{
				for ( auto& message : temporaryQueueRef->value )
				{
					receiver.PushMessage( message );
				}

				this->_temporaryReceiverMessageQueueList.erase( temporaryQueueRef );
			}
		}


		Uint32 CoreMessageDispatcher::_GetReceiverIndex( CoreMessageReceiverID receiverID )
		{
			auto receiverIndexRef = this->_receiverIndexMap.find( receiverID );
			return ( receiverIndexRef != this->_receiverIndexMap.end() ) ? receiverIndexRef->value : invalidReceiverIndex;
		}


	}
}
