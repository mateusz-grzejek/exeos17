
#ifndef __Exs_Core_AsyncPackagedThread_H__
#define __Exs_Core_AsyncPackagedThread_H__

#include <Concrt/WorkerThreadCommon.h>
#include "Thread.h"


namespace Exs
{
	namespace Concrt
	{


#define EXS_WORKER_THREAD_CTOR_PARAMS_DECL \
	EXS_THREAD_CTOR_PARAMS_DECL, WorkerThreadContainer& container, Worker_thread_index_t index

#define EXS_WORKER_THREAD_CTOR_PARAMS_DEF \
	EXS_THREAD_CTOR_PARAMS_DEF, container, index


		///<summary>
		///</summary>
		class WorkerThread : public Thread
		{
		EXS_DECLARE_NONCOPYABLE( WorkerThread );

		public:
			WorkerThread( EXS_THREAD_CTOR_PARAMS_DECL, WorkerThreadContainer& container, Worker_thread_index_t workerIndex );

			virtual ~WorkerThread() = default;

			///
			Worker_thread_index_t GetWorkerIndex() const;

		protected:
			//
			virtual ThreadUpdateResult Update() override;

			//
			virtual bool OnInit( const ThreadInitContext& initContext ) override;

			//
			virtual void OnRelease( const ThreadReleaseContext& releaseContext ) noexcept override;

			//
			void SetWorkerStatusFlags( stdx::mask<Worker_thread_status_value_t> flags );

			//
			void UnsetWorkerStatusFlags( stdx::mask<Worker_thread_status_value_t> flags );

			//
			bool CheckWorkerStatus( stdx::mask<Worker_thread_status_value_t> flags ) const;

			//
			bool CheckWorkerStatusAny( stdx::mask<Worker_thread_status_value_t> flags ) const;

		private:
			//
			WorkerThreadContainer* _container;

			//
			Worker_thread_index_t _workerIndex;
		};


		inline Worker_thread_index_t WorkerThread::GetWorkerIndex() const
		{
			return this->_workerIndex;
		}


	}
}


#endif /* __Exs_Core_AsyncPackagedThread_H__ */
