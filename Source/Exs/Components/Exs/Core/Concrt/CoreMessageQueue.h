
#ifndef __Exs_Core_CoreMessageQueue_H__
#define __Exs_Core_CoreMessageQueue_H__

#include "CoreMessageBase.h"
#include <queue>


namespace Exs
{
	namespace Concrt
	{


		///<summary>
		///</summary>
		class CoreMessageQueue
		{
		public:
			using InternalQueueType = std::queue< CoreMessageHandle, std::deque<CoreMessageHandle> >;

		private:
			InternalQueueType _messages; // Internal queue which stores enqueued messages.

		public:
			CoreMessageQueue( const CoreMessageQueue & ) = delete;
			CoreMessageQueue& operator=( const CoreMessageQueue & ) = delete;

			CoreMessageQueue() = default;

			///<summary>
			///</summary>
			void Push( CoreMessageHandle&& message )
			{
				this->_messages.push( std::move( message ) );
			}

			///<summary>
			///</summary>
			void Push( const CoreMessageHandle& message )
			{
				this->_messages.push( message );
			}

			///<summary>
			///</summary>
			void Pop()
			{
				ExsDebugAssert( !this->IsEmpty() );
				this->_messages.pop();
			}

			///<summary>
			///</summary>
			CoreMessageHandle Fetch()
			{
				CoreMessageHandle message{};

				if ( !this->IsEmpty() )
				{
					message = this->_messages.front();
					this->_messages.pop();
				}

				return message;
			}

			///<summary>
			///</summary>
			bool Fetch( CoreMessageHandle* outputCoreMessageHandle )
			{
				bool result = false;

				if ( !this->IsEmpty() )
				{
					*outputCoreMessageHandle = this->_messages.front();
					this->_messages.pop();
					result = true;
				}

				return result;
			}

			///<summary>
			///</summary>
			CoreMessageHandle FetchUnchecked()
			{
				CoreMessageHandle message = this->_messages.front();
				this->_messages.pop();
				return message;
			}

			///<summary>
			///</summary>
			void FetchUnchecked( CoreMessageHandle* outputCoreMessageHandle )
			{
				*outputCoreMessageHandle = this->_messages.front();
				this->_messages.pop();
			}

			///<summary>
			///</summary>
			const CoreMessageHandle& Front() const
			{
				return this->_messages.front();
			}

			///<summary>
			///</summary>
			Size_t Size() const
			{
				return this->_messages.size();
			}

			///<summary>
			///</summary>
			bool IsEmpty() const
			{
				return this->_messages.empty();
			}
		};


	}
}


#endif /* __Exs_Core_CoreCoreMessageQueue_H__ */
