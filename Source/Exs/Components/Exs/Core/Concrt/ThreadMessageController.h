
#ifndef __Exs_Core_ThreadMessageController_H__
#define __Exs_Core_ThreadMessageController_H__

#include "CoreMessageBase.h"


namespace Exs
{
	namespace Concrt
	{


		class ThreadMessageProxyQueue;
		class ThreadMessageTransceiver;


		struct ThreadProxyQueueCreateInfo
		{
			U32ID queueID = ID32_Auto;
		};


		class EXS_LIBCLASS_CORE ThreadMessageController
		{
		public:
			ThreadMessageController( ThreadMessageTransceiver* transceiver );

			~ThreadMessageController();

			/// Creates new proxy queue. Queue represents the type of the queue to be created. It must be either 
			/// ThreadMessageProxyQueue (default) or its subclass. Lifetime of the queue is controlled by the
			/// messaging system (see
			ThreadMessageProxyQueue* CreateProxyQueue( U32ID queueID );

			///
			ThreadMessageProxyQueue* CreateProxyQueue( const ThreadProxyQueueCreateInfo& createInfo );

			//
			bool AddThreadMessageFilter( MessageFilter messageFilter, ThreadMessageProxyQueue* proxyQueue, bool overwrite );

			// 
			void RemoveThreadMessageFilter( MessageFilter messageFilter, ThreadMessageProxyQueue* proxyQueue );

			CoreMessageHandle PeekMessage();

			CoreMessageHandle WaitForMessage();

			CoreMessageHandle WaitForMessage( const Milliseconds& timeout );

			Result SendMessage( ThreadUID threadUID, const CoreMessageHandle& message );

			Result SendMessage( Thread_ref_id_t threadRefID, const CoreMessageHandle& message );

			Result SendMessage( const CoreMessageSendRequest& sendRequest, const CoreMessageHandle& message );

			CoreSyncMessageSendResult SendMessage( const CoreSyncMessageSendRequest& sendRequest, const CoreMessageHandle& message );

			Result SendSimpleMessage( ThreadUID threadUID, Message_code_t messageCode, MessagePriority priority = MessagePriority::Default );

			Result SendSimpleMessage( Thread_ref_id_t threadRefID, Message_code_t messageCode, MessagePriority priority = MessagePriority::Default );

			// Waits for response for the most recent sync message.
			CoreMessageResponse* WaitForResponse();

			// Waits for response for the most recent sync message.
			CoreMessageResponse* WaitForResponse( CoreMessage& message );

			CoreMessageResponse* GetResponse();

			CoreMessageResponse* GetResponse( CoreMessage& message );

		private:
			//
			ThreadMessageTransceiver* _transceiver;
		};


	}
}


#endif /* __Exs_Core_ThreadMessageController_H__ */
