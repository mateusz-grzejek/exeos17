
#ifndef __Exs_Core_ExclusiveSyncObject_H__
#define __Exs_Core_ExclusiveSyncObject_H__

#include "SyncObjectImpl.h"


namespace Exs
{
	namespace Concrt
	{


		/**
		 *
		 */
		class ExclusiveSyncObjectImpl : public SyncObjectImplBase
		{
		protected:
			/// @see SyncObjectBase::SyncObjectBase( ThreadSystemManager& threadSystemManager )
			ExclusiveSyncObjectImpl()
			: _currentThread( nullptr )
			, _currentWaitState( nullptr )
			{ }

			bool OnWaitBeginImpl( ThreadWaitSyncState& threadWaitSyncState )
			{
				this->_currentThread = threadWaitSyncState.thread;
				this->_currentWaitState = &threadWaitSyncState;

				return true;
			}

			void OnWaitEndImpl( ThreadWaitSyncState& threadWaitSyncState )
			{
				this->_currentWaitState = nullptr;
				this->_currentThread = nullptr;
			}

			template <class TLock>
			Size_t AbortImpl( TLock& lock )
			{
				return this->_Signal( lock, SyncObjectSignalFlag_Notify );
			}

			template <class TLock>
			bool InterruptImpl( TLock& lock, Thread& thread )
			{
				return ( &thread == this->_currentThread ) && ( this->_Signal( lock, SyncObjectSignalFlag_Interrupt ) != 0 );
			}

			template <class TLock>
			bool NotifyOneImpl( TLock& lock )
			{
				return this->_Signal( lock, SyncObjectSignalFlag_Notify ) != 0;
			}

			template <class TLock>
			Size_t NotifyAllImpl( TLock& lock )
			{
				return this->_Signal( lock, SyncObjectSignalFlag_Notify );
			}

		private:
			template <class TLock>
			Size_t _Signal( TLock& lock, SyncObjectSignalFlags signalFlags )
			{
				if ( this->_currentWaitState != nullptr )
				{
					//
					SyncObjectImplBase::SignalThread<TLock>( *( this->_currentWaitState ), signalFlags );

					//
					return 1;
				}

				return 0;
			}

		private:
			//
			Thread* _currentThread;

			//
			ThreadWaitSyncState* _currentWaitState;
		};


		/**
		 *
		 */
		template <class TMutex = SyncMutex>
		class ExclusiveSyncObject : public SyncObjectImplProxy<TMutex, ExclusiveSyncObjectImpl>
		{
		public:
			using BaseImplProxyType = SyncObjectImplProxy<TMutex, ExclusiveSyncObjectImpl>;

		public:
			/**
			 *
			 * @param concrtSharedState
			 */
			ExclusiveSyncObject()
			: BaseImplProxyType( CurrentThread::GetThreadSystemManager() )
			{ }

			/**
			 *
			 */
			explicit ExclusiveSyncObject( ThreadSystemManager& threadSystemManager )
			: BaseImplProxyType( threadSystemManager )
			{ }

			/**
			 *
			 * @param concrtSharedState
			 */
			explicit ExclusiveSyncObject( const ConcrtSharedStateHandle& concrtSharedState )
			: BaseImplProxyType( concrtSharedState->GetThreadSystemManager() )
			{ }
		};


	}
}


#endif /* __Exs_Core_ExclusiveSyncObject_H__ */

