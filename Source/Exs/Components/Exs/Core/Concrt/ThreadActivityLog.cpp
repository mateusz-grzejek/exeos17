
#include "Precomp.h"
#include <Exs/Core/Concrt/ThreadActivityLog.h>


namespace Exs
{
	namespace Concrt
	{


		ThreadActivityLog::ThreadActivityLog()
		{ }


		ThreadActivityLog::~ThreadActivityLog()
		{ }


		void ThreadActivityLog::PostEvent( ThreadActivityEventInfo&& eventInfo )
		{
			Event event;
			event.eventCategoryID = eventInfo.eventCategory;
			event.eventCategoryName = _QueryEventCategoryName( eventInfo.eventCategory );
			event.result = eventInfo.result;
			event.threadObject = eventInfo.threadObject;

			auto eventRef = this->_events.insert( this->_events.end(), std::move( event ) );
			this->_OnEvent( *eventRef );
		}


		void ThreadActivityLog::PostEvent( const ThreadActivityEventInfo& eventInfo )
		{
			Event event;
			event.eventCategoryID = eventInfo.eventCategory;
			event.eventCategoryName = _QueryEventCategoryName( eventInfo.eventCategory );
			event.result = eventInfo.result;
			event.threadObject = eventInfo.threadObject;

			auto eventRef = this->_events.insert( this->_events.end(), std::move( event ) );
			this->_OnEvent( *eventRef );
		}


		void ThreadActivityLog::_OnEvent( const Event& event )
		{
			for ( auto& callback : this->_eventCallbacks )
			{
				callback( event );
			}
		}


		const char* ThreadActivityLog::_QueryEventCategoryName( ThreadActivityEventCategory eventCategory )
		{
			switch ( eventCategory )
			{
			case ThreadActivityEventCategory::Registration:
				return "Registration";

			case ThreadActivityEventCategory::Rstate_Acquire:
				return "Rstate_Acquire";

			case ThreadActivityEventCategory::Rstate_Release:
				return "Rstate_Release";

			case ThreadActivityEventCategory::Unregistration:
				return "Unregistration";

			default:
				return "Unknown";
			}
		}


	}
}
