
#ifndef __Exs_Core_ThreadPool_H__
#define __Exs_Core_ThreadPool_H__

#include "WorkerThreadContainer.h"
#include "ThreadPoolCommon.h"
#include "ThreadSyncGroup.h"


namespace Exs
{
	namespace Concrt
	{


		struct ThreadPoolCreateInfo
		{
		};


		enum class ThreadPoolReleaseMode : Enum
		{
			Abort,

			Continue,

			Synchronize,

			Default = Continue
		};


		///<summary>
		///</summary>
		class ThreadPoolWorkerThreadFactory : public WorkerThreadFactory
		{
		public:
			ThreadPoolWorkerThreadFactory( ConcrtSharedStateHandle concrtSharedState, ThreadPool& threadPool )
			: _concrtSharedState( concrtSharedState )
				, _threadPool( &threadPool )
			{
			}

			virtual ~ThreadPoolWorkerThreadFactory() = default;

		private:
			virtual WorkerThreadHandle CreateWorkerThread( EXS_WORKER_THREAD_FACTORY_PARAMS_DECL ) noexcept override final;

		private:
			//
			ConcrtSharedStateHandle _concrtSharedState;

			//
			ThreadPool* _threadPool;
		};


		///<summary>
		///</summary>
		class ThreadPoolSharedQueue
		{
		public:
			using InternalSyncObject = SharedSyncObject<>;
			using InternalMutex = InternalSyncObject::Mutex;
			using InternalMutexLock = InternalSyncObject::MutexLock;

		public:
			ThreadPoolAsyncResult Enqueue( InternalMutexLock& internalLock, ThreadPoolAsyncCallback&& callback )
			{
				this->_queue.push_back( std::move(callback) );

				auto& pushedCallback = this->_queue.back();
				auto asyncResult = pushedCallback.GetResult();

				this->_queueSize.fetch_add( 1, std::memory_order_relaxed );
				this->_internalSyncObject.NotifyOne( internalLock );

				return asyncResult;
			}

			ThreadPoolAsyncCallback FetchNext( InternalMutexLock& internalLock )
			{
				ThreadPoolAsyncCallback callback;

				if ( !this->_queue.empty() )
				{
					callback = std::move( this->_queue.front() );
					this->_queue.pop_front();
					this->_queueSize.fetch_sub( 1, std::memory_order_relaxed );
				}

				return std::move( callback );
			}

			ThreadPoolAsyncCallback WaitForCallback( InternalMutexLock& internalLock, const Nanoseconds& timeout )
			{
				if ( this->_queue.empty() )
				{
					auto& queue = this->_queue;
					this->_internalSyncObject.WaitFor( internalLock, timeout, [&queue]() -> bool
					{
						return !queue.empty();
					} );
				}

				return this->FetchNext( internalLock );
			}

			void InterruptWait( InternalMutexLock& internalLock )
			{
				this->_internalSyncObject.Abort();
			}

			void Clear()
			{

			}

			InternalMutex& GetInternalMutex() const
			{
				return this->_internalSyncObject.GetMutex();
			}

			bool IsEmpty() const
			{
				return this->Size() == 0;
			}

			Size_t Size() const
			{
				return this->_queueSize.load( std::memory_order_relaxed );
			}

		private:
			using InternalQueue = std::deque<ThreadPoolAsyncCallback>;

		private:
			//
			InternalSyncObject _internalSyncObject;

			//
			InternalQueue _queue;

			//
			std::atomic<Size_t> _queueSize;
		};


		///<summary>
		///</summary>
		class EXS_LIBCLASS_CORE ThreadPool
		{
			friend class ThreadPoolWorkerThread;

		public:
			using InternalMutex = LightMutex;
			using InternalMutexLock = std::unique_lock<LightMutex>;

			using SharedQueueMutex = ThreadPoolSharedQueue::InternalMutex;
			using SharedQueueMutexLock = ThreadPoolSharedQueue::InternalMutexLock;

		public:
			static constexpr Size_t maxSize = Config::CCRT_THR_Max_Thread_Pool_Size;

		public:
			ThreadPool( ConcrtSharedStateHandle concrtSharedState, std::string name, Size_t poolSize = 0 );
			~ThreadPool();

			template <class TCallable>
			ThreadPoolAsyncResult QueueWorkItem( TCallable callable );

			///
			void Init( Size_t workerThreadsNum );

			///
			void Release( ThreadPoolReleaseMode releaseMode = ThreadPoolReleaseMode::Default );

			///
			void WaitForWorkCompletion( const Nanoseconds& timeout = Timeout_Infinite );

			///
			Size_t GetActiveWorkersNum() const;

			///
			Size_t GetIdleWorkersNum() const;

			///
			Size_t GetSharedQueueSize() const;

			///
			Size_t Size() const
			{
				return this->_workerThreadContainer.Size();
			}

			/// Returns true if pool has not been released, i.e. it accepts incoming work items.
			bool IsActive() const;

			///
			bool IsSharedQueueEmpty() const
			{
				return this->_sharedQueue.IsEmpty();
			}

			///
			bool IsWorkerActive( Size_t workerIndex ) const
			{
				return false;
			}

		friendapi:
			// @ ThreadPoolWorkerThread
			// Called by worker thread when it is initialized.
			bool RegisterWorkerThread( ThreadPoolWorkerThread& workerThread );

			// @ ThreadPoolWorkerThread
			// Called by worker thread when it is released.
			void UnregisterWorkerThread( ThreadPoolWorkerThread& workerThread );

			// @ ThreadPoolWorkerThread
			// Called by worker thread when it is initialized.
			void OnCallbackExecutionBegin( ThreadPoolWorkerThread& workerThread );

			// @ ThreadPoolWorkerThread
			// Called by worker thread when it is released.
			void OnCallbackExecutionEnd( ThreadPoolWorkerThread& workerThread );

			// @ ThreadPoolWorkerThread
			// Blocks calling thread until there is a work item available in the work queue. Returns that item.
			// May return an empty callback (fore example, if pool has been released and queue is empty).
			ThreadPoolAsyncCallback WaitForCallback( ThreadPoolWorkerThread& workerThread );

			// @ ThreadPoolWorkerThread
			// Return true is Release() method for the pool has been called, or false otherwise.
			bool IsReleaseRequestSet() const;

		private:
			// Returns true if queue is empty or false otherwise. Not thread-safe, calls empty() method of the queue.
			bool _CheckWorkQueueEmpty();

			//
			ThreadPoolWorkerState& _GetWorkerState( const ThreadPoolWorkerThread& workerThread );

			//
			ThreadPoolAsyncResult _QueueCallback( ThreadPoolAsyncCallback&& callback );

			//
			void _Initialize( Size_t workerThreadsNum );

			//
			void _RunWorkerThreads( Size_t workerThreadsNum );

			//
			void _StopWorkerThreads();

			//
			bool _SetInitializedFlag();

			//
			bool _SetReleaseRequest();

			//
			void _WaitForWorkCompletion( const Nanoseconds& timeout );

		private:
			enum InternalStateFlags : Enum
			{
				InternalStateFlag_Initialized = 1,
				InternalStateFlag_Release_Request_Set = 2
			};

			//
			using WorkerContainer = WorkerThreadGenericContainer<ThreadPoolWorkerState>;

		private:
			//
			ConcrtSharedStateHandle _concrtSharedState;

			//
			ThreadPoolSharedQueue _sharedQueue;

			//
			ThreadPoolWorkerThreadFactory _workerFactory;

			//
			WorkerContainer _workerThreadContainer;

			//
			InternalMutex _internalMutex;

			//
			ThreadPoolIdleWorkerList _idleWorkerList;

			//
			std::atomic<Size_t> _idleThreadsNum;

			//
			stdx::atomic_mask<InternalStateFlags> _internalStateMask;
		};


		template <class TCallable>
		inline ThreadPoolAsyncResult ThreadPool::QueueWorkItem( TCallable callable )
		{
			return this->_QueueCallback( ThreadPoolAsyncCallback( std::forward<TCallable>( callable ) ) );
		}

		inline Size_t ThreadPool::GetActiveWorkersNum() const
		{
			return this->_workerThreadContainer.Size();
		}

		inline Size_t ThreadPool::GetIdleWorkersNum() const
		{
			return this->_idleThreadsNum.load( std::memory_order_relaxed );
		}

		inline Size_t ThreadPool::GetSharedQueueSize() const
		{
			return this->_sharedQueue.Size();
		}

		inline bool ThreadPool::IsActive() const
		{
			return !this->_internalStateMask.is_set( InternalStateFlag_Release_Request_Set );
		}

		inline bool ThreadPool::IsReleaseRequestSet() const
		{
			return !this->_internalStateMask.is_set( InternalStateFlag_Release_Request_Set );
		}

		inline bool ThreadPool::_CheckWorkQueueEmpty()
		{
			return this->_sharedQueue.IsEmpty();
		}


	}
}


#endif /* __Exs_Core_ThreadPool_H__ */
