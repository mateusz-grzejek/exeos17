
#ifndef __Exs_Core_Thread_H__
#define __Exs_Core_Thread_H__

#include "ThreadSyncObject.h"
#include "ThreadInternal.h"


namespace Exs
{
	namespace Concrt
	{


		struct ThreadProperties;
		struct ThreadInitContext;
		struct ThreadReleaseContext;


		enum class ThreadStateUpdatePolicy : Enum
		{
			Update_And_Notify,
			Update_Only,
			Default = Update_Only
		};


	#define EXS_THREAD_CTOR_PARAMS_DECL \
		ConcrtSharedStateHandle concrtSharedState, Thread* parent, const ThreadProperties& properties

	#define EXS_THREAD_CTOR_PARAMS_DEF \
		concrtSharedState, parent, properties

		///<summary>
		///</summary>
		class EXS_LIBCLASS_CORE Thread
		{
			EXS_DECLARE_NONCOPYABLE( Thread );

		public:
			using InternalSyncObjectType = ThreadSyncObject<>;
			using InternalMutex = InternalSyncObjectType::Mutex;
			using InternalMutexLock = InternalSyncObjectType::MutexLock;

		public:
			Thread( ConcrtSharedStateHandle concrtSharedState, Thread* parent, const ThreadProperties& properties );
			virtual ~Thread();

			//
			bool Exit();

			//
			bool Terminate();

			//
			bool Suspend( bool interruptSleep = false );

			//
			bool Sleep( const Nanoseconds& timeout = Timeout_Infinite );

			//
			void Resume();

			///
			void Join();

			///
			void SetSyncGroup( ThreadSyncGroup& syncGroup );

			//
			bool WaitForExecutionEvent( InternalMutexLock& internalMutexLock, ThreadExecutionEvent event );

			//
			const ConcrtSharedStateHandle& GetConcrtSharedState() const;

			///
			const std::string& GetName() const;

			///
			const stdx::mask<ThreadPermissionFlags>& GetPermissions() const;

			///
			const System::ThreadHandle& GetSystemHandle() const;

			///
			ThreadSystemManager& GetSystemManager() const;

			///
			Thread_ref_id_t GetRefID() const;

			///
			Thread_index_t GetIndex() const;

			///
			ThreadUID GetUID() const;

			///
			std::string GetTextInfo() const;

			/// Returns internal mutex of the thread.
			InternalMutex& GetInternalMutex() const;

			///
			bool IsActive() const;

			///
			bool IsSuspended() const;

			///
			static Result Run( ThreadObjectHandle<Thread> thread, stdx::mask<ThreadRunFlags> runFlags );

		friendapi:
			// @ CurrentThread
			bool ExecuteSleepFor( const Nanoseconds& timeout );

		protected:
			//
			virtual ThreadUpdateResult Update();

			//
			bool WaitForStateChange( InternalMutexLock& internalMutexLock, stdx::mask<ThreadExecutionStateFlags> state, bool expected );

			//
			bool SetExecutionStateFlag( InternalMutexLock& internalMutexLock, stdx::mask<ThreadExecutionStateFlags> state, bool set = true );

			//
			virtual bool OnInit( const ThreadInitContext& initContext );

			//
			virtual void OnRelease( const ThreadReleaseContext& releaseContext ) noexcept;

			//
			static bool InitializeExternal( ThreadObjectHandle<Thread> thread );

			//
			static void ReleaseExternal( ThreadObjectHandle<Thread> thread );

		private:
			// Main thread function, the entry proc, executed after successful initialization.
			virtual Result Entry() = 0;

		private:
			//
			bool _SetActiveFlag();

			//
			bool _AcquireThreadState();

			//
			void _ReleaseThreadState();

			//
			void _ResetRunState();

			//
			void _CreateDynamicThreadLocalStorage();

			//
			void _DestroyDynamicThreadLocalStorage() noexcept;

			//
			void _Register();

			//
			void _Unregister() noexcept;

			//
			void _Initialize();

			//
			void _Release() noexcept;

			//
			void _SyncWithChildThreads() noexcept;

			// Adds thread represented by 'this' to the list of child threads of thread 'parent'.
			// This causes synchronization between 'this' and 'parent' (thread waits for all children to finish).
			ChildThreadRef _AddChildThread( Thread& childThread );

			//
			void _RemoveChildThread( Thread& childThread, ChildThreadRef childThreadRef );

			//
			ThreadRequestParameters* _SetRequest( InternalMutexLock& internalLock, ThreadRequestCode requestCode );

			//
			ThreadRequestProcessResult _ProcessRequestState();

			//
			void _SuspendExecution( InternalMutexLock& internalLock, const Nanoseconds& timeout );

			//
			static bool _RunSystemThread( ThreadObjectHandle<Thread> thread, stdx::mask<ThreadRunFlags> runFlags );

		private:
			// Initialization wrapper. Thread is passed from Run() function, system thread handle is received from
			// the sys-level caller. This function performs full initialization of the thread object, including
			// reservation of runtime context, registration and initialization of thread-specific state. If any part
			// of this process fails, false is returned back to the sys-level caller and init process is aborted.
			static bool _InitWrapper( ThreadObjectHandle<Thread> thread, System::ThreadHandle systemThreadHandle );

			// Thread execution function, passed to SystemThread.
			static void _ExecutionWrapper( ThreadObjectHandle<Thread> thread );

			// Release wrapper. Frees all data allocated for the thread, unregisters thread from the system and returns
			// its runtime context back to the pool. This function gets executed even if initialization and/or execution
			// wrapper exits via error or exception, so resources are always deallocated properly.
			// For threads which spawned subthreads with Sync flag, this function also performs parent-child synchronization.
			static void _ReleaseWrapper( ThreadObjectHandle<Thread> thread );

		private:
			// Handle to the shared concurrency subsystem state.
			ConcrtSharedStateHandle _concrtSharedState;

			// Pointer to the thread system manager.
			ThreadSystemManager* _threadSystemManager;

			// Handle to the system-level thread object.
			System::ThreadHandle _sysThreadHandle;

			// Ref ID assigned to this thread object.
			Thread_ref_id_t _refID;

			// Unique, runtime ID of the thread.
			ThreadUID _uid;

			// Name of the thread.
			std::string _name;

			// Internal sync object. Used to synchronize internal state changes and wait for events.
			// Internal SO mutex serves also as the lock for unique access to thread's internal state.
			InternalSyncObjectType _internalSyncObject;

			//
			ThreadRequestWrapper _internalRequest;

			// Internal state of the thread.
			stdx::atomic_mask<ThreadExecutionStateFlags> _internalExecutionState;

			// Permissions assigned to the thread.
			stdx::mask<ThreadPermissionFlags> _permissions;

			// Parent thread object.
			Thread* _parent;

			//
			ChildThreadRef _parentListRef;

			// Runtime context assigned to this thread.
			ThreadCoreSystemData _coreSystemData;

			// Pointer to this thread's local storage.
			ThreadLocalStorage* _localStorage;

			//
			ThreadSyncGroup* _syncGroup;
		};


		inline const ConcrtSharedStateHandle& Thread::GetConcrtSharedState() const
		{
			return this->_concrtSharedState;
		}

		inline const std::string& Thread::GetName() const
		{
			return this->_name;
		}

		inline const stdx::mask<ThreadPermissionFlags>& Thread::GetPermissions() const
		{
			return this->_permissions;
		}

		inline const System::ThreadHandle& Thread::GetSystemHandle() const
		{
			return this->_sysThreadHandle;
		}

		inline ThreadSystemManager& Thread::GetSystemManager() const
		{
			return *( this->_threadSystemManager );
		}

		inline Thread_ref_id_t Thread::GetRefID() const
		{
			return this->_refID;
		}

		inline Thread_index_t Thread::GetIndex() const
		{
			return this->_uid.GetIndex();
		}

		inline ThreadUID Thread::GetUID() const
		{
			return this->_uid;
		}

		inline Thread::InternalMutex& Thread::GetInternalMutex() const
		{
			return this->_internalSyncObject.GetMutex();
		}

		inline bool Thread::IsActive() const
		{
			return this->_internalExecutionState.is_set( ThreadExecutionStateFlag_Activated );
		}

		inline bool Thread::IsSuspended() const
		{
			return this->_internalExecutionState.is_set( ThreadExecutionStateFlag_Suspended );
		}


		///<summary>
		///</summary>
		template <class TBaseThread>
		class AsyncInvokerThread : public TBaseThread
		{
		public:
			using FunctionType = std::function<Result( AsyncInvokerThread<TBaseThread>* )>;

		public:
			template <typename TCallable, typename... TArgs>
			AsyncInvokerThread( EXS_THREAD_CTOR_PARAMS_DECL, TCallable thrFunction, TArgs&&... args )
				: TBaseThread( EXS_THREAD_CTOR_PARAMS_DEF, std::forward<TArgs>( args )... )
				, _thrFunction( thrFunction )
			{ }

			virtual ~AsyncInvokerThread() = default;

		private:
			//
			virtual Result Entry() override final
			{
				return this->_thrFunction( this );
			}

		private:
			FunctionType _thrFunction;
		};


	}
}


#endif /* __Exs_Core_Thread_H__ */
