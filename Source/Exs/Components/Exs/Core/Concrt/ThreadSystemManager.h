
#ifndef __Exs_Core_ThreadSystemManager_H__
#define __Exs_Core_ThreadSystemManager_H__

#include "ThreadActivityLog.h"
#include "ThreadInternal.h"
#include "ThreadRegistry.h"
#include "ThreadSyncController.h"


namespace Exs
{
	namespace Concrt
	{


		enum ThreadFeatureFlags : Enum
		{
			ThreadFeature_Messaging = 0x0001,

			ThreadFeatureSet_Default = ThreadFeature_Messaging
		};


		struct ThreadRunInfo
		{
			stdx::mask<ThreadFeatureFlags> featureFlags = ThreadFeatureSet_Default;
		};


		///<summary>
		///</summary>
		class EXS_LIBCLASS_CORE ThreadSystemManager
		{
			friend class SyncObject;
			friend class Thread;

		public:
			ThreadSystemManager( ConcrtSharedStateHandle concrtSharedState );
			~ThreadSystemManager();

			template <class TThread, class... TArgs>
			ThreadObjectHandle<TThread> CreateThreadObject( const ThreadCreateInfo& createInfo, TArgs&& ...args );

			template <class TCallable>
			ThreadActivityLog::EventCallbackRef RegisterThreadActivityEventCallback( TCallable callback );

			/**
			 *
			 * @return
			 */
			bool ValidateThreadCreateRequest() const;

			/**
			 *
			 * @param threadsNum
			 * @return
			 */
			bool CheckThreadResourceAvailability( Size_t threadsNum = 1 ) const;

			/**
			 *
			 * @param threadRefID
			 * @return
			 */
			ThreadUID QueryThreadUID( Thread_ref_id_t threadRefID ) const;

			/**
			 *
			 * @return
			 */
			Thread& GetCurrentThread() const;

			/**
			 *
			 * @return
			 */
			const ThreadEnvironmentConfig& GetEnvironmentConfig() const;

			/**
			 *
			 * @return
			 */
			ThreadRegistry& GetThreadRegistry();

			/**
			 *
			 * @return
			 */
			const ThreadRegistry& GetThreadRegistry() const;

			/**
			 *
			 * @return
			 */
			ThreadSyncController& GetThreadSyncController();

			/**
			 *
			 * @return
			 */
			const ThreadSyncController& GetThreadSyncController() const;

			/**
			 *
			 * @return
			 */
			const ThreadActivityLog& GetThreadActivityLog() const;

		friendapi:
			/**
			 * @caller Thread
			 * @param thread
			 * @return
			 */
			ThreadSharedSyncContext* GetThreadSyncContext( Thread& thread );

			/**
			 * @brief Reserves entry for a new thread object. Generates UID which is returned. If new thread cannot be created,
			 * returns UID_Invalid.
			 *
			 * @caller Thread
			 * @param thread Thread object for which the state should be created.
			 * @return
			 */
			ThreadUID CreateThreadState( Thread& thread );

			/**
			 * @brief Releases state reserved for the specified thread.
			 * @caller Thread
			 * @param thread
			 */
			void ReleaseThreadState( Thread& thread );

			/**
			 * @caller Thread
			 * @param thread
			 * @param systemRegInfo
			 */
			void RegisterThread( Thread& thread, const ThreadSystemRegInfo& systemRegInfo );

			/**
			 * @caller Thread
			 * @param thread
			 */
			void UnregisterThread( Thread& thread );

			/**
			 * @caller Thread
			 * @param thread
			 * @return
			 */
			ThreadCoreSystemData CreateThreadRuntimeObjects( Thread& thread );

			/**
			 * @caller Thread
			 * @param thread
			 */
			void DestroyThreadRuntimeObjects( Thread& thread );

		private:
			/**
			 * @brief Persistent data allocated for threads when they are registered within the system. Stores core state
			 * of the thread and some of its runtime objects (those which depends on thread's permissions, for example).
			 */
			struct ThreadPersistentData
			{
			public:
				// Index of the thread within the system (globally unique).
				Thread_index_t index;

				// Pointer to the thread object which currently own this data. Used for state validation.
				std::atomic<Thread*> threadObject;

				// UID of the thread.
				ThreadUID threadUID;

				// Thread's sync context.
				ThreadSharedSyncContext sharedSyncContext;

				// Container with references to child threads. Available only if thread has permissions to start new threads.
				std::unique_ptr<ThreadChildRefContainer> childRefContainer;

				// Message transceiver. Available only if thread has permissions to access the core messaging system.
				std::unique_ptr<ThreadMessageTransceiver> messageTransceiver;

			public:
				void ResetThread()
				{
					this->threadObject.store( nullptr, std::memory_order_relaxed );
				}

				void SetThread( Thread& thread )
				{
					this->threadObject.store( &thread, std::memory_order_relaxed );
				}

				bool CheckThread( Thread& thread ) const
				{
					return this->threadObject.load( std::memory_order_relaxed ) == &thread;
				}
			};

			// Local constant for max allowed number of active threads in the system.
			static constexpr Size_t maxThreadsNum = Config::CCRT_THR_Max_Active_Threads_Num;

			// Main 'index' which stores actual slots acquired by threads on registration.
			using ThreadPersistentDataIndex = stdx::concurrent_data_pool<ThreadPersistentData, maxThreadsNum>;

			//
			using ThreadPersistentDataHandle = ThreadPersistentDataIndex::value_handle_type;

		private:
			///
			ThreadPersistentData& _GetThreadPersistentData( Thread& thread );

			///
			ThreadPersistentDataHandle _AcquireThreadData();

			///
			void _ReleaseThreadData( ThreadPersistentData& threadData );

			///
			void _CreateRuntimeObjects( Thread& thread, ThreadPersistentData& threadPersistentData, stdx::mask<ThreadPermissionFlags> permissions );

			///
			void _DestroyRuntimeObjects( Thread& thread, ThreadPersistentData& threadPersistentData );

		private:
			//
			ConcrtSharedStateHandle _concrtSharedState;

			// Environment configuration.
			ThreadEnvironmentConfig _environmentConfig;

			// Fixed array containing instances of persistent data reserved by threads on creation.
			ThreadPersistentDataIndex _threadPersistentDataIndex;

			// Registry. Controls thread registration process, performs refID --> UID mapping.
			ThreadRegistry _registry;

			//
			ThreadSyncController _syncController;

			// Activity log, collects most important events issued by threads.
			ThreadActivityLog _activityLog;

			//
			stdx::reg_ctrl_counter<Size_t, maxThreadsNum> _thrActiveCounter;

			//
			std::atomic<Thread_ref_id_t> _systemRefIDValue;
		};


		template <class TThread, class... TArgs>
		inline ThreadObjectHandle<TThread> ThreadSystemManager::CreateThreadObject( const ThreadCreateInfo& createInfo, TArgs&& ...args )
		{
			Thread* currentThread = nullptr;
			ThreadCreateInfo checkedCreateInfo = createInfo;

			if ( createInfo.properties.refID == Thread_Ref_ID_Auto )
			{
				checkedCreateInfo.properties.refID = this->_systemRefIDValue.fetch_add( 1, std::memory_order_relaxed );
			}

			if ( createInfo.runFlags.is_set( ThreadRun_Enable_Parent_Sync ) )
			{
				currentThread = &( this->GetCurrentThread() );
			}

			return std::make_shared<TThread>( this->_concrtSharedState, currentThread, checkedCreateInfo.properties, std::forward<TArgs>( args )... );
		}

		template <class TCallable>
		inline ThreadActivityLog::EventCallbackRef ThreadSystemManager::RegisterThreadActivityEventCallback( TCallable callback )
		{
			return this->_activityLog.RegisterCallback( std::forward<TCallable>( callback ) );
		}

		inline bool ThreadSystemManager::CheckThreadResourceAvailability( Size_t threadsNum ) const
		{
			auto currentThreadsNum = this->_thrActiveCounter.get_value();
			return maxThreadsNum - currentThreadsNum >= threadsNum;
		}

		inline const ThreadEnvironmentConfig& ThreadSystemManager::GetEnvironmentConfig() const
		{
			return this->_environmentConfig;
		}

		inline ThreadRegistry& ThreadSystemManager::GetThreadRegistry()
		{
			return this->_registry;
		}

		inline const ThreadRegistry& ThreadSystemManager::GetThreadRegistry() const
		{
			return this->_registry;
		}

		inline ThreadSyncController& ThreadSystemManager::GetThreadSyncController()
		{
			return this->_syncController;
		}

		inline const ThreadSyncController& ThreadSystemManager::GetThreadSyncController() const
		{
			return this->_syncController;
		}

		inline const ThreadActivityLog& ThreadSystemManager::GetThreadActivityLog() const
		{
			return this->_activityLog;
		}


	}
}


#endif /* __Exs_Core_ThreadSystemManager_H__ */
