
#ifndef __Exs_Core_ThreadActivityLog_H__
#define __Exs_Core_ThreadActivityLog_H__

#include "ThreadCommon.h"
#include <deque>


namespace Exs
{
	namespace Concrt
	{


		enum class ThreadActivityEventCategory : Enum
		{
			Registration = 1,
			Rstate_Acquire,
			Rstate_Release,
			Unregistration,
		};


		struct ThreadActivityEventInfo
		{
			ThreadActivityEventCategory eventCategory;
			Result result;
			Thread* threadObject;
		};


		class ThreadActivityLog : public Lockable<LightSharedMutex, LockAccess::Relaxed>
		{
		public:
			struct Event
			{
				ThreadActivityEventCategory eventCategoryID;
				std::string eventCategoryName;
				Result result;
				Thread* threadObject;
			};

			using EventList = std::deque<Event>;

			using EventCallback = std::function<void( const Event& )>;

			using EventCallbackList = std::list<EventCallback>;

			using EventCallbackRef = EventCallbackList::iterator;

		public:
			ThreadActivityLog( const ThreadActivityLog& ) = delete;
			ThreadActivityLog& operator=( const ThreadActivityLog& ) = delete;

			ThreadActivityLog();
			~ThreadActivityLog();

			void PostEvent( ThreadActivityEventInfo&& eventInfo );

			void PostEvent( const ThreadActivityEventInfo& eventInfo );

			template <typename F>
			EventCallbackRef RegisterCallback( F callback );

			void UnregisterCallback( EventCallbackRef callbackRef );

			const EventList& GetEventList() const;

		private:
			void _OnEvent( const Event& event );

			static const char* _QueryEventCategoryName( ThreadActivityEventCategory eventCategory );

		private:
			EventList _events;
			EventCallbackList _eventCallbacks;
		};


		template <typename F>
		inline ThreadActivityLog::EventCallbackRef ThreadActivityLog::RegisterCallback( F callback )
		{
			return this->_eventCallbacks.insert( this->_eventCallbacks.end(), callback );
		}

		inline void ThreadActivityLog::UnregisterCallback( EventCallbackRef callbackRef )
		{
			this->_eventCallbacks.erase( callbackRef );
		}

		inline const ThreadActivityLog::EventList& ThreadActivityLog::GetEventList() const
		{
			return this->_events;
		}


	}
}


#endif /* __Exs_Core_ThreadActivityLog_H__ */
