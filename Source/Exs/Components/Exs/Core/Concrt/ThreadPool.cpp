
#include "Precomp.h"
#include <Exs/Core/Concrt/ThreadPool.h>
#include <Exs/Core/Concrt/ThreadLauncher.h>
#include <Exs/Core/Concrt/ThreadPoolWorkerThread.h>

namespace Exs
{
	namespace Concrt
	{


		WorkerThreadHandle ThreadPoolWorkerThreadFactory::CreateWorkerThread( EXS_WORKER_THREAD_FACTORY_PARAMS_DECL ) noexcept
		{
			return ThreadLauncher::CreateThread<ThreadPoolWorkerThread>( EXS_WORKER_THREAD_FACTORY_PARAMS_DEF, *( this->_threadPool ) );
		}


		ThreadPool::ThreadPool( ConcrtSharedStateHandle concrtSharedState, std::string name, Size_t poolSize )
		: _concrtSharedState( concrtSharedState )
		, _workerFactory( _concrtSharedState, *this )
		, _workerThreadContainer( concrtSharedState, _workerFactory, name )
		{
			if ( poolSize > 0 )
			{
				this->_Initialize( poolSize );
			}
		}


		ThreadPool::~ThreadPool()
		{
			// Release the pool. We mus ensure, that all work enqueued so far gets executed!
			this->Release( ThreadPoolReleaseMode::Continue );

			// Wait for all threads to completely finish their execution. This ensures, that threads
			// will not attempt to access the pool after it is destroyed. ::Released event is used (not
			// ::Finished), because thread may execute custom user code after its Entry() method quits
			// (for example, when OnRelease() method is overwitten).
			// this->_workerThreadSyncGroup.Wait( ThreadExecutionEvent::Released );
		}


		void ThreadPool::Init( Size_t workerThreadsNum )
		{
			if ( ( workerThreadsNum == 0 ) || ( workerThreadsNum > maxSize ) )
			{
				return;
			}

			if ( !this->_SetInitializedFlag() )
			{
				throw 0;
			}

			this->_Initialize( workerThreadsNum );
		}


		void ThreadPool::Release( ThreadPoolReleaseMode releaseMode )
		{
			if ( this->_SetReleaseRequest() )
			{
				if ( releaseMode == ThreadPoolReleaseMode::Abort )
				{
					// Abort: sets Exit() on all workers, causing them to quit execution loop after finishing
					// execution of current work item. Remaining work (items still in the queue) will NOT be
					// executed and will be discarded (destroyed!).

					this->_StopWorkerThreads();
				}
				else if ( releaseMode == ThreadPoolReleaseMode::Synchronize )
				{
					// Synchronize: wait untill all queued work is completed. After this wait, queue should be
					// always empty. Then, send SIG_EXIT to all workers, so they can nicely finish their execution.

					this->_WaitForWorkCompletion( Timeout_Infinite );
				}
			}
		}


		void ThreadPool::WaitForWorkCompletion( const Nanoseconds& timeout )
		{
			if ( !this->IsReleaseRequestSet() )
			{
				// Calling this method on an active pool (i.e. pool, on which Release() was not called),
				// may result in a veeery long wait, and will be actually a wait for empty queue, not
				// completion of all work (possible scenario: Wait() in this function gets notification
				// and immediately after that, and before this function is called, new work item is added).

				// issue a warning..?
			}

			if ( this->IsSharedQueueEmpty() )
			{
				return;
			}

			this->_WaitForWorkCompletion( timeout );
		}


//		bool ThreadPool::IsWorkerActive( Size_t workerIndex ) const
//		{
//			return false;
//		}


		bool ThreadPool::RegisterWorkerThread( ThreadPoolWorkerThread& workerThread )
		{
			InternalMutexLock internalLock { this->_internalMutex };
			{
				auto& workerState = this->_GetWorkerState( workerThread );
				workerState.idleRef = this->_idleWorkerList.insert( this->_idleWorkerList.end(),  &workerThread );
				this->_idleThreadsNum.fetch_add( 1, std::memory_order_relaxed );
			}

			return true;
		}


		void ThreadPool::UnregisterWorkerThread( ThreadPoolWorkerThread& workerThread )
		{
			InternalMutexLock internalLock { this->_internalMutex };
			{
				auto& workerState = this->_GetWorkerState( workerThread );
				this->_idleWorkerList.erase( workerState.idleRef );
				this->_idleThreadsNum.fetch_add( 1, std::memory_order_relaxed );
			}
		}


		void ThreadPool::OnCallbackExecutionBegin( ThreadPoolWorkerThread& workerThread )
		{
		}


		void ThreadPool::OnCallbackExecutionEnd( ThreadPoolWorkerThread& workerThread )
		{
		}


		ThreadPoolAsyncCallback ThreadPool::WaitForCallback( ThreadPoolWorkerThread& workerThread )
		{
			ThreadPoolAsyncCallback callback;

			SharedQueueMutexLock sharedQueueLock { this->_sharedQueue.GetInternalMutex() };
			{
				// If queue is empty and release flag is set, it means, that there is no
				// items to wait for, as new items will not arrive (pool has been released).

				if ( this->IsReleaseRequestSet() && this->_sharedQueue.IsEmpty() )
				{
					return nullptr;
				}

				// Wait if queue is empty AND release flag is not set. If queue is not empty, waiting is not
				// required as there is at least one element in the queue. If queue is empty and release flag
				// is set, it means, that there is nothing to wait for, as new items will not arrive (inactive).

				callback = this->_sharedQueue.WaitForCallback( sharedQueueLock, Timeout_Infinite );
			}

			return callback;
		}


		ThreadPoolWorkerState& ThreadPool::_GetWorkerState( const ThreadPoolWorkerThread& workerThread )
		{
			return this->_workerThreadContainer.GetWorkerThreadState( workerThread );
		}


		ThreadPoolAsyncResult ThreadPool::_QueueCallback( ThreadPoolAsyncCallback&& callback )
		{
			if ( this->IsReleaseRequestSet() )
			{
				// Queueing new work items after pool is released is an invalid operation.
				ExsThrowException( EXC_Invalid_Operation );
			}

			SharedQueueMutexLock sharedQueueLock { this->_sharedQueue.GetInternalMutex() };
			{
				return this->_sharedQueue.Enqueue( sharedQueueLock, std::move( callback ) );
			}
		}


		void ThreadPool::_Initialize( Size_t workerThreadsNum )
		{
			this->_RunWorkerThreads( workerThreadsNum );
		}


		void ThreadPool::_RunWorkerThreads( Size_t workerThreadsNum )
		{
			this->_workerThreadContainer.Resize( workerThreadsNum );
		}


		void ThreadPool::_StopWorkerThreads()
		{
			this->_workerThreadContainer.Resize( 0 );

			SharedQueueMutexLock sharedQueueLock { this->_sharedQueue.GetInternalMutex() };
			{
				this->_sharedQueue.InterruptWait( sharedQueueLock );
			}
		}


		bool ThreadPool::_SetInitializedFlag()
		{
			return this->_internalStateMask.test_and_set( InternalStateFlag_Initialized );
		}


		bool ThreadPool::_SetReleaseRequest()
		{
			SharedQueueMutexLock sharedQueueLock { this->_sharedQueue.GetInternalMutex() };
			{
				if ( this->_internalStateMask.test_and_set( InternalStateFlag_Release_Request_Set ) )
				{
					//
					this->_sharedQueue.Clear();

					return true;
				}
			}

			return false;
		}


		void ThreadPool::_WaitForWorkCompletion( const Nanoseconds& timeout )
		{
		}


	}
}
