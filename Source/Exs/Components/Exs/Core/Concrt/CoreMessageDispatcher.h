
#ifndef __Exs_Core_CoreMessageDispatcher_H__
#define __Exs_Core_CoreMessageDispatcher_H__

#include "CoreMessageBase.h"


namespace Exs
{
	namespace Concrt
	{


		class CoreMessageReceiver;
		class CoreMessageSystemRoot;


		class CoreMessageDispatcher
		{
			friend class CoreMessageSystemRoot;

		private:
			using InternalMessageQueue = std::deque<CoreMessageHandle>;

			enum ReceiverStateFlags : Enum
			{
				ReceiverState_Inactive = 0x8000
			};

			// Represents state of a registered receiver.
			struct ReceiverState
			{
				//
				LightMutex inactiveMessageQueueMutex;

				// List of messages received when receiver is inactive.
				InternalMessageQueue inactiveMessageQueue;

				// Pointer to the message receiver.
				CoreMessageReceiver* receiver = nullptr;

				//
				CoreMessageReceiverID receiverID = Core_Message_Receiver_ID_None;

				//
				stdx::atomic_mask<ReceiverStateFlags> stateFlags = ATOMIC_VAR_INIT( 0 );
			};

			//
			using ReceiverArray = std::array<ReceiverState, Config::CCRT_MSG_Max_Registered_Receivers_Num>;

			//
			using ReceiverIndexMap = stdx::assoc_array<CoreMessageReceiverID, Uint32>;

			// Temporary queues for messages sent to receivers which are not registered at the time a message is being dispatched.
			using TemporaryReceiverMessageQueueList = stdx::assoc_array<CoreMessageReceiverID, InternalMessageQueue>;

			//
			static constexpr Uint32 invalidReceiverIndex = stdx::limits<Uint32>::max_value;

		public:
			CoreMessageDispatcher( CoreMessageSystemRoot* messageSystemRoot );

		friendapi:
			// @ CoreMessageSystemRoot
			Result RegisterReceiver( CoreMessageReceiverID receiverID, Uint32 receiverIndex, CoreMessageReceiver& receiver );

			// @ CoreMessageSystemRoot
			void UnregisterReceiver( CoreMessageReceiverID receiverID, CoreMessageReceiver* receiver = nullptr );

			//
			void ActivateReceiver( CoreMessageReceiverID receiverID );

			//
			void DeactivateReceiver( CoreMessageReceiverID receiverID );

			// @ CoreMessageSystemRoot
			Result DispatchMessage( const CoreMessageDispatchRequest& dispatchRequest, const CoreMessageHandle& message );

		private:
			//
			Result _DispatchReg( Uint32 receiverIndex, const CoreMessageHandle& message, bool isImmediateMessage );

			//
			Result _DispatchUnreg( CoreMessageReceiverID receiverID, const CoreMessageHandle& message, bool isImmediateMessage );

			//
			void _FetchTemporaryMessageQueue( CoreMessageReceiverID receiverID, CoreMessageReceiver& receiver );

			//
			Uint32 _GetReceiverIndex( CoreMessageReceiverID receiverID );

		private:
			//
			CoreMessageSystemRoot* _messageSystemRoot;

			// Array of receiver states.
			ReceiverArray _receiverArray;

			// ReceiverID -> index map.
			ReceiverIndexMap _receiverIndexMap;

			// Controls access to internal state. Locked uniquely when receiver is registered/unregistered
			// or its state is modified (activation/deactivation). Locked with shared ownership on disptach.
			LightSharedMutex _sharedMutex;

			// List of temporary queues for unregistered receivers.
			TemporaryReceiverMessageQueueList _temporaryReceiverMessageQueueList;

			//
			LightMutex _temporaryReceiverMessageQueueListMutex;
		};


	}
}


#endif /* __Exs_Core_CoreMessageDispatcher_H__ */
