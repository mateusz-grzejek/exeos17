
#include "Precomp.h"
#include <Exs/Core/Concrt/ThreadSyncController.h>
#include <Exs/Core/Concrt/Thread.h>


namespace Exs
{
	namespace Concrt
	{


		void ThreadSyncController::RegisterActiveWait( Thread& thread, SyncObject& syncObject )
		{
			auto& threadEntry = this->_GetThreadEntry( thread );

			ExsBeginCriticalSection( this->GetLock() );
			{
				auto waitInfoIter = this->_activeWaitList.insert( &thread, ThreadActiveWaitInfo() );
				auto& waitInfo = waitInfoIter->value;

				waitInfo.thread = &thread;
				waitInfo.syncObject = &syncObject;

				threadEntry.isWaiting.store( true, std::memory_order_relaxed );
			}
			ExsEndCriticalSection();
		}


		void ThreadSyncController::UnregisterActiveWait( Thread& thread )
		{
			auto& threadEntry = this->_GetThreadEntry( thread );

			ExsBeginCriticalSection( this->GetLock() );
			{
				auto waitInfoIter = this->_activeWaitList.find( &thread );

				if ( waitInfoIter == this->_activeWaitList.end() )
				{
					throw 0;
				}

				this->_activeWaitList.erase( waitInfoIter );
				threadEntry.isWaiting.store( false, std::memory_order_relaxed );
			}
			ExsEndCriticalSection();
		}


		ThreadSharedSyncContext* ThreadSyncController::GetThreadSyncContext( Thread& thread )
		{
			auto& threadEntry = this->_GetThreadEntry( thread );

			return threadEntry.sharedSyncContext;
		}


		void ThreadSyncController::RegisterThreadSharedSyncContext( Thread& thread, ThreadSharedSyncContext& threadSharedSyncContext )
		{
			auto& threadEntry = this->_GetThreadEntry( thread );

			threadEntry.sharedSyncContext = &threadSharedSyncContext;
		}


		void ThreadSyncController::UnregisterThreadSharedSyncContext( Thread& thread )
		{
			auto& threadEntry = this->_GetThreadEntry( thread );

			threadEntry.sharedSyncContext = nullptr;
		}


		ThreadSyncController::ThreadEntry& ThreadSyncController::_GetThreadEntry( Thread& thread )
		{
			auto threadIndex = thread.GetIndex();
			return this->_contextList[threadIndex];
		}


		const ThreadSyncController::ThreadEntry& ThreadSyncController::_GetThreadEntry( Thread& thread ) const
		{
			auto threadIndex = thread.GetIndex();
			return this->_contextList[threadIndex];
		}


	}
}
