
#include "Precomp.h"
#include <Exs/Core/Concrt/ThreadSyncGroup.h>
#include <Exs/Core/Concrt/Thread.h>


namespace Exs
{
	namespace Concrt
	{


		void ThreadSyncGroup::AddThread( Thread& thread )
		{
			thread.SetSyncGroup( *this );
			++this->_registeredThreadsNum;
		}


		void ThreadSyncGroup::SubscriveEvent( ThreadExecutionEvent event )
		{
			auto eventStateIter = this->_subscribedEvents.find( event );

			if ( eventStateIter != this->_subscribedEvents.end() )
			{
				return;
			}

			eventStateIter = this->_subscribedEvents.insert( event, {} );
			eventStateIter->value.notifyCounter = 0;
		}


		void ThreadSyncGroup::Wait( ThreadExecutionEvent event )
		{
			auto* eventState = this->_GetEventState( event );

			if ( eventState == nullptr )
			{
				ExsThrowException( EXC_Invalid_Operation );
			}

			SyncMutexLock syncLock{ this->_syncObject.GetMutex() };
			{
				if ( !this->_CheckNotifyCounter( *eventState ) )
				{
					this->_syncObject.Wait( syncLock, [this, eventState]() -> bool {
						return this->_CheckNotifyCounter( *eventState );
					} );
				}
			}
		}


		void ThreadSyncGroup::NotifyOne( Thread& thread, ThreadExecutionEvent event )
		{
			if ( auto* eventState = this->_GetEventState( event ) )
			{
				if ( this->_CheckNotifyCounter( *eventState ) )
				{
					throw 0;
				}

				SyncMutexLock syncLock{ this->_syncObject.GetMutex() };
				{
					if ( ++eventState->notifyCounter == this->_threadsNum )
					{
						this->_syncObject.NotifyAll( syncLock );
					}
				}
			}
		}


		ThreadSyncGroup::EventState* ThreadSyncGroup::_GetEventState( ThreadExecutionEvent event )
		{
			auto eventStateIter = this->_subscribedEvents.find( event );
			return ( eventStateIter != this->_subscribedEvents.end() ) ? &( eventStateIter->value ) : nullptr;
		}


		bool ThreadSyncGroup::_CheckNotifyCounter( EventState& eventState )
		{
			return eventState.notifyCounter == this->_threadsNum;
		}
		

		bool ThreadSyncGroup::_CheckNotifyCounter( ThreadExecutionEvent event )
		{
			if ( auto* eventState = this->_GetEventState( event ) )
			{
				if ( this->_CheckNotifyCounter( *eventState ) )
				{
					return true;
				}
			}

			return false;
		}


	}
}
