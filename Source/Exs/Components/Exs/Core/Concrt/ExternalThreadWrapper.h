
#ifndef __Exs_Core_ExternalThreadWrapper_H__
#define __Exs_Core_ExternalThreadWrapper_H__

#include "Thread.h"
#include "ThreadSystemManager.h"


namespace Exs
{
	namespace Concrt
	{


		struct ExternalThreadCreateInfo : public ThreadCreateInfo
		{
		};


		class ExternalThread : public Thread
		{
			friend class ExternalThreadWrapper;

		public:
			ExternalThread( EXS_THREAD_CTOR_PARAMS_DECL )
				: Thread( EXS_THREAD_CTOR_PARAMS_DEF )
			{ }

		private:
			// 
			virtual Result Entry() override final
			{
				ExsThrowException( EXC_Access_Violation );
			}
		};

		
		class ExternalThreadWrapper
		{
		public:
			ExternalThreadWrapper( ExternalThreadWrapper&& ) = default;
			ExternalThreadWrapper& operator=( ExternalThreadWrapper&& ) = default;

			ExternalThreadWrapper( const ExternalThreadWrapper& ) = delete;
			ExternalThreadWrapper& operator=( const ExternalThreadWrapper& ) = delete;

			ExternalThreadWrapper( ThreadObjectHandle<Thread> threadObject )
			{
				this->_Initialize( threadObject );
			}

			~ExternalThreadWrapper()
			{
				this->_Release();
			}

			explicit operator bool() const
			{
				return this->_threadObject ? true : false;
			}

		private:
			void _Initialize( ThreadObjectHandle<Thread> threadObject )
			{
				if ( threadObject && ExternalThread::InitializeExternal( threadObject ) )
				{
					this->_threadObject = threadObject;
				}
			}

			void _Release()
			{
				if ( this->_threadObject )
				{
					ExternalThread::ReleaseExternal( this->_threadObject );
					this->_threadObject = nullptr;
				}
			}

		private:
			ThreadObjectHandle<Thread> _threadObject;
		};


		template <class Thr, class... Args>
		inline ExternalThreadWrapper CreateExternalThreadWrapper( ConcrtSharedStateHandle concrtSharedState, const ExternalThreadCreateInfo& createInfo, Args&& ...args )
		{
			auto threadObject = concrtSharedState->threadSystemManager->CreateThreadObject<Thr>( createInfo, std::forward<Args>( args )... );
			return ExternalThreadWrapper( threadObject );
		}


		template <class Thr, class... Args>
		inline ExternalThreadWrapper CreateMainThreadWrapper( ConcrtSharedStateHandle concrtSharedState, const ExternalThreadCreateInfo& createInfo, Args&& ...args )
		{
			ExternalThreadCreateInfo checkedCreateInfo = createInfo;
			checkedCreateInfo.runFlags.unset( ThreadRun_Detached | ThreadRun_Enable_Parent_Sync );
			auto threadObject = concrtSharedState->threadSystemManager->CreateThreadObject<Thr>( checkedCreateInfo, std::forward<Args>( args )... );
			return ExternalThreadWrapper( threadObject );
		}


	}
}


#endif /* __Exs_Core_ExternalThreadWrapper_H__ */
