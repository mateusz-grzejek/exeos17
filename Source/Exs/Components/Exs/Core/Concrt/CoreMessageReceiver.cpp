
#include "Precomp.h"
#include <Exs/Core/Concrt/CoreMessageReceiver.h>
#include <Exs/Core/Concrt/CoreMessage.h>
#include <ExsLib/CoreUtils/Exception.h>


namespace Exs
{
	namespace Concrt
	{


		CoreMessageReceiver::CoreMessageReceiver( CoreMessageSystemRoot& messageSystemRoot, Thread& thread, CoreMessageReceiverID id )
		: _messageSystemRoot( &messageSystemRoot )
		, _thread( &thread )
		, _id( id )
		, _syncObject( thread )
		{ }


		void CoreMessageReceiver::RegisterExternalQueue( CoreMessageQueue& externalQueue )
		{
			ExsBeginSharedCriticalSectionWithUniqueAccess( this->_queueDataLock );
			{
				auto queueStateRef = this->_externalQueueStateMap.find( &externalQueue );

				if ( queueStateRef != this->_externalQueueStateMap.end() )
				{
					ExsThrowException( EXC_Invalid_State );
				}

				ExternalQueueState queueState;
				queueState.isActive = true;
				queueState.queue = &externalQueue;

				this->_externalQueueStateMap.insert( &externalQueue, std::move( queueState ) );
			}
			ExsEndCriticalSection();
		}


		void CoreMessageReceiver::UnregisterExternalQueue( CoreMessageQueue& externalQueue )
		{
			ExsBeginSharedCriticalSectionWithUniqueAccess( this->_queueDataLock );
			{
				auto queueStateRef = this->_externalQueueStateMap.find( &externalQueue );

				if ( queueStateRef == this->_externalQueueStateMap.end() )
				{
					ExsThrowException( EXC_Invalid_State );
				}

				this->_externalQueueStateMap.erase( queueStateRef );
			}
			ExsEndCriticalSection();
		}


		bool CoreMessageReceiver::AddMessageFilter( MessageFilter messageFilter, CoreMessageQueue& externalQueue, bool overwrite )
		{
			ExsBeginSharedCriticalSectionWithUniqueAccess( this->_queueDataLock );
			{
				auto filterQueueRef = this->_messageFilterMap.find( messageFilter );

				if ( ( filterQueueRef != this->_messageFilterMap.end() ) && !overwrite )
				{
					return false;
				}

				this->_messageFilterMap[messageFilter] = &externalQueue;
			}
			ExsEndCriticalSection();

			return true;
		}


		void CoreMessageReceiver::RemoveMessageFilter( MessageFilter messageFilter, CoreMessageQueue* externalQueue )
		{
			ExsBeginSharedCriticalSectionWithUniqueAccess( this->_queueDataLock );
			{
				auto filterQueueRef = this->_messageFilterMap.find( messageFilter );

				if ( filterQueueRef == this->_messageFilterMap.end() )
				{
					ExsThrowException( EXC_Invalid_State );
				}

				if ( ( externalQueue != nullptr ) &&  ( filterQueueRef->value != externalQueue ) )
				{
					ExsThrowException( EXC_Invalid_State );
				}

				this->_messageFilterMap.erase( filterQueueRef );
			}
			ExsEndCriticalSection();
		}


		CoreMessageHandle CoreMessageReceiver::PeekMessage( CoreMessageQueue* sourceQueue )
		{
			if ( sourceQueue == nullptr )
			{
				sourceQueue = &( this->_defaultQueue );
			}

			SyncMutexLock syncLock{ this->_syncObject.GetMutex() };
			{
				if ( !sourceQueue->IsEmpty() )
				{
					return sourceQueue->FetchUnchecked();
				}
			}

			return nullptr;
		}


		CoreMessageHandle CoreMessageReceiver::WaitForMessage( CoreMessageQueue* sourceQueue )
		{
			if ( sourceQueue == nullptr )
			{
				sourceQueue = &( this->_defaultQueue );
			}

			SyncMutexLock syncLock{ this->_syncObject.GetMutex() };
			{
				if ( sourceQueue->IsEmpty() )
				{
					this->_currentWaitQueue = sourceQueue;
					this->_syncObject.Wait( *( this->_thread ), syncLock, [sourceQueue]() -> bool {
						return !sourceQueue->IsEmpty();
					} );
					this->_currentWaitQueue = nullptr;
				}

				if ( !sourceQueue->IsEmpty() )
				{
					return sourceQueue->FetchUnchecked();
				}
			}

			return nullptr;
		}


		CoreMessageHandle CoreMessageReceiver::WaitForMessage( const Milliseconds& timeout, CoreMessageQueue* sourceQueue )
		{
			if ( sourceQueue == nullptr )
			{
				sourceQueue = &( this->_defaultQueue );
			}

			SyncMutexLock syncLock{ this->_syncObject.GetMutex() };
			{
				if ( sourceQueue->IsEmpty() )
				{
					this->_currentWaitQueue = sourceQueue;
					this->_syncObject.WaitFor( *( this->_thread ), syncLock, timeout, [sourceQueue]() -> bool {
						return !sourceQueue->IsEmpty();
					} );
					this->_currentWaitQueue = nullptr;
				}

				if ( !sourceQueue->IsEmpty() )
				{
					return sourceQueue->FetchUnchecked();
				}
			}

			return nullptr;
		}


		Result CoreMessageReceiver::PushMessage( const CoreMessageHandle& message )
		{
			auto messageCode = message->GetCode();
			auto messageClass = message->GetClass();

			ExsBeginSharedCriticalSectionWithSharedAccess( this->_queueDataLock );
			{
				auto* targetQueue = this->_GetTargetQueue( messageCode, messageClass );

				if ( targetQueue == nullptr )
				{
					return ExsResult( RSC_Msg_Message_Discarded );
				}

				ExsBeginCriticalSection( this->_syncObject.GetMutex() );
				{
					targetQueue->Push( message );
				}
				ExsEndCriticalSection();
			}
			ExsEndCriticalSection();

			return ExsResult( RSC_Success );
		}


		Result CoreMessageReceiver::PostMessage( const CoreMessageHandle& message )
		{
			auto messageCode = message->GetCode();
			auto messageClass = message->GetClass();

			ExsBeginSharedCriticalSectionWithSharedAccess( this->_queueDataLock );
			{
				auto* targetQueue = this->_GetTargetQueue( messageCode, messageClass );

				if ( targetQueue == nullptr )
				{
					return ExsResult( RSC_Msg_Message_Discarded );
				}

				SyncMutexLock syncLock{ this->_syncObject.GetMutex() };
				{
					targetQueue->Push( message );

					if ( targetQueue == this->_currentWaitQueue )
					{
						this->_syncObject.NotifyOne( syncLock );
					}
				}
			}
			ExsEndCriticalSection();

			return ExsResult( RSC_Success );
		}


		CoreMessageQueue* CoreMessageReceiver::_GetTargetQueue( Message_code_t messageCode, Message_class_t messageClass )
		{
			if ( !this->_messageFilterMap.empty() )
			{
				auto messageCodeFilterRef = this->_messageFilterMap.find( messageCode );

				if ( messageCodeFilterRef != this->_messageFilterMap.end() )
				{
					auto* queue = messageCodeFilterRef->value;
					auto* queueState = this->_GetExternalQueueState( queue );

					if ( ( queueState != nullptr ) && queueState->isActive )
					{
						return queue;
					}
				}

				auto messageClassFilterRef = this->_messageFilterMap.find( messageClass );

				if ( messageClassFilterRef != this->_messageFilterMap.end() )
				{
					auto* queue = messageClassFilterRef->value;
					auto* queueState = this->_GetExternalQueueState( queue );

					if ( ( queueState != nullptr ) && queueState->isActive )
					{
						return queue;
					}
				}
			}

			return &( this->_defaultQueue );
		}


		CoreMessageReceiver::ExternalQueueState* CoreMessageReceiver::_GetExternalQueueState( CoreMessageQueue* queue )
		{
			auto queueStateRef = this->_externalQueueStateMap.find( queue );
			return ( queueStateRef != this->_externalQueueStateMap.end() ) ? &( queueStateRef->value ) : nullptr;
		}


	}
}
