
#ifndef __Exs_Core_ThreadInternal_H__
#define __Exs_Core_ThreadInternal_H__

#include "ConcrtSharedState.h"
#include "ThreadCommon.h"


namespace Exs
{
	namespace Concrt
	{


		class ThreadChildRefContainer;
		class ThreadMessageController;
		class ThreadMessageTransceiver;


		using Thread_execution_state_value_t = Enum;


		enum class ThreadRequestCode : Enum
		{
			None = 0,

			Sleep,

			Suspend,

			Exit,

			Terminate
		};


		enum class ThreadRequestProcessResult : Enum
		{
			None,

			Exit
		};


		enum ThreadExecutionStateFlags : Thread_execution_state_value_t
		{
			ThreadExecutionStateFlag_Event_Mask = 0x70000000,

			ThreadExecutionStateFlag_Activated = 0x0001,

			ThreadExecutionStateFlag_Registered = 0x0002 | ThreadExecutionStateFlag_Event_Mask,

			ThreadExecutionStateFlag_Suspended = 0x0040,

			ThreadExecutionStateFlag_Finished = 0x0010 | ThreadExecutionStateFlag_Event_Mask,

			ThreadExecutionStateFlag_Released = 0x0020 | ThreadExecutionStateFlag_Event_Mask,
		};


		enum class ThreadExecutionEvent : Enum
		{
			Registered = static_cast<Enum>( ThreadExecutionStateFlag_Registered ),

			Finished = static_cast<Enum>( ThreadExecutionStateFlag_Finished ),

			Released = static_cast<Enum>( ThreadExecutionStateFlag_Released ),

			None = 0
		};


		union ThreadRequestParameters
		{
		public:
			struct SleepParameters
			{
				Nanoseconds timeout;
			};

			struct SuspendParameters
			{
			};

			struct ExitParameters
			{
			};

			struct TerminateParameters
			{
			};

			SleepParameters sleepParams;

			SuspendParameters suspendParams;

			ExitParameters exitParams;

			TerminateParameters terminateParams;

		public:
			ThreadRequestParameters()
			{ }
		};


		///<summary>
		///</summary>
		struct ThreadCoreSystemData
		{
		public:
			//
			Thread* threadObject = nullptr;

			///
			ThreadSharedSyncContext* sharedSyncContext = nullptr;

			//.
			ThreadChildRefContainer* childRefContainer = nullptr;

			///
			ThreadMessageTransceiver* messageTransceiver = nullptr;

			///
			ThreadMessageController* messageController = nullptr;

		public:
			ThreadCoreSystemData() = default;

			ThreadCoreSystemData( std::nullptr_t )
				: ThreadCoreSystemData()
			{ }

			explicit operator bool() const
			{
				return this->threadObject != nullptr;
			}
		};


		/// Temporary context created and used during thread initialization phase.
		struct ThreadInitContext
		{
			//
			Thread* thread;

			// Pointer to the thread's local storage.
			ThreadLocalStorage* localStorage;
		};


		/// Temporary context created and used during thread relase phase.
		struct ThreadReleaseContext
		{
			//
			Thread* thread;

			// Pointer to the thread's local storage.
			ThreadLocalStorage* localStorage;
		};


		/// Stores additional information about thread that may be saved in registry.
		struct ThreadSystemRegInfo
		{
			//
			ThreadUID threadUID;

			//
			std::string threadName;

			//
			stdx::mask<ThreadPermissionFlags> permissions;
		};


		///<summary>
		///</summary>
		class ThreadRequestWrapper
		{
		public:
			explicit operator bool() const
			{
				return this->GetCode() != ThreadRequestCode::None;
			}

			bool CheckPriority( ThreadRequestCode requestCode )
			{
				return static_cast<Enum>( requestCode ) > static_cast<Enum>( this->GetCode() );
			}

			void Set( ThreadRequestCode requestCode )
			{
				this->_code.store( requestCode, std::memory_order_relaxed );
			}

			void Clear()
			{
				this->_code.store( ThreadRequestCode::None, std::memory_order_relaxed );
			}

			ThreadRequestCode GetCode() const
			{
				return this->_code.load( std::memory_order_relaxed );
			}

			ThreadRequestParameters& GetParameters()
			{
				return this->_parameters;
			}

			const ThreadRequestParameters& GetParameters() const
			{
				return this->_parameters;
			}

		private:
			//
			std::atomic<ThreadRequestCode> _code = ATOMIC_VAR_INIT( ThreadRequestCode::None );

			//
			ThreadRequestParameters _parameters;
		};


		///<summary>
		///</summary>
		template <class Lock>
		class ThreadRuntimeInterface : public Lockable<Lock, LockAccess::Relaxed>
		{
		public:
			ThreadRuntimeInterface( Thread& owner )
				: _owner( &owner )
			{ }

			/// Returns pointer to thread object which owns this interface.
			Thread* GetOwner() const
			{
				return this->_owner;
			}

		protected:
			// Pointer to the thread object which owns this interface.
			Thread* _owner;
		};


		///<summary>
		///</summary>
		class ThreadChildRefContainer : public ThreadRuntimeInterface<LightSharedMutex>
		{
			friend class Thread;

		public:
			ThreadChildRefContainer( Thread& owner );

			/// Returns internal list of currently registered child threads.
			const ChildThreadList& GetChildThreads() const;

			///
			Size_t GetChildThreadsNum() const;

			///
			Size_t GetChildThreadsNum( std::memory_order loadOrder ) const;

			//
			bool IsChildRegistered( Thread& thread ) const;

		friendapi:
			// Adds new thread to the list of child threads for the owning thread.
			ChildThreadRef AddChild( Thread& childThread );

			// Removes thread from the list of child threads.
			void RemoveChild( Thread& childThread, ChildThreadRef childThreadRef );

		private:
			//
			ChildThreadList _childThreads;

			//
			std::atomic<Size_t> _childThreadsNum;
		};


		namespace Internal
		{
			///
			ThreadUID CreateThreadUID( Thread* thread, Thread_index_t thrIndex, Thread_ref_id_t refID = 0 );

			///
			std::pair<ThreadLocalStorage*, Uint> GetThreadLocalStorageUnchecked();

			///
			ThreadLocalStorage* InitializeThreadLocalStorage();

			///
			void ReleaseThreadLocalStorage( ThreadLocalStorage* tls );
		}


	}
}


#endif /* __Exs_Core_ThreadInternal_H__ */
