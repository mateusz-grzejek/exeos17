
#ifndef __Exs_Core_CoreMessage_H__
#define __Exs_Core_CoreMessage_H__

#include "CoreMessageBase.h"


namespace Exs
{
	namespace Concrt
	{


		class CoreMessageReceiver;
		class CoreMessageSender;


		class EXS_LIBAPI_CORE CoreMessage : public MessageBase<CoreMessageHeader>
		{
			friend class CoreMessageResponse;
			friend class CoreMessageSender;

		public:
			CoreMessage( Message_code_t code = 0, MessagePriority priority = MessagePriority::Default );

			template <class M>
			M* As();

			template <class M>
			const M* As() const;

			CoreMessageResponse* CreateResponse( Message_code_t responseCode = 0 );

			CoreMessageResponse* GetResponse();

			CoreMessageResponse* WaitForResponse();

			MessageUID GetUID() const;

		friendapi:
			// @ CoreMessageSender
			//
			void UpdateSenderInfo( CoreMessageSender* sender, CoreMessageSenderID senderID );

			//
			void SetResponse( CoreMessageResponse& response );

			// @ CoreMessageResponse
			//
			CoreMessageSender* GetSender();

		private:
			//
			CoreMessageHeader _header;

			//
			CoreMessageSender* _sender;

			//
			CoreMessageResponse* _response;
		};


		template <class M>
		inline M* CoreMessage::As()
		{
			return dbgsafe_ptr_cast< M* >( this );
		}

		template <class M>
		inline const M* CoreMessage::As() const
		{
			return dbgsafe_ptr_cast< const M* >( this );
		}

		inline MessageUID CoreMessage::GetUID() const
		{
			return this->_header.uid;
		}

		inline void CoreMessage::UpdateSenderInfo( CoreMessageSender* sender, CoreMessageSenderID senderID )
		{
			this->_sender = sender;
			this->_header.senderID = senderID;
		}

		inline void CoreMessage::SetResponse( CoreMessageResponse& response )
		{
			this->_response = &response;
		}

		inline CoreMessageSender* CoreMessage::GetSender()
		{
			return this->_sender;
		}


		class EXS_LIBAPI_CORE CoreMessageResponse : public MessageRepsonseBase<CoreMessage, CoreMessageResponseHeader>
		{
		public:
			CoreMessageResponse( CoreMessage& message, Message_code_t responseCode = 0 );

			void SetReady();

		private:
			//
			CoreMessageSender* _sender;
		};


		template <class Base, class T>
		class MessagePayloadProxy : public Base
		{
		public:
			using PayloadType = MessagePayload<sizeof( T ), alignof( T )>;

		public:
			template <typename... Args>
			MessagePayloadProxy( Message_code_t code, MessagePriority priority, Args&& ...args )
				: Base( code, priority )
			{
				new ( this->_payload.data ) T( std::forward<Args>( args )... );
			}

			template <typename... Args>
			MessagePayloadProxy( CoreMessage& message, Message_code_t responseCode, Args&& ...args )
				: Base( message, responseCode )
			{
				new ( this->_payload.data ) T( std::forward<Args>( args )... );
			}

			virtual ~MessagePayloadProxy()
			{
				static_cast< T* >( this->_payload.data )->~T();
			}

			T* SetPayload()
			{
				return static_cast<T*>( this->_payload.data );
			}

			const T* GetPayload() const
			{
				return static_cast<const T*>( this->_payload.data );
			}

			Size_t GetPayloadSize() const
			{
				return this->_payload.dataSize;
			}

		private:
			//
			PayloadType _payload;
		};


		template <class T>
		class CoreMessageWithPayload : public MessagePayloadProxy<CoreMessage, T>
		{
		public:
			using BaseType = MessagePayloadProxy<CoreMessage, T>;

		public:
			template <typename... Args>
			CoreMessageWithPayload( Message_code_t code, MessagePriority priority, Args&& ...args )
				: BaseType( code, priority, std::forward<Args>( args )... )
			{ }
		};


		template <class T>
		class CoreMessageResponseWithPayload : public MessagePayloadProxy<CoreMessageResponse, T>
		{
		public:
			using BaseType = MessagePayloadProxy<CoreMessageResponse, T>;

		public:
			template <typename... Args>
			CoreMessageResponseWithPayload( Message_code_t code, MessagePriority priority, Args&& ...args )
				: BaseType( code, priority, std::forward<Args>( args )... )
			{ }
		};


		inline CoreMessageHandle CreateCoreMessage( Message_code_t code, MessagePriority priority = MessagePriority::Default )
		{
			return std::make_shared<CoreMessage>( code, priority );
		}

		template <class Message, class... Args>
		inline MessageHandle<Message> CreateCoreMessage( Args&& ...args )
		{
			return std::make_shared<Message>( std::forward<Args>(args)... );
		}


		inline CoreMessageResponseHandle CreateCoreMessageResponse( CoreMessage& message, Message_code_t responseCode )
		{
			return std::make_shared<CoreMessageResponse>( message, responseCode );
		}

		template <class Response, class... Args>
		inline MessageResponseHandle<Response> CreateCoreMessageResponse( CoreMessage& message, Args&& ...args )
		{
			return std::make_shared<Response>( message, std::forward<Args>( args )... );
		}


	}
}


#endif /* __Exs_Core_CoreMessage_H__ */
