
#ifndef __Exs_Core_Task_H__
#define __Exs_Core_Task_H__

#include "TaskCommon.h"


namespace Exs
{
	namespace Concrt
	{


		class Task
		{
		public:
			template <class TCallable>
			Task( TCallable callable, TaskIntensityClass intensityClass, TaskPriorityLevel priorityLevel = TaskPriorityLevel::Default )
			: Task( std::forward<TCallable>( callable ), intensityClass, static_cast<Uint32>( priorityLevel ) )
			{
			}

			template <class TCallable>
			Task( TCallable callable, TaskIntensityClass intensityClass, Uint32 priorityValue )
			: _callback( std::forward<TCallable>( callable ) )
			, _intensityClass( intensityClass )
			, _priorityValue( priorityValue )
			{
			}

			void Execute()
			{
				this->_callback.Run();
			}

			TaskIntensityClass GetIntensityClass() const
			{
				return this->_intensityClass;
			}

			Uint32 GetPriorityValue() const
			{
				return this->_priorityValue;
			}

		private:
			//
			TaskAsyncCallback _callback;

			//
			TaskIntensityClass _intensityClass;

			//
			Uint32 _priorityValue;
		};


	}
}


#endif /* __Exs_Core_Task_H__ */
