
#ifndef __Exs_Core_CoreMessageBase_H__
#define __Exs_Core_CoreMessageBase_H__

#include "ConcrtCommon.h"


namespace Exs
{
	namespace Config
	{

		enum : Size_t
		{
			//
			CCRT_MSG_Message_Payload_Max_Size = 64,

			//
			CCRT_MSG_Default_Message_Wait_Timeout = 16,

			//
			CCRT_MSG_Default_Message_Response_Wait_Timeout = 50,

			//
			CCRT_MSG_Max_Registered_Receivers_Num = CCRT_THR_Max_Active_Threads_Num
		};

	}
}

namespace Exs
{
	namespace Concrt
	{


		class CoreMessage;
		class CoreMessageQueue;
		class CoreMessageResponse;


		// Value type used to store IDs of receivers in core messaging system.
		using Core_message_receiver_id_value_t = U32ID;

		// Value type used to store IDs of senders in core messaging system.
		using Core_message_sender_id_value_t = U32ID;

		// Represents receiver ID. Currently typedef to value type, may be extended like ThreadUID.
		using CoreMessageReceiverID = Core_message_receiver_id_value_t;

		// Represents sender ID. Currently typedef to value type, may be extended like ThreadUID.
		using CoreMessageSenderID = Core_message_sender_id_value_t;

		// Forward declaration of CoreMessageHandle.
		using CoreMessageHandle = MessageHandle<CoreMessage>;

		// Forward declaration of CoreMessageHandle.
		using CoreMessageResponseHandle = MessageResponseHandle<CoreMessageResponse>;

		//
		struct CoreMessageHeader : public MessageHeader
		{
			MessageUID uid;

			CoreMessageSenderID senderID;
		};

		//
		struct CoreMessageResponseHeader : public MessageResponseHeader
		{
			CoreMessage* respondedMessage;
		};

		//
		using CoreMessagePayload = MessagePayload<Config::CCRT_MSG_Message_Payload_Max_Size, EXS_MEMORY_BASE_ALIGNMENT>;


		enum : Enum
		{
			RSC_Msg_Message_Discarded,
			RSC_Msg_Message_Suspended,
			RSC_Msg_Receiver_Inactive,
			RSC_Msg_Invalid_Recipient
		};


		enum CoreMessageSendOptionFlags : Enum
		{
			// Message is delivered only if the receiver is registered and active. If delivery fails
			// for some reason, message is not enqueued (as "suspended" message), but discarded instead.
			CoreMessageSendOption_Immediate = 0x0001,

			// Indicates whether synchronization should be allowed in case receiver is not registered
			// or not active. If this is set to false, message is discarded if immediate delivery fails.
			CoreMessageSendOption_Sync_Wait_For_Inactive_Recipient = 0x0400,

			//
			CoreMessageSendOption_Sync_Wait_Implicit = 0x0800,

			// Default flags used to send messages.
			CoreMessageSendOption_Default = 0
		};


		enum : Core_message_receiver_id_value_t
		{
			///
			Core_Message_Receiver_ID_Invalid = ID32_Invalid,
			Core_Message_Receiver_ID_None = ID32_None
		};


		///<summary>
		///</summary>
		struct CoreMessageSendRequest
		{
			//
			CoreMessageReceiverID receiverID = Core_Message_Receiver_ID_None;

			//
			stdx::mask<CoreMessageSendOptionFlags> sendOptions;
		};


		///<summary>
		///</summary>
		struct CoreSyncMessageSendRequest : public CoreMessageSendRequest
		{
			using WaitTimeoutUnit = Milliseconds;

			//
			WaitTimeoutUnit syncWaitTimeoutMs;
		};


		///<summary>
		///</summary>
		struct CoreMessageDispatchRequest
		{
			//
			CoreMessageReceiverID receiverID = Core_Message_Receiver_ID_None;

			//
			stdx::mask<CoreMessageSendOptionFlags> sendOptions = CoreMessageSendOption_Default;
		};


		///<summary>
		///</summary>
		struct CoreMessageSendResult
		{
		public:
			//
			Result result;

		public:
			CoreMessageSendResult( Result result )
				: result( result )
			{ }

			operator const Result&( ) const
			{
				return this->result;
			}

			explicit operator bool() const
			{
				return this->result == RSC_Success;
			}
		};


		///<summary>
		///</summary>
		struct CoreSyncMessageSendResult
		{
		public:
			//
			Result result;

		public:
			CoreSyncMessageSendResult( Result result )
				: result( result )
			{ }

			operator const Result&( ) const
			{
				return this->result;
			}

			explicit operator bool() const
			{
				return this->result == RSC_Success;
			}
		};


		///<summary>
		///</summary>
		struct CoreMessageResponseResult
		{
		public:
			//
			Result result;

			//
			CoreMessageResponse* response;

		public:
			CoreMessageResponseResult( Result result, CoreMessageResponse* response = nullptr )
				: result( result )
				, response( response )
			{ }

			operator const Result&( ) const
			{
				return this->result;
			}

			explicit operator bool() const
			{
				return this->response != nullptr;
			}
		};


		// Helper class which implements generation of global message UIDs.
		struct CoreMessageUIDGen
		{
			// UID counter value is stored within UID on sizeof(UID)-2 bytes (16 bits are for message class).
			static constexpr Message_uid_value_t maxUIDCounter = stdx::limits<Message_uid_value_t>::max_value >> 16;

			//
			static inline MessageUID GenerateGlobalMessageUID( Message_code_t messageCode )
			{
				static std::atomic<Message_uid_value_t> uidCounter = ATOMIC_VAR_INIT( 0 );

				// Message class, higher 16 bits of the UID.
				auto messageClass = EnumDef::GetMessageClassFromCode( messageCode );

				//
				messageClass = ( messageClass & 0xFFFF );

				// Current value of the UID counter, lower 48 bits of the UID.
				auto uidNumValue = uidCounter.fetch_add( 1, std::memory_order_relaxed );

				// Combine them to get the UID.
				return ( static_cast<Message_uid_value_t>( messageClass ) << 48 ) | ( uidNumValue & maxUIDCounter );
			};
		};


	}


}


#endif /* __Exs_Core_CoreMessageBase_H__ */
