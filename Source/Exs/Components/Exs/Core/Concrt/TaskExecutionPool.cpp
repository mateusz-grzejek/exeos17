
#include "Precomp.h"
#include <Exs/Core/Concrt/TaskExecutionPool.h>


namespace Exs
{
	namespace Concrt
	{


		static constexpr auto tepPriorityLevelMaxValue = static_cast<Uint32>( TaskPriorityLevel::Max );
		static constexpr auto tepPriorityLevelMinValue = static_cast<Uint32>( TaskPriorityLevel::Min );
		static constexpr auto tepIntensityClassMaxValue = static_cast<Uint32>( TaskIntensityClass::Max );
		static constexpr auto tepIntensityClassMinValue = static_cast<Uint32>( TaskIntensityClass::Min );
		static constexpr auto tepIntensityValueMultiplier = static_cast<float>( tepPriorityLevelMaxValue ) / static_cast<float>( tepIntensityClassMaxValue );


		static constexpr auto ComputeTaskHeuristicPriority( Uint32 priorityValue, Uint32 priorityWeight, Uint32 intensityClassValue, Uint32 intensityClassWeight )
		{
			return ( ( priorityValue * priorityWeight ) + (Uint32)( tepIntensityValueMultiplier * intensityClassValue * intensityClassWeight ) ) / ( priorityWeight + intensityClassWeight );
		}





		void TaskExecutionPool::SetElevatedPriorityParameters( Uint32 thresholdValuePercentage, Uint32 favorCountLimit )
		{
			thresholdValuePercentage = std::min( std::max( thresholdValuePercentage, 0U ), 100U );

			this->_hmetrics.elevatedPriorityThresholdValue = this->_heuristicPriorityMaxValue * thresholdValuePercentage / 100;
			this->_hmetrics.elevatedPriorityFavorCountLimit = favorCountLimit;
		}


		void TaskExecutionPool::SetHeuristicPriorityWeights( Uint32 priorityLevelWeight, Uint32 intensityClassWeight )
		{
			const auto currentElevatedPriorityThresholdPercentage = this->_hmetrics.elevatedPriorityThresholdValue / this->_heuristicPriorityMaxValue;

			this->_hmetrics.intensityClassWeight = intensityClassWeight;
			this->_hmetrics.priorityLevelWeight = priorityLevelWeight;

			this->_heuristicPriorityMaxValue = ComputeTaskHeuristicPriority( tepPriorityLevelMaxValue, priorityLevelWeight, tepIntensityClassMaxValue, intensityClassWeight );

			this->SetElevatedPriorityParameters( currentElevatedPriorityThresholdPercentage, this->_hmetrics.elevatedPriorityFavorCountLimit );
		}


		Uint32 TaskExecutionPool::_ComputeHeuristicPriorityValue( Uint32 priorityValue, TaskIntensityClass intensityClass )
		{
			Uint32 checkedPriorityValue = std::min( std::max( priorityValue, tepPriorityLevelMinValue ), tepPriorityLevelMaxValue );
			Uint32 checkedIntensityClassValue = std::min( std::max( static_cast<Uint32>( intensityClass ), tepIntensityClassMinValue ), tepIntensityClassMaxValue );

			return ComputeTaskHeuristicPriority( checkedPriorityValue, this->_hmetrics.priorityLevelWeight, checkedIntensityClassValue, this->_hmetrics.intensityClassWeight );
		}


		bool TaskExecutionPool::_IsElevatedPriorityTask( Uint32 heuristicPriorityValue )
		{
			return heuristicPriorityValue >= this->_hmetrics.elevatedPriorityThresholdValue;
		}


	}
}
