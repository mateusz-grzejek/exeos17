
#ifndef __Exs_Core_ThreadMessageCommon_H__
#define __Exs_Core_ThreadMessageCommon_H__

#include "CoreMessageBase.h"


namespace Exs
{
	namespace Concrt
	{


		///<summary>
		///</summary>
		struct ThreadMessageSendRequest
		{
			//
			Thread_ref_id_t threadRefID = 0;

			//
			ThreadUID threadUID = Thread_UID_None;

			//
			stdx::mask<CoreMessageSendOptionFlags> sendOptions;
		};


		///<summary>
		///</summary>
		struct ThreadSyncMessageSendRequest : public ThreadMessageSendRequest
		{
			struct ResponseSyncInfo
			{
				//
				Milliseconds waitTimeout;
			};

			//
			ResponseSyncInfo responseSyncInfo;
		};


	}
}


#endif /* __Exs_Core_ThreadMessageCommon_H__ */
