
#include "Precomp.h"
#include <Exs/Core/Concrt/SyncObject.h>
#include <Exs/Core/Concrt/Thread.h>
#include <Exs/Core/Concrt/ThreadSystemManager.h>


namespace Exs
{
	namespace Concrt
	{


		bool SyncObject::BeginWait( ThreadWaitSyncState& threadWaitSyncState )
		{
			ExsDebugCode({
				if ( threadWaitSyncState.thread != nullptr )
				{
					auto* currentThread = &( CurrentThread::GetThreadObject() );
					if ( threadWaitSyncState.thread != currentThread  )
					{
						throw 0;
					}
				}
			});

			if ( threadWaitSyncState.thread == nullptr )
			{
				threadWaitSyncState.thread = &( CurrentThread::GetThreadObject() );
			}

			threadWaitSyncState.threadSharedSyncContext = this->_threadSystemManager->GetThreadSyncContext( *( threadWaitSyncState.thread ) );

			if ( !this->OnWaitBegin( threadWaitSyncState ) )
			{
				return false;
			}

			this->_UpdateThreadState( threadWaitSyncState );

			return true;
		}


		void SyncObject::EndWait( ThreadWaitSyncState& threadWaitSyncState )
		{
			this->_ResetThreadState( threadWaitSyncState );

			this->OnWaitEnd( threadWaitSyncState );

			threadWaitSyncState.thread = nullptr;
			threadWaitSyncState.threadSharedSyncContext = nullptr;

		}


		WaitResult SyncObject::GetWaitResult( ThreadWaitSyncState& threadWaitSyncState, bool waitTimeout )
		{
			auto waitResult = WaitResult::Failure;

			if ( waitTimeout )
			{
				waitResult = WaitResult::Timeout;
			}
			else if ( threadWaitSyncState.IsInterruptFlagSet() )
			{
				waitResult = WaitResult::Interrupt;
			}
			else if ( threadWaitSyncState.IsNotifyFlagSet() )
			{
				waitResult = WaitResult::Signaled;
			}

			return waitResult;
		}


		void SyncObject::_UpdateThreadState( ThreadWaitSyncState& threadWaitSyncState )
		{

		}


		void SyncObject::_ResetThreadState( ThreadWaitSyncState& threadWaitSyncState )
		{

		}


	}
}
