
#ifndef __Exs_Core_ThreadPoolPerf_H__
#define __Exs_Core_ThreadPoolPerf_H__

#include "ThreadPoolCommon.h"


namespace Exs
{
	namespace Concrt
	{


		class ThreadPoolPerfStatController
		{
		public:
			void BeginTimeUnit();

			void EndTimeUnit();

			void OnBeginItemExecution( ThreadPoolWorkerThread& workerThread, Thread_index_t workerIndex );

			void OnEndItemExecution( ThreadPoolWorkerThread& workerThread, Thread_index_t workerIndex );

		private:
			struct GlobalData
			{

			};

			struct PerWorkerThreadData
			{
				Size_t totalExecutedItemsNum = 0;

				Size_t throughputMaxValue = 0;

				Size_t throughputMinValue = 0;

				Nanoseconds singleItemMaxExecutionTime = 0;

				Nanoseconds singleItemMinExecutionTime = 0;

				Nanoseconds totalExecutionTime = 0;
			};

			struct PerWorkerThreadPerfContext
			{
			};

			//
			using PerWorkerThreadDataArray = std::array<PerWorkerThreadData, Config::CCRT_THR_Max_Thread_Pool_Size>;

		private:
			//
			LightSharedMutex _accessMutex;

			//
			PerWorkerThreadDataArray _perWorkerThreadData;
		};


	}
}


#endif /* __Exs_Core_ThreadPoolPerf_H__ */
