
#ifndef __Exs_Core_ThreadSyncObject_H__
#define __Exs_Core_ThreadSyncObject_H__

#include "SyncObjectImpl.h"


namespace Exs
{
	namespace Concrt
	{


		/**
		 *
		 */
		class ThreadSyncObjectImpl : public SyncObjectImplBase
		{
		protected:
			/// @see SyncObjectBase::SyncObjectBase( ThreadSystemManager& threadSystemManager )
			explicit ThreadSyncObjectImpl( Thread& thread )
			: _owner( &thread )
			{ }

			bool OnWaitBeginImpl( ThreadWaitSyncState& threadWaitSyncState )
			{
				if ( threadWaitSyncState.thread != this->_owner )
				{
					return false;
				}

				this->currentWaitState = &threadWaitSyncState;

				return true;
			}

			void OnWaitEndImpl( ThreadWaitSyncState& threadWaitSyncState )
			{
				this->currentWaitState = nullptr;
			}

			template <class TLock>
			Size_t AbortImpl( TLock& lock )
			{
				return this->_Signal( lock, SyncObjectSignalFlag_Notify );
			}

			template <class TLock>
			bool InterruptImpl( TLock& lock, Thread& thread )
			{
				return ( &thread == this->_owner ) && ( this->_Signal( lock, SyncObjectSignalFlag_Interrupt ) != 0 );
			}

			template <class TLock>
			bool NotifyOneImpl( TLock& lock )
			{
				return this->_Signal( lock, SyncObjectSignalFlag_Notify ) != 0;
			}

			template <class TLock>
			Size_t NotifyAllImpl( TLock& lock )
			{
				return this->_Signal( lock, SyncObjectSignalFlag_Notify );
			}

		private:
			template <class TLock>
			Size_t _Signal( TLock& lock, SyncObjectSignalFlags signalFlags )
			{
				if ( this->currentWaitState != nullptr )
				{
					//
					SyncObjectImplBase::SignalThread<TLock>( *( this->currentWaitState ), signalFlags );

					//
					return 1;
				}

				return 0;
			}

		private:
			//
			Thread* _owner;

			//
			ThreadWaitSyncState* currentWaitState;
		};


		/**
		 *
		 */
		template <class TMutex = SyncMutex>
		class ThreadSyncObject : public SyncObjectImplProxy<TMutex, ThreadSyncObjectImpl>
		{
		public:
			using BaseImplProxyType = SyncObjectImplProxy<TMutex, ThreadSyncObjectImpl>;

		public:
			/**
			 *
			 * @param thread
			 */
			explicit ThreadSyncObject( Thread& thread )
			: BaseImplProxyType( SyncObjectImplBase::GetThreadSystemManager( thread ), thread )
			{ }

			/**
			 *
			 * @param threadSystemManager
			 * @param thread
			 */
			ThreadSyncObject( ThreadSystemManager& threadSystemManager, Thread& thread )
			: BaseImplProxyType( threadSystemManager, thread )
			{ }
		};


	}
}


#endif /* __Exs_Core_ThreadSyncObject_H__ */

