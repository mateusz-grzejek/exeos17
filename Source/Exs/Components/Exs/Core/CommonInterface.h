
#ifndef __Exs_Core_DriverSpecificInterface_H__
#define __Exs_Core_DriverSpecificInterface_H__

#include "Core.h"


namespace Exs
{


	template <class TBase>
	class ComponentInterface
	{
	};


		template <class TBase>
	class DriverSpecificInterface : public ComponentInterface<TBase>
	{
	public:
		virtual ~DriverSpecificInterface() = default;

		template <class T>
		T* As()
		{
			return dbgsafe_ptr_cast<T*>( this );
		}

		template <class T>
		const T* As() const
		{
			return dbgsafe_ptr_cast<const T*>( this );
		}

		TBase* AsBase()
		{
			return dbgsafe_ptr_cast<T*>( this );
		}

		const TBase* AsBase() const
		{
			return dbgsafe_ptr_cast<const T*>( this );
		}
	};


}


#endif /* __Exs_Core_DriverSpecificInterface_H__ */
