
#ifndef __Exs_Core_StateMachine_H__
#define __Exs_Core_StateMachine_H__

#include "Core.h"
#include <stdx/assoc_array.h>


namespace Exs
{


	template <typename TState, typename... Args>
	using StateTransitionCallback = std::function<void(Args&&...)>;

	template <typename TState>
	using StateTransitionInfoCallback = std::function<void( TState, TState )>;


	template <typename TState>
	struct StateTransition
	{
	public:
		//
		TState currentState;

		//
		TState nextState;

	public:
		StateTransition( TState current, TState next )
			: currentState( current )
			, nextState( next )
		{ }

		StateTransition( const std::pair<TState, TState>& states )
			: currentState( states.first )
			, nextState( states.second )
		{ }

		StateTransition( const std::initializer_list<TState>& stateList )
			: currentState( *( stateList.begin() ) )
			, nextState( *( stateList.begin() + 1 ) )
		{ }

		bool operator==( const StateTransition& rhs ) const
		{
			return ( this->currentState == rhs.currentState ) && ( this->nextState == rhs.nextState );
		}

		bool operator!=( const StateTransition& rhs ) const
		{
			return ( this->currentState != rhs.currentState ) || ( this->nextState != rhs.nextState );
		}
	};


	template <typename TState, typename TCallback>
	struct StateTransitionDesc
	{
		//
		using Callback = TCallback;

		// List of callbacks to call when transition is applied. To provide required functionality,
		// this must be a container which does not invalidate iterators across insertions and removals.
		using CallbackList = std::list<Callback>;

		//
		using CallbackRef = typename CallbackList::iterator;

		// List of callbacks.
		CallbackList callbackList;
	};


	template <typename TState>
	struct StateTransitionDesc<TState, void>
	{
	};


	template <typename TState, typename TCallback>
	class StateMachine
	{
	public:
		using Transition = StateTransition<TState>;
		using TransitionDesc = StateTransitionDesc<TState, TCallback>;
		using TransitionCallback = typename TransitionDesc::Callback;
		using TransitionCallbackRef = typename TransitionDesc::CallbackRef;

	private:
		struct TransitionSortCmp
		{
			bool operator()( const Transition& left, const Transition& right ) const
			{
				return ( left.currentState < right.currentState ) || ( ( left.currentState == right.currentState ) && ( left.nextState < right.nextState ) );
			}
		};

		using TransitionMap = stdx::assoc_array<Transition, TransitionDesc, TransitionSortCmp>;

	public:
		StateMachine( TState initialState )
			: _currentState( initialState )
		{ }

		void AddTransition( TState currentState, TState nextState )
		{
			this->_transitionMap[{currentState, nextState}] = TransitionDesc{};
		}

		void AddTransition( const Transition& transition )
		{
			this->_transitionMap[transition] = TransitionDesc{};
		}

		TransitionCallbackRef AddTransitionCallback( const Transition& transition, TransitionCallback callback )
		{
			auto& transitionDescRef = this->_transitionMap[transition];
			auto& callbackList = transitionDescRef->callbackList;

			return callbackList.insert( callbackList.end(), std::move( callback ) );
		}

		bool SetState( TState nextState )
		{
			auto transitionDescRef = this->_transitionMap.find( { this->_currentState, nextState } );
			if ( transitionDescRef == this->_transitionMap.end() )
			{
				return false;
			}

			this->_ApplyTransition( transitionDescRef->key, transitionDescRef->value );

			return true;
		}

		TState GetCurrentState() const
		{
			return this->_currentState;
		}

	private:
		void _ApplyTransition( const Transition& transition, const TransitionDesc& transitionDesc )
		{
			for ( auto& callback : transitionDesc.callbackList )
			{
				try
				{
					if ( callback )
					{
						callback();
					}
				}
				catch ( ... )
				{
					//..
				}
			}

			this->_currentState = transition.nextState;
		}

	private:
		TState _currentState;
		TransitionMap _transitionMap;
	};


	template <typename TState>
	class StateMachine<TState, void>
	{
	public:
		using Transition = StateTransition<TState>;
		using TransitionDesc = StateTransitionDesc<TState, void>;

	private:
		struct TransitionSortCmp
		{
			bool operator()( const Transition& left, const Transition& right ) const
			{
				return ( left.currentState < right.currentState ) || ( ( left.currentState == right.currentState ) && ( left.nextState < right.nextState ) );
			}
		};

		using TransitionMap = stdx::assoc_array<Transition, TransitionDesc, TransitionSortCmp>;

	public:
		StateMachine( TState initialState )
			: _currentState( initialState )
		{ }

		void AddTransition( TState currentState, TState nextState )
		{
			this->_transitionMap[{currentState, nextState}] = TransitionDesc{};
		}

		void AddTransition( const Transition& transition )
		{
			this->_transitionMap[transition] = TransitionDesc{};
		}

		bool SetState( TState nextState )
		{
			auto transitionDescRef = this->_transitionMap.find( { this->_currentState, nextState } );
			if ( transitionDescRef == this->_transitionMap.end() )
			{
				return false;
			}

			this->_ApplyTransition( transitionDescRef->key, transitionDescRef->value );

			return true;
		}

		TState GetCurrentState() const
		{
			return this->_currentState;
		}

	private:
		void _ApplyTransition( const Transition& transition, const TransitionDesc& transitionDesc )
		{
			this->_currentState = transition.nextState;
		}

	private:
		TState _currentState;
		TransitionMap _transitionMap;
	};


	template <typename TState, typename TCallback>
	class StateTransitionLocalGuard
	{
	public:
		using StateMachineType = StateMachine<TState, TCallback>;

	public:
		StateTransitionLocalGuard( StateMachineType& stateMachine, TState nextState, TState restoreState )
			: _stateMachine( &stateMachine )
		{
			if ( this->_stateMachine->SetState( nextState ) )
			{
				this->_restore = true;
			}
		}

		~StateTransitionLocalGuard()
		{
			if ( this->_restore )
			{
				this->_stateMachine->SetState( this->_restoreState );
				this->_restore = false;
			}
		}

	private:
		StateMachineType* _stateMachine;

		TState _restoreState;

		bool _restore;
	};


}


#endif /* __Exs_Core_StateMachine_H__ */
