
#include <Exs/Core/Plugins/Plugin.h>


namespace Exs
{


	bool PluginHandle::AddPluginActiveRef(Plugin* plugin)
	{
		Uint32 refCount = plugin->AddUseRef();
		return refCount > 0;
	}


	void PluginHandle::RemovePluginActiveRef(Plugin* plugin)
	{
		plugin->RemoveUseRef();
	}


	Uint32 PluginHandle::GetPluginActiveRef(Plugin* plugin)
	{
		return plugin->GetUseCount();
	}


}
