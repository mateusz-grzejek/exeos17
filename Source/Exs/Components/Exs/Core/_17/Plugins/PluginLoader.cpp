
#include <Exs/Core/Plugins/PluginLoader.h>
#include <Exs/Core/Plugins/Plugin.h>
#include <Exs/Core/Plugins/PluginManager.h>


namespace Exs
{


	Result PluginValidator::ValidateModuleBaseInfo(const PluginModuleBaseInfo* moduleBaseInfo)
	{
		return ExsResult(RSC_Success);
	}


	Result PluginValidator::ValidateModuleInterface(const PluginModuleInterface* moduleInterface)
	{
		return ExsResult(RSC_Success);
	}
	


	
	PluginLoader::PluginLoader(PluginSystem* pluginSystem)
	: _pluginSystem(pluginSystem)
	{ }


	PluginLoader::~PluginLoader()
	{ }


	Plugin* PluginLoader::Initialize(const PluginBaseInfo& pluginBaseInfo, PluginManager* manager, PluginValidator* validator)
	{
		const char* pluginName = pluginBaseInfo.moduleBaseInfo->name;

		// Validate module data and interface using specified validator object.
		Result validationResult = this->Validate(pluginBaseInfo.moduleBaseInfo, pluginBaseInfo.moduleInterface, validator);

		if (validationResult != RSC_Success)
		{
			ExsTraceError(TRC_Core_Plugins, "%s: validation has failed.", pluginName);
			return nullptr;
		}

		// Create plugin object using procedure exposed by the module. This executes inside the DLL library.
		Plugin* pluginPtr = pluginBaseInfo.moduleInterface->onInitProc(&(pluginBaseInfo.dllLibray), manager);

		if (pluginPtr == nullptr)
		{
			ExsTraceError(TRC_Core_Plugins, "%s: initialization has failed (onInitProc() returned NULL).", pluginName);
			return nullptr;
		}

		this->OnPluginInitialize(pluginBaseInfo, pluginPtr);
		
		return pluginPtr;
	}


	void PluginLoader::Release(const PluginBaseInfo& pluginBaseInfo, Plugin* plugin)
	{
		this->OnPluginRelease(pluginBaseInfo, plugin);
		
		// Release plugin object using procedure exposed by the module. This executes inside the DLL library.
		pluginBaseInfo.moduleInterface->onReleaseProc(&(pluginBaseInfo.dllLibray), plugin);
	}


	Result PluginLoader::LoadPlugin(const std::string& pluginFile, PluginBaseInfo* pluginBaseInfo)
	{
		// Load dynamic library with plugin.
		DynamicLibrary pluginDLL = DynamicLibrary::Load(pluginFile, DLLPathType::Base);

		if (!pluginDLL.IsLoaded())
		{
			ExsTraceNotification(TRC_Core_Plugins, "%s: not found.", pluginFile.c_str());
			return ExsResult(RSC_Err_DLL_Missing);
		}

		// Retrieve plugin data, containing base information about plugin: its name, version, supported architecture, etc.
		// It is used to validate and initialize plugin object.
		const PluginModuleBaseInfo* pluginModuleBaseInfo =
			pluginDLL.RetrieveSymbolAs<PluginModuleBaseInfo*>(EXS_PLUGIN_MODULE_BASE_INFO_REF_NAME_STR);

		// Retrieve plugin interface, containing procedures exposed by the library: init and release proc, etc.
		// It is used to create and destroy plugin objects (it is the responsibility of the module to do it).
		const PluginModuleInterface* pluginModuleInterface =
			pluginDLL.RetrieveSymbolAs<PluginModuleInterface*>(EXS_PLUGIN_MODULE_INTERFACE_REF_NAME_STR);

		// If any of the above is missing, plugin is considered invalid.
		if ((pluginModuleBaseInfo == nullptr) || (pluginModuleInterface == nullptr))
		{
			ExsTraceWarning(TRC_Core_Plugins, "%s: missing module data (info and/or interface).", pluginFile.c_str());
			return ExsResult(RSC_Plugin_Err_Missing_Module_Data);
		}
		
		pluginBaseInfo->dllLibray = pluginDLL;
		pluginBaseInfo->moduleBaseInfo = pluginModuleBaseInfo;
		pluginBaseInfo->moduleInterface = pluginModuleInterface;

		return ExsResult(RSC_Success);
	}


	Result PluginLoader::Validate(const PluginModuleBaseInfo* moduleBaseInfo, const PluginModuleInterface* moduleInterface, PluginValidator* validator)
	{
		// Required data validation:
		// - target architecture.
		// Required module validation:
		// - init proc may not be NULL,
		// - release proc may not be NULL.

		if (moduleBaseInfo->archID != EXS_TARGET_ARCHITECTURE)
			return ExsResult(RSC_Err_Arch_Not_Compatible);

		if (moduleInterface->onInitProc == nullptr)
			return ExsResult(RSC_Err_DLL_Module_Invalid);
		
		if (moduleInterface->onReleaseProc == nullptr)
			return ExsResult(RSC_Err_DLL_Module_Invalid);

		// If validator is not empty, perform additional validation.
		if (validator != nullptr)
		{
			Result dataValidationResult = validator->ValidateModuleBaseInfo(moduleBaseInfo);
			if (dataValidationResult != RSC_Success)
			{
				ExsTraceError(TRC_Core_Plugins, "%s: invalid plugin module data.", moduleBaseInfo->name);
				return dataValidationResult;
			}

			Result interfaceValidationResult = validator->ValidateModuleInterface(moduleInterface);
			if (interfaceValidationResult != RSC_Success)
			{
				ExsTraceError(TRC_Core_Plugins, "%s: invalid plugin module interface.", moduleBaseInfo->name);
				return interfaceValidationResult;
			}
		}

		return ExsResult(RSC_Success);
	}


	void PluginLoader::OnPluginInitialize(const PluginBaseInfo& pluginBaseInfo, Plugin* plugin)
	{ }


	void PluginLoader::OnPluginRelease(const PluginBaseInfo& pluginBaseInfo, Plugin* plugin)
	{ }


}
