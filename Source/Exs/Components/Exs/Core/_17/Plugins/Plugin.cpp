
#include <Exs/Core/Plugins/Plugin.h>
#include <Exs/Core/Plugins/PluginSystem.h>
#include <Exs/Core/Plugins/PluginManager.h>


namespace Exs
{


	Plugin::Plugin(PluginManager* manager)
	: _pluginSystem(manager->GetPluginSystem())
	, _manager(manager)
	, _id(Invalid_Plugin_ID)
	, _type(manager->GetType())
	, _useCount(1)
	{ }


	Plugin::~Plugin()
	{ }


	Result Plugin::Unload()
	{
		Result unloadResult = this->_pluginSystem->UnloadPlugin(this->_id);

		if (unloadResult != RSC_Success)
			return unloadResult;

		return ExsResult(RSC_Success);
	}


}
