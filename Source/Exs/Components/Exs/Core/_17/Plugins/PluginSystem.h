
#ifndef __Exs_Core_PluginSystem_H__
#define __Exs_Core_PluginSystem_H__

#include "PluginBase.h"
#include <stdx/assoc_array.h>


namespace Exs
{


	class PluginManager;
	class PluginValidator;

	ExsDeclareRefPtrClass(PluginManager);

	
	///<summary>
	/// Represents plugin system. It is responsible for registering and storing managers, plugins loading and serves as
	/// the main core of plugin system. There should be always only one instance of this class with application lifetime
	/// (this restriction should be ensured byt he user).
	///</summary>
	class EXS_LIBCLASS_CORE PluginSystem
	{
		friend class PluginManager;

	private:
		struct InternalPluginData
		{
			PluginHandle handle; // Handle to the plugin.
			std::string name; // Name of the plugin.
			PluginLoader* loader; // Loader which was used for plugin loading and will be used for unloading.
			PluginManager* manager; // Parent component of a plugin.
			Plugin* plugin; // Pointer to plugin itself.
			PluginBaseInfo refData; // Reference data (containing its module, exposed interface, etc.).
			std::vector<std::string> keywords; // Keywords used to locate plugin.
		};

	public:
		typedef stdx::assoc_array<PluginType, PluginManagerRefPtr> ManagerList;
		typedef stdx::assoc_array<PluginID, InternalPluginData> PluginList;
		typedef std::vector<PluginID> OrderedPluginList;

	private:
		ManagerList                    _managers;
		PluginList                     _plugins;
		OrderedPluginList              _pluginsLoadOrderList;
		std::unique_ptr<PluginLoader>  _defaultLoader;

	public:
		PluginSystem();
		~PluginSystem();
		
		///<summary>
		///</summary>
		template <PluginType Plugin_type_tag, class... Args>
		typename PluginTypeProperties<Plugin_type_tag>::ManagerType* AddManager(Args&&... args);
		
		///<summary>
		/// Loads plugin from specified file and returns handle to it
		///</summary>
		std::pair<PluginHandle, Result> LoadPlugin(const char* pluginFile, PluginLoader* loader = nullptr, PluginValidator* validator = nullptr);
		
		///<summary>
		///</summary>
		Result UnloadPlugin(PluginID pluginID);
		
		///<summary>
		///</summary>
		Result UnloadAll();
		
		///<summary>
		///</summary>
		PluginManager* GetManager(PluginType type) const;
		
		///<summary>
		///</summary>
		PluginHandle GetPlugin(PluginID pluginID) const;
		
		///<summary>
		///</summary>
		PluginHandle GetPlugin(const char* name) const;

		Size_t GetLoadedPluginsNum() const;
		Size_t GetRegisteredManagersNum() const;
		
		bool IsPluginLoaded(PluginID pluginID) const;
		bool IsManagerRegistered(PluginType type) const;
	};
	

	template <PluginType Plugin_type_tag, class... Args>
	inline typename PluginTypeProperties<Plugin_type_tag>::ManagerType* PluginSystem::AddManager(Args&&... args)
	{
		typedef typename PluginTypeProperties<Plugin_type_tag>::ManagerType ManagerType;

		if (PluginManager* manager = this->GetManager(Plugin_type_tag))
		{
			ExsTraceWarning(TRC_Core_Plugins, "");
			return dbgsafe_ptr_cast<ManagerType*>(manager);
		}

		auto managerPtr = std::make_shared<ManagerType>(this, std::forward<Args>(args)...);
		this->_managers.insert(Plugin_type_tag, managerPtr);

		return managerPtr.get();
	}

	inline Size_t PluginSystem::GetLoadedPluginsNum() const
	{
		return this->_plugins.size();
	}

	inline Size_t PluginSystem::GetRegisteredManagersNum() const
	{
		return this->_managers.size();
	}

	inline bool PluginSystem::IsPluginLoaded(PluginID pluginID) const
	{
		auto pluginHandle = this->GetPlugin(pluginID);
		return pluginHandle ? true : false;
	}

	inline bool PluginSystem::IsManagerRegistered(PluginType type) const
	{
		const PluginManager* manager = this->GetManager(type);
		return manager != nullptr;
	}


}


#endif /* __Exs_Core_PluginSystem_H__ */
