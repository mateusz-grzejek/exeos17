
#ifndef __Exs_Core_SharedState_H__
#define __Exs_Core_SharedState_H__


namespace Exs
{


	///<summary>
	/// Common base type for shared state. Shared state is a class/struct which encapsulates some shared data
	/// used by a specific subsystem of the engine's component (one or more). Basically, it serves as a container
	/// for data that is used across certain functionality. For example, CoreThreadSystemState is a shared state
	/// used within threading system. It contains instance of ThreadSystemManager, dispatcher of core messages
	/// and other threading-related objects. CoreThreadSystemState is required by many objects, including Thread,
	/// CoreMessenger and most of the public threading API.
	///
	/// The main reasoning behind shared states is to avoid using singletons - instead of having ThreadSystemManager
	/// singleton, CoreMessageDispatcher singleton, etc., we ewncapsulate those objects in a shared state, which is
	/// created on certain point of initialization and passed to all objects/functions which require this state.
	/// This also makes it easier to control the lifetime of a state - as long as there is at least one active handle,
	/// it will stay alive (so, for example, TSM can be safely used by dameon threads, as every thread object holds
	/// active handle to CoreThreadSystemState).
	///
	/// TODO: mutex.
	///</summary>
	struct ComponentSubsystemSharedState
	{
		using MutexType = SharedMutex;

		/// Lock used to acquire unique access to the data. Every sub-type of shared data is assumed
		/// to be possibly shared in every meaning of this word, including access from multiple threads.
		mutable MutexType accessMutex;
	};


	///<summary>
	///</summary>
	template <class Mutex = LightMutex>
	class ComponentContext : public Lockable<Mutex, LockAccess::Relaxed>
	{
	public:
		using MutexType = Mutex;
	};


	///<summary>
	///</summary>
	template <typename Type>
	class SharedHandle
	{
	public:
		typedef Type DataType;
		typedef SharedHandle<Type> MyType;

	private:
		std::shared_ptr<Type> _data;

	public:
		SharedHandle() = default;

		SharedHandle( SharedHandle<Type>&& source )
			: _data( std::move( source._data ) )
		{ }

		SharedHandle( const SharedHandle<Type>& origin )
			: _data( origin._data )
		{ }

		template <class V>
		SharedHandle( SharedHandle<V>&& other )
			: _data( std::move( other._data ) )
		{ }

		template <class V>
		SharedHandle( const SharedHandle<V>& other )
			: _data( other._data )
		{ }

		SharedHandle( std::nullptr_t )
		{ }

		explicit SharedHandle( std::shared_ptr<Type>&& data )
			: _data( std::move( data ) )
		{ }

		explicit SharedHandle( const std::shared_ptr<Type>& data )
			: _data( data )
		{ }

		SharedHandle& operator=( SharedHandle<Type>&& rhs )
		{
			if ( this != &rhs )
				MyType( std::move( rhs ) ).Swap( *this );

			return *this;
		}

		SharedHandle& operator=( const SharedHandle<Type>& rhs )
		{
			if ( this != &rhs )
				MyType( rhs ).Swap( *this );

			return *this;
		}

		template <class V>
		SharedHandle& operator=( SharedHandle<V>&& rhs )
		{
			if ( this != &rhs )
				MyType( std::move( rhs ) ).Swap( *this );

			return *this;
		}

		template <class V>
		SharedHandle& operator=( const SharedHandle<V>& rhs )
		{
			if ( this != &rhs )
				MyType( rhs ).Swap( *this );

			return *this;
		}

		SharedHandle& operator=( std::nullptr_t )
		{
			if ( this != &rhs )
				MyType( std::move( rhs ) ).Swap( *this );

			return *this;
		}

		explicit operator bool() const
		{
			return this->_data ? true : false;
		}

		Type* operator->() const
		{
			return this->_data.get();
		}

		Type& operator*() const
		{
			return *( this->_data );
		}
		void Release()
		{
			this->_data.reset();
		}

		Uint GetShareCount() const
		{
			return this->_data.use_count();
		}

		bool IsShared() const
		{
			return !this->_data.unique();
		}

		void Swap( MyType& other )
		{
			std::swap( this->_data, other._data );
		}
	};


	/// Forward-declares 'type' and typedefs SharedHandle{type} as type##handle. Forward declaration used 'class' keyword.
#define ExsDeclareSharedHandleTypeClass(type) \
	class type; typedef SharedHandle<type> type##Handle;

	/// Forward-declares 'type' and typedefs SharedHandle{type} as type##handle. Forward declaration used 'struct' keyword.
#define ExsDeclareSharedHandleTypeStruct(type) \
	struct type; typedef SharedHandle<type> type##Handle;


	/// Locks component context with SharedMutex for read-only access.
	inline stdx::shared_lock<SharedMutex> LockComponentContextReadOnly( const ComponentContext<SharedMutex>& context )
	{
		return stdx::shared_lock<SharedMutex>{ context.GetLock() };
	}

	/// Locks component context with SharedMutex for read-only access. Returns empty lock if handle does not refer to a context.
	inline stdx::shared_lock<SharedMutex> LockComponentContextReadOnly( const SharedHandle< ComponentContext<SharedMutex> >& contextHandle )
	{
		return contextHandle ? stdx::shared_lock<SharedMutex>{ contextHandle->GetLock() } : stdx::shared_lock<SharedMutex>{ };
	}


	/// Locks component context for read-only access.
	template <typename Mutex>
	inline std::unique_lock<Mutex> LockComponentContextReadOnly( const ComponentContext<Mutex>& context )
	{
		return std::unique_lock<Mutex>{ context.GetLock() };
	}

	/// Locks component context for read-only access. Returns empty lock if handle does not refer to a context.
	template <typename Mutex>
	inline std::unique_lock<Mutex> LockComponentContextReadOnly( const SharedHandle< ComponentContext<Mutex> >& contextHandle )
	{
		return contextHandle ? std::unique_lock<Mutex>{ contextHandle->GetLock() } : stdx::shared_lock<Mutex>{};
	}


	/// Locks component context for read/write access.
	template <typename Mutex>
	inline std::unique_lock<Mutex> LockComponentContextReadWrite( const ComponentContext<Mutex>& context )
	{
		return std::unique_lock<Mutex>{ context.GetLock() };
	}

	/// Locks component context for read/write access. Returns empty lock if handle does not refer to a context.
	template <typename Mutex>
	inline std::unique_lock<Mutex> LockComponentContextReadWrite( const SharedHandle< ComponentContext<Mutex> >& contextHandle )
	{
		return contextHandle ? std::unique_lock<Mutex>{ contextHandle->GetLock() } : stdx::shared_lock<Mutex>{ };
	}


	/// Locks component context with SharedMutex for access, which is specified as template parameter.
	template <SharedMutexAccessType Access>
	inline typename SharedMutexLockType<Access>::Type LockComponentContext( const ComponentContext<SharedMutex>& context )
	{
		using Lock = typename SharedMutexLockType<Access>::Type;
		return Lock{ context.GetLock() };
	}

	/// Locks component context with SharedMutex for access, which is specified as template parameter. Returns empty lock if handle does not refer to a context.
	template <SharedMutexAccessType Access>
	inline typename SharedMutexLockType<Access>::Type LockComponentContext( const SharedHandle< ComponentContext<SharedMutex> >& sharedStateHandle )
	{
		using Lock = typename SharedMutexLockType<Access>::Type;
		return sharedStateHandle ? Lock{ contextHandle->GetLock() } : Lock{ };
	}


	/// Locks component context for access, which is specified as template parameter.
	template <SharedMutexAccessType Access, typename Mutex>
	inline std::unique_lock<Mutex> LockComponentContext( const ComponentContext<Mutex>& context )
	{
		return std::unique_lock<Mutex>{ context.GetLock() };
	}

	/// Locks component context for access, which is specified as template parameter. Returns empty lock if handle does not refer to a context.
	template <SharedMutexAccessType Access, typename Mutex>
	inline std::unique_lock<Mutex> LockComponentContext( const SharedHandle< ComponentContext<Mutex> >& contextHandle )
	{
		return sharedStateHandle ? std::unique_lock<Mutex>{ contextHandle->GetLock() } : std::unique_lock<Mutex>{ };
	}


	/// Locks shared state for read-only access.
	inline stdx::shared_lock<SharedMutex> LockSharedStateReadOnly( const ComponentSubsystemSharedState& sharedState )
	{
		return stdx::shared_lock<SharedMutex>{ sharedState.accessMutex };
	}

	/// Locks shared state (passed via handle) for read-only access. Returns empty lock if handle does not refer to any state.
	inline stdx::shared_lock<SharedMutex> LockSharedStateReadOnly( const SharedHandle<ComponentSubsystemSharedState>& sharedStateHandle )
	{
		return sharedStateHandle ? stdx::shared_lock<SharedMutex>{ sharedStateHandle->accessMutex } : stdx::shared_lock<SharedMutex>{};
	}


	/// Locks shared state for read/write access.
	inline std::unique_lock<SharedMutex> LockSharedStateReadWrite( const ComponentSubsystemSharedState& sharedState )
	{
		return std::unique_lock<SharedMutex>{ sharedState.accessMutex };
	}

	/// Locks shared state (passed via handle) for read/write access. Returns empty lock if handle does not refer to any state.
	inline std::unique_lock<SharedMutex> LockSharedStateReadWrite( const SharedHandle<ComponentSubsystemSharedState>& sharedStateHandle )
	{
		return sharedStateHandle ? std::unique_lock<SharedMutex>{ sharedStateHandle->accessMutex } : std::unique_lock<SharedMutex>{};
	}


	/// Locks shared state for access, which is specified as template parameter. Lock type depends on access.
	template <SharedMutexAccessType Access>
	inline typename SharedMutexLockType<Access>::Type LockSharedState( const ComponentSubsystemSharedState& sharedState )
	{
		using Lock = typename SharedMutexLockType<Access>::Type;
		return Lock{ sharedState.accessMutex };
	}

	/// Locks shared state for access, which is specified as template parameter. Lock type depends on access. Returns empty lock if handle does not refer to any state.
	template <SharedMutexAccessType Access>
	inline typename SharedMutexLockType<Access>::Type LockSharedState( const SharedHandle<ComponentSubsystemSharedState>& sharedStateHandle )
	{
		using Lock = typename SharedMutexLockType<Access>::Type;
		return sharedStateHandle ? Lock{ sharedStateHandle->accessMutex } : Lock{};
	}


}


#endif /* __Exs_Core_SharedState_H__ */
