
#ifndef __Exs_Core_Config_H__
#define __Exs_Core_Config_H__

#if ( EXS_BUILD_MODULE_CORE )
#  define EXS_LIBAPI_CORE     EXS_MODULE_EXPORT
#  define EXS_LIBCLASS_CORE   EXS_MODULE_EXPORT
#  define EXS_LIBOBJECT_CORE  EXS_MODULE_EXPORT
#else
#  define EXS_LIBAPI_CORE     EXS_MODULE_IMPORT
#  define EXS_LIBCLASS_CORE   EXS_MODULE_IMPORT
#  define EXS_LIBOBJECT_CORE  EXS_MODULE_IMPORT
#endif

#endif // __Exs_Core_Config_H__
