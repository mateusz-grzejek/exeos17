
#ifndef __Exs_Core_EventBase_H__
#define __Exs_Core_EventBase_H__


namespace Exs
{


	template <class Event_code_t>
	class Event
	{
	protected:
		Event_code_t    _code;

	public:
		Event()
		: _code(static_cast<Event_code_t>(0))
		{ }

		Event(Event_code_t code)
		: _code(code)
		{ }

		Event_code_t GetCode() const
		{
			return this->_code;
		}
	};


}


#endif /* __Exs_Core_EventBase_H__ */
