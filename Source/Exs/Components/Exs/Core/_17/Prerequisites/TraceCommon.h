
#ifndef __Exs_Core_TraceCommon_H__
#define __Exs_Core_TraceCommon_H__


#define EXS_TRACE_TYPE_INFO          0x0001
#define EXS_TRACE_TYPE_NOTIFICATION  0x0002
#define EXS_TRACE_TYPE_WARNING       0x0004
#define EXS_TRACE_TYPE_ERROR         0x0008
#define EXS_TRACE_TYPE_EXCEPTION     0x0010

#define EXS_TRACE_TYPE_ALL              0xFFFF
#define EXS_TRACE_TYPE_DEFAULT_ENABLED  EXS_TRACE_TYPE_EXCEPTION | EXS_TRACE_TYPE_ERROR


#if !defined( EXS_TRACE_CONFIG_ACTIVE_TYPES_MASK )
#  if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
#    define EXS_TRACE_CONFIG_ACTIVE_TYPES_MASK  EXS_TRACE_TYPE_ALL
#  else
#    define EXS_TRACE_CONFIG_ACTIVE_TYPES_MASK  EXS_TRACE_TYPE_DEFAULT_ENABLED
#  endif
#endif


#define EXS_TRACE_SET_CATEGORY_NAME(category, name) \
	template <> inline const char* GetTraceCategoryName<category>() { return name; }

#define EXS_TRACE_SET_TYPE_NAME(type, name) \
	template <> inline const char* GetTraceTypeName<type>() { return name; }


namespace Exs
{


	typedef Enum TraceCategoryID;


	enum class TraceType : Enum
	{
		Info = EXS_TRACE_TYPE_INFO,
		Notification = EXS_TRACE_TYPE_NOTIFICATION,
		Warning = EXS_TRACE_TYPE_WARNING,
		Error = EXS_TRACE_TYPE_ERROR,
		Exception = EXS_TRACE_TYPE_EXCEPTION
	};


	enum : TraceCategoryID
	{
		TRC_None,
		TRC_Common,
		TRC_Core_Filesystem,
		TRC_Core_Messaging,
		TRC_Core_Plugins,
		TRC_Core_System,
		TRC_Core_Threading
	};


	template <TraceCategoryID>
	inline const char* GetTraceCategoryName()
	{
		return "Unknown";
	}
	

	template <TraceType>
	inline const char* GetTraceTypeName()
	{
		return "Null";
	}
	

	template <TraceType>
	inline void TraceOutputFinalizer()
	{ }

	template <>
	inline void TraceOutputFinalizer<TraceType::Error>()
	{
		ExsDebugInterrupt();
	}

	template <>
	inline void TraceOutputFinalizer<TraceType::Exception>()
	{
		ExsDebugInterrupt();
	}
	

	EXS_TRACE_SET_TYPE_NAME(TraceType::Info, "Inf");
	EXS_TRACE_SET_TYPE_NAME(TraceType::Notification, "Ntf");
	EXS_TRACE_SET_TYPE_NAME(TraceType::Warning, "Wrn");
	EXS_TRACE_SET_TYPE_NAME(TraceType::Error, "Err");
	EXS_TRACE_SET_TYPE_NAME(TraceType::Exception, "Exc");
	

	EXS_TRACE_SET_CATEGORY_NAME(TRC_Common, "Common");
	EXS_TRACE_SET_CATEGORY_NAME(TRC_Core_Filesystem, "Core:Filesystem");
	EXS_TRACE_SET_CATEGORY_NAME(TRC_Core_Messaging, "Core:Messaging");
	EXS_TRACE_SET_CATEGORY_NAME(TRC_Core_Plugins, "Core:Plugins");
	EXS_TRACE_SET_CATEGORY_NAME(TRC_Core_System, "Core:System");
	EXS_TRACE_SET_CATEGORY_NAME(TRC_Core_Threading, "Core:Threading");


	namespace Internal
	{

		EXS_LIBAPI_CORE void TraceOutputImpl(TraceType type, TraceCategoryID category, const char* typeName, const char* categoryName, const char* format, ...);
		

		template <bool Enable>
		struct TraceOutputFwd;

		template <>
		struct TraceOutputFwd<true>
		{
			template <class... Args>
			static void Print(TraceType type, TraceCategoryID category, const char* typeName, const char* categoryName, const char* format, Args&&... args)
			{
				TraceOutputImpl(type, category, typeName, categoryName, format, std::forward<Args>(args)...);
			}
		};

		template <>
		struct TraceOutputFwd<false>
		{
			template <class... Args>
			static void Print(TraceType type, TraceCategoryID category, const char* typeName, const char* categoryName, const char* format, Args&&... args)
			{ }
		};


		template <TraceType Type, TraceCategoryID Category>
		struct TraceOutputWrapper
		{
			template <class... Args>
			static void Print(const char* format, Args&&... args)
			{
				const Uint32 traceMask = static_cast<Uint32>(Type) & EXS_TRACE_CONFIG_ACTIVE_TYPES_MASK;
				const char* typeName = GetTraceTypeName<Type>();
				const char* categoryName = GetTraceCategoryName<Category>();
				TraceOutputFwd<traceMask != 0>::Print(Type, Category, typeName, categoryName, format, std::forward<Args>(args)...);
				TraceOutputFinalizer<Type>();
			}
		};

	}


}


#if ( EXS_CONFIG_BASE_ENABLE_TRACING )
#  define TraceOutput(type, category, format, ...) Internal::TraceOutputWrapper<type, category>::Print(format, ##__VA_ARGS__)
#else
#  define TraceOutput(type, category, format, ...)
#endif


#define ExsTraceInfo(category, format, ...) \
	TraceOutput(TraceType::Info, category, format, ##__VA_ARGS__)

#define ExsTraceNotification(category, format, ...) \
	TraceOutput(TraceType::Notification, category, format, ##__VA_ARGS__)

#define ExsTraceWarning(category, format, ...) \
	TraceOutput(TraceType::Warning, category, format, ##__VA_ARGS__)

#define ExsTraceError(category, format, ...) \
	TraceOutput(TraceType::Error, category, format, ##__VA_ARGS__)

#define ExsTraceException(category, format, ...) \
	TraceOutput(TraceType::Exception, category, format, ##__VA_ARGS__)


#endif /* __Exs_Core_TraceCommon_H__ */
