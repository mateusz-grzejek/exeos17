
#ifndef __Exs_Core_NativeWindow_WinPhone81_H__
#define __Exs_Core_NativeWindow_WinPhone81_H__


namespace Exs
{


	class EXS_LIBCLASS_CORE NativeWindow : public NativeWindowBase
	{
	protected:
		WindowStyle    _style;
		String         _title;

	public:
		NativeWindow();
		virtual ~NativeWindow();
		
		///<summary>
		///</summary>
		Result CreateFrame(const NativeWindowDesc& desc);
		
		///<summary>
		///</summary>
		void Close();
		
		///<summary>
		///</summary>
		void Destroy();
		
		///<summary>
		///</summary>
		void Release();
		
		///<summary>
		///</summary>
		bool Show(bool show);
		
		///<summary>
		///</summary>
		Result Resize(const Size& size, WindowAreaType areaType);
		
		///<summary>
		///</summary>
		Result SetStyle(WindowStyle style);
		
		///<summary>
		///</summary>
		Result SetTitle(const String& title);
		
		///<summary>
		///</summary>
		WindowStyle GetStyle() const;
		
		///<summary>
		///</summary>
		const String& GetTitle() const;
		
		///<summary>
		///</summary>
		Size GetClientAreaSize() const;
		
		///<summary>
		///</summary>
		bool IsValid() const;
		
		///<summary>
		///</summary>
		bool IsVisible() const;
	};


	inline WindowStyle NativeWindow::GetStyle() const
	{
		return this->_style;
	}

	inline const String& NativeWindow::GetTitle() const
	{
		return this->_title;
	}


}


#endif /* __Exs_Core_NativeWindow_WinPhone81_H__ */
