
#ifndef __Exs_Core_AppStateObject_WinPhone81_H__
#define __Exs_Core_AppStateObject_WinPhone81_H__


namespace Exs
{


	class EXS_LIBCLASS_CORE AppStateObject : public AppStateObjectBase
	{
	public:
		AppStateObject();
		virtual ~AppStateObject();

	protected:
		virtual Result InitSystemResources() override final;
		virtual void ReleaseSystemResources() override final;
	};


}


#endif /* __Exs_Core_AppStateObject_WinPhone81_H__ */
