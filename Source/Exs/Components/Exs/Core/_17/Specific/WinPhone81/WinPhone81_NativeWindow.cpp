
#include <Exs/Core/NativeWindow.h>
#include <Exs/Core/PlatformUtils.h>


namespace Exs
{


	NativeWindow::NativeWindow(Enum capabilitiesMask)
	: NativeWindowBase()
	, _style(WindowStyle::Unknown)
	, _windowClassID(0)
	, _windowClassName(nullptr)
	, _wndprocModule(nullptr)
	{ }
	

	NativeWindow::~NativeWindow()
	{
		this->Release();
	}


	Result NativeWindow::CreateFrame(const NativeWindowDesc& desc)
	{
		if (this->IsInitialized())
			return RSC_Already_Initialized;

		return RSC_Success;
	}


	Result NativeWindow::Close()
	{
		return RSC_Success;
	}


	void NativeWindow::Release()
	{
	}


	Result NativeWindow::Show(bool show)
	{
		return RSC_Success;
	}
	
	
	Result NativeWindow::Resize(const Size& size, WindowArea area)
	{
		return RSC_Success;
	}

	
	Result NativeWindow::SetStyle(WindowStyle style)
	{
		return RSC_Success;
	}


	Result NativeWindow::SetTitle(const String& title)
	{
		return RSC_Success;
	}


	Size NativeWindow::GetClientAreaSize() const
	{
		return Size(0, 0);
	}


	bool NativeWindow::IsValid() const
	{
		return false;
	}

	
	bool NativeWindow::IsVisible() const
	{
		return false;
	}


}
