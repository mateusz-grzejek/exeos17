
#ifndef __Exs_Core_NativeWindow_Android_H__
#define __Exs_Core_NativeWindow_Android_H__


struct ANativeWindow;


namespace Exs
{


	class EXS_LIBCLASS_CORE NativeWindow : public NativeWindowBase
	{
	public:
		NativeWindow(SystemEventDispatcher* eventDispatcher);
		virtual ~NativeWindow();
		
		///<summary>
		///</summary>
		Result Initialize(ANativeWindow* aNativeWindow);
		
		///<summary>
		///</summary>
		void Release();
		
		///<summary>
		///</summary>
		Result Resize(const Size& size, WindowAreaType areaType);
		
		///<summary>
		///</summary>
		Size GetClientAreaSize() const;
		
		///<summary>
		///</summary>
		bool IsValid() const;
		
		///<summary>
		///</summary>
		bool IsVisible() const;
	};


}


#endif /* __Exs_Core_NativeWindow_Android_H__ */
