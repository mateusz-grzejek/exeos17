
#ifndef __Exs_Core_AppEventLoop_Android_H__
#define __Exs_Core_AppEventLoop_Android_H__


namespace Exs
{


	class EXS_LIBCLASS_CORE AppEventLoop : public AppEventLoopBase
	{
	public:
		AppEventLoop(AppObject* appObject, AppExecutionState* appExecutionState);
		virtual ~AppEventLoop();
		
	protected:
		virtual Result EnterSystemLoop(const UserCallback& loopUpdateCallback) override;
	};


}


#endif /* __Exs_Core_AppEventLoop_Android_H__ */
