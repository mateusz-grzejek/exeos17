
#ifndef __Exs_Core_SystemEventDispatcher_Win32_H__
#define __Exs_Core_SystemEventDispatcher_Win32_H__


namespace Exs
{


	struct SystemEventDataAndroid
	{
	};


	class SystemEventDispatcher : public SystemEventDispatcherBase
	{
	public:
		SystemEventDispatcher(SystemEventReceiver* eventReceiver);
		~SystemEventDispatcher();

		void DispatchEvent(const SystemEventDataAndroid* eventMessage);

	private:
		static void TranslateAndDispatch(const SystemEventDataAndroid* eventMessage, SystemEventReceiver* eventReceiver);
	};


}


#endif /* __Exs_Core_SystemEventDispatcher_Win32_H__ */
