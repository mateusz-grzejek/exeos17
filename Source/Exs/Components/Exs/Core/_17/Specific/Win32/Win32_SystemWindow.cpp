
#include <Exs/Core/PlatformUtils.h>
#include <Exs/Core/System/SystemWindow.h>


namespace Exs
{

	
	DWORD Win32TranslateStyle(SystemWindowStyle style);

	bool Win32RegisterWndClass(Win32SystemWindowData* windowData);
	void Win32UnregisterWndClass(Win32SystemWindowData* windowData);

	void Win32ValidateDimensions(Size& size, DWORD style, Size* screenSizeOutput = nullptr);
	void Win32ValidateDimensionsAndPos(Size& size, Position& position, DWORD style, Size* screenSizeOutput = nullptr);

	LRESULT __stdcall Win32SystemEventProc(HWND wndHandle, UINT msgCode, WPARAM wParam, LPARAM lParam);
	LRESULT __stdcall Win32RedirectingEventProc(HWND wndHandle, UINT msgCode, WPARAM wParam, LPARAM lParam);




	bool SystemWindow::Destroy()
	{
		if (this->_handle == ExsNativeWindowHandleNULL)
			return false;

		BOOL isValidWindow = ::IsWindow(this->_handle);
		if (isValidWindow != FALSE)
		{
			BOOL destroyResult = ::DestroyWindow(this->_handle);
			if (destroyResult != TRUE)
			{
				//?
				PlatformUtils::Win32CheckLastError();
			}
		}

		this->_handle = ExsNativeWindowHandleNULL;
		return true;
	}


	bool SystemWindow::Show(bool show)
	{
		ExsDebugAssert( this->IsValid() );

		DWORD showCmd = show ? SW_SHOWNORMAL : SW_HIDE;
		BOOL prevState = ::ShowWindow(this->_handle, showCmd);

		return prevState == TRUE;
	}


	bool SystemWindow::Resize(const Size& size)
	{
		ExsDebugAssert( this->IsValid() );

		DWORD frameStyle = ::GetWindowLongW(this->_handle, GWL_STYLE);

		Size frameSize = size;
		Win32ValidateDimensions(frameSize, frameStyle);

		UINT updateFlags = SWP_NOMOVE | SWP_NOZORDER | SWP_FRAMECHANGED;
		BOOL setResult = ::SetWindowPos(this->_handle, nullptr, 0, 0, frameSize.width, frameSize.height, updateFlags);
		if (setResult != TRUE)
		{
			PlatformUtils::Win32CheckLastError();
			return false;
		}

		return true;
	}


	bool SystemWindow::SetStyle(SystemWindowStyle style)
	{
		ExsDebugAssert( this->IsValid() );
		
		DWORD frameStyle = Win32TranslateStyle(style);
		::SetWindowLongW(this->_handle, GWL_STYLE, static_cast<LONG>(frameStyle));

		UINT updateFlags = SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED | SWP_SHOWWINDOW;
		BOOL updateResult = ::SetWindowPos(this->_handle, nullptr, 0, 0, 0, 0, updateFlags);
		if (updateResult != TRUE)
		{
			PlatformUtils::Win32CheckLastError();
			return false;
		}

		return true;
	}


	bool SystemWindow::SetTitle(const std::string& title)
	{
		ExsDebugAssert( this->IsValid() );

		std::string titleStr = title;
		titleStr += " [Exeos 3D Engine, version " EXS_VER_PRODUCTVERSION_STR " " EXS_BUILD_TYPE_STR "]";

		BOOL setResult = ::SetWindowTextA(this->_handle, titleStr.c_str());
		if (setResult != TRUE)
		{
			PlatformUtils::Win32CheckLastError();
			return false;
		}

		return true;
	}


	bool SystemWindow::GetClientAreaSize(Size* size) const
	{
		ExsDebugAssert( this->IsValid() );

		RECT clientRect = { 0 };
		BOOL getResult = ::GetClientRect(this->_handle, &clientRect);

		if (getResult != TRUE)
		{
			PlatformUtils::Win32CheckLastError();
			return false;
		}

		if (size != nullptr)
		{
			size->width = static_cast<Uint32>(clientRect.right - clientRect.left);
			size->height = static_cast<Uint32>(clientRect.bottom - clientRect.top);
		}

		return true;
	}


	bool SystemWindow::GetSize(Size* size) const
	{
		ExsDebugAssert( this->IsValid() );

		RECT windowRect = { 0 };
		BOOL getResult = ::GetWindowRect(this->_handle, &windowRect);

		if (getResult != TRUE)
		{
			PlatformUtils::Win32CheckLastError();
			return false;
		}

		if (size != nullptr)
		{
			size->width = static_cast<Uint32>(windowRect.right - windowRect.left);
			size->height = static_cast<Uint32>(windowRect.bottom - windowRect.top);
		}

		return true;
	}




	SystemWindowRefPtr CreateSystemWindow(AppGlobalStateRefHandle appGlobalState, const SystemWindowFrameDesc& frameDesc)
	{
		ExsDebugAssert( appGlobalState );
		auto& systemEnvData = appGlobalState->systemEnvData;

		Win32SystemWindowData windowData;
		if (!Win32RegisterWndClass(&windowData))
		{
			ExsTraceError(TRC_Core_System, "Window class registration failed!");
			return nullptr;
		}

		Size frameSize = frameDesc.size;
		Position framePos = frameDesc.position;
		DWORD frameStyle = Win32TranslateStyle(frameDesc.style);
		Win32ValidateDimensionsAndPos(frameSize, framePos, frameStyle);

		HWND windowHandle = ::CreateWindowExA( 0,
		                                       windowData.windowClassName,
		                                       frameDesc.title.c_str(),
		                                       frameStyle,
		                                       framePos.x,
		                                       framePos.y,
		                                       frameSize.width,
		                                       frameSize.height,
		                                       nullptr,
		                                       nullptr,
		                                       windowData.wndprocModule,
		                                       appGlobalState->eventDispatcher);

		if (windowHandle == nullptr)
		{
			PlatformUtils::Win32CheckLastError();
			return nullptr;
		}
		
		UINT updateFlags = SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED;
		::SetWindowPos(windowHandle, nullptr, 0, 0, 0, 0, updateFlags);
		
		auto windowObject = std::make_shared<SystemWindow>(windowHandle, appGlobalState, windowData);
		return windowObject;
	}




	DWORD Win32TranslateStyle(SystemWindowStyle style)
	{
		DWORD resultStyle = 0;

		switch(style)
		{
			case SystemWindowStyle::Fullscreen:
				resultStyle = WS_POPUP;
				break;

			case SystemWindowStyle::Normal:
				resultStyle = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MAXIMIZEBOX | WS_MINIMIZEBOX;
				break;

			case SystemWindowStyle::Normal_No_Resize:
				resultStyle = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU;
				break;
		}

		return resultStyle;
	}


	bool Win32RegisterWndClass(Win32SystemWindowData* windowData)
	{
		HMODULE wndprocModule = ::GetModuleHandleA(nullptr);
		LPCSTR windowClassName = "EXS_SWCORE_CLS";

		WNDCLASSEXA windowClass;
		ExsZeroMemory(&windowClass, sizeof(WNDCLASSEXA));

		BOOL classFindResult = ::GetClassInfoExA(wndprocModule, windowClassName, &windowClass);
		if (classFindResult == FALSE)
		{
			windowClass.cbSize = sizeof(WNDCLASSEXA);
			windowClass.cbClsExtra = 0;
			windowClass.cbWndExtra = 0;
			windowClass.hbrBackground = static_cast<HBRUSH>(GetStockObject(GRAY_BRUSH));
			windowClass.hCursor = LoadCursorA(nullptr, IDC_ARROW);
			windowClass.hIcon = LoadIconA(nullptr, IDI_WINLOGO);
			windowClass.hIconSm = LoadIconA(nullptr, IDI_WINLOGO);
			windowClass.hInstance = wndprocModule;
			windowClass.lpfnWndProc = Win32RedirectingEventProc;
			windowClass.lpszClassName = windowClassName;
			windowClass.lpszMenuName = nullptr;
			windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

			ATOM classID = ::RegisterClassExA(&windowClass);
			if (classID == 0)
			{
				PlatformUtils::Win32CheckLastError();
				return false;
			}

			windowData->windowClassID = classID;
		}

		windowData->windowClassName = windowClassName;
		windowData->wndprocModule = wndprocModule;

		return true;
	}


	void Win32UnregisterWndClass(Win32SystemWindowData* windowData)
	{
		ExsDebugAssert( windowData->windowClassID != 0 );

		BOOL result = UnregisterClassA(windowData->windowClassName, windowData->wndprocModule);
		if (result != TRUE)
		{
			PlatformUtils::Win32CheckLastError();
			//?
		}

		windowData->windowClassID = 0;
		windowData->windowClassName = nullptr;
		windowData->wndprocModule = nullptr;
	}


	void Win32ValidateDimensions(Size& size, DWORD style, Size* screenSizeOutput)
	{
		RECT windowRect = { 0 };
		int screenWidth = ::GetSystemMetrics(SM_CXSCREEN);
		int screenHeight = ::GetSystemMetrics(SM_CYSCREEN);

		if ((size.width > 0) && (size.height > 0))
		{
			windowRect.right = size.width;
			windowRect.bottom = size.height;
		}
		else
		{
			windowRect.right = static_cast<LONG>((screenWidth * 0.6f + 50.0f) / 100.0f) * 100;
			windowRect.bottom = static_cast<LONG>((screenHeight * 0.6f + 50.0f) / 100.0f) * 100;
		}

		::AdjustWindowRect(&windowRect, style, FALSE);

		size.width = windowRect.right - windowRect.left;
		size.height = windowRect.bottom - windowRect.top;

		if (screenSizeOutput != nullptr)
		{
			screenSizeOutput->width = screenWidth;
			screenSizeOutput->height = screenHeight;
		}
	}


	void Win32ValidateDimensionsAndPos(Size& size, Position& position, DWORD style, Size* screenSizeOutput)
	{
		Size screenSize { };
		Win32ValidateDimensions(size, style, &screenSize);

		if (position.x < 0)
			position.x = (size.width >= screenSize.width) ? 0 : (screenSize.width - size.width) / 2;

		if (position.y < 0)
			position.y = (size.height >= screenSize.height) ? 0 : (screenSize.height - size.height) / 2;

		if (screenSizeOutput != nullptr)
		{
			screenSizeOutput->width = screenSize.width;
			screenSizeOutput->height = screenSize.height;
		}
	}


}
