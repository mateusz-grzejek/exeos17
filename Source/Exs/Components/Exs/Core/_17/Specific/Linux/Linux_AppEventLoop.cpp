
#include <Exs/Core/System/AppEventLoop.h>
#include <Exs/Core/System/AppObject.h>
#include <Exs/Core/System/SystemEventDispatcher.h>
#include <X11/keysymdef.h>


namespace Exs
{


	bool X11CheckPendingEvents(const X11SystemEnvData* x11SystemEnvData);
	void X11ProcessEventActive(const X11SystemEnvData* x11SystemEnvData, SystemEventDispatcher* eventDispatcher);
	void X11ProcessEventIdle(const X11SystemEnvData* x11SystemEnvData, SystemEventDispatcher* eventDispatcher);


	void AppEventLoop::EnterSystemLoop(const AppInternalCallback& loopUpdateCallback)
	{
		X11SystemEnvData* x11SystemEnvData = &(this->_appGlobalState->systemEnvData);
		ExsDebugAssert( x11SystemEnvData != nullptr );

		while (!this->_appGlobalState->executionState.IsSet(AppExecutionState_Terminate_Request))
		{
			if (X11CheckPendingEvents(x11SystemEnvData))
			{
				if (this->_appGlobalState->executionState.IsSet(AppExecutionState_Idle))
				{
					// Idle mode: blocks current thread until event is received.
					X11ProcessEventIdle(x11SystemEnvData, &(this->_eventDispatcher));
				}
				else
				{
					// Active mode: event is checked and if there is no event, continue.
					X11ProcessEventActive(x11SystemEnvData, &(this->_eventDispatcher));
				}
			}

			if (!loopUpdateCallback())
				break;

			CurrentThread::SleepFor(Milliseconds(16));
		}
	}


	bool X11CheckPendingEvents(const X11SystemEnvData* x11SystemEnvData)
	{
		if (XEventsQueued(x11SystemEnvData->display, QueuedAlready) == 0)
		{
			if (XEventsQueued(x11SystemEnvData->display, QueuedAfterFlush) == 0)
				return false;

			XFlush(x11SystemEnvData->display);
		}

		return true;
	}


	void X11ProcessEventActive(const X11SystemEnvData* x11SystemEnvData, SystemEventDispatcher* eventDispatcher)
	{
		while (XEventsQueued(x11SystemEnvData->display, QueuedAlready) > 0)
		{
			XEvent x11Event { };
			XNextEvent(x11SystemEnvData->display, &x11Event);

			X11SystemEventInfo eventInfo { };
			eventInfo.xEvent = &x11Event;
			eventInfo.x11SystemEnvData = x11SystemEnvData;
			eventDispatcher->TranslateAndDispatch(eventInfo);
		}
	}


	void X11ProcessEventIdle(const X11SystemEnvData* x11SystemEnvData, SystemEventDispatcher* eventDispatcher)
	{
		XEvent x11Event { };
		XNextEvent(x11SystemEnvData->display, &x11Event);

		X11SystemEventInfo eventInfo { };
		eventInfo.xEvent = &x11Event;
		eventInfo.x11SystemEnvData = x11SystemEnvData;
		eventDispatcher->TranslateAndDispatch(eventInfo);
	}


}
