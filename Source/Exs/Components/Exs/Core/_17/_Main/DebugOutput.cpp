
#include <Exs/Core/DebugOutput.h>
#include <stdx/string_utils.h>


namespace Exs
{


	static DebugInfoOutputProc Debug_info_output_proc = nullptr;
	static RuntimeAssertHandler Runtime_assert_handler = nullptr;
	static RuntimeInterruptHandler Runtime_interrupt_handler = nullptr;

	
	DebugInfoOutputProc DebugExecutionControl::SetDebugInfoOutputProc(DebugInfoOutputProc proc)
	{
		DebugInfoOutputProc prevProc = Debug_info_output_proc;
		Debug_info_output_proc = proc;

		return prevProc;
	}


	RuntimeAssertHandler DebugExecutionControl::SetRuntimeAssertHandler(RuntimeAssertHandler handler)
	{
		RuntimeAssertHandler prevHandler = Runtime_assert_handler;
		Runtime_assert_handler = handler;

		return prevHandler;
	}


	RuntimeInterruptHandler DebugExecutionControl::SetRuntimeInterruptHandler(RuntimeInterruptHandler handler)
	{
		RuntimeInterruptHandler prevHandler = Runtime_interrupt_handler;
		Runtime_interrupt_handler = handler;

		return prevHandler;
	}

	
	DebugInfoOutputProc DebugExecutionControl::RestoreDefaultDebugInfoOutputProc()
	{
		return SetDebugInfoOutputProc(nullptr);
	}


	RuntimeAssertHandler DebugExecutionControl::RestoreDefaultRuntimeAssertHandler()
	{
		return SetRuntimeAssertHandler(nullptr);
	}


	RuntimeInterruptHandler DebugExecutionControl::RestoreDefaultRuntimeInterruptHandler()
	{
		return SetRuntimeInterruptHandler(nullptr);
	}


	DebugInfoOutputProc DebugExecutionControl::GetDebugInfoOutputProc()
	{
		return (Debug_info_output_proc != nullptr) ? Debug_info_output_proc : _DefaultDebugInfoOutputProc;
	}
	

	RuntimeAssertHandler DebugExecutionControl::GetRuntimeAssertHandler()
	{
		return (Runtime_assert_handler != nullptr) ? Runtime_assert_handler : _DefaultRuntimeAssertHandler;
	}
	

	RuntimeInterruptHandler DebugExecutionControl::GetRuntimeInterruptHandler()
	{
		return (Runtime_interrupt_handler != nullptr) ? Runtime_interrupt_handler : _DefaultRuntimeInterruptHandler;
	}


	void DebugExecutionControl::_PrintOutputDebug(const char* output)
	{
		DEBUG_OUTPUT(output);
		DEBUG_OUTPUT("\n");
	}


	void DebugExecutionControl::_PrintOutputDefault(const char* output)
	{
		fprintf(stderr, "%s\n", output);
	}


	void DebugExecutionControl::_DefaultDebugInfoOutputProc(const char* text)
	{
	#if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
		_PrintOutputDebug(text);
	#else
		_PrintOutputDefault(text);
	#endif
	}


	bool DebugExecutionControl::_DefaultRuntimeAssertHandler(const char* info)
	{
	#if ( EXS_TARGET_PLATFORM == EXS_TARGET_SYSTEM_WIN32 )
		std::string messageBoxText = info;
		messageBoxText += "\n\nBreak execution here?";
		int response = MessageBoxA(nullptr, messageBoxText.data(), "Runtime assertion", MB_YESNO | MB_ICONERROR | MB_APPLMODAL);
		return response == IDYES;
	#else
		_PrintOutputDefault(info);
		return true;
	#endif
	}


	bool DebugExecutionControl::_DefaultRuntimeInterruptHandler(const char* info)
	{
	#if ( EXS_TARGET_PLATFORM == EXS_TARGET_SYSTEM_WIN32 )
		std::string messageBoxText = info;
		messageBoxText += "\n\nBreak execution here?";
		int response = MessageBoxA(nullptr, messageBoxText.data(), "Runtime interrupt", MB_YESNO | MB_ICONERROR | MB_APPLMODAL);
		return response == IDYES;
	#else
		_PrintOutputDefault(info);
		return true;
	#endif
	}


	namespace Internal
	{


		void DebugInfoOutputImpl(const char* format, ...)
		{
			const Size_t Local_buffer_size = 1024;
			char localBuffer[Local_buffer_size];

			Size_t bufferCapacity = Local_buffer_size;
			char* bufferPtr = &(localBuffer[0]);

			// Arguments
			{
				va_list varArgsList;
				va_start(varArgsList, format);

				auto printCount = vsnprintf(bufferPtr, bufferCapacity, format, varArgsList);
				bufferCapacity -= printCount;
				bufferPtr += printCount;

				va_end(varArgsList);
			}
			
			ExsDebugAssert( bufferPtr < &(localBuffer[Local_buffer_size]) );

			auto outputProc = DebugExecutionControl::GetDebugInfoOutputProc();
			outputProc(localBuffer);
		}


		bool RuntimeAssertImpl(const char* file, Int32 line, const char* conditionStr)
		{
			const Size_t Local_buffer_size = 1024;
			char localBuffer[Local_buffer_size];

			std::string shortFilePath = stdx::extract_short_file_path(file, EXS_ENV_DEFAULT_PATH_DELIMITER);

			snprintf(localBuffer, Local_buffer_size,
				"Runtime assertion has failed!\n\"%s\" is false.\nSource: %s, line %u.", conditionStr, shortFilePath.data(), line);
			
			auto assertHandler = DebugExecutionControl::GetRuntimeAssertHandler();
			bool breakExecution = assertHandler(localBuffer);

			return breakExecution;
		}


		bool RuntimeInterruptImpl(const char* file, Int32 line)
		{
			const Size_t Local_buffer_size = 1024;
			char localBuffer[Local_buffer_size];

			std::string shortFilePath = stdx::extract_short_file_path(file, EXS_ENV_DEFAULT_PATH_DELIMITER);

			snprintf(localBuffer, Local_buffer_size,
				"Runtime interrupt point encountered.\nLocation: %s, line %u.", shortFilePath.data(), line);
			
			auto interruptHandler = DebugExecutionControl::GetRuntimeInterruptHandler();
			bool breakExecution = interruptHandler(localBuffer);

			return breakExecution;
		}


	}


}
