
#ifndef __Exs_Core_Singleton_H__
#define __Exs_Core_Singleton_H__

#include "Prerequisites.h"


namespace Exs
{


	enum class SingletonType : Enum
	{
		Normal = 0x5001,

		Synchronized = 0x5002,

		Thread_Local = 0x5003
	};


	template <class Type, SingletonType = SingletonType::Normal>
	class Singleton;


	template <class Type>
	class SingletonSpec
	{
	public:
		static Type* Create()
		{
			Type* singleton = new Type();
			return singleton;
		}

		static void Destroy(void* singletonPtr)
		{
			ExsDebugAssert( singletonPtr != nullptr );
			Type* singleton = pointer_cast<Type*>(singletonPtr);
			delete singleton;
		}
	};

	
	typedef decltype(SingletonSpec<void>::Destroy)* SingletonDestroyProc;


	class SingletonHolder
	{
	protected:
		void*                   _singletonPtr;
		SingletonDestroyProc    _destroyProc;

	public:
		SingletonHolder(void* singletonPtr, SingletonDestroyProc destroyProc)
		: _singletonPtr(singletonPtr)
		, _destroyProc(destroyProc)
		{ }

		~SingletonHolder()
		{
			ExsDebugAssert( this->_singletonPtr != nullptr );
			this->_destroyProc(this->_singletonPtr);
			this->_singletonPtr = nullptr;
			this->_destroyProc = nullptr;
		}

		void* GetSingletonPtr()
		{
			return this->_singletonPtr;
		}

		const void* GetSingletonPtr() const
		{
			return this->_singletonPtr;
		}

		template <class Cast_type>
		Cast_type* GetSingletonPtrAs()
		{
			return pointer_cast<Cast_type*>(this->_singletonPtr);
		}

		template <class Cast_type>
		const Cast_type* GetSingletonPtrAs() const
		{
			return pointer_cast<Cast_type*>(this->_singletonPtr);
		}
	};


	template <class Type>
	class Singleton<Type, SingletonType::Normal>
	{
	public:
		static Type* GetSingletonPtr()
		{
			static std::unique_ptr<SingletonHolder> holder(_CreateInstance());
			return holder->GetSingletonPtrAs<Type>();
		}

	protected:
		static SingletonHolder* _CreateInstance()
		{
			Type* singleton = SingletonSpec<Type>::Create();
			return new SingletonHolder(singleton, SingletonSpec<Type>::Destroy);
		}
	};


	template <class Type>
	class Singleton<Type, SingletonType::Synchronized>
	{
	public:
		static Type* GetSingletonPtr()
		{
			static std::unique_ptr<SingletonHolder> holder(_CreateInstance());
			return holder->GetSingletonPtrAs<Type>();
		}

	protected:
		static SingletonHolder* _CreateInstance()
		{
			Type* singleton = SingletonSpec<Type>::Create();
			return new SingletonHolder(singleton, SingletonSpec<Type>::Destroy);
		}
	};


	///<summary>
	/// Returns pointer to storage for singleton stored in ThreadLocalStorage.
	/// Singleton is identified by singletonID. Note, that maximum number of
	/// these singletons is determined by CFG_TLS_Max_Singletons_Num defined in
	/// file ThreadLocalStorage.h. All internal singletons (those, that are
	/// part of the engine) are stored in TLS and there are always at least 32 slots
	/// left for user code. You can obtain first free id, using TODO: undefined identifier for first free id.
	///</summary>
	void** GetSingletonStorageTLS(Enum singletonID);

	
	///<summary>
	/// Registers thread-local singleton in manager of current thread.
	///</summary>
	void RegisterThreadLocalSingleton(void* singletonPtr, SingletonDestroyProc destroyProc);

	
	///<summary>
	/// Returns pointer to storage for thread-local singleton of type <c>Type</c>. Specialization of this
	/// function is required for every <c>T</c>, that is a thread-local singleton in terms of deriving from
	/// class <c>Singleton{T, SingletonType::Thread_Local}</c>.
	///</summary>
	template <class Type>
	inline void** GetThreadLocalSingletonPointer()
	{
		return nullptr;
	}


	template <class Type>
	class Singleton<Type, SingletonType::Thread_Local>
	{
	public:
		static Type* GetSingletonPtr()
		{
			return _GetInstance();
		}

	protected:
		static Type* _GetInstance()
		{
			void** storagePtr = GetThreadLocalSingletonPointer<Type>();
			void*& singletonPtr = *storagePtr;

			if (*storagePtr == nullptr)
			{
				*storagePtr = SingletonSpec<Type>::Create();
				RegisterThreadLocalSingleton(singletonPtr, SingletonSpec<Type>::Destroy);
			}

			return reinterpret_cast<Type*>(singletonPtr);
		}
	};


}


#endif /* __Exs_Core_Singleton_H__ */
