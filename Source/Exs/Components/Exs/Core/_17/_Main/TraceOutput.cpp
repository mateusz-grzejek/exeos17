
#include <Exs/Core/TraceOutput.h>
#include <Exs/Core/Concurrency/Thread.h>
#include <Exs/Core/Concurrency/ThreadSharedApi.h>


namespace Exs
{


	namespace Internal
	{


		void TraceOutputImpl(TraceType type, TraceCategoryID category, const char* typeName, const char* categoryName, const char* format, ...)
		{
			TraceOrigin traceOrigin { };
			traceOrigin.type = type;
			traceOrigin.category = category;
			traceOrigin.typeName = typeName;
			traceOrigin.categoryName = categoryName;

			va_list varArgsList;
			va_start(varArgsList, format);

			TraceOutputController* traceOutputController = GetTraceOutputController();
			traceOutputController->PrintOutput(traceOrigin, format, varArgsList);

			va_end(varArgsList);
		}


	}


	TraceOutputController* GetTraceOutputController()
	{
		static TraceOutputController traceController { };
		return &traceController;
	}




	TraceOutputController::TraceOutputController()
	: _activeInfoContent(EXS_TRACE_INFO_CONTENT_INIT_PHASE)
	, _activeTraceTypes(EXS_TRACE_CONFIG_ACTIVE_TYPES_MASK)
	, _threadNameColumnWidth(Default_Trace_Output_Thread_Name_Column_Width)
	{ }


	TraceOutputController::~TraceOutputController()
	{ }


	void TraceOutputController::PrintOutput(const TraceOrigin& traceOrigin, const char* format, va_list argsList)
	{
		if (!this->IsTraceTypeEnabled(traceOrigin.type))
			return;

		CategoryInfo* categoryInfo = this->GetCategoryInfo(traceOrigin.category);
		if ((categoryInfo != nullptr) && !categoryInfo->active)
			return;

		const Size_t Local_buffer_size = 1024;
		char localBuffer[Local_buffer_size];

		Size_t bufferCapacity = Local_buffer_size;
		char* bufferPtr = &(localBuffer[0]);

		if (this->GetInfoContentDisplayState(TraceInfoContent::Base_Type))
		{
			auto printCount = snprintf(bufferPtr, bufferCapacity, "<%s> ", traceOrigin.typeName);
			bufferCapacity -= printCount;
			bufferPtr += printCount;
		}

	#if ( EXS_TRACE_CONFIG_ENABLE_INFO_CONTENT_CURRENT_TIME )
		if (this->GetInfoContentDisplayState(TraceInfoContent::Current_Time))
		{
		}
	#endif

	#if ( EXS_TRACE_CONFIG_ENABLE_INFO_CONTENT_CURRENT_THREAD )
		if (this->GetInfoContentDisplayState(TraceInfoContent::Thread))
		{
			Thread* currentThread = CurrentThread::GetThread();

			if (this->GetInfoContentDisplayState(TraceInfoContent::Thread_UID))
			{
				ThreadUID threadUID = currentThread->GetUID();
				auto printCount = snprintf(bufferPtr, bufferCapacity, "[UIDx%" EXS_PFU32_HEX "] ", threadUID.value);
				bufferCapacity -= printCount;
				bufferPtr += printCount;
			}

			if (this->GetInfoContentDisplayState(TraceInfoContent::Thread_Address))
			{
				Uint threadAddress = reinterpret_cast<Uint>(currentThread);
				auto printCount = snprintf(bufferPtr, bufferCapacity, "[0x%" EXS_PFU_HEX "] ", threadAddress);
				bufferCapacity -= printCount;
				bufferPtr += printCount;
			}

			// Size_t maxThreadNameLength = this->GetThreadNameColumnWidth();
			// Size_t threadNameLength = 0;
			// 
			// if (this->GetInfoContentDisplayState(TraceInfoContent::Thread_Name))
			// {
			// 	threadNameLength = stdx::get_min_of(currentThread->GetName().length(), maxThreadNameLength);
			// 	std::string threadName { currentThread->GetName(), 0, threadNameLength };
			// 
			// 	auto printCount = snprintf(bufferPtr, bufferCapacity, "@%s", threadName.c_str());
			// 	bufferCapacity -= printCount;
			// 	bufferPtr += printCount;
			// }

			*bufferPtr++ =  ':';
			*bufferPtr++ =  ' ';
			bufferCapacity -= 2;

			// if (this->GetInfoContentDisplayState(TraceInfoContent::Thread_Name))
			// {
			// 	if (threadNameLength < maxThreadNameLength)
			// 	{
			// 		std::string padding;
			// 		padding.append(maxThreadNameLength - threadNameLength, ' ');
			// 
			// 		auto printCount = snprintf(bufferPtr, bufferCapacity, "%s", padding.c_str());
			// 		bufferCapacity -= printCount;
			// 		bufferPtr += printCount;
			// 	}
			// }
		}
	#endif

		if (this->GetInfoContentDisplayState(TraceInfoContent::Base_Category))
		{
			auto printCount = snprintf(bufferPtr, bufferCapacity, "[%s] ", traceOrigin.categoryName);
			bufferCapacity -= printCount;
			bufferPtr += printCount;
		}

		{
			auto printCount = vsnprintf(bufferPtr, bufferCapacity, format, argsList);
			bufferCapacity -= printCount;
			bufferPtr += printCount;
		}

		ExsDebugAssert( bufferPtr < &(localBuffer[Local_buffer_size]) );

		TraceOutputHandler outputHandler = DefaultOutputHandler;

		if (categoryInfo && categoryInfo->outputHandler)
			outputHandler = categoryInfo->outputHandler;
		else if (this->_commonOutputHandler)
			outputHandler = this->_commonOutputHandler;

		outputHandler(localBuffer);
	}


	void TraceOutputController::EnableCategory(TraceCategoryID categoryID, bool enable)
	{
		auto categoryInfoRef = this->_categories.find(categoryID);
		if (categoryInfoRef == this->_categories.end())
		{
			CategoryInfo categoryInfo { };
			categoryInfo.id = categoryID;
			categoryInfoRef = this->_categories.insert(categoryID, std::move(categoryInfo));
		}

		categoryInfoRef->value.active = enable;
	}


	void TraceOutputController::SetCategoryOutputHandler(TraceCategoryID categoryID, const TraceOutputHandler& handler)
	{
		auto categoryInfoRef = this->_categories.find(categoryID);
		if (categoryInfoRef == this->_categories.end())
		{
			CategoryInfo categoryInfo { };
			categoryInfo.id = categoryID;
			categoryInfoRef = this->_categories.insert(categoryID, std::move(categoryInfo));
		}

		categoryInfoRef->value.active = true;
		categoryInfoRef->value.outputHandler = handler;
	}


	static LightMutex defaultOutputHandlerLock { };


	void TraceOutputController::DefaultOutputHandler(const char* output)
	{
	#if ( EXS_CONFIG_BASE_ENABLE_DEBUG && ( EXS_TARGET_PLATFORM == EXS_TARGET_SYSTEM_WIN32 ) )
		OutputDebugStringA(output);
		OutputDebugStringA("\n");
	#endif

		ExsBeginCriticalSection(defaultOutputHandlerLock);
		{
			fflush(stdout);
			fprintf(stdout, "%s\n", output);
			fflush(stdout);
		}
		ExsEndCriticalSection();
	}


}
