
#ifndef __Exs_Core_File_H__
#define __Exs_Core_File_H__

#include "FilesystemBase.h"
#include <stdx/data_array.h>


namespace Exs
{


	///<summary>
	///</summary>
	class EXS_LIBCLASS_CORE File
	{
		EXS_DECLARE_NONCOPYABLE(File);

	private:
		std::string   _path;
		FileHandle    _handle;
		Enum          _accessMode;

	public:
		File();
		File(File&& source);
		File(const std::string& filePath, FileHandle handle, Enum access);

		~File();

		File& operator=(File&& rhs);
		
		explicit operator bool() const;
		
		File_size_t Read(void* buffer, Size_t capacity, File_size_t readSize);
		File_size_t Read(stdx::memory_buffer<Byte>& output, File_size_t readSize);
		File_size_t Read(stdx::data_array<Byte>& output, File_size_t readSize);

		File_size_t ReadAll(void* buffer, Size_t capacity);
		File_size_t ReadAll(stdx::dynamic_data_array<Byte>& output);

		File_size_t Write(const void* data, Size_t dataSize, File_size_t writeSize = Max_File_Size);

		File_offset_t Seek(File_offset_t offset, FileRefPoint refPoint = FileRefPoint::Start);
		File_offset_t SeekStart(File_offset_t offset = 0);
		File_offset_t SeekEnd(File_offset_t offset = 0);

		bool Flush();
		bool Rewind();
		bool Close();

		Enum GetAccessMode() const;

		FileHandle GetHandle() const;
		File_offset_t GetCurrentOffset() const;
		File_size_t GetLength() const;

		const std::string& GetPath() const;

		bool IsOpened() const;

		void Swap(File& other);

		static bool Exists(const std::string& filePath);
		static File Create(const std::string& filePath, FileCreateMode createMode, Enum access = AccessMode_Write);
		static File Open(const std::string& filePath, FileOpenMode openMode, Enum access = AccessMode_Read);
		static File CreateTemporary();

	private:
		//
		File_size_t _Read(void* buffer, Size_t bufferSize, File_size_t chunkSize, File_size_t chunksNum);

		//
		File_size_t _Write(const void* data, File_size_t writeSize);

		//
		File_offset_t _Seek(File_offset_t offset, FileRefPoint refPoint);
	};


	inline File::operator bool() const
	{
		return this->IsOpened();
	}

	inline Enum File::GetAccessMode() const
	{
		return this->_accessMode;
	}

	inline FileHandle File::GetHandle() const
	{
		return this->_handle;
	}

	inline const std::string& File::GetPath() const
	{
		return this->_path;
	}

	inline bool File::IsOpened() const
	{
		return this->_handle != nullptr;
	}

	inline void File::Swap(File& other)
	{
		std::swap(this->_accessMode, other._accessMode);
		std::swap(this->_handle, other._handle);
		std::swap(this->_path, other._path);
	}


	inline void swap(File& left, File& right)
	{
		left.Swap(right);
	}


}


#endif /* __Exs_Core_File_H__ */
