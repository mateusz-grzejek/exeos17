
#include <Exs/Core/File.h>
#include <Exs/Core/System/SystemUtils.h>


namespace Exs
{
	

	File::File()
	: _handle(nullptr)
	, _accessMode(0)
	{ }


	File::File(File&& source)
	: _handle(nullptr)
	, _accessMode(0)
	{
		this->Swap(source);
	}


	File::File(const std::string& path, FileHandle handle, Enum access)
	: _path(path)
	, _handle(handle)
	, _accessMode(access)
	{ }


	File::~File()
	{
		if (this->IsOpened())
		{
			this->Close();
		}
	}


	File& File::operator=(File&& rhs)
	{
		if (this != &rhs)
			File(std::move(rhs)).Swap(*this);

		return *this;
	}


	File_size_t File::Read(void* buffer, Size_t capacity, File_size_t readSize)
	{
		ExsDebugAssert( this->IsOpened() );

		if ((capacity == 0) || (readSize == 0))
			return 0;
		
		return this->_Read(buffer, capacity, 1, readSize);
	}


	File_size_t File::Read(stdx::memory_buffer<Byte>& output, File_size_t readSize)
	{
		ExsDebugAssert( this->IsOpened() );

		if (readSize == 0)
			return 0;

		if (readSize > output.size())
			readSize = truncate_cast<File_size_t>(output.size());

		return this->_Read(output.data_ptr(), output.size(), 1, readSize);
	}


	File_size_t File::Read(stdx::data_array<Byte>& output, File_size_t readSize)
	{
		ExsDebugAssert( this->IsOpened() );

		if (readSize == 0)
			return 0;

		if (readSize > output.capacity())
			readSize = truncate_cast<File_size_t>(output.capacity());
		
		stdx::dynamic_memory_buffer<Byte> readBuffer { readSize };
		File_size_t readBytesNum = this->_Read(readBuffer.data_ptr(), readSize, 1, readSize);

		if ((readBytesNum > 0) && (readBytesNum != Invalid_File_Size))
			output.assign(readBuffer.data_ptr(), readBytesNum);

		return readBytesNum;
	}


	File_size_t File::ReadAll(void* buffer, Size_t capacity)
	{
		ExsDebugAssert( this->IsOpened() );
		return this->Read(buffer, capacity, Max_File_Size);
	}


	File_size_t File::ReadAll(stdx::dynamic_data_array<Byte>& output)
	{
		ExsDebugAssert( this->IsOpened() );
		stdx::fixed_memory_buffer<Byte, 1024> readBuffer;

		while (true)
		{
			File_size_t readSize = truncate_cast<File_size_t>(readBuffer.size());
			File_size_t readBytesNum = this->_Read(readBuffer.data_ptr(), readSize, 1, readSize);

			if (readBytesNum == 0)
				break;

			if (readBytesNum == Invalid_File_Size)
				return Invalid_File_Size;

			output.append(readBuffer.data_ptr(), readBytesNum);
		}

		return truncate_cast<File_size_t>(output.size());
	}


	File_size_t File::Write(const void* data, Size_t dataSize, File_size_t writeSize)
	{
		ExsDebugAssert( this->IsOpened() );

		if ((dataSize == 0) || (writeSize == 0))
			return 0;

		if (writeSize > dataSize)
			writeSize = truncate_cast<File_size_t>(dataSize);

		return this->_Write(data, writeSize);
	}


	File_offset_t File::Seek(File_offset_t offset, FileRefPoint refPoint)
	{
		ExsDebugAssert( this->IsOpened() );
		return this->_Seek(offset, refPoint);
	}


	File_offset_t File::SeekStart(File_offset_t offset)
	{
		ExsDebugAssert( this->IsOpened() );
		return this->_Seek(offset, FileRefPoint::Start);
	}


	File_offset_t File::SeekEnd(File_offset_t offset)
	{
		ExsDebugAssert( this->IsOpened() );
		return this->_Seek(offset, FileRefPoint::End);
	}


	bool File::Flush()
	{
		ExsDebugAssert( this->IsOpened() );
		int flushResult = fflush(this->_handle);

		if ((flushResult != 0) && ferror(this->_handle))
			return false;

		return true;
	}


	bool File::Rewind()
	{
		ExsDebugAssert( this->IsOpened() );
		rewind(this->_handle);
		return true;
	}


	bool File::Close()
	{
		if (!this->IsOpened())
			return true;

		int closeResult = fclose(this->_handle);

		this->_path.clear();
		this->_handle = nullptr;
		this->_accessMode = 0;

		return closeResult == 0;
	}


	File_offset_t File::GetCurrentOffset() const
	{
		if (!this->IsOpened())
			return Invalid_File_Offset;

		File_offset_t streamPos = ftell(this->_handle);

		if (streamPos < 0)
			return Invalid_File_Offset;

		return streamPos;
	}


	File_size_t File::GetLength() const
	{
		if (!this->IsOpened())
			return Invalid_File_Size;

		// Using file opened as FILE* by its descriptor can be risky due to caching. We flush all buffers to ensure,
		// that we are dealing with the most recent content. TODO: is that so...?

		fflush(this->_handle);

		struct stat statInfo { };
		
		int fileDesciptor = fileno(this->_handle);
		int statResult = fstat(fileDesciptor, &statInfo);

		if (statResult != 0)
			return Invalid_File_Size;
		
		return truncate_cast<File_size_t>(statInfo.st_size);
	}


	bool File::Exists(const std::string& filePath)
	{
		if (filePath.empty())
			return false;

		bool exists = System::CheckExistence(filePath.data());
		return exists;
	}


	File File::Create(const std::string& filePath, FileCreateMode createMode, Enum access)
	{
		File file { };

		if (!filePath.empty())
		{
			if (FileHandle fileHandle = System::CreateFile(filePath.data(), createMode, access))
				file = std::move(File(filePath, fileHandle, access));
		}

		return file;
	}


	File File::Open(const std::string& filePath, FileOpenMode openMode, Enum access)
	{
		File file { };

		if (!filePath.empty())
		{
			if (FileHandle fileHandle = System::OpenFile(filePath.data(), openMode, access))
				file = std::move(File(filePath, fileHandle, access));
		}

		return file;
	}


	File File::CreateTemporary()
	{
		File file { };
		std::string tmpFilePath = System::GetTemporaryFilePath(nullptr);

		if (!tmpFilePath.empty())
		{
			Enum fileAccess = AccessMode_Read | AccessMode_Write;
			FileHandle fileHandle = System::CreateFile(tmpFilePath.data(), FileCreateMode::Create_Always, fileAccess);
			file = std::move(File(tmpFilePath, fileHandle, fileAccess));
		}

		return file;
	}


	File_size_t File::_Read(void* buffer, Size_t bufferSize, File_size_t chunkSize, File_size_t chunksNum)
	{
		Size_t totalReadSize = chunkSize * chunksNum;

		if (totalReadSize > bufferSize)
		{
			// If read buffer is smaller than requested amount of bytes to read, shrink it to match the size of the buffer.
			totalReadSize = bufferSize;
			chunksNum = static_cast<File_size_t>(totalReadSize / chunkSize);
		}

		// Clear possible error flags.
		clearerr(this->_handle);

		// Read data from file.
		size_t readChunksNum = fread(buffer, chunkSize, chunksNum, this->_handle);

		// If nummber of bytes read is smaller than specified it means, that either an error has occured or EOF
		// was reached (it may be expected or not).
		if (readChunksNum < chunksNum)
		{
			if (int errCode = ferror(this->_handle))
			{
				ExsTraceError(TRC_Core_Filesystem, "fread() error has occured. Error code (ferror()) is %d.", errCode);
				totalReadSize = Invalid_File_Size;
			}
		}
		
		return truncate_cast<File_size_t>(readChunksNum);
	}


	File_size_t File::_Write(const void* data, File_size_t writeSize)
	{
		ExsDebugAssert( (this->_handle != nullptr) && data && (writeSize > 0) );

		clearerr(this->_handle);
		
		size_t truncWriteSize = truncate_cast<size_t>(writeSize);
		size_t writtenBytesNum = fwrite(data, truncWriteSize, 1, this->_handle);

		int errState = ferror(this->_handle);

		if (errState != 0)
			return Invalid_File_Size;

		return truncate_cast<File_size_t>(writtenBytesNum);
	}


	File_offset_t File::_Seek(File_offset_t offset, FileRefPoint refPoint)
	{
		ExsDebugAssert( this->_handle != nullptr );

		int seekOrigin = static_cast<int>(refPoint);
		int seekResult = fseek(this->_handle, offset, seekOrigin);

		if (seekResult != 0)
			return Invalid_File_Offset;

		return offset;
	}


}
