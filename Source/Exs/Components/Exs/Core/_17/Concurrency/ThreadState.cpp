
#include <Exs/Core/Concurrency/ThreadState.h>
#include <Exs/Core/Concurrency/SyncObject.h>
#include <Exs/Core/Concurrency/Thread.h>
#include <Exs/Core/Concurrency/ThreadSyncContext.h>


namespace Exs
{


	bool ThreadInternalState::AddChild(Thread* thread, ThreadUID threadUID)
	{
		// Iterate over child threads and check if this thread is not already there.
		auto childThreadRef = std::find_if(this->_childThreads.begin(), this->_childThreads.end(), [threadUID](const ChildThreadInfo& info) -> bool {
			return info.uid == threadUID;
		});

		if (childThreadRef != this->_childThreads.end())
		{
			return false;
		}

		ChildThreadInfo threadInfo;
		threadInfo.thread = thread;
		threadInfo.uid = threadUID;

		this->_childThreads.push_back(threadInfo);
		this->_childThreadsNum.fetch_add(1, std::memory_order_relaxed);

		return true;
	}


	void ThreadInternalState::RemoveChild(Thread* thread, ThreadUID threadUID)
	{
		auto childThreadRef = std::find_if(this->_childThreads.begin(), this->_childThreads.end(), [thread](const ChildThreadInfo& info) -> bool {
			return info.thread == thread;
		});

		ExsDebugAssert(childThreadRef->uid == threadUID);

		this->_childThreadsNum.fetch_sub(1, std::memory_order_relaxed);
		this->_childThreads.erase(childThreadRef);
	}




	void ThreadSyncState::NotifyThread()
	{
		this->_threadSyncContext->syncCnd.NotifyOne();
	}


	bool ThreadSyncState::InterruptSync()
	{
		ExsBeginCriticalSection(this->GetLock());
		{
			return this->InterruptSync(Tag::noLock);
		}
		ExsEndCriticalSection();
	}


	bool ThreadSyncState::InterruptSync(const NoLockTag&)
	{
		if (this->IsThreadWaiting())
		{
			ExsBeginCriticalSection(this->_syncObject->GetLock());
			{
				bool interruptResult = this->_syncObject->Interrupt(this->_owner);

				// We hold the syncObject's lock here, so if IsThreadWaiting() returned true,
				// Interrupt() must also return true, since the state could not be changed (lock).
				ExsDebugAssert(interruptResult);

				return interruptResult;
			}
			ExsEndCriticalSection();
		}

		return false;
	}


	void ThreadSyncState::SetActiveWaitState(SyncObject* syncObject)
	{
		this->_syncObject = syncObject;
		this->_waitStateActive.store(true, std::memory_order_relaxed);
	}


	void ThreadSyncState::ResetWaitState()
	{
		this->_syncObject = nullptr;
		this->_waitStateActive.store(false, std::memory_order_relaxed);
	}


}
