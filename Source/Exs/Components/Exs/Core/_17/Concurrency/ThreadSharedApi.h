
#ifndef __Exs_Core_ThreadSharedApi_H__
#define __Exs_Core_ThreadSharedApi_H__

#include "ConcrtCommon.h"


namespace Exs
{


	struct ThreadLocalStorage;

	
	///<summary>
	///</summary>
	class CurrentThread
	{
	public:
		///<summary>  </summary>
		static ThreadLocalStorage* GetLocalStorage();
		
		///<summary>  </summary>
		static Thread* GetThread();
		
		///<summary>  </summary>
		static ThreadUID GetUID();
	};


}


#endif /* __Exs_Core_ThreadSharedApi_H__ */
