
#ifndef __Exs_Core_ThreadState_H__
#define __Exs_Core_ThreadState_H__

#include "ThreadInternal.h"


namespace Exs
{


	///<summary>
	///</summary>
	template <class Lock_t>
	class ThreadBaseState : public Lockable<Lock_t, LockAccess::Relaxed>
	{
		friend class Thread;

	protected:
		Thread*    _owner;
		ThreadUID  _ownerUID;

	public:
		ThreadBaseState( Thread* owner )
			: _owner( owner )
			, _ownerUID( Thread_UID_Invalid )
		{ }

	friendapi:
		//
		void SetUID( ThreadUID threadUID )
		{
			this->_ownerUID = threadUID;
		}
	};


	///<summary>
	///</summary>
	class ThreadInternalState : public ThreadBaseState<SharedMutex>
	{
		friend class Thread;

	public:
		struct ChildThreadInfo
		{
			Thread* thread;
			ThreadUID uid;
		};

		typedef std::vector<ChildThreadInfo> ChildThreadList;

	private:
		ChildThreadList      _childThreads;
		std::atomic<Size_t>  _childThreadsNum;

	public:
		ThreadInternalState( Thread* owner )
			: ThreadBaseState( owner )
			, _childThreadsNum( 0 )
		{ }

		// Returns internal list of currently registsred child threads.
		const ChildThreadList& GetChildThreads() const;

	friendapi:
		// Adds new thread to the list of child threads for the owning thread.
		bool AddChild( Thread* thread, ThreadUID threadUID );

		// Removes thread from the list of child threads.
		void RemoveChild( Thread* thread, ThreadUID threadUID );
	};


	inline const ThreadInternalState::ChildThreadList& ThreadInternalState::GetChildThreads() const
	{
		return this->_childThreads;
	}


	///<summary>
	///</summary>
	class ThreadSyncState : public ThreadBaseState<LightMutex>
	{
		friend class SyncThreadInterface;
		friend class Thread;

	private:
		SyncObject*  _syncObject; // Pointer to a SO thread is currently waiting on.
		void*        _threadSyncContext; // SyncContext allocated for the thread which owns this state.

	public:
		ThreadSyncState( Thread* owner )
			: ThreadBaseState( owner )
		{ }

		//
		ThreadSyncContext* GetSyncContext() const;

		//
		bool IsThreadWaiting() const;

	friendapi: // Thread
		// Interrupts current wait (if thread is waiting). Must *NOT* be called with lock of this object acquired (locks internally).
		bool InterruptSync();

		// Interrupts current wait (if thread is waiting). Requires manual locking of the internal lock.
		bool InterruptSync( const NoLockTag& );

		// Sets sync context allocated for the thread which owns this state.
		void SetThreadSyncContext( ThreadSyncContext* threadSyncContext );

	friendapi: // SyncObject
		// Updates thread's state and sets it to waiting. syncObject is an object thread is waiting for.
		void SetActiveWaitState( SyncObject* syncObject );

		// Resets thread's state to idle, clears syncObject.
		void ResetWaitState();
	};


}


#endif /* __Exs_Core_ThreadState_H__ */
