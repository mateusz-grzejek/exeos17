
#ifndef __Exs_Core_ThreadActivityLog_H__
#define __Exs_Core_ThreadActivityLog_H__

#include "ConcrtCommon.h"
#include "ThreadInternal.h"


namespace Exs
{


	class ThreadActivityLog;
	class ThreadSystemManager;


	///<summary>
	///</summary>
	enum class ThreadActivityEventCode : Enum
	{
		Reg_Evt_Thread_Registered = 1,
		Reg_Evt_Thread_Unregistered,
		State_Evt
	};


	///<summary>
	///</summary>
	struct ThreadActivityEventInfo
	{
		ThreadActivityEventCode code;

		ThreadUID threadUID;
	};


	///<summary>
	///</summary>
	class EXS_LIBCLASS_CORE ThreadActivityLog : public Lockable<SharedMutex, LockAccess::Relaxed>
	{
		friend class ThreadSystemManager;

	public:
		typedef std::vector<ThreadActivityEventInfo> EventList;

	private:
		ThreadSystemManager*  _thrSystemManager;
		EventList             _eventList;

	public:
		ThreadActivityLog(ThreadSystemManager* thrSystemManager);

		///<summary>
		///</summary>
		const EventList& GetEventList() const;

	friendapi:
		void PostRegistrationEvent(ThreadActivityEventCode eventCode, ThreadUID threadUID);
	};


	inline const ThreadActivityLog::EventList& ThreadActivityLog::GetEventList() const
	{
		return this->_eventList;
	}


}


#endif /* __Exs_Core_ThreadActivityLog_H__ */
