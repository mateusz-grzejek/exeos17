

#ifndef __Exs_Core_SyncObject_H__
#define __Exs_Core_SyncObject_H__

#include "SyncInternal.h"


namespace Exs
{


	///<summary>
	///</summary>
	template <ThreadSyncStateUpdateMode TSUMode = ThreadSyncStateUpdateMode::Default>
	class InternalSyncObject : public SyncObjectCore<InternalSyncObjectInterfaceProvider>
	{
	public:
		InternalSyncObject(Thread* owningThread)
		: SyncObjectCore(this, owningThread)
		{ }

		template <class Lock>
		WaitResult Wait(Lock& lock)
		{
			return this->template ExecuteWait<TSUMode>(lock);
		}

		template <class Lock, class Predicate>
		WaitResult Wait(Lock& lock, Predicate predicate)
		{
			return this->template ExecuteWait<TSUMode>(lock, predicate);
		}

		template <class Lock, class Duration_t>
		WaitResult WaitFor(Lock& lock, const Duration_t& duration)
		{
			return this->template ExecuteWaitFor<TSUMode>(lock, duration);
		}

		template <class Lock, class Duration_t, class Predicate>
		WaitResult WaitFor(Lock& lock, const Duration_t& duration, Predicate predicate)
		{
			return this->template ExecuteWaitFor<TSUMode>(lock, duration, predicate);
		}
	};


	///<summary>
	///</summary>
	template <ThreadSyncStateUpdateMode TSUMode = ThreadSyncStateUpdateMode::Default>
	class UniqueSyncObject : public SyncObjectCore<UniqueSyncObjectInterfaceProvider>
	{
	public:
		UniqueSyncObject()
		: SyncObjectCore()
		{ }

		template <class Lock>
		WaitResult Wait(Lock& lock)
		{
			return this->template ExecuteWait<TSUMode>(lock);
		}

		template <class Lock, class Predicate>
		WaitResult Wait(Lock& lock, Predicate predicate)
		{
			return this->template ExecuteWait<TSUMode>(lock, predicate);
		}

		template <class Lock, class Duration_t>
		WaitResult WaitFor(Lock& lock, const Duration_t& duration)
		{
			return this->template ExecuteWaitFor<TSUMode>(lock, duration);
		}

		template <class Lock, class Duration_t, class Predicate>
		WaitResult WaitFor(Lock& lock, const Duration_t& duration, Predicate predicate)
		{
			return this->template ExecuteWaitFor<TSUMode>(lock, duration, predicate);
		}
	};


	///<summary>
	///</summary>
	template <ThreadSyncStateUpdateMode TSUMode = ThreadSyncStateUpdateMode::Default>
	class SharedSyncObject : public SyncObjectCore<SharedSyncObjectInterfaceProvider>
	{
	public:
		SharedSyncObject()
		: SyncObjectCore()
		{ }

		template <class Lock>
		WaitResult Wait(Lock& lock)
		{
			return this->template ExecuteWait<TSUMode>(lock);
		}

		template <class Lock, class Predicate>
		WaitResult Wait(Lock& lock, Predicate predicate)
		{
			return this->template ExecuteWait<TSUMode>(lock, predicate);
		}

		template <class Lock, class Duration_t>
		WaitResult WaitFor(Lock& lock, const Duration_t& duration)
		{
			return this->template ExecuteWaitFor<TSUMode>(lock, duration);
		}

		template <class Lock, class Duration_t, class Predicate>
		WaitResult WaitFor(Lock& lock, const Duration_t& duration, Predicate predicate)
		{
			return this->template ExecuteWaitFor<TSUMode>(lock, duration, predicate);
		}
	};


}


#endif /* __Exs_Core_SyncObject_H__ */
