
#include <Exs/Core/Concurrency/CoreThreadSystemState.h>
#include <Exs/Core/Concurrency/ThreadSystemManager.h>


namespace Exs
{


	struct CoreThreadSystemState::InitData
	{
		std::unique_ptr<ThreadSystemManager> threadSystemManager;
		std::unique_ptr<CoreMessageDispatcher> coreMessageDispatcher;

		InitData() = default;
		InitData( InitData && ) = default;
	};


	CoreThreadSystemState::CoreThreadSystemState( InitData && initData )
		: _threadSystemManager( std::move( initData.threadSystemManager ) )
		, _coreMessageDispatcher( std::move( initData.coreMessageDispatcher ) )
		, threadSystemManager( _threadSystemManager.get() )
		, coreMessageDispatcher( _coreMessageDispatcher.get() )
	{ }


	CoreThreadSystemState::~CoreThreadSystemState() = default;




	SharedHandle<CoreThreadSystemState> CoreThreadSystemStateBuilder::CreateState()
	{
		CoreThreadSystemState::InitData initData;

		// TODO: if (enableThreading)
		initData.threadSystemManager = this->CreateThreadSystemManager();

		// TODO: if (enableThreadMessages)
		initData.coreMessageDispatcher = this->CreateCoreMessageDispatcher();

		auto state = std::make_shared<CoreThreadSystemState>( std::move( initData ) );

		return SharedHandle<CoreThreadSystemState>{ state };
	}


	std::unique_ptr<ThreadSystemManager> CoreThreadSystemStateBuilder::CreateThreadSystemManager()
	{
		return std::make_unique<ThreadSystemManager>();
	}


	std::unique_ptr<CoreMessageDispatcher> CoreThreadSystemStateBuilder::CreateCoreMessageDispatcher()
	{
		return std::make_unique<CoreMessageDispatcher>();
	}


}
