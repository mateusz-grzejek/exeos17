
#include <Exs/Core/Concurrency/SyncInternal.h>
#include <Exs/Core/Concurrency/Thread.h>
#include <Exs/Core/Concurrency/ThreadInternal.h>
#include <Exs/Core/Concurrency/ThreadSharedApi.h>
#include <Exs/Core/Concurrency/ThreadState.h>
#include <Exs/Core/Concurrency/ThreadSyncContext.h>
#include <Exs/Core/Exception.h>


namespace Exs
{


	CommonSyncObjectBase::CommonSyncObjectBase()
	{ }


	bool CommonSyncObjectBase::NotifyOne( SystemMutexLock& lock )
	{
		if (!this->_activeContexts.empty())
		{
			auto* context = this->_activeContexts.front();
			this->_activeContexts.pop_front();

			ExsDebugAssert( context->IsActive() );

			context->SetNotifyFlag();
			context->NotifyThread();

			return true;
		}

		return false;
	}


	bool CommonSyncObjectBase::NotifyAll( SystemMutexLock& lock )
	{
		if (!this->_activeContexts.empty())
		{
			for (auto* context : this->_activeContexts)
			{
				ExsDebugAssert( context->IsActive() );

				context->SetNotifyFlag();
				context->NotifyThread();
			}

			this->_activeContexts.clear();

			return true;
		}

		return false;
	}


	bool CommonSyncObjectBase::Interrupt( SystemMutexLock& lock )
	{
		if (!this->_activeContexts.empty())
		{
			for (auto* context : this->_activeContexts)
			{
				ExsDebugAssert( context->IsActive() );

				context->SetInterruptFlag();
				context->NotifyThread();
			}

			this->_activeContexts.clear();

			return true;
		}

		return false;
	}


	CommonSyncObjectBase::SyncContextType* CommonSyncObjectBase::AcquireContext()
	{
		for (auto& context : this->_contextPool)
		{
			if (!context.acquireFlag)
			{
				context.acquireFlag = true;
				context.thread = CurrentThread::GetThread();
				context.SetActive();

				return &context;
			}
		}

		return nullptr;
	}


	void CommonSyncObjectBase::ReleaseContext( SyncContextType* waitContext )
	{
		waitContext->Reset();
		waitContext->thread = nullptr;
		waitContext->acquireFlag = false;
	}


	void CommonSyncObjectBase::OnWaitBegin( SyncContextType* syncContext )
	{
		this->_activeContexts.push_back( syncContext );
	}


	void CommonSyncObjectBase::OnWaitEnd( SyncContextType* syncContext )
	{
		ExsDebugCode( {
			auto contextRef = std::find_if( this->_activeContexts.begin(), this->_activeContexts.end(), [syncContext]( const auto* context ) -> bool {
				return context == syncContext;
			} );
			ExsDebugAssert( contextRef == this->_activeContexts.end() );
		} );
	}




	InternalSyncObjectBase::InternalSyncObjectBase( Thread* owningThread )
		: _owningThread( owningThread )
	{ }


	bool InternalSyncObjectBase::NotifyOne( SystemMutexLock& lock )
	{
		if (this->_syncContext.IsActive())
		{
			this->_syncContext.SetNotifyFlag();
			this->_syncContext.NotifyThread();

			return true;
		}

		return false;
	}


	bool InternalSyncObjectBase::NotifyAll( SystemMutexLock& lock )
	{
		// As only one thread can wait for the InternalSyncObject,
		// NotifyAll() has the same effect as NotifyOne().

		return this->NotifyOne( lock );
	}


	bool InternalSyncObjectBase::Interrupt( SystemMutexLock& lock )
	{
		if (this->_syncContext.IsActive())
		{
			this->_syncContext.SetInterruptFlag();
			this->_syncContext.NotifyThread();

			return true;
		}

		return false;
	}


	InternalSyncObjectBase::SyncContextType* InternalSyncObjectBase::AcquireContext()
	{
		// InternalSyncObject can be used only by the owning thread (specified at construction time).
		// Get current thread and check if it is the right one. Debug only (performance impact).

		ExsDebugCode( {
			Thread* currentThread = CurrentThread::GetThread();
			ExsDebugAssert( currentThread == this->_owningThread );
		} );

		this->_syncContext.thread = this->_owningThread;
		this->_syncContext.SetActive();

		return &(this->_syncContext);
	}


	void InternalSyncObjectBase::ReleaseContext( SyncContextType* waitContext )
	{
		ExsDebugAssert( waitContext == &(this->_syncContext) );

		this->_syncContext.Reset();
		this->_syncContext.thread = nullptr;
	}


	void InternalSyncObjectBase::OnWaitBegin( SyncContextType* syncContext )
	{ }


	void InternalSyncObjectBase::OnWaitEnd( SyncContextType* syncContext )
	{ }




	UniqueSyncObjectBase::UniqueSyncObjectBase()
	{ }


	bool UniqueSyncObjectBase::NotifyOne( SystemMutexLock& lock )
	{
		if (this->_syncContext.IsActive())
		{
			this->_syncContext.SetNotifyFlag();
			this->_syncContext.NotifyThread();

			return true;
		}

		return false;
	}


	bool UniqueSyncObjectBase::NotifyAll( SystemMutexLock& lock )
	{
		// As only one thread can wait for the UniqueSyncObject,
		// NotifyAll() has the same effect as NotifyOne().

		return this->NotifyOne( lock );
	}


	bool UniqueSyncObjectBase::Interrupt( SystemMutexLock& lock )
	{
		if (this->_syncContext.IsActive())
		{
			this->_syncContext.SetInterruptFlag();
			this->_syncContext.NotifyThread();
			return true;
		}

		return false;
	}


	UniqueSyncObjectBase::SyncContextType* UniqueSyncObjectBase::AcquireContext()
	{
		if (!this->_acquireFlag.test_and_set())
		{
			return nullptr;
		}

		this->_currentThread = CurrentThread::GetThread();
		this->_syncContext.thread = this->_currentThread;
		this->_syncContext.SetActive();

		return &(this->_syncContext);
	}


	void UniqueSyncObjectBase::ReleaseContext( SyncContextType* waitContext )
	{
		ExsDebugAssert( waitContext == &(this->_syncContext) );

		this->_syncContext.Reset();
		this->_syncContext.thread = this->_currentThread;
		this->_acquireFlag.clear();
	}


	void UniqueSyncObjectBase::OnWaitBegin( SyncContextType* syncContext )
	{ }


	void UniqueSyncObjectBase::OnWaitEnd( SyncContextType* syncContext )
	{ }


}
