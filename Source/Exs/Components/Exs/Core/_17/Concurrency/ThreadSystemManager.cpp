
#include <Exs/Core/Concurrency/ThreadSystemManager.h>
#include <Exs/Core/Concurrency/Thread.h>
#include <Exs/Core/Concurrency/ThreadInternal.h>
#include <Exs/Core/Exception.h>


namespace Exs
{


	ThreadUID ThreadSystemManager::QueryThreadUID(ThreadRefID threadRefID) const
	{
		ExsBeginSharedCriticalSectionWithSharedAccess(this->_registry.GetLock());
		{
			return this->_registry.QueryThreadUID(threadRefID);
		}
		ExsEndCriticalSection();
	}


	bool ThreadSystemManager::IsThreadRegistered(ThreadRefID threadRefID) const
	{
		ExsBeginSharedCriticalSectionWithSharedAccess(this->_registry.GetLock());
		{
			return this->_registry.IsThreadRegistered(threadRefID);
		}
		ExsEndCriticalSection();
	}


	bool ThreadSystemManager::IsThreadRegistered(ThreadUID threadUID) const
	{
		ExsBeginSharedCriticalSectionWithSharedAccess(this->_registry.GetLock());
		{
			return this->_registry.IsThreadRegistered(threadUID);
		}
		ExsEndCriticalSection();
	}


	const ThreadCoreInfo* ThreadSystemManager::RegisterThread(Thread* thread, ThreadInternalState* internalState, const ThreadRegSpecification& regSpecification)
	{
		// Increment reg counter. If new thread can be registered (limit has not been reached), increment() will return
		// a value up to maxThreads - 1, indicating how many threads were registered so far (without this one).
		auto regCounterValue = this->_regCounter.increment();

		if (regCounterValue < maxThreadsNum)
		{
			// Allocate entry for the thread from the pool.
			auto threadEntryRef = this->_threadPersistentDataIndex.Alloc();

			// Allocation should never fail if reg counter was incremented.
			ExsDebugAssert( threadEntryRef );

			auto threadBaseType = thread->GetBaseType();
			auto threadIndex = truncate_cast<Thread_index_t>(threadEntryRef.key);
			auto threadUID = ThreadInternal::GenerateThreadUID(thread, threadBaseType, threadIndex, regSpecification.refID);

			// Add thread to the registry. Returns true if successfuly registered.
			// If it returns false, no new threads can be registered. [LOCK: registry]

			if (this->_RegisterThreadInternal(thread, threadUID, regSpecification))
			{
				// Handle to a thread is in fact a pointer to the persistent base info object allocated for that thread.
				const auto* threadHandle = &(threadEntryRef->coreInfo);
				
				threadEntryRef->internalState = internalState;
				threadEntryRef->thread = thread;
				threadEntryRef->coreInfo.baseType = threadBaseType;
				threadEntryRef->coreInfo.regIndex = threadIndex;
				threadEntryRef->coreInfo.uid = threadUID;
				
				// Save registration event in the activity log. [LOCK: activity log]
				this->_PostRegistrationEvent(ThreadActivityEventCode::Reg_Evt_Thread_Registered, threadUID);

				return threadHandle;
			}

			// If thread could not be registered, ensure proper cleanup - release data back to the pool
			// and decrement reg counter (order is important - see assertion after pool.Alloc()).

			this->_threadPersistentDataIndex.Release(threadEntryRef);
			this->_regCounter.decrement();
		}

		return nullptr;
	}


	void ThreadSystemManager::UnregisterThread(Thread* thread, const ThreadCoreInfo* coreInfo)
	{
		ExsDebugAssert( thread != nullptr );
		ExsDebugAssert( coreInfo != nullptr );

		// Fetch UID from the thread to compare it against the one from the base info object.
		auto threadUID = thread->GetUID();

		if (this->_UnregisterThreadInternal(thread, threadUID))
		{
			auto threadIndex = coreInfo->regIndex;
			auto& threadEntry = this->_threadPersistentDataIndex.Fetch(threadIndex);

			threadEntry.internalState = nullptr;
			threadEntry.thread = nullptr;
			threadEntry.coreInfo.baseType = ThreadBaseType::Unknown;
			threadEntry.coreInfo.regIndex = Thread_Index_Invalid;
			threadEntry.coreInfo.uid = Thread_UID_Invalid;

			this->_threadPersistentDataIndex.Release(threadIndex);
			this->_regCounter.decrement();

			// Save registration event in the activity log. [LOCK: activity log]
			this->_PostRegistrationEvent(ThreadActivityEventCode::Reg_Evt_Thread_Unregistered, threadUID);
		}
	}


	bool ThreadSystemManager::_RegisterThreadInternal(Thread* thread, ThreadUID threadUID, const ThreadRegSpecification& regSpecification)
	{
		ExsBeginSharedCriticalSectionWithUniqueAccess(this->_registry.GetLock());
		{
			return this->_registry.RegisterThread(thread, threadUID, regSpecification);
		}
		ExsEndCriticalSection();
	}


	bool ThreadSystemManager::_UnregisterThreadInternal(Thread* thread, ThreadUID threadUID)
	{
		ExsBeginSharedCriticalSectionWithUniqueAccess(this->_registry.GetLock());
		{
			return this->_registry.UnregisterThread(thread, threadUID);
		}
		ExsEndCriticalSection();
	}


	void ThreadSystemManager::_PostRegistrationEvent(ThreadActivityEventCode eventCode, ThreadUID threadUID)
	{
		ExsBeginSharedCriticalSectionWithUniqueAccess(this->_activityLog.GetLock());
		{
			this->_activityLog.PostRegistrationEvent(eventCode, threadUID);
		}
		ExsEndCriticalSection();
	}


}
