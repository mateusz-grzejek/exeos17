
#ifndef __Exs_Core_ThreadLocalStorage_H__
#define __Exs_Core_ThreadLocalStorage_H__

#include "ConcrtCommon.h"


namespace Exs
{


	struct ThreadCoreInfo;
	struct ThreadLocalStorage;

	class ThreadSystemManager;
	class TLSReleaseCallbackController;

	// Type of function that may be registered and called before local storage data is released.
	using TLSReleaseCallback = std::function<void(ThreadLocalStorage*)>;


	namespace Config
	{

		enum : Size_t
		{
			CCRT_TLS_Max_User_Data_Entries_Num = 80,

			CCRT_TLS_Max_User_Release_Callbacks_Num = 32
		};

	}


	enum : Uint32
	{
		Invalid_TLS_User_Data_Index = stdx::limits<Uint32>::max_value
	};


	struct ThreadLocalStorageInitParams
	{
		const ThreadCoreInfo* threadCoreInfo;
	};


	///<summary>
	///</summary>
	struct ThreadLocalStorage
	{
	public:
		using UserDataPtrArray = std::array<void*, Config::CCRT_TLS_Max_User_Data_Entries_Num>;

		//
		struct alignas(64) ControlBlock
		{
			/// Validation key, set after successful initialization of a TLS data.
			Uint32 validationKey /* = 0 */;

			/// Represents number of TLS fetches BEFORE initialization.
			Uint32 uncheckedFetchCount /* = 0 */;
		};

		//
		struct alignas(64) InternalState
		{
			/// UID of a thread which owns this TLS.
			ThreadUID threadUID /* = Thread_UID_Invalid */;

			/// Pointer to a thread which owns this TLS.
			Thread* thread /* = nullptr */;

			/// Sync context reserved for the owning thread.
			ThreadSyncContext* syncContext /* = nullptr */;

			///
			TLSReleaseCallbackController* releaseCallbackController /* = nullptr */;
		};

	public:
		ControlBlock controlBlock;
		InternalState internalState;
		UserDataPtrArray userDataArray;
	};


	///<summary>
	///</summary>
	class TLSInterface
	{
		friend class ThreadInternal;

	public:
		static void RegisterReleaseCallback(ThreadLocalStorage* tls, TLSReleaseCallback&& callback);

		static void RegisterReleaseCallback(ThreadLocalStorage* tls, const TLSReleaseCallback& callback);

		static bool SetEmptyState(ThreadLocalStorage* tls);

	private:
		static bool Initialize(ThreadLocalStorage* tls, const ThreadLocalStorageInitParams& initParams);

		static void Release(ThreadLocalStorage* tls);

		static bool Validate(ThreadLocalStorage* tls);
	};


}


#endif /* __Exs_Core_ThreadLocalStorage_H__ */
