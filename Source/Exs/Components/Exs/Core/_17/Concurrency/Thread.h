
#ifndef __Exs_Core_Thread_H__
#define __Exs_Core_Thread_H__

#include "ThreadState.h"


namespace Exs
{


	struct ThreadLocalStorage;

	class CoreMessageController;
	class CoreMessageDispatcher;
	class SystemThread;
	class Thread;
	class ThreadSystemManager;


	enum : Enum
	{
		RSC_Err_Thr_Registration_Failed
	};


	///<summary>
	///</summary>
	struct ThreadStartParameters
	{
		ThreadRegSpecification regSpecification;
	};


	///<summary>
	///</summary>
	class EXS_LIBCLASS_CORE Thread
	{
		EXS_DECLARE_NONCOPYABLE(Thread);

		friend class SyncThreadInterface;

	private:
		SharedThreadSystemStateHandle           _tsSharedState; // Active handle guarantees, that even detached threads will have valid state.
		ThreadInternalState                     _internalState; // Internal state object - contains core data of the thread (e.g. list of its child threads).
		ThreadSyncState                         _syncState; // Sync state object - stores synchronization info, allows for sync interruption.
		ThreadSystemManager*                    _threadSystemManager; // Pointer to the control manager.
		CoreMessageDispatcher*                  _messageDispatcher; // Pointer to the message dispatcher.
		const ThreadCoreInfo*                   _coreInfo; // Core info of this thread object. Retrieved from ThreadSystemManager after successful registration.
		ThreadUID                               _uid; // Unique ID.
		SystemThread*                           _systemThread; // SystemThread representing actual os-level thread.
		ThreadLocalStorage*                     _tls; // Pointer to the local storage initialized for this thread.
		std::unique_ptr<CoreMessageController>  _messageController; // Transceiver object used to send messages. NULL if thread does not have access to messaging.

	public:
		Thread(SharedThreadSystemStateHandle tsSharedState);

		virtual ~Thread();

		///
		virtual ThreadBaseType GetBaseType() const = 0;

		///
		// virtual const ThreadProperties& GetProperties() const = 0;

		///
		const ThreadInternalState& GetInternalState() const;

		///
		const ThreadSyncState& GetSyncState() const;

		///
		ThreadRefID GetRefID() const;

		///
		ThreadUID GetUID() const;

	protected:
		bool AddChildThread(Thread* thread);

		void RemoveChildThread(Thread* thread);

		bool RegisterMessageController(bool activate);

		void UnregisterMessageController();

	friendapi:
		// Initializes thread interface and runs SystemThread.
		Result Start(const ThreadStartParameters& startParams);

		//
		ThreadInternalState& GetInternalState();

		//
		ThreadSyncState& GetSyncState();

		//
		ThreadSyncContext* GetSyncContext();

	private:
		//
		bool Register(const ThreadRegSpecification& regSpecification);

		//
		void Unregister();

		//
		ThreadInitResult Initialize();

		//
		void Release();

		//
		virtual void Entry() = 0;

		// Thread subtype: custom on-init - initializes subtype-specific data (Persistent/Transient/etc.).
		virtual bool InternalOnInit(ThreadLocalStorage* tls) = 0;

		// Thread subtype: custom on-release - releases subtype-specific data (Persistent/Transient/etc.).
		virtual void InternalOnRelease(ThreadLocalStorage* tls) noexcept = 0;

		// User: custom internal on-init method.
		virtual bool OnInit(ThreadLocalStorage* tls);

		// User: custom internal on-release method.
		virtual void OnRelease(ThreadLocalStorage* tls) noexcept;

		// Thread execution function, passed to SystemThread.
		static void _ExecutionWrapper(Thread* thread);

		// Exetrnal initialization function, called by SystemThread before _ExecutionWrapper() is executed.
		static bool _ExternalInit(Thread* thread);

		// Exetrnal release function, called by SystemThread after _ExecutionWrapper() exits.
		static void _ExternalRelease(Thread* thread);
	};


	class EXS_LIBCLASS_CORE ApplicationThread : public Thread
	{
	private:
		Thread* _parent;

	public:
		/// @override Thread::GetCategory.
		virtual ThreadBaseType GetBaseType() const override final;

	private:
		/// @override Thread::InternalOnInit.
		virtual bool InternalOnInit(ThreadLocalStorage* tls) override final;

		/// @override Thread::InternalOnRelease.
		virtual void InternalOnRelease(ThreadLocalStorage* tls) noexcept override final;
	};


	class EXS_LIBCLASS_CORE BackgroundThread : public Thread
	{
	public:
		/// @override Thread::GetCategory.
		virtual ThreadBaseType GetBaseType() const override final;

	private:
		/// @override Thread::InternalOnInit.
		virtual bool InternalOnInit(ThreadLocalStorage* tls) override final;

		/// @override Thread::InternalOnRelease.
		virtual void InternalOnRelease(ThreadLocalStorage* tls) noexcept override final;
	};


	class EXS_LIBCLASS_CORE TemporaryThread : public Thread
	{
	private:
		Thread* _parent;

	public:
		/// @override Thread::GetCategory.
		virtual ThreadBaseType GetBaseType() const override final;

	private:
		/// @override Thread::InternalOnInit.
		virtual bool InternalOnInit(ThreadLocalStorage* tls) override final;

		/// @override Thread::InternalOnRelease.
		virtual void InternalOnRelease(ThreadLocalStorage* tls) noexcept override final;
	};


	class EXS_LIBCLASS_CORE WorkerThread : public Thread
	{
	private:
		Thread* _parent;

	public:
		/// @override Thread::GetCategory.
		virtual ThreadBaseType GetBaseType() const override final;

	private:
		/// @override Thread::InternalOnInit.
		virtual bool InternalOnInit(ThreadLocalStorage* tls) override final;

		/// @override Thread::InternalOnRelease.
		virtual void InternalOnRelease(ThreadLocalStorage* tls) noexcept override final;
	};


};


#endif /* __Exs_Core_Thread_H__ */
