
#ifndef __Exs_Core_LockedResource_H__
#define __Exs_Core_LockedResource_H__

#include "ConcrtCommon.h"


namespace Exs
{


	///<summary>
	///</summary>
	template <class T, class Lock>
	class LockedResource
	{
	private:
		Lock _lock;
		T*   _resource;

	public:
		LockedResource(Lock&& lock, T& resource)
		: _lock(std::move(lock))
		, _resource(&resource)
		{ }

		explicit operator bool() const
		{
			return this->_resource != nullptr;
		}

		T& operator*() const
		{
			return *(this->_resource);
		}

		T* operator->() const
		{
			return this->_resource;
		}
	};


}


#endif /* __Exs_Core_LockedResource_H__ */
