
#ifndef __Exs_Core_ThreadException_H__
#define __Exs_Core_ThreadException_H__

#include "ThreadInternal.h"
#include <Exs/Core/Exception.h>


namespace Exs
{


	enum : Enum
	{
		// Reuqest for thread interruption (exit command).
		EXC_Thread_Exit = ExsEnumDeclareExceptionCode(ExceptionType::Interrupt, 0x0001),

		// Reuqest for thread interruption (termination command).
		EXC_Thread_Terminate,

		//
		EXC_Thread_Initialization_Error = ExsEnumDeclareExceptionCode(ExceptionType::Runtime, 0x0007),
	};


	template <class Base_exception>
	class ThreadException : public Base_exception
	{
	private:
		ThreadUID  _srcThreadUID; // UID of thread which thrown the exception.

	public:
		ThreadException(const ExceptionInfo& info, ThreadUID srcThreadUID = ThreadUID()) noexcept
		: Base_exception(info)
		, _srcThreadUID(srcThreadUID)
		{ }
		
		ThreadUID GetSrcThreadUID() const
		{
			return this->_srcThreadUID;
		}
	};


	///<summary>
	///</summary>
	class ThreadInitializationErrorException : public ThreadException<RuntimeException>
	{
	private:
		ThreadInitResult  _initResult;

	public:
		ThreadInitializationErrorException(const ExceptionInfo& info, ThreadInitResult initResult) noexcept
		: ThreadException(info)
		, _initResult(initResult)
		{ }

		ThreadInitResult GetInitResult() const
		{
			return this->_initResult;
		}

		static const char* TranslateInitResultToString(ThreadInitResult initResult);
	};


	///<summary>
	///</summary>
	class ThreadInterruptException : public ThreadException<InterruptException>
	{
	private:
		Enum  _interruptCode;

	public:
		ThreadInterruptException(const ExceptionInfo& info, ThreadUID srcThreadUID, Enum interruptCode) noexcept
		: ThreadException(info, srcThreadUID)
		, _interruptCode(interruptCode)
		{ }

		Enum GetInterruptCode() const
		{
			return this->_interruptCode;
		}
	};


	EXS_EXCEPTION_SET_CODE_TYPE(EXC_Thread_Initialization_Error, ThreadInitializationErrorException);


}


#endif /* __Exs_Core_ThreadException_H__ */
