
#include <Exs/Core/Concurrency/ThreadActivityLog.h>


namespace Exs
{


	ThreadActivityLog::ThreadActivityLog(ThreadSystemManager* thrSystemManager)
	: _thrSystemManager(thrSystemManager)
	{ }


	void ThreadActivityLog::PostRegistrationEvent(ThreadActivityEventCode eventCode, ThreadUID threadUID)
	{
		ThreadActivityEventInfo eventInfo;
		eventInfo.code = eventCode;
		eventInfo.threadUID = threadUID;

		this->_eventList.push_back(eventInfo);
	}


}

#include <Exs/Core/Concurrency/SyncObject.h>


namespace Exs
{

	template class InternalSyncObject<ThreadSyncStateUpdateMode::Update>;
	template class InternalSyncObject<ThreadSyncStateUpdateMode::No_Update>;

	template class UniqueSyncObject<ThreadSyncStateUpdateMode::Update>;
	template class UniqueSyncObject<ThreadSyncStateUpdateMode::No_Update>;

	template class SharedSyncObject<ThreadSyncStateUpdateMode::Update>;
	template class SharedSyncObject<ThreadSyncStateUpdateMode::No_Update>;

}
