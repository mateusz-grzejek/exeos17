
#include <Exs/Core/Concurrency/ThreadInternal.h>
#include <Exs/Core/Concurrency/ThreadLocalStorage.h>
#include <Exs/Core/Concurrency/ThreadSharedApi.h>
#include <Exs/Core/Exception.h>


namespace Exs
{


	ThreadLocalStorage* CurrentThread::GetLocalStorage()
	{
		// Get the TLS for the current thread.
		auto tlsInfo = ThreadInternal::GetThreadLocalStorageUnchecked();
		
		if (tlsInfo.second == 0)
		{
			// TLS fetch counter should be at least 1 - this function cannot be used
			// to fetch the TLS before it is initialized (Thread::Initialize()).

			ExsThrowExceptionEx(EXC_Invalid_Operation, "");
		}

		return tlsInfo.first;
	}


	Thread* CurrentThread::GetThread()
	{
		ThreadLocalStorage* threadLocalStorage = GetLocalStorage();
		return threadLocalStorage->internalState.thread;
	}


	ThreadUID CurrentThread::GetUID()
	{
		ThreadLocalStorage* threadLocalStorage = GetLocalStorage();
		return threadLocalStorage->internalState.threadUID;
	}


}
