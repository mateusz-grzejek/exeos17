
#ifndef __Exs_Core_ConcurrentDataPool_H__
#define __Exs_Core_ConcurrentDataPool_H__

/*
 * ConcurrentDataPool.h
 *
 * Implementation of a simple concurrent pool of homogenous data, that can be safely acquired by multiple threads
 * at the same time in a lockless manner.
 *
 * Conceptually, data pool stores a fixed array of objects of some type. Each of these objects can be "reserved"
 * by a single thread - when it happens, reserved objects becomes a property of that thread and no other thread
 * can even touch it (it is marked as inaccessible). When thread does not use its data anymore, it should released,
 * so other threads can use it when such need arises.
 *
 * Details:
 * - pool can store objects either as a plain objects (by value) or via pointers (e.g. you can instantiate both
 *   ConcurrentDataPool<MyClass, N> and ConcurrentDataPool<void*, M>),
 * - when data is reserved, an info object is returned which evaluates to true if reservation was successful;
 *   this object contains a pointer to the data and a 'key' which is a value used to release data back to the pool.
 *   [NOTE: if a pool contains objects by value, info object will contain pointer to the reserved object; when pool
 *   stores pointers (e.g. void*), info object will also contain pointer (void*, not void**); you can change this
 *   behavior by].
 */

#include "ConcrtCommon.h"


namespace Exs
{


	// Represents id of an entry within a pool.
	typedef Size_t Concurrent_pool_key_t;


	enum : Concurrent_pool_key_t
	{
		Concurrent_Data_Pool_Invalid_Key = stdx::limits<Concurrent_pool_key_t>::max_value
	};


	enum class ConcurrentDataPoolFetchMode : Enum
	{
		Collapse_Ptr,
		No_Change,
	};


	template <class T, ConcurrentDataPoolFetchMode>
	struct ConcurrentDataPoolFetchWrapper
	{
		typedef T* Ref_type;

		static T* GetDataPtr(T& data)
		{
			return &data;
		}
	};

	template <class T>
	struct ConcurrentDataPoolFetchWrapper<T, ConcurrentDataPoolFetchMode::Collapse_Ptr>
	{
	};

	template <class T>
	struct ConcurrentDataPoolFetchWrapper<T*, ConcurrentDataPoolFetchMode::Collapse_Ptr>
	{
		typedef T* Ref_type;

		static T* GetDataPtr(T*& data)
		{
			return data;
		}
	};


	template <class T>
	struct ConcurrentDataPoolFetchTraits
	{
		typedef ConcurrentDataPoolFetchWrapper<T, ConcurrentDataPoolFetchMode::No_Change> Wrapper;
	};

	template <class T>
	struct ConcurrentDataPoolFetchTraits<T*>
	{
		typedef ConcurrentDataPoolFetchWrapper<T*, ConcurrentDataPoolFetchMode::Collapse_Ptr> Wrapper;
	};


	template <class Ref_t>
	struct ConcurrentDataPoolHandle
	{
	public:
		typedef Concurrent_pool_key_t KeyType;

		// Key used to identify the entry allocated from the pool.
		Concurrent_pool_key_t key;

		// Pointer to the reserved data.
		Ref_t* dataPtr;

	public:
		ConcurrentDataPoolHandle()
		: key(Concurrent_Data_Pool_Invalid_Key)
		, dataPtr(nullptr)
		{ }

		ConcurrentDataPoolHandle(Concurrent_pool_key_t key, Ref_t& data)
		: key(key)
		, dataPtr(&data)
		{ }

		ConcurrentDataPoolHandle(std::nullptr_t)
		: ConcurrentDataPoolHandle()
		{ }

		operator bool() const
		{
			return this->key != Concurrent_Data_Pool_Invalid_Key;
		}

		Ref_t& operator*() const
		{
			return *(this->dataPtr);
		}

		Ref_t* operator->() const
		{
			return this->dataPtr;
		}
	};


	template <class T, Size_t Size>
	class ConcurrentDataPool
	{
		EXS_DECLARE_NONCOPYABLE(ConcurrentDataPool);

	public:
		typedef typename ConcurrentDataPoolFetchTraits<T>::Wrapper FetchWrapper;
		typedef typename FetchWrapper::Ref_type Ref_type;
		typedef ConcurrentDataPoolHandle<Ref_type> Handle;

		static constexpr poolSize = Size;
		static constexpr cacheOptPadding = Config::HW_Destructive_Cache_Interference_Size;

	private:
		struct alignas(cacheOptPadding) Entry
		{
		private:
			typedef Uint32 Alloc_val_t;
			std::atomic<Alloc_val_t>  _allocState;

		public:
			T  data;

		public:
			Entry()
			: _allocState(0)
			{ }

			bool Acquire()
			{
				Alloc_val_t desiredAllocState = 0;
				return this->_allocState.compare_exchange_strong(desiredAllocState, 1, std::memory_order_acq_rel, std::memory_order_relaxed);
			}

			void Release()
			{
				ExsDebugAssert( this->Check() );
				this->_allocState.store(0, std::memory_order_release);
			}

			bool Check() const
			{
				return this->_allocState.load(std::memory_order_acquire) != 0;
			}
		};

		typedef std::array<Entry, Size> Entries;

	private:
		std::atomic<Size_t>  _allocCounter;
		Entries              _entries;

	public:
		ConcurrentDataPool()
		: _allocCounter(0)
		{ }

		T& operator[](Size_t index)
		{
			Entry& entry = this->_entries[index];
			ExsDebugAssert( entry.Check() );
			return entry.data;
		}

		const T& operator[](Size_t index) const
		{
			const Entry& entry = this->_entries[index];
			ExsDebugAssert( entry.Check() );
			return entry.data;
		}

		T& Fetch(Size_t index)
		{
			Entry& entry = this->_entries[index];
			ExsDebugAssert( entry.Check() );
			return entry.data;
		}

		const T& Fetch(Size_t index) const
		{
			const Entry& entry = this->_entries[index];
			ExsDebugAssert( entry.Check() );
			return entry.data;
		}

		T& Fetch(Size_t index, const UncheckedTag&)
		{
			Entry& entry = this->_entries[index];
			return entry.data;
		}

		const T& Fetch(Size_t index, const UncheckedTag&) const
		{
			const Entry& entry = this->_entries[index];
			return entry.data;
		}

		ConcurrentDataPoolHandle<T> Alloc()
		{
			for (Size_t allocCnt = this->_allocCounter.load(std::memory_order_relaxed); ; )
			{
				if (allocCnt == Size)
					return nullptr;

				if (this->_allocCounter.compare_exchange_weak(allocCnt, allocCnt + 1, std::memory_order_acq_rel, std::memory_order_relaxed))
					break;
			}

			for (Size_t entryIndex = 0 ; entryIndex < Size; ++entryIndex)
			{
				Entry& entry = this->_entries[entryIndex];

				if (entry.Acquire())
				{
					return ConcurrentDataPoolHandle<T>(entryIndex, entry.data);
				}
			}

			// Execution should never enter here!

			ExsDebugInterrupt();
			return nullptr;
		}

		void Release(Concurrent_pool_key_t key)
		{
			Entry& entry = this->_entries[key];
			ExsDebugAssert( entry.Check() );
			entry.Release();
			this->_allocCounter.fetch_sub(1, std::memory_order_release);
		}

		void Release(const ConcurrentDataPoolHandle<T>& handle)
		{
			this->Release(handle.key);
		}

		bool CheckAllocStatus(Concurrent_pool_key_t key) const
		{
			const Entry& entry = this->_entries[key];
			return entry.Check();
		}

		Size_t GetAllocCounter() const
		{
			return this->_allocCounter.load(std::memory_order_acquire);
		}

		bool HasAllocatedEntries() const
		{
			return this->_allocCounter.load(std::memory_order_acquire) > 0;
		}

		bool IsFullyReserved() const
		{
			return this->_allocCounter.load(std::memory_order_acquire) == Size;
		}
	};


	template <Size_t Size>
	class ConcurrentDataPool<void, Size>
	{
		EXS_DECLARE_NONCOPYABLE(ConcurrentDataPool);

	public:
		typedef ConcurrentDataPoolHandle<Size_t> Handle;

		enum : Size_t
		{
			poolSize = Size
		};

	private:
		struct Entry
		{
		private:
			typedef Uint32 Alloc_val_t;
			std::atomic<Alloc_val_t>  _allocState;

		public:
			Entry()
			: _allocState(0)
			{ }

			bool Acquire()
			{
				Alloc_val_t desiredAllocState = 0;
				return this->_allocState.compare_exchange_strong(desiredAllocState, 1, std::memory_order_acq_rel, std::memory_order_relaxed);
			}

			void Release()
			{
				ExsDebugAssert( this->Check() );
				this->_allocState.store(0, std::memory_order_release);
			}

			bool Check() const
			{
				return this->_allocState.load(std::memory_order_acquire) != 0;
			}
		};

		typedef std::array<Entry, Size> Entries;

	private:
		std::atomic<Size_t>  _allocCounter;
		Entries              _entries;

	public:
		ConcurrentDataPool()
		: _allocCounter(0)
		{ }

		bool operator[](Size_t index)
		{
			Entry& entry = this->_entries[index];
			return entry.Check();
		}

		bool operator[](Size_t index) const
		{
			const Entry& entry = this->_entries[index];
			return entry.Check();
		}

		ConcurrentDataPoolHandle<Size_t> Alloc()
		{
			for (Size_t allocCnt = this->_allocCounter.load(std::memory_order_relaxed); ; )
			{
				if (allocCnt == Size)
					return nullptr;

				if (this->_allocCounter.compare_exchange_weak(allocCnt, allocCnt + 1, std::memory_order_acq_rel, std::memory_order_relaxed))
					return ConcurrentDataPoolHandle<Size_t>(allocCnt, allocCnt);
			}
		}

		void Release(Concurrent_pool_key_t key)
		{
			Entry& entry = this->_entries[key];
			ExsDebugAssert( entry.Check() );
			entry.Release();
			this->_allocCounter.fetch_sub(1, std::memory_order_release);
		}

		void Release(const ConcurrentDataPoolHandle<Size_t>& handle)
		{
			this->Release(handle.key);
		}

		bool CheckAllocStatus(Concurrent_pool_key_t key) const
		{
			const Entry& entry = this->_entries[key];
			return entry.Check();
		}

		Size_t GetAllocCounter() const
		{
			return this->_allocCounter.load(std::memory_order_acquire);
		}

		bool HasAllocatedEntries() const
		{
			return this->_allocCounter.load(std::memory_order_acquire) > 0;
		}

		bool IsFullyReserved() const
		{
			return this->_allocCounter.load(std::memory_order_acquire) == Size;
		}
	};


}


#endif /* __Exs_Core_ConcurrentDataPool_H__ */
