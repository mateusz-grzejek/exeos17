
#ifndef __Exs_Core_ThreadRegistry_H__
#define __Exs_Core_ThreadRegistry_H__

#include "ConcrtCommon.h"
#include "ThreadInternal.h"
#include <stdx/assoc_array.h>


namespace Exs
{


	class ThreadRegistry;
	class ThreadSystemManager;


	///<summary>
	///</summary>
	class EXS_LIBCLASS_CORE ThreadRegistry : public Lockable<SharedMutex, LockAccess::Relaxed>
	{
		friend class ThreadSystemManager;

	public:
		// Info about single registered thread object.
		struct ThreadInfo
		{
			Thread* thread;
			ThreadUID uid;
			std::string name;
		};

		// List of threads in the registry.
		using ThreadList = std::vector<ThreadInfo>;

		// Map used for mapping ref IDs to UIDs.
		using ThreadRefIDMap = stdx::assoc_array<ThreadRefID, ThreadUID>;

	private:
		ThreadSystemManager*   _threadSystemManager; // Pointer to the control manager.
		ThreadList             _threadList; // List of registsred threads.
		ThreadRefIDMap         _refIDMap; // RefID --> UID map. Allows ref IDs to be used in external API.
		std::atomic<Size_t>    _registeredThreadsNum; // Number of registsred threads.

	public:
		ThreadRegistry(ThreadSystemManager* thrSystemManager);

		///
		ThreadUID QueryThreadUID(ThreadRefID threadRefID) const;

		///
		bool IsThreadRegistered(ThreadUID threadUID) const;

		///
		bool IsThreadRegistered(ThreadRefID threadRefID) const;

		///
		ThreadSystemManager* GetSystemManager() const;

		///
		const ThreadList& GetThreadList() const;

		///
		Size_t GetRegisteredThreadsNum() const;

	friendapi:
		//
		bool RegisterThread(Thread* thread, ThreadUID threadUID, const ThreadRegSpecification& regSpecification);

		//
		bool UnregisterThread(Thread* thread, ThreadUID threadUID);
	};


	inline ThreadSystemManager* ThreadRegistry::GetSystemManager() const
	{
		return this->_threadSystemManager;
	}

	inline const ThreadRegistry::ThreadList& ThreadRegistry::GetThreadList() const
	{
		return this->_threadList;
	}

	inline Size_t ThreadRegistry::GetRegisteredThreadsNum() const
	{
		return this->_registeredThreadsNum.load(std::memory_order_relaxed);
	}


}


#endif /* __Exs_Core_ThreadRegistry_H__ */
