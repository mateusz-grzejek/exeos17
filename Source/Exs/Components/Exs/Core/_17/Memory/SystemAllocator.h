
#ifndef __Exs_Core_SystemAllocator_H__
#define __Exs_Core_SystemAllocator_H__

#include "AllocatorBase.h"


namespace Exs
{


	class EXS_LIBCLASS_CORE SystemAllocator
	{
	public:
		static void* AllocAligned(Size_t memSize, Uint16 alignment);
		static void* Alloc(Size_t memSize);
		static void  Free(void* memPtr);
		static void* Realloc(void* memPtr, Size_t newSize);
		
		static void* AllocAligned(Size_t memSize, Uint16 alignment, const SourceLocationInfo& srcInfo);
		static void* Alloc(Size_t memSize, const SourceLocationInfo& srcInfo);
		static void  Free(void* memPtr, const SourceLocationInfo& srcInfo);
		static void* Realloc(void* memPtr, Size_t newSize, const SourceLocationInfo& srcInfo);
	};


}


#endif /* __Exs_Core_SystemAllocator_H__ */
