

#if 0

#include <Exs/Core/Memory/AllocatorLog.h>
#include <Exs/Core/Memory/Allocator.h>


namespace Exs
{


	AllocatorLog::AllocatorLog()
	{ }


	AllocatorLog::~AllocatorLog()
	{ }


	Result AllocatorLog::RegisterAllocation(Allocator* allocator,
											void* memPtr,
											Size_t memSize,
											const SourceLocationInfo& srcInfo)
	{
		AllocatorID allocatorID = allocator->GetID();
		auto allocatorInfoRef = this->_registeredAllocators.Find(allocatorID);

		if (allocatorInfoRef == this->_registeredAllocators.End())
			return ExsResult(RSC_Err_Not_Found);

		auto& allocatorInfo = allocatorInfoRef->value;
		Uint memoryAddress = reinterpret_cast<Uint>(memPtr);

		if (allocatorInfo.allocator != allocator)
			return ExsResult(RSC_Err_Invalid_State);

		MemoryEntry memoryEntry { };
		memoryEntry.allocatorID = allocatorID;
		memoryEntry.entryIndex = this->_memoryEntries.Size();
		memoryEntry.allocEventIndex = this->_memoryAllocEvents.Size();
		memoryEntry.deallocEventIndex = Invalid_Position;
		memoryEntry.memAddress = memoryAddress;
		memoryEntry.memSize = memSize;
		memoryEntry.state = MemoryStateFlags::Active;

		MemoryAllocEvent allocEvent { };
		allocEvent.memoryEntryIndex = memoryEntry.entryIndex;
		allocEvent.srcLocationInfo = srcInfo;

		this->_activeMemoryRefList[memoryAddress] = memoryEntry.entryIndex;

		this->_memoryEntries.PushBack(memoryEntry);
		this->_memoryAllocEvents.PushBack(allocEvent);
		this->_allocationsStatistics.RegisterAllocation(memSize);

		allocatorInfo.memoryAllocationsStatistics.RegisterAllocation(memSize);

		return ExsResult(RSC_Success);
	}


	Result AllocatorLog::RegisterDeallocation(Allocator* allocator, void* memPtr)
	{
		AllocatorID allocatorID = allocator->GetID();
		auto allocatorInfoRef = this->_registeredAllocators.Find(allocatorID);

		if (allocatorInfoRef == this->_registeredAllocators.End())
			return ExsResult(RSC_Err_Not_Found);

		auto& allocatorInfo = allocatorInfoRef->value;
		Uint memoryAddress = reinterpret_cast<Uint>(memPtr);

		if (allocatorInfo.allocator != allocator)
			return ExsResult(RSC_Err_Invalid_State);

		auto memoryEntryRef = this->_activeMemoryRefList.Find(memoryAddress);

		if (memoryEntryRef == this->_activeMemoryRefList.End())
			return ExsResult(RSC_Err_Not_Found);

		auto& memoryEntry = this->_memoryEntries[memoryEntryRef->value];
		memoryEntry.deallocEventIndex = this->_memoryDeallocEvents.Size();
		memoryEntry.state.Set(MemoryStateFlags::Released);

		MemoryDeallocEvent deallocEvent { };
		deallocEvent.memoryEntryIndex = memoryEntry.entryIndex;
		
		this->_memoryDeallocEvents.PushBack(deallocEvent);
		this->_allocationsStatistics.RegisterDeallocation(memoryEntry.memSize);
		this->_activeMemoryRefList.Erase(memoryEntryRef);

		allocatorInfo.memoryAllocationsStatistics.RegisterDeallocation(memoryEntry.memSize);

		return ExsResult(RSC_Success);
	}


	Result AllocatorLog::AddAllocator(Allocator* allocator, const String& name)
	{
		AllocatorID allocatorID = allocator->GetID();
		auto allocatorInfoRef = this->_registeredAllocators.Find(allocatorID);

		if (allocatorInfoRef != this->_registeredAllocators.End())
		{
			auto& allocatorInfo = allocatorInfoRef->value;
			return (allocatorInfo.allocator == allocator) ? ExsResult(RSC_Err_Already_Registered) : ExsResult(RSC_Err_Invalid_State);
		}

		AllocatorInfo allocatorInfo { };
		allocatorInfo.allocator = allocator;
		allocatorInfo.id = allocatorID;
		allocatorInfo.name = name;

		this->_registeredAllocators.Insert(allocatorID, std::move(allocatorInfo));

		return ExsResult(RSC_Success);
	}


}

#endif
