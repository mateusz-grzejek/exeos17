
#ifndef __Exs_Core_KeyboardState_H__
#define __Exs_Core_KeyboardState_H__

#include "KeyboardKeyCodes.h"


namespace Exs
{


	class KeyboardState
	{
	protected:
		enum : Size_t
		{
			Key_Codes_Num = Keyboard_Key_Codes_Num
		};

	protected:
		std::array<bool, Key_Codes_Num>    _keyStateArray;

	public:
		KeyboardState()
		{
			this->Reset();
		}

		~KeyboardState()
		{ }

		bool Set(SystemKeyCode keyCode)
		{
			bool& currentState = this->_keyStateArray[keyCode];
			bool newState = true;
			std::swap(currentState, newState);
			return currentState != newState;
		}

		bool Unset(SystemKeyCode keyCode)
		{
			bool& currentState = this->_keyStateArray[keyCode];
			bool newState = false;
			std::swap(currentState, newState);
			return currentState != newState;
		}

		bool Toggle(SystemKeyCode keyCode)
		{
			bool& currentState = this->_keyStateArray[keyCode];
			currentState = !currentState;

			return currentState;
		}

		void Reset()
		{
			this->_keyStateArray.fill(false);
		}

		bool IsSet(SystemKeyCode keyCode) const
		{
			return this->_keyStateArray[keyCode];
		}
	};


}


#endif /* __Exs_Core_KeyboardState_H__ */
