
#ifndef __Exs_Core_CommonSystemTypes_H__
#define __Exs_Core_CommonSystemTypes_H__

#include "../Prerequisites.h"


namespace Exs
{

	
#if ( EXS_TARGET_PLATFORM == EXS_TARGET_SYSTEM_WIN32 )

	typedef struct Win32SystemEnvData SystemEnvData;

	typedef struct Win32SystemEventInfo SystemEventInfo;

	typedef struct Win32SystemWindowData SystemWindowData;

#elif ( EXS_TARGET_PLATFORM == EXS_TARGET_SYSTEM_LINUX )

	typedef struct X11SystemEnvData SystemEnvData;

	typedef struct X11SystemEventInfo SystemEventInfo;

	typedef struct X11SystemWindowData SystemWindowData;

#elif ( EXS_TARGET_PLATFORM == EXS_TARGET_SYSTEM_ANDROID )
#endif


};


#if ( EXS_TARGET_PLATFORM == EXS_TARGET_SYSTEM_WIN32 )
#  include "../Specific/Win32/Win32_CommonSystemTypes.h"
#elif ( EXS_TARGET_PLATFORM == EXS_TARGET_SYSTEM_LINUX )
#  include "../Specific/Linux/Linux_CommonSystemTypes.h"
#elif ( EXS_TARGET_PLATFORM == EXS_TARGET_SYSTEM_ANDROID )
#endif


#endif /* __Exs_Core_CommonSystemTypes_H__ */
