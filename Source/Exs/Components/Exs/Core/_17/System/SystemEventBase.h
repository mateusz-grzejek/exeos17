
#ifndef __Exs_Core_SystemEventBase_H__
#define __Exs_Core_SystemEventBase_H__

#include "../Prerequisites.h"


#define EXS_EVT_SYS_CODE_NCID_RESERVED   0xA3000000
#define EXS_EVT_SYS_CODE_BASE_TYPE_MASK  0x00FF0000
#define EXS_EVT_SYS_CODE_CATEGORY_MASK   0x00FFFF00
#define EXS_EVT_SYS_CODE_INDEX_MASK      0x000000FF


#define EXS_EVT_SYS_MOUSE_BUTTON_ID_FLAG_MASK   0xFF00
#define EXS_EVT_SYS_MOUSE_BUTTON_ID_INDEX_MASK  0x00FF


#define ExsEnumDeclareSystemEventCode(eventCategory, index) \
	(EXS_RESULT_CODE_NCID_RESERVED | ((Enum)eventCategory << 8) | (Enum)index)

#define ExsEnumGetSystemEventBaseType(eventCode) \
	static_cast<SystemEventBaseType>((static_cast<Enum>(eventCode) & EXS_EVT_SYS_CODE_BASE_TYPE_MASK) >> 8)

#define ExsEnumGetSystemEventCategory(eventCode) \
	static_cast<SystemEventCategory>((static_cast<Enum>(eventCode) & EXS_EVT_SYS_CODE_CATEGORY_MASK) >> 8)

#define ExsEnumGetSystemEventIndex(eventCode) \
	static_cast<Uint8>(static_cast<Enum>(eventCode) & EXS_EVT_SYS_CODE_INDEX_MASK)

#define ExsEnumValidateSystemEventCode(eventCode) \
	EXS_NCID_GET_RESERVED(eventCode) == EXS_EVT_SYS_CODE_NCID_RESERVED


namespace Exs
{


	class SystemEvent;

	// Represents code of a system event.
	typedef Uint32 SystemKeyCode;

	// Type for handlers of system events, which is a functor object which accepts reference to the
	// event as its only parameter and returns bool. Handler for every event type has the same signature.
	typedef std::function<bool( const SystemEvent& )> SystemEventHandler;


	///<summary>
	///</summary>
	enum class SystemEventBaseType : Uint16
	{
		App = 0x0100,
		Input = 0x0200,
		Window = 0x0300
	};


	///<summary>
	///</summary>
	enum class SystemEventCategory : Uint16
	{
		App_Command = static_cast<Uint16>(SystemEventBaseType::App) + 1,

		Input_Keyboard = static_cast<Uint16>(SystemEventBaseType::Input) + 1,
		Input_Mouse,
		Input_Touch,

		Window_Command = static_cast<Uint16>(SystemEventBaseType::Window) + 1,
		Window_Notification,
	};


	///<summary>
	///</summary>
	enum class SystemEventCode : Enum
	{
		App_Command_Initialize = ExsEnumDeclareSystemEventCode( SystemEventCategory::App_Command, 0x01 ),
		App_Command_Terminate,
		App_Command_Focus_Gain,
		App_Command_Focus_Lost,

		Input_Keyboard_Key_Press = ExsEnumDeclareSystemEventCode( SystemEventCategory::Input_Keyboard, 0x11 ),
		Input_Keyboard_Key_Release,

		Input_Mouse_Button_Dblclick = ExsEnumDeclareSystemEventCode( SystemEventCategory::Input_Mouse, 0x21 ),
		Input_Mouse_Button_Press,
		Input_Mouse_Button_Release,
		Input_Mouse_Move,
		Input_Mouse_Wheel_Scroll,

		Window_Command_Close = ExsEnumDeclareSystemEventCode( SystemEventCategory::Window_Command, 0x31 ),

		Window_Notification_Move = ExsEnumDeclareSystemEventCode( SystemEventCategory::Window_Notification, 0x41 ),
		Window_Notification_Resize,
		Window_Notification_Visibility_Change,

		_Reserved
	};

	///
	constexpr Size_t System_Event_Codes_Num = 80;

	///
	static_assert(ExsEnumGetSystemEventIndex( SystemEventCode::_Reserved ) < System_Event_Codes_Num, "System_Event_Codes_Num: value too small!");


	///<summary>
	///</summary>
	class SystemEventHandlerObject
	{
	public:
		///
		virtual bool HandleEvent( const SystemEvent& event ) = 0;
	};


	///<summary>
	/// Creates SystemEventHandler from specified callable and additional arguments which are bound with this callable.
	/// First argument of the callable must be <c>const SystemEvent&</c> and its return type must be <c>bool</c>.
	///</summary>
	template <class C, class... Args>
	inline SystemEventHandler BindEventHandler( C&& callable, Args&&... optArgs )
	{
		return std::bind<bool>( std::forward<C>( callable ), std::placeholders::_1, std::forward<Args>( optArgs )... );
	}


	///<summary>
	/// Creates SystemEventHandler from given handler object. Handler object is stored using specified pointer (no copy is done)
	/// and it is the responsibility of the caller to ensure, that this object is valid as long as newly created handler is used.
	///</summary>
	inline SystemEventHandler BindEventHandlerObject( SystemEventHandlerObject* handler )
	{
		return std::bind<bool>( [handler]( const SystemEvent& event ) -> bool {
			handler->HandleEvent( event );
		}, std::placeholders::_1 );
	}


	///<summary>
	///</summary>
	inline SystemEventHandler BindEventHandlerObject( const std::shared_ptr<SystemEventHandlerObject>& handlerRefPtr )
	{
		return std::bind<bool>( [handlerRefPtr]( const SystemEvent& event ) -> bool {
			handlerRefPtr->HandleEvent( event );
		}, std::placeholders::_1 );
	}


	///<summary>
	/// Creates SystemEventHandler from new SystemEventHandlerObject object of type <c>Handler</c>. Created handler object
	/// is initialized and managed by the newly created handler functor and is not accessible to the client at all.
	///</summary>
	template <class Handler, class... Args>
	SystemEventHandler BindEventHandlerObject( Args&&... cargs )
	{
		auto handlerObject = std::make_shared<Handler>( std::forward<Args>( cargs )... );
		return BindEventHandlerObject( handlerObject );
	}


}


#endif /* __Exs_Core_SystemEventBase_H__ */
