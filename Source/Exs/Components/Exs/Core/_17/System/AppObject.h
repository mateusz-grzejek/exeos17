
#ifndef __Exs_Core_AppObject_H__
#define __Exs_Core_AppObject_H__

#include "../TraceOutput.h"
#include "AppCommon.h"
#include "AppExecutionArgs.h"


namespace Exs
{


	class AppEventLoop;
	class AppObject;
	class AppObjectInitializer;
	class SystemEventReceiver;

	ExsDeclareRefPtrClass(AppEventLoop);


	///<summary>
	///</summary>
	class EXS_LIBCLASS_CORE AppObject
	{
		friend class AppObjectInitializer;

	protected:
		CoreAppSystemStateHandle    _coreAppSystemState;
		AppEventLoopRefPtr          _eventLoop;
		SystemEventReceiver*        _activeEventReceiver;

	public:
		AppObject( CoreAppSystemStateHandle coreAppSystemState );
		virtual ~AppObject();

		///<summary>
		///</summary>
		void SetExitRequest();

		///<summary>
		///</summary>
		bool SetActiveEventReceiver(SystemEventReceiver* eventReceiver);

		///<summary>
		///</summary>
		bool RunEventLoop(stdx::mask<AppEventLoopRunFlags> flags, const AppUpdateCallback& updateCallback = nullptr);
		
		///<summary>
		///</summary>
		const CoreAppSystemStateHandle& GetAppSystemState() const;

	private:
		//!
		void BindEventLoop(const AppEventLoopRefPtr& eventLoop);
	};


	inline const CoreEngineStateRefHandle& AppObject::GetCoreEngineState() const
	{
		return this->_coreEngineState;
	}

	inline const AppGlobalStateRefHandle& AppObject::GetAppGlobalState() const
	{
		return this->_appGlobalState;
	}


	class AppObjectInitializer
	{
		friend class AppObject;

	public:
		template <class App_object_t, class Event_loop_t, class Main_thread_t, class...Args>
		static std::shared_ptr<App_object_t> CreateAppObject(const AppConfiguration& appConfiguration, Args&&... args)
		{
			// Createshared global state using specified feature configuration.
			CoreEngineStateRefHandle coreEngineState = CreateCoreEngineState(appConfiguration.coreEngineFeatureMask);
			auto mainThreadObject = CreateExternalThreadObject<Main_thread_t>(coreEngineState);

			// Create shared app state (used by many app-related objects).
			AppGlobalStateRefHandle appGlobalState = CreateAppGlobalState(coreEngineState);

			// Initialize system state data (os-specific routine).
			if (!InitializeSystemData(&(appGlobalState->systemEnvData)))
				return nullptr;

			// Create app object and its event loop.
			auto appObject = std::make_shared<App_object_t>(coreEngineState, appGlobalState, std::forward<Args>(args)...);
			auto appEventLoop = std::make_shared<Event_loop_t>(appObject.get(), coreEngineState, appGlobalState);

			// We need to cast to base type - to avoid friendship declarations in all system-level AppObject classes,
			// it is better to have one in AppObject and do a single cast here.
			auto baseAppObject = stdx::dbgsafe_shared_ptr_cast<AppObject>(appObject);

			baseAppObject->BindEventLoop(appEventLoop);
			baseAppObject->SetAppArgDefinitions(appConfiguration);

			appGlobalState->appObject = appObject.get();
			appGlobalState->mainThread = mainThreadObject;
			appGlobalState->eventDispatcher = &(appEventLoop->_eventDispatcher);

			// Enable standard trace info content. Initially, all thread-related informations are not displayed, because
			// we do not have TLS initialized for main thread. After successful initialization of mainThreadObject,
			// we can safely enable all required information.

			TraceOutputController* traceOutputController = GetTraceOutputController();
			traceOutputController->SetInfoContentDisplayState(TraceInfoContent::All, false);
			traceOutputController->SetInfoContentDisplayState(TraceInfoContent::Base_Type, true);
			traceOutputController->SetInfoContentDisplayState(TraceInfoContent::Base_Category, true);

			return appObject;
		}

	private:
		// Initializes system data, os-specific. Used by this class in CreateAppObject().
		EXS_LIBAPI_CORE static bool InitializeSystemData(SystemEnvData* systemEnvData);

		// Releases system resources, os-specific. Used by AppObject when it is destroyed.
		EXS_LIBAPI_CORE static void ReleaseSystemData(SystemEnvData* systemEnvData);
	};


	template <class App_object_t, class Event_loop_t, class Main_thread_t, class...Args>
	inline std::shared_ptr<App_object_t> CreateAppObject(const AppConfiguration& appConfiguration, Args&&... args)
	{
		return AppObjectInitializer::CreateAppObject<App_object_t, Event_loop_t, Main_thread_t>(appConfiguration, std::forward<Args>(args)...);
	}


}


#endif /* __Exs_Core_AppObject_H__ */
