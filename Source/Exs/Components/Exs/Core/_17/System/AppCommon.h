
#ifndef __Exs_Core_AppCommon_H__
#define __Exs_Core_AppCommon_H__

#include "CommonSystemTypes.h"


namespace Exs
{


	class AppEventLoop;
	class AppObject;
	class SystemEventDispatcher;

	//
	typedef std::function<bool()> AppControlCallback;

	ExsDeclareSharedHandleTypeStruct( CoreAppSystemState );


	///<summary>
	///</summary>
	enum AppExecutionStateFlags : Enum
	{
		//
		AppExecutionState_Idle = 0x4000,

		//
		AppExecutionState_Terminate_Request = 0x8000
	};


	///<summary>
	/// Flags used to control how application loop is started and executed.
	///</summary>
	enum AppEventLoopRunFlags : Enum
	{
		// Loop is started in normal (default) mode. This is the default behavior.
		AppEventLoopRun_Default = 0,
	};


	///
	using AppExecutionStateMask = StateMask<AppExecutionStateFlags, StateMaskType::Atomic>;


	///<summary>
	///</summary>
	struct CoreAppSystemState : public ComponentSubsystemSharedState
	{
	public:
		struct InitData;

	private:
		const std::unique_ptr<AppObject> _appObject;
		const std::unique_ptr<SystemEventDispatcher> _eventDispatcher;
		SystemEnvData _systemEnvData;
		AppExecutionStateMask _executionState;

	public:
		AppObject* const appObject;
		SystemEventDispatcher* const eventDispatcher;
		SystemEnvData* const systemEnvData;
		AppExecutionStateMask* const executionState;

	public:
		CoreAppSystemState( InitData && initData );
		~CoreAppSystemState();
	};


	///<summary>
	///</summary>
	class CoreAppSystemStateBuilder
	{
	public:
		///<summary>
		///</summary>
		SharedHandle<CoreAppSystemState> CreateState();

	protected:
		///
		virtual void OnInitSystemEnvData( SystemEnvData* systemEnvData );

	private:
		///
		virtual std::unique_ptr<AppObject> CreateAppObject();

		///
		virtual std::unique_ptr<SystemEventDispatcher> CreateSystemEventDispatcher();

		///
		void InitializeSystemEnvData( SystemEnvData* systemEnvData );
	};


}


#endif /* __Exs_Core_AppCommon_H__ */
