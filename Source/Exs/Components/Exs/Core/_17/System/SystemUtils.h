
#ifndef __Exs_Core_SystemUtils_H__
#define __Exs_Core_SystemUtils_H__

#include "../FilesystemBase.h"


namespace Exs
{


	namespace System
	{

		FileHandle CreateFile(const char* path, FileCreateMode createMode, Enum accessMode);
		
		FileHandle OpenFile(const char* path, FileOpenMode openMode, Enum accessMode);
		
		FileHandle CreateTemporaryFile();

		std::string GetTemporaryFilePath(const char* prefix = nullptr);

		bool CheckExistence(const char* path);
		
		void CloseFile(FileHandle handle);

		NativeDLLHandle LoadDynamicLibrary(const char* path);
		
		void* RetrieveDynamicLibrarySymbol(NativeDLLHandle handle, const char* symName);
		
		void UnloadDynamicLibrary(NativeDLLHandle handle);

	}


}


#endif /* __Exs_Core_SystemUtils_H__ */
