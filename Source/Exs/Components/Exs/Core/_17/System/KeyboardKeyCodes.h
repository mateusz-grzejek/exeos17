
#ifndef __Exs_Core_KeyboardKeyCodes_H__
#define __Exs_Core_KeyboardKeyCodes_H__

#include "SystemEventBase.h"


namespace Exs
{


	enum KeyboardKeyCodes : SystemKeyCode
	{
		KeyboardKey_Unknown = stdx::limits<SystemKeyCode>::max_value,

		KeyboardKey_Num_0 = 0,
		KeyboardKey_Num_1,
		KeyboardKey_Num_2,
		KeyboardKey_Num_3,
		KeyboardKey_Num_4,
		KeyboardKey_Num_5,
		KeyboardKey_Num_6,
		KeyboardKey_Num_7,
		KeyboardKey_Num_8,
		KeyboardKey_Num_9,

		KeyboardKey_Numpad_Add,
		KeyboardKey_Numpad_Substract,
		KeyboardKey_Numpad_Multiply,
		KeyboardKey_Numpad_Divide,
		KeyboardKey_Numpad_Decimal,

		KeyboardKey_Numpad_0,
		KeyboardKey_Numpad_1,
		KeyboardKey_Numpad_2,
		KeyboardKey_Numpad_3,
		KeyboardKey_Numpad_4,
		KeyboardKey_Numpad_5,
		KeyboardKey_Numpad_6,
		KeyboardKey_Numpad_7,
		KeyboardKey_Numpad_8,
		KeyboardKey_Numpad_9,

		KeyboardKey_Arrow_Left,
		KeyboardKey_Arrow_Up,
		KeyboardKey_Arrow_Right,
		KeyboardKey_Arrow_Down,
		KeyboardKey_Page_Up,
		KeyboardKey_Page_Down,
		
		KeyboardKey_Enter,
		KeyboardKey_Backspace,
		KeyboardKey_Tab,
		KeyboardKey_Escape,
		KeyboardKey_Space,
		KeyboardKey_End,
		KeyboardKey_Home,
		KeyboardKey_Insert,
		KeyboardKey_Delete,

		KeyboardKey_F1,
		KeyboardKey_F2,
		KeyboardKey_F3,
		KeyboardKey_F4,
		KeyboardKey_F5,
		KeyboardKey_F6,
		KeyboardKey_F7,
		KeyboardKey_F8,
		KeyboardKey_F9,
		KeyboardKey_F10,
		KeyboardKey_F11,
		KeyboardKey_F12,
		
		KeyboardKey_Control_Left,
		KeyboardKey_Control_Right,
		KeyboardKey_Shift_Left,
		KeyboardKey_Shift_Right,
		KeyboardKey_Alt_Left,
		KeyboardKey_Alt_Right,
		KeyboardKey_Caps_Lock,
		
		KeyboardKey_Char_A = 65,
		KeyboardKey_Char_B,
		KeyboardKey_Char_C,
		KeyboardKey_Char_D,
		KeyboardKey_Char_E,
		KeyboardKey_Char_F,
		KeyboardKey_Char_G,
		KeyboardKey_Char_H,
		KeyboardKey_Char_I,
		KeyboardKey_Char_J,
		KeyboardKey_Char_K,
		KeyboardKey_Char_L,
		KeyboardKey_Char_M,
		KeyboardKey_Char_N,
		KeyboardKey_Char_O,
		KeyboardKey_Char_P,
		KeyboardKey_Char_Q,
		KeyboardKey_Char_R,
		KeyboardKey_Char_S,
		KeyboardKey_Char_T,
		KeyboardKey_Char_U,
		KeyboardKey_Char_V,
		KeyboardKey_Char_W,
		KeyboardKey_Char_X,
		KeyboardKey_Char_Y,
		KeyboardKey_Char_Z,

		KeyboardKey_Unused
	};


	enum : Size_t
	{
		Keyboard_Key_Codes_Num = static_cast<Size_t>(KeyboardKey_Unused)
	};


}


#endif /* __Exs_Core_KeyboardKeyCodes_H__ */
