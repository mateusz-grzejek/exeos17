
#ifndef __Exs_Core_CoreAsyncMessageSender_H__
#define __Exs_Core_CoreAsyncMessageSender_H__

#include "CoreMessageAsync.h"


namespace Exs
{


	class CoreMessageController;
	class CoreMessageDispatcher;


	class EXS_LIBCLASS_CORE CoreMessageSender
	{
		friend class CoreMessageController;

	private:
		using AsyncMessageResponseStatePtr = std::shared_ptr<CoreAsyncMessageSharedResponseState>;

		Thread*                       _thread;
		CoreMessageController*        _controller;
		CoreMessageDispatcher*        _dispatcher;
		ThreadUID                     _threadUID;
		CoreMessageSystemSyncObject   _syncObject;
		AsyncMessageResponseStatePtr  _responseState;

	public:
		CoreMessageSender(CoreMessageController& controller, CoreMessageDispatcher& dispatcher, Thread& thread, ThreadUID threadUID);

	friendapi: // Controller
		///
		CoreAsyncMessageSendResult Send(const CoreAsyncMessageSendRequest& request, const CoreAsyncMessageHandle& messageHandle);

		///
		Result Send(const CoreMessageSendRequest& request, const CoreMessage& message);

		///
		Result Send(const CoreMessageSendRequest& request, const CoreNotification& notification);

	private:
		//
		Result _DispatchAsync(const CoreAsyncMessageSendRequest& request, const CoreAsyncMessageHandle& messageHandle);

		//
		CoreAsyncMessageSendResult _DispatchAsyncWithResponseReuqest(const CoreAsyncMessageSendRequest& request, const CoreAsyncMessageHandle& messageHandle);

		//
		template <class Message_t>
		Result _InternalDispatch(const CoreMessageSendRequest& request, const Message_t& message);
	};


}


#endif /* __Exs_Core_CoreAsyncMessageSender_H__ */
