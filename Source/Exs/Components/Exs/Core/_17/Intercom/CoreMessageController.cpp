
#include <Exs/Core/Intercom/CoreMessageController.h>
#include <Exs/Core/Intercom/CoreMessageDispatcher.h>
#include <Exs/Core/Concurrency/ThreadSystemManager.h>


namespace Exs
{


	CoreAsyncMessageSendResult CoreMessageController::SendAsyncMessage(const CoreAsyncMessageSendRequest& request, const CoreAsyncMessageHandle& messageHandle)
	{
	}


	Result CoreMessageController::SendImmediateMessage(const CoreMessageSendRequest& request, const CoreMessage& message)
	{
	}


	Result CoreMessageController::SendNotification(const CoreMessageSendRequest& request, const CoreNotification& notification)
	{
	}


	CoreAsyncMessageHandle CoreMessageController::PeekAsyncMessage()
	{
		return nullptr;
	}


	CoreAsyncMessageHandle CoreMessageController::WaitForAsyncMessage()
	{
		return nullptr;
	}


	CoreAsyncMessageHandle CoreMessageController::WaitForAsyncMessage(const Milliseconds& timeout)
	{
		return nullptr;
	}


	/*template <class Message_t>
	bool CoreMessageController::DispatchMessage(const CoreMessageDestinationInfo& destination, const Message_t& message)
	{
		auto threadUID = this->_thrSystemManager->QueryThreadUID(destination.threadRefID);

		if (threadUID == Thread_UID_Invalid)
		{
			return false;
		}

		CoreMessageDispatchInfo dispatchInfo;
		dispatchInfo.destination.threadUID = threadUID;
		dispatchInfo.destination.controllerID = destination.controllerID;
		dispatchInfo.source.controller = this;
		dispatchInfo.source.threadUID = this->_threadUID;

		auto result = this->_msgDispatcher->DispatchMessage(dispatchInfo, message);

		if (result != RSC_Success)
		{
			return false;
		}

		return true;
	}*/


}
