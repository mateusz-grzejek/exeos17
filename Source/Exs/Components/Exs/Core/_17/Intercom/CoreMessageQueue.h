
#ifndef __Exs_Core_CoreMessageQueue_H__
#define __Exs_Core_CoreMessageQueue_H__

#include "CoreMessage.h"
#include "MessageQueue.h"


namespace Exs
{


	///<summary>
	/// Queue for async messages stored in a per-thread basis. Those queues do not have additional locking mechanism,
	/// as they are used directly at the thread level after whole transceiver is locked for exclusive access.
	///</summary>
	class CoreAsyncMessageInternalQueue : public CoreAsyncMessageQueue
	{

	};


	///<summary>
	/// Queue for async messages, that may be register within message dispatcher to capture some selected types of messages.
	/// Queue is locked for unique access every time messages are being enqueued.
	///</summary>
	class CoreAsyncMessageExternalQueue : public CoreAsyncMessageQueue, public Lockable<LightMutex, LockAccess::Relaxed>
	{

	};


};


#endif /* __Exs_Core_CoreMessageQueue_H__ */
