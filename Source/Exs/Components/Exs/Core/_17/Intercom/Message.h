
#ifndef __Exs_Core_Message_H__
#define __Exs_Core_Message_H__

#include "MessageBase.h"


namespace Exs
{


	///<summary>
	///</summary>
	class alignas(sizeof(Message_code_t)) MessageBase
	{
	private:
		Message_code_t _code; // Message code.

	public:
		MessageBase()
		: _code(0)
		{ }

		MessageBase(Message_code_t code)
		: _code(code)
		{ }

		template <class T>
		T* As()
		{
			return dbgsafe_ptr_cast<T*>(this);
		}

		template <class T>
		const T* As() const
		{
			return dbgsafe_ptr_cast<T*>(this);
		}

		Message_code_t GetCode() const
		{
			return this->_code;
		}

		Message_class_t GetClass() const
		{
			return EnumDef::GetMessageClassFromCode(this->_code);
		}
	};


	///<summary>
	///</summary>
	class MessageBaseWithUID : public MessageBase
	{
	private:
		MessageUID _uid; // Unique ID.

	public:
		MessageBaseWithUID(Message_code_t code)
		: MessageBase(code)
		, _uid(Internal::MessageUIDGen::GenerateGlobalMessageUID(code))
		{ }

		MessageBaseWithUID(MessageUID uid, Message_code_t code)
		: MessageBase(code)
		, _uid(uid)
		{ }

		MessageUID GetUID() const
		{
			return this->_uid;
		}
	};


	///<summary>
	/// Base proxy type for all shared messages. Contains functionality required for messages
	/// to work properly with handles implemented as SharedMessageHandle class.
	///</summary>
	template <typename Message_t, SharedMessageType SMT = SharedMessageType::Default>
	class SharedMessageProxy : public Message_t, public Internal::SharedMessageBase<SMT>
	{
	public:
		template <typename... Args>
		SharedMessageProxy(Args&&... args)
		: Message_t(std::forward<Args>(args)...)
		{ }

		virtual ~SharedMessageProxy() = default;
	};


	template <class Message_t, class... Args>
	inline SharedMessageHandle<Message_t> CreateSharedMessage(Message_code_t messageCode, Args&&... args)
	{
		return SharedMessageCreateDispatcher<Message_t>::Create(messageCode, std::forward<Args>(args)...);
	}


	template <class Message_t, class... Args>
	inline SharedMessageHandle<Message_t> CreateSharedMessageWithUID(Message_code_t messageCode, Args&&... args)
	{
		auto messageUID = Internal::MessageUIDGen::GenerateGlobalMessageUID(messageCode);
		return SharedMessageCreateDispatcher<Message_t>::Create(messageUID, messageCode, std::forward<Args>(args)...);
	}


};


#endif /* __Exs_Core_Message_H__ */
