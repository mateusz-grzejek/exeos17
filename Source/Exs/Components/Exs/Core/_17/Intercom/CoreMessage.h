
#ifndef __Exs_Core_CoreMessage_H__
#define __Exs_Core_CoreMessage_H__

#include "CoreMessageBase.h"
#include "Message.h"


namespace Exs
{


	///<summary>
	///</summary>
	class CoreMessage : public CoreMessageBase
	{
	public:
		CoreMessage(Message_code_t code)
		: CoreMessageBase(code)
		{ }
	};


	///<summary>
	///</summary>
	class CoreNotification : public MessageBase
	{
	public:
		CoreNotification(Message_code_t code)
		: MessageBase(code)
		{ }
	};


	///<summary>
	///</summary>
	class CoreAsyncMessage : public SharedMessageProxy<CoreMessage, SharedMessageType::Ref_Counted>
	{
		friend class CoreMessageSender;

	private:
		std::shared_ptr<CoreAsyncMessageSharedResponseState>  _responseState;

	public:
		template <class... Args>
		CoreAsyncMessage(Message_code_t code, Args&&... args)
		: SharedMessageProxy(code, std::forward<Args>(args)...)
		{ }

	friendapi: // CoreMessageSender
		void SetResponseState(const std::shared_ptr<CoreAsyncMessageSharedResponseState>& responseStatePtr)
		{
			this->_responseState = responseStatePtr;
		}

		CoreAsyncMessageSharedResponseState* GetResponseState()
		{
			return this->_responseState.get();
		}

		bool IsResponseRequested() const
		{
			return this->_responseState ? true : false;
		}
	};


	template <class... Args>
	inline CoreAsyncMessageHandle CreateCoreAsyncMessage(Message_code_t messageCode, Args&&... args)
	{
		return CreateSharedMessage<CoreAsyncMessage>(messageCode, std::forward<Args>(args)...);
	}


	template <class Message_t, class... Args>
	inline CoreAsyncMessageHandleEx<Message_t> CreateCoreAsyncMessageEx(Message_code_t messageCode, Args&&... args)
	{
		return CreateSharedMessage<Message_t>(messageCode, std::forward<Args>(args)...);
	}


};


#endif /* __Exs_Core_CoreMessage_H__ */
