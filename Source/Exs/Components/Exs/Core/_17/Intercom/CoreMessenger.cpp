
#include <Exs/Core/Intercom/CoreMessenger.h>
#include <Exs/Core/Intercom/CoreMessage.h>
#include <Exs/Core/Intercom/CoreMessageController.h>
#include <Exs/Core/Intercom/CoreMessageDispatcher.h>


namespace Exs
{


	Result CoreMessenger::SendAsyncMessage(ThreadRefID threadRefID, U64ID controllerID, const CoreAsyncMessageHandle& messageHandle)
	{
		CoreAsyncMessageSendRequest sendRequest;
		sendRequest.destination.controllerID = controllerID;
		sendRequest.destination.threadRefID = threadRefID;

		auto sendResult = this->_controller->SendAsyncMessage(sendRequest, messageHandle);

		return sendResult.code;
	}


	CoreAsyncMessageSendResult CoreMessenger::SendAsyncMessage(const CoreAsyncMessageSendRequest& request, const CoreAsyncMessageHandle& messageHandle)
	{
		return this->_controller->SendAsyncMessage(request, messageHandle);
	}


	Result CoreMessenger::SendImmediateMessage(ThreadRefID threadRefID, U64ID controllerID, const CoreMessage& message)
	{
		CoreMessageSendRequest sendRequest;
		sendRequest.destination.controllerID = controllerID;
		sendRequest.destination.threadRefID = threadRefID;

		auto sendResult = this->_controller->SendImmediateMessage(sendRequest, message);

		return sendResult;
	}


	Result CoreMessenger::SendImmediateMessage(const CoreMessageSendRequest& request, const CoreMessage& message)
	{
		return this->_controller->SendImmediateMessage(request, message);
	}


	Result CoreMessenger::SendNotification(ThreadRefID threadRefID, U64ID controllerID, const CoreNotification& notification)
	{
		CoreMessageSendRequest sendRequest;
		sendRequest.destination.controllerID = controllerID;
		sendRequest.destination.threadRefID = threadRefID;

		auto sendResult = this->_controller->SendNotification(sendRequest, notification);

		return sendResult;
	}


	Result CoreMessenger::SendNotification(const CoreMessageSendRequest& request, const CoreNotification& notification)
	{
		return this->_controller->SendNotification(request, notification);
	}


	void CoreMessenger::RegisterMessageHandler(Message_code_t messageCode, const CoreMessageHandlerFunction& handlerFunction, HandlerOverwritePolicy overwritePolicy)
	{
		if (overwritePolicy == HandlerOverwritePolicy::Unspecified)
		{
			overwritePolicy = this->_handlerOverwritePolicy;
		}

		auto handlerStateRef = this->_registeredMessageHandlers.find(messageCode);

		if ((handlerStateRef != this->_registeredMessageHandlers.end()) && (overwritePolicy != HandlerOverwritePolicy::Overwrite))
		{
			return;
		}

		this->_msgDispatcher->RegisterMessageHandler(this, messageCode, handlerFunction, true);
	}


	void CoreMessenger::UnregisterMessageHandler(Message_code_t messageCode)
	{
		this->_msgDispatcher->UnregisterMessageHandler(this, messageCode);
	}


	void CoreMessenger::UnregisterAllMessageHandlers()
	{
		for (auto& entry : this->_registeredMessageHandlers)
		{
			this->_msgDispatcher->UnregisterMessageHandler(this, entry.key);
		}

		this->_registeredMessageHandlers.clear();
	}


	void CoreMessenger::RegisterNotificationHandler(Message_code_t notificationCode, const CoreNotificationHandlerFunction& handlerFunction, HandlerOverwritePolicy overwritePolicy)
	{
		if (overwritePolicy == HandlerOverwritePolicy::Unspecified)
		{
			overwritePolicy = this->_handlerOverwritePolicy;
		}

		auto handlerStateRef = this->_registeredNotificationHandlers.find(notificationCode);

		if ((handlerStateRef != this->_registeredNotificationHandlers.end()) && (overwritePolicy != HandlerOverwritePolicy::Overwrite))
		{
			return;
		}

		this->_msgDispatcher->RegisterNotificationHandler(this, notificationCode, handlerFunction, true);
	}


	void CoreMessenger::UnregisterNotificationHandler(Message_code_t notificationCode)
	{
		this->_msgDispatcher->UnregisterNotificationHandler(this, notificationCode);
	}


	void CoreMessenger::UnregisterAllNotificationHandlers()
	{
		for (auto& entry : this->_registeredNotificationHandlers)
		{
			this->_msgDispatcher->UnregisterNotificationHandler(this, entry.key);
		}

		this->_registeredNotificationHandlers.clear();
	}


}
