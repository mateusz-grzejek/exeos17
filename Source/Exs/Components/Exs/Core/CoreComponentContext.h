
#ifndef __Exs_Core_CoreComponentContext_H__
#define __Exs_Core_CoreComponentContext_H__

#include "Core.h"


namespace Exs
{


	ExsDeclareSharedStateHandle( ConcrtSharedState );


	class CoreComponentContext : public ComponentContext<void>
	{
	public:
		System::SharedSysContextHandle systemContext;

		ConcrtSharedStateHandle concrtSharedState;
	};


	CoreComponentContextHandle CreateCoreComponentContext();


}


#endif /* __Exs_Core_CoreComponentContext_H__ */
