
#ifndef __Exs_Graphics_GraphicsDriver_H__
#define __Exs_Graphics_GraphicsDriver_H__

#include "Graphics.h"


namespace Exs
{
	namespace Graphics
	{


		class GraphicsDevice;


		struct GraphicsDriverCaps
		{
			ShaderModelVersion shaderModel;

			stdx::mask<ShaderStageFlags> shaderStageSupportMask;
		};


		class EXS_LIBCLASS_GRAPHICS GraphicsDriver
		{
		public:
			using GraphicsDeviceList = std::vector<GraphicsDevice*>;

		public:
			GraphicsDriver();

			GraphicsDeviceList EnumGraphicsDevices() const;
		};


	}
}



#endif /* __Exs_Graphics_GraphicsDriver_H__ */
