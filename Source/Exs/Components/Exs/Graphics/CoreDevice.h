
#ifndef __Exs_Graphics_CoreDevice_H__
#define __Exs_Graphics_CoreDevice_H__

#include "Graphics.h"


namespace Exs
{
	namespace Graphics
	{


		struct CoreDeviceDesc
		{
		};


		// Logical (virtual) device: VkDevice, ID3D11Device, ID3D12Device+ID3D12CmdQueue
		class EXS_LIBCLASS_GRAPHICS CoreDevice
		{
		};


	}
}


#endif /* __Exs_Graphics_CoreDevice_H__ */
