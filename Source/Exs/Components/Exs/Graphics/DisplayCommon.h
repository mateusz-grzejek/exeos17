
#ifndef __Exs_Graphics_DisplayCommon_H__
#define __Exs_Graphics_DisplayCommon_H__

#include "Graphics.h"
#include <ExsLib/System/DisplayCommon.h>


namespace Exs
{


	using DisplayRect = System::DisplayRect;

	using DisplaySettings = System::DisplaySettings;

	using DisplayModeFlags = System::DisplayModeFlags;


	struct DisplayModeDesc
	{

	};


}


#endif /* __Exs_Graphics_DisplayCommon_H__ */
