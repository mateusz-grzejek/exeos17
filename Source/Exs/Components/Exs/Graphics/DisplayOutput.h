
#ifndef __Exs_Graphics_DisplayOutput_H__
#define __Exs_Graphics_DisplayOutput_H__

#include "DisplayCommon.h"


namespace Exs
{
	namespace Graphics
	{


		class GraphicsDevice;


		struct DisplayOutputDesc
		{
			std::string name;
		};


		// Logical display device, connected to a physical device. Represents virtual display.
		// DXGI: IDXGIOutput + monitor info
		// VK: VkDisplay + properties
		// Others: System::DisplayDevice
		class EXS_LIBCLASS_GRAPHICS DisplayOutput : public DriverSpecificInterface<DisplayOutput>
		{
		public:
			using DisplayModeList = std::vector<DisplayModeDesc>;

		public:
			DisplayOutput();

		protected:
			GraphicsDevice* _graphicsDevice;
			DisplayModeList _displayModeList;
		};


	}
}


#endif /* __Exs_Graphics_DisplayOutput_H__ */
