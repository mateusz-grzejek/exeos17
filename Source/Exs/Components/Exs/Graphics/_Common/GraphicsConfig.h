
#ifndef __Exs_Graphics_GraphicsConfig_H__
#define __Exs_Graphics_GraphicsConfig_H__

#if ( EXS_BUILD_MODULE_GRAPHICS )
#  define EXS_LIBAPI_GRAPHICS     EXS_MODULE_EXPORT
#  define EXS_LIBCLASS_GRAPHICS   EXS_MODULE_EXPORT
#  define EXS_LIBOBJECT_GRAPHICS  EXS_MODULE_EXPORT
#else
#  define EXS_LIBAPI_GRAPHICS     EXS_MODULE_IMPORT
#  define EXS_LIBCLASS_GRAPHICS   EXS_MODULE_IMPORT
#  define EXS_LIBOBJECT_GRAPHICS  EXS_MODULE_IMPORT
#endif


namespace Exs
{
}


#endif // __Exs_Graphics_GraphicsConfig_H__
