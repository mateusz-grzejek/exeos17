
#ifndef __Exs_Graphics_GraphicsEnums_H__
#define __Exs_Graphics_GraphicsEnums_H__


namespace Exs
{
	namespace Graphics
	{


		enum ShaderStageFlags : Enum
		{
			ShaderStage_Vertex_Bit = 0x0001,
			ShaderStage_Geometry_Bit = 0x00002,
			ShaderStage_Pixel_Bit = 0x0004,
			ShaderStage_Tess_Control_Bit = 0x0010,
			ShaderStage_Tess_Evaluation_Bit = 0x0020,
			ShaderStage_Compute_Bit = 0x1000,
		};


		enum class ShaderModelVersion : Enum
		{
		};


	}
}

#endif /* __Exs_Graphics_GraphicsEnums_H__ */
