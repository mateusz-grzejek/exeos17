
#ifndef __Exs_RenderSystem_CommonDefs_H__
#define __Exs_RenderSystem_CommonDefs_H__


#define ExsEnumDeclareGraphicDataBaseType(index, typeSize) \
	(((Uint8)(index) & 0xF) << 4) | ((Uint8)(typeSize) & 0xF)

#define ExsEnumGetGraphicDataBaseTypeIndex(baseType) \
	(((Uint8)(baseType) >> 4) & 0xF)

#define ExsEnumGetGraphicDataBaseTypeSize(baseType) \
	((Uint8)(baseType) & 0xF)


#define ExsEnumDeclareVertexAttribFormat(index, baseType, cnum, norm) \
	((((Enum)(baseType) & 0xFF) << 24) | (((Enum)(cnum) & 0xFF) << 16) | (norm ? 0xFF00 : 0) | ((Enum)(index) & 0xFF))

#define ExsEnumGetVertexAttribFormatBaseType(format) \
	(GraphicDataBaseType)(((Enum)(format) >> 24) & 0xFF)

#define ExsEnumGetVertexAttribFormatComponentsNum(format) \
	(Uint8)(((Enum)(format) >> 16) & 0xFF)

#define ExsEnumIsVertexAttribFormatNormalized(format) \
	(((Enum)(format) & 0xFF00) != 0)

#define ExsEnumGetVertexAttribFormatIndex(format) \
	(Uint8)((Enum)(format) & 0xFF)


#define ExsEnumDeclareGraphicDataFormat(index, baseType, size) \
	((((Enum)(baseType) & 0xFF) << 24) | (((Enum)(size) & 0xFF) << 16) | ((Enum)(index) & 0xFF))

#define ExsEnumGetGraphicDataFormatBaseType(format) \
	(GraphicDataBaseType)(((Enum)(format) >> 24) & 0xFF)

#define ExsEnumGetGraphicDataFormatSize(format) \
	(Uint8)(((Enum)(format) >> 16) & 0xFF)

#define ExsEnumGetGraphicDataFormatIndex(format) \
	(Uint8)((Enum)(format) & 0xFF)

#define ExsEnumGetGraphicDataFormatComponentsNum(format) \
	(ExsEnumGetGraphicDataFormatSize(format) / ExsEnumGetGraphicDataBaseTypeSize(ExsEnumGetGraphicDataFormatBaseType(format)))


namespace Exs
{

	// TMP //
	class ActiveObject;
	typedef ActiveObject RSThread;
	// TMP //

	class GraphicsDriver;
	class RenderSystem;


	// Type used to represent ID of RSBaseObject instances.
	typedef U32ID RSBaseObjectID;
	
	// Type used to represent index of RSThread instances.
	typedef Uint32 RSThreadIndex;
	
	
	template <class T>
	using RSHandle = std::shared_ptr<T>;
	
	template <class T>
	using RSInternalHandle = stdx::intrusive_ptr<T>;


	#define ExsDeclareRSClassObjHandle(type) \
		class type; typedef std::shared_ptr<type> type##RefPtr; typedef RSHandle<type> type##Handle;

	#define ExsDeclareRSStructObjHandle(type) \
		struct type; typedef std::shared_ptr<type> type##RefPtr; typedef RSHandle<type> type##Handle;
	

	ExsDeclareRSClassObjHandle(RSBaseObject);
	ExsDeclareRSClassObjHandle(RSResource);
	ExsDeclareRSClassObjHandle(RSResourceView);
	ExsDeclareRSClassObjHandle(RSStateDescriptor);
	ExsDeclareRSClassObjHandle(RSStateObject);


	enum : RSBaseObjectID
	{
		Invalid_RSObject_ID = Invalid_U32ID
	};


	enum : RSThreadIndex
	{
		RS_Thr_Invalid_Thread_Index = stdx::limits<RSThreadIndex>::max_value,
		RS_Thr_Main_Thread_Index = 0,
		RS_Thr_Worker_Thread_Index_Base = 1,
	};


	enum : TraceCategoryID
	{
		TRC_RenderSystem = 0x740001
	};

	
	EXS_TRACE_SET_CATEGORY_NAME(TRC_RenderSystem, "RenderSystem");


	namespace Config
	{
		enum : Size_t
		{
			RS_CFG_Max_MSAA_Quality = 16,

			RS_OM_Max_Render_Targets_Num = 8,

			RS_THR_Worker_Threads_Num_Limit = 4
		};
	}


	///<summary>
	///</summary>
	enum class RSThreadType : Enum
	{
		Main = 1,
		Worker,
		Unknown = 0
	};

	
	///<summary>
	/// Indentifies types of components which GraphicDataFormats are consisted of.
	///</summary>
	enum class GraphicDataBaseType : Uint8
	{
		Byte             = ExsEnumDeclareGraphicDataBaseType(0, 1),
		Ubyte            = ExsEnumDeclareGraphicDataBaseType(1, 1),
		Int16            = ExsEnumDeclareGraphicDataBaseType(2, 2),
		Uint16           = ExsEnumDeclareGraphicDataBaseType(3, 2),
		Int32            = ExsEnumDeclareGraphicDataBaseType(4, 4),
		Uint32           = ExsEnumDeclareGraphicDataBaseType(5, 4),
		Float            = ExsEnumDeclareGraphicDataBaseType(6, 4),
		Depth24Stencil8  = ExsEnumDeclareGraphicDataBaseType(7, 4),
		Depth32Stencil8  = ExsEnumDeclareGraphicDataBaseType(8, 8),
	};

	
	///<summary>
	/// Specifies internal data format of pixel-based GPU resources like textures, buffers or render targets.
	///</summary>
	enum class GraphicDataFormat : Enum
	{
		R32F                = ExsEnumDeclareGraphicDataFormat( 0, GraphicDataBaseType::Float,  4),
		R32I                = ExsEnumDeclareGraphicDataFormat( 1, GraphicDataBaseType::Int32,  4),
		R32U                = ExsEnumDeclareGraphicDataFormat( 2, GraphicDataBaseType::Uint32, 4),
		R32G32F             = ExsEnumDeclareGraphicDataFormat( 3, GraphicDataBaseType::Float,  8),
		R32G32I             = ExsEnumDeclareGraphicDataFormat( 4, GraphicDataBaseType::Int32,  8),
		R32G32U             = ExsEnumDeclareGraphicDataFormat( 5, GraphicDataBaseType::Uint32, 8),
		R32G32B32F          = ExsEnumDeclareGraphicDataFormat( 6, GraphicDataBaseType::Float,  12),
		R32G32B32I          = ExsEnumDeclareGraphicDataFormat( 7, GraphicDataBaseType::Int32,  12),
		R32G32B32U          = ExsEnumDeclareGraphicDataFormat( 8, GraphicDataBaseType::Uint32, 12),
		R32G32B32A32F       = ExsEnumDeclareGraphicDataFormat( 9, GraphicDataBaseType::Float,  16),
		R32G32B32A32I       = ExsEnumDeclareGraphicDataFormat(10, GraphicDataBaseType::Int32,  16),
		R32G32B32A32U       = ExsEnumDeclareGraphicDataFormat(11, GraphicDataBaseType::Uint32, 16),
	
		R16F                = ExsEnumDeclareGraphicDataFormat(12, GraphicDataBaseType::Float,  2),
		R16I                = ExsEnumDeclareGraphicDataFormat(13, GraphicDataBaseType::Int16,  2),
		R16INORM            = ExsEnumDeclareGraphicDataFormat(14, GraphicDataBaseType::Int16,  2),
		R16U                = ExsEnumDeclareGraphicDataFormat(15, GraphicDataBaseType::Uint16, 2),
		R16UNORM            = ExsEnumDeclareGraphicDataFormat(16, GraphicDataBaseType::Uint16, 2),
		R16G16F             = ExsEnumDeclareGraphicDataFormat(17, GraphicDataBaseType::Float,  4),
		R16G16I             = ExsEnumDeclareGraphicDataFormat(18, GraphicDataBaseType::Int16,  4),
		R16G16INORM         = ExsEnumDeclareGraphicDataFormat(19, GraphicDataBaseType::Int16,  4),
		R16G16U             = ExsEnumDeclareGraphicDataFormat(20, GraphicDataBaseType::Uint16, 4),
		R16G16UNORM         = ExsEnumDeclareGraphicDataFormat(21, GraphicDataBaseType::Uint16, 4),
		R16G16B16A16F       = ExsEnumDeclareGraphicDataFormat(22, GraphicDataBaseType::Float,  8),
		R16G16B16A16I       = ExsEnumDeclareGraphicDataFormat(23, GraphicDataBaseType::Int16,  8),
		R16G16B16A16INORM   = ExsEnumDeclareGraphicDataFormat(24, GraphicDataBaseType::Int16,  8),
		R16G16B16A16U       = ExsEnumDeclareGraphicDataFormat(25, GraphicDataBaseType::Uint16, 8),
		R16G16B16A16UNORM   = ExsEnumDeclareGraphicDataFormat(26, GraphicDataBaseType::Uint16, 8),
		
		R8I                 = ExsEnumDeclareGraphicDataFormat(27, GraphicDataBaseType::Byte,   1),
		R8INORM             = ExsEnumDeclareGraphicDataFormat(28, GraphicDataBaseType::Byte,   1),
		R8U                 = ExsEnumDeclareGraphicDataFormat(29, GraphicDataBaseType::Ubyte,  1),
		R8UNORM             = ExsEnumDeclareGraphicDataFormat(30, GraphicDataBaseType::Ubyte,  1),
		R8G8I               = ExsEnumDeclareGraphicDataFormat(31, GraphicDataBaseType::Byte,   2),
		R8G8INORM           = ExsEnumDeclareGraphicDataFormat(32, GraphicDataBaseType::Byte,   2),
		R8G8U               = ExsEnumDeclareGraphicDataFormat(33, GraphicDataBaseType::Ubyte,  2),
		R8G8UNORM           = ExsEnumDeclareGraphicDataFormat(34, GraphicDataBaseType::Ubyte,  2),
		R8G8B8A8I           = ExsEnumDeclareGraphicDataFormat(35, GraphicDataBaseType::Byte,   4),
		R8G8B8A8INORM       = ExsEnumDeclareGraphicDataFormat(36, GraphicDataBaseType::Byte,   4),
		R8G8B8A8U           = ExsEnumDeclareGraphicDataFormat(37, GraphicDataBaseType::Ubyte,  4),
		R8G8B8A8UNORM       = ExsEnumDeclareGraphicDataFormat(38, GraphicDataBaseType::Ubyte,  4),
		
		B8G8R8A8UNORM       = ExsEnumDeclareGraphicDataFormat(39, GraphicDataBaseType::Ubyte,  4),
		B8G8R8X8UNORM       = ExsEnumDeclareGraphicDataFormat(40, GraphicDataBaseType::Ubyte,  4),
		
		R8G8B8SRGB          = ExsEnumDeclareGraphicDataFormat(41, GraphicDataBaseType::Ubyte,  4),
		R8G8B8A8SRGB        = ExsEnumDeclareGraphicDataFormat(42, GraphicDataBaseType::Ubyte,  4),
		
		R5G5B5A1            = ExsEnumDeclareGraphicDataFormat(43, GraphicDataBaseType::Ubyte,  2),
		R5G6B5              = ExsEnumDeclareGraphicDataFormat(44, GraphicDataBaseType::Ubyte,  2),
		R9G9B9E5            = ExsEnumDeclareGraphicDataFormat(45, GraphicDataBaseType::Uint32, 4),
		R10G10B10A2U        = ExsEnumDeclareGraphicDataFormat(46, GraphicDataBaseType::Uint32, 4),
		R10G10B10A2UNORM    = ExsEnumDeclareGraphicDataFormat(47, GraphicDataBaseType::Uint32, 4),
		R11G11B10F          = ExsEnumDeclareGraphicDataFormat(48, GraphicDataBaseType::Float,  4),
		
		D16U                = ExsEnumDeclareGraphicDataFormat(49, GraphicDataBaseType::Uint16,           2),
		D24S8               = ExsEnumDeclareGraphicDataFormat(50, GraphicDataBaseType::Depth24Stencil8,  4),
		D32F                = ExsEnumDeclareGraphicDataFormat(51, GraphicDataBaseType::Float,            4),
		D32FS8X24           = ExsEnumDeclareGraphicDataFormat(52, GraphicDataBaseType::Depth32Stencil8,  8),
		
		A8UNORM             = ExsEnumDeclareGraphicDataFormat(53, GraphicDataBaseType::Byte, 1),

		Unspecified = 0
	};

	
	///<summary>
	///</summary>
	enum class VertexAttribFormat : Enum
	{
		Float             = ExsEnumDeclareVertexAttribFormat( 0,  GraphicDataBaseType::Float,   1, 0),
		Byte              = ExsEnumDeclareVertexAttribFormat( 1,  GraphicDataBaseType::Byte,    1, 0),
		Byte_Norm         = ExsEnumDeclareVertexAttribFormat( 2,  GraphicDataBaseType::Byte,    1, 1),
		Int16             = ExsEnumDeclareVertexAttribFormat( 3,  GraphicDataBaseType::Int16,   1, 0),
		Int16_Norm        = ExsEnumDeclareVertexAttribFormat( 4,  GraphicDataBaseType::Int16,   1, 1),
		Int32             = ExsEnumDeclareVertexAttribFormat( 5,  GraphicDataBaseType::Int32,   1, 0),
		Ubyte             = ExsEnumDeclareVertexAttribFormat( 6,  GraphicDataBaseType::Ubyte,   1, 0),
		Ubyte_Norm        = ExsEnumDeclareVertexAttribFormat( 7,  GraphicDataBaseType::Ubyte,   1, 1),
		Uint16            = ExsEnumDeclareVertexAttribFormat( 8,  GraphicDataBaseType::Uint16,  1, 0),
		Uint16_Norm       = ExsEnumDeclareVertexAttribFormat( 9,  GraphicDataBaseType::Uint16,  1, 1),
		Uint32            = ExsEnumDeclareVertexAttribFormat(10,  GraphicDataBaseType::Uint32,  1, 0),
		
		Vec2_Float        = ExsEnumDeclareVertexAttribFormat(11,  GraphicDataBaseType::Float,   2, 0),
		Vec2_Byte         = ExsEnumDeclareVertexAttribFormat(12,  GraphicDataBaseType::Byte,    2, 0),
		Vec2_Byte_Norm    = ExsEnumDeclareVertexAttribFormat(13,  GraphicDataBaseType::Byte,    2, 1),
		Vec2_Int16        = ExsEnumDeclareVertexAttribFormat(14,  GraphicDataBaseType::Int16,   2, 0),
		Vec2_Int16_Norm   = ExsEnumDeclareVertexAttribFormat(15,  GraphicDataBaseType::Int16,   2, 1),
		Vec2_Int32        = ExsEnumDeclareVertexAttribFormat(16,  GraphicDataBaseType::Int32,   2, 0),
		Vec2_Ubyte        = ExsEnumDeclareVertexAttribFormat(17,  GraphicDataBaseType::Ubyte,   2, 0),
		Vec2_Ubyte_Norm   = ExsEnumDeclareVertexAttribFormat(18,  GraphicDataBaseType::Ubyte,   2, 1),
		Vec2_Uint16       = ExsEnumDeclareVertexAttribFormat(19,  GraphicDataBaseType::Uint16,  2, 0),
		Vec2_Uint16_Norm  = ExsEnumDeclareVertexAttribFormat(20,  GraphicDataBaseType::Uint16,  2, 1),
		Vec2_Uint32       = ExsEnumDeclareVertexAttribFormat(21,  GraphicDataBaseType::Uint32,  2, 0),
		
		Vec3_Float        = ExsEnumDeclareVertexAttribFormat(22,  GraphicDataBaseType::Float,   3, 0),
		Vec3_Byte         = ExsEnumDeclareVertexAttribFormat(23,  GraphicDataBaseType::Byte,    3, 0),
		Vec3_Byte_Norm    = ExsEnumDeclareVertexAttribFormat(24,  GraphicDataBaseType::Byte,    3, 1),
		Vec3_Int16        = ExsEnumDeclareVertexAttribFormat(25,  GraphicDataBaseType::Int16,   3, 0),
		Vec3_Int16_Norm   = ExsEnumDeclareVertexAttribFormat(26,  GraphicDataBaseType::Int16,   3, 1),
		Vec3_Int32        = ExsEnumDeclareVertexAttribFormat(27,  GraphicDataBaseType::Int32,   3, 0),
		Vec3_Ubyte        = ExsEnumDeclareVertexAttribFormat(28,  GraphicDataBaseType::Ubyte,   3, 0),
		Vec3_Ubyte_Norm   = ExsEnumDeclareVertexAttribFormat(29,  GraphicDataBaseType::Ubyte,   3, 1),
		Vec3_Uint16       = ExsEnumDeclareVertexAttribFormat(30,  GraphicDataBaseType::Uint16,  3, 0),
		Vec3_Uint16_Norm  = ExsEnumDeclareVertexAttribFormat(31,  GraphicDataBaseType::Uint16,  3, 1),
		Vec3_Uint32       = ExsEnumDeclareVertexAttribFormat(32,  GraphicDataBaseType::Uint32,  3, 0),
		
		Vec4_Float        = ExsEnumDeclareVertexAttribFormat(33,  GraphicDataBaseType::Float,   4, 0),
		Vec4_Byte         = ExsEnumDeclareVertexAttribFormat(34,  GraphicDataBaseType::Byte,    4, 0),
		Vec4_Byte_Norm    = ExsEnumDeclareVertexAttribFormat(35,  GraphicDataBaseType::Byte,    4, 1),
		Vec4_Int16        = ExsEnumDeclareVertexAttribFormat(36,  GraphicDataBaseType::Int16,   4, 0),
		Vec4_Int16_Norm   = ExsEnumDeclareVertexAttribFormat(37,  GraphicDataBaseType::Int16,   4, 1),
		Vec4_Int32        = ExsEnumDeclareVertexAttribFormat(38,  GraphicDataBaseType::Int32,   4, 0),
		Vec4_Ubyte        = ExsEnumDeclareVertexAttribFormat(39,  GraphicDataBaseType::Ubyte,   4, 0),
		Vec4_Ubyte_Norm   = ExsEnumDeclareVertexAttribFormat(40,  GraphicDataBaseType::Ubyte,   4, 1),
		Vec4_Uint16       = ExsEnumDeclareVertexAttribFormat(41,  GraphicDataBaseType::Uint16,  4, 0),
		Vec4_Uint16_Norm  = ExsEnumDeclareVertexAttribFormat(42,  GraphicDataBaseType::Uint16,  4, 1),
		Vec4_Uint32       = ExsEnumDeclareVertexAttribFormat(43,  GraphicDataBaseType::Uint32,  4, 0),

		Unspecified = 0
	};

	
	///<summary>
	/// Returns size (in bytes) of the graphic data format.
	///</summary>
	inline constexpr Uint32 GetGraphicDataFormatSize(GraphicDataFormat format)
	{
		return ExsEnumGetGraphicDataFormatSize(format);
	}

	
	///<summary>
	/// Specifies format of an index buffer: either 16 or 32 bit unsigned integer values.
	///</summary>
	enum class IndexBufferDataType : Enum
	{
		// 16-bit unsigned integer value.
		Uint16 = static_cast<Enum>(GraphicDataBaseType::Uint16),
		
		// 32-bit unsigned integer value.
		Uint32 = static_cast<Enum>(GraphicDataBaseType::Uint32)
	};

	
	///<summary>
	/// Returns size (in bytes) of the index buffer data type.
	///</summary>
	inline constexpr Uint32 GetIndexBufferDataTypeSize(IndexBufferDataType dataType)
	{
		return GetGraphicDataFormatSize(static_cast<GraphicDataFormat>(dataType));
	}
	

	template <GraphicDataBaseType>
	struct GetGraphicDataBaseType;

	template <>
	struct GetGraphicDataBaseType<GraphicDataBaseType::Byte>
	{
		typedef Int8 Type;
	};

	template <>
	struct GetGraphicDataBaseType<GraphicDataBaseType::Ubyte>
	{
		typedef Uint8 Type;
	};

	template <>
	struct GetGraphicDataBaseType<GraphicDataBaseType::Int16>
	{
		typedef Int16 Type;
	};

	template <>
	struct GetGraphicDataBaseType<GraphicDataBaseType::Uint16>
	{
		typedef Uint16 Type;
	};

	template <>
	struct GetGraphicDataBaseType<GraphicDataBaseType::Int32>
	{
		typedef Int32 Type;
	};

	template <>
	struct GetGraphicDataBaseType<GraphicDataBaseType::Uint32>
	{
		typedef Uint32 Type;
	};

	template <>
	struct GetGraphicDataBaseType<GraphicDataBaseType::Float>
	{
		typedef float Type;
	};
	

	template <GraphicDataFormat Format>
	struct PixelStorage
	{
		static const Size_t arraySize = ExsEnumGetGraphicDataFormatComponentsNum(Format);

		static const GraphicDataBaseType baseTypeTag = ExsEnumGetGraphicDataFormatBaseType(Format);

		typedef typename GetGraphicDataBaseType<baseTypeTag>::Type BaseType;

		typedef BaseType Type[arraySize];

		static const Size_t storageSize = arraySize * sizeof(BaseType);
	};


}


#endif /* __Exs_RenderSystem_CommonDefs_H__ */
