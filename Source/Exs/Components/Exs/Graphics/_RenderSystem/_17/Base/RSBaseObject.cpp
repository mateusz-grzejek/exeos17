


namespace Exs
{


	RSBaseObject::RSBaseObject( RenderSystem* renderSystem,
	                            RSBaseObjectType baseObjectType,
	                            RSBaseObjectID baseObjectID,
	                            Enum initialState)
	: _renderSystem(renderSystem)
	, _baseObjectType(baseObjectType)
	, _baseObjectID(baseObjectID)
	, _state(initialState)
	{ }


	RSBaseObject::~RSBaseObject()
	{ }


	void RSBaseObject::BindInterface(const RSInternalHandle<RSBaseObjectInterface>& interface)
	{
		ExsDebugAssert( !this->_interface && interface );
		this->OnBindInterface(interface.get());
		this->_interface = interface;
	}


	void RSBaseObject::OnBindInterface(RSBaseObjectInterface* interface)
	{ }


}
