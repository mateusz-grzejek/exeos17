
#ifndef __Exs_RenderSystem_RSMiscObject_H__
#define __Exs_RenderSystem_RSMiscObject_H__


namespace Exs
{


	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM RSMiscObject : public RSBaseObject
	{
		EXS_DECLARE_NONCOPYABLE(RSMiscObject);

	private:
		RSMiscObjectType  _miscObjectType;

	public:
		///<param name="renderSystem"> Pointer to the RenderSystem instance. </param>
		///<param name="baseObjectID"> Unique ID assigned to this resource. </param>
		///<param name="initialState">  </param>
		///<param name="miscObjectType">  </param>
		RSMiscObject( RenderSystem* renderSystem,
		              RSBaseObjectID baseObjectID,
		              Enum initialState,
		              RSMiscObjectType miscObjectType);

		virtual ~RSMiscObject();
		
		///<summary>
		///</summary>
		RSMiscObjectType GetMiscObjectType() const;
	};


	inline RSMiscObjectType RSMiscObject::GetMiscObjectType() const
	{
		return this->_miscObjectType;
	}


}


#endif /* __Exs_RenderSystem_RSMiscObject_H__ */
