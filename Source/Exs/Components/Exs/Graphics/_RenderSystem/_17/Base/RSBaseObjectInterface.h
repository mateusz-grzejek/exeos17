
#ifndef __Exs_RenderSystem_RSBaseObjectInterface_H__
#define __Exs_RenderSystem_RSBaseObjectInterface_H__


namespace Exs
{


	///<summary>
	///</summary>
	class RSBaseObjectInterface : public stdx::ref_counted_base<stdx::atomic_ref_counter>
	{
		EXS_DECLARE_NONCOPYABLE(RSBaseObjectInterface);

	public:
		RSBaseObjectInterface()
		{ }

		virtual ~RSBaseObjectInterface()
		{ }
		
		///<summary>
		///</summary>
		template <class T>
		T* As()
		{
			T* otherPtr = dbgsafe_ptr_cast<T*>(this);
			return otherPtr;
		}
		
		///<summary>
		///</summary>
		template <class T>
		const T* As() const
		{
			const T* otherPtr = dbgsafe_ptr_cast<const T*>(this);
			return otherPtr;
		}
	};

	
	template <class Interface, class... Args>
	inline RSInternalHandle<Interface> CreateRSBaseObjectInterface(Args&&... args)
	{
		return stdx::make_instrusive<Interface>(std::forward<Args>(args)...);
	}


}


#endif /* _Exs_RenderSystem_RSBaseObjectInterface_H__ */
