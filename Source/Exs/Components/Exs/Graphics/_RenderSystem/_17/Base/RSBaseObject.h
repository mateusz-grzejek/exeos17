
#ifndef __Exs_RenderSystem_RSObject_H__
#define __Exs_RenderSystem_RSObject_H__


namespace Exs
{


	///<summary>
	/// Base class for objects created, used and managed by the rendering system (like resources or state objects).
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM RSBaseObject
	{
		EXS_DECLARE_NONCOPYABLE(RSBaseObject);

		friend class RSObjectManager;

	protected:
		RenderSystem*                            _renderSystem; // Pointer to the RenderSystem instance.
		RSBaseObjectType                         _baseObjectType; // Type of this object.
		RSBaseObjectID                           _baseObjectID; // Unique ID assigned to this object.
		RSInternalHandle<RSBaseObjectInterface>  _interface;
		stdx::mask<Enum>                         _state; // Current state of this object.

	public:
		///<param name="renderSystem"> Pointer to the RenderSystem instance. </param>
		///<param name="baseObjectType"> Type of this object. </param>
		///<param name="baseObjectID"> Unique ID assigned to this object. </param>
		///<param name="initialState"> Initial state that is set for this object. </param>
		RSBaseObject( RenderSystem* renderSystem,
		              RSBaseObjectType baseObjectType,
		              RSBaseObjectID baseObjectID,
		              Enum initialState);

		virtual ~RSBaseObject();
		
		///<summary>
		///</summary>
		template <class T>
		T* As();
		
		///<summary>
		///</summary>
		template <class T>
		const T* As() const;

		///<summary>
		///</summary>
		void BindInterface(const RSInternalHandle<RSBaseObjectInterface>& interface);
		
		///<summary>
		///</summary>
		RSBaseObjectInterface* GetInterface();

		///<summary>
		///</summary>
		const RSBaseObjectInterface* GetInterface() const;

		///<summary>
		/// Returns current state of this object.
		///</summary>
		stdx::mask<Enum>& GetState();

		///<summary>
		/// Returns current state of this object.
		///</summary>
		const stdx::mask<Enum>& GetState() const;

		///<summary>
		/// Returns base type of this object.
		///</summary>
		RSBaseObjectType GetBaseObjectType() const;

		///<summary>
		/// Returns unique id of this object.
		///</summary>
		RSBaseObjectID GetBaseObjectID() const;

	protected:
		//
		virtual void OnBindInterface(RSBaseObjectInterface* interface);
	};


	template <class T>
	inline T* RSBaseObject::As()
	{
		return dbgsafe_ptr_cast<T*>(this);
	}
	
	///<summary>
	///</summary>
	template <class T>
	inline const T* RSBaseObject::As() const
	{
		return dbgsafe_ptr_cast<const T*>(this);
	}
	
	///<summary>
	///</summary>
	inline RSBaseObjectInterface* RSBaseObject::GetInterface()
	{
		return this->_interface.get();
	}
	
	///<summary>
	///</summary>
	inline const RSBaseObjectInterface* RSBaseObject::GetInterface() const
	{
		return this->_interface.get();
	}
	
	///<summary>
	/// Returns current state of this object.
	///</summary>
	inline stdx::mask<Enum>& RSBaseObject::GetState()
	{
		return this->_state;
	}
	
	///<summary>
	/// Returns current state of this object.
	///</summary>
	inline const stdx::mask<Enum>& RSBaseObject::GetState() const
	{
		return this->_state;
	}
	
	///<summary>
	/// Returns base type of this object.
	///</summary>
	inline RSBaseObjectType RSBaseObject::GetBaseObjectType() const
	{
		return this->_baseObjectType;
	}
	
	///<summary>
	/// Returns unique id of this object.
	///</summary>
	inline RSBaseObjectID RSBaseObject::GetBaseObjectID() const
	{
		return this->_baseObjectID;
	}


}


#endif /* __Exs_RenderSystem_RSObject_H__ */
