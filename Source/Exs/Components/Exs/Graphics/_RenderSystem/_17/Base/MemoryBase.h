
#ifndef __Exs_RenderSystem_MemoryBase_H__
#define __Exs_RenderSystem_MemoryBase_H__


#define ExsRSCreateMemoryHandleFromRefData(memorySyatem, memoryRefData) \
	static_cast<RSMemoryHandle>(memoryRefData)

#define ExsRSGetMemoryRefDataFromHandle(memorySyatem, memoryHandle) \
	static_cast<RSMemoryRefData*>(memoryHandle)


namespace Exs
{


	struct RSMemoryRefData;
	
	class RSMemoryHeap;
	class RSMemorySystem;


	///<summary> Type of lock used by RS memory system contollers which are controlled externally. </summary>
	typedef LightMutex RSMemoryLockExternal;

	///<summary> Type of lock used by RS memory system contollers for their internal use only. </summary>
	typedef LightMutex RSMemoryLockInternal;

	///<summary>  </summary>
	typedef U32ID RSMemoryHeapID;

	///<summary>  </summary>
	typedef Uint32 RS_memory_size_t;

	///<summary> Implementation-specific, opaque type, which represents unique ID of a memory block. </summary>
	typedef RSMemoryRefData* RSMemoryHandle;

	//
	#define ExsRSMemoryHandleInvalid reinterpret_cast<RSMemoryRefData*>(stdx::limits<Ptrdiff_t>::max_value)

	//
	#define ExsRSMemoryHandleNone nullptr
	
	///<summary>
	///</summary>
	inline constexpr RSMemoryHandle RSMemoryHandleCreateFromRefData(RSMemoryRefData* refData)
	{
		return static_cast<RSMemoryHandle>(refData);
	}
	
	///<summary>
	///</summary>
	inline constexpr RSMemoryRefData* RSMemoryHandleGetRefData(RSMemoryHandle memoryHandle)
	{
		return static_cast<RSMemoryRefData*>(memoryHandle);
	}


	enum : RSMemoryHeapID
	{
		//
		RS_Invalid_Heap_ID = Invalid_U32ID,

		//
		RS_Auto_Heap_ID = Auto_U32ID
	};


	enum : RS_memory_size_t
	{
		//
		RS_Memory_Invalid_Offset = stdx::limits<RS_memory_size_t>::max_value,

		//
		RS_Memory_Invalid_Size = stdx::limits<RS_memory_size_t>::max_value
	};


	///<summary>
	/// Specifies type of access granted to CPU for a resource memory.
	///</summary>
	enum class RSMemoryAccess : Enum
	{
		None = 0,

		Read_Only = AccessMode_Read,

		Read_Write = AccessMode_Read | AccessMode_Write,

		Write_Only = AccessMode_Write
	};


	enum RSMemoryHeapPropertyFlags : Enum
	{
		RSMemoryHeapProperty_Resource_Type = 0xFF,
		RSMemoryHeapProperty_Resource_Type_All = 0x0F,
		RSMemoryHeapProperty_Resource_Type_Only_Buffers = 0x01,
		RSMemoryHeapProperty_Resource_Type_Only_Non_RTDS_Textures = 0x02,
		RSMemoryHeapProperty_Resource_Type_Only_RTDS_Textures = 0x04,

		RSMemoryHeapProperty_Usage = 0xFF00,
		RSMemoryHeapProperty_Usage_Default = 0x0100,
		RSMemoryHeapProperty_Usage_Dynamic = 0x0200,
		RSMemoryHeapProperty_Usage_Readback = 0x0400,
		
		RSMemoryHeapProperty_Ext = 0xFF0000,
		RSMemoryHeapProperty_Ext_MSAA_Compatible = 0x800000,
	};


	enum class RSMemoryAlignment : Enum
	{
		Default = 0,

		MSAA_Compatible = RSMemoryHeapProperty_Ext_MSAA_Compatible
	};


	enum class RSMemoryHeapResourceType : Enum
	{
		All = RSMemoryHeapProperty_Resource_Type_All,

		Only_Buffers = RSMemoryHeapProperty_Resource_Type_Only_Buffers,

		Only_Non_RTDS_Textures = RSMemoryHeapProperty_Resource_Type_Only_Non_RTDS_Textures,

		Only_RTDS_Textures = RSMemoryHeapProperty_Resource_Type_Only_RTDS_Textures
	};


	enum class RSMemoryHeapUsage : Enum
	{
		Default = RSMemoryHeapProperty_Usage_Default,

		Dynamic = RSMemoryHeapProperty_Usage_Dynamic,

		Readback = RSMemoryHeapProperty_Usage_Readback
	};


	///<summary>
	///</summary>
	struct RSMemoryRange
	{
	public:
		Uint    offset; // Offset to the begining of the range.
		Size_t  length; // Length of the range.

	public:
		RSMemoryRange()
		: offset(0)
		, length(Invalid_Length)
		{ }

		RSMemoryRange(Uint offset, Size_t length)
		: offset(offset)
		, length(length)
		{ }

		void Reset()
		{
			this->offset = 0;
			this->length = Invalid_Length;
		}
	};


	///<summary>
	///</summary>
	struct RSMappedMemoryInfo
	{
	public:
		stdx::mask<AccessModeFlags>  access;
		Byte*                        ptr;
		RSMemoryRange                range;

	public:
		RSMappedMemoryInfo()
		: access(AccessMode_None)
		, ptr(nullptr)
		{ }

		RSMappedMemoryInfo(stdx::mask<AccessModeFlags>, Byte* ptr, const RSMemoryRange& range)
		: access(access)
		, ptr(ptr)
		, range(range)
		{ }

		operator bool() const
		{
			return this->ptr != nullptr;
		}

		void Reset()
		{
			this->access = AccessMode_None;
			this->ptr = nullptr;
			this->range.Reset();
		}
	};


	///<summary>
	/// Represents memory allocated for a resource. Contains handle to the actual memory
	/// and access, that may be reqested at runtime by the host.
	///</summary>
	struct RSResourceMemoryInfo
	{
	public:
		RSMemoryHandle handle; // Handle to the memory.
		RSMemoryAccess access; // Allowed access which can be requested.

	public:
		RSResourceMemoryInfo()
		: handle(ExsRSMemoryHandleNone)
		, access(RSMemoryAccess::None)
		{ }

		RSResourceMemoryInfo(RSMemoryHandle handle, RSMemoryAccess access)
		: handle(handle)
		, access(access)
		{ }
	};

	
	///<summary>
	/// Trait class which defines type of pointer to the mapped memory. Type of
	/// this pointer (in terms of its const-ness) depends on the specified acces.
	///</summary>
	template <class T, RSMemoryAccess>
	struct RSMappedMemoryPtrType;

	template <class T>
	struct RSMappedMemoryPtrType<T, RSMemoryAccess::Read_Only>
	{
		typedef const T* Type;
	};

	template <class T>
	struct RSMappedMemoryPtrType<T, RSMemoryAccess::Read_Write>
	{
		typedef T* Type;
	};

	template <class T>
	struct RSMappedMemoryPtrType<T, RSMemoryAccess::Write_Only>
	{
		typedef T* Type;
	};


	template <RSMemoryAccess Access>
	struct RSMappedMemoryPtrGet
	{
		template <typename T>
		static typename RSMappedMemoryPtrType<T, Access>::Type Get(RSMemoryAccess memoryAccess, T* memoryPtr)
		{
			stdx::mask<AccessModeFlags> accessFlags = static_cast<AccessModeFlags>(memoryAccess) & AccessMode_Full;
			return accessFlags.is_set(Access) ? memoryPtr : nullptr;
		}
	};


	template <class T, RSMemoryAccess Access>
	using RSMappedMemoryPtr = typename RSMappedMemoryPtrType<T, Access>::Type;


}


#endif /* __Exs_RenderSystem_MemoryBase_H__ */
