
#ifndef __Exs_RenderSystem_CommonIADefinitions_H__
#define __Exs_RenderSystem_CommonIADefinitions_H__

#include "../Prerequisites.h"


namespace Exs
{


	ExsDeclareRSClassObjHandle(IndexBuffer);
	ExsDeclareRSClassObjHandle(VertexBuffer);


	///<summary>
	///</summary>
	enum class IAVertexAttribLocation : Uint32
	{
		Attrib_0,
		Attrib_1,
		Attrib_2,
		Attrib_3,
		Attrib_4,
		Attrib_5,
		Attrib_6,
		Attrib_7,
		Attrib_8,
		Attrib_9,
		Attrib_10,
		Attrib_11,
		Attrib_12,
		Attrib_13,
		Attrib_14,
		Attrib_15,

		// Represents an unspecified vertex attribute location.
		Unspecified = stdx::limits<Uint32>::max_value
	};


	///<summary>
	///</summary>
	enum class IAVertexStreamIndex : Uint32
	{
		Stream_0,
		Stream_1,
		Stream_2,
		Stream_3,
		Stream_4,
		Stream_5,
		Stream_6,
		Stream_7,
		Stream_8,
		Stream_9,
		Stream_10,
		Stream_11,
		Stream_12,
		Stream_13,
		Stream_14,
		Stream_15,

		// Represents an unspecified vertex stream index.
		Unspecified = stdx::limits<Uint32>::max_value
	};


	namespace Config
	{
		enum : Size_t
		{
			// Represents total number of available vertex input attributes.
			RS_IA_Max_Vertex_Input_Attribs_Num = 16,

			// Represents total number of available vertex input streams.
			RS_IA_Max_Vertex_Input_Streams_Num = 16
		};
	}


	///<summary>
	///</summary>
	enum class IAVertexAttribState : Enum
	{
		Active,

		Inactive
	};


	///<summary>
	///</summary>
	enum class IAVertexStreamState : Enum
	{
		Active,

		Inactive
	};


	///<summary>
	/// Specifies how data stream advances during rendering process.
	///</summary>
	enum class IAVertexStreamRate : Enum
	{
		// Stream specifies per-vertex data and is advanced after single vertex.
		Per_Vertex,

		// Stream specifies per-instance data and is advanced after single instance.
		Per_Instance
	};

	
	///<summary>
	/// Helper wrapper over std::array. Allows underlying array to be indexed using
	/// numbers and values of an additional type. These values are directly converted
	/// (with static_cast<>) to numerical values and used as array offsets.
	///</summary>
	template <class K, class V, Size_t Size>
	class VertexSpecArray
	{
	public:
		typedef std::array<V, Size> DataArray;
		
	public:
		DataArray data;
		
	public:
		V& operator[](Size_t index)
		{
			return this->data[index];
		}

		const V& operator[](Size_t index) const
		{
			return this->data[index];
		}

		V& operator[](K key)
		{
			Size_t index = static_cast<Size_t>(key);
			return this->data[index];
		}

		const V& operator[](K key) const
		{
			Size_t index = static_cast<Size_t>(key);
			return this->data[index];
		}
	};


}


#endif /* __Exs_RenderSystem_CommonIADefinitions_H__ */
