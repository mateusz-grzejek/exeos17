
#ifndef __Exs_RenderSystem_DepthStencilState_H__
#define __Exs_RenderSystem_DepthStencilState_H__

#include "DepthStencilConfig.h"


namespace Exs
{

	
	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM DepthStencilStateDescriptor : public RSStateDescriptor
	{
		EXS_DECLARE_NONCOPYABLE(DepthStencilStateDescriptor);

	protected:
		DepthStencilConfiguration    _configuration;

	public:
		///<param name="renderSystem"> Pointer to the RenderSystem instance. </param>
		///<param name="baseObjectID"> Unique ID assigned to this descriptor. </param>
		///<param name="descriptorPID">  </param>
		///<param name="configuration">  </param>
		DepthStencilStateDescriptor( RenderSystem* renderSystem,
		                             RSBaseObjectID baseObjectID,
		                             RSStateDescriptorPID descriptorPID,
		                             const DepthStencilConfiguration& configuration)
		: RSStateDescriptor(renderSystem, baseObjectID, 0, RSStateDescriptorType::Depth_Stencil, descriptorPID)
		, _configuration(configuration)
		{ }
		
		///<summary>
		///</summary>
		const DepthStencilConfiguration& GetConfiguration() const
		{
			return this->_configuration;
		}
		
		///<summary>
		///</summary>
		static bool ValidateConfiguration(const DepthStencilConfiguration& configuration)
		{
			return true;
		}
	};


}


#endif /* __Exs_RenderSystem_DepthStencilState_H__ */
