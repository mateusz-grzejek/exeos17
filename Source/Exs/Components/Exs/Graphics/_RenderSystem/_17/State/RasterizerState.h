
#ifndef __Exs_RenderSystem_RasterizerState_H__
#define __Exs_RenderSystem_RasterizerState_H__

#include "RasterizerConfig.h"


namespace Exs
{


	///<summary>
	///</summary>
	class RasterizerStateDescriptor : public RSStateDescriptor
	{
		EXS_DECLARE_NONCOPYABLE(RasterizerStateDescriptor);

	protected:
		RasterizerConfiguration    _configuration;

	public:
		///<param name="renderSystem"> Pointer to the RenderSystem instance. </param>
		///<param name="baseObjectID"> Unique ID assigned to this descriptor. </param>
		///<param name="descriptorPID">  </param>
		///<param name="configuration">  </param>
		RasterizerStateDescriptor( RenderSystem* renderSystem,
		                           RSBaseObjectID baseObjectID,
		                           RSStateDescriptorPID descriptorPID,
		                           const RasterizerConfiguration& configuration)
		: RSStateDescriptor(renderSystem, baseObjectID, 0, RSStateDescriptorType::Rasterizer, descriptorPID)
		, _configuration(configuration)
		{ }

		const RasterizerConfiguration& GetConfiguration() const
		{
			return this->_configuration;
		}

		static bool ValidateConfiguration(const RasterizerConfiguration& configuration)
		{
			return true;
		}
	};


}


#endif /* __Exs_RenderSystem_RasterizerState_H__ */
