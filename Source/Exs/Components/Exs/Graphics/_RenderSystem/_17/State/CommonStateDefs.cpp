
#include <ExsRenderSystem/State/BlendState.h>
#include <ExsRenderSystem/State/DepthStencilState.h>
#include <ExsRenderSystem/State/RasterizerState.h>


namespace Exs
{
	

	static const BlendConfiguration defaultBlendConfiguration =
	{
		// Blend state
		ActiveState::Disabled,

		// Blend settings
		{
			// Color blending
			{
				BlendEquation::Add,
				BlendFactor::One,
				BlendFactor::Zero
			},

			// Alpha blending
			{
				BlendEquation::Add,
				BlendFactor::One,
				BlendFactor::Zero
			},

			// Constant color
			Color(0xFFFFFFFF)
		}
	};

	static const DepthStencilConfiguration defaultDepthStencilConfiguration =
	{
		// Depth test state
		ActiveState::Enabled,

		// Depth test settings
		{
			ComparisonFunction::Less,
			DepthWriteMask::All
		},

		// Stencil test state
		ActiveState::Disabled,

		// Stencil test settings
		{
			// Back face op
			{
				ComparisonFunction::Always,
				StencilOp::Keep,
				StencilOp::Keep,
				StencilOp::Keep
			},

			// Front face op
			{
				ComparisonFunction::Always,
				StencilOp::Keep,
				StencilOp::Keep,
				StencilOp::Keep
			},

			// Ref value
			0,

			// Write mask
			0xFF
		}
	};

	static const RasterizerConfiguration defaultRasterizerConfiguration =
	{
		// Sscissor test state
		ActiveState::Disabled,

		// Rasterizer settings
		{
			CullMode::Default,
			VerticesOrder::Default,
			FillMode::Default
		}
	};


	const BlendConfiguration& RSStateConfig::GetDefaultBlendConfiguration()
	{
		return defaultBlendConfiguration;
	}


	const DepthStencilConfiguration& RSStateConfig::GetDefaultDepthStencilConfiguration()
	{
		return defaultDepthStencilConfiguration;
	}


	const RasterizerConfiguration& RSStateConfig::GetDefaultRasterizerConfiguration()
	{
		return defaultRasterizerConfiguration;
	}


}
