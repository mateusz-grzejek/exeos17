
#include <ExsRenderSystem/RenderSystem.h>
#include <ExsRenderSystem/RSContextCommandQueue.h>
#include <ExsRenderSystem/RSContextStateController.h>
#include <ExsRenderSystem/RSStateDescriptorCache.h>
#include <ExsRenderSystem/RSThreadManager.h>
#include <ExsRenderSystem/RSThreadState.h>
#include <ExsRenderSystem/Memory/RSMemorySystem.h>
#include <ExsRenderSystem/State/GraphicsPipelineStateObject.h>
#include <ExsRenderSystem/State/VertexArrayStateObject.h>


namespace Exs
{


	RenderSystem::RenderSystem(GraphicsDriver* graphicDriver, RSMemorySystem* memorySystem)
	: _graphicDriver(graphicDriver)
	, _memorySystem(memorySystem)
	, _rsObjectFactory(this)
	, _rsObjectManager(this, &_rsObjectFactory)
	, _rsStateDescriptorCache(this)
	, _rsThreadManager(new RSThreadManager(this))
	{
		ExsTraceInfo(TRC_RenderSystem, "RenderSystem: Init (%p).", this);
	}


	RenderSystem::~RenderSystem()
	{
		ExsTraceInfo(TRC_RenderSystem, "RenderSystem: Release (%p).", this);
	}


	void RenderSystem::Cleanup()
	{
		this->_rsObjectManager.Cleanup();
	}


	void RenderSystem::ReleaseObjects()
	{
		this->_rsStateDescriptorCache.Reset();
		this->_rsObjectManager.ReleaseObjects();
	}

	RSThreadState* RenderSystem::AcquireWorkerThreadState(RSThread* threadObject)
	{
		return this->_rsThreadManager->AcquireThreadState(RSThreadType::Worker, threadObject);
	}


	void RenderSystem::ReleaseWorkerThreadState(RSThreadState* threadState)
	{
		if (threadState != nullptr)
		{
			if (threadState->GetRSThreadType() == RSThreadType::Main)
			{
				ExsDebugInterrupt();
				return;
			}

			this->_rsThreadManager->ReleaseThreadState(threadState);
		}
	}


	GraphicsPipelineStateObjectHandle RenderSystem::CreateGraphicsPipelineStateObject(const GraphicsPipelineStateObjectCreateInfo& createInfo)
	{
		GraphicsPipelineStateObjectInitDesc initDesc { };
		initDesc.blendStateDescriptor = createInfo.blendStateDescriptor;
		initDesc.depthStencilStateDescriptor = createInfo.depthStencilStateDescriptor;
		initDesc.graphicsShaderStateDescriptor = createInfo.graphicsShaderStateDescriptor;
		initDesc.inputLayoutStateDescriptor = createInfo.inputLayoutStateDescriptor;
		initDesc.rasterizerStateDescriptor = createInfo.rasterizerStateDescriptor;

		if (!initDesc.blendStateDescriptor)
		{
			if (!(initDesc.blendStateDescriptor = this->CreateBlendStateDescriptor(createInfo.blendConfiguration)))
				return nullptr;
		}

		if (!initDesc.depthStencilStateDescriptor)
		{
			if (!(initDesc.depthStencilStateDescriptor = this->CreateDepthStencilStateDescriptor(createInfo.depthStencilConfiguration)))
				return nullptr;
		}

		if (!initDesc.graphicsShaderStateDescriptor)
		{
			if (!(initDesc.graphicsShaderStateDescriptor = this->CreateGraphicsShaderStateDescriptor(createInfo.graphicsShaderConfiguration)))
				return nullptr;
		}

		if (!initDesc.inputLayoutStateDescriptor)
		{
			if (!(initDesc.inputLayoutStateDescriptor = this->CreateInputLayoutStateDescriptor(createInfo.inputLayoutConfiguration, initDesc.graphicsShaderStateDescriptor)))
				return nullptr;
		}

		if (!initDesc.rasterizerStateDescriptor)
		{
			if (!(initDesc.rasterizerStateDescriptor = this->CreateRasterizerStateDescriptor(createInfo.rasterizerConfiguration)))
				return nullptr;
		}

		auto stateObject = this->NewStateObject<GraphicsPipelineStateObject>(this, initDesc);

		return stateObject;
	};


	VertexArrayStateObjectHandle RenderSystem::CreateVertexArrayStateObject(const VertexArrayStateObjectCreateInfo& createInfo)
	{
		VertexArrayStateObjectInitDesc initDesc { };
		initDesc.vertexStreamStateDescriptor = createInfo.vertexStreamStateDescriptor;

		if (!initDesc.vertexStreamStateDescriptor)
		{
			if (!(initDesc.vertexStreamStateDescriptor = this->CreateVertexStreamStateDescriptor(createInfo.vertexStreamConfiguration)))
				return nullptr;
		}

		auto stateObject = this->NewStateObject<VertexArrayStateObject>(this, initDesc);

		return stateObject;
	}


	RSThreadState* RenderSystem::AcquireMainThreadState(RSThread* threadObject)
	{
		return this->_rsThreadManager->AcquireThreadState(RSThreadType::Main, threadObject);
	}


	void RenderSystem::ReleaseMainThreadState(RSThreadState* threadState)
	{
		ExsDebugAssert( threadState != nullptr );
		ExsDebugAssert( threadState->GetRSThreadType() == RSThreadType::Main );
		this->_rsThreadManager->ReleaseThreadState(threadState);
	}


}
