
#ifndef __Exs_RenderSystem_RSObjectFactory_H__
#define __Exs_RenderSystem_RSObjectFactory_H__

#include "Prerequisites.h"


#define ExsEnumDeclareRSObjectID(objectTypeKey, objectIndex) \
	(((static_cast<Uint32>(objectTypeKey) & 0xFF) << 24) | (static_cast<Uint32>(objectIndex) & 0xFFFFFF))

#define ExsEnumGetRSObjectIDTypeKey(rsObjectID) \
	((static_cast<Uint32>(rsObjectID) & 0xFF) >> 24)


namespace Exs
{


	class RSObjectManager;
	

	///<summary>
	/// Class responsible for creation and basic initialization of all RS objects (that is, represented by RSBaseObject-based types).
	/// This class does not manage those objects - it only generates basic meta-info and ensures consistency of their default state.
	///</summary>
	class RSObjectFactory
	{
		EXS_DECLARE_NONCOPYABLE(RSObjectFactory);

		friend class RSObjectManager;

	public:
		enum : Uint32
		{
			ObjectID_Key_Misc_Object = 0xE1,
			ObjectID_Key_Resource = 0xE3,
			ObjectID_Key_Resource_View = 0xE5,
			ObjectID_Key_State_Descriptor = 0xE6,
			ObjectID_Key_State_Object = 0xE7
		};

	private:
		RenderSystem*          _renderSystem;
		std::atomic<Uint32>    _miscObjectIDCounter; // Counter of IDs generated for misc objects.
		std::atomic<Uint32>    _resourceIDCounter; // Counter of IDs generated for resources.
		std::atomic<Uint32>    _resourceViewIDCounter; // Counter of IDs generated for resource views.
		std::atomic<Uint32>    _stateDescriptorIDCounter; // Counter of IDs generated for state descriptors.
		std::atomic<Uint32>    _stateObjectIDCounter; // Counter of IDs generated for state objects.

	public:
		RSObjectFactory(RenderSystem* renderSystem)
		: _renderSystem(renderSystem)
		, _miscObjectIDCounter(0)
		, _resourceIDCounter(0)
		, _resourceViewIDCounter(0)
		, _stateDescriptorIDCounter(0)
		, _stateObjectIDCounter(0)
		{ }

		~RSObjectFactory()
		{ }

		///<summary>
		/// Creates misc object of a type specified as the first template parameter.
		///</summary>
		///<param name="renderSystem"> Pointer to the RenderSystem instance. </param>
		///<param name="args"> Any number of parameters (including zero) forwarded to constructor of the misc object. </param>
		template <class Misc_object_t, class Render_system_t, class... Args>
		std::shared_ptr<Misc_object_t> CreateMiscObject(Render_system_t* renderSystem, Args&&... args)
		{
			Uint32 miscObjectIndex = this->_miscObjectIDCounter.fetch_add(1, std::memory_order_acq_rel) + 1;
			RSBaseObjectID miscObjectID = ExsEnumDeclareRSObjectID(ObjectID_Key_Misc_Object, miscObjectIndex);
			auto miscObject = std::make_shared<Misc_object_t>(renderSystem, miscObjectID, std::forward<Args>(args)...);
			return miscObject;
		}

		///<summary>
		/// Creates resource of a type specified as the first template parameter.
		///</summary>
		///<param name="renderSystem"> Pointer to the RenderSystem instance. </param>
		///<param name="args"> Any number of parameters (including zero) forwarded to constructor of the resource. </param>
		template <class Resource_t, class Render_system_t, class... Args>
		std::shared_ptr<Resource_t> CreateResource(Render_system_t* renderSystem, Args&&... args)
		{
			Uint32 resourceIndex = this->_resourceIDCounter.fetch_add(1, std::memory_order_acq_rel) + 1;
			RSBaseObjectID resourceID = ExsEnumDeclareRSObjectID(ObjectID_Key_Resource, resourceIndex);
			auto resource = std::make_shared<Resource_t>(renderSystem, resourceID, std::forward<Args>(args)...);
			return resource;
		}

		///<summary>
		/// Creates resource view of a type specified as the first template parameter.
		///</summary>
		///<param name="renderSystem"> Pointer to the RenderSystem instance. </param>
		///<param name="args"> Any number of parameters (including zero) forwarded to constructor of the resource view. </param>
		template <class Resource_view_t, class Render_system_t, class... Args>
		std::shared_ptr<Resource_view_t> CreateResourceView(Render_system_t* renderSystem, Args&&... args)
		{
			Uint32 resourceViewIndex = this->_resourceViewIDCounter.fetch_add(1, std::memory_order_acq_rel) + 1;
			RSBaseObjectID resourceViewID = ExsEnumDeclareRSObjectID(ObjectID_Key_Resource_View, resourceViewIndex);
			auto resourceView = std::make_shared<Resource_view_t>(renderSystem, resourceViewID, std::forward<Args>(args)...);
			return resourceView;
		}

		///<summary>
		/// Creates state descriptor of a type specified as the first template parameter.
		///</summary>
		///<param name="renderSystem"> Pointer to the RenderSystem instance. </param>
		///<param name="args"> Any number of parameters (including zero) forwarded to constructor of the state descriptor. </param>
		template <class State_descriptor_t, class Render_system_t, class... Args>
		std::shared_ptr<State_descriptor_t> CreateStateDescriptor(Render_system_t* renderSystem, Args&&... args)
		{
			Uint32 stateDescriptorIndex = this->_stateDescriptorIDCounter.fetch_add(1, std::memory_order_acq_rel) + 1;
			RSBaseObjectID stateDescriptorID = ExsEnumDeclareRSObjectID(ObjectID_Key_State_Descriptor, stateDescriptorIndex);
			auto stateDescriptor = std::make_shared<State_descriptor_t>(renderSystem, stateDescriptorID, std::forward<Args>(args)...);
			return stateDescriptor;
		}

		///<summary>
		/// Creates state object of a type specified as the first template parameter.
		///</summary>
		///<param name="renderSystem"> Pointer to the RenderSystem instance. </param>
		///<param name="args"> Any number of parameters (including zero) forwarded to constructor of the state object. </param>
		template <class State_object_t, class Render_system_t, class... Args>
		std::shared_ptr<State_object_t> CreateStateObject(Render_system_t* renderSystem, Args&&... args)
		{
			Uint32 stateObjectIndex = this->_stateObjectIDCounter.fetch_add(1, std::memory_order_acq_rel) + 1;
			RSBaseObjectID stateObjectID = ExsEnumDeclareRSObjectID(ObjectID_Key_State_Object, stateObjectIndex);
			auto stateObject = std::make_shared<State_object_t>(renderSystem, stateObjectID, std::forward<Args>(args)...);
			return stateObject;
		}
	};


}


#endif /* __Exs_RenderSystem_RSObjectFactory_H__ */
