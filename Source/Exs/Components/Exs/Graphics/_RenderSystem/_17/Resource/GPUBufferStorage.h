
#ifndef __Exs_RenderSystem_GPUBufferStorage_H__
#define __Exs_RenderSystem_GPUBufferStorage_H__

#include "GPUBufferBase.h"


namespace Exs
{


	///<summary>
	///</summary>
	struct GPUBufferStorageInitDesc : public RSResourceStorageInitDesc
	{
		GPUBufferType bufferType;

		Size_t bufferSize;
	};


	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM GPUBufferStorage : public RSResourceStorage
	{
		EXS_DECLARE_NONCOPYABLE(GPUBufferStorage);

		friend class GPUBuffer;

	protected:
		Size_t  _size; // Size of the storage, in bytes.

	public:
		GPUBufferStorage(const RSResourceMemoryInfo& memoryInfo, Size_t size);
		virtual ~GPUBufferStorage();
		
		///<summary>
		///</summary>
		virtual void CopySubdata(Uint offset, Size_t length, const void* data, Size_t dataSize) = 0;

		///<summary>
		///</summary>
		virtual void FlushMappedMemory() = 0;

		///<summary>
		///</summary>
		virtual void FlushMappedRange(const RSMemoryRange& range) = 0;

		///<summary>
		/// Returns size of the buffer, in bytes.
		///</summary>
		Size_t GetSize() const;
	};
	

	inline Size_t GPUBufferStorage::GetSize() const
	{
		return this->_size;
	}


}


#endif /* __Exs_RenderSystem_GPUBufferStorage_H__ */
