
#ifndef __Exs_RenderSystem_ConstantBuffer_H__
#define __Exs_RenderSystem_ConstantBuffer_H__

#include "GPUBuffer.h"
#include "ShaderBase.h"
#include <stdx/memory_buffer.h>


namespace Exs
{


	ExsDeclareRSClassObjHandle(ConstantBuffer);


	struct ConstantBufferBinding
	{
		//
		Uint32 cbufferIndex;

		//
		ConstantBufferHandle constantBuffer;

		//
		stdx::mask<ShaderStageFlags> stageAccess = ShaderStage_None;
	};


	enum class ConstantBufferUpdateMode : Enum
	{
		Default = 0,

		Invalidate_All = static_cast<Enum>(RSResourceMapMode::Write_Invalidate_All),

		Invalidate_Range = static_cast<Enum>(RSResourceMapMode::Write_Invalidate_Range),

		Preserve_Content = static_cast<Enum>(RSResourceMapMode::Write)
	};


	class EXS_LIBCLASS_RENDERSYSTEM ConstantBuffer : public GPUBuffer
	{
		EXS_DECLARE_NONCOPYABLE(ConstantBuffer);

	private:
		stdx::dynamic_memory_buffer<Byte>  _localMemoryBuffer;
		Byte*                              _localMemoryPtr;

	public:
		ConstantBuffer(RenderSystem* renderSystem, RSBaseObjectID baseObjectID);
		virtual ~ConstantBuffer();
		
		bool BeginUpdate(ConstantBufferUpdateMode updateMode);
		bool BeginUpdate(const RSMemoryRange& range, ConstantBufferUpdateMode updateMode);
		void EndUpdate();
		
		void Flush();
		void FlushRange(const RSMemoryRange& range);

		template <class Data>
		void Update(Uint baseOffset, const Data& data);

		template <class Data, class Member, class Value>
		void Update(Uint baseOffset, Member Data::* mptr, const Value& value);

		template <class Data>
		Data& GetData(Uint baseOffset);

		template <class Data>
		const Data& GetData(Uint baseOffset, const ConstQualifiedCallTag&);

		template <class Data>
		const Data& GetData(Uint baseOffset) const;

		template <class Data, class Member>
		Member& GetSubdata(Uint baseOffset, Member Data::* mptr);

		template <class Data, class Member>
		const Member& GetSubdata(Uint baseOffset, Member Data::* mptr, const ConstQualifiedCallTag&);

		template <class Data, class Member>
		const Member& GetSubdata(Uint baseOffset, Member Data::* mptr) const;

	private:
		virtual void OnBindStorage(RSResourceStorage* storage) override;
	};


	template <class Data>
	inline void ConstantBuffer::Update(Uint baseOffset, const Data& data)
	{
		ExsDebugAssert( baseOffset + sizeof(Data) <= this->_size );
		Data* dataPtr = reinterpret_cast<Data*>(this->_localMemoryPtr + baseOffset);
		*dataPtr = data;
	}

	template <class Data, class Member, class Value>
	inline void ConstantBuffer::Update(Uint baseOffset, Member Data::* mptr, const Value& value)
	{
		ExsDebugAssert( baseOffset + sizeof(Data) <= this->_size );
		Data* dataPtr = reinterpret_cast<Data*>(this->_localMemoryPtr + baseOffset);
		dataPtr->*mptr = value;
	}

	template <class Data>
	inline Data& ConstantBuffer::GetData(Uint baseOffset)
	{
		ExsDebugAssert( baseOffset + sizeof(Data) <= this->_size );
		Data* dataPtr = reinterpret_cast<Data*>(this->_localMemoryPtr + baseOffset);
		return *dataPtr;
	}

	template <class Data>
	inline const Data& ConstantBuffer::GetData(Uint baseOffset, const ConstQualifiedCallTag&)
	{
		ExsDebugAssert( baseOffset + sizeof(Data) <= this->_size );
		const Data* dataPtr = reinterpret_cast<const Data*>(this->_localMemoryPtr + baseOffset);
		return *dataPtr;
	}

	template <class Data>
	inline const Data& ConstantBuffer::GetData(Uint baseOffset) const
	{
		ExsDebugAssert( baseOffset + sizeof(Data) <= this->_size );
		const Data* dataPtr = reinterpret_cast<const Data*>(this->_localMemoryPtr + baseOffset);
		return *dataPtr;
	}

	template <class Data, class Member>
	inline Member& ConstantBuffer::GetSubdata(Uint baseOffset, Member Data::* mptr)
	{
		ExsDebugAssert( baseOffset + sizeof(Data) <= this->_size );
		Data* dataPtr = reinterpret_cast<Data*>(this->_localMemoryPtr + baseOffset);
		Member& memberRef = dataPtr->*mptr;
		return memberRef;
	}

	template <class Data, class Member>
	inline const Member& ConstantBuffer::GetSubdata(Uint baseOffset, Member Data::* mptr, const ConstQualifiedCallTag&)
	{
		ExsDebugAssert( baseOffset + sizeof(Data) <= this->_size );
		const Data* dataPtr = reinterpret_cast<const Data*>(this->_localMemoryPtr + baseOffset);
		const Member& memberRef = dataPtr->*mptr;
		return memberRef;
	}

	template <class Data, class Member>
	inline const Member& ConstantBuffer::GetSubdata(Uint baseOffset, Member Data::* mptr) const
	{
		ExsDebugAssert( baseOffset + sizeof(Data) <= this->_size );
		const Data* dataPtr = reinterpret_cast<const Data*>(this->_localMemoryPtr + baseOffset);
		const Member& memberRef = dataPtr->*mptr;
		return memberRef;
	}


}


#endif /* __Exs_RenderSystem_ConstantBuffer_H__ */
