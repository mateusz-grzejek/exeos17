
#include <ExsRenderSystem/Resource/IndexBuffer.h>
#include <ExsRenderSystem/Resource/GPUBufferStorage.h>


namespace Exs
{


	IndexBuffer::IndexBuffer(RenderSystem* renderSystem, RSBaseObjectID baseObjectID)
	: GeometryBuffer(renderSystem, baseObjectID, GeometryBufferType::Index_Buffer)
	{ }


	IndexBuffer::~IndexBuffer()
	{ }


}
