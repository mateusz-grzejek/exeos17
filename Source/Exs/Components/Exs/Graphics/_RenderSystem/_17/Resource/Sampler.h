
#ifndef __Exs_RenderSystem_Sampler_H__
#define __Exs_RenderSystem_Sampler_H__

#include "SamplerBase.h"


namespace Exs
{


	struct SamplerCreateInfo
	{
		SamplerParameters parameters;
	};


	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM Sampler : public RSMiscObject
	{
	protected:
		SamplerParameters  _parameters;

	public:
		Sampler(RenderSystem* renderSystem, RSBaseObjectID baseObjectID, const SamplerParameters& parameters);
		virtual ~Sampler();

		const SamplerParameters& GetParameters() const;
	};


	inline const SamplerParameters& Sampler::GetParameters() const
	{
		return this->_parameters;
	}


}


#endif /* __Exs_RenderSystem_Sampler_H__ */
