
#ifndef __Exs_RenderSystem_ShaderBase_H__
#define __Exs_RenderSystem_ShaderBase_H__

#include "../Prerequisites.h"


namespace Exs
{


	///<summary>
	///</summary>
	enum class ShaderType : Enum
	{
		// Vertex shader
		Vertex,

		// Tesselation control shader
		Tesselation_Control,

		// Tesselation evaluation shader
		Tesselation_Evaluation,

		// Geometry shader
		Geometry,

		// Pixel shader
		Pixel,

		// Compute shader
		Compute
	};


	namespace Config
	{
		enum : Size_t
		{
			// Number of shader stages in graphics pipeline.
			RS_CFG_Shader_Stages_Num_Graphics = 5,

			// Number of shader stages in compute pipeline.
			RS_CFG_Shader_Stages_Num_Compute = 1
		};
	}


	enum ShaderCreateFlags : Enum
	{
		//
		ShaderCreate_Optimize = 0x0001,

		//
		ShaderCreate_Preserve_Bytecode = 0x0002,

		//
		ShaderCreate_Default = ShaderCreate_Optimize
	};


	struct ShaderCreateInfo
	{
		//
		ShaderType shaderType;

		//
		const void* shaderCode = nullptr;

		//
		Size_t shaderCodeSize = 0;

		//
		stdx::mask<ShaderCreateFlags> createFlags = ShaderCreate_Default;
	};


}


#endif /* __Exs_RenderSystem_ShaderBase_H__ */

