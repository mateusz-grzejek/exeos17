
#ifndef __Exs_RenderSystem_TextureStorage_H__
#define __Exs_RenderSystem_TextureStorage_H__

#include "TextureBase.h"


namespace Exs
{


	enum : Enum
	{
		RSC_RS_Err_Texture_Type_Mismatch,
		RSC_RS_Err_Invalid_Texture_Dimensions,
		RSC_RS_Err_Texture_Data_Missing
	};


	///<summary>
	///</summary>
	struct TextureStorageInitDesc : public RSResourceStorageInitDesc
	{
		TextureType textureType;
		
		GraphicDataFormat textureFormat;

		TextureDimensions textureDimensions;

		Uint32 mipmapLevelsNum;

		Uint32 msaaSamplesNum;
	};

	
	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM TextureStorage : public RSResourceStorage
	{
		EXS_DECLARE_NONCOPYABLE(TextureStorage);

	protected:
		TextureDimensions  _texDimensions;
		GraphicDataFormat  _texFormat;

	public:
		TextureStorage( const RSResourceMemoryInfo& memoryInfo,
			const TextureDimensions& dimensions,
			GraphicDataFormat textureFormat);

		virtual ~TextureStorage();
		
		///<summary>
		/// Updates data of a 2D texture within specified range. Range must specify a valid subimage of the texture.
		/// If range.offset + range.size specifies an area outside the texture boundaries, RSC_RS_Err_Invalid_Texture_Dimensions
		/// error is returned. If storage is not a 2D texture storage, RSC_RS_Err_Texture_Type_Mismatch is returned.
		///</summary>
		virtual Result UpdateTexture2D(const TextureRange& range, const TextureData2DDesc& dataDesc) = 0;
		
		///<summary>
		///</summary>
		virtual Result UpdateTexture3D(const TextureRange& range, const TextureData3DDesc& dataDesc) = 0;
		
		///<summary>
		///</summary>
		virtual Result UpdateTexture2DArray(const TextureRange& range, const TextureData2DArrayDesc& dataDesc) = 0;
		
		///<summary>
		/// Updates data of a single subtexture in the texture array. arrayIndex is the index of that texture.
		///</summary>
		virtual Result UpdateTexture2DArraySubtexture(Uint32 arrayIndex, const TextureRange& range, const TextureData2DDesc& dataDesc) = 0;
		
		///<summary>
		///</summary>
		virtual Result UpdateTexture3DLayer(Uint32 layerIndex, const TextureRange& range, const TextureData2DDesc& dataDesc) = 0;
		
		///<summary>
		///</summary>
		virtual Result UpdateTextureCubeMapFace(CubeMapFace face, const TextureRange& range, const TextureData2DDesc& dataDesc) = 0;
		
		///<summary>
		///</summary>
		const TextureDimensions& GetTextureDimensions() const;
	};


	inline const TextureDimensions& TextureStorage::GetTextureDimensions() const
	{
		return this->_texDimensions;
	}


}


#endif /* __Exs_RenderSystem_TextureStorage_H__ */
