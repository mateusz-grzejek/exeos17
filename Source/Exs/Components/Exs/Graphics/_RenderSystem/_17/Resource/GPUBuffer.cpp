
#include <ExsRenderSystem/Resource/GPUBuffer.h>
#include <ExsRenderSystem/Resource/GPUBufferStorage.h>


namespace Exs
{


	GPUBuffer::GPUBuffer(RenderSystem* renderSystem, RSBaseObjectID baseObjectID, GPUBufferType bufferType)
	: RSResource(renderSystem, baseObjectID, 0, RSResourceType::Buffer, 0)
	, _bufferType(bufferType)
	, _size(0)
	, _bufferStorage(nullptr)
	{ }


	GPUBuffer::~GPUBuffer()
	{ }


	void GPUBuffer::OnBindStorage(RSResourceStorage* storage)
	{
		RSResource::OnBindStorage(storage);
		this->_bufferStorage = dbgsafe_ptr_cast<GPUBufferStorage*>(storage);
		this->_size = this->_bufferStorage->GetSize();
	}


}
