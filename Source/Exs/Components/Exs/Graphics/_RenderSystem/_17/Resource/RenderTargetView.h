
#ifndef __Exs_RenderSystem_RenderTargetView_H__
#define __Exs_RenderSystem_RenderTargetView_H__

#include "../Prerequisites.h"


namespace Exs
{


	enum class RenderTargetViewResType : Enum
	{
		// View represents 2D texture.
		Texture_2D,
		
		// View represents 2D texture which is a subtexture of a texture array.
		Texture_2D_Array_Subtexture,
		
		// View represents multisampled 2D texture.
		Texture_2D_Multisample,
		
		// View represents multisampled 2D texture which is a subtexture of a texture array.
		Texture_2D_Multisample_Array_Subtexture,
		
		// View represents 2D texture which is a single face of a cube map.
		Texture_Cube_Map_Face,
		
		// View represents 2D texture which is a single depth layer of a 3D texture.
		Texture_3D_Layer,
		
		
		// View represents an array of 2D textures.
		Texture_2D_Array,
		
		// View represents an array of 2D multisampled textures.
		Texture_2D_Multisample_Array,
		
		// View represents a 3D texture.
		Texture_3D,
		
		// View represents a cube map texture.
		Texture_Cube_Map,
		
		// View represents an array of cube map textures.
		Texture_Cube_Map_Array
	};


	class RenderTargetView : public RSResourceView
	{
		EXS_DECLARE_NONCOPYABLE(RenderTargetView);

	private:

	private:
		RenderTargetViewResType  _resType;

	public:
		RenderTargetView(RenderSystem* renderSystem, RSBaseObjectID baseObjectID, RenderTargetViewResType resType, const RSResourceHandle& resource)
		: RSResourceView(renderSystem, baseObjectID, 0, RSResourceViewType::Render_Target, resource)
		, _resType(resType)
		{ }

		RenderTargetViewResType GetResourceType() const
		{
			return this->_resType;
		}
	};


}


#endif /* __Exs_RenderSystem_RenderTargetView_H__ */
