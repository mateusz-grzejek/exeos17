
#include <ExsRenderSystem/Resource/ConstantBuffer.h>
#include <ExsRenderSystem/Resource/GPUBufferStorage.h>


namespace Exs
{


	ConstantBuffer::ConstantBuffer(RenderSystem* renderSystem, RSBaseObjectID baseObjectID)
	: GPUBuffer(renderSystem, baseObjectID, GPUBufferType::Constant_Buffer)
	, _localMemoryPtr(nullptr)
	{ }


	ConstantBuffer::~ConstantBuffer()
	{ }
	

	bool ConstantBuffer::BeginUpdate(ConstantBufferUpdateMode updateMode)
	{
		if (this->_bufferStorage->IsMapped())
		{
			ExsRuntimeInterrupt();
			return false;
		}

		if (updateMode == ConstantBufferUpdateMode::Default)
			updateMode = ConstantBufferUpdateMode::Invalidate_All;

		if (updateMode == ConstantBufferUpdateMode::Invalidate_Range)
			updateMode = ConstantBufferUpdateMode::Invalidate_All;

		RSResourceMapMode mapMode = static_cast<RSResourceMapMode>(updateMode);
		bool mapResult = this->_storage->MapMemory(mapMode, &(this->_mappedMemoryInfo));
		return mapResult;
	}


	bool ConstantBuffer::BeginUpdate(const RSMemoryRange& range, ConstantBufferUpdateMode updateMode)
	{
		if (this->_bufferStorage->IsMapped())
		{
			ExsRuntimeInterrupt();
			return false;
		}

		if (updateMode == ConstantBufferUpdateMode::Default)
			updateMode = ConstantBufferUpdateMode::Invalidate_Range;

		RSResourceMapMode mapMode = static_cast<RSResourceMapMode>(updateMode);
		bool mapResult = this->_storage->MapMemory(mapMode, &(this->_mappedMemoryInfo));
		return mapResult;
	}
	

	void ConstantBuffer::EndUpdate()
	{
		if (this->_bufferStorage->IsMapped())
		{
			ExsRuntimeInterrupt();
			return;
		}

		this->_storage->UnmapMemory();
		this->_mappedMemoryInfo.Reset();
	}

	
	void ConstantBuffer::Flush()
	{
		if (this->_bufferStorage->IsMapped())
		{
			ExsRuntimeAssert( this->_mappedMemoryInfo.access.is_set(AccessMode_Write) );

			const auto& mappedRange = this->_mappedMemoryInfo.range;
			ExsCopyMemory(this->_mappedMemoryInfo.ptr, this->_localMemoryPtr + mappedRange.offset, mappedRange.length);
			this->_bufferStorage->FlushMappedRange(this->_mappedMemoryInfo.range);
		}
		else
		{
			this->_bufferStorage->CopySubdata(0, this->_size, this->_localMemoryPtr, this->_size);
		}
	}


	void ConstantBuffer::FlushRange(const RSMemoryRange& range)
	{
		if (range.length == 0)
			return;
		
		if (this->_bufferStorage->IsMapped())
		{
			ExsRuntimeAssert( this->_mappedMemoryInfo.access.is_set(AccessMode_Write) );

			const auto& mappedRange = this->_mappedMemoryInfo.range;
			ExsDebugAssert( range.offset >= mappedRange.offset );
			ExsDebugAssert( range.offset + range.length <= mappedRange.offset + mappedRange.offset );

			// Note, that range.offset is the offset from the beginning of the buffer, while mapped ptr always
			// points to the beginning of the mapped range. Because of that, we must calculate proper ptr offset.
			Size_t mappedPtrOffset = range.offset - mappedRange.offset;

			ExsCopyMemory(this->_mappedMemoryInfo.ptr + mappedPtrOffset, this->_localMemoryPtr + range.offset, range.length);
			this->_bufferStorage->FlushMappedRange(this->_mappedMemoryInfo.range);
		}
		else
		{
			this->_bufferStorage->CopySubdata(range.offset, range.length, this->_localMemoryPtr + range.offset, range.length);
		}
	}


	void ConstantBuffer::OnBindStorage(RSResourceStorage* storage)
	{
		GPUBuffer::OnBindStorage(storage);
		this->_localMemoryBuffer.resize(this->_size);
		this->_localMemoryPtr = this->_localMemoryBuffer.data_ptr();
	}


}
