
#ifndef __Exs_RenderSystem_RSMemoryCommon_H__
#define __Exs_RenderSystem_RSMemoryCommon_H__

#include "../Resource/GPUBufferBase.h"
#include "../Resource/TextureBase.h"


namespace Exs
{


	enum class RSMemoryAllocationTarget : Enum
	{
		Buffer,

		Texture
	};


	///<summary>
	/// Represents block of memory allocated from a pool for direct use with specified resource type. Ref data objects
	/// are persistent and can be used directly to access content of referenced memory block.
	///</summary>
	struct RSMemoryRefData
	{
		// Pool, from which this memory has been allocated.
		RSMemoryHeap* heap;

		// Resource which currently owns this memory, may be NULL.
		RSResource* resource;

		// Memory key, which is a pool/allocator-specific value used to identify referenced memory.
		RS_memory_size_t memoryOffset;

		// Size of this memory block.
		RS_memory_size_t memorySize;
	};


	///<summary>
	///</summary>
	struct RSResourceMemoryAllocationDesc
	{
		//
		union TargetInfo
		{
			struct Buffer
			{
				//
				GPUBufferType type;
			};

			struct Texture
			{
				//
				TextureType type;

				//
				bool isRTDSTexture;

				//
				bool isMSAAEnabled;
			};

			//
			Buffer buffer;

			//
			Texture texture;
		};

		// Size of memory to allocate, in bytes.
		Size_t size;

		//
		RSMemoryAccess access;
		
		//
		RSMemoryAllocationTarget allocationTarget;

		//
		TargetInfo targetInfo;

		//
		stdx::mask<RSResourceUsageFlags> resourceUsageFlags;
	};


}


#endif /* __Exs_RenderSystem_RSMemoryCommon_H__ */
