
#ifndef __Exs_RenderSystem_RSMemorySystem_H__
#define __Exs_RenderSystem_RSMemorySystem_H__

#include "DeviceMemory.h"
#include "RSMemoryHeap.h"
#include "Allocators/RSAllocatorDefault.h"


namespace Exs
{


	ExsDeclareRefPtrClass(DeviceMemoryPool);
	ExsDeclareRefPtrClass(RSMemoryHeap);


	struct RSResourceMemoryHeapDesc
	{
		//
		RS_memory_size_t size;

		//
		RSMemoryHeapResourceType resourceType;

		//
		RSMemoryHeapUsage usage;

		//
		RSMemoryAlignment alignment;
	};


	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM RSMemorySystem
	{
		friend class RenderSystem;

	public:
		struct DeviceMemoryPoolInfo
		{
			DeviceMemoryType memoryType;
			DeviceMemoryPoolRefPtr pool;
		};

		struct RSMemoryHeapInfo
		{
			RSMemoryHeapRefPtr heap;
		};

		typedef std::vector<DeviceMemoryPoolInfo> DeviceMemoryPoolArray;
		typedef std::vector<RSMemoryHeapInfo> RSMemoryHeapArray;

	protected:
		RenderSystem*            _renderSystem;
		DeviceMemoryPoolArray    _deviceMemoryPools;
		RSMemoryHeapArray        _rsMemoryHeaps;
		LightMutex _internalLock;

	public:
		///<summary>
		///</summary>
		RSMemorySystem(RenderSystem* renderSystem);

		///<summary>
		///</summary>
		virtual ~RSMemorySystem();

		///<summary>
		///</summary>
		template <class Allocator_t, class... Args>
		RSMemoryHeap* CreateHeap(const RSResourceMemoryHeapDesc& heapDesc, Args&&... args);

		///<summary>
		///</summary>
		Result Reset();
		
		///<summary>
		///</summary>
		static RSMemoryHeapProperties GetHeapRequirements(const RSResourceMemoryAllocationDesc& allocationDesc);
		
		///<summary>
		///</summary>
		static bool CheckHeapCompatibility(const RSMemoryHeapProperties& heapProperties, const RSMemoryHeapProperties& requiredProperties);

	private:
		//
		virtual RSMemoryHeap* _CreateResourceHeap(const RSResourceMemoryHeapDesc& heapDesc) = 0;
	};


	template <class Allocator_t, class... Args>
	inline RSMemoryHeap* RSMemorySystem::CreateHeap(const RSResourceMemoryHeapDesc& heapDesc, Args&&... args)
	{
		ExsEnterCriticalSection(this->_internalLock);
		{
			if (RSMemoryHeap* heap = this->_CreateResourceHeap(heapDesc))
			{
				auto allocatorWrapper = CreateRSAllocatorWrapper<Allocator_t>(heap, heapDesc.size, std::forward<Args>(args)...);
				heap->SetAllocator(std::move(allocatorWrapper));
				return heap;
			}
		}
		ExsLeaveCriticalSection();

		return nullptr;
	}


}


#endif /* __Exs_RenderSystem_RSMemorySystem_H__ */
