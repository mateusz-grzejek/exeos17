
#ifndef __Exs_RenderSystem_RSMemoryHeap_H__
#define __Exs_RenderSystem_RSMemoryHeap_H__

#include "RSAllocator.h"

#define ExsEnumDeclareMemoryHeapID(heapIndex) \
	(RSMemoryHeapID)((Uint16)heapIndex);

#define ExsEnumGetMemoryHeapIDIndex(heapID) \
	(Uint16)((RSMemoryHeapID)heapID);


namespace Exs
{


	struct DeviceMemoryBlock;

	class DeviceMemoryPool;
	class RSMemoryHeap;


	struct RSMemoryHeapProperties
	{
		//
		stdx::mask<RSMemoryHeapPropertyFlags> flags;
	};


	///<summary>
	///</summary>
	class RSMemoryHeap
	{
		friend class RSMemorySystem;

	protected:
		RSMemorySystem*           _memorySystem;
		DeviceMemoryBlock*        _deviceMemory;
		RSMemoryHeapID            _id;
		RS_memory_size_t          _size;
		RSMemoryHeapProperties    _properties;
		RSAllocatorWrapper        _allocatorWrapper;

	public:
		///<summary>
		///</summary>
		///<param name="memorySystem"> Pointer to the memory system object. </param>
		///<param name="heapBlock">  </param>
		///<param name="id">  </param>
		RSMemoryHeap(RSMemorySystem* memorySystem, DeviceMemoryBlock* deviceMemory, RSMemoryHeapID id, RS_memory_size_t size, const RSMemoryHeapProperties& properties)
		: _memorySystem(memorySystem)
		, _deviceMemory(deviceMemory)
		, _id(id)
		, _size(size)
		, _properties(properties)
		{ }

		///<summary>
		///</summary>
		RSMemoryRefData* Alloc(RS_memory_size_t size)
		{
			return this->_allocatorWrapper.Alloc(size);
		}

		///<summary>
		///</summary>
		void Free(RSMemoryRefData* memoryRefData)
		{
			this->_allocatorWrapper.Free(memoryRefData);
		}
		
		///<summary>
		///</summary>
		RSMemorySystem* GetMemorySystem() const
		{
			return this->_memorySystem;
		}
		
		///<summary>
		///</summary>
		const DeviceMemoryBlock* GetDeviceMemory() const
		{
			return this->_deviceMemory;
		}

		RSMemoryHeapID GetID() const
		{
			return this->_id;
		}

		///<summary>
		///</summary>
		RS_memory_size_t GetSize() const
		{
			return this->_size;
		}
		
		///<summary>
		///</summary>
		const RSMemoryHeapProperties& GetProperties() const
		{
			return this->_properties;
		}

	friendapi:
		void SetAllocator(RSAllocatorWrapper&& allocatorWrapper)
		{
			this->_allocatorWrapper = std::forward<RSAllocatorWrapper>(allocatorWrapper);
		}
	};


}


#endif /* __Exs_RenderSystem_RSMemoryHeap_H__ */
