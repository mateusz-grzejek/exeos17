
#ifndef __Exs_RenderSystem_GraphicsDriverPlugin_H__
#define __Exs_RenderSystem_GraphicsDriverPlugin_H__

#include "Prerequisites.h"
#include <Exs/Core/Plugins/Plugin.h>
#include <Exs/Core/Plugins/PluginManager.h>


namespace Exs
{


	class GraphicsDriverPlugin;
	class GraphicsDriverPluginManager;

	ExsDeclareRefPtrClass(GraphicsDriver);


	enum : PluginType
	{
		PluginType_GraphicsDriver = 0x4E000007
	};


	EXS_PLUGIN_REGISTER_PLUGIN_TYPE(PluginType_GraphicsDriver, GraphicsDriverPluginManager, GraphicsDriverPlugin);


	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM GraphicsDriverPlugin : public Plugin
	{
	protected:
		GraphicsDriverRefPtr  _graphicDriver;

	public:
		///<summary>
		///</summary>
		GraphicsDriverPlugin(GraphicsDriverPluginManager* manager);

		///<summary>
		///</summary>
		virtual ~GraphicsDriverPlugin();

		///<summary>
		///</summary>
		virtual GraphicsDriverRefPtr CreateGraphicsDriver() = 0;

		///<summary>
		///</summary>
		const GraphicsDriverRefPtr& GetGraphicsDriver() const;
	};


	inline const GraphicsDriverRefPtr& GraphicsDriverPlugin::GetGraphicsDriver() const
	{
		return this->_graphicDriver;
	}


	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM GraphicsDriverPluginManager : public PluginManager
	{
	public:
		GraphicsDriverPluginManager(PluginSystem* pluginSystem);
		virtual ~GraphicsDriverPluginManager();

	private:
		virtual void OnRegisterPlugin(Plugin* plugin) override;
		virtual void OnUnregisterPlugin(Plugin* plugin) override;
	};


}


#endif /* __Exs_RenderSystem_GraphicsDriverPlugin_H__ */
