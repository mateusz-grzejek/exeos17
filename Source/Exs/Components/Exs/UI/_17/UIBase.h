
#ifndef __Exs_UI_UIBase_H__
#define __Exs_UI_UIBase_H__

#include "Prerequisites.h"


namespace Exs
{


	struct UIGeometryBatch
	{
		// Base offset within the index buffer storage of a manager which owns the geometry this batch references.
		Uint indicesBaseOffset;
	
		// Number of indices this batch contains.
		Uint32 indicesNum;
	
		// Number of vertices this batch contains.
		Uint32 verticesNum;

		//
		// ShaderResourceViewHandle sourceTexture;
	};


	enum class UIAnchorPoint : Enum
	{
		Bottom_Left,

		Bottom_Right,

		Center,

		Top_Left,

		Top_Right,

		Default = Top_Left
	};


	enum class UIScaleMode : Enum
	{
		// Normal scale mode, which scales control and all its children. Offsets are scaled
		// as well, so if the control is attached to its parent at position (10,6), after scaling
		// by 0.5 its position will change to (5,3).
		Normal,

		// Scaling is done only to control it is set on (do not recursively propagate on its children).
		Non_Recursive,

		// Scales parent and all its children, but preserves offsets. For example, if the control is
		// attached to its parent at position (10,6), after scaling by 0.5 its position will not change,
		// but its size will be properly reduced to the half of the original one.
		Preserve_Offset,

		// Default scale mode: Normal.
		Default = Normal
	};


}


#endif /* __Exs_UI_UIBase_H__ */
