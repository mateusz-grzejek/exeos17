
#include <ExsUI/Geometry/UIGeometry.h>
#include <ExsUI/UIText.h>
#include <ExsResManager/Resources/Font.h>


namespace Exs
{
	

	UIGeometryManager::UIGeometryManager(RenderSystem* renderSystem)
	: GenericRectGeometryManager(renderSystem, sizeof(Uint32), sizeof(UIGeometryVertex))
	{ }


	UIGeometryManager::~UIGeometryManager()
	{ }


	Handle<UIGeometry> UIGeometryManager::AddGeometry(Size_t geometrySize)
	{
		if (geometrySize == 0)
			return nullptr;

		auto geometry = this->Allocate<UIGeometry, UIGeometryDataRef>(geometrySize);
		return geometry;
	}


	bool UIGeometryManager::UpdateTextGeometry(UIGeometry& textGeometry, const std::string& newText, const TextProperties& textProperties)
	{
		ExsDebugAssert( textGeometry.GetGeometryManager() == this );
		const auto& textGeometryDataRef = textGeometry.GetDataRef();

		auto charData = TextUtils::GenerateTextCharData(newText, textProperties);
		Size_t charDataSize = charData.size() - 1; // Last element is the trailing one (-1).

		Uint32* indicesDataPtr = this->GetIndicesUpdatePtr<Uint32>(textGeometryDataRef, charDataSize);
		auto* verticesDataPtr = this->GetVerticesUpdatePtr<UIGeometryVertex>(textGeometryDataRef, charDataSize);

		// Index of first vertex within this textGeometry.
		Uint32 baseIndex = textGeometryDataRef.verticesElementOffset;
		Uint32 currentLayer = charData[0].bitmapLayer;

		// Curent textGeometry batch;
		UIGeometryBatch currentBatch;
		currentBatch.indicesNum = currentBatch.verticesNum = 0;

		// Base offset to indices used by this textGeometry.
		currentBatch.indicesBaseOffset = textGeometryDataRef.indicesMemory.bufferRange.offset;

		for (auto& ch : charData)
		{
			// Check if chars are placed on different layers of the font. In this case,
			// we might need to create another batch, as texture switch may be required.
			if (ch.bitmapLayer != currentLayer)
			{
				// Get texture of current char and the next one. If it is different from the current one, create new
				// batch. Some fonts may use texture arrays for storing glyphs and do not require one batch per layer.

				auto currentLayerTexture = textProperties.font->GetLayerTextureView(currentLayer);
				auto nextLayerTexture = (ch.bitmapLayer >= 0) ? textProperties.font->GetLayerTextureView(ch.bitmapLayer) : nullptr;

				if (nextLayerTexture != currentLayerTexture)
				{
					currentBatch.sourceTexture = currentLayerTexture;

					// Add batch to the textGeometry.
					textGeometry.AddBatch(currentBatch);

					currentBatch.indicesBaseOffset += currentBatch.indicesNum * this->_indicesDataSize;
					currentBatch.indicesNum = currentBatch.verticesNum = 0;
				}

				if (ch.bitmapLayer == -1)
					break;

				currentLayer = ch.bitmapLayer;
			}

			// Compute and fill data of vertices.
			FillTextCharGeometry(ch, textProperties, verticesDataPtr);

			indicesDataPtr[0] = (baseIndex);
			indicesDataPtr[1] = (baseIndex + 1);
			indicesDataPtr[2] = (baseIndex + 2);
			indicesDataPtr[3] = (baseIndex);
			indicesDataPtr[4] = (baseIndex + 2);
			indicesDataPtr[5] = (baseIndex + 3);

			baseIndex += 4;
			indicesDataPtr += 6;
			verticesDataPtr += 4;
			currentBatch.indicesNum += 6;
			currentBatch.verticesNum += 4;
		}

		return true;
	}


	void UIGeometryManager::FillTextCharGeometry(const TextCharData& charData, const TextProperties& textProperties, UIGeometryVertex* vertices)
	{
		float cxpos = static_cast<float>(charData.rect.position.x) / 64.0f + textProperties.baseOffset.x;
		float cypos = static_cast<float>(charData.rect.position.y) / 64.0f + textProperties.baseOffset.y;
		float cxsize = static_cast<float>(charData.rect.size.x) / 64.0f;
		float cysize = static_cast<float>(charData.rect.size.y) / 64.0f;

		vertices[0].position.x = cxpos;
		vertices[0].position.y = cypos;
		vertices[0].position.z = textProperties.baseOffset.z;
		vertices[0].fixedColor = textProperties.color;
		vertices[0].texCoord.x = charData.texc.position.x;
		vertices[0].texCoord.y = charData.texc.position.y;
		vertices[0].texCoord.z = static_cast<float>(charData.bitmapLayer);
		
		vertices[1].position.x = cxpos + cxsize;
		vertices[1].position.y = cypos;
		vertices[1].position.z = textProperties.baseOffset.z;
		vertices[1].fixedColor = textProperties.color;
		vertices[1].texCoord.x = charData.texc.position.x + charData.texc.size.x;
		vertices[1].texCoord.y = charData.texc.position.y;
		vertices[1].texCoord.z = static_cast<float>(charData.bitmapLayer);
		
		vertices[2].position.x = cxpos + cxsize;
		vertices[2].position.y = cypos + cysize;
		vertices[2].position.z = textProperties.baseOffset.z;
		vertices[2].fixedColor = textProperties.color;
		vertices[2].texCoord.x = charData.texc.position.x + charData.texc.size.x;
		vertices[2].texCoord.y = charData.texc.position.y + charData.texc.size.y;
		vertices[2].texCoord.z = static_cast<float>(charData.bitmapLayer);
		
		vertices[3].position.x = cxpos;
		vertices[3].position.y = cypos + cysize;
		vertices[3].position.z = textProperties.baseOffset.z;
		vertices[3].fixedColor = textProperties.color;
		vertices[3].texCoord.x = charData.texc.position.x;
		vertices[3].texCoord.y = charData.texc.position.y + charData.texc.size.y;
		vertices[3].texCoord.z = static_cast<float>(charData.bitmapLayer);
	}




	Handle<UIGeometryManager> CreateUIGeometryManager(RenderSystem* renderSystem, const UIGeometryManagerInitDesc& initDesc)
	{
		auto manager = CreateAutoRefObject<UIGeometryManager>(renderSystem);

		if (!manager->Initialize(initDesc))
			return nullptr;

		return manager;
	}


}
