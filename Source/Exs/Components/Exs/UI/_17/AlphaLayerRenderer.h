
#ifndef __Exs_UI_AlphaLayerRenderer_H__
#define __Exs_UI_AlphaLayerRenderer_H__

#include "Prerequisites.h"
#include <ExsEngine/RS/BaseRenderer.h>


namespace Exs
{


	class EXS_LIBCLASS_UI AlphaLayerRenderer : public BaseRenderer
	{
		friend class TextRenderer;
		
	private:
		//
		static ShaderHandle sharedPixelShader;

		//
		static ShaderHandle sharedVertexShader;

		//
		static GraphicsPipelineStateObjectHandle sharedGraphicsPipelineState;

	public:
		AlphaLayerRenderer(RSContextCommandQueue* commandQueue);
		
		static bool InitDefaultGraphicsPipelineState(RenderSystem* renderSystem, const ShaderHandle& vertexShader, const ShaderHandle& pixelShader);
	};


}


#endif /* __Exs_UI_AlphaLayerRenderer_H__ */
