
#ifndef __Exs_UI_UIText_H__
#define __Exs_UI_UIText_H__

#include "UIComponent.h"
#include "UITextBase.h"
#include "Geometry/UITextGeometry.h"


namespace Exs
{


	class EXS_LIBCLASS_UI UIText : public UIComponent
	{
	private:
		Font*           _font;
		std::string     _text;
		UITextGeometry  _geometry;

	public:
		UIText(UIWidget* parent, Font& font, const std::string& text, const Color& color = ColorU32::White);
		
		void UpdateFont(Font& font);
		
		void UpdateText(const std::string& text);

		const std::string& GetText() const;
	};


	inline void UIText::UpdateFont(Font& font)
	{
		this->_font = &font;
		this->_state.set(UIComponentState_Base_Property_Changed);

	}
	
	inline void UIText::UpdateText(const std::string& text)
	{
		this->_text = text;
		this->_state.set(UIComponentState_Base_Property_Changed);
	}
	
	inline const std::string& UIText::GetText() const
	{
		return this->_text;
	}


}


#endif /* __Exs_UI_UIText_H__ */
