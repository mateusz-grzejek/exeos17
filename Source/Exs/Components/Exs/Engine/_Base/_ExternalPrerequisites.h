
#ifndef __Exs_Engine_ExternalPrerequisites_H__
#define __Exs_Engine_ExternalPrerequisites_H__

#include <Exs/Core/Concrt/AsyncCommon.h>
#include <Exs/Core/Concrt/ThreadCommon.h>

#pragma comment( lib, "stdx.lib" )

#pragma comment( lib, "ExsLib.CoreUtils.lib" )
#pragma comment( lib, "ExsLib.System.lib" )

#pragma comment( lib, "Exs.Core.lib" )

#endif /* __Exs_Engine_ExternalPrerequisites_H__ */
