
#ifndef __Exs_Engine_GameObject_H__
#define __Exs_Engine_GameObject_H__

#include "../EngineBase.h"


namespace Exs
{


	template <typename TValue>
	class GenericProperty;

	class DynamicProperty;


	class GameObjectProperty
	{
	public:
		template <typename TValue>
		GenericProperty<TValue>* AsGeneric();

		DynamicProperty* AsDynamic();

		virtual void SetValueString( const std::string& valstr ) = 0;

		virtual std::string ToString() const = 0;
	};


	template <typename TValue>
	class GenericProperty : public GameObjectProperty
	{public:
		using GetFunc = std::function<const TValue&( )>;

		using SetFunc = std::function<void( const TValue& )>;

	public:
		template <class TGetCallable, class TSetCallable>
		GenericProperty( TGetCallable getCallable, TSetCallable setCallable )
		{ }

		virtual void SetValueString( const std::string& valstr ) override final
		{

		}

		virtual std::string ToString() const override final
		{

		}

	private:
		GetFunc _getFunc;
		SetFunc _setFunc;
	};


}


#endif /* __Exs_Engine_GameObject_H__ */
