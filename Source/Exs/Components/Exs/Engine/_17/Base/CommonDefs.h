
#ifndef __Exs_Engine_CommonDefs_H__
#define __Exs_Engine_CommonDefs_H__


namespace Exs
{


	enum : TraceCategoryID
	{
		TRC_Engine_Common = 0x770001,
		TRC_Engine_Font_System = 0x7700F1,
	};

	
	EXS_TRACE_SET_CATEGORY_NAME(TRC_Engine_Common, "Engine:Common");
	EXS_TRACE_SET_CATEGORY_NAME(TRC_Engine_Font_System, "Engine:Fonts");


	template <class T>
	using Handle = std::shared_ptr<T>;


	template <class T, class... Args>
	inline Handle<T> CreateAutoRefObject(Args&&... args)
	{
		return std::make_shared<T>(std::forward<Args>(args)...);
	}


}


#endif /* __Exs_Engine_CommonDefs_H__ */
