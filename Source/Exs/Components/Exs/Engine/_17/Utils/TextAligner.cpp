
#include <ExsResManager/Resources/Font.h>
#include <ExsEngine/Utils/TextAligner.h>


namespace Exs
{


	AlignedText AlignText(const std::string& text, Font& font, const Vector2U32& baseOffset)
	{
		Uint64 xbase = baseOffset.x * 64;
		Uint64 ybase = baseOffset.y * 64;
		Uint32 hmax = 0;

		AlignedText alignedText;
		alignedText.length = 0;

		for (auto& c : text)
		{
			if (const auto* cglyph = font.GetGlyph(c))
			{
				AlignedChar achar;
				achar.codePoint = c;
				achar.glyph = cglyph;
				achar.bitmapLayer = cglyph->bitmap.layerIndex;

				achar.pos.x = xbase;
				achar.pos.y = ybase;
				achar.size.x = cglyph->metrics.size.x;
				achar.size.y = cglyph->metrics.size.y;

				xbase +=cglyph->metrics.advance.x;
				Uint32 gheight = (Uint32)(cglyph->metrics.size.y / 64);

				if (gheight > hmax)
					hmax = gheight;

				alignedText.data.push_back(achar);
				++alignedText.length;
			}
		}

		alignedText.wsize.x = (float)xbase;
		alignedText.wsize.y = (float)hmax;

		std::sort(
			alignedText.data.begin(),
			alignedText.data.end(),
			[](const AlignedChar& left, const AlignedChar& right) -> bool {
				return left.bitmapLayer < right.bitmapLayer;
		});

		return alignedText;
	}


}
