
#ifndef __Exs_Engine_BaseRenderer_H__
#define __Exs_Engine_BaseRenderer_H__

#include "../Prerequisites.h"


namespace Exs
{

	
	class RenderingContext;

	ExsDeclareRSClassObjHandle(ConstantBuffer);
	ExsDeclareRSClassObjHandle(GraphicsPipelineStateObject);
	ExsDeclareRSClassObjHandle(Sampler);
	ExsDeclareRSClassObjHandle(Shader);


	class BaseRenderer
	{
	protected:
		RenderingContext*  _rcontext;

	public:
		BaseRenderer(RenderingContext* rcontext)
		: _rcontext(rcontext)
		{ }
	};


}


#endif /* __Exs_Engine_BaseRenderer_H__ */
