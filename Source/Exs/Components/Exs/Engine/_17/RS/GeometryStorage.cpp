
#include <ExsEngine/RS/GeometryStorage.h>
#include <ExsRenderSystem/Resource/GeometryBuffer.h>


namespace Exs
{


	GeometryStorage::GeometryStorage(RenderSystem* renderSystem, const GeometryBufferHandle& buffer, const RSMemoryRange& bufferRange)
	: _renderSystem(renderSystem)
	, _buffer(buffer)
	, _bufferRange(bufferRange)
	, _reservedStorageSize(0)
	, _updateMode(GeometryUpdateMode::None)
	{ }


	GeometryStorage::~GeometryStorage()
	{ }


	bool GeometryStorage::Allocate(Size_t dataSize, GeometryMemory* reservedMemory)
	{
		if (dataSize > 0)
		{
			if (!this->CheckFreeSpace(dataSize))
				return false;
		
			if (reservedMemory != nullptr)
			{
				// Data byte offset within the WHOLE buffer: base offset + size of data currently in the buffer.
				reservedMemory->bufferRange.offset = this->_bufferRange.offset + this->_reservedStorageSize;
				reservedMemory->bufferRange.length = dataSize;

				this->_reservedStorageSize += dataSize;
			}
		}

		return true;
	}


	void GeometryStorage::BeginUpdate(GeometryUpdateMode updateMode)
	{
		// This means, that BeginUpdate() was called before without matching EndUpdate().
		if (this->_updateMode != GeometryUpdateMode::None)
		{
			ExsDebugInterrupt();
			return;
		}
		
		if (updateMode == GeometryUpdateMode::None)
		{
			ExsDebugInterrupt();
			return;
		}
		
		RSMemoryRange bufferMapRange;
		GPUBufferUpdateMode bufferUpdateMode;
		
		if (updateMode == GeometryUpdateMode::Append)
		{
			// Append: Map subrange of the buffer which has not been allocated yet.
			bufferUpdateMode = GPUBufferUpdateMode::Append;
			bufferMapRange.offset = this->_reservedStorageSize;
			bufferMapRange.length = this->_bufferRange.length - bufferMapRange.offset;
		}
		else if (updateMode == GeometryUpdateMode::Modify)
		{
			bufferUpdateMode = GPUBufferUpdateMode::Modify;
			bufferMapRange.offset = 0;
			bufferMapRange.length = this->_reservedStorageSize;
		}
		else if (updateMode == GeometryUpdateMode::Unspecified)
		{
			bufferUpdateMode = GPUBufferUpdateMode::Modify;
			bufferMapRange.offset = 0;
			bufferMapRange.length = this->_bufferRange.length;
		}
		else // Added new update mode? Implement!
		{
			ExsDebugInterrupt();
			return;
		}

		if (!this->_buffer->BeginDynamicUpdate(bufferMapRange, bufferUpdateMode))
		{
			//?
			return;
		}

		this->_updateMode = updateMode;
	}


	void GeometryStorage::EndUpdate()
	{
		// This means, that EndUpdate() was called without a previous call to BeginUpdate().
		if (this->_updateMode == GeometryUpdateMode::None)
		{
			ExsDebugInterrupt();
			return;
		}

		this->_buffer->EndDynamicUpdate();
		this->_updateMode = GeometryUpdateMode::None;
	}


	void* GeometryStorage::UpdateDynamic(const GeometryMemory& memory, const RSMemoryRange& subrange)
	{
		ExsDebugAssert( subrange.length <= memory.bufferRange.length );

		// Absolute data offset (from the beginning of the buffer).
		Uint dataOffset = memory.bufferRange.offset + subrange.offset;

		if (const auto& mappedMemory = this->_buffer->GetMappedMemoryInfo())
		{
			if (this->IsMemoryMapped(memory, subrange))
			{
				// Compute pointer to the beginning of the data used by the specified geometry object.
				return mappedMemory.ptr + (dataOffset - mappedMemory.range.offset);
			}
		}

		Byte* updatePtr = this->_updateCache.Reserve<Byte>(subrange.length, dataOffset);
		ExsDebugAssert( updatePtr != nullptr );

		return updatePtr;
	}


	void GeometryStorage::UpdateImmediate(const GeometryMemory& memory, const RSMemoryRange& subrange, const DataDesc& dataDesc)
	{
		Uint rangeOffset = memory.bufferRange.offset + subrange.offset;
		Uint storageEnd = memory.bufferRange.offset + memory.bufferRange.length;

		if ((rangeOffset >= storageEnd) || (rangeOffset + subrange.length > storageEnd))
		{
			ExsDebugInterrupt();
			return;
		}

		this->_buffer->CopySubdata(RSMemoryRange(rangeOffset, subrange.length), dataDesc.ptr, dataDesc.size);
	}


	bool GeometryStorage::IsMemoryMapped(const GeometryMemory& memory, const RSMemoryRange& subrange) const
	{
		if (const auto& mappedMemory = this->_buffer->GetMappedMemoryInfo())
		{
			Uint storageOffset = memory.bufferRange.offset + subrange.offset;
			Uint mappedMemoryEnd = mappedMemory.range.offset + mappedMemory.range.length;

			if ((storageOffset >= mappedMemory.range.offset) && (storageOffset + subrange.length <= mappedMemoryEnd))
				return true;
		}

		return false;
	}


	void GeometryStorage::FlushDynamicUpdateCache()
	{
		if (!this->_updateCache.IsEmpty())
		{
			// Fetch list of batches with update info.
			const auto& updateList = this->_updateCache.GetUpdateList();

			for (const auto& updateBatch : updateList)
			{
				// Get pointer to the data inside the cache. 
				const Byte* dataPtr = this->_updateCache.GetBatchMemory(updateBatch);
				this->_buffer->CopySubdata(RSMemoryRange(updateBatch.bufferOffset, updateBatch.dataSize), dataPtr, updateBatch.dataSize);
			}

			this->_updateCache.Reset();
		}
	}


}
