
#include <ExsEngine/RS/GeometryManager.h>
#include <ExsEngine/RS/GeometryStorage.h>
#include <ExsRenderSystem/RenderSystem.h>
#include <ExsRenderSystem/Resource/IndexBuffer.h>
#include <ExsRenderSystem/Resource/VertexBuffer.h>
#include <ExsRenderSystem/State/VertexArrayStateObject.h>



namespace Exs
{


	GeometryManager::GeometryManager(RenderSystem* renderSystem, Uint32 vbnumHint)
	: _renderSystem(renderSystem)
	{
		if (vbnumHint > 0)
		{
			// If hint is specified, reserve memory.
			this->_vbRefInfoArray.reserve(vbnumHint);
		}
	}


	GeometryManager::~GeometryManager()
	{ }


	void GeometryManager::BeginUpdate(GeometryUpdateMode updateMode)
	{
		if (this->_ibRefInfo.storage)
		{
			this->_ibRefInfo.storage->BeginUpdate(updateMode);
		}

		for (auto& vbState : this->_vbRefInfoArray)
		{
			vbState.storage->BeginUpdate(updateMode);
		}
	}


	void GeometryManager::EndUpdate()
	{
		if (this->_ibRefInfo.storage)
		{
			this->_ibRefInfo.storage->EndUpdate();
			this->_ibRefInfo.storage->FlushDynamicUpdateCache();
		}

		for (auto& vbState : this->_vbRefInfoArray)
		{
			vbState.storage->EndUpdate();
			vbState.storage->FlushDynamicUpdateCache();
		}
	}


	void GeometryManager::FlushUpdateCache()
	{
		if (this->_ibRefInfo.storage)
		{
			this->_ibRefInfo.storage->FlushDynamicUpdateCache();
		}

		for (auto& vbState : this->_vbRefInfoArray)
		{
			vbState.storage->FlushDynamicUpdateCache();
		}
	}


	bool GeometryManager::BindIndexBuffer(const IndexBufferHandle& indexBuffer, const RSMemoryRange& bindRange, IndexBufferDataType indexDataType, GeometryBufferState* state)
	{
		if (this->_vertexArrayStateObject)
		{
			ExsTraceError(TRC_Engine_Common, "Geometry buffers cannot be bound after VASO is created!");
			return false;
		}

		if (this->_ibRefInfo.storage)
		{
			ExsTraceError(TRC_Engine_Common, "Index buffer has been already created!");
			return false;
		}

		this->_InitializeIndexBuffer(indexBuffer, bindRange, indexDataType, state);
		return true;
	}

	
	bool GeometryManager::BindVertexBuffer(Uint32 index, const VertexBufferHandle& vertexBuffer, const RSMemoryRange& bindRange, Size_t vertexSize, GeometryBufferState* state)
	{
		if (this->_vertexArrayStateObject)
		{
			ExsTraceError(TRC_Engine_Common, "Geometry buffers cannot be bound after VASO is created!");
			return false;
		}

		if (this->_IsVertexBufferPresent(index))
		{
			ExsTraceError(TRC_Engine_Common, "Vertex buffer fir stream  has been already created!");
			return false;
		}
		
		this->_InitializeVertexBuffer(index, vertexBuffer, bindRange, vertexSize, state);
		return true;
	}
	

	bool GeometryManager::CreateIndexBuffer(const IndexBufferCreateInfo& createInfo, Size_t bufferElementCapacity, IndexBufferDataType indexDataType, GeometryBufferState* state)
	{
		if (this->_vertexArrayStateObject)
		{
			ExsTraceError(TRC_Engine_Common, "Geometry buffers cannot be bound after VASO is created!");
			return false;
		}

		if (this->_ibRefInfo.storage)
		{
			ExsTraceError(TRC_Engine_Common, "Index buffer has been already created!");
			return false;
		}

		IndexBufferCreateInfo bufferCreateInfo;
		bufferCreateInfo.memoryAccess = createInfo.memoryAccess;
		bufferCreateInfo.usageFlags = createInfo.usageFlags;
		bufferCreateInfo.sizeInBytes = bufferElementCapacity * ExsEnumGetGraphicDataBaseTypeSize(indexDataType);
		
		if (auto indexBuffer = this->_renderSystem->CreateIndexBuffer(bufferCreateInfo))
		{
			this->_InitializeIndexBuffer(indexBuffer, RSMemoryRange(0, bufferCreateInfo.sizeInBytes), indexDataType, state);
			return true;
		}

		return false;
	}
	

	bool GeometryManager::CreateVertexBuffer(Uint32 index, const VertexBufferCreateInfo& createInfo, Size_t bufferElementCapacity, Size_t vertexSize, GeometryBufferState* state)
	{
		if (this->_vertexArrayStateObject)
		{
			ExsTraceError(TRC_Engine_Common, "Geometry buffers cannot be bound after VASO is created!");
			return false;
		}

		if (this->_IsVertexBufferPresent(index))
		{
			ExsTraceError(TRC_Engine_Common, "Vertex buffer fir stream  has been already created!");
			return false;
		}

		VertexBufferCreateInfo bufferCreateInfo;
		bufferCreateInfo.memoryAccess = createInfo.memoryAccess;
		bufferCreateInfo.usageFlags = createInfo.usageFlags;
		bufferCreateInfo.sizeInBytes = bufferElementCapacity * vertexSize;

		if (auto vertexBuffer = this->_renderSystem->CreateVertexBuffer(bufferCreateInfo))
		{
			this->_InitializeVertexBuffer(index, vertexBuffer, RSMemoryRange(0, bufferCreateInfo.sizeInBytes), vertexSize, state);
			return true;
		}

		return false;
	}


	void GeometryManager::CreateVertexArrayStateObject()
	{
		if (this->_vertexArrayStateObject)
		{
			ExsTraceError(TRC_Engine_Common, "VASO has been already created!");
			return;
		}

		VertexArrayStateObjectCreateInfo vasCreateInfo;
		vasCreateInfo.vertexStreamConfiguration.indexBufferBinding.buffer = this->_ibRefInfo.buffer;
		vasCreateInfo.vertexStreamConfiguration.indexBufferBinding.dataBaseOffset = 0;

		for (auto& vbState : this->_vbRefInfoArray)
		{
			if (vbState.buffer)
			{
				auto& vbbinding = vasCreateInfo.vertexStreamConfiguration.vertexBufferBindingArray[vbState.index];
				vbbinding.buffer = vbState.buffer;
				vbbinding.dataBaseOffset = 0;
				vbbinding.dataStride = vbState.elementSize;
			}
		}

		this->_vertexArrayStateObject = this->_renderSystem->CreateVertexArrayStateObject(vasCreateInfo);
		ExsDebugAssert( this->_vertexArrayStateObject );
	}


	void GeometryManager::_InitializeIndexBuffer(const IndexBufferHandle& indexBuffer, const RSMemoryRange& bindRange, IndexBufferDataType indexDataType, GeometryBufferState* state)
	{
		ExsDebugAssert( !this->_ibRefInfo.buffer );

		this->_ibRefInfo.buffer = indexBuffer;
		this->_ibRefInfo.elementSize = ExsEnumGetGraphicDataBaseTypeSize(indexDataType);
		this->_ibRefInfo.storage = std::make_unique<GeometryStorage>(this->_renderSystem, indexBuffer, bindRange);

		state->bufferStorage = this->_ibRefInfo.storage.get();
		state->elementSize = this->_ibRefInfo.elementSize;
		state->elementsNum = 0;
		state->elementsNumLimit = truncate_cast<Uint32>(bindRange.length / this->_ibRefInfo.elementSize);
	}


	void GeometryManager::_InitializeVertexBuffer(Uint32 index, const VertexBufferHandle& vertexBuffer, const RSMemoryRange& bindRange, Size_t vertexSize, GeometryBufferState* state)
	{
		ExsDebugAssert( !this->_IsVertexBufferPresent(index) );

		this->_vbRefInfoArray.push_back(VertexBufferRefInfo());
		auto& vbRefInfo = this->_vbRefInfoArray.back();

		vbRefInfo.buffer = vertexBuffer;
		vbRefInfo.elementSize = vertexSize;
		vbRefInfo.index = index;
		vbRefInfo.storage = std::make_unique<GeometryStorage>(this->_renderSystem, vertexBuffer, bindRange);

		state->bufferStorage = vbRefInfo.storage.get();
		state->elementSize = vbRefInfo.elementSize;
		state->elementsNum = 0;
		state->elementsNumLimit = truncate_cast<Uint32>(bindRange.length / vbRefInfo.elementSize);
	}


	bool GeometryManager::_IsVertexBufferPresent(Uint32 index) const
	{
		for (auto& vbState : this->_vbRefInfoArray)
		{
			if (vbState.index == index)
				return true;
		}

		return false;
	}


}
