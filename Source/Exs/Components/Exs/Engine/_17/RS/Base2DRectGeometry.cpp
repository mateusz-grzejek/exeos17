
#include <ExsEngine/RS/GeometryStorage.h>
#include <ExsEngine/RS/Base2DRectGeometry.h>
#include <ExsRenderSystem/Resource/IndexBuffer.h>
#include <ExsRenderSystem/Resource/VertexBuffer.h>


namespace Exs
{


	Base2DRectGeometryManager::Base2DRectGeometryManager(RenderSystem* renderSystem)
	: GeometryManager(renderSystem, 1)
	{ }


	Base2DRectGeometryManager::~Base2DRectGeometryManager()
	{ }


	bool Base2DRectGeometryManager::Initialize(const Base2DRectGeometryManagerInitDesc& initDesc)
	{
		ExsDebugAssert( (this->_indexBufferStorage == nullptr) && (this->_vertexBufferStorage == nullptr) );

		if (!initDesc.vertexBuffer && (initDesc.verticesStorageCapacity == 0))
			return false;

		if (initDesc.indexBuffer)
		{
			this->BindIndexBuffer(initDesc.indexBuffer, initDesc.indexBufferRange, initDesc.indicesDataType, &(this->_indexBufferState));
		}
		else if (initDesc.indicesStorageCapacity > 0)
		{
			IndexBufferCreateInfo ibCreateInfo;
			ibCreateInfo.memoryAccess = RSMemoryAccess::Write_Only;
			ibCreateInfo.usageFlags = RSResourceUsage_Dynamic;
			this->CreateIndexBuffer(ibCreateInfo, initDesc.indicesStorageCapacity, initDesc.indicesDataType, &(this->_indexBufferState));
		}

		if (initDesc.vertexBuffer)
		{
			this->BindVertexBuffer(0, initDesc.vertexBuffer, initDesc.vertexBufferRange, initDesc.verticesDataSize, &(this->_vertexBufferState));
		}
		else
		{
			VertexBufferCreateInfo vbCreateInfo;
			vbCreateInfo.memoryAccess = RSMemoryAccess::Write_Only;
			vbCreateInfo.usageFlags = RSResourceUsage_Dynamic;
			this->CreateVertexBuffer(0, vbCreateInfo, initDesc.verticesStorageCapacity, initDesc.verticesDataSize, &(this->_vertexBufferState));
		}

		this->CreateVertexArrayStateObject();
		
		this->_indexBufferStorage = this->_indexBufferState.bufferStorage;
		this->_vertexBufferStorage = this->_vertexBufferState.bufferStorage;

		return true;
	}
	
	
	bool Base2DRectGeometryManager::CheckAvailableSpace(Size_t geometrySize, GeometryDataType geometryType) const
	{
		Uint32 verticesFreeSpace = this->_vertexBufferState.elementsNumLimit - this->_vertexBufferState.elementsNum;
		Uint32 indicesFreeSpace = this->_indexBufferState.elementsNumLimit - this->_indexBufferState.elementsNum;
		
		Size_t requiredVerticesNum = (geometryType == GeometryDataType::Indexed) ? geometrySize * 4 : geometrySize * 6;
		Size_t requiredIndicesNum = (geometryType == GeometryDataType::Indexed) ? geometrySize * 6 : 0;
		
		return (verticesFreeSpace >= requiredVerticesNum) && (indicesFreeSpace >= requiredIndicesNum);
	}

	
	bool Base2DRectGeometryManager::ReserveIndexedGeometryStorage(Size_t geometrySize, Base2DRectGeometry* geometry)
	{
		if (geometrySize == 0)
			return false;
		
		if (!this->_ReserveIndexed(truncate_cast<Uint32>(geometrySize), &(geometry->dataStorageInfo)))
			return false;
		
		geometry->geometryManager = this;
		geometry->flags = 0;

		return true;
	}

	
	bool Base2DRectGeometryManager::ReserveNonIndexedGeometryStorage(Size_t geometrySize, Base2DRectGeometry* geometry)
	{
		if (geometrySize == 0)
			return false;

		if (!this->_ReserveNonIndexed(truncate_cast<Uint32>(geometrySize), &(geometry->dataStorageInfo)))
			return false;
		
		geometry->geometryManager = this;
		geometry->flags = 0;

		return true;
	}


	bool Base2DRectGeometryManager::_ReserveIndexed(Uint32 geometrySize, Base2DRectGeometryDataStorageInfo* geometryDataStorageInfo)
	{
		if (!this->CheckAvailableSpace(geometrySize, GeometryDataType::Indexed))
			return false;
		
		const Uint32 indicesNum = geometrySize * 6;
		const Uint32 verticesNum = geometrySize * 4;
		const Uint32 indicesSize = indicesNum * this->_indexBufferState.elementSize;
		const Uint32 verticesSize = verticesNum * this->_vertexBufferState.elementSize;

		if (!this->_vertexBufferStorage->CheckFreeSpace(verticesSize) || !this->_indexBufferStorage->CheckFreeSpace(indicesSize))
			return false;
		
		bool iballocRes = this->_indexBufferStorage->Allocate(indicesSize, &(geometryDataStorageInfo->indicesMemory));
		ExsDebugAssert( iballocRes );

		bool vballocRes = this->_vertexBufferStorage->Allocate(verticesSize, &(geometryDataStorageInfo->verticesMemory));
		ExsDebugAssert( vballocRes );
		
		geometryDataStorageInfo->indicesElementCapacity = indicesNum;
		geometryDataStorageInfo->indicesRectCapacity = geometrySize;
		geometryDataStorageInfo->verticesElementCapacity = verticesNum;
		geometryDataStorageInfo->verticesRectCapacity = geometrySize;
		geometryDataStorageInfo->geometryManager = this;
		geometryDataStorageInfo->verticesElementOffset = this->_vertexBufferState.elementsNum;
		
		this->_indexBufferState.elementsNum += truncate_cast<Uint32>(indicesNum);
		this->_vertexBufferState.elementsNum += truncate_cast<Uint32>(verticesNum);

		return true;
	}


	bool Base2DRectGeometryManager::_ReserveNonIndexed(Uint32 geometrySize, Base2DRectGeometryDataStorageInfo* geometryDataStorageInfo)
	{
		if (!this->CheckAvailableSpace(geometrySize, GeometryDataType::Non_Indexed))
			return false;
		
		const Uint32 verticesNum = geometrySize * 6;
		const Uint32 verticesSize = verticesNum * this->_vertexBufferState.elementSize;

		if (!this->_vertexBufferStorage->CheckFreeSpace(verticesSize))
			return false;

		bool vballocRes = this->_vertexBufferStorage->Allocate(verticesSize, &(geometryDataStorageInfo->verticesMemory));
		ExsDebugAssert( vballocRes );

		geometryDataStorageInfo->indicesElementCapacity = 0;
		geometryDataStorageInfo->indicesRectCapacity = 0;
		geometryDataStorageInfo->verticesElementCapacity = verticesNum;
		geometryDataStorageInfo->verticesRectCapacity = geometrySize;
		geometryDataStorageInfo->geometryManager = this;
		geometryDataStorageInfo->verticesElementOffset = this->_vertexBufferState.elementsNum;
		
		this->_vertexBufferState.elementsNum += truncate_cast<Uint32>(verticesNum);

		return true;
	}


	void* Base2DRectGeometryManager::_RetrieveIndicesUpdatePtr(const Base2DRectGeometry& geometry, Size_t rectOffset, Size_t rectsNum)
	{
		const Uint32 indicesPerRect = 6;
		const auto& dataStorageInfo = geometry.dataStorageInfo;

		if (rectOffset >= dataStorageInfo.indicesRectCapacity)
			return nullptr;

		rectsNum = stdx::get_min_of(dataStorageInfo.indicesRectCapacity - rectOffset, rectsNum);

		Size_t memoryOffset = rectOffset * indicesPerRect * this->_indexBufferState.elementSize;
		Size_t memorySize = rectsNum * indicesPerRect * this->_indexBufferState.elementSize;

		return this->_indexBufferStorage->UpdateDynamic(dataStorageInfo.indicesMemory, RSMemoryRange(memoryOffset, memorySize));
	}

	
	void* Base2DRectGeometryManager::_RetrieveVerticesUpdatePtr(const Base2DRectGeometry& geometry, Size_t rectOffset, Size_t rectsNum)
	{
		const Uint32 verticesPerRect = (geometry.type == GeometryDataType::Indexed) ? 4 : 6;
		const auto& dataStorageInfo = geometry.dataStorageInfo;

		if (rectOffset >= dataStorageInfo.verticesRectCapacity)
			return nullptr;

		rectsNum = stdx::get_min_of(dataStorageInfo.verticesRectCapacity - rectOffset, rectsNum);

		Size_t memoryOffset = rectOffset * verticesPerRect * this->_vertexBufferState.elementSize;
		Size_t memorySize = rectsNum * verticesPerRect * this->_vertexBufferState.elementSize;

		return this->_vertexBufferStorage->UpdateDynamic(dataStorageInfo.verticesMemory, RSMemoryRange(memoryOffset, memorySize));
	}


}
