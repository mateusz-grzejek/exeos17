
#ifndef __Exs_Engine_RSCommonDefs_H__
#define __Exs_Engine_RSCommonDefs_H__

#include "../Prerequisites.h"


namespace Exs
{


	typedef Uint64 RSObjectUID;


	template <class T>
	class RSObjectLibraryMap
	{
	private:
		struct EntryID
		{
			RSObjectUID uid;

			std::string name;
		};

		struct EntryIDCompare
		{
			bool operator()(const EntryID& lhs, const EntryID& rhs) const
			{
				return lhs.uid < rhs.uid;
			}

			bool operator()(const EntryID& lhs, RSObjectUID rhs) const
			{
				return lhs.uid < rhs;
			}

			bool operator()(const EntryID& lhs, const char* rhs) const
			{
				return lhs.name < rhs;
			}

			bool operator()(const EntryID& lhs, const std::string& rhs) const
			{
				return lhs.name < rhs;
			}
		};

		typedef RSHandle<T> ObjectHandle;
		typedef std::map<EntryID, ObjectHandle, EntryIDCompare> InternalMap;

	private:
		InternalMap  _internalMap;

	public:
		RSObjectLibraryMap()
		{ }

		void Add(RSObjectUID uid, const char* name, const RSHandle<T>& handle)
		{
			auto objref = this->_internalMap.find();
		}

		RSHandle<T> Get(RSObjectUID uid) const
		{

		}

		RSHandle<T> Get(const char* name) const
		{

		}

		RSHandle<T> Get(const std::string& name) const
		{

		}
	};


}


#endif /* __Exs_Engine_RSCommonDefs_H__ */
