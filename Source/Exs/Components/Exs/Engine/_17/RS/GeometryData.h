
#ifndef __Exs_Enigne_GeometryData_H__
#define __Exs_Enigne_GeometryData_H__

#include "GeometryBase.h"


namespace Exs
{
	

	ExsDeclareRSClassObjHandle(ShaderResourceView);

	
	///<summary>
	///</summary>
	enum class GeometryDataType : Enum
	{
		//
		Indexed,

		//
		Non_Indexed
	};

	
	///<summary>
	/// Stores basic info about geometry data of objects.
	///</summary>
	struct GeometryDataStorageBaseInfo
	{
		// Geometry manager which owns buffers used by this geometry.
		GeometryManager* geometryManager;
	};

	
	///<summary>
	/// Represents geometry chunk - a group of vertices and optional indices (in case of indexed geometry)
	/// that can bee seen as a whole physical renderable entity. Geometry is consisted of one or more batches
	/// which are used to render the geometry.
	///</summary>
	template <class Data_storage_info_t>
	struct Geometry
	{
		// Manager which stores data used by this geometry.
		GeometryManager* geometryManager = nullptr;

		// Basic info about data storage allocated for this geometry.
		Data_storage_info_t dataStorageInfo;

		// Type of this geometry.
		GeometryDataType type;

		// Additional, geometry-specific flags
		stdx::mask<Enum> flags = 0;
	};

	
	///<summary>
	///</summary>
	template <class Batch_t>
	class GeometryBatchCache
	{
	public:
		typedef std::vector<Batch_t> BatchList;

	private:
		BatchList  _batchList;

	public:
		GeometryBatchCache()
		{ }
		
		///<summary>
		///</summary>
		void AddBatch(const Batch_t& batch)
		{
			this->_batchList.push_back(batch);
		}
		
		///<summary>
		///</summary>
		void Reset()
		{
			this->_batchList.clear();
		}
		
		///<summary>
		///</summary>
		const BatchList& GetBatchList() const
		{
			return this->_batchList;
		}
	};


}


#endif /* __Exs_Enigne_GeometryData_H__ */
