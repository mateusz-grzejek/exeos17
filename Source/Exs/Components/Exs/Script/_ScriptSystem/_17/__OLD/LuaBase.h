
#pragma once

#ifndef __Exs_ScriptSystem_LuaBase_H__
#define __Exs_ScriptSystem_LuaBase_H__

#include "Prerequisites.h"


namespace Exs
{


	namespace Lua
	{


		enum class LocalOwnershipTransfer : Enum
		{
			// Creates new copy of an object in Lua.
			Copy,

			// Creates new instance in Lua, that is move-constructed from given object.
			Move,

			// Object is shared with Lua, but C++ retains full ownership.
			Share
		};


		template <class T>
		inline const char* GetTypeMetatableName()
		{
			static std::string metatable_name { 32 };

			if(metatable_name.empty())
			{
				metatable_name = typeid(T).name();
				metatable_name += ".Metatable";
			}

			return metatable_name.data();
		}


		template <class T>
		inline T* GetThisPointer(lua_State* lua, Size_t argsCount)
		{
			int this_ptr_index = -static_cast<int>(argsCount) - 1;
			return GetValue<T>(lua, this_ptr_index);
		}
		
		
		template <int Lua_type>
		struct DataTypeByTag;
		
		template <>
		struct DataTypeByTag<LUA_TBOOLEAN>
		{
			typedef bool Type;
		};


	}


}


#endif /* __Exs_ScriptSystem_LuaBase_H__ */
