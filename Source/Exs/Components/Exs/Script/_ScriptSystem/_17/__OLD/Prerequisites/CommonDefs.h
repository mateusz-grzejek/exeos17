
#pragma once

#ifndef __Exs_ScriptSystem_CommonDefs_H__
#define __Exs_ScriptSystem_CommonDefs_H__


namespace Exs
{


	typedef int(*LuaCallback)(lua_State*);


	typedef DefaultHashCode<const char*> ScriptID;
	typedef DefaultHashCode<const char*> ScriptsGroupID;


	enum : TraceCategoryID
	{
		TRC_Script_System = 0x1C00,
		TRC_Script_Engine
	};
	

	EXS_TRACE_SET_CATEGORY_NAME(TRC_Script_System, "ScriptSystem");
	EXS_TRACE_SET_CATEGORY_NAME(TRC_Script_Engine, EXS_SCRIPT_ENGINE_NAME);



	template <class T>
	struct LuaDataType
	{
		typedef T* Type;
	};

	template <>
	struct LuaDataType<void*>
	{
		typedef void* Type;
	};

	template <>
	struct LuaDataType<const char*>
	{
		typedef const char* Type;
	};

	template <>
	struct LuaDataType<bool>
	{
		typedef bool Type;
	};

	template <>
	struct LuaDataType<Int32>
	{
		typedef Int32 Type;
	};

	template <>
	struct LuaDataType<Uint32>
	{
		typedef Uint32 Type;
	};

	template <>
	struct LuaDataType<Int64>
	{
		typedef Int64 Type;
	};

	template <>
	struct LuaDataType<Uint64>
	{
		typedef Uint64 Type;
	};

	template <>
	struct LuaDataType<float>
	{
		typedef float Type;
	};

	template <>
	struct LuaDataType<double>
	{
		typedef double Type;
	};


}


#endif /* __Exs_ScriptSystem_CommonDefs_H__ */
