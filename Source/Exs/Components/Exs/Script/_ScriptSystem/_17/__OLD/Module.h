
#pragma once

#ifndef __Exs_ScriptSystem_Module_H__
#define __Exs_ScriptSystem_Module_H__

#include "Prerequisites.h"


namespace Exs
{


	/*
	Module:
	- name
	- id

	Consists of:
	- types (structs/classes)
	- enums
	- global variables (also: unnamed enums here!)
	*/


}


#endif /* __Exs_ScriptSystem_Module_H__ */
