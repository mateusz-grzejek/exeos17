
#pragma once

#ifndef __Exs_ScriptSystem_LuaClass_H__
#define __Exs_ScriptSystem_LuaClass_H__

#include "LuaFunctions.h"


namespace Exs
{


	namespace Lua
	{


		template <class T>
		inline void* AllocNewObject(lua_State* lua)
		{
			const size_t alloc_size = sizeof(T);

			const char* metatable_name = GetTypeMetatableName<T>();
			void* object_ptr = NewObject(lua, alloc_size, metatable_name);

			return object_ptr;
		}


		namespace ComponentsWrappers
		{


			template <class F>
			struct AssignmentOperator;


			template <class T, class U>
			struct AssignmentOperator<T&(T::*)(U)>
			{
			};


			template <class F>
			struct Constructor;


			template <class T, class R, class... Args>
			struct Constructor<R*(T*, Args...)>
			{
				typedef FunctionArgs<Args...> ArgsWrapper;
				typedef typename ArgsWrapper::SeqType ArgsSequence;

				static const Size_t Args_num = ArgsWrapper::Args_num;

				template <Int... Seq>
				static R* Execute(R*(*funcPtr)(T*, Args...), T* memPtr, const std::tuple<Args...>& args, std::integer_sequence<Int, Seq...>)
				{
					return (funcPtr)(memPtr, std::get<Seq>(args)...);
				}

				static void Construct(lua_State* lua, R*(*funcPtr)(void*, Args...))
				{
					void* mem_ptr = Lua::AllocNewObject<R>(lua);
					R* obj_ptr = reinterpret_cast<R*>(mem_ptr);
					auto args = ArgsWrapper::GetArgs(lua, 0, ArgsSequence());
					Execute(funcPtr, obj_ptr, args, ArgsSequence());
					Lua::PushValue<R>(lua, obj_ptr);
				}
			};


			template <class F>
			struct Destructor;


			template <>
			struct Destructor<void(void*)>
			{
				typedef FunctionArgs<void*> ArgsWrapper;
				typedef ArgsWrapper::SeqType ArgsSequence;

				static const Size_t Args_num = ArgsWrapper::Args_num;

				static void Execute(void(*funcPtr)(void*), const std::tuple<void*>& args)
				{
					return (funcPtr)(std::get<0>(args));
				}

				static void Destroy(lua_State* lua, void(*funcPtr)(void*))
				{
					auto args = ArgsWrapper::GetArgs(lua, 0, ArgsSequence());
					Execute(funcPtr, args);
				}
			};

		}


	}
	

	namespace Internal
	{


		template <class Constructor_type, Constructor_type Constructor_ptr>
		struct ConstructorWrapper
		{
			static int Wrapper(lua_State* lua)
			{
				Lua::ComponentsWrappers::Constructor<Constructor_type>::Construct(lua, Constructor_ptr);
				return 1;
			}
		};


		template <class Destructor_type, Destructor_type Destructor_ptr>
		struct DestructorWrapper
		{
			static int Wrapper(lua_State* lua)
			{
				Lua::ComponentsWrappers::Destructor<Destructor_type>::Destroy(lua, Destructor_ptr);
				return 1;
			}
		};


	}


}

	
#define ExsScriptConstructor(constructor) \
	Internal::ConstructorWrapper<decltype(constructor), constructor>::Wrapper

#define ExsScriptDestructor(destructor) \
	Internal::DestructorWrapper<decltype(destructor), destructor>::Wrapper


#endif /* __Exs_ScriptSystem_LuaClass_H__ */
