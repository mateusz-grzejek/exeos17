
#include "Precompiled.h"
#include <Exs/ScriptSystem/ScriptEngineInterface.h>


namespace Exs
{


	int LuaDumpCodeWriter(lua_State* lua, const void* data, size_t length, void* arg);




	ScriptEngineInterface::ScriptEngineInterface(ScriptEngineState* state)
	:	_state(state)
	{ }


	ScriptEngineInterface::~ScriptEngineInterface()
	{ }


	void ScriptEngineInterface::Initialize()
	{
		if(this->_state->lua_state != nullptr)
		{
			ExsTraceError(TRC_Script_System, "Engine state is already initialized.");
			return;
		}

		auto& lua_state = this->_state->lua_state;

		lua_state = luaL_newstate();
		
		Lua::OpenLibrary(lua_state, luaopen_base);
		Lua::OpenLibrary(lua_state, luaopen_debug);
		Lua::OpenLibrary(lua_state, luaopen_io);
		Lua::OpenLibrary(lua_state, luaopen_math);
		Lua::OpenLibrary(lua_state, luaopen_os);
		Lua::OpenLibrary(lua_state, luaopen_package);
		Lua::OpenLibrary(lua_state, luaopen_string);
		Lua::OpenLibrary(lua_state, luaopen_table);
		Lua::OpenLibrary(lua_state, luaopen_utf8);
	}


	void ScriptEngineInterface::Release()
	{
		if(this->_state->lua_state == nullptr)
		{
			ExsTraceError(TRC_Script_System, "Engine state is not initialized.");
			return;
		}

		lua_close(this->_state->lua_state);
		this->_state->lua_state = nullptr;
	}


	Result ScriptEngineInterface::CompileSource(const char* name, const char* source, Size_t length, DynamicDataArray<Byte>& output)
	{
		if(!this->_check_state())
			return RSC_Invalid_State;

		lua_State* lua_state = this->_state->lua_state;

		int load_result = luaL_loadbufferx(lua_state, source, length, name, "t");

		if(!this->_result_check(load_result))
			return RSC_Err_Failed;

		lua_dump(lua_state, LuaDumpCodeWriter, &output, 0);
		lua_pop(lua_state, 1);

		return RSC_Success;
	}

	
	Result ScriptEngineInterface::ExecuteCompiledScript(const char* name, const Byte* binary, Size_t length)
	{
		if(!this->_check_state())
			return RSC_Invalid_State;

		lua_State* lua_state = this->_state->lua_state;

		int load_result = luaL_loadbufferx(lua_state, reinterpret_cast<const char*>(binary), length, name, "b");
		
		if(!this->_result_check(load_result))
			return RSC_Err_Failed;

		int call_result = lua_pcallk(lua_state, 0, 0, 0, 0, nullptr);
		
		if(!this->_result_check(call_result))
			return RSC_Err_Failed;

		return RSC_Success;
	}

	
	Result ScriptEngineInterface::ExecuteTextScript(const char* name, const char* source, Size_t length)
	{
		if(!this->_check_state())
			return RSC_Invalid_State;

		lua_State* lua_state = this->_state->lua_state;
		
		int load_result = luaL_loadbufferx(lua_state, source, length, name, "t");
		
		if(!this->_result_check(load_result))
			return RSC_Err_Failed;

		int call_result = lua_pcallk(lua_state, 0, 0, 0, 0, nullptr);
		
		if(!this->_result_check(call_result))
			return RSC_Err_Failed;

		return RSC_Success;
	}


	void ScriptEngineInterface::RegisterLib(const char* name, const luaL_Reg* reg)
	{
		if(!this->_check_state())
			return;

		lua_State* lua_state = this->_state->lua_state;

		luaL_newlib(lua_state, reg);
		lua_setglobal(lua_state, name);
	}


	void ScriptEngineInterface::RegisterType(const char* libName, const char* metaName, const luaL_Reg* lib, const luaL_Reg* meta)
	{
		if(!this->_check_state())
			return;

		lua_State* lua_state = this->_state->lua_state;

		luaL_newmetatable(lua_state, metaName);

		lua_pushvalue(lua_state, -1);
		lua_setfield(lua_state, -2, "__index");

		luaL_setfuncs(lua_state, meta, 0);
		luaL_newlib(lua_state, lib);

		lua_setglobal(lua_state, libName);
	}


	bool ScriptEngineInterface::_check_state()
	{
		if(this->_state == nullptr)
			return false;

		if(this->_state->lua_state == nullptr)
			return false;
		
		return true;
	}


	void ScriptEngineInterface::_print_error()
	{
		lua_State* lua_state = this->_state->lua_state;

		size_t str_length = Invalid_Length;
		const char* error_str = luaL_checklstring(lua_state, -1, &str_length);

		if(error_str == nullptr)
		{
			ExsTraceError(TRC_Script_System, "Critical error: could not found error string on the stack.");
			return;
		}

		ExsTraceError(TRC_Script_Engine, "%s", error_str);

		ExsTraceInfo(TRC_Script_Engine, "Stack: %u (%s).", lua_gettop(lua_state), EXS_FUNC);
	}


	bool ScriptEngineInterface::_result_check(int result)
	{
		if(result == LUA_OK)
			return true;

		this->_print_error();

		return false;
	}




	int LuaDumpCodeWriter(lua_State* lua, const void* data, size_t length, void* arg)
	{
		DynamicDataArray<Byte>* code_array = reinterpret_cast<DynamicDataArray<Byte>*>(arg);
		code_array->Append(reinterpret_cast<const Byte*>(data), length);

		return 0;
	}


}
