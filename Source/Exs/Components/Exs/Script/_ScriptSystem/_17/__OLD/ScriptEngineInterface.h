
#pragma once

#ifndef __Exs_ScriptSystem_ScriptEngineInterface_H__
#define __Exs_ScriptSystem_ScriptEngineInterface_H__

#include "Prerequisites.h"
#include <Exs/Core/DataArray.h>


namespace Exs
{


	class EXS_LIBCLASS_SCRIPTSYSTEM ScriptEngineInterface
	{
	private:
		ScriptEngineState*	_state;

	public:
		ScriptEngineInterface(ScriptEngineState* state);
		~ScriptEngineInterface();

		void Initialize();

		void Release();
		
		Result CompileSource(const char* name, const char* source, Size_t length, DynamicDataArray<Byte>& output);
		
		Result ExecuteCompiledScript(const char* name, const Byte* binary, Size_t length);
		
		Result ExecuteTextScript(const char* name, const char* source, Size_t length);

		void RegisterLib(const char* name, const luaL_Reg* reg);

		void RegisterType(const char* libName, const char* metaName, const luaL_Reg* lib, const luaL_Reg* meta);

	private:
		bool _check_state();
		void _print_error();
		bool _result_check(int result);
	};


}


#endif /* __Exs_ScriptSystem_ScriptEngineInterface_H__ */
