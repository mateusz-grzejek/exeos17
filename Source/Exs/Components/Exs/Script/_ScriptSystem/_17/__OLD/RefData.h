
#pragma once

#ifndef __Exs_ScriptSystem_RefData_H__
#define __Exs_ScriptSystem_RefData_H__

#include "Prerequisites.h"


namespace Exs
{


	class ScriptSystem;


	class RefData
	{
	public:
		enum class Color
		{
			White,
			Black
		};

		std::string name;
		Uint64 uid;

		int value = 0;
		double multiplier = 1.0;

	private:
		std::vector<std::string>	_data;

		static int _cnt;

	public:
		RefData(const char* name = "default");
		~RefData();

		void Add(const char* str);
		void AddTwo(const char* s1, const char* s2);
		void Print();

		static int GetCnt();

		Size_t GetSize() const;

		static void RegisterType(ScriptSystem* ssystem);
	};


	inline int RefData::GetCnt()
	{
		return _cnt;
	}

	inline Size_t RefData::GetSize() const
	{
		return this->_data.size();
	}


}


#endif /* __Exs_ScriptSystem_RefData_H__ */
