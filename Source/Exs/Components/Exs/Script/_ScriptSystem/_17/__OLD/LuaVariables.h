
#pragma once

#ifndef __Exs_ScriptSystem_LuaVariables_H__
#define __Exs_ScriptSystem_LuaVariables_H__

#include "LuaBase.h"


namespace Exs
{


	namespace Lua
	{


		namespace ComponentsWrappers
		{


			template <class Variable_type>
			struct MemberVariable;


			template <class T, class V>
			struct MemberVariable<V T::*>
			{
				static void Get(lua_State* lua, V T::* varPtr)
				{
					// Stack: [..., userdata]

					T* this_ptr = GetThisPointer<T>(lua, 0);
					auto value = this_ptr->*(varPtr);
					PushValue<V>(lua, value);
				}

				static void Set(lua_State* lua, V T::* varPtr)
				{
					// Stack: [..., userdata, value]
			
					T* this_ptr = GetThisPointer<T>(lua, 1);
					auto new_value = GetValue<V>(lua, -1);
					this_ptr->*(varPtr) = new_value;
				}
			};


		}


	}



	namespace Internal
	{


		template <class Var_type, Var_type Var_ptr>
		struct MemberVariableGetterWrapper
		{
			static int Wrapper(lua_State* lua)
			{
				Lua::ComponentsWrappers::MemberVariable<Var_type>::Get(lua, Var_ptr);
				return 1;
			}
		};


		template <class Var_type, Var_type Var_ptr>
		struct MemberVariableSetterWrapper
		{
			static int Wrapper(lua_State* lua)
			{
				Lua::ComponentsWrappers::MemberVariable<Var_type>::Set(lua, Var_ptr);
				return 0;
			}
		};


	}


}


#define ExsScriptMemberVariableGetter(variable) \
	Internal::MemberVariableGetterWrapper<decltype(variable), variable>::Wrapper

#define ExsScriptMemberVariableSetter(variable) \
	Internal::MemberVariableSetterWrapper<decltype(variable), variable>::Wrapper


#endif /* __Exs_ScriptSystem_LuaVariables_H__ */
