
#pragma once

#ifndef __Exs_ScriptSystem_LuaReference_H__
#define __Exs_ScriptSystem_LuaReference_H__

#include "LuaBase.h"


namespace Exs
{


	namespace Lua
	{


		typedef int ReferenceKey;


		class Reference
		{
		public:
			Reference();

		private:
			ReferenceKey	_ref_key;
		};


		class ActiveRef
		{
		};


	}


}


#endif /* __Exs_ScriptSystem_LuaReference_H__ */
