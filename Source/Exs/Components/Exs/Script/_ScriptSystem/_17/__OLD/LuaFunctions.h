
#pragma once

#ifndef __Exs_ScriptSystem_LuaFunctions_H__
#define __Exs_ScriptSystem_LuaFunctions_H__

#include "LuaBase.h"


namespace Exs
{

	
	namespace Lua
	{


		template <class T>
		inline auto GetFunctionArgument(lua_State* lua, Size_t totalArgsCount, Int argNum) -> decltype(GetValue<T>(lua, 0))
		{
			Int argument_index = -static_cast<Int>(totalArgsCount) + argNum;
			return GetValue<T>(lua, argument_index);
		}


		namespace ComponentsWrappers
		{

			///<summary>
			///</summary>
			template <class... Args>
			struct FunctionArgs
			{
				static const Size_t Args_num = sizeof...(Args);

				typedef std::make_integer_sequence<Int, Args_num> SeqType;

				template <Int Arg_num>
				struct Arg
				{
					typedef typename std::tuple_element< Arg_num, std::tuple<Args...> >::type Type;
				};

				///<summary>
				/// Retrieves arguments from the lua stack and returns them packed into a tuple. 
				///</summary>
				///
				///<param name="lua"> Lua state object. </param>
				///<param name="stackHeadSize"> Number of elements on the stack *after* last argument. </param>
				///<param name="sequence"> Generated integer sequence, used for proper pack expansion. </param>
				template <Int... Seq>
				static std::tuple<Args...> GetArgs(lua_State* lua, Int stackHeadSize, std::integer_sequence<Int, Seq...> sequence)
				{
					return std::tuple<Args...>(GetFunctionArgument<Args>(lua, Args_num, Seq - stackHeadSize)...);
				}
			};
	

			template <class T, class R, class... Args>
			struct MemberFunction
			{
				template <Int... Seq>
				static R Execute(R(T::*funcPtr)(Args...), T* objPtr, const std::tuple<Args...>& args, std::integer_sequence<Int, Seq...>)
				{
					return (objPtr->*(funcPtr))(std::get<Seq>(args)...);
				}
		
				template <Int... Seq>
				static R Execute(R(T::*funcPtr)(Args...) const, const T* objPtr, const std::tuple<Args...>& args, std::integer_sequence<Int, Seq...>)
				{
					return (objPtr->*(funcPtr))(std::get<Seq>(args)...);
				}
			};


			template <class R, class... Args>
			struct StaticFunction
			{
				template <Int... Seq>
				static R Execute(R(*funcPtr)(Args...), const std::tuple<Args...>& args, std::integer_sequence<Int, Seq...>)
				{
					return (funcPtr)(std::get<Seq>(args)...);
				}
			};
	

			template <class Func>
			struct Function;
	

			template <class T, class... Args>
			struct Function<void(T::*)(Args...)> : public MemberFunction<T, void, Args...>
			{
				typedef FunctionArgs<Args...> ArgsWrapper;
				typedef typename ArgsWrapper::SeqType ArgsSequence;

				static const Size_t Args_num = ArgsWrapper::Args_num;

				static int Call(lua_State* lua, void(T::*funcPtr)(Args...))
				{
					T* this_ptr = GetThisPointer<T>(lua, Args_num);
					auto args = ArgsWrapper::GetArgs(lua, 0, ArgsSequence());
					Execute(funcPtr, this_ptr, args, ArgsSequence());
			
					return 0;
				}
			};


			template <class T, class R, class... Args>
			struct Function<R(T::*)(Args...)> : public MemberFunction<T, R, Args...>
			{
				typedef FunctionArgs<Args...> ArgsWrapper;
				typedef typename ArgsWrapper::SeqType ArgsSequence;

				static const Size_t Args_num = ArgsWrapper::Args_num;

				static int Call(lua_State* lua, R(T::*funcPtr)(Args...))
				{
					T* this_ptr = GetThisPointer<T>(lua, Args_num);
					auto args = ArgsWrapper::GetArgs(lua, 0, ArgsSequence());
					auto result = Execute(funcPtr, this_ptr, args, ArgsSequence());
					PushValue<R>(lua, result);
			
					return 1;
				}
			};


			template <class T, class... Args>
			struct Function<void(T::*)(Args...) const> : public MemberFunction<T, void, Args...>
			{
				typedef FunctionArgs<Args...> ArgsWrapper;
				typedef typename ArgsWrapper::SeqType ArgsSequence;

				static const Size_t Args_num = ArgsWrapper::Args_num;

				static int Call(lua_State* lua, void(T::*funcPtr)(Args...) const)
				{
					T* this_ptr = GetThisPointer<T>(lua, Args_num);
					auto args = ArgsWrapper::GetArgs(lua, 0, ArgsSequence());
					Execute(funcPtr, this_ptr, args, ArgsSequence());
			
					return 0;
				}
			};


			template <class T, class R, class... Args>
			struct Function<R(T::*)(Args...) const> : public MemberFunction<T, R, Args...>
			{
				typedef FunctionArgs<Args...> ArgsWrapper;
				typedef typename ArgsWrapper::SeqType ArgsSequence;

				static const Size_t Args_num = ArgsWrapper::Args_num;

				static int Call(lua_State* lua, R(T::*funcPtr)(Args...) const)
				{
					T* this_ptr = GetThisPointer<T>(lua, Args_num);
					auto args = ArgsWrapper::GetArgs(lua, 0, ArgsSequence());
					auto result = Execute(funcPtr, this_ptr, args, ArgsSequence());
					PushValue<R>(lua, result);
			
					return 1;
				}
			};
	

			template <class... Args>
			struct Function<void(Args...)> : public StaticFunction<void, Args...>
			{
				typedef FunctionArgs<Args...> ArgsWrapper;
				typedef typename ArgsWrapper::SeqType ArgsSequence;

				static const Size_t Args_num = ArgsWrapper::Args_num;

				static int Call(lua_State* lua, void(*funcPtr)(Args...))
				{
					auto args = ArgsWrapper::GetArgs(lua, 0, ArgsSequence());
					Execute(funcPtr, args, ArgsSequence());
			
					return 0;
				}
			};


			template <class R, class... Args>
			struct Function<R(Args...)> : public StaticFunction<R, Args...>
			{
				typedef FunctionArgs<Args...> ArgsWrapper;
				typedef typename ArgsWrapper::SeqType ArgsSequence;

				static const Size_t Args_num = ArgsWrapper::Args_num;

				static int Call(lua_State* lua, R(*funcPtr)(Args...))
				{
					auto args = ArgsWrapper::GetArgs(lua, 0, ArgsSequence());
					auto result = Execute(funcPtr, args, ArgsSequence());
					PushValue<R>(lua, result);
			
					return 1;
				}
			};


		}


	}


	namespace Internal
	{


		template <class Function_type, Function_type Function_ptr>
		struct FunctionWrapper
		{
			static int Wrapper(lua_State* lua)
			{
				return Lua::ComponentsWrappers::Function<Function_type>::Call(lua, Function_ptr);
			}
		};


	}


}


#define ExsScriptFunction(function) \
	Internal::FunctionWrapper<decltype(function), function>::Wrapper


#endif /* __Exs_ScriptSystem_LuaFunctions_H__ */
