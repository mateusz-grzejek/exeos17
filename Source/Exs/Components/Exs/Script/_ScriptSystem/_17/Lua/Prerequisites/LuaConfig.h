
#ifndef __Exs_ScriptSystem_LuaConfig_H__
#define __Exs_ScriptSystem_LuaConfig_H__

#include "../Prerequisites.h"

extern "C"
{
#include <Lua/lua.h>
#include <Lua/lualib.h>
#include <Lua/lauxlib.h>
}


namespace Exs
{
namespace Lua
{


	static constexpr auto Meta_Cls_Non_Static_Variables_RO_Index_Name = "__member_vars_ro";
	static constexpr auto Meta_Cls_Non_Static_Variables_RW_Index_Name = "__member_vars_rw";
	static constexpr auto Meta_Cls_Static_Variables_RO_Index_Name = "__static_vars_ro";
	static constexpr auto Meta_Cls_Static_Variables_RW_Index_Name = "__static_vars_rw";


}
}


#endif /* __Exs_ScriptSystem_LuaConfig_H__ */
