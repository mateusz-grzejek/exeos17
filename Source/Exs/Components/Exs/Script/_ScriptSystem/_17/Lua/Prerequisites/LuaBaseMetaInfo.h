
#ifndef __Exs_ScriptSystem_LuaBaseMetaInfo_H__
#define __Exs_ScriptSystem_LuaBaseMetaInfo_H__


namespace Exs
{
namespace Lua
{


	template <typename T>
	struct TypeMetaInfo
	{
		static const char* GetClsMetatableName()
		{
			static const std::string metatableName { std::string("lmeta_cls_") + GetTypeName<T>()  };
			return metatableName.c_str();
		}

		static const char* GetInternalMetatableName()
		{
			static const std::string metatableName { std::string("lmeta_internal_") + GetTypeName<T>() };
			return metatableName.c_str();
		}

		static const char* GetProxyMetatableName()
		{
			static const std::string metatableName{ std::string("lmeta_proxy_") + GetTypeName<T>() };
			return metatableName.c_str();
		}
	};

	template <typename T>
	struct TypeMetaInfo<const T>
	{
		static const char* GetClsMetatableName()
		{
			static const std::string metatableName{ std::string("lmeta_ref_const_") + GetTypeName<T>() };
			return metatableName.c_str();
		}

		static const char* GetInternalMetatableName()
		{
			static const std::string metatableName{ std::string("lmeta_internal_const_") + GetTypeName<T>() };
			return metatableName.c_str();
		}

		static const char* GetProxyMetatableName()
		{
			static const std::string metatableName{ std::string("lmeta_proxy_const_") + GetTypeName<T>() };
			return metatableName.c_str();
		}
	};


}
}


#endif /* __Exs_ScriptSystem_LuaBaseMetaInfo_H__ */
