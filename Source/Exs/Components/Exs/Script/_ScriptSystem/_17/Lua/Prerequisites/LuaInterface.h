
#pragma once

#ifndef __Exs_ScriptSystem_LuaInterface_H__
#define __Exs_ScriptSystem_LuaInterface_H__


namespace Exs
{
namespace Lua
{


	template <typename T>
	struct ValueType
	{
		typedef typename UserdataValueType<T>::Type Type;
	};


	namespace Internal
	{

		template <typename T, RefObjectTransferPolicy TP>
		struct PushWrapper
		{
			static bool Push(lua_State* luaState, T value)
			{
				return PushUserdataValue<typename UserdataValueType<T>::Type, TP>::Push(luaState, value);
			}
		};

		template <typename T>
		struct PushWrapper<T&, RefObjectTransferPolicy::Ref_Only>
		{
			static bool Push(lua_State* luaState, T value)
			{
				return PushUserdataValue<typename UserdataValueType<T&>::Type, TP>::Push(luaState, value);
			}
		};

		template <typename T>
		struct PushWrapper<const T&, RefObjectTransferPolicy::Ref_Only>
		{
			static bool Push(lua_State* luaState, T value)
			{
				return PushUserdataValue<typename UserdataValueType<const T&>::Type, TP>::Push(luaState, value);
			}
		};

	}
	

	struct LuaInterface
	{
		template <RefObjectTransferPolicy TP, typename T>
		static bool PushValue(lua_State* luaState, T value)
		{
			return Internal::PushWrapper<T, TP>::Push(luaState, value);
		}

		template <typename T>
		static bool PushValue(lua_State* luaState, T value)
		{
			return PushValue<RefObjectTransferPolicy::Default>(luaState, value);
		}

		template <typename T>
		static typename ValueType<T>::Type GetValue(lua_State* luaState, int idx)
		{
			return GetUserdataValue<typename UserdataValueType<T>::Type>::Get(luaState, idx);
		}
	};


#define ExsScriptLuaSetCustomValueType(type, valType) \
	template <> \
	struct ValueType<type> \
	{ \
		typedef valType Type; \
	};


#define ExsScriptLuaDefineCustomGetForBuiltInType(type, luaGetFunction) \
	template <> \
	inline ValueType<type>::Type LuaInterface::GetValue<type>(lua_State* luaState, int idx) \
	{ \
		auto value = luaGetFunction(luaState, idx); \
		return static_cast<type>(value); \
	}


#define ExsScriptLuaDefineCustomPushForBuiltInType(type, luaType, luaPushFunction) \
	namespace Internal \
	{ \
	template <RefObjectTransferPolicy TP> \
	struct PushWrapper<type, TP> \
	{ \
		static bool Push(lua_State* luaState, type value) \
		{ \
			luaPushFunction(luaState, static_cast<luaType>(value)); \
			return true; \
		} \
	}; \
	}


#define ExsScriptLuaSetCustomValueTypeForBuiltInType(type) \
	ExsScriptLuaSetCustomValueType(type, type)

#define ExsScriptLuaDefineCustomGetForIntegerType(type) \
	ExsScriptLuaDefineCustomGetForBuiltInType(type, lua_tointeger)

#define ExsScriptLuaDefineCustomGetForFloatType(type) \
	ExsScriptLuaDefineCustomGetForBuiltInType(type, lua_tonumber)

#define ExsScriptLuaDefineCustomPushForIntegerType(type) \
	ExsScriptLuaDefineCustomPushForBuiltInType(type, lua_Integer, lua_pushinteger)

#define ExsScriptLuaDefineCustomPushForFloatType(type) \
	ExsScriptLuaDefineCustomPushForBuiltInType(type, lua_Number, lua_pushnumber)


	ExsScriptLuaSetCustomValueTypeForBuiltInType(Int8);
	ExsScriptLuaSetCustomValueTypeForBuiltInType(Uint8);
	ExsScriptLuaSetCustomValueTypeForBuiltInType(Int16);
	ExsScriptLuaSetCustomValueTypeForBuiltInType(Uint16);
	ExsScriptLuaSetCustomValueTypeForBuiltInType(Int32);
	ExsScriptLuaSetCustomValueTypeForBuiltInType(Uint32);
	ExsScriptLuaSetCustomValueTypeForBuiltInType(Int64);
	ExsScriptLuaSetCustomValueTypeForBuiltInType(Uint64);
	ExsScriptLuaSetCustomValueTypeForBuiltInType(float);
	ExsScriptLuaSetCustomValueTypeForBuiltInType(double);
	ExsScriptLuaSetCustomValueTypeForBuiltInType(long double);

	ExsScriptLuaDefineCustomGetForIntegerType(Int8);
	ExsScriptLuaDefineCustomGetForIntegerType(Uint8);
	ExsScriptLuaDefineCustomGetForIntegerType(Int16);
	ExsScriptLuaDefineCustomGetForIntegerType(Uint16);
	ExsScriptLuaDefineCustomGetForIntegerType(Int32);
	ExsScriptLuaDefineCustomGetForIntegerType(Uint32);
	ExsScriptLuaDefineCustomGetForIntegerType(Int64);
	ExsScriptLuaDefineCustomGetForIntegerType(Uint64);

	ExsScriptLuaDefineCustomPushForIntegerType(Int8);
	ExsScriptLuaDefineCustomPushForIntegerType(Uint8);
	ExsScriptLuaDefineCustomPushForIntegerType(Int16);
	ExsScriptLuaDefineCustomPushForIntegerType(Uint16);
	ExsScriptLuaDefineCustomPushForIntegerType(Int32);
	ExsScriptLuaDefineCustomPushForIntegerType(Uint32);
	ExsScriptLuaDefineCustomPushForIntegerType(Int64);
	ExsScriptLuaDefineCustomPushForIntegerType(Uint64);

	ExsScriptLuaDefineCustomGetForFloatType(float);
	ExsScriptLuaDefineCustomGetForFloatType(double);
	ExsScriptLuaDefineCustomGetForFloatType(long double);

	ExsScriptLuaDefineCustomPushForFloatType(float);
	ExsScriptLuaDefineCustomPushForFloatType(double);
	ExsScriptLuaDefineCustomPushForFloatType(long double);


}
}


#endif /* __Exs_ScriptSystem_LuaInterface_H__ */
