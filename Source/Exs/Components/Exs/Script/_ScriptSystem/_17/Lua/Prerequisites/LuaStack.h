
#ifndef __Exs_ScriptSystem_Lua_LuaStack_H__
#define __Exs_ScriptSystem_Lua_LuaStack_H__


namespace Exs
{
namespace Lua
{


	template <typename T>
	struct DataValueType
	{
		typedef typename UserdataValueType<T>::Type Type;
	};

	template <>
	struct DataValueType<Int32>
	{
		typedef Int32 Type;
	};

	template <>
	struct DataValueType<Uint32>
	{
		typedef Uint32 Type;
	};

	template <>
	struct DataValueType<Int64>
	{
		typedef Int64 Type;
	};

	template <>
	struct DataValueType<Uint64>
	{
		typedef Uint64 Type;
	};

	template <>
	struct DataValueType<float>
	{
		typedef float Type;
	};

	template <>
	struct DataValueType<double>
	{
		typedef double Type;
	};


	template <typename T>
	struct GetFwd
	{
		static typename DataValueType<T>::Type Get(lua_State* luaState, int idx)
		{
			return GetUserdata<T>::Get(luaState, idx);
		}
	};

	template <typename T>
	struct GetFwd<T&>
	{
		static std::reference_wrapper<T> Get(lua_State* luaState, int idx)
		{
			return GetUserdata< ReferenceWrapper<T> >::Get(luaState, idx);
		}
	};

	template <typename T>
	struct GetFwd<const T&>
	{
		static std::reference_wrapper<const T> Get(lua_State* luaState, int idx)
		{
			return GetUserdata< ReferenceWrapper<const T> >::Get(luaState, idx);
		}
	};

	template <typename T>
	struct GetFwd<T*>
	{
		static T* Get(lua_State* luaState, int idx)
		{
			return GetUserdata< PointerWrapper<T> >::Get(luaState, idx);
		}
	};

	template <typename T>
	struct GetFwd<const T*>
	{
		static const T* Get(lua_State* luaState, int idx)
		{
			return GetUserdata< PointerWrapper<const T> >::Get(luaState, idx);
		}
	};


	class Stack
	{
	public:
		template <typename T>
		static T* GetThisPtr(lua_State* luaState, int idx)
		{
			if (auto* userdata = CheckUserdata<T>::Check(luaState, idx))
			{
				return userdata->objectPtr;
			}

			ExsDebugInterrupt();
		}

		// template <typename T>
		// static typename DataValueType<T>::Type Get(lua_State* luaState, int idx)
		// {
		// 	return GetFwd<T>::Get(luaState, idx);
		// }
		
		template <typename T>
		static void Push(lua_State* luaState, T value)
		{
			PushUserdata<T>::Push(luaState, value);
		}

		template <typename T>
		static void PushRef(lua_State* luaState, T& value)
		{
			PushUserdata<T>::Push(luaState, std::ref(value));
		}

		template <typename T>
		static void PushRef(lua_State* luaState, const T& value)
		{
			PushUserdata<T>::Push(luaState, std::cref(value));
		}
	};

	/*template <>
	inline Int32 Stack::Get<Int32>(lua_State* luaState, int idx)
	{
		auto value = lua_tointeger(luaState, idx);
		return static_cast<Int32>(value);
	}

	template <>
	inline Uint32 Stack::Get<Uint32>(lua_State* luaState, int idx)
	{
		auto value = lua_tointeger(luaState, idx);
		return static_cast<Uint32>(value);
	}

	template <>
	inline Int64 Stack::Get<Int64>(lua_State* luaState, int idx)
	{
		auto value = lua_tointeger(luaState, idx);
		return static_cast<Int64>(value);
	}

	template <>
	inline Uint64 Stack::Get<Uint64>(lua_State* luaState, int idx)
	{
		auto value = lua_tointeger(luaState, idx);
		return static_cast<Uint64>(value);
	}

	template <>
	inline float Stack::Get<float>(lua_State* luaState, int idx)
	{
		auto value = lua_tonumber(luaState, idx);
		return static_cast<float>(value);
	}

	template <>
	inline double Stack::Get<double>(lua_State* luaState, int idx)
	{
		auto value = lua_tonumber(luaState, idx);
		return static_cast<double>(value);
	}

	template <>
	inline void Stack::Push(lua_State* luaState, Int32 value)
	{
		lua_pushinteger(luaState, static_cast<Int32>(value));
	}

	template <>
	inline void Stack::Push(lua_State* luaState, Uint32 value)
	{
		lua_pushinteger(luaState, static_cast<Uint32>(value));
	}

	template <>
	inline void Stack::Push(lua_State* luaState, Int64 value)
	{
		lua_pushinteger(luaState, static_cast<Int64>(value));
	}

	template <>
	inline void Stack::Push(lua_State* luaState, Uint64 value)
	{
		lua_pushinteger(luaState, static_cast<Uint64>(value));
	}

	template <>
	inline void Stack::Push(lua_State* luaState, float value)
	{
		lua_pushnumber(luaState, static_cast<float>(value));
	}

	template <>
	inline void Stack::Push(lua_State* luaState, double value)
	{
		lua_pushnumber(luaState, static_cast<double>(value));
	}*/


}
}


#endif /* __Exs_ScriptSystem_Lua_LuaStack_H__ */
