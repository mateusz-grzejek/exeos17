
#ifndef __Exs_ScriptSystem_LuaClassInternals_H__
#define __Exs_ScriptSystem_LuaClassInternals_H__


namespace Exs
{
namespace Lua
{


	struct StaticVariableDef
	{
		const char* name;
		void* varPtr;
		luaL_Reg wrapFunction;
	};


	template <typename T>
	struct ClassDef
	{
		const char* name;
		std::initializer_list<luaL_Reg> memberConstFunctions;
		std::initializer_list<luaL_Reg> memberNonConstFunctions;
		std::initializer_list<luaL_Reg> staticFunctions;
		std::initializer_list<StaticVariableDef> staticVariables;
	};


	// template <size_t N>
	// void CreateNonStaticIndexMetamethod(lua_State* luaState, const char* name, const luaL_Reg(&funcs)[N])
	// {
	// 	lua_newtable(luaState);
	// 	// Stack: [table, metatable]
	// 
	// 	for (auto& reg : funcs)
	// 	{
	// 		if (reg.func == nullptr)
	// 		{
	// 			break;
	// 		}
	// 
	// 		lua_pushcfunction(luaState, reg.func);
	// 		// Stack: [reg.func, table, metatable]
	// 
	// 		lua_setfield(luaState, -2, reg.name);
	// 		// Stack: [table, metatable]
	// 	}
	// 
	// 	lua_setfield(luaState, -2, name);
	// 	// Stack: [metatable]
	// }


	// template <size_t N>
	// void CreateStaticIndexMetamethod(lua_State* luaState, const char* name, const constantReg(&consts)[N])
	// {
	// 	lua_newtable(luaState);
	// 	// Stack: [table, metatable]
	// 
	// 	for (auto& reg : consts)
	// 	{
	// 		if (reg.name == nullptr)
	// 		{
	// 			break;
	// 		}
	// 
	// 		Lua::StaticWrapper<lua_Integer>::Create(luaState, reg.valuePtr);
	// 
	// 		lua_setfield(luaState, -2, reg.name);
	// 		// Stack: [table, metatable]
	// 	}
	// 
	// 	lua_setfield(luaState, -2, name);
	// 	// Stack: [metatable]
	// }


	template <typename T>
	int LuaClsNonStaticIndex(lua_State* luaState)
	{
		auto* userdata = CheckUserdata<T>::Check(luaState, -2);
		const char* propertyName = luaL_checkstring(luaState, -1);

		lua_getmetatable(luaState, -2);
		lua_pushvalue(luaState, -2);
		lua_rawget(luaState, -2);

		if (!lua_isnil(luaState, -1))
		{
			return 1;
		}
		else
		{
			lua_pop(luaState, 1);
			lua_pushstring(luaState, Meta_Cls_Non_Static_Variables_RO_Index_Name);
			lua_rawget(luaState, -2);

			if (!lua_isnil(luaState, -1))
			{
				lua_pushstring(luaState, propertyName);
				lua_rawget(luaState, -2);

				if (!lua_isnil(luaState, -1))
				{
					lua_pushvalue(luaState, 1);
					lua_call(luaState, 1, 1);
					return 1;
				}
			}

		}

		return 0;
	}


	template <typename T>
	int LuaClsNonStaticNewIndex(lua_State* luaState)
	{
		auto* userdata = CheckUserdata<T>::Check(luaState, -3);
		const char* propertyName = luaL_checkstring(luaState, -2);

		lua_getmetatable(luaState, -3);
		lua_pushvalue(luaState, -2);
		lua_rawget(luaState, -2);

		if (!lua_isnil(luaState, -1))
		{
			return 1;
		}
		else
		{
			lua_pop(luaState, 1);
			lua_pushstring(luaState, Meta_Cls_Non_Static_Variables_RW_Index_Name);
			lua_rawget(luaState, -2);

			if (!lua_isnil(luaState, -1))
			{
				lua_pushstring(luaState, propertyName);
				lua_rawget(luaState, -2);

				if (!lua_isnil(luaState, -1))
				{
					lua_pushvalue(luaState, 1);
					lua_pushvalue(luaState, 3);
					lua_call(luaState, 2, 0);
					return 0;
				}
			}

		}

		return 0;
	}

	template <typename T>
	int LuaClsStaticIndex(lua_State* luaState)
	{
		const char* propertyName = luaL_checkstring(luaState, -1);

		lua_pushstring(luaState, Meta_Cls_Static_Variables_RO_Index_Name);
		lua_rawget(luaState, -3);

		if (lua_isnil(luaState, -1))
		{
			lua_pop(luaState, 1);
			return 0;
		}
		else
		{
			lua_pushstring(luaState, propertyName);
			lua_rawget(luaState, -2);

			if (!lua_isnil(luaState, -1))
			{
				lua_call(luaState, 0, 1);
				return 1;
			}

		}

		return 0;
	}


}
}


#endif /* __Exs_ScriptSystem_LuaClassInternals_H__ */
