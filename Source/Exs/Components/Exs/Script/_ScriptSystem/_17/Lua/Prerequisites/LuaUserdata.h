
#pragma once

#ifndef __Exs_ScriptSystem_LuaUserdata_H__
#define __Exs_ScriptSystem_LuaUserdata_H__


namespace Exs
{
namespace Lua
{


	enum class UserdataType : Enum
	{
		Object,
		Pointer,
		Reference,
		Unknown
	};


	enum class RefObjectTransferPolicy : Enum
	{
		Copy = 1,
		Ref_Only,
		Default = Ref_Only,
		Undefined = 0
	};


	template <typename T>
	struct UserdataObjectDestroy
	{
		void operator()(T* ptr)
		{
			ptr->~T();
		}
	};


	namespace Internal
	{

		template <typename T>
		struct UserdataBase
		{
		public:
			T* objectPtr = nullptr;

			UserdataType type = UserdataType::Unknown;

		public:
			virtual ~UserdataBase()
			{
				this->objectPtr = nullptr;
				this->type = UserdataType::Unknown;
			}
		};

		template <typename T>
		struct UserdataObjectBase : public UserdataBase<T>
		{
		private:
			using Storage = typename std::aligned_storage<sizeof(T)>::type;

			Storage storage;

		public:
			UserdataObjectBase()
			{
				this->objectPtr = reinterpret_cast<T*>(&(this->storage));
			}

			virtual ~UserdataObjectBase()
			{
				if ((this->objectPtr != nullptr) && (this->type == UserdataType::Object))
				{
					UserdataObjectDestroy<T>()(this->objectPtr);
				}
			}

			template <typename... Args>
			void Construct(Args&&... args)
			{
				new (this->objectPtr) T(std::forward<Args>(args)...);
				this->type = UserdataType::Object;
			}
		};

		template <typename T>
		struct UserdataPointerBase : public UserdataBase<T>
		{
			void Set(T& valueRef)
			{
				this->objectPtr = &valueRef
				this->type = UserdataType::Reference;
			}
		};

		template <typename T>
		struct UserdataReferenceBase : public UserdataBase<T>
		{
			void Set(T* valuePtr)
			{
				this->objectPtr = valuePtr;
				this->type = UserdataType::Pointer;
			}
		};

	}


	template <typename T>
	using Userdata = Internal::UserdataBase<typename std::remove_cv<T>::type>;

	template <typename T>
	using UserdataObject = Internal::UserdataObjectBase<typename std::remove_cv<T>::type>;

	template <typename T>
	using UserdataPointer = Internal::UserdataPointerBase<typename std::remove_cv<T>::type>;

	template <typename T>
	using UserdataReference = Internal::UserdataReferenceBase<typename std::remove_cv<T>::type>;


	template <typename T>
	struct UserdataReleaseProcWrapper
	{
		static int Release(lua_State* luaState)
		{
			if (Userdata<T>* userdata = (Userdata<T>*)luaE_checkuserdata(luaState, 1, TypeMetaInfo<T>::GetClsMetatableName()))
			{
				userdata->~UserdataBase();
			}

			return 0;
		}
	};


	namespace Internal
	{

		template <typename U>
		U* CreateUserdata(lua_State* luaState)
		{
			// This function is called by three CreateUserdataXXX() functions defined below.
			// Metatable of specified type is currently at the top of the Lua stack.

			if (lua_isnil(luaState, -1) != 0)
			{
				lua_pop(luaState, 1);
				return nullptr;
			}

			auto* userdata = (U*)lua_newuserdata(luaState, sizeof(U));
			new (userdata) U();

			lua_insert(luaState, -2);
			lua_setmetatable(luaState, -2);

			return userdata;
		}

	}
	

	template <typename T>
	UserdataObject<T>* CreateUserdataObject(lua_State* luaState)
	{
		luaL_getmetatable(luaState, TypeMetaInfo<T>::GetClsMetatableName());
		return Internal::CreateUserdata< UserdataObject<T> >(luaState);
	}

	template <typename T>
	UserdataPointer<T>* CreateUserdataPointer(lua_State* luaState)
	{
		luaL_getmetatable(luaState, TypeMetaInfo<T>::GetClsMetatableName());
		return Internal::CreateUserdata< UserdataPointer<T> >(luaState);
	}

	template <typename T>
	UserdataReference<T>* CreateUserdataReference(lua_State* luaState)
	{
		luaL_getmetatable(luaState, TypeMetaInfo<T>::GetClsMetatableName());
		return Internal::CreateUserdata< UserdataReference<T> >(luaState);
	}


	struct CheckNil
	{
		static bool Check(lua_State* luaState, int idx)
		{
			if (lua_isnil(luaState, idx))
			{
				return true;
			}

			return false;
		}
	};


	template <typename T>
	struct CheckUserdata
	{
		static Userdata<T>* Check(lua_State* luaState, int idx)
		{
			if (void* userdata = luaE_checkuserdata(luaState, idx, TypeMetaInfo<T>::GetClsMetatableName()))
			{
				return reinterpret_cast<Userdata<T>*>(userdata);
			}
			return nullptr;
		}
	};

	template <typename T>
	struct CheckUserdata<const T>
	{
		static Userdata<const T>* Check(lua_State* luaState, int idx)
		{
			if (void* userdata = luaE_checkuserdata(luaState, idx, TypeMetaInfo<const T>::GetClsMetatableName()))
			{
				return reinterpret_cast<Userdata<T>*>(userdata);
			}
			if (void* userdata = luaE_checkuserdata(luaState, idx, TypeMetaInfo<T>::GetClsMetatableName()))
			{
				return reinterpret_cast<Userdata<T>*>(userdata);
			}
			return nullptr;
		}
	};


	template <typename T>
	struct UserdataValueType
	{
		typedef T Type;
	};

	template <typename T>
	struct UserdataValueType<T&>
	{
		typedef std::reference_wrapper<T> Type;
	};

	template <typename T>
	struct UserdataValueType<const T&>
	{
		typedef std::reference_wrapper<const T> Type;
	};

	template <typename T>
	struct UserdataValueType<T*>
	{
		typedef T* Type;
	};

	template <typename T>
	struct UserdataValueType<const T*>
	{
		typedef const T* Type;
	};


	template <typename T>
	struct GetUserdataValue
	{
		static T Get(lua_State* luaState, int idx)
		{
			if (auto* userdata = CheckUserdata<T>::Check(luaState, idx))
			{
				return *(userdata->objectPtr);
			}

			throw 0;
		}
	};

	template <typename T>
	struct GetUserdataValue< std::reference_wrapper<T> >
	{
		static std::reference_wrapper<T> Get(lua_State* luaState, int idx)
		{
			if (auto* userdata = CheckUserdata<T>::Check(luaState, idx))
			{
				return std::ref(*(userdata->objectPtr));
			}

			throw 0;
		}
	};

	template <typename T>
	struct GetUserdataValue< std::reference_wrapper<const T> >
	{
		static std::reference_wrapper<const T> Get(lua_State* luaState, int idx)
		{
			if (auto* userdata = CheckUserdata<const T>::Check(luaState, idx))
			{
				return std::cref(*(userdata->objectPtr));
			}

			throw 0;
		}
	};

	template <typename T>
	struct GetUserdataValue< T* >
	{
		static T* Get(lua_State* luaState, int idx)
		{
			if (auto* userdata = CheckUserdata<T>::Check(luaState, idx))
			{
				return userdata->objectPtr;
			}
			if (CheckNil::Check(luaState, idx))
			{
				return nullptr;
			}

			throw 0;
		}
	};

	template <typename T>
	struct GetUserdataValue< const T* >
	{
		static const T* Get(lua_State* luaState, int idx)
		{
			if (auto* userdata = CheckUserdata<const T>::Check(luaState, idx))
			{
				return userdata->objectPtr;
			}
			if (CheckNil::Check(luaState, idx))
			{
				return nullptr;
			}

			throw 0;
		}
	};


	template <typename T, RefObjectTransferPolicy>
	struct PushUserdataValue
	{
		static bool Push(lua_State* luaState, const T& value)
		{
			if (auto* userdata = CreateUserdataObject<T>(luaState))
			{
				userdata->Construct(std::move(value));
				return true;
			}

			return false;
		}
	};

	template <typename T>
	struct PushUserdataValue< std::reference_wrapper<T>, RefObjectTransferPolicy::Copy >
	{
		static bool Push(lua_State* luaState, std::reference_wrapper<T> ref)
		{
			if (auto* userdata = CreateUserdataObject<T>(luaState))
			{
				userdata->Construct(ref.get());
				return true;
			}

			return false;
		}
	};

	template <typename T>
	struct PushUserdataValue< std::reference_wrapper<T>, RefObjectTransferPolicy::Ref_Only >
	{
		static bool Push(lua_State* luaState, std::reference_wrapper<T> ref)
		{
			if (auto* userdata = CreateUserdataObject<T>(luaState))
			{
				userdata->Set(ref.get());
				return true;
			}

			return false;
		}
	};

	template <typename T>
	struct PushUserdataValue< std::reference_wrapper<const T>, RefObjectTransferPolicy::Copy >
	{
		static bool Push(lua_State* luaState, std::reference_wrapper<const T> ref)
		{
			if (auto* userdata = CreateUserdataObject<const T>(luaState))
			{
				userdata->Construct(ref);
				return true;
			}

			return false;
		}
	};

	template <typename T>
	struct PushUserdataValue< std::reference_wrapper<const T>, RefObjectTransferPolicy::Ref_Only >
	{
		static bool Push(lua_State* luaState, std::reference_wrapper<const T> ref)
		{
			if (auto* userdata = CreateUserdataReference<const T>(luaState))
			{
				userdata->Set(*const_cast<T*>(&(ref.get())));
				return true;
			}

			return false;
		}
	};

	template <typename T>
	struct PushUserdataValue< T*, RefObjectTransferPolicy::Copy >
	{
		static bool Push(lua_State* luaState, T* ptr)
		{
			if (auto* userdata = CreateUserdataObject<T>(luaState))
			{
				userdata->Construct(*ptr);
				return true;
			}

			return false;
		}
	};

	template <typename T>
	struct PushUserdataValue< T*, RefObjectTransferPolicy::Ref_Only >
	{
		static bool Push(lua_State* luaState, T* ptr)
		{
			if (auto* userdata = CreateUserdataPointer<T>(luaState))
			{
				userdata->Set(ptr);
				return true;
			}

			return false;
		}
	};

	template <typename T>
	struct PushUserdataValue< const T*, RefObjectTransferPolicy::Copy >
	{
		static bool Push(lua_State* luaState, const T* ptr)
		{
			if (auto* userdata = CreateUserdataObject<const T>(luaState))
			{
				userdata->Construct(*ptr);
				return true;
			}

			return false;
		}
	};

	template <typename T>
	struct PushUserdataValue< const T*, RefObjectTransferPolicy::Ref_Only >
	{
		static bool Push(lua_State* luaState, const T* ptr)
		{
			if (auto* userdata = CreateUserdataPointer<const T>(luaState))
			{
				userdata->Set(const_cast<T*>(ptr));
				return true;
			}

			return false;
		}
	};


}
}


#endif /* __Exs_ScriptSystem_LuaUserdata_H__ */
