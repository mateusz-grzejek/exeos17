
#ifndef __Exs_ScriptSystem_ClassRegDefinition_H__
#define __Exs_ScriptSystem_ClassRegDefinition_H__

#include "Prerequisites.h"

// These defines shall be define dby the actual ScriptSystem (Lua, JS, etc.)

#define ExsScriptRegClassNonStaticVariableReadOnly(varPtr)
#define ExsScriptRegClassNonStaticVariableReadWrite(varPtr)
#define ExsScriptRegClassNonStaticFunction(functionPtr)
#define ExsScriptRegClassStaticVariableReadOnly(varPtr)
#define ExsScriptRegClassStaticVariableReadWrite(varPtr)
#define ExsScriptRegClassStaticFunction(functionPtr)
#define ExsScriptRegClassEnum(enumName, ...)


typedef void ClassNonStaticVariableWrapper;
typedef void ClassNonStaticFunctionWrapper;
typedef void ClassStaticVariableWrapper;
typedef void ClassStaticFunctionWrapper;
typedef void ClassEnumWrapper;


namespace Exs
{


	class ClassRegDefinition
	{
	private:
		
	};


}


#endif /* __Exs_ScriptSystem_ClassRegDefinition_H__ */
