
#ifndef __STDX_XOR_LIST_H__
#define __STDX_XOR_LIST_H__

#include "common.h"


namespace stdx
{


	template <class _T>
	struct xor_list_traits
	{
		typedef _T value_type;
		typedef _T element_type;
	};


	template < class _T,
	           class _Alloc = std::allocator<typename xor_list_traits<_T>::value_type> >
	class xor_list
	{
	private:
		template <IteratorDirection, IteratorType>
		struct iterator_impl;

	public:
		typedef xor_list<_T, _Alloc> my_type;
		typedef xor_list_traits<_T> traits_type;

		typedef typename traits_type::value_type value_type;
		typedef typename traits_type::element_type element_type;

		typedef iterator_impl<IteratorDirection::Forward, IteratorType::Non_Const> iterator;
		typedef iterator_impl<IteratorDirection::Forward, IteratorType::Const> const_iterator;
		typedef iterator_impl<IteratorDirection::Backward, IteratorType::Non_Const> reverse_iterator;
		typedef iterator_impl<IteratorDirection::Backward, IteratorType::Const> const_reverse_iterator;
	};

}


#endif /* __STDX_XOR_LIST_H__ */
