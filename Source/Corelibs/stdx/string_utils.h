
#ifndef __STDX_STRING_UTILS_H__
#define __STDX_STRING_UTILS_H__

#include "base_config.h"
#include <cctype>
#include <cwctype>
#include <string>


namespace stdx
{


	template <typename _Char>
	struct char_conv;


	template <>
	struct char_conv<char>
	{
		static int to_lower( char ch )
		{
			return std::tolower( ch );
		}

		static int to_upper( char ch )
		{
			return std::toupper( ch );
		}
	};

	template <>
	struct char_conv<wchar_t>
	{
		static wint_t to_lower( wchar_t ch )
		{
			return std::towlower( ch );
		}

		static wint_t to_upper( wchar_t ch )
		{
			return std::towupper( ch );
		}
	};


	template <typename _Char>
	inline std::basic_string<_Char> make_string_lowercase( const std::basic_string<_Char>& str )
	{
		std::basic_string<_Char> result = str;
		std::transform( result.begin(), result.end(), result.begin(), char_conv<_Char>::to_lower );
		return result;
	};

	template <typename _Char>
	inline std::basic_string<_Char> make_string_uppercase( const std::basic_string<_Char>& str )
	{
		std::basic_string<_Char> result = str;
		std::transform( result.begin(), result.end(), result.begin(), char_conv<_Char>::to_upper );
		return result;
	};


	template <typename _Char, class _Apred>
	inline size_t split_string( const _Char* str, size_t strLength, _Char separator, _Apred appendPredicate )
	{
		size_t result = 0;

		for (const _Char* strEnd = str + strLength; str < strEnd; )
		{
			const _Char* tokenPtr = std::char_traits<_Char>::find( str, strLength, separator );
			if (tokenPtr == nullptr)
			{
				appendPredicate( str, strEnd - str );
				break;
			}

			size_t substrLen = tokenPtr - str;
			assert( substrLen > 0 );

			appendPredicate( str, substrLen );
			++result;

			str = tokenPtr + 1;
			strLength -= (substrLen + 1);
		}

		return result;
	};

	template <typename _Char, class _Apred>
	inline size_t split_string( const _Char* str, _Char separator, _Apred appendPredicate )
	{
		size_t strLength = std::char_traits<_Char>::length( str );
		return split_string( str, strLength, separator, appendPredicate );
	};

	template <typename _Char, class _Apred>
	inline size_t split_string( const std::basic_string<_Char>& str, _Char separator, _Apred appendPredicate )
	{
		return split_string( str.data(), str.length(), separator, appendPredicate );
	};


	template <typename _Res, typename _Char, class _Apred>
	inline _Res split_string_ex( const _Char* str, size_t strLength, _Char separator, _Apred appendPredicate )
	{
		_Res result{ };
		auto appendPredicateRes = std::bind( appendPredicate, std::ref( result ), std::placeholders::_1, std::placeholders::_2 );
		split_string( str, strLength, separator, appendPredicateRes );
		return result;
	};

	template <typename _Res, typename _Char, class _Apred>
	inline _Res split_string_ex( const _Char* str, _Char separator, _Apred appendPredicate )
	{
		size_t strLength = std::char_traits<_Char>::length( str );
		return split_string_ex<_Res>( str, strLength, separator, appendPredicate );
	};

	template <typename _Res, typename _Char, class _Apred>
	inline _Res split_string_ex( const std::basic_string<_Char>& str, _Char separator, _Apred appendPredicate )
	{
		return split_string_ex<_Res>( str.data(), str.length(), separator, appendPredicate );
	};


	template <typename _Char>
	inline std::basic_string<_Char> extract_short_file_path( const std::basic_string<_Char>& fileName, _Char pathDelim )
	{
		std::basic_string<_Char> shortFilename = fileName;
		size_t filenameSeparator = shortFilename.find_last_of( pathDelim, 0 );

		if (filenameSeparator != std::basic_string<_Char>::npos)
		{
			size_t lastDirSeparator = shortFilename.find_last_of( pathDelim, filenameSeparator );
			if ( lastDirSeparator != std::basic_string<_Char>::npos )
			{
				shortFilename.erase( 0, lastDirSeparator + 1 );
			}
		}

		return shortFilename;
	}

	template <typename _Char>
	inline std::basic_string<_Char> extract_short_file_path( const _Char* fileName, _Char pathDelim )
	{
		std::basic_string<_Char> fileNameStr = fileName;
		return extract_short_file_path( fileNameStr, pathDelim );
	}


	template <typename _Char>
	inline std::basic_string<_Char> str_make_lowercase( const std::basic_string<_Char>& str )
	{
		constexpr auto lowercaseLeft = static_cast<size_t>( 'a' );
		constexpr auto uppercaseLeft = static_cast<size_t>( 'A' );
		constexpr auto uppercaseRight = static_cast<size_t>( 'Z' );

		std::basic_string<_Char> result{ str };

		for ( auto& ch : result )
		{
			auto codePoint = static_cast<size_t>( ch );

			if ( ( codePoint >= uppercaseLeft ) && ( codePoint <= uppercaseRight ) )
			{
				codePoint = lowercaseLeft + ( codePoint - uppercaseLeft );
				ch = static_cast<_Char>( codePoint );
			}
		}

		return result;
	}


	template <typename _Char>
	inline std::basic_string<_Char> str_make_uppercase( const std::basic_string<_Char>& str )
	{
		constexpr auto uppercaseLeft = static_cast<size_t>( 'A' );
		constexpr auto lowercaseLeft = static_cast<size_t>( 'a' );
		constexpr auto lowercaseRight = static_cast<size_t>( 'z' );

		std::basic_string<_Char> result{ str };

		for ( auto& ch : result )
		{
			auto codePoint = static_cast<size_t>( ch );

			if ( ( codePoint >= lowercaseLeft ) && ( codePoint <= lowercaseRight ) )
			{
				codePoint = uppercaseLeft + ( codePoint - lowercaseLeft );
				ch = static_cast<_Char>( codePoint );
			}
		}

		return result;
	}


}


#endif /* __STDX_STRING_UTILS_H__ */
