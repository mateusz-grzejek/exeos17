
#ifndef __STDX_CONCURRENT_DATA_POOL_H__
#define __STDX_CONCURRENT_DATA_POOL_H__

/*
* concurrent_data_pool.h
*
* Implementation of a simple concurrent pool of homogenous data, that can be safely acquired by multiple threads
* at the same time in a lockless manner.
*
* Conceptually, data pool stores a fixed array of objects of some type. Each of these objects can be "reserved"
* by a single thread - when it happens, reserved objects becomes a property of that thread and no other thread
* can even touch it (it is marked as reserved). When thread does not use its data anymore, it is released,
* so other threads can use it when such need arises.
*
* Details:
* - pool can store objects either as a plain objects (by value) or via pointers (e.g. you can instantiate both
*   concurrent_data_pool<MyClass, N> and concurrent_data_pool<void*, N>),
* - when data is reserved, a handle-like is returned which evaluates to true if reservation was successful;
*   this object contains a pointer to the data and a 'key' which is a value used to release data back to the pool.
*/

#include "limits.h"
#include <array>
#include <atomic>


namespace stdx
{


	//
	using concurrent_data_pool_key_t = size_t;

	//
	constexpr concurrent_data_pool_key_t invalid_concurrent_data_pool_key = stdx::limits<concurrent_data_pool_key_t>::max_value;


	template <class _T, size_t alignment>
	class alignas(alignment) concurrent_data_pool_entry
	{
	public:
		using my_type = concurrent_data_pool_entry<_T, alignment>;
		using value_type = _T;

	public:
		concurrent_data_pool_entry()
			: _alloc_flag{ ATOMIC_FLAG_INIT }
		{ }

		bool acquire()
		{
			return !this->_alloc_flag.test_and_set( std::memory_order_acq_rel );
		}

		void release()
		{
			this->_alloc_flag.clear();
		}

		value_type& get_data()
		{
			return this->_data;
		}

		const value_type& get_data() const
		{
			return this->_data;
		}

	private:
		std::atomic_flag _alloc_flag;
		value_type _data;
	};


	template <class _T>
	class concurrent_data_pool_value_handle
	{
	public:
		using my_type = concurrent_data_pool_value_handle<_T>;
		using key_type = concurrent_data_pool_key_t;
		using value_type = _T;

	public:
		concurrent_data_pool_value_handle()
			: _key( invalid_concurrent_data_pool_key )
			, _data_ptr( nullptr )
		{ }

		concurrent_data_pool_value_handle( std::nullptr_t )
			: concurrent_data_pool_value_handle()
		{ }

		concurrent_data_pool_value_handle( key_type key, value_type* dataPtr = nullptr )
			: _key( key )
			, _data_ptr( dataPtr )
		{ }

		concurrent_data_pool_value_handle( key_type key, value_type& data )
			: _key( key )
			, _data_ptr( &data )
		{ }

		explicit operator bool() const
		{
			return this->_key != invalid_concurrent_data_pool_key;
		}

		value_type& operator*() const
		{
			return *( this->_data_ptr );
		}

		value_type* operator->() const
		{
			return this->_data_ptr;
		}

		value_type& get_data()
		{
			return *( this->_data_ptr );
		}

		const value_type& get_data() const
		{
			return *( this->_data_ptr );
		}

		key_type get_key() const
		{
			return this->_key;
		}

	private:
		key_type _key;
		value_type* _data_ptr;
	};


	template <class _T, size_t size>
	class concurrent_data_pool
	{
	public:
		static constexpr auto pool_size = size;

		static constexpr auto entry_alignment = 64;

		using my_type = concurrent_data_pool<_T, size>;
		using value_type = _T;
		using entry_type = concurrent_data_pool_entry<_T, entry_alignment>;
		using value_handle_type = concurrent_data_pool_value_handle<_T>;

	private:
		using entry_array = std::array<entry_type, pool_size>;

	public:
		concurrent_data_pool()
		{ }

		concurrent_data_pool( const concurrent_data_pool& ) = delete;
		concurrent_data_pool& operator=( const concurrent_data_pool& ) = delete;

		value_type& operator[]( size_t index )
		{
			auto& entry = this->_entries[index];
			return entry.get_data();
		}

		const value_type& operator[]( size_t index ) const
		{
			const auto& entry = this->_entries[index];
			return entry.get_data();
		}

		value_handle_type acquire()
		{
			size_t alloc_counter_value = this->_alloc_counter.load( std::memory_order_relaxed );

			while ( true )
			{
				if ( alloc_counter_value == pool_size )
				{
					return nullptr;
				}

				if ( this->_alloc_counter.compare_exchange_weak( alloc_counter_value, alloc_counter_value + 1, std::memory_order_acq_rel, std::memory_order_relaxed ) )
				{
					break;
				}
			}

			for ( size_t entry_index = 0; entry_index < pool_size; ++entry_index )
			{
				auto& entry = this->_entries[entry_index];

				if ( entry.acquire() )
				{
					return value_handle_type( entry_index, entry.get_data() );
				}
			}

			throw std::runtime_error( "stdx::concurrent_data_pool desynchronization - alloc counter incr ok, but entry reservation failed!" );
		}

		void release( concurrent_data_pool_key_t key )
		{
			auto& entry = this->_entries[key];
			entry.release();
			this->_alloc_counter.fetch_sub( 1, std::memory_order_release );
		}

		void release( const value_handle_type& valueHandle )
		{
			this->release( valueHandle.get_key() );
		}

		size_t get_alloc_counter() const
		{
			return this->_alloc_counter.load( std::memory_order_relaxed );
		}

		bool empty() const
		{
			return this->get_alloc_counter() == 0;
		}

		bool is_full() const
		{
			return this->get_alloc_counter() == pool_size;
		}

	private:
		std::atomic<size_t> _alloc_counter;
		entry_array _entries;

	};


	template <class _T, size_t size>
	inline _T* begin( concurrent_data_pool<_T, size>& pool )
	{
		return &( pool[0] );
	}

	template <class _T, size_t size>
	inline const _T* begin( const concurrent_data_pool<_T, size>& pool )
	{
		return &( pool[0] );
	}

	template <class _T, size_t size>
	inline _T* end( concurrent_data_pool<_T, size>& pool )
	{
		return begin( pool ) + size;
	}

	template <class _T, size_t size>
	inline const _T* end( const concurrent_data_pool<_T, size>& pool )
	{
		return begin( pool ) + size;
	}


}


#endif /* __STDX_CONCURRENT_DATA_POOL_H__ */
