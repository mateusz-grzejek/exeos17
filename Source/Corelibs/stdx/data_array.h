
#ifndef __STDX_DATA_ARRAY_H__
#define __STDX_DATA_ARRAY_H__

#include "memory_buffer.h"


namespace stdx
{


	template <class _T>
	struct data_array_traits
	{
		typedef _T data_type;
		typedef _T* pointer_type;
	};


	template <class _T>
	class data_array
	{
	public:
		typedef data_array<_T> my_type;
		typedef data_array_traits<_T> traits_type;

		typedef typename traits_type::data_type data_type;
		typedef typename traits_type::pointer_type pointer_type;

	protected:
		_T*     _dataPtr;
		size_t  _capacity;
		size_t  _elementsNum;

	public:
		data_array( const data_array& ) = delete;
		data_array& operator=( const data_array& ) = delete;

		data_array()
			: _dataPtr( nullptr )
			, _capacity( 0 )
			, _elementsNum( 0 )
		{ }

		data_array( data_type* memory, size_t capacity, size_t dataSize = 0 )
			: _dataPtr( memory )
			, _capacity( capacity )
			, _elementsNum( dataSize )
		{ }

		virtual ~data_array()
		{ }

		data_type& operator[]( size_t pos )
		{
			assert( pos < this->_elementsNum );
			return this->_dataPtr[pos];
		}

		const data_type& operator[]( size_t pos ) const
		{
			assert( pos < this->_elementsNum );
			return this->_dataPtr[pos];
		}

		bool append( const _T& value )
		{
			if (this->free_space() == 0)
				return false;

			this->_buffer[this->_elementsNum++] = value;
			return true;
		}

		bool append( const _T* values, size_t count )
		{
			assert( (values != nullptr) || (count == 0) );

			if (count == 0)
				return true;

			if (this->free_space() < count)
				return false;

			memcpy( this->_buffer.data_offset_ptr( this->_elementsNum ), values, sizeof( _T ) * count );
			this->_elementsNum += count;

			return true;
		}

		bool assign( const _T* data, size_t count )
		{
			assert( (data != nullptr) || (count == 0) );

			if (this->free_space() < count)
				return false;

			if (count > 0)
			{
				assert( data != nullptr );
				memcpy( this->_dataPtr, data, sizeof( _T ) * count );
			}

			this->_elementsNum = count;
			return true;
		}

		void erase( size_t position )
		{
			assert( position < this->_elementsNum );

			if (position < (this->_elementsNum - 1))
				safe_memmove( this->_dataPtr, this->_capacity, position, this->_elementsNum - position, -1 );

			--this->_elementsNum;
		}

		void erase( size_t start, size_t count )
		{
			assert( (start < this->_elementsNum) && (this->_elementsNum - start >= count) );

			if (count == 0)
				return;

			size_t eraseCount = get_min_of( count, this->_elementsNum - start + 1 );
			size_t lastErasePos = start + eraseCount - 1;

			if (lastErasePos < this->_elementsNum)
				safe_memmove( this->_dataPtr, this->_capacity, start, eraseCount, -static_cast<ptrdiff_t>(count) );

			this->_elementsNum -= eraseCount;
		}

		void clear()
		{
			this->_elementsNum = 0;
		}

		data_type* data_ptr()
		{
			return this->_dataPtr;
		}

		const data_type* data_ptr() const
		{
			return this->_dataPtr;
		}

		data_type* data_offset_ptr( size_t offset )
		{
			assert( (this->_size > 0) && (offset <= this->_elementsNum) );
			return this->_dataPtr + offset;
		}

		const data_type* data_offset_ptr( size_t offset ) const
		{
			assert( (this->_size > 0) && (offset <= this->_elementsNum) );
			return this->_dataPtr + offset;
		}

		size_t free_space() const
		{
			return this->_capacity - this->_elementsNum;
		}

		size_t size() const
		{
			return this->_elementsNum;
		}

		size_t size_in_bytes() const
		{
			return sizeof( data_type ) * this->_elementsNum;
		}

		size_t capacity() const
		{
			return this->_capacity;
		}

		bool empty() const
		{
			return this->_elementsNum == 0;
		}
	};


	template <class _T, class _Alloc>
	struct dynamic_data_array_traits : public data_array_traits<_T>
	{
		typedef typename data_array_traits<_T>::data_type data_type;
		typedef typename data_array_traits<_T>::pointer_type pointer_type;

		typedef _Alloc allocator_type;
	};


	///<summary>
	///</summary>
	template < class _T,
		class _Alloc = std::allocator<_T> >
		class dynamic_data_array : public data_array<_T>
	{
	public:
		typedef data_array<_T> base_type;
		typedef dynamic_data_array<_T, _Alloc> my_type;
		typedef dynamic_data_array_traits<_T, _Alloc> traits_type;

		typedef dynamic_memory_buffer<_T, _Alloc> memory_buffer_type;

	private:
		memory_buffer_type  _storage;

	public:
		dynamic_data_array()
			: base_type()
		{ }

		dynamic_data_array( my_type&& source )
			: base_type()
		{
			this->swap( source );
		}

		dynamic_data_array( const my_type& origin )
			: base_type()
		{
			if (!origin.empty())
				this->_assign( origin._dataPtr, origin._elementsNum );
		}

		dynamic_data_array( const _T* data, size_t length )
			: base_type()
		{
			assert( (data != nullptr) || (length == 0) );

			if (length > 0)
				this->append( data, length );
		}

		explicit dynamic_data_array( size_t initialCapacity )
			: base_type()
		{
			if (initialCapacity > 0)
				this->_check_total_space( initialCapacity );
		}

		virtual ~dynamic_data_array()
		{
			this->release();
		}

		my_type& operator=( my_type&& rhs )
		{
			if (this != &rhs)
				my_type( std::move( rhs ) ).swap( *this );

			return *this;
		}

		my_type& operator=( const my_type& rhs )
		{
			if (this != &rhs)
				my_type( rhs ).swap( *this );

			return *this;
		}

		using base_type::append;
		using base_type::assign;

		void append( const _T& value )
		{
			this->_check_free_space( 1 );
			this->_dataPtr[this->_elementsNum++] = value;
		}

		void append( const _T* data, size_t count )
		{
			if (count > 0)
			{
				assert( data != nullptr );
				this->_check_free_space( count );
				memcpy( this->_dataPtr + this->_elementsNum, data, sizeof( _T ) * count );
				this->_elementsNum += count;
			}
		}

		void assign( const _T* data, size_t count )
		{
			if (count > 0)
			{
				assert( data != nullptr );
				this->_check_total_space( count );
				memcpy( this->_dataPtr, data, sizeof( _T ) * count );
			}

			this->_elementsNum = count;
		}

		void release()
		{
			this->_storage.release();
			this->_dataPtr = 0;
			this->_capacity = 0;
			this->_elementsNum = 0;
		}

		void reserve( size_t capacity )
		{
			this->_check_total_space( capacity );
		}

		void resize( size_t newSize )
		{
			if (newSize == this->_elementsNum)
				return;

			if (newSize > this->_elementsNum)
				this->_check_total_space( newSize );

			this->_elementsNum = newSize;
		}

		void resize( size_t newSize, const _T& value )
		{
			if (newSize == this->_elementsNum)
				return;

			if (newSize > this->_elementsNum)
			{
				this->_check_total_space( newSize );
				for (size_t pos = this->_elementsNum; pos < newSize; ++pos)
				{
					this->_dataPtr[pos] = value;
				}
			}

			this->_elementsNum = newSize;
		}

		void swap( my_type& other )
		{
			std::swap( this->_storage, other._storage );
			std::swap( this->_dataPtr, other._dataPtr );
			std::swap( this->_capacity, other._capacity );
			std::swap( this->_elementsNum, other._elementsNum );
		}

	private:
		void _check_free_space( size_t count )
		{
			if (this->_elementsNum + count > this->_capacity)
				this->_expand_storage( this->_elementsNum + count );
		}

		void _check_total_space( size_t capacity )
		{
			if (this->_capacity < capacity)
				this->_resize_storage( capacity );
		}

		void _resize_storage( size_t storageSize )
		{
			this->_storage.resize( storageSize );
			this->_capacity = this->_storage.size();
			this->_dataPtr = this->_storage.data_ptr();
		}

		void _expand_storage( size_t minCapacity = 0 )
		{
			this->_capacity = this->_storage.expand( minCapacity );
			this->_dataPtr = this->_storage.data_ptr();
		}

		void _assign( const _T* data, size_t count )
		{
			assert( (data != nullptr) && (count > 0) );
			this->_check_total_space( count );
			memcpy( this->_dataPtr, data, sizeof( _T ) * count );
			this->_elementsNum = count;
		}
	};


	template <class _T, size_t _S>
	struct fixed_data_array_traits : public data_array_traits<_T>
	{
		typedef typename data_array_traits<_T>::data_type data_type;
		typedef typename data_array_traits<_T>::pointer_type pointer_type;

		static const size_t size = _S;
	};


	///<summary>
	///</summary>
	template <class _T, size_t _S>
	class fixed_data_array : public data_array<_T>
	{
	public:
		typedef data_array<_T> base_type;
		typedef fixed_data_array<_T, _S> my_type;
		typedef fixed_data_array_traits<_T, _S> traits_type;

		typedef typename traits_type::data_type data_type;
		typedef typename traits_type::pointer_type pointer_type;

		static const size_t size = traits_type::size;
		static const size_t storageSize = traits_type::size * sizeof( data_type );

		typedef fixed_memory_buffer<_T, _S> memory_buffer_type;

	private:
		memory_buffer_type  _storage;

	public:
		fixed_data_array()
			: base_type()
		{
			this->_init();
		}

		fixed_data_array( const my_type& origin )
			: base_type()
		{
			this->_init();

			if (!origin.empty())
				this->_assign( origin );
		}

		template <size_t Other_size>
		fixed_data_array( const fixed_data_array<_T, Other_size>& origin )
			: base_type()
		{
			this->_init();

			if (!origin.empty())
				this->_assign( origin );
		}

		virtual ~fixed_data_array()
		{
			this->clear();
		}

		my_type& operator=( const my_type& rhs )
		{
			if (this == &rhs)
				return *this;

			this->_assign( rhs );

			return *this;
		}

		template <size_t _N>
		my_type& operator=( const fixed_data_array<_T, _N>& rhs )
		{
			if (this == &rhs)
				return *this;

			this->_assign( rhs );

			return *this;
		}

	private:
		void _init()
		{
			this->_dataPtr = this->_buffer.data_ptr();
			this->_capacity = this->_buffer.size();
		}

		template <size_t _N>
		void _assign( const fixed_data_array<_T, _N>& other )
		{
			size_t otherSize = other.size();
			size_t assignSize = get_min_of( otherSize, this->_capacity );

			if (assignSize > 0)
				memcpy( this->_dataPtr, other._dataPtr, sizeof( _T ) * assignSize );

			this->_elementsNum = assignSize;
		}
	};


}


#endif /* __STDX_DATA_ARRAY_H__ */
