
#ifndef __STDX_SORTED_ARRAY_H__
#define __STDX_SORTED_ARRAY_H__

#include "common.h"
#include <vector>


namespace stdx
{


	template <class _T, class _Cmp>
	struct sorted_array_traits
	{
		typedef _T value_type;
		typedef _T element_type;
		typedef _Cmp compare_type;
	};


	template < class _T,
		class _Cmp = std::less<_T>,
		class _Alloc = std::allocator<typename sorted_array_traits<_T, _Cmp>::value_type> >
	class sorted_array
	{
	public:
		typedef sorted_array<_T, _Cmp, _Alloc> my_type;
		typedef sorted_array_traits<_T, _Cmp> traits_type;

		typedef typename traits_type::value_type value_type;
		typedef typename traits_type::element_type element_type;
		typedef typename traits_type::compare_type compare_type;

		typedef std::vector<element_type, _Alloc> underlying_container_type;

		typedef typename underlying_container_type::iterator iterator;
		typedef typename underlying_container_type::const_iterator const_iterator;
		typedef typename underlying_container_type::reverse_iterator reverse_iterator;
		typedef typename underlying_container_type::const_reverse_iterator const_reverse_iterator;

		typedef typename underlying_container_type::size_type size_type;
		typedef typename underlying_container_type::difference_type difference_type;
		typedef typename underlying_container_type::allocator_type allocator_type;

	private:
		underlying_container_type  _underlyingContainer;

	public:
		sorted_array( sorted_array&& ) = default;
		sorted_array( const sorted_array& ) = default;
		sorted_array& operator=( sorted_array&& ) = default;
		sorted_array& operator=( const sorted_array& ) = default;

		sorted_array()
		{ }

		explicit sorted_array( const allocator_type& allocator )
			: _underlyingContainer( allocator )
		{ }

		explicit sorted_array( size_t capacity, const allocator_type& allocator = allocator_type() )
			: _underlyingContainer( capacity, allocator )
		{ }

		value_type& operator[]( size_t index )
		{
			value_type& value = this->_underlyingContainer[index];
			return value;
		}

		const value_type& operator[]( size_t index ) const
		{
			const value_type& value = this->_underlyingContainer[index];
			return value;
		}

		value_type& at( size_t index )
		{
			value_type& value = this->_underlyingContainer.at( index );
			return value;
		}

		const value_type& at( size_t index ) const
		{
			const value_type& value = this->_underlyingContainer.at( index );
			return value;
		}

		iterator find( const value_type& value )
		{
			size_t element_pos = this->_find_lower( value, compare_type() );

			if ((element_pos != invalid_position) && (this->_underlyingContainer[element_pos] == value))
				return this->_underlyingContainer.begin() + element_pos;

			return this->end();
		}

		const_iterator find( const value_type& value ) const
		{
			size_t element_pos = this->_find_lower( value, compare_type() );

			if ((element_pos != invalid_position) && (this->_underlyingContainer[element_pos] == value))
				return this->_underlyingContainer.begin() + element_pos;

			return this->end();
		}

		template <class _Cx>
		iterator find( const _Cx& value )
		{
			size_t element_pos = this->_find_lower( value, compare_type() );

			if ((element_pos != invalid_position) && (this->_underlyingContainer[element_pos] == value))
				return this->_underlyingContainer.begin() + element_pos;

			return this->end();
		}

		template <class _Cx>
		const_iterator find( const _Cx& value ) const
		{
			size_t element_pos = this->_find_lower( value, compare_type() );

			if ((element_pos != invalid_position) && (this->_underlyingContainer[element_pos] == value))
				return this->_underlyingContainer.begin() + element_pos;

			return this->end();
		}

		template <class _Cx, class _Eqcmp>
		iterator find( const _Cx& value, _Eqcmp&& eqcmp )
		{
			size_t element_pos = this->_find_lower( value, compare_type() );

			if ((element_pos != invalid_position) && eqcmp( this->_underlyingContainer[element_pos], value ))
				return this->_underlyingContainer.begin() + element_pos;

			return this->end();
		}

		template <class _Cx, class _Eqcmp>
		const_iterator find( const _Cx& value, _Eqcmp&& eqcmp ) const
		{
			size_t element_pos = this->_find_lower( value, compare_type() );

			if ((element_pos != invalid_position) && eqcmp( this->_underlyingContainer[element_pos], value ))
				return this->_underlyingContainer.begin() + element_pos;

			return this->end();
		}

		iterator lower_bound( const value_type& value )
		{
			size_t element_pos = this->_find_lower( value, compare_type() );
			return (element_pos != invalid_position) ? (this->_underlyingContainer.begin() + element_pos) : this->end();
		}

		const_iterator lower_bound( const value_type& value ) const
		{
			size_t element_pos = this->_find_lower( value, compare_type() );
			return (element_pos != invalid_position) ? (this->_underlyingContainer.begin() + element_pos) : this->end();
		}

		template <class _Cx>
		iterator lower_bound( const _Cx& value )
		{
			size_t element_pos = this->_find_lower( value, compare_type() );
			return (element_pos != invalid_position) ? (this->_underlyingContainer.begin() + element_pos) : this->end();
		}

		template <class _Cx>
		const_iterator lower_bound( const _Cx& value ) const
		{
			size_t element_pos = this->_find_lower( value, compare_type() );
			return (element_pos != invalid_position) ? (this->_underlyingContainer.begin() + element_pos) : this->end();
		}

		iterator upper_bound( const value_type& value )
		{
			size_t element_pos = this->_find_upper( value, compare_type() );
			return (element_pos != invalid_position) ? (this->_underlyingContainer.begin() + element_pos) : this->end();
		}

		const_iterator upper_bound( const value_type& value ) const
		{
			size_t element_pos = this->_find_upper( value, compare_type() );
			return (element_pos != invalid_position) ? (this->_underlyingContainer.begin() + element_pos) : this->end();
		}

		template <class _Cx>
		iterator upper_bound( const _Cx& value )
		{
			size_t element_pos = this->_find_upper( value, compare_type() );
			return (element_pos != invalid_position) ? (this->_underlyingContainer.begin() + element_pos) : this->end();
		}

		template <class _Cx>
		const_iterator upper_bound( const _Cx& value ) const
		{
			size_t element_pos = this->_find_upper( value, compare_type() );
			return (element_pos != invalid_position) ? (this->_underlyingContainer.begin() + element_pos) : this->end();
		}

		iterator begin()
		{
			return this->_underlyingContainer.begin();
		}

		const_iterator begin() const
		{
			return this->_underlyingContainer.begin();
		}

		iterator end()
		{
			return this->_underlyingContainer.end();
		}

		const_iterator end() const
		{
			return this->_underlyingContainer.end();
		}

		reverse_iterator rbegin()
		{
			return this->_underlyingContainer.rbegin();
		}

		const_reverse_iterator rbegin() const
		{
			return this->_underlyingContainer.rbegin();
		}

		reverse_iterator rend()
		{
			return this->_underlyingContainer.rend();
		}

		const_reverse_iterator rend() const
		{
			return this->_underlyingContainer.rend();
		}

		value_type& front()
		{
			return this->_underlyingContainer.front();
		}

		const value_type& front() const
		{
			return this->_underlyingContainer.front();
		}

		value_type& back()
		{
			return this->_underlyingContainer.back();
		}

		const value_type& back() const
		{
			return this->_underlyingContainer.back();
		}

		iterator insert( value_type&& value )
		{
			size_t insert_pos = this->_get_insert_position( value, compare_type() );
			auto insert_iter = this->_underlyingContainer.begin() + insert_pos;
			return this->_underlyingContainer.insert( insert_iter, std::move( value ) );
		}

		iterator insert( const value_type& value )
		{
			size_t insert_pos = this->_get_insert_position( value, compare_type() );
			auto insert_iter = this->_underlyingContainer.begin() + insert_pos;
			return this->_underlyingContainer.insert( insert_iter, value );
		}

		iterator erase( iterator pos )
		{
			return this->_underlyingContainer.erase( pos );
		}

		iterator erase( iterator start, iterator end )
		{
			return this->_underlyingContainer.erase( start, end );
		}

		iterator remove( const value_type& value )
		{
			if (!this->empty())
			{
				size_t beginPos = this->_find_lower( value, compare_type() );

				if (beginPos != invalid_position)
				{
					size_t endPos = this->_find_upper( value, compare_type() );
					auto eraseFrom = this->_underlyingContainer.begin() + beginPos;
					auto eraseTo = this->_underlyingContainer.begin() + endPos;
					return this->_underlyingContainer.erase( eraseFrom, eraseTo );
				}
			}

			return this->end();
		}

		iterator remove( const value_type& left, const value_type& right )
		{
			if (!this->empty())
			{
				size_t beginPos = this->_find_lower( left, compare_type() );
				if (beginPos != invalid_position)
				{
					size_t endPos = this->_find_lower( right, compare_type() );
					auto eraseFrom = this->_underlyingContainer.begin() + beginPos;
					auto eraseTo = this->_underlyingContainer.begin() + endPos;
					return this->_underlyingContainer.erase( eraseFrom, eraseTo );
				}
			}

			return this->end();
		}

		void clear()
		{
			this->_underlyingContainer.clear();
		}

		void reserve( size_t capacity )
		{
			this->_underlyingContainer.reserve( capacity );
		}

		size_t capacity() const
		{
			return this->_underlyingContainer.capacity();
		}

		size_t size() const
		{
			return this->_underlyingContainer.size();
		}

		bool empty() const
		{
			return this->_underlyingContainer.empty();
		}

		void swap( my_type& other )
		{
			std::swap( this->_underlyingContainer, other._underlyingContainer );
		}

	private:
		value_type* _data_ptr()
		{
			return this->_underlyingContainer.data();
		}

		const value_type* _data_ptr() const
		{
			return this->_underlyingContainer.data();
		}

		template <class _Vx, class... _Pred>
		size_t _get_insert_position( const _Vx& value, _Pred... predicate ) const
		{
			auto begin = this->_underlyingContainer.begin();
			auto end = this->_underlyingContainer.end();
			auto findPos = std::lower_bound( begin, end, value, std::forward<_Pred>( predicate )... );

			return (findPos != end) ? (findPos - begin) : (end - begin);
		}

		template <class _Vx, class... _Pred>
		size_t _find_lower( const _Vx& value, _Pred... predicate ) const
		{
			if (this->empty())
				return invalid_position;

			auto begin = this->_underlyingContainer.begin();
			auto end = this->_underlyingContainer.end();
			auto find_pos = std::lower_bound( begin, end, value, std::forward<_Pred>( predicate )... );

			return (find_pos != end) ? (find_pos - begin) : invalid_position;
		}

		template <class _Vx, class... _Pred>
		size_t _find_upper( const _Vx& value, _Pred... predicate ) const
		{
			if (this->empty())
				return invalid_position;

			auto begin = this->_underlyingContainer.begin();
			auto end = this->_underlyingContainer.end();
			auto find_pos = std::upper_bound( begin, end, value, std::forward<_Pred>( predicate )... );

			return (find_pos != end) ? (find_pos - begin) : invalid_position;
		}
	};


	template <class _T, class _Cmp, class _Alloc>
	inline typename sorted_array<_T, _Cmp, _Alloc>::iterator begin( sorted_array<_T, _Cmp, _Alloc>& container )
	{
		return container.begin();
	}

	template <class _T, class _Cmp, class _Alloc>
	inline typename sorted_array<_T, _Cmp, _Alloc>::const_iterator begin( const sorted_array<_T, _Cmp, _Alloc>& container )
	{
		return container.begin();
	}

	template <class _T, class _Cmp, class _Alloc>
	inline typename sorted_array<_T, _Cmp, _Alloc>::iterator end( sorted_array<_T, _Cmp, _Alloc>& container )
	{
		return container.end();
	}

	template <class _T, class _Cmp, class _Alloc>
	inline typename sorted_array<_T, _Cmp, _Alloc>::const_iterator end( const sorted_array<_T, _Cmp, _Alloc>& container )
	{
		return container.end();
	}


}


#endif /* __STDX_SORTED_ARRAY_H__ */
