
#ifndef __STDX_SHARED_MUTEX_H__
#define __STDX_SHARED_MUTEX_H__

#include "thr_common.h"
#include <atomic>
#include <mutex>


namespace stdx
{


	class shared_spin_lock
	{
	private:
		std::atomic<size_t>  _sharedCounter;
		std::atomic_flag     _uniqueAccess;

	public:
		shared_spin_lock( const shared_spin_lock& ) = delete;
		shared_spin_lock& operator=( const shared_spin_lock& ) = delete;

		shared_spin_lock()
			: _sharedCounter{ ATOMIC_VAR_INIT( 0 ) }
			, _uniqueAccess{ ATOMIC_FLAG_INIT }
		{ }


		/***********************************************************************************************/
		/**************************************** Unique access ****************************************/
		/***********************************************************************************************/

		void lock()
		{
			for ( auto spinCounter = 0; ; ++spinCounter )
			{
				// Acquire unique access. If other thread is holding it, this loop will block current thread.
				if ( !this->_uniqueAccess.test_and_set( std::memory_order_acq_rel ) )
				{
					// Wait untill all threads which hold shared access will release it.
					for ( spinCounter = 0; this->_sharedCounter.load( std::memory_order_acquire ) > 0; ++spinCounter )
					{
						thr_yield( spinCounter );
					}

					break;
				}

				thr_yield( spinCounter );
			}
		}

		bool try_lock()
		{
			// Acquire unique access. If other thread is holding it, this loop will block current thread.
			if ( !this->_uniqueAccess.test_and_set( std::memory_order_acq_rel ) )
			{
				// Wait untill all threads which hold shared access will release it.
				if ( this->_sharedCounter.load( std::memory_order_acquire ) == 0 )
				{
					// Unique access flag has been set, shared access counter is zero - unique access has been acquired.
					return true;
				}

				this->_uniqueAccess.clear();
			}

			return false;
		}

		void unlock()
		{
			this->_uniqueAccess.clear();
		}


		/***********************************************************************************************/
		/**************************************** Shared access ****************************************/
		/***********************************************************************************************/

		void lock_shared()
		{
			for ( auto spinCounter = 0; !this->try_lock_shared(); ++spinCounter )
			{
				thr_yield_spinlock( spinCounter );
			}
		}

		bool try_lock_shared()
		{
			// Acquire temporary unique access.
			if ( !this->_uniqueAccess.test_and_set( std::memory_order_acq_rel ) )
			{
				// Add 1 to shared counter. Unique access will not be allowed as long as shared counter is greater than zero.
				this->_sharedCounter.fetch_add( 1, std::memory_order_release );

				// Not_Acquired unique access flag, as we acquired shared access.
				this->_uniqueAccess.clear();

				// Exit.
				return true;
			}

			return false;
		}

		void unlock_shared()
		{
			this->_sharedCounter.fetch_sub( 1, std::memory_order_release );
		}
	};


	template <class Mutex>
	class shared_lock_adapter
	{
	private:
		Mutex* _mutex;

	public:
		shared_lock_adapter()
			: _mutex( nullptr )
		{ }

		shared_lock_adapter( shared_lock_adapter&& source )
			: _mutex( source._mutex )
		{
			source._mutex = nullptr;
		}

		shared_lock_adapter( Mutex& mutex )
			: _mutex( &mutex )
		{ }

		void lock()
		{
			this->_mutex->lock_shared();
		}

		bool try_lock()
		{
			return this->_mutex->try_lock_shared();
		}

		void unlock()
		{
			this->_mutex->unlock_shared();
		}

		Mutex* mutex()
		{
			return this->_mutex;
		}

		Mutex* release()
		{
			Mutex* mutex_ptr = this->_mutex;
			this->_mutex = nullptr;
			return mutex_ptr;
		}

		void swap( shared_lock_adapter<Mutex>& other )
		{
			std::swap( this->_mutex, other._mutex );
		}
	};


	template <class Mutex>
	inline void swap( shared_lock_adapter<Mutex>&left, shared_lock_adapter<Mutex>& right )
	{
		left.swap( right );
	}


	template <class Mutex>
	class shared_lock
	{
	public:
		using mutex_type = Mutex;

		using AdapterType = shared_lock_adapter<Mutex>;
		using InternalLock = std::unique_lock<AdapterType>;

	private:
		AdapterType _adapter;
		InternalLock _internalLock;

	public:
		shared_lock() = default;

		shared_lock( shared_lock&& source )
			: _adapter( std::move( source._adapter ) )
			, _internalLock( std::move( source._internalLock ) )
		{ }

		shared_lock( Mutex& mutex )
			: _adapter( mutex )
			, _internalLock( _adapter )
		{ }

		shared_lock( Mutex& mutex, const std::adopt_lock_t& )
			: _adapter( mutex )
			, _internalLock( _adapter, std::adopt_lock )
		{ }

		shared_lock( Mutex& mutex, const std::defer_lock_t& )
			: _adapter( mutex )
			, _internalLock( _adapter, std::defer_lock )
		{ }

		shared_lock( Mutex& mutex, const std::try_to_lock_t& )
			: _adapter( mutex )
			, _internalLock( _adapter, std::try_to_lock )
		{ }

		~shared_lock() = default;

		shared_lock& operator=( shared_lock&& rhs )
		{
			if ( this != &rhs )
			{
				shared_lock( std::move( rhs ) ).swap( *this );
			}

			return *this;
		}

		operator bool() const
		{
			return this->_internalLock;
		}

		void lock()
		{
			this->_internalLock.lock();
		}

		bool try_lock()
		{
			return this->_internalLock.try_lock();
		}

		void unlock()
		{
			this->_internalLock.unlock();
		}

		Mutex* mutex()
		{
			return this->_adapter.mutex();
		}

		Mutex* release()
		{
			auto* mutex = this->_adapter.release();
			this->_internalLock.release();
			return mutex;
		}

		bool owns_lock() const
		{
			return this->_internalLock.owns_lock();
		}

		void swap( shared_lock<Mutex>& other )
		{
			swap( this->_adapter, other._adapter );
			std::swap( this->_internalLock, other._internalLock );
		}
	};


	template <class Mutex>
	inline void swap( shared_lock<Mutex>&left, shared_lock<Mutex>& right )
	{
		left.swap( right );
	}


}


#endif /* __STDX_SHARED_MUTEX_H__ */
