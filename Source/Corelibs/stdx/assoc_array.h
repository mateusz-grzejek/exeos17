
#ifndef __STDX_ASSOC_ARRAY__
#define __STDX_ASSOC_ARRAY__

#include "common.h"
#include <vector>


namespace stdx
{


	template <class _Key, class _Value>
	struct assoc_array_traits
	{
		typedef _Key key_type;
		typedef _Value value_type;
		typedef key_value_pair<_Key, _Value> element_type;
	};


	template < class _Key, class _Value, class _Comp = std::less<_Key>, class _Alloc = std::allocator<typename assoc_array_traits<_Key, _Value>::element_type> >
	class assoc_array
	{
	public:
		typedef assoc_array<_Key, _Value, _Alloc> my_type;
		typedef assoc_array_traits<_Key, _Value> traits_type;

		typedef typename traits_type::key_type key_type;
		typedef typename traits_type::value_type value_type;
		typedef typename traits_type::element_type element_type;

		typedef std::vector<element_type, _Alloc> underlying_container_type;

		typedef typename underlying_container_type::iterator iterator;
		typedef typename underlying_container_type::const_iterator const_iterator;
		typedef typename underlying_container_type::reverse_iterator reverse_iterator;
		typedef typename underlying_container_type::const_reverse_iterator const_reverse_iterator;

		typedef typename underlying_container_type::size_type size_type;
		typedef typename underlying_container_type::difference_type difference_type;
		typedef typename underlying_container_type::allocator_type allocator_type;

	private:
		struct search_result
		{
			size_t position;
			bool found;
		};

	private:
		underlying_container_type  _underlyingContainer;

	public:
		assoc_array( assoc_array&& ) = default;
		assoc_array( const assoc_array& ) = default;
		assoc_array& operator=( assoc_array&& ) = default;
		assoc_array& operator=( const assoc_array& ) = default;

		assoc_array()
		{ }

		explicit assoc_array( const allocator_type& allocator )
			: _underlyingContainer( allocator )
		{ }

		explicit assoc_array( size_t capacity, const allocator_type& allocator = allocator_type() )
			: _underlyingContainer( capacity, allocator )
		{ }

		value_type& operator[]( key_type&& key )
		{
			auto elementRef = this->_find_insert( std::move( key ) );
			return elementRef->value;
		}

		value_type& operator[]( const key_type& key )
		{
			auto elementRef = this->_find_insert( key );
			return elementRef->value;
		}

		value_type& at( const key_type& key )
		{
			size_t element_pos = this->_find( key );
			auto& element_entry = this->_underlyingContainer.at( element_pos );
			return element_entry.value;
		}

		const value_type& at( const key_type& key ) const
		{
			size_t element_pos = this->_find( key );
			const auto& element_entry = this->_underlyingContainer.at( element_pos );
			return element_entry.value;
		}

		iterator find( const key_type& key )
		{
			size_t element_pos = this->_find( key );
			return ( element_pos != invalid_position ) ? this->begin() + element_pos : this->end();
		}

		const_iterator find( const key_type& key ) const
		{
			size_t element_pos = this->_find( key );
			return ( element_pos != invalid_position ) ? this->begin() + element_pos : this->end();
		}

		template <class Key_type>
		iterator find( const Key_type& key )
		{
			size_t element_pos = this->_find( key );
			return ( element_pos != invalid_position ) ? this->begin() + element_pos : this->end();
		}

		template <class Key_type>
		const_iterator find( const Key_type& key ) const
		{
			size_t element_pos = this->_find( key );
			return ( element_pos != invalid_position ) ? this->begin() + element_pos : this->end();
		}

		// template <class Key_type, class Predicate>
		// iterator find(const Key_type& key, Predicate predicate)
		// {
		// 	size_t element_pos = this->_find(key, predicate);
		// 	return (element_pos != invalid_position) ? this->begin() + element_pos : this->end();
		// }
		// 
		// template <class Key_type, class Predicate>
		// const_iterator find(const Key_type& key, Predicate predicate) const
		// {
		// 	size_t element_pos = this->_find(key, predicate);
		// 	return (element_pos != invalid_position) ? this->begin() + element_pos : this->end();
		// }

		iterator begin()
		{
			return this->_underlyingContainer.begin();
		}

		const_iterator begin() const
		{
			return this->_underlyingContainer.begin();
		}

		iterator end()
		{
			return this->_underlyingContainer.end();
		}

		const_iterator end() const
		{
			return this->_underlyingContainer.end();
		}

		reverse_iterator rbegin()
		{
			return this->_underlyingContainer.rbegin();
		}

		const_reverse_iterator rbegin() const
		{
			return this->_underlyingContainer.rbegin();
		}

		reverse_iterator rend()
		{
			return this->_underlyingContainer.rend();
		}

		const_reverse_iterator rend() const
		{
			return this->_underlyingContainer.rend();
		}

		value_type& front()
		{
			return this->_underlyingContainer.front();
		}

		const value_type& front() const
		{
			return this->_underlyingContainer.front();
		}

		value_type& back()
		{
			return this->_underlyingContainer.back();
		}

		const value_type& back() const
		{
			return this->_underlyingContainer.back();
		}

		// template <class ... _Args>
		// iterator emplace(key_type&& key, _Args&&... args)
		// {
		// 	search_result result = this->_get_insert_position(key);
		// 
		// 	if (result.found)
		// 		return this->end();
		// 
		// 	auto insert_iter = this->_underlyingContainer.begin() + result.position;
		// 	return this->_underlyingContainer.emplace(insert_iter, std::move(key), std::forward<_Args>(args)...);
		// }
		// 
		// template <class ... _Args>
		// iterator emplace(const key_type& key, _Args&&... args)
		// {
		// 	search_result result = this->_get_insert_position(key);
		// 
		// 	if (result.found)
		// 		return this->end();
		// 
		// 	auto insert_iter = this->_underlyingContainer.begin() + result.position;
		// 	return this->_underlyingContainer.emplace(insert_iter, key, std::forward<_Args>(args)...);
		// }

		iterator insert( key_type&& key, value_type&& value )
		{
			search_result result = this->_get_insert_position( key );
			auto insert_iter = this->_underlyingContainer.begin() + result.position;

			if ( !result.found )
				insert_iter = this->_underlyingContainer.insert( insert_iter, element_type( std::move( key ), std::move( value ) ) );
			else
				this->_underlyingContainer[result.position].value = std::move( value );

			return this->_underlyingContainer.begin() + result.position;
		}

		iterator insert( key_type&& key, const value_type& value )
		{
			search_result result = this->_get_insert_position( key );
			auto insert_iter = this->_underlyingContainer.begin() + result.position;

			if ( !result.found )
				insert_iter = this->_underlyingContainer.insert( insert_iter, element_type( std::move( key ), value ) );
			else
				this->_underlyingContainer[result.position].value = value;

			return this->_underlyingContainer.begin() + result.position;
		}

		iterator insert( const key_type& key, value_type&& value )
		{
			search_result result = this->_get_insert_position( key );
			auto insert_iter = this->_underlyingContainer.begin() + result.position;

			if ( !result.found )
				insert_iter = this->_underlyingContainer.insert( insert_iter, element_type( key, std::move( value ) ) );
			else
				this->_underlyingContainer[result.position].value = std::move( value );

			return insert_iter;
		}

		iterator insert( const key_type& key, const value_type& value )
		{
			search_result result = this->_get_insert_position( key );
			auto insert_iter = this->_underlyingContainer.begin() + result.position;

			if ( !result.found )
				insert_iter = this->_underlyingContainer.insert( insert_iter, element_type( key, value ) );
			else
				this->_underlyingContainer[result.position].value = value;

			return this->_underlyingContainer.begin() + result.position;
		}

		iterator erase( iterator pos )
		{
			return this->_underlyingContainer.erase( pos );
		}

		iterator erase( iterator start, iterator end )
		{
			return this->_underlyingContainer.erase( start, end );
		}

		iterator remove( const key_type& key )
		{
			size_t elementIndex = this->_get_index_by_key( key );

			if ( elementIndex == invalid_position )
				return this->end();

			return this->_underlyingContainer.erase( elementIndex );
		}

		void clear()
		{
			this->_underlyingContainer.clear();
		}

		void reserve( size_t capacity )
		{
			this->_underlyingContainer.reserve( capacity );
		}

		size_t capacity() const
		{
			return this->_underlyingContainer.capacity();
		}

		size_t size() const
		{
			return this->_underlyingContainer.size();
		}

		bool empty() const
		{
			return this->_underlyingContainer.empty();
		}

		void swap( my_type& other )
		{
			std::swap( this->_underlyingContainer, other._underlyingContainer );
		}

	private:
		element_type* _data_ptr()
		{
			return this->_underlyingContainer.data();
		}

		const element_type* _data_ptr() const
		{
			return this->_underlyingContainer.data();
		}

		size_t _get_index_by_key( const key_type& key ) const
		{
			if ( this->empty() )
				return invalid_position;

			auto begin = this->_underlyingContainer.begin();
			auto end = this->_underlyingContainer.end();

			auto refElem = std::lower_bound( begin, end, key,
				[]( const element_type& lhs, const key_type& rhs ) -> bool {
				return _Comp()( lhs.key, rhs );
			} );

			bool found = ( refElem != end ) && ( refElem->key == key );
			return found ? refElem - begin : invalid_position;
		}

		template <class _Kx>
		search_result _get_insert_position( const _Kx& key ) const
		{
			if ( this->empty() )
				return search_result{ 0, false };

			auto begin = this->_underlyingContainer.begin();
			auto end = this->_underlyingContainer.end();

			auto refElem = std::lower_bound( begin, end, key,
				[]( const element_type& lhs, const _Kx& rhs ) -> bool {
				return _Comp()( lhs.key, rhs );
			} );

			bool found = ( refElem != end ) && ( refElem->key == key );
			return search_result{ static_cast<size_t>( refElem - begin ), found };
		}

		template <class _Kx, class... _Pred>
		size_t _find( const _Kx& key, _Pred... predicate )
		{
			search_result search_res = this->_get_insert_position( key, std::forward<_Pred>( predicate )... );
			return search_res.found ? search_res.position : invalid_position;
		}

		template <class _Kx, class... _Pred>
		size_t _find( const _Kx& key, _Pred... predicate ) const
		{
			search_result search_res = this->_get_insert_position( key, std::forward<_Pred>( predicate )... );
			return search_res.found ? search_res.position : invalid_position;
		}

		iterator _find_insert( key_type&& key )
		{
			search_result result = this->_get_insert_position( key );
			auto insert_iter = this->_underlyingContainer.begin() + result.position;

			if ( !result.found )
				insert_iter = this->_underlyingContainer.insert( insert_iter, element_type( std::move( key ), value_type() ) );

			return insert_iter;
		}

		iterator _find_insert( const key_type& key )
		{
			search_result result = this->_get_insert_position( key );
			auto insert_iter = this->_underlyingContainer.begin() + result.position;

			if ( !result.found )
				insert_iter = this->_underlyingContainer.insert( insert_iter, element_type( key, value_type() ) );

			return insert_iter;
		}
	};


	template <class _Key, class _Value, class _Alloc>
	inline typename assoc_array<_Key, _Value, _Alloc>::iterator begin( assoc_array<_Key, _Value, _Alloc>& container )
	{
		return container.begin();
	}

	template <class _Key, class _Value, class _Alloc>
	inline typename assoc_array<_Key, _Value, _Alloc>::const_iterator begin( const assoc_array<_Key, _Value, _Alloc>& container )
	{
		return container.begin();
	}

	template <class _Key, class _Value, class _Alloc>
	inline typename assoc_array<_Key, _Value, _Alloc>::iterator end( assoc_array<_Key, _Value, _Alloc>& container )
	{
		return container.end();
	}

	template <class _Key, class _Value, class _Alloc>
	inline typename assoc_array<_Key, _Value, _Alloc>::const_iterator end( const assoc_array<_Key, _Value, _Alloc>& container )
	{
		return container.end();
	}


}


#endif /* __STDX_ASSOC_ARRAY__ */
