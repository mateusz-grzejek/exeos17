
#ifndef __STDX_THR_COMMON_H__
#define __STDX_THR_COMMON_H__

#include "common.h"


namespace stdx
{


	namespace impl
	{

		void hw_pause();
		void thrd_yield();

	}


	template <int64_t busySpinCount = 4, int64_t spinCountBeforeYield = 16>
	inline void thr_yield( int64_t value )
	{
		if (value < busySpinCount)
		{
		}
		else if (value < spinCountBeforeYield)
		{
			impl::hw_pause();
		}
		else
		{
			impl::thrd_yield();
		}
	}


	inline void thr_yield_counter( int64_t value )
	{
		constexpr auto busySpinCount = 6;
		constexpr auto spinCountBeforeYield = 24;

		thr_yield<busySpinCount, spinCountBeforeYield>( value );
	}


	inline void thr_yield_spinlock( int64_t value )
	{
		constexpr auto busySpinCount = 8;
		constexpr auto spinCountBeforeYield = 16;

		thr_yield<busySpinCount, spinCountBeforeYield>( value );
	}


}


#endif /* __STDX_THR_COMMON_H__ */
