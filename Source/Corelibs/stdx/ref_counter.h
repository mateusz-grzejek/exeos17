
#ifndef __STDX_REF_COUNTER_H__
#define __STDX_REF_COUNTER_H__

#include "common.h"
#include <atomic>


namespace stdx
{


	typedef size_t ref_counter_value_t;


	enum class ref_counter_type : uint32_t
	{
		atomic = 1,
		atomic_sync = 2,
		non_atomic = 3,
		undefined
	};


	class ref_counter
	{
	private:
		ref_counter_value_t _refsNum;

	public:
		ref_counter( const ref_counter& ) = delete;
		ref_counter& operator=( const ref_counter& ) = delete;

		ref_counter()
			: _refsNum( 1 )
		{ }

		explicit ref_counter( ref_counter_value_t c )
			: _refsNum( c )
		{ }

		ref_counter_value_t increment()
		{
			return ++this->_refsNum;
		}

		ref_counter_value_t increment_cnz()
		{
			if ( this->_refsNum > 0 )
			{
				++this->_refsNum;
			}

			return this->_refsNum;
		}

		ref_counter_value_t decrement()
		{
			return --this->_refsNum;
		}

		ref_counter_value_t get_value() const
		{
			return this->_refsNum;
		}
	};


	class atomic_ref_counter
	{
	private:
		std::atomic<ref_counter_value_t> _refsNum;

	public:
		atomic_ref_counter( const atomic_ref_counter& ) = delete;
		atomic_ref_counter& operator=( const atomic_ref_counter& ) = delete;

		atomic_ref_counter()
			: _refsNum( 1 )
		{ }

		explicit atomic_ref_counter( ref_counter_value_t c )
			: _refsNum( c )
		{ }

		ref_counter_value_t increment()
		{
			ref_counter_value_t refs_num = this->_refsNum.fetch_add( 1, std::memory_order_relaxed );
			return refs_num + 1;
		}

		ref_counter_value_t increment_cnz()
		{
			for ( ref_counter_value_t refsNum = this->_refsNum.load( std::memory_order_relaxed ); ; )
			{
				if ( refsNum == 0 )
				{
					return 0;
				}

				if ( this->_refsNum.compare_exchange_weak( refsNum, refsNum + 1, std::memory_order_relaxed ) )
				{
					return refsNum + 1;
				}
			}
		}

		ref_counter_value_t decrement()
		{
			ref_counter_value_t refs_num = this->_refsNum.fetch_sub( 1, std::memory_order_relaxed );
			return refs_num - 1;
		}

		ref_counter_value_t get_value() const
		{
			return this->_refsNum.load( std::memory_order_relaxed );
		}
	};


	class sync_ref_counter
	{
	private:
		std::atomic<ref_counter_value_t> _refsNum;

	public:
		sync_ref_counter( const sync_ref_counter& ) = delete;
		sync_ref_counter& operator=( const sync_ref_counter& ) = delete;

		sync_ref_counter()
			: _refsNum( 1 )
		{ }

		explicit sync_ref_counter( ref_counter_value_t c )
			: _refsNum( c )
		{ }

		ref_counter_value_t increment()
		{
			ref_counter_value_t refs_num = this->_refsNum.fetch_add( 1, std::memory_order_release );
			return refs_num + 1;
		}

		ref_counter_value_t increment_cnz()
		{
			for (ref_counter_value_t refsNum = this->_refsNum.load( std::memory_order_relaxed ); ; )
			{
				if (refsNum == 0)
				{
					return 0;
				}

				if (this->_refsNum.compare_exchange_weak( refsNum, refsNum + 1, std::memory_order_acq_rel, std::memory_order_relaxed ))
				{
					return refsNum + 1;
				}
			}
		}

		ref_counter_value_t decrement()
		{
			ref_counter_value_t refs_num = this->_refsNum.fetch_sub( 1, std::memory_order_release );
			return refs_num - 1;
		}

		ref_counter_value_t get_value() const
		{
			return this->_refsNum.load( std::memory_order_relaxed );
		}
	};


	template <ref_counter_type>
	struct ref_counter_class;

	template <>
	struct ref_counter_class<ref_counter_type::atomic>
	{
		typedef atomic_ref_counter type;
	};

	template <>
	struct ref_counter_class<ref_counter_type::atomic_sync>
	{
		typedef sync_ref_counter type;
	};

	template <>
	struct ref_counter_class<ref_counter_type::non_atomic>
	{
		typedef ref_counter type;
	};


}


#endif /* __STDX_REF_COUNTER_H__ */
