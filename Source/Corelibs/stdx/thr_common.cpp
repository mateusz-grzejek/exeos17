
#include "thr_common.h"
#include <thread>
#include <emmintrin.h>


namespace stdx
{
	namespace impl
	{

		void hw_pause()
		{
			_mm_pause();
		}


		void thrd_yield()
		{
			std::this_thread::yield();
		}

	}
}
