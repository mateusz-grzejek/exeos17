
#ifndef __STDX_STATIC_ALGO_H__
#define __STDX_STATIC_ALGO_H__

#include "common.h"
#include "type_traits.h"
#include <numeric>
#include <ratio>


namespace stdx
{


	template <intmax_t tValue>
	struct static_abs_value
	{
		static const intmax_t value = (tValue < 0) ? -tValue : tValue;
	};


	template <intmax_t tValue>
	struct static_sign
	{
		static const int32_t value = (tValue < 0) ? -1 : 1;
	};


	namespace impl
	{

		template <intmax_t tM, intmax_t tN>
		struct static_gcd_impl
		{
			static const intmax_t value = static_gcd_impl<tN, tM % tN>::value;
		};

		template <intmax_t tX>
		struct static_gcd_impl<tX, 0>
		{
			static const intmax_t value = tX;
		};

	}

	template <typename T, T tM, T tN>
	struct static_gcd
	{
		static const T value = impl::static_gcd_impl<static_abs_value<tM>::value, static_abs_value<tN>::value>::value;
	};


	namespace impl
	{

		template <typename T, bool _Cond, T tVal, T tPow, T tExp>
		struct static_pow2_round_impl;

		template <typename T, T tVal, T tPow, T tExp>
		struct static_pow2_round_impl<T, false, tVal, tPow, tExp>
		{
			static const T exponent = static_pow2_round_impl<T, tVal <= (tPow << 1), tVal, tPow << 1, tExp + 1>::exponent;
			static const T value = static_pow2_round_impl<T, tVal <= (tPow << 1), tVal, tPow << 1, tExp + 1>::value;
		};

		template <typename T, T tVal, T tPow, T tExp>
		struct static_pow2_round_impl<T, true, tVal, tPow, tExp>
		{
			static const T exponent = tExp;
			static const T value = tPow;
		};

	}


	template <typename T, T tVal>
	struct static_pow2_round
	{
		static const T exponent = impl::static_pow2_round_impl<T, tVal <= 1, tVal, 1, 0>::exponent;
		static const T value = impl::static_pow2_round_impl<T, tVal <= 1, tVal, 1, 0>::value;
	};


	template <int... tSeq>
	struct static_integer_sequence
	{ };

	template <int tN, int... tSeq>
	struct static_integer_sequence_generator : static_integer_sequence_generator<tN-1, tN-1, tSeq...>
	{ };

	template <int... tSeq>
	struct static_integer_sequence_generator<0, tSeq...>
	{
		typedef static_integer_sequence<tSeq...> Type;
	};


	template <class T, class... _Tn>
	struct static_type_counter
	{
		static const size_t value = 1 + static_type_counter<_Tn...>::value;
	};

	template <class T>
	struct static_type_counter<T>
	{
		static const size_t value = 1;
	};


	template <typename T, T tNum, T _Den = 1>
	struct static_ratio : public std::ratio<static_cast<intmax_t>(tNum), static_cast<intmax_t>(_Den)>
	{
		typedef std::ratio<static_cast<intmax_t>(tNum), static_cast<intmax_t>(_Den)> base_type;

		template <typename _Fp>
		static constexpr _Fp gettValue()
		{
			return static_cast<_Fp>(tNum) / static_cast<_Fp>(_Den);
		}
	};


	template <intmax_t tVal1, intmax_t tVal2, intmax_t... tValN>
	struct static_max_constant
	{
		static constexpr intmax_t value = static_max_constant<tVal1, static_max_constant<tVal2, tValN...>::value>::value;
	};

	template <intmax_t tVal1, intmax_t tVal2>
	struct static_max_constant<tVal1, tVal2>
	{
		static constexpr intmax_t value = tVal1 >= tVal2 ? tVal1 : tVal2;
	};


	template <intmax_t tVal1, intmax_t tVal2, intmax_t... tValN>
	struct static_min_constant
	{
		static constexpr intmax_t value = static_min_constant<static_max_constant<tVal2, tVal1>::value, tValN...>::value;
	};

	template <intmax_t tVal1, intmax_t tVal2>
	struct static_min_constant<tVal1, tVal2>
	{
		static constexpr intmax_t value = tVal1 <= tVal2 ? tVal1 : tVal2;
	};


	template <class... TTypes>
	struct static_max_size_of
	{
		static constexpr size_t value = static_cast<size_t>(static_max_constant<sizeof(TTypes)...>::value);
	};


	template <class... TTypes>
	struct static_max_alignment_of
	{
		static constexpr size_t value = static_cast<size_t>(static_max_constant<alignof(TTypes)...>::value);
	};


}


#endif /* __STDX_STATIC_ALGO_H__ */
