
include_guard()

ebsCoreAddModuleRef( "VPRBase" )


function( _ebsVPRGenImplGetFullFileMaskList pBasePath pFileTypeMask outFullList )
    foreach( fileType ${pFileTypeMask} )
        set( fullMaskList "${fullMaskList}" "${pBasePath}/*${fileType}" )
    endforeach()
	set( ${outFullList} ${fullMaskList} PARENT_SCOPE )
endfunction()


function( _ebsVPRGenImplGetProjectBaseDir pProjectID outBaseDir )
    
    ebsVMSBuildQueryProjectProperty( "OVERWRITE" ${pProjectID} "INCLUDE_SUBDIR" includeSubdir )
    ebsVMSBuildQueryProjectAttribute( ${pProjectID} "INCLUDE_SUBDIR" )
    ebsVMSBuildQueryProjectAttribute( ${pProjectID} "INCLUDE_SUBDIR" )

endfunction()


function( _ebsVPRGenImplGetProjectIncludePath pProjectID outIncludePath )
    
    ebsVMSBuildQueryProjectProperty( projectIncludePath ${pProjectID} "INCLUDE_SUBDIR" )
    set( ${outIncludePath} ${projectIncludePath} PARENT_SCOPE )

endfunction()


function( _ebsVPRGenImplGetProjectSourceSubdir pProjectID outSrcDir )

    ebsVMSBuildGetProjectGroupID( ${pProjectID} projectGroupID )

    if( projectGroupID )
    else()
        ebsVMSBuildGetGroupVariable( ${projectGroupID} ROOT_DIR groupRootDir )
        ebsVMSBuildGetGroupVariable( ${projectGroupID} SRC_SUBDIR groupSrcSubdir )
    endif()

endfunction()


function( _ebsVPRGenImplGetProjectSourceFileList pProjectID outFileList )

    ebsWorkspaceGetProjectGroupID( ${pProjectID} projectGroupID )

    _ebsVMSWorkspaceVGroupGetInternal( ${projectGroupID} "ROOT_DIR" groupRootDir )
    _ebsVMSWorkspaceVGroupGetInternal( ${projectGroupID} "SRC_SUBDIR" groupSrcSubdir )

    set( srcBasePath "${CMAKE_SOURCE_DIR}/${groupRootDir}/${groupSrcSubdir}" )

    #
    _ebsVMSBuildVGlobalGetInternal( "SRC_FILE_TYPE_MASK" fileTypeMask )
        
    if( EXISTS ${srcBasePath} )
    
        _ebsVMSBuildVProjectGetInternal( ${pProjectID} "SRC_SUBDIR_LIST" projectSrcSubdirList )

        foreach( srcSubdir ${projectSrcSubdirList} )

            if( "${srcSubdir}" STREQUAL "." )
                set( srcPath "${srcBasePath}" )
                set( subdirSrcGroupName "" )
            else()
                set( srcPath "${srcBasePath}/${srcSubdir}" )
                string( REPLACE "/" "\\" subdirSrcGroupName "${srcSubdir}" )
                set( subdirSrcGroupName "\\${subdirSrcGroupName}" )
            endif()
            
            _ebsVPRGenImplGetFullFileMaskList( ${srcPath} ${fileTypeMask} fileGlobMask )

            file( GLOB sourceFiles ${fileGlobMask} )

            source_group( "${subdirSrcGroupName}" FILES ${sourceFiles} )

            set( projectSourceFileList ${projectSourceFileList} ${sourceFiles} )
            
        endforeach()
    endif()
    
    _ebsVMSBuildVGlobalGetInternal( "SRC_SPECIFIC_DIR_LIST" specificSrcSubdirList )

    foreach( specificSrcSubdir ${specificSrcSubdirList} )

        set( srcPath "${srcBasePath}/${specificSrcSubdir}" )

        _ebsVPRGenImplGetFullFileMaskList( ${srcPath} ${fileTypeMask} fileGlobMask )

        file( GLOB sourceFiles ${fileGlobMask} )
        source_group( "\\${specificSrcSubdir}" FILES ${sourceFiles} )
        set( projectSourceFileList ${projectSourceFileList} ${sourceFiles} )

    endforeach()
	
	set( ${outFileList} ${projectSourceFileList} PARENT_SCOPE )

endfunction()
