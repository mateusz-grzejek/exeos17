
include_guard()

ebsCoreAddModuleRef( "VMSBase" )

#
set( _EBS_VMS_BUILD_DEF_VARCATEGORYLIST
    "CONFIG" #
    "VGLOBAL" # Category for global variables, set for all projects within the workspace
    "VGROUP" # Category for per-group variables, set for a single build group
    "VPROJECT" # Category for per-project variables, set for a single project
)

#
set( _EBS_VMS_BUILD_DEF_VARLIST_CONFIG
)

#
set( _EBS_VMS_BUILD_DEF_VARLIST_CONFIG_INTERNAL
    ${_EBS_VMS_BUILD_DEF_VARLIST_CONFIG}
    "OUTPUT_SUBDIR"
    "OUTPUT_SUBDIR_ARCH"
    "OUTPUT_SUBDIR_BUILD"
)

# List of BUILD:: variables than can be set for each build item (and globally):
set( _EBS_VMS_BUILD_DEF_VARLIST_VCOMMON
    "BIN_PATH"
    "COMPILE_DEFS" # [property]
    "COMPILE_FLAGS" # [property]
    "INCLUDE_SEARCH_PATH" # [property]
    "LINK_FLAGS" # [property]
    "LINK_INPUT" # [property]
    "LINK_SEARCH_PATH" # [property]
    "PCH_ACTIVE" # [attribute]
    "PCH_FILE_HEADER"
    "PCH_FILE_SOURCE"
    "RUNTIME_BINARIES" # [property]
)

# List of BUILD::GLOBAL variables:
set( _EBS_VMS_BUILD_DEF_VARLIST_VGLOBAL
    ${_EBS_VMS_BUILD_DEF_VARLIST_VCOMMON}
    "OUTPUT_SUBDIR"
    "OUTPUT_SUBDIR_ARCH"
    "OUTPUT_SUBDIR_BUILD"
    "SRC_FILE_TYPE_MASK"
    "SRC_SPECIFIC_DIR_LIST"
)

# List of BUILD::GROUP variables:
set( _EBS_VMS_BUILD_DEF_VARLIST_VGROUP
    ${_EBS_VMS_BUILD_DEF_VARLIST_VCOMMON}
    "ARTIFACT_TYPE" # [attribute]
    "INCLUDE_SUBDIR" # [attribute]
    "OUTPUT_FILENAME_PREFIX"
    "OUTPUT_FILENAME_SUFFIX"
)

# List of BUILD::PROJECT variables:
set( _EBS_VMS_BUILD_DEF_VARLIST_VPROJECT
    ${_EBS_VMS_BUILD_DEF_VARLIST_VCOMMON}
    "ARTIFACT_TYPE" # [attribute]
    "INCLUDE_SUBDIR" # [attribute]
    "INTERNAL_DEPENDENCIES"
    "SRC_FILE_LIST"
    "SRC_SUBDIR_LIST"
)


#
set( _EBS_VMS_BUILD_DEF_VARVALUELIST_NCAT_ARTIFACT_TYPE
    "EXE"
    "LIBOBJ"
    "LIBSHARED"
)

# Returns string representation of a filter. Filter is reduced to a minimal disnctive
# value, that is, trailing ALLs are trimmed.
function( _ebsVMSBuildImplGetFilterStr pCompiler pOS pArch pBuildcfg outFilter )
    if( "${pBuildcfg}" STREQUAL "-" )
        unset( pBuildcfg )
        if( "${pArch}" STREQUAL "-" )
            unset( pArch )
            if( "${pOS}" STREQUAL "-" )
                unset( pOS )
                if( "${pCompiler}" STREQUAL "-" )
                    unset( pCompiler )
                endif()
            endif()
        endif()
    endif()

    if( pOS )
        set( pOS "_${pOS}" )
    endif()

    if( pArch )
        set( pArch "_${pArch}" )
    endif()

    if( pBuildcfg )
        set( pBuildcfg "_${pBuildcfg}" )
    endif()

    set( ${outFilter} "${pCompiler}${pOS}${pArch}${pBuildcfg}" PARENT_SCOPE )
endfunction()


function( _ebsVMSBuildImplParseVarargInput outFilter )
    
    if( ${ARGC} GREATER 0 )

        set( vargList "COMPILER" "OS" "ARCH" "BUILDCFG" )

        cmake_parse_arguments( BUILDFILTER "" "${vargList}" "${valueArg}" ${ARGN} )

        if( DEFINED BUILDFILTER_UNPARSED_ARGUMENTS )
            message( "<!BUILD> Filters.ParseVararg: unknown filters: ${BUILDFILTER_UNPARSED_ARGUMENTS}" )
            message( "<!BUILD> Allowed filters: ${vargList}" )
        endif()

        if( BUILDFILTER_COMPILER OR BUILDFILTER_OS OR BUILDFILTER_ARCH OR BUILDFILTER_BUILDCFG )

            if( NOT BUILDFILTER_COMPILER )
                set( BUILDFILTER_COMPILER "-" )
            endif()
            if( NOT BUILDFILTER_OS )
                set( BUILDFILTER_OS "-" )
            endif()
            if( NOT BUILDFILTER_ARCH )
                set( BUILDFILTER_ARCH "-" )
            endif()
            if( NOT BUILDFILTER_BUILDCFG )
                set( BUILDFILTER_BUILDCFG "-" )
            endif()

            _ebsVMSBuildImplGetFilterStr( ${BUILDFILTER_COMPILER} ${BUILDFILTER_OS} ${BUILDFILTER_ARCH} ${BUILDFILTER_BUILDCFG} filterStr )
            
            set( ${outFilter} ${filterStr} PARENT_SCOPE )

        endif()

    endif()

endfunction()


function( _ebsVMSBuildImplCheckVariableRequest pAllowInternal pVarCategory pWorkspaceItem pVarName pValue )

    # TODO

endfunction()


function( _ebsVMSBuildImplGetVariableRefName pWorkspaceItem pVarName pFilter outRefName )

    if( pWorkspaceItem )
        set( workspaceItemStr "${pWorkspaceItem}_" )
    endif()

    if( pFilter )
        set( filterStr "_${pFilter}" )
    endif()

    set( ${outRefName} "${workspaceItemStr}${pVarName}${filterStr}" PARENT_SCOPE )

endfunction()


macro( _ebsVMSBuildImplVarGet pVarCategory pWorkspaceItem pVarName pFilter outValue )
    _ebsVMSBuildImplCheckVariableRequest( TRUE ${pVarCategory} "${pWorkspaceItem}" ${pVarName} "" )
    _ebsVMSBuildImplGetVariableRefName( "${pWorkspaceItem}" ${pVarName} "${pFilter}" variableRefName )
    _ebsVMSBaseVarGet( "BUILD" ${pVarCategory} ${variableRefName} ${outValue} )
endmacro()

macro( _ebsVMSBuildImplVarAppend pAllowInternal pVarCategory pWorkspaceItem pVarName pFilter pValue )
    _ebsVMSBuildImplCheckVariableRequest( ${pAllowInternal} ${pVarCategory} "${pWorkspaceItem}" ${pVarName} "${pValue}" )
    _ebsVMSBuildImplGetVariableRefName( "${pWorkspaceItem}" ${pVarName} "${pFilter}" variableRefName )
    _ebsVMSBaseVarAppend( ${pAllowInternal} "BUILD" ${pVarCategory} ${variableRefName} "${pValue}" )
endmacro()

macro( _ebsVMSBuildImplVarSet pAllowInternal pVarCategory pWorkspaceItem pVarName pFilter pValue )
    _ebsVMSBuildImplCheckVariableRequest( ${pAllowInternal} ${pVarCategory} "${pWorkspaceItem}" ${pVarName} "${pValue}" )
    _ebsVMSBuildImplGetVariableRefName( "${pWorkspaceItem}" ${pVarName} "${pFilter}" variableRefName )
    _ebsVMSBaseVarSet( ${pAllowInternal} "BUILD" ${pVarCategory} ${variableRefName} "${pValue}" )
endmacro()

macro( _ebsVMSBuildImplVarUnset pAllowInternal pVarCategory pWorkspaceItem pVarName pFilter )
    _ebsVMSBuildImplCheckVariableRequest( ${pAllowInternal} ${pVarCategory} "${pWorkspaceItem}" ${pVarName} "" )
    _ebsVMSBuildImplGetVariableRefName( "${pWorkspaceItem}" ${pVarName} "${pFilter}" variableRefName )
    _ebsVMSBaseVarUnset( ${pAllowInternal} "BUILD" ${pVarCategory} ${variableRefName} )
endmacro()


macro( _ebsVMSBuildConfigGetInternal pVarName outValue )
    _ebsVMSBuildImplVarGet( "CONFIG" "" ${pVarName} "" ${outValue} )
endmacro()

macro( _ebsVMSBuildConfigAppendInternal pVarName pValue )
    _ebsVMSBuildImplVarAppend( TRUE "CONFIG" "" ${pVarName} "" "${pValue}" )
endmacro()

macro( _ebsVMSBuildConfigSetInternal pVarName pValue )
    _ebsVMSBuildImplVarSet( TRUE "CONFIG" "" ${pVarName} "" "${pValue}" )
endmacro()

macro( _ebsVMSBuildConfigUnsetInternal pVarName )
    _ebsVMSBuildImplVarUnset( TRUE "CONFIG" "" ${pVarName} "" )
endmacro()


macro( _ebsVMSBuildVGlobalGetInternal pVarName outValue )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarGet( "VGLOBAL" "" ${pVarName} "${varFilter}" ${outValue} )
endmacro()

macro( _ebsVMSBuildVGlobalAppendInternal pVarName pValue )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarAppend( TRUE "VGLOBAL" "" ${pVarName} "${varFilter}" "${pValue}" )
endmacro()

macro( _ebsVMSBuildVGlobalSetInternal pVarName pValue )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarSet( TRUE "VGLOBAL" "" ${pVarName} "${varFilter}" "${pValue}" )
endmacro()

macro( _ebsVMSBuildVGlobalUnsetInternal pVarName )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarUnset( TRUE "VGLOBAL" "" ${pVarName} "${varFilter}" )
endmacro()


macro( _ebsVMSBuildVGroupGetInternal pGroupID pVarName outValue )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarGet( "VGROUP" ${pGroupID} ${pVarName} "${varFilter}" ${outValue} )
endmacro()

macro( _ebsVMSBuildVGroupAppendInternal pGroupID pVarName pValue )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarAppend( TRUE "VGROUP" ${pGroupID} ${pVarName} "${varFilter}" "${pValue}" )
endmacro()

macro( _ebsVMSBuildVGroupSetInternal pGroupID pVarName pValue )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarSet( TRUE "VGROUP" ${pGroupID} ${pVarName} "${varFilter}" "${pValue}" )
endmacro()

macro( _ebsVMSBuildVGroupUnsetInternal pGroupID pVarName )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarUnset( TRUE "VGROUP" ${pGroupID} ${pVarName} "${varFilter}" )
endmacro()


macro( _ebsVMSBuildVProjectGetInternal pProjectID pVarName outValue )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarGet( "VPROJECT" ${pProjectID} ${pVarName} "${varFilter}" ${outValue} )
endmacro()

macro( _ebsVMSBuildVProjectAppendInternal pProjectID pVarName pValue )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarAppend( TRUE "VPROJECT" ${pProjectID} ${pVarName} "${varFilter}" "${pValue}" )
endmacro()

macro( _ebsVMSBuildVProjectSetInternal pProjectID pVarName pValue )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarSet( TRUE "VPROJECT" ${pProjectID} ${pVarName} "${varFilter}" "${pValue}" )
endmacro()

macro( _ebsVMSBuildVProjectUnsetInternal pProjectID pVarName )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarUnset( TRUE "VPROJECT" ${pProjectID} ${pVarName} "${varFilter}" )
endmacro()


function( _ebsVMSBuildImplQueryVariableInternal pInherit pGroupID pProjectID pVarName pCompiler pOS pArch pBuildcfg outValue )
    
	set( compilerFilterList "-" )
	set( osFilterList       "-" )
	set( archFilterList     "-" )
    set( buildcfgFilterList "-" )
    
    if( pCompiler )
        set( compilerFilterList "${compilerFilterList};${pCompiler}" )
    endif()

    if( pOS )
        set( osFilterList "${osFilterList};${pOS}" )
    endif()

    if( pArch )
        set( archFilterList "${archFilterList};${pArch}" )
    endif()

    if( pBuildcfg )
        set( buildcfgFilterList "${buildcfgFilterList};${pBuildcfg}" )
    endif()
    
    # Since name pattern for the build variables is fixed, simply iterate over possible names.
	foreach( compiler ${compilerFilterList} )
	    foreach( os ${osFilterList} )
			foreach( arch ${archFilterList} )
                foreach( buildcfg ${buildcfgFilterList} )
                
                    # Manually query the filter string to avoid parsing three times.
                    _ebsVMSBuildImplGetFilterStr( ${compiler} ${os} ${arch} ${buildcfg} varFilter )
                    
                    if( pProjectID )
                        #
                        _ebsVMSBuildImplGetVariableRefName( ${pProjectID} ${pVarName} "${varFilter}" projectVarRefName )
                        # If projectID was specified, query the project level variable.
                        _ebsVMSBaseVarGet( "BUILD" "VPROJECT" ${projectVarRefName} projectValue )

                        if( projectValue )
                            if( pInherit )
                                ebsListAppendUnique( result ${projectValue} )
                                unset( projectValue ) # !!!
                            else()
                                set( ${outValue} ${projectValue} PARENT_SCOPE )
                                return()
                            endif()
                        endif()
                    endif()
                    
                    if( pGroupID )
                        #
                        _ebsVMSBuildImplGetVariableRefName( ${pGroupID} ${pVarName} "${varFilter}" groupVarRefName )
                        # If groupID was specified, query the group level variable.
                        _ebsVMSBaseVarGet( "BUILD" "VGROUP" ${groupVarRefName} groupValue )

                        if( groupValue )
                            if( pInherit )
                                ebsListAppendUnique( result ${groupValue} )
                                unset( groupValue ) # !!!
                            else()
                                set( ${outValue} ${groupValue} PARENT_SCOPE )
                                return()
                            endif()
                        endif()
                    endif()
                    
                    #
                    _ebsVMSBuildImplGetVariableRefName( "" ${pVarName} "${varFilter}" globalVarRefName )
                    # Query variable using Base layer to bypass the filter parsing.
                    _ebsVMSBaseVarGet( "BUILD" "VGLOBAL" ${globalVarRefName} globalValue )

                    if( globalValue )
                        if( pInherit )
                            ebsListAppendUnique( result ${globalValue} )
                            unset( globalValue ) # These unsets are crucial - result variables have function scope!
                        else()
                            set( ${outValue} ${globalValue} PARENT_SCOPE )
                            return()
                        endif()
					endif()
				endforeach()
			endforeach()
		endforeach()
    endforeach()
    
    set( ${outValue} ${result} PARENT_SCOPE )
    
endfunction()


macro( _ebsBuildImplQueryVariable pMode pGroupID pProjectID pVarName outValue )

    if( NOT pMode )
        set( pMode "INHERIT" )
    endif()

    ebsVMSSysconfEnvhostGet( "COMPILER" compiler )
    ebsVMSSysconfEnvtargetGet( "OS" os )
    ebsVMSSysconfEnvtargetGet( "ARCH" arch )
    ebsVMSSysconfEnvtargetGet( "BUILDCFG" buildcfg )

    if( ${pMode} STREQUAL "OVERWRITE" )
        _ebsVMSBuildImplQueryVariableInternal( FALSE "${pGroupID}" "${pProjectID}" ${pVarName} ${compiler} ${os} ${arch} ${buildcfg} ${outValue} )
    else()
        _ebsVMSBuildImplQueryVariableInternal( TRUE "${pGroupID}" "${pProjectID}" ${pVarName} ${compiler} ${os} ${arch} ${buildcfg} ${outValue} )
    endif()

endmacro()
