
include_guard()


# Core APIs

# Returns internal (EBS) string describing the system - either host
# or target (depending on the boolean value of the first parameter).
function( _ebsCoreGetSystemName pHost outSystemName )
    if( pHost )
        set( CMAKE_VAR_PREFIX "CMAKE_HOST_" )
    endif()

    if( ${CMAKE_VAR_PREFIX}ANDROID )
        set( systemName "Android" )
    elseif( ${CMAKE_VAR_PREFIX}SOLARIS )
        set( systemName "Oracle Solaris" )
    elseif( ${CMAKE_VAR_PREFIX}APPLE )
        if( ${CMAKE_SYSTEM_NAME} STREQUAL "Darwin" )
            set( systemName "MacOSX" )
        else()
            set( systemName "iOS" )
        endif()
    elseif( ${CMAKE_VAR_PREFIX}UNIX )
        if( ${CMAKE_SYSTEM_NAME} STREQUAL "Linux" )
            set( systemName "Linux" )
        endif()
    elseif( ${CMAKE_VAR_PREFIX}WIN32 )
        if( ${CMAKE_SYSTEM_NAME} STREQUAL "Windows" )
            set( systemName "WinDesktop" )
        endif()
    endif()

    set( ${outSystemName} ${systemName} PARENT_SCOPE )
endfunction()
