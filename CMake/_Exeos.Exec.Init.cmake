
function( exsInitialize )

    foreach( groupID ${EXEOS_CCV_WORKSPACE_GROUP_LIST} )

        #
        ebsWorkspaceRegisterGroup( ${groupID} )

        ebsWorkspaceSetGroupProjectIDPrefix( ${groupID} ${EXEOS_CCV_WORKSPACE_GROUP_PROJECT_ID_PREFIX_${groupID}} )
        ebsWorkspaceSetGroupRootDir( ${groupID} "${EXEOS_CCV_WORKSPACE_GROUP_ROOT_DIR_${groupID}}" )
        ebsWorkspaceSetGroupProjectSubdir( ${groupID} "${EXEOS_CCV_WORKSPACE_GROUP_PROJECT_SUBDIR_${groupID}}" )

        if( DEFINED EXEOS_CCV_WORKSPACE_GROUP_DEPENDENCY_WHITELIST_${groupID} )
            ebsWorkspaceSetGroupDependencyWhitelist( ${groupID} "${EXEOS_CCV_WORKSPACE_GROUP_DEPENDENCY_WHITELIST_${groupID}}" )
        endif()

        if( DEFINED EXEOS_CCV_WORKSPACE_GROUP_OS_SUPPORT_LIST_${groupID} )
            ebsWorkspaceSetGroupTargetPlatformSupport( ${groupID} "${EXEOS_CCV_WORKSPACE_GROUP_OS_SUPPORT_LIST_${groupID}}" )
        endif()

        if( DEFINED EXEOS_CCV_BUILD_GROUP_ARTIFACT_TYPE_${groupID} )
            ebsBuildSetGroupArtifactType( ${groupID} "${EXEOS_CCV_BUILD_GROUP_ARTIFACT_TYPE_${groupID}}" )
        endif()

        #
        foreach( projectName ${EXEOS_CCV_WORKSPACE_PROJECT_LIST_${groupID}} )

            ebsWorkspaceRegisterProject( ${groupID} ${projectName} projectID )

            if( DEFINED EXEOS_CCV_WORKSPACE_PROJECT_OS_SUPPORT_LIST_${groupID}_${projectName} )
                ebsWorkspaceSetProjectTargetPlatformSupport( ${projectID} "${EXEOS_CCV_WORKSPACE_PROJECT_OS_SUPPORT_LIST_${groupID}_${projectName}}" )
            endif()
             
        endforeach()

    endforeach()

endfunction()
