
include_guard()

ebsCoreAddModuleRef( "VMSSysconf.Defs" )


#
function( _ebsVMSSysconfEnvTargetDetectPlatform )

	# Target system may be set via config for cross-compiling, check.
	if( NOT EBS_VAR_SYSCONF_ENVTARGET_OS )
		_ebsCoreGetSystemName( FALSE targetOSName )
		_ebsVMSSysconfEnvtargetSetInternal( OS ${targetOSName} )
	endif()

	if( NOT EBS_VAR_SYSCONF_ENVTARGET_OS )
		message( FATAL_ERROR "Current platform (${CMAKE_SYSTEM_NAME}) is not supported!" )
	endif()

	if( ${EBS_VAR_SYSCONF_ENVTARGET_OS} STREQUAL "Android" )
		_ebsVMSSysconfEnvtargetSetInternal( PLATFORM "Android" )
		_ebsVMSSysconfEnvtargetSetInternal( SUBSYSTEM "Android" )
		_ebsVMSSysconfEnvtargetSetInternal( ARCHDEF32 "ARM" )
		_ebsVMSSysconfEnvtargetSetInternal( ARCHDEF64 "ARM64" )
		_ebsVMSSysconfEnvtargetSetInternal( ARCHDEFBIT 64 )
		_ebsVMSSysconfEnvtargetSetInternal( FLAG_OS_ANDROID TRUE )
	elseif( ${EBS_VAR_SYSCONF_ENVTARGET_OS} STREQUAL "Linux" )
		_ebsVMSSysconfEnvtargetSetInternal( PLATFORM "Linux" )
		_ebsVMSSysconfEnvtargetSetInternal( SUBSYSTEM "X11" )
		_ebsVMSSysconfEnvtargetSetInternal( ARCHDEF32 "x86" )
		_ebsVMSSysconfEnvtargetSetInternal( ARCHDEF64 "x86_64" )
		_ebsVMSSysconfEnvtargetSetInternal( ARCHDEFBIT 64 )
		_ebsVMSSysconfEnvtargetSetInternal( FLAG_OS_POSIX TRUE )
	elseif( ${EBS_VAR_SYSCONF_ENVTARGET_OS} STREQUAL "iOS" )
		_ebsVMSSysconfEnvtargetSetInternal( PLATFORM "Darwin" )
		_ebsVMSSysconfEnvtargetSetInternal( SUBSYSTEM "UIKit" )
		# _ebsVMSSysconfEnvtargetSetInternal( ARCHDEF32 "ARM" ) 32 bit deprecated by Apple, not supported
		_ebsVMSSysconfEnvtargetSetInternal( ARCHDEF64 "ARM64" )
		_ebsVMSSysconfEnvtargetSetInternal( ARCHDEFBIT 64 )
		_ebsVMSSysconfEnvtargetSetInternal( FLAG_OS_APPLE TRUE )
		_ebsVMSSysconfEnvtargetSetInternal( FLAG_OS_POSIX TRUE )
	elseif( ${EBS_VAR_SYSCONF_ENVTARGET_OS} STREQUAL "MacOSX" )
		_ebsVMSSysconfEnvtargetSetInternal( PLATFORM "Darwin" )
		_ebsVMSSysconfEnvtargetSetInternal( SUBSYSTEM "Cocoa" )
		_ebsVMSSysconfEnvtargetSetInternal( ARCHDEF32 "x86" )
		_ebsVMSSysconfEnvtargetSetInternal( ARCHDEF64 "x86_64" )
		_ebsVMSSysconfEnvtargetSetInternal( ARCHDEFBIT 64 )
		_ebsVMSSysconfEnvtargetSetInternal( FLAG_OS_APPLE TRUE )
		_ebsVMSSysconfEnvtargetSetInternal( FLAG_OS_POSIX TRUE )
	elseif( ${EBS_VAR_SYSCONF_ENVTARGET_OS} STREQUAL "WinDesktop" )
		_ebsVMSSysconfEnvtargetSetInternal( PLATFORM "Windows" )
		_ebsVMSSysconfEnvtargetSetInternal( SUBSYSTEM "Win32" )
		_ebsVMSSysconfEnvtargetSetInternal( ARCHDEF32 "x86" )
		_ebsVMSSysconfEnvtargetSetInternal( ARCHDEF64 "x86_64" )
		_ebsVMSSysconfEnvtargetSetInternal( ARCHDEFBIT 64 )
		_ebsVMSSysconfEnvtargetSetInternal( FLAG_OS_WINFAMILY TRUE )
	elseif( ${EBS_VAR_SYSCONF_ENVTARGET_OS} STREQUAL "WinPhone" )
		_ebsVMSSysconfEnvtargetSetInternal( PLATFORM "Windows" )
		_ebsVMSSysconfEnvtargetSetInternal( SUBSYSTEM "WinPhone" )
		_ebsVMSSysconfEnvtargetSetInternal( ARCHDEF32 "x86" )
		_ebsVMSSysconfEnvtargetSetInternal( ARCHDEF64 "x86_64" )
		_ebsVMSSysconfEnvtargetSetInternal( ARCHDEFBIT 64 )
		_ebsVMSSysconfEnvtargetSetInternal( FLAG_OS_WINFAMILY TRUE )
	elseif( ${EBS_VAR_SYSCONF_ENVTARGET_OS} STREQUAL "WinRT" )
		_ebsVMSSysconfEnvtargetSetInternal( PLATFORM "Windows" )
		_ebsVMSSysconfEnvtargetSetInternal( SUBSYSTEM "WinRT" )
		_ebsVMSSysconfEnvtargetSetInternal( ARCHDEF32 "ARM" )
		# _ebsVMSSysconfEnvtargetSetInternal( ARCHDEF64 "ARM64" ) WinRT is specifically desgined for 32-bit ARMv7
		_ebsVMSSysconfEnvtargetSetInternal( ARCHDEFBIT 32 )
		_ebsVMSSysconfEnvtargetSetInternal( FLAG_OS_WINFAMILY TRUE )
	elseif( ${EBS_VAR_SYSCONF_ENVTARGET_OS} STREQUAL "WinUWP" )
		_ebsVMSSysconfEnvtargetSetInternal( PLATFORM "Windows" )
		_ebsVMSSysconfEnvtargetSetInternal( SUBSYSTEM "WinUWP" )
		_ebsVMSSysconfEnvtargetSetInternal( ARCHDEF32 "x86" )
		_ebsVMSSysconfEnvtargetSetInternal( ARCHDEF64 "x86_64" )
		_ebsVMSSysconfEnvtargetSetInternal( ARCHDEFBIT 64 )
		_ebsVMSSysconfEnvtargetSetInternal( FLAG_OS_WINFAMILY TRUE )
	endif()

	if( NOT EBS_VAR_SYSCONF_ENVTARGET_PLATFORM OR NOT EBS_VAR_SYSCONF_ENVTARGET_SUBSYSTEM )
		message( FATAL_ERROR "" )
	endif()

endfunction()


function( _ebsVMSSysconfEnvTargetDetectArch )

	if( EBS_CONFIG_ENV_HOST_TOOLSET_ARCH )
		if( EBS_VAR_SYSCONF_ENVTARGET_ARCH )
			set( hostToolsetArch "${EBS_CONFIG_ENV_HOST_TOOLSET_ARCH}" )
			set( targetArch "${EBS_VAR_SYSCONF_ENVTARGET_ARCH}" )

			if( ${hostToolsetArch} MATCHES "${_EBS_MATCH_STR_ARCH_ARMF}" AND ${targetArch} MATCHES "${_EBS_MATCH_STR_ARCH_X86F}" )
				set( targetToolsetArchMismatch TRUE )
			elseif( ${hostToolsetArch} MATCHES "${_EBS_MATCH_STR_ARCH_X86F}" AND ${targetArch} MATCHES "${_EBS_MATCH_STR_ARCH_ARMF}" )
				set( targetToolsetArchMismatch TRUE )
			endif()

			if( targetToolsetArchMismatch )
				message( "Warning: Specified target arch (${targetArch}) does not match toolset arch (${hostToolsetArch})" )
				_ebsVMSSysconfVariableUnsetEnvTarget( EBS_VAR_SYSCONF_ENVTARGET_ARCH )
			endif()
		endif()

		if( NOT EBS_VAR_SYSCONF_ENVTARGET_ARCH )
			if( NOT EBS_VAR_SYSCONF_ENVTARGET_ARCHDEF_${EBS_CONFIG_ENV_HOST_TOOLSET_ARCHBIT} )
				message( FATAL_ERROR "Target platform is not supported by selected toolset arch (${EBS_VAR_SYSCONF_ENVTARGET_ARCH})" )
			endif()

			_ebsVMSSysconfEnvtargetSetInternal( ARCH ${EBS_CONFIG_ENV_HOST_TOOLSET_ARCH} )
		endif()
	endif()

	if( NOT EBS_VAR_SYSCONF_ENVTARGET_ARCH )
		# Check if specified ARCHBIT is supported by the platform.
		# EBS_VAR_SYSCONF_ENVTARGET_ARCHDEF_{ARCHBIT} should be defined.
		if( EBS_VAR_SYSCONF_ENVTARGET_ARCHBIT AND NOT EBS_VAR_SYSCONF_ENVTARGET_ARCHDEF_${EBS_VAR_SYSCONF_ENVTARGET_ARCHBIT} )
			# Requested bit-ness is not supported on that platform. Unset
			# the variable to automatically fallback to the default value.
			_ebsVMSSysconfVariableUnsetEnvTarget( ARCHBIT )
		endif()

		if( NOT EBS_VAR_SYSCONF_ENVTARGET_ARCHBIT )
			_ebsVMSSysconfEnvtargetSetInternal( ARCHBIT ${EBS_VAR_SYSCONF_ENVTARGET_ARCHDEFBIT} )
		endif()

		_ebsVMSSysconfEnvtargetSetInternal( ARCH ${EBS_VAR_SYSCONF_ENVTARGET_ARCHDEF${EBS_VAR_SYSCONF_ENVTARGET_ARCHBIT}} )
	endif()
	
	if( EBS_VAR_SYSCONF_ENVTARGET_ARCH )
		if( ${EBS_VAR_SYSCONF_ENVTARGET_ARCH} MATCHES "${_EBS_MATCH_STR_ARCH_32}" )
			_ebsVMSSysconfEnvtargetSetInternal( ARCHBIT 32 )
		elseif( ${EBS_VAR_SYSCONF_ENVTARGET_ARCH} MATCHES "${_EBS_MATCH_STR_ARCH_64}" )
			_ebsVMSSysconfEnvtargetSetInternal( ARCHBIT 64 )
		else()
			# Invalid architecture, unset the variable to trigger error below
			_ebsVMSSysconfVariableUnsetEnvTarget( ARCH )
		endif()
	endif()

	if( NOT EBS_VAR_SYSCONF_ENVTARGET_ARCH )
		message( FATAL_ERROR "" )
	endif()

endfunction()


# Validate ARCH setup and unset them if both 32 and 64 is set.
function( _ebsVMSSysconfEnvTargetDetectBuildcfg )

	if( EBS_VAR_SYSCONF_ENVTARGET_BUILDCFG )
		if( NOT "${EBS_VAR_SYSCONF_ENVTARGET_BUILDCFG}" MATCHES "Release|Debug" )
			_ebsVMSSysconfVariableUnsetEnvTarget( BUILDCFG )
		endif()
	endif()

	if( NOT EBS_VAR_SYSCONF_ENVTARGET_BUILDCFG )
		string( TOLOWER "${CMAKE_BUILD_TYPE}" buildTypeStr )
		string( FIND "${buildTypeStr}" "deb" debStrPos )
		string( FIND "${buildTypeStr}" "rel" relStrPos )

		if( NOT ${debStrPos} EQUAL -1 )
			set( hasDebStr TRUE )
		endif()

		if( NOT ${relStrPos} EQUAL -1 )
			set( hasRelStr TRUE )
		endif()

		if( hasDebStr AND hasRelStr )
			_ebsVMSSysconfEnvtargetSetInternal( BUILDCFG "Release" )
			_ebsVMSSysconfEnvtargetSetInternal( DBGINFO TRUE )
		elseif( hasDebStr )
			_ebsVMSSysconfEnvtargetSetInternal( BUILDCFG "Debug" )
			_ebsVMSSysconfEnvtargetSetInternal( DBGINFO TRUE )
		elseif( hasRelStr )
			_ebsVMSSysconfEnvtargetSetInternal( BUILDCFG "Release" )
		else()
			_ebsVMSSysconfEnvtargetSetInternal( BUILDCFG "Release" )
		endif()
	else()
		if( NOT EBS_VAR_SYSCONF_ENVTARGET_DBGINFO )
			string( TOLOWER "${EBS_VAR_SYSCONF_ENVTARGET_BUILDCFG}" buildTypeStr )
			string( FIND "${buildTypeStr}" "deb" debStrPos )

			if( NOT ${debStrPos} EQUAL -1 )
				_ebsVMSSysconfEnvtargetSetInternal( DBGINFO TRUE )
			endif()
		endif()
	endif()

endfunction()


# 
function( _ebsVMSSysconfEnvTargetInitialize )
	#
	_ebsVMSSysconfEnvTargetDetectPlatform()

	#
	_ebsVMSSysconfEnvTargetDetectArch()

	#
	_ebsVMSSysconfEnvTargetDetectBuildcfg()
endfunction()
