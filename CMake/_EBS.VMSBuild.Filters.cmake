
# Filters
#
# Filtering is a mechanism to restrict application of a value to specific conditions.
# EBS uses filters for build variables, that may be set individually for every single
# project condition (for example, it is quite common to have different lib directories
# for 32- and 64-bit builds or different include paths for every supported target OS).
#
# Since CMake is heavily text-based, EBS introduces filters as string with a fixed format,
# which is appended to the name of a variable, forming new variable called.
# Currently, four filters are support:
# - compiler (@SYSCONF::ENVHOST::COMPILER)
# - OS (@SYSCONF::ENVTARGET::OS)
# - architecture (@SYSCONF::ENVTARGET::ARCH)
# - build config (@SYSCONF::ENVTARGET::BUILDCFG)
#
# Values for this filters are combined into a single string which represents a filter string.
# Universal keyword, ALL, menas, that this filter is not used. Trailing ALLs are always trimmed.
# Example filter strings:
# MSVC_WinDesktop - value used for MSVC compiler on WinDesktop platform, arch and build config: any.
# ALL_WinDesktop - value used on WinDesktop platform, compiler, arch and build config: any.
# ALL_ALL_ALL_Debug - value used for 'Debug' configurations, compiler, OS and arch: any.

include_guard()

ebsCoreAddModuleRef( "VMSBuild.Defs" )

# Internal helper function, used to query value of a variable which matches specified filters.
# - pOS - build filter -> ENVTARGET::OS
# - pCompiler - build filter -> ENVHOST::COMPILER
# - pArch - build filter -> ENVTARGET::ARCH
# - pBuildcfg - build filter -> ENVTARGET::BUILDCFG
# Each one of these values may be ommited, in such case that filter is ignored. In addition:
# - if pGroupID is not empty, variables set for that build group are also fetched.
# - if pProjectID is not empty, variables set for that project are also fetched.
# These function is used to query variable for the specific environment.
