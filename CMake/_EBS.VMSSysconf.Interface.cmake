
include_guard()

ebsCoreAddModuleRef( "VMSSysconf.Impl.EnvHost" )
ebsCoreAddModuleRef( "VMSSysconf.Impl.EnvTarget" )


function( ebsVMSSysconfInitialize )
    #
    _ebsVMSBaseImplResetRegistry()
    
    #
    _ebsVMSSysconfEnvHostInitialize()

    #
    _ebsVMSSysconfEnvTargetInitialize()
endfunction()


macro( ebsVMSSysconfEnvhostGet pVarName outValue )
    _ebsVMSSysconfImplVarGet( "ENVHOST" ${pVarName} ${outValue} )
endmacro()


macro( ebsVMSSysconfEnvtargetGet pVarName outValue )
    _ebsVMSSysconfImplVarGet( "ENVTARGET" ${pVarName} ${outValue} )
endmacro()

macro( ebsVMSSysconfEnvtargetAppend pVarName pValue )
    _ebsVMSSysconfImplVarAppend( FALSE "ENVTARGET" ${pVarName} "${pValue}" )
endmacro()

macro( ebsVMSSysconfEnvtargetSet pVarName pValue )
    _ebsVMSSysconfImplVarSet( FALSE "ENVTARGET" ${pVarName} "${pValue}" )
endmacro()

macro( ebsVMSSysconfEnvtargetUnset pVarName )
    _ebsVMSSysconfImplVarUnset( FALSE "ENVTARGET" ${pVarName} )
endmacro()


macro( ebsVMSSysconfPlatformGet pVarName outValue )
    _ebsVMSSysconfImplVarGet("PLATFORM" ${pVarName} ${outValue}  )
endmacro()

macro( ebsVMSSysconfPlatformAppend pVarName pValue )
    _ebsVMSSysconfImplVarAppend( TRUE "PLATFORM" ${pVarName} "${pValue}" )
endmacro()

macro( ebsVMSSysconfPlatformSet pVarName pValue )
    _ebsVMSSysconfImplVarSet( TRUE "PLATFORM" ${pVarName} "${pValue}" )
endmacro()

macro( ebsVMSSysconfPlatformUnset pVarName )
    _ebsVMSSysconfImplVarUnset( TRUE "PLATFORM" ${pVarName} )
endmacro()
