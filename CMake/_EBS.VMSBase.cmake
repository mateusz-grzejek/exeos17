
# Variable Management System
#
# Set of common defs and APIs used to manage config variables consumed by the build system.
# VMS is divided into modules, each one manages specific subset of the configuration variables.
# Currently, there are two modules:
# - SYSCONF - detects host environment, manages configuration for the target platform, handles
#   settings for cross-compiling, manages platform-specific settings and workspace definitions.
# - BUILD - manages groups and projects, stores all properties needed for build, enables easy
#   setting of global, group- and project-specific settings, implements filters functionality.
# Modules are stored in {_EBS_VMS_BASE_DEF_MODULE_LIST} variable.
#
# Each module defines set of categories each variable belongs to. Category list for each module
# is stored in module-specific file in {_EBS_VMS_{MODULENAME}_DEF_VARCATEGORY_LIST} variable
# where {MODULENAME} is the name of a specific module.
#
# Each variable has a name which must be unique within its category (but two different categories
# can define variable with the same name). Each module category must have fixed list of available
# variables. Those are stored in {_EBS_VMS_{MODULENAME}_DEF_{VARCATEGORY}_VARIABLE_LIST} variable,
# where {MODULENAME} is the name of a module and {VARCATEGORY} is the name of category within that
# module. Attempt to set a non-existing variable results in an error.
#
# There are two additional things used to define a variable: optional module item and a filter.
# -- ModuleItem was introduced to nicely and easily integrate group- and project-specific settings
# used by BUILD module. It is simply the name of a group or project the variable refers to and it
# is appended to a variable name after category.
# -- Filter was also introduced for BUILD module. It is used for the platform filter functionality.
# It is simply a string made of values, one for each supported filter property.
#
# To summarize, every variable managed by the VMS is named using following pattern:
# EBS_VAR_${baseModule}_${varCategory}[_${moduleItem}]_${varName}[_${filter}]
# where meaning of every $ variable is as described above. Examples of (actual) variables:
# - EBS_VAR_SYSCONF_ENVHOST_COMPILER - name of the compiler on the host system
# - EBS_VAR_SYSCONF_ENVTARGET_OS - name of the OS the build is targeted for
# - EBS_VAR_BUILD_VGLOBAL_COMPILE_DEFS - global list of compiler definitions
# - EBS_VAR_BUILD_VGROUP_Libs_COMPILE_DEFS - list of compiler definitions for all projects from group "Libs"
# - EBS_VAR_BUILD_VPROJECT_Core_COMPILE_DEFS - list of compiler definitions for project "Core"
# - EBS_VAR_BUILD_VPROJECT_Core_COMPILE_DEFS_FILTER - list of compiler definitions for project "Core", filtered.
#
# Above details are provided for information purposes only - all these details are hidden by convenient
# functions every module provides. See their .cmake files for more info.

# String dynamic references:
# @{ProjectId}

include_guard()

if( NOT DEFINED _EBS_CONTROL_DEF_ )
    message( FATAL_ERROR "!!!" )
endif()


# List of base modules of the Exeos Build System:
set( _EBS_VMS_BASE_DEF_MODLIST
    "BUILD" # Manages build configuration, filters, groups and projects.
    "SYSCONF" # Detectes host environment, stores config for the target.
    "WORKSPACE" # WorkSpace Manager
)

#
function( _ebsVMSBaseImplAddInternalRegListEntry pBaseModule pVarRefName )

    set( registryList "_EBS_VMS_BASE_REG_${pBaseModule}" )

    ebsListAppendUnique( ${registryList} ${pVarRefName} )

    list( SORT ${registryList} )

    set( ${registryList} ${${registryList}} CACHE INTERNAL "${registryList}" FORCE )

endfunction()

#
function( _ebsVMSBaseImplRemoveInternalRegListEntry pBaseModule pVarRefName )

    set( registryList "_EBS_VMS_BASE_REG_${pBaseModule}" )

    list( FIND ${registryList} ${pVarRefName} varPos )

    if( NOT ${varPos} EQUAL -1 )
        list( REMOVE_AT ${registryList} ${varPos} )
        set( ${registryList} ${${registryList}} CACHE INTERNAL "${registryList}" FORCE )
    endif()

endfunction()

#
function( _ebsVMSBaseImplGetInternalRegList pBaseModule outList )

    set( registryList "_EBS_VMS_BASE_REG_${pBaseModule}" )

    set( ${outList} ${${registryList}} PARENT_SCOPE )

endfunction()

#
function( _ebsVMSBaseImplResetRegistry )

    foreach( baseModule ${_EBS_VMS_BASE_DEF_MODLIST} )
        set( moduleRegVariableListName "_EBS_VMS_BASE_REG_${baseModule}" )
        foreach( regVariableName ${${moduleRegVariableListName}} )
            unset( ${regVariableName} CACHE )
        endforeach()
        unset( ${moduleRegVariableListName} CACHE )
    endforeach()

endfunction()

#
function( _ebsVMSBaseImplGetVariableRefName pBaseModule pVarCategory pVarRefName outRefName )

    # Pattern used to compose names of variables:
    # EBS_VAR_${baseModule}_${varCategory}_${varName}
    set( ${outRefName} "EBS_VAR_${pBaseModule}_${pVarCategory}_${pVarRefName}" PARENT_SCOPE )

endfunction()

#
function( _ebsVMSBaseImplCheckVariableRequest pBaseModule pVarCategory )

    if( NOT ${pBaseModule} IN_LIST _EBS_VMS_BASE_DEF_MODLIST )
        message( FATAL_ERROR "[VMSBASE] Invalid base module: ${pBaseModule} (${_EBS_VMS_BASE_DEF_MODLIST})" )
    endif()

    if( NOT _EBS_VMS_${pBaseModule}_DEF_VARCATEGORYLIST )
        message( FATAL_ERROR "[${pBaseModule}] VARCATEGORY list has not been defined for this module." )
    endif()

    if( NOT ${pVarCategory} IN_LIST _EBS_VMS_${pBaseModule}_DEF_VARCATEGORYLIST )
        message( FATAL_ERROR "[${pBaseModule}] Invalid variable pVarCategory: ${pVarCategory}" )
    endif()
    
endfunction()

#

function( _ebsVMSBaseVarGet pBaseModule pVarCategory pVarRefName outValue )

    _ebsVMSBaseImplCheckVariableRequest( "${pBaseModule}" "${pVarCategory}" )
    _ebsVMSBaseImplGetVariableRefName( "${pBaseModule}" "${pVarCategory}" "${pVarRefName}" variableRefName )

    if( EBS_CONFIG_VMS_ENABLE_VAR_TRACE )
        if( ${variableRefName} )
            message( STATUS "[VARTRACE] GET ${variableRefName} <-- ${${variableRefName}}" )
        else()
            message( STATUS "[VARTRACE] GET ${variableRefName} (not found)" )
        endif()
    endif()

    if( ${variableRefName} )
        set( ${outValue} ${${variableRefName}} PARENT_SCOPE )
    endif()

endfunction()

#

function( _ebsVMSBaseVarAppend pAllowInternal pBaseModule pVarCategory pVarName pValue )

    _ebsVMSBaseImplCheckVariableRequest( "${pBaseModule}" "${pVarCategory}" )
    _ebsVMSBaseImplGetVariableRefName( "${pBaseModule}" "${pVarCategory}" "${pVarName}" variableRefName )

    if( EBS_CONFIG_VMS_ENABLE_VAR_TRACE )
        if( ${variableRefName} )
            message( STATUS "[VARTRACE] ADD ${variableRefName} (${${variableRefName}}) +== ${pValue}" )
        else()
            message( STATUS "[VARTRACE] ADD ${variableRefName} (not found) +== ${pValue}" )
        endif()
    endif()

    ebsListAppendUnique( ${variableRefName} ${pValue} )

    list( SORT ${variableRefName} )

    set( ${variableRefName} ${${variableRefName}} CACHE INTERNAL "${variableRefName}" FORCE )

    _ebsVMSBaseImplAddInternalRegListEntry( ${pBaseModule} ${variableRefName} )

endfunction()


#

function( _ebsVMSBaseVarSet pAllowInternal pBaseModule pVarCategory pVarName pValue )

    _ebsVMSBaseImplCheckVariableRequest( "${pBaseModule}" "${pVarCategory}" )
    _ebsVMSBaseImplGetVariableRefName( "${pBaseModule}" "${pVarCategory}" "${pVarName}" variableRefName )

    if( EBS_CONFIG_VMS_ENABLE_VAR_TRACE )
        if( ${variableRefName} )
            message( STATUS "[VARTRACE] SET ${variableRefName} --> ${pValue} (current: ${${variableRefName}})" )
        else()
            message( STATUS "[VARTRACE] SET ${variableRefName} --> ${pValue} (new)" )
        endif()
    endif()

    set( ${variableRefName} ${pValue} CACHE INTERNAL "${variableRefName}" FORCE )

    _ebsVMSBaseImplAddInternalRegListEntry( ${pBaseModule} ${variableRefName} )

endfunction()

#

function( _ebsVMSBaseVarUnset pAllowInternal pBaseModule pVarCategory pVarName )

    _ebsVMSBaseImplCheckVariableRequest( "${pBaseModule}" "${pVarCategory}" )
    _ebsVMSBaseImplGetVariableRefName( "${pBaseModule}" "${pVarCategory}" "${pVarName}" variableRefName )

    if( EBS_CONFIG_VMS_ENABLE_VAR_TRACE )
        if( ${variableRefName} )
            message( STATUS "[VARTRACE] UNSET ${variableRefName} (current: ${${variableRefName}})" )
        else()
            message( STATUS "[VARTRACE] UNSET ${variableRefName} (not found)" )
        endif()
    endif()

    unset( ${variableRefName} CACHE )

    _ebsVMSBaseImplRemoveInternalRegListEntry( ${pBaseModule} ${variableRefName} )

endfunction()
