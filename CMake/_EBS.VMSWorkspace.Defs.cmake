
include_guard()

ebsCoreAddModuleRef( "VMSBase" )

#
set( _EBS_VMS_WORKSPACE_DEF_VARCATEGORYLIST
    "REGISTRY" #
    "VGROUP"
    "VPROJECT"
)

#
set( _EBS_VMS_WORKSPACE_DEF_VARLIST_REGISTRY
    "GROUP_LIST"
    "PROJECT_LIST"
)

#
set( _EBS_VMS_WORKSPACE_DEF_VARLIST_VGROUP
    "DEPENDENCY_WHITELIST"
    "PROJECT_ID_PREFIX"
    "ROOT_DIR"
    "PROJECT_SUBDIR"
    "TARGET_NAME_PREFIX"
    "TARGET_OS_SUPPORT_LIST"
)

#
set( _EBS_VMS_WORKSPACE_DEF_VARLIST_VPROJECT
    "GROUP_ID"
    "ID_PREFIX"
    "NAME"
    "ROOT_DIR"
    "TARGET_OS_SUPPORT_LIST"
)


function( _ebsVMSWorkspaceImplCheckVariableRequest pAllowInternal pVarCategory pWorkspaceItem pVarName pVarSuffix )

    # TODO

endfunction()


function( _ebsVMSWorkspaceImplGetVariableRefName pWorkspaceItem pVarName pVarSuffix outRefName )

    if( pWorkspaceItem )
        set( workspaceItemStr "${pWorkspaceItem}_" )
    endif()

    if( pVarSuffix )
        set( varSuffixStr "_${pVarSuffix}" )
    endif()

    set( ${outRefName} "${workspaceItemStr}${pVarName}${varSuffixStr}" PARENT_SCOPE )

endfunction()


macro( _ebsVMSWorkspaceImplVarGet pVarCategory pWorkspaceItem pVarName pVarSuffix outValue )
    _ebsVMSWorkspaceImplCheckVariableRequest( TRUE ${pVarCategory} "${pWorkspaceItem}" ${pVarName} "${pVarSuffix}" )
    _ebsVMSWorkspaceImplGetVariableRefName( "${pWorkspaceItem}" ${pVarName} "${pVarSuffix}" variableRefName )
    _ebsVMSBaseVarGet( "WORKSPACE" ${pVarCategory} ${variableRefName} ${outValue} )
endmacro()

macro( _ebsVMSWorkspaceImplVarAppend pAllowInternal pVarCategory pWorkspaceItem pVarName pVarSuffix pValue )
    _ebsVMSWorkspaceImplCheckVariableRequest( ${pAllowInternal} ${pVarCategory} "${pWorkspaceItem}" ${pVarName} "${pVarSuffix}" )
    _ebsVMSWorkspaceImplGetVariableRefName( "${pWorkspaceItem}" ${pVarName} "${pVarSuffix}" variableRefName )
    _ebsVMSBaseVarAppend( ${pAllowInternal} "WORKSPACE" ${pVarCategory} ${variableRefName} "${pValue}" )
endmacro()

macro( _ebsVMSWorkspaceImplVarSet pAllowInternal pVarCategory pWorkspaceItem pVarName pVarSuffix pValue )
    _ebsVMSWorkspaceImplCheckVariableRequest( ${pAllowInternal} ${pVarCategory} "${pWorkspaceItem}" ${pVarName} "${pVarSuffix}" )
    _ebsVMSWorkspaceImplGetVariableRefName( "${pWorkspaceItem}" ${pVarName} "${pVarSuffix}" variableRefName )
    _ebsVMSBaseVarSet( ${pAllowInternal} "WORKSPACE" ${pVarCategory} ${variableRefName} "${pValue}" )
endmacro()

macro( _ebsVMSWorkspaceImplVarUnset pAllowInternal pVarCategory pWorkspaceItem pVarName pVarSuffix )
    _ebsVMSWorkspaceImplCheckVariableRequest( ${pAllowInternal} ${pVarCategory} "${pWorkspaceItem}" ${pVarName} "${pVarSuffix}" )
    _ebsVMSWorkspaceImplGetVariableRefName( "${pWorkspaceItem}" ${pVarName} "${pVarSuffix}" variableRefName )
    _ebsVMSBaseVarUnset( ${pAllowInternal} "WORKSPACE" ${pVarCategory} ${variableRefName} )
endmacro()


macro( _ebsVMSWorkspaceRegistryGetInternal pVarName outValue )
    _ebsVMSWorkspaceImplVarGet( "REGISTRY" "" ${pVarName} "" ${outValue} )
endmacro()

macro( _ebsVMSWorkspaceRegistryAppendInternal pVarName pValue )
    _ebsVMSWorkspaceImplVarAppend( TRUE "REGISTRY" "" ${pVarName} "" "${pValue}" )
endmacro()

macro( _ebsVMSWorkspaceRegistrySetInternal pVarName pValue )
    _ebsVMSWorkspaceImplVarSet( TRUE "REGISTRY" "" ${pVarName} "" "${pValue}" )
endmacro()

macro( _ebsVMSWorkspaceRegistryUnsetInternal pVarName )
    _ebsVMSWorkspaceImplVarUnset( TRUE "REGISTRY" "" ${pVarName} "" )
endmacro()


macro( _ebsVMSWorkspaceVGroupGetInternal pGroupID pVarName outValue )
    _ebsVMSWorkspaceImplVarGet( "VGROUP" ${pGroupID} ${pVarName} "" ${outValue} )
endmacro()

macro( _ebsVMSWorkspaceVGroupAppendInternal pGroupID pVarName pValue )
    _ebsVMSWorkspaceImplVarAppend( TRUE "VGROUP" ${pGroupID} ${pVarName} "" "${pValue}" )
endmacro()

macro( _ebsVMSWorkspaceVGroupSetInternal pGroupID pVarName pValue )
    _ebsVMSWorkspaceImplVarSet( TRUE "VGROUP" ${pGroupID} ${pVarName} "" "${pValue}" )
endmacro()

macro( _ebsVMSWorkspaceVGroupUnsetInternal pGroupID pVarName )
    _ebsVMSWorkspaceImplVarUnset( TRUE "VGROUP" ${pGroupID} ${pVarName} "" )
endmacro()


macro( _ebsVMSWorkspaceVProjectGetInternal pProjectID pVarName outValue )
    _ebsVMSWorkspaceImplVarGet( "VPROJECT" ${pProjectID} ${pVarName} "" ${outValue} )
endmacro()

macro( _ebsVMSWorkspaceVProjectAppendInternal pProjectID pVarName pValue )
    _ebsVMSWorkspaceImplVarAppend( TRUE "VPROJECT" ${pProjectID} ${pVarName} "" "${pValue}" )
endmacro()

macro( _ebsVMSWorkspaceVProjectSetInternal pProjectID pVarName pValue )
    _ebsVMSWorkspaceImplVarSet( TRUE "VPROJECT" ${pProjectID} ${pVarName} "" "${pValue}" )
endmacro()

macro( _ebsVMSWorkspaceVProjectUnsetInternal pProjectID pVarName )
    _ebsVMSWorkspaceImplVarUnset( TRUE "VPROJECT" ${pProjectID} ${pVarName} "" )
endmacro()
