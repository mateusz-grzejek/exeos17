
message( "${CMAKE_GENERATOR}" )

message( "${CMAKE_SYSTEM_NAME}" )

if( "${EXEOS_CCV_PLATFORM}" STREQUAL "WinDesktop" )
	string( REGEX MATCH "10\.[0-9]\.[0-9]+" win10 ${CMAKE_HOST_SYSTEM_VERSION} )
	if( win10 )
		message("WIN10 (${CMAKE_HOST_SYSTEM_VERSION})")
	endif()
elseif( ${_platformName} STREQUAL "Linux" )
elseif( ${_platformName} STREQUAL "MacOSX" )
elseif( ${_platformName} STREQUAL "Android" )
endif()

# Project Defs:
# - archive (project output) type ("APP","LIB","SHARED") -> EXEOS_PDEF_ARCHIVE_TYPE
# - include dirs -> EXEOS_PDEF_DIR_INCLUDE
# - linker dirs -> EXEOS_PDEF_DIR_LINKER
# - compiler options -> EXEOS_PDEF_OPTIONS_COMPILER
# - linker options -> EXEOS_PDEF_OPTIONS_LINKER
# - source list -> EXEOS_PDEF_SRC_FILES

# params:
# - targetID -> DP_stdx, ECL_CoreUtils, ECL_System, ECM_Core, EDR_GL4, WRK_Test, ...
# - targetCategory -> "DPLIBS_CUSTOM", "ENGINE_COMMON_LIBS", "ENGINE_COMPONENTS", "ENGINE_DRIVERS", "TOOLSET", "UTILITIES", "WORKSHOP", ...
# - targetOutputType -> "APP", "LIB", "SHARED"

# exsGetTargetOutputArtifactType( return, targetRefID, targetGID )
#
function( exsGetTargetOutputArtifactType return targetRefID targetGID  )
	if( ${EXEOS_PDEF_${targetRefID}_ARTIFACT_TYPE} )
		set( targetOutputArtifactType ${EXEOS_PDEF_${targetRefID}_ARTIFACT_TYPE} )
	else()
		if( ${targetGID} STREQUAL "GIDDepCustom" )
			set( targetOutputArtifactType "STATIC" )
		elseif( ${targetGID} STREQUAL "GIDExsCommonLibs" )
			set( targetOutputArtifactType "STATIC" )
		elseif( ${targetGID} STREQUAL "GIDExsDrclayerGraphics" )
			set( targetOutputArtifactType "STATIC" )
		elseif( ${targetGID} STREQUAL "GIDExsComponents" )
			set( targetOutputArtifactType ${EXEOS_CCV_MODULE_LIB_TYPE} )
		elseif( ${targetGID} STREQUAL "GIDExsDriverGraphics" )
			set( targetOutputArtifactType ${EXEOS_CCV_MODULE_LIB_TYPE} )
		elseif( ${targetGID} STREQUAL "GIDWorkshop" )
			set( targetOutputArtifactType "EXE" )
		else()
			message( ERROR "${targetRefID}: output file (artifact) type is not set!" )
		endif()
	endif()
	set( ${return} ${targetOutputArtifactType} PARENT_SCOPE )
endfunction( exsGetTargetOutputArtifactType )

# exsSetTargetDependencies( targetGID, targetRefID, targetName )
#
function( exsSetTargetDependencies targetGID targetRefID targetName )
	message( STATUS "-- Setting up dependencies for ${targetName}..." )
	math( EXPR dependenciesNum 0 )
	foreach( allowedGroupGID ${EXEOS_CCV_BUILD_ENV_TARGET_GROUP_ALLOWED_DEPENDENCY_LIST_${targetGID}} )
		foreach( dependencyID ${EXEOS_PDEF_${targetRefID}_INTERNAL_DEPENDENCIES_${allowedGroupGID}} )
			exsGetTargetName( dependencyName ${allowedGroupGID} ${dependencyID} )
			target_link_libraries( ${targetName} ${dependencyName} )
			message( STATUS "---- ${targetName} --> ${dependencyName}" )
			math( EXPR dependenciesNum "${dependenciesNum}+1" )
		endforeach()
	endforeach()
	if( dependenciesNum EQUAL 0 )
		message( STATUS "---- No dependencies has been found." )
	else()
		message( STATUS "---- Done (total: ${dependenciesNum})" )
	endif()
endfunction( exsSetTargetDependencies )

#
#
#EXEOS_CCV_DIR_BIN_COMMON
function( exsSetBinaryDependencies targetGID targetRefID targetName targetOutputDir )
	exsQueryCurrentBuildVariableValue( requiredRuntimeBinaries RUNTIME_BINARIES GID ${targetGID} TRID ${targetRefID} )
	foreach( binary ${requiredRuntimeBinaries} )
		foreach( searchDir ${EXEOS_CCV_DIR_BIN_COMMON} )
			set( binaryFile "${CMAKE_CURRENT_SOURCE_DIR}/${searchDir}/${binary}" )
			if( EXISTS ${binaryFile} )
				add_custom_command( ENV_TARGET ${targetName} POST_BUILD
					COMMAND ${CMAKE_COMMAND} -E copy ${binaryFile} ${targetOutputDir} )
				break()
			endif()
		endforeach()
	endforeach()
endfunction( exsSetBinaryDependencies )

#
function( exsGetTargetSourceFiles return targetRefID targetBaseDir )
	set( srcBasePath ${CMAKE_SOURCE_DIR}/${targetBaseDir} )
	if( EXISTS ${srcBasePath} )
		exsGetSourceFileListGlobStr( srcFilesGlobStr ${srcBasePath} )
		file( GLOB sourceFiles ${srcFilesGlobStr} )
		source_group( "" FILES ${sourceFiles} )
		set( targetSourceFiles ${targetSourceFiles} ${sourceFiles} )
		
		foreach( srcSubdir ${EXEOS_PDEF_${targetRefID}_SRC_SUBDIRS} )
			#
			set( srcPath ${srcBasePath}/${srcSubdir} )
			if( EXISTS ${srcPath} )
				exsGetSourceFileListGlobStr( srcFilesGlobStr ${srcPath} )
				file( GLOB sourceFiles ${srcFilesGlobStr} )
				source_group( ${srcSubdir} FILES ${sourceFiles} )
				set( targetSourceFiles ${targetSourceFiles} ${sourceFiles} )
			endif()
		endforeach()
	endif()
	
	if( EXEOS_PDEF_${targetRefID}_SRC_SUBDIRS_EXT )
		foreach( srcSubdir ${EXEOS_PDEF_${targetRefID}_SRC_SUBDIRS_EXT} )
			set( srcPath ${srcBasePath}/${srcSubdir} )
			if( EXISTS ${srcPath} )
				exsGetSourceFileListGlobStr( srcFilesGlobStr ${srcPath} )
				file( GLOB sourceFiles ${srcFilesGlobStr} )
				source_group( ${srcSubdir} FILES ${sourceFiles} )
				set( targetSourceFiles ${targetSourceFiles} ${sourceFiles} )
			endif()
		endforeach()
	endif()
	
	foreach( specificFilesDir ${EXEOS_CCV_DIR_SRC_SPECIFIC_DIRECTORIES_LIST} )
		set( srcPath "${srcBasePath}/${specificFilesDir}" )
		if( EXISTS ${srcPath} )
			exsGetSourceFileListGlobStr( srcFilesGlobStr ${srcPath} )
			file( GLOB sourceFiles ${srcFilesGlobStr} )
			source_group( ${specificFilesDir} FILES ${sourceFiles} )
			set( targetSourceFiles ${targetSourceFiles} ${sourceFiles} )
		endif()
	endforeach()
	
	set( ${return} ${targetSourceFiles} PARENT_SCOPE )
endfunction( exsGetTargetSourceFiles )


#
function( exsAddBuildTarget targetGID targetID )
	
	exsGetTargetRefID( targetRefID ${targetGID} ${targetID} )
	exsGetTargetName( targetName ${targetGID} ${targetID} )
	exsGetTargetGroupRootDir( targetRootDir ${targetGID} )
	exsGetTargetGroupSrcDir( targetSrcDir ${targetGID} )

	exsQueryCurrentBuildVariableValue( targetIncludeSearchPath INCLUDE_SEARCH_PATH GID ${targetGID} TRID ${targetRefID} )
	message( "## ${targetRefID} -> ${targetIncludeSearchPath}" )
	
	set( targetSubdir ${targetSrcDir}/${targetID} )
	
	if( NOT IS_DIRECTORY "${CMAKE_SOURCE_DIR}/${targetSubdir}" )
		message( "-- Source dir for target ${targetID} (${CMAKE_SOURCE_DIR}/${targetSubdir}) does not exist. Target ignored.\n" )
		return()
	endif()
	
	if( NOT EXISTS "${CMAKE_SOURCE_DIR}/${targetSubdir}/CMakeLists.txt" )
		message( "-- Target ${targetID} does not have CMakeLists.txt. Target ignored.\n" )
		return()
	endif()
	
	add_subdirectory( ${targetSubdir} )
	
	exsGetTargetSourceFiles( targetSourceFiles ${targetRefID} ${targetSubdir} )
	
	if( NOT targetSourceFiles )
		message( "-- Target ${targetID} does not contain any source files. Target ignored.\n" )
		return()
	endif()
	
	exsAddPrecompiledHeader( ${targetName} ${targetGID} ${targetSubdir} targetSourceFiles )

	exsGetTargetOutputArtifactType( targetArtifactType ${targetRefID} ${targetGID} )
	
	#
	if( ${targetArtifactType} STREQUAL "EXE" )
		add_executable( ${targetName} ${targetSourceFiles} )
	elseif( ${targetArtifactType} STREQUAL "STATIC" )
		add_library( ${targetName} STATIC ${targetSourceFiles} )
	elseif( ${targetArtifactType} STREQUAL "SHARED" )
		add_library( ${targetName} SHARED ${targetSourceFiles} )
	endif()
	
	set( targetOutputFile "${EXEOS_CCV_ENV_TARGET_OUTPUT_FILE_NAME_PREFIX_${targetGID}}${EXEOS_PDEF_${targetRefID}_BASE_NAME}" )
	set( targetOutputDirectoryBase "${CMAKE_SOURCE_DIR}/${EXEOS_CCV_DIR_OUTPUT_${targetArtifactType}_BASE}" )
	set( targetOutputDirectory "${CMAKE_SOURCE_DIR}/${EXEOS_CCV_DIR_OUTPUT_${targetArtifactType}}" )

	# Set output directories.
	set_target_properties( ${targetName} PROPERTIES
		OUTPUT_NAME "${targetOutputFile}"
		ARCHIVE_OUTPUT_DIRECTORY "${targetOutputDirectory}"
		LIBRARY_OUTPUT_DIRECTORY "${targetOutputDirectory}"
		RUNTIME_OUTPUT_DIRECTORY "${targetOutputDirectory}" )
		
	exsQueryCurrentBuildVariableValue( targetCompileOptions COMPILE_OPTIONS GID ${targetGID} TRID ${targetRefID} )
	target_compile_options( ${targetName} PRIVATE ${targetCompileOptions} )
	
	exsQueryCurrentBuildVariableValue( targetCompileDefinitions COMPILE_DEFINITIONS GID ${targetGID} TRID ${targetRefID} )
	target_compile_definitions( ${targetName} PRIVATE ${targetCompileDefinitions} )
	
	exsQueryCurrentBuildVariableValue( targetLinkInput LINK_INPUT GID ${targetGID} TRID ${targetRefID} )
	target_link_libraries( ${targetName} ${targetLinkInput} )
	
	exsQueryCurrentBuildVariableValue( targetLinkOptions LINK_OPTIONS GID ${targetGID} TRID ${targetRefID} )
	set( CMAKE_${targetArtifactType}_LINKER_FLAGS "${CMAKE_${targetArtifactType}_LINKER_FLAGS} ${targetLinkOptions}" )
	
	#exsQueryCurrentBuildVariableValue( targetIncludeSearchPath INCLUDE_SEARCH_PATH GID ${targetGID} TRID ${targetRefID} )
	#message( "## ${targetRefID} -> ${targetIncludeSearchPath}" )
	target_include_directories( ${targetName} PRIVATE ${targetIncludeSearchPath} )

	exsQueryCurrentBuildVariableValue( linkSearchPath LINK_SEARCH_PATH GID ${targetGID} TRID ${targetRefID} )
	link_directories( ${linkSearchPath} )
	
	exsSetTargetDependencies( ${targetGID} ${targetRefID} ${targetName} )
	
	exsSetBinaryDependencies( ${targetGID} ${targetRefID} ${targetName} "${targetOutputDirectoryBase}/${EXEOS_CCV_DIR_ENV_TARGET_OUTPUT_SUBDIR_BUILD}" )
	
endfunction( exsAddBuildTarget )
