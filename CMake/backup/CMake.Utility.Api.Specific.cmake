
#
function( exsAddPrecompiledHeader targetName targetGID srcDirBase sourceFileList )

	set( pchIncName "Precomp.h" )
	set( pchSrcName "_Precomp/Precomp.cpp" )
	
	set( pchBinaryOutputDir "${PROJECT_BINARY_DIR}/${targetName}.dir" )
	
	exsIsPrecompiledHeaderTarget( enablePrecompiledHeader ${targetGID} )
	
	if( ${enablePrecompiledHeader} )
		get_filename_component( pchBaseName ${pchIncName} NAME_WE )
		
		set( pchIncFile "${CMAKE_CURRENT_SOURCE_DIR}/${srcDirBase}/${pchIncName}" )
		set( pchSrcFile "${CMAKE_CURRENT_SOURCE_DIR}/${srcDirBase}/${pchSrcName}" )
		
		if( EXISTS ${pchIncFile} AND EXISTS ${pchSrcFile} )
			if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
			
				set( pchBinary "${pchBinaryOutputDir}/${pchBaseName}.pch" )

				set_source_files_properties( ${pchSrcFile} PROPERTIES
					COMPILE_FLAGS "/Yc\"${pchIncName}\" /Fp\"${pchBinary}\""
					OBJECT_OUTPUTS "${pchBinary}" )

				set_source_files_properties( ${${sourceFileList}} PROPERTIES
					COMPILE_FLAGS "/Yu\"${pchIncName}\" /FI\"${pchIncName}\" /Fp\"${pchBinary}\""
					OBJECT_DEPENDS "${pchBinary}" )

				source_group( "_Precomp" FILES ${pchSrcFile} )
				list( APPEND ${sourceFileList} ${pchSrcFile} )
				
			elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GCC")
			
				set( pchBinary "${pchBinaryOutputDir}/${pchBaseName}.h.gch" )

				set_source_files_properties( ${pchIncFile} PROPERTIES
					COMPILE_FLAGS "-x"
					OBJECT_OUTPUTS "${pchBinary}" )

				set_source_files_properties( ${${sourceFileList}} PROPERTIES
					OBJECT_DEPENDS "${pchBinary}" )

				source_group( "_Precomp" FILES ${pchIncFile} )
				list( APPEND ${sourceFileList} ${pchIncFile} )
				
			endif()
			
			set( ${sourceFileList} ${${sourceFileList}} PARENT_SCOPE )
			message( STATUS "${Yellow}-- PCH has been activated.${ColourReset}" )
		endif()
	endif()
	
endfunction( exsAddPrecompiledHeader )


function( addVisualStudioUpdateTargetVersionEx vsVersion )
	set( cmakeWorkingDir "${CMAKE_SOURCE_DIR}/Temp/CMakeOutput/IDE/VS${vsVersion}" )
	
	set( targetCommandOptions "-D EXSCFG_ENGINE_DRIVER_SUPPORT_FLAG_GRAPHICS_D3D11=1" )
	
	file( MAKE_DIRECTORY ${cmakeWorkingDir} )
	
	add_custom_target( "CMake.Update"
		ALL
		WORKING_DIRECTORY ${cmakeWorkingDir}
		COMMAND "${CMAKE_COMMAND} ${targetCommandOptions} ../../../.." )
	
endfunction( addVisualStudioUpdateTargetVersionEx )


function( addVisualStudioUpdateTarget )
	string( FIND ${CMAKE_GENERATOR} "Visual Studio" visualStudioGenerator )
	if( NOT ${visualStudioGenerator} EQUAL -1 )
		string( FIND ${CMAKE_GENERATOR} "2017" visualStudio2017 )
		if( NOT ${visualStudio2017} EQUAL -1 )
			addVisualStudioUpdateTargetVersionEx( 17 )
			return()
		endif()
		
		string( FIND ${CMAKE_GENERATOR} "2015" visualStudio2015 )
		if( NOT ${visualStudio2015} EQUAL -1 )
			addVisualStudioUpdateTargetVersionEx( 15 )
			return()
		endif()
	endif()
endfunction( addVisualStudioUpdateTarget )
