
#
#
function( exsGetTargetRefID return targetGID targetID )
	set( ${return} ${EXEOS_CCV_ENV_TARGET_REFID_PREFIX_${targetGID}}${targetID} PARENT_SCOPE )
endfunction( exsGetTargetRefID )

#
#
function( exsGetTargetName return targetGID targetID )
	set( ${return} ${EXEOS_CCV_ENV_TARGET_NAME_PREFIX_${targetGID}}${targetID} PARENT_SCOPE )
endfunction( exsGetTargetName )

# exsGetTargetGroupRootDir( return, GID )
#
# Returns root dir for the specified target group. Root dir is the top directory,
# used, for example, as the include directory for other projects which reference
# targets from this group.
function( exsGetTargetGroupRootDir return GID )
	set( ${return} ${EXEOS_CCV_DIR_GROUP_ROOT_${GID}} PARENT_SCOPE )
endfunction( exsGetTargetGroupRootDir )

# exsGetTargetGroupSrcDir( return, GID )
#
#
function( exsGetTargetGroupSrcDir return GID )
	set( srcSubdir "${EXEOS_CCV_DIR_GROUP_SRC_SUBDIR_${GID}}" )
	set( ${return} "${EXEOS_CCV_DIR_GROUP_ROOT_${GID}}${srcSubdir}" PARENT_SCOPE )
endfunction( exsGetTargetGroupSrcDir )

# exsIsPrecompiledHeaderTarget( return, GID )
#
#
function( exsIsPrecompiledHeaderTarget return GID )
	foreach( pchTarget ${EXEOS_CCV_BUILD_PRECOMPILED_HEADER_ENV_TARGET_LIST} )
		if( "${GID}" STREQUAL "${pchTarget}" )
			set( ${return} TRUE PARENT_SCOPE )
			return()
		endif()
	endforeach()
endfunction( exsIsPrecompiledHeaderTarget )

function( exsGetSourceFileListGlobStr return path )
	foreach( fileType ${EXEOS_CCV_FILE_COMMON_SRC_FILE_TYPE_LIST} )
		set( globStr "${globStr}" "${path}/*${fileType}" )
	endforeach()
	set( ${return} ${globStr} PARENT_SCOPE )
endfunction( exsGetSourceFileListGlobStr )

# exsValidateEngineDriverSupportGraphics( driverGroupID, driverID )
#
#
function( exsValidateEngineDriverSupportGraphics driverGroupID driverID )
	set( driverSupportVar CMAKE_EXSCFG_ENGINE_DRIVER_SUPPORT_GRAPHICS_${driverID} )
	
	list( FIND "${EXEOS_CCV_ENGINE_DRIVER_PLATFORM_SUPPORT_LIST_GRAPHICS_${driverGroupID}}" "${EXEOS_CCV_PLATFORM}" isDriverSupported )
	
	if( NOT ${isDriverSupported} EQUAL -1 )
		if( ${driverSupportVar} )
			message( WARNING "Support for ${driverID} was disabled, because platform does not support it." )
		endif()
		set( ${${driverSupportVar}} FALSE CACHE INTERNAL "${driverSupportVar}" )
	else()
		set( ${${driverSupportVar}} TRUE CACHE INTERNAL "${driverSupportVar}" )
	endif()
endfunction( exsValidateEngineDriverSupportGraphics )

# exsGetEngineDriverGroupName( driverGroupID, driverID )
#
#
function( exsGetEngineDriverGroupName result driverType driverID )
	foreach( groupName ${EXEOS_CCV_ENGINE_DRIVER_GROUP_LIST_${driverType}} )
		string( FIND ${driverID} ${groupName} rpos )
		if( NOT ${rpos} EQUAL -1 )
			set( ${result} ${groupName} PARENT_SCOPE )
			return()
		endif()
	endforeach()
endfunction( exsGetEngineDriverGroupName )

# exsCheckEngineDriverSupport( driverGroupID, driverID )
#
#
function( exsCheckEngineDriverGroupSupport result driverType driverGroupID )
	foreach( supportedPlatform ${EXEOS_CCV_ENGINE_DRIVER_PLATFORM_SUPPORT_LIST_${driverType}_${driverGroupID}} )
		if( ${EXEOS_CCV_PLATFORM} STREQUAL ${supportedPlatform} )
			set( isDriverGroupSupportedByPlatform TRUE )
			break()
		endif()
	endforeach()
	
	if( "${isDriverGroupSupportedByPlatform}" )
		set( ${result} TRUE PARENT_SCOPE )
	else()
		set( ${result} FALSE PARENT_SCOPE )
	endif()
endfunction( exsCheckEngineDriverGroupSupport )

# exsCheckEngineDriverSupport( driverGroupID, driverID )
#
#
function( exsCheckEngineDriverSupport result retDriverGroupID driverType driverID )
	set( driverSupportConfigFlagName EXSCFG_ENGINE_DRIVER_SUPPORT_FLAG_${driverType}_${driverID} )
	
	if( ${driverSupportConfigFlagName} )
		set( isDriverEnabledInConfig TRUE )
	endif()

	exsGetEngineDriverGroupName( driverGroupID ${driverType} ${driverID} )
	exsCheckEngineDriverGroupSupport( isDriverGroupSupported ${driverType} ${driverGroupID} )
	
	if( retDriverGroupID )
		set( ${retDriverGroupID} ${driverGroupID} PARENT_SCOPE )
	endif()
	
	if( "${isDriverGroupSupported}" AND "${isDriverEnabledInConfig}" )
		set( ${result} TRUE PARENT_SCOPE )
	else()
		set( ${result} FALSE PARENT_SCOPE )
	endif()
	
	if( NOT "${isDriverGroupSupported}" )
		message( "-- ${driverID} is not supported on current platform (${EXEOS_CCV_PLATFORM})." )
	else()
		message( STATUS "${Green}-- ${driverID} is supported on current platform (${EXEOS_CCV_PLATFORM}).${ColourReset}" )
		if( NOT "${isDriverEnabledInConfig}" )
			message( "-- Error: ${driverID} has been disabled in config (${driverSupportConfigFlagName})." )
		else()
			message( STATUS "${Green}-- ${driverID} has been enabled in config (${driverSupportConfigFlagName}).${ColourReset}" )
		endif()
	endif()
endfunction( exsCheckEngineDriverSupport )

#
#
#
function( exsDebugPrintArray array title )
	message( STATUS "${title}" )
	foreach( arrayElement ${array} )
		message( STATUS "-- ${arrayElement}" )
	endforeach()
endfunction( exsDebugPrintArray )