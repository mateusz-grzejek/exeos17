
if( NOT WIN32 )
	string( ASCII 27 Esc )
	set( ColourReset "${Esc}[m" )
	set( ColourBold  "${Esc}[1m" )
	set( Red         "${Esc}[31m" )
	set( Green       "${Esc}[32m" )
	set( Yellow      "${Esc}[33m" )
	set( Blue        "${Esc}[34m" )
	set( Magenta     "${Esc}[35m" )
	set( Cyan        "${Esc}[36m" )
	set( White       "${Esc}[37m" )
endif()

set( EXEOS_CCV_FILE_COMMON_SRC_FILE_TYPE_LIST
	".h"
	".hpp"
	".hxx"
	".inc"
	".inl"
	".c"
	".cpp" )

# Properties of a target:
# - REFID - compact ID of a target, globally unique, used for logging.
# - NAME - longer name of a target, globally unique, passed to add_{executable|library}.


# EXEOS_CCV_BUILD_ENV_TARGET_GROUP_GID_LIST
# List of target groups, which use precompiled headers. Currently, PCH is enabled
# for all compiled projects except the custom external libraries (DepCustom).
set( EXEOS_CCV_BUILD_ENV_TARGET_GROUP_GID_LIST
	"GIDDepCustom"
	"GIDExsCommonLibs"
	"GIDExsComponents"
	"GIDExsDrclayerGraphics"
	"GIDExsDriverGraphics"
	"GIDWorkshop" )


# EXEOS_CCV_BUILD_PRECOMPILED_HEADER_ENV_TARGET_LIST
# List of target groups, which use precompiled headers. Currently, PCH is enabled
# for all compiled projects except the custom external libraries (DepCustom).
set( EXEOS_CCV_BUILD_PRECOMPILED_HEADER_ENV_TARGET_LIST
	"GIDExsCommonLibs"
	"GIDExsComponents"
	"GIDExsDrclayerGraphics"
	"GIDExsDriverGraphics"
	"GIDWorkshop" )


# EXEOS_CCV_BUILD_ENV_TARGET_GROUP_GID_LIST
#
set( EXEOS_CCV_BUILD_ENV_TARGET_GROUP_ALLOWED_DEPENDENCY_LIST_GIDExsCommonLibs
	"GIDDepCustom"
	"GIDExsCommonLibs" )
	
set( EXEOS_CCV_BUILD_ENV_TARGET_GROUP_ALLOWED_DEPENDENCY_LIST_GIDExsComponents
	"GIDDepCustom"
	"GIDExsCommonLibs"
	"GIDExsComponents" )
	
set( EXEOS_CCV_BUILD_ENV_TARGET_GROUP_ALLOWED_DEPENDENCY_LIST_GIDExsDrclayerGraphics
	"GIDDepCustom"
	"GIDExsCommonLibs"
	"GIDExsComponents" )
	
set( EXEOS_CCV_BUILD_ENV_TARGET_GROUP_ALLOWED_DEPENDENCY_LIST_GIDExsDriverGraphics
	"GIDDepCustom"
	"GIDExsCommonLibs"
	"GIDExsComponents"
	"GIDExsDrclayerGraphics" )
	
set( EXEOS_CCV_BUILD_ENV_TARGET_GROUP_ALLOWED_DEPENDENCY_LIST_GIDWorkshop
	"GIDDepCustom"
	"GIDExsCommonLibs"
	"GIDExsComponents"
	"GIDExsDriverGraphics" )


# EXEOS_CCV_BUILD_ENV_TARGET_GROUP_GID_LIST
#
set( EXEOS_CCV_SUPPORT_LIST_ARCH
	"AArch64"
	"ARM"
	"x86"
	"x86_64" )

set( EXEOS_CCV_SUPPORT_LIST_COMPILER
	"Clang"
	"GCC"
	"MSVC"
	"ICC" )

set( EXEOS_CCV_SUPPORT_LIST_PLATFORM
	"Android"
	"Linux"
	"iOS"
	"OSX"
	"WinDesktop"
	"WinPhone81"
	"WinRT" )


# EXEOS_CCV_ENGINE_DRIVER_GROUP_LIST_GRAPHICS
#
set( EXEOS_CCV_ENGINE_DRIVER_GROUP_LIST_GRAPHICS
	D3D
	GL
	MTL
	VK )


# EXEOS_CCV_ENGINE_DRIVER_PLATFORM_SUPPORT_LIST
#
set( EXEOS_CCV_ENGINE_DRIVER_PLATFORM_SUPPORT_LIST_GRAPHICS_D3D
	"WinDesktop"
	"WinPhone81"
	"WinRT" )
	
set( EXEOS_CCV_ENGINE_DRIVER_PLATFORM_SUPPORT_LIST_GRAPHICS_GL
	"Android"
	"Linux"
	"iOS"
	"OSX"
	"WinDesktop" )
	
set( EXEOS_CCV_ENGINE_DRIVER_PLATFORM_SUPPORT_LIST_GRAPHICS_MTL
	"iOS"
	"OSX" )
	
set( EXEOS_CCV_ENGINE_DRIVER_PLATFORM_SUPPORT_LIST_GRAPHICS_VK
	"Android"
	"Linux"
	"WinDesktop" )
	

# EXEOS_CCV_ENV_TARGET_REFID_PREFIX
# Group-specific prefix, prepended to a target id, which form the REFID of a target.
set( EXEOS_CCV_ENV_TARGET_REFID_PREFIX_GIDDepCustom
	"CDLB_")

set( EXEOS_CCV_ENV_TARGET_REFID_PREFIX_GIDExsCommonLibs
	"EXCL_")

set( EXEOS_CCV_ENV_TARGET_REFID_PREFIX_GIDExsComponents
	"EXCM_")

set( EXEOS_CCV_ENV_TARGET_REFID_PREFIX_GIDExsDrclayerGraphics
	"EXDC_")

set( EXEOS_CCV_ENV_TARGET_REFID_PREFIX_GIDExsDriverGraphics
	"EXDG_")

set( EXEOS_CCV_ENV_TARGET_REFID_PREFIX_GIDWorkshop
	"WRKS_")


# EXEOS_CCV_ENV_TARGET_NAME_PREFIX
# Group-specific prefix, prepended to a target id, which form the NAME of a target.
set( EXEOS_CCV_ENV_TARGET_NAME_PREFIX_GIDDepCustom
	"DepCustom.")
	
set( EXEOS_CCV_ENV_TARGET_NAME_PREFIX_GIDExsCommonLibs
	"EngineCommonLib.")
	
set( EXEOS_CCV_ENV_TARGET_NAME_PREFIX_GIDExsComponents
	"EngineComponent.")
	
set( EXEOS_CCV_ENV_TARGET_NAME_PREFIX_GIDExsDrclayerGraphics
	"EngineDrvGraphics.Common.")
	
set( EXEOS_CCV_ENV_TARGET_NAME_PREFIX_GIDExsDriverGraphics
	"EngineDrvGraphics.")
	
set( EXEOS_CCV_ENV_TARGET_NAME_PREFIX_GIDWorkshop
	"Workshop.")

	
# EXEOS_CCV_ENV_TARGET_OUTPUT_FILE_NAME_PREFIX
# String prepended to a name of the output file of targets from specific group.
set( EXEOS_CCV_ENV_TARGET_OUTPUT_FILE_NAME_PREFIX_GIDDepCustom
	"")
	
set( EXEOS_CCV_ENV_TARGET_OUTPUT_FILE_NAME_PREFIX_GIDExsCommonLibs
	"ExsLib")
	
set( EXEOS_CCV_ENV_TARGET_OUTPUT_FILE_NAME_PREFIX_GIDExsComponents
	"Exs.")
	
set( EXEOS_CCV_ENV_TARGET_OUTPUT_FILE_NAME_PREFIX_GIDExsDrclayerGraphics
	"ExsDriverCommonLayer.Graphics.")
	
set( EXEOS_CCV_ENV_TARGET_OUTPUT_FILE_NAME_PREFIX_GIDExsDriverGraphics
	"ExsDriver.Graphics.")
	
set( EXEOS_CCV_ENV_TARGET_OUTPUT_FILE_NAME_PREFIX_GIDWorkshop
	"")
