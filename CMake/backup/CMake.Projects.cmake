
set( EXEOS_PROJECTS_GIDDepCustom
	lua;
	png;
	stdx;
	zlib )
    
set( EXEOS_PROJECTS_GIDExsCommonLibs
	CoreUtils;
	System )
    
set( EXEOS_PROJECTS_GIDExsComponents
	Core
	Graphics )
    
set( EXEOS_PROJECTS_GIDExsDrclayerGraphics
	D3D
	GL
	MTL
	VK )
    
set( EXEOS_PROJECTS_GIDExsDriverGraphics
	D3D11
	D3D12
	GL3
	GL4
	MTL1
	MTL2
	VK1 )
    
set( EXEOS_PROJECTS_GIDWorkshop
	Test )

	
#
function( ExeosBuildAddCustomDependency name )
	message( STATUS "[Configuring custom dependency library: ${name}]" )
	exsAddBuildTarget( "GIDDepCustom" "${name}" )
	message( "" )
endfunction()

#
function( ExeosBuildAddEngineCommonLib name )
	message( STATUS "[Configuring common Exs library: ${name}]" )
	exsAddBuildTarget( "GIDExsCommonLibs" "${name}" )
	message( "" )
endfunction()

#
function( ExeosBuildAddEngineComponent name )
	message( STATUS "[Configuring engine component: ${name}]" )
	exsAddBuildTarget( "GIDExsComponents" "${name}" )
	message( "" )
endfunction()

#
function( ExeosBuildAddEngineDriverGraphics name )
	message( STATUS "[Configuring engine graphics driver: ${name}]" )
	exsCheckEngineDriverSupport( isDriverSupported driverGroupID "GRAPHICS" ${name} )
	if( ${isDriverSupported} )
		if( NOT EXS_CACHE_ENGINE_DRIVER_GRAPHICS_CLAYER_PROJECT_FLAG_${driverGroupID} )
			set( EXS_CACHE_ENGINE_DRIVER_GRAPHICS_CLAYER_PROJECT_FLAG_${driverGroupID} TRUE PARENT_SCOPE )
			exsAddBuildTarget( "GIDExsDrclayerGraphics" "${driverGroupID}" )
		endif()
		exsAddBuildTarget( "GIDExsDriverGraphics" "${name}" )
	else()
		message( STATUS "-- ${name} ignored." )
	endif()
	message( "" )
endfunction()

#
function( ExeosBuildAddWorkshopApp name )
	message( STATUS "[Configuring test application: ${name}]" )
	exsAddBuildTarget( "GIDWorkshop" "${name}" )
	message( "" )
endfunction()
    