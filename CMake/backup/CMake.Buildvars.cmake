


function( _internalExsParseBuildVariable )

	set( argsArray PLATFORM COMPILER ARCH BUILD GID TRID RETNAME RETVALUE RETGID RETTRID )
	
	cmake_parse_arguments( ARGLIST "" "${argsArray}" "VALUE" ${ARGN} )
	
	if( ARGLIST_RETVALUE )
		if( NOT ARGLIST_VALUE )
			cmake_parse_arguments( ARGLIST "" "${argsArray};VALUE" "" ${ARGN} )
			if( NOT ARGLIST_VALUE )
				cmake_parse_arguments( ARGLIST "VALUE" "${argsArray}" "" ${ARGN} )
			endif()
		endif()
		if( ARGLIST_VALUE )
			set( ${ARGLIST_RETVALUE} ${ARGLIST_VALUE} PARENT_SCOPE )
		endif()
	endif()
	
	if( NOT ARGLIST_PLATFORM )
		set( ARGLIST_PLATFORM "ALL" )
	endif()
	
	if( NOT ARGLIST_COMPILER )
		set( ARGLIST_COMPILER "ALL" )
	endif()
	
	if( NOT ARGLIST_ARCH )
		set( ARGLIST_ARCH "ALL" )
	endif()
	
	if( NOT ARGLIST_BUILD )
		set( ARGLIST_BUILD "ALL" )
	endif()
	
	if( ARGLIST_RETNAME )
		set( ${ARGLIST_RETNAME} "${ARGLIST_PLATFORM}_${ARGLIST_COMPILER}_${ARGLIST_ARCH}_${ARGLIST_BUILD}" PARENT_SCOPE )
	endif()
	
	if( ARGLIST_RETGID AND ARGLIST_GID )
		set( ${ARGLIST_RETGID} ${ARGLIST_GID} PARENT_SCOPE )
	endif()
	
	if( ARGLIST_RETTRID AND ARGLIST_TRID )
		set( ${ARGLIST_RETTRID} ${ARGLIST_TRID} PARENT_SCOPE )
	endif()
endfunction( _internalExsParseBuildVariable )

function( exsGetBuildVariable return variableName )
	_internalExsParseBuildVariable(
		PLATFORM ${EXEOS_CCV_PLATFORM}
		COMPILER ${EXEOS_CCV_COMPILER_NAME}
		ARCH ${EXEOS_CCV_ARCH_NAME}
		BUILD ${EXEOS_CCV_BUILD_TYPE_NAME}
		RETNAME varConditionalStr
		RETGID targetGID
		RETTRID targetRefID
		${ARGN} )
		
	if( targetRefID AND NOT targetGID )
		set( variableRegName EXEOS_BUILDVAR_ENV_TARGET_${targetRefID}_${variableName}_${varConditionalStr} )
	elseif( targetGID AND NOT targetRefID )
		set( variableRegName EXEOS_BUILDVAR_GROUP_${targetGID}_${variableName}_${varConditionalStr} )
	else()
		set( variableRegName EXEOS_BUILDVAR_GLOBAL_${variableName}_${varConditionalStr} )
	endif()
	
	set( ${return} ${${variableRegName}} PARENT_SCOPE )
endfunction( exsGetBuildVariable )

function( exsSetBuildVariable variableName )
	_internalExsParseBuildVariable(
		RETNAME varConditionalStr
		RETVALUE varValue
		RETGID targetGID
		RETTRID targetRefID
		${ARGN} )
		
	if( targetRefID )
		set( variableRegName EXEOS_BUILDVAR_ENV_TARGET_${targetRefID}_${variableName}_${varConditionalStr} )
	elseif( targetGID )
		set( variableRegName EXEOS_BUILDVAR_GROUP_${targetGID}_${variableName}_${varConditionalStr} )
	else()
		set( variableRegName EXEOS_BUILDVAR_GLOBAL_${variableName}_${varConditionalStr} )
	endif()
	
	set( ${variableRegName} ${varValue} CACHE FORCE INTERNAL ${variableRegName} )
endfunction( exsSetBuildVariable )

function( exsSetBuildVariableForCurrentConfig variableName )
	exsSetBuildVariable(
		${variableName}
		PLATFORM ${EXEOS_CCV_PLATFORM}
		COMPILER ${EXEOS_CCV_COMPILER_NAME}
		ARCH ${EXEOS_CCV_ARCH_NAME}
		BUILD ${EXEOS_CCV_BUILD_TYPE_NAME}
		${ARGN} )
endfunction( exsSetBuildVariableForCurrentConfig )

function( exsQueryCurrentBuildVariableValue return variableName )
	cmake_parse_arguments( VARARGS "" "GID;TRID" "" ${ARGN} )
	#set( result )
	foreach( platform ${EXEOS_CCV_DEF_ARRAY_PLATFORM} )
		foreach( compiler ${EXEOS_CCV_DEF_ARRAY_COMPILER} )
			foreach( arch ${EXEOS_CCV_DEF_ARRAY_ARCH} )
				foreach( build ${EXEOS_CCV_DEF_ARRAY_BUILD} )
					_internalExsParseBuildVariable( RETNAME varConditionalStr PLATFORM ${platform} COMPILER ${compiler} ARCH ${arch} BUILD ${build} )
					set( variableRegNameGlobal EXEOS_BUILDVAR_GLOBAL_${variableName}_${varConditionalStr} )
					if( variableRegNameGlobal )
						list( APPEND result ${${variableRegNameGlobal}} )
						#message("__QUERYGLOBAL: ${variableRegNameGlobal} ---> ${${variableRegNameGlobal}}" )
					endif()
					if( VARARGS_GID )
						set( variableRegNameGroup EXEOS_BUILDVAR_GROUP_${VARARGS_GID}_${variableName}_${varConditionalStr} )
						if( variableRegNameGroup )
							list( APPEND result ${${variableRegNameGroup}} )
							#message("__QUERYGROUP: ${variableRegNameGroup} ---> ${${variableRegNameGroup}}" )
						endif()
					endif()
					if( VARARGS_TRID )
						set( variableRegNameTarget EXEOS_BUILDVAR_ENV_TARGET_${VARARGS_TRID}_${variableName}_${varConditionalStr} )
						if( variableRegNameTarget )
							list( APPEND result ${${variableRegNameTarget}} )
							#message("__QUERYENV_TARGET: ${variableRegNameTarget} ---> ${${variableRegNameTarget}}" )
						endif()
					endif()
					if( result )
						#message( "##QUERY: ${variableName}_${varConditionalStr} -> ${result}" )
					endif()
				endforeach()
			endforeach()
		endforeach()
	endforeach()
	if( ${variableName} STREQUAL "INCLUDE_SEARCH_PATH" )
		message( "VQUERY: ${variableName}_${varConditionalStr} ---> ${result}" )
	endif()
	set( ${return} ${result} PARENT_SCOPE )
endfunction( exsQueryCurrentBuildVariableValue )
