
include_guard()

ebsCoreAddModuleRef( "Utils.List" )
ebsCoreAddModuleRef( "VMSBuild.Defs" )
ebsCoreAddModuleRef( "VMSBuild.Filters" )
ebsCoreAddModuleRef( "VMSWorkspace.Interface" )


macro( ebsBuildSetGlobalVariable pVarName pValue )
    _ebsVMSBuildVGlobalSetInternal( ${pVarName} "${pValue}" )
endmacro()

macro( ebsBuildSetGroupVariable pGroupID pVarName pValue )
    _ebsVMSBuildVGroupSetInternal( ${pGroupID} ${pVarName} "${pValue}" )
endmacro()

macro( ebsBuildSetProjectVariable pProjectID pVarName pValue )
    _ebsVMSBuildVProjectSetInternal( ${pProjectID} ${pVarName} "${pValue}" )
endmacro()

macro( ebsWorkspaceSetGroupRootDir pGroupID pRootDir )
    _ebsVMSBuildVGroupSetInternal( ${pGroupID} "ROOT_DIR" ${pRootDir} )
endmacro()

macro( ebsWorkspaceSetGroupSrcSubdir pGroupID pSrcSubdir )
    _ebsVMSBuildVGroupSetInternal( ${pGroupID} "SRC_SUBDIR" ${pSrcSubdir} )
endmacro()

macro( ebsBuildSetGroupArtifactType pGroupID pArtifactType )
    _ebsVMSBuildVGroupSetInternal( ${pGroupID} "ARTIFACT_TYPE" "${pArtifactType}" )
endmacro()


macro( ebsVMSBuildQueryProjectProperty pMode pProjectID pPropertyName outValue )
    ebsWorkspaceGetProjectGroupID( ${pProjectID} projectGroupID )
    _ebsBuildImplQueryVariable( "${pMode}" "${projectGroupID}" "${pProjectID}" ${pPropertyName} ${outValue} )
endmacro()


macro( ebsBuildConfigGet pVarName outValue )
    _ebsVMSBuildImplVarGet( "CONFIG" "" ${pVarName} "" ${outValue} )
endmacro()

macro( ebsBuildConfigAppend pVarName pValue )
    _ebsVMSBuildImplVarAppend( FALSE "CONFIG" "" ${pVarName} "" "${pValue}" )
endmacro()

macro( ebsBuildConfigSet pVarName pValue )
    _ebsVMSBuildImplVarSet( FALSE "CONFIG" "" ${pVarName} "" "${pValue}" )
endmacro()

macro( ebsBuildConfigUnset pVarName )
    _ebsVMSBuildImplVarUnset( FALSE "CONFIG" "" ${pVarName} "" )
endmacro()


macro( ebsBuildVGlobalGet pVarName outValue )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarGet( "VGLOBAL" "" ${pVarName} "${varFilter}" ${outValue} )
endmacro()

macro( ebsBuildVGlobalAppend pVarName pValue )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarAppend( FALSE "VGLOBAL" "" ${pVarName} "${varFilter}" "${pValue}" )
endmacro()

macro( ebsBuildVGlobalSet pVarName pValue )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarSet( FALSE "VGLOBAL" "" ${pVarName} "${varFilter}" "${pValue}" )
endmacro()

macro( ebsBuildVGlobalUnset pVarName )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarUnset( FALSE "VGLOBAL" "" ${pVarName} "${varFilter}" )
endmacro()


macro( ebsBuildVGroupGet pGroupID pVarName outValue )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarGet( "VGROUP" ${pGroupID} ${pVarName} "${varFilter}" ${outValue} )
endmacro()

macro( ebsBuildVGroupAppend pGroupID pVarName pValue )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarAppend( FALSE "VGROUP" ${pGroupID} ${pVarName} "${varFilter}" "${pValue}" )
endmacro()

macro( ebsBuildVGroupSet pGroupID pVarName pValue )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarSet( FALSE "VGROUP" ${pGroupID} ${pVarName} "${varFilter}" "${pValue}" )
endmacro()

macro( ebsBuildVGroupUnset pGroupID pVarName )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarUnset( FALSE "VGROUP" ${pGroupID} ${pVarName} "${varFilter}" )
endmacro()


macro( ebsBuildVProjectGet pProjectID pVarName outValue )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarGet( "VPROJECT" ${pProjectID} ${pVarName} "${varFilter}" ${outValue} )
endmacro()

macro( ebsBuildVProjectAppend pProjectID pVarName pValue )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarAppend( FALSE "VPROJECT" ${pProjectID} ${pVarName} "${varFilter}" "${pValue}" )
endmacro()

macro( ebsBuildVProjectSet pProjectID pVarName pValue )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarSet( FALSE "VPROJECT" ${pProjectID} ${pVarName} "${varFilter}" "${pValue}" )
endmacro()

macro( ebsBuildVProjectUnset pProjectID pVarName )
    _ebsVMSBuildImplParseVarargInput( varFilter "${ARGN}" )
    _ebsVMSBuildImplVarUnset( FALSE "VPROJECT" ${pProjectID} ${pVarName} "${varFilter}" )
endmacro()
