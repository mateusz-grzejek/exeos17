
include_guard()

ebsCoreAddModuleRef( "VPRGen.Impl.Helpers" )

# set( EBS_CONFIG_VMS_ENABLE_VAR_TRACE TRUE )

function( ebsBuildProjectTree )

    ebsWorkspaceGetRegisteredProjectList( projectList )

    foreach( projectID ${projectList} )
    
        ebsWorkspaceGetProjectRootDir( ${projectID} rootDir )
        
        set( projectRootPath "${CMAKE_SOURCE_DIR}/${rootDir}" )

        if( NOT EXISTS "${projectRootPath}" )
            message( STATUS "[EBSGen] Root path of project ${projectID} does not exist. Project has been ingored." )
            continue()
        endif()

        if( NOT EXISTS "${projectRootPath}/CMakeLists.txt" )
            message( STATUS "[EBSGen] CMakeLists.txt for project ${projectID} not found. Project has been ingored." )
            continue()
        endif()

        ebsWorkspaceCheckProjectPlatformSupport( ${projectID} isProjectSupported )

        if( NOT isProjectSupported )
            message( STATUS "[EBSGen] Project ${projectID} is not supported on this platform. Project has been ingored." )
            continue()
        endif()
	
        add_subdirectory( ${projectRootPath} )

        _ebsVPRGenImplGetProjectSourceFileList( ${projectID} srcFileList )
    
        ebsVMSBuildQueryProjectProperty( "OVERWRITE" ${projectID} "ARTIFACT_TYPE" projectArtifactType )

        message( "ATT: ${projectArtifactType}" )

    endforeach()

endfunction()
