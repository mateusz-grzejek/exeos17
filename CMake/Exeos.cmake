
include_guard()


if( NOT EXEOS_CMAKE_PATH )
    message( FATAL_ERROR "EXEOS_CMAKE_PATH has not been defined."  )
endif()

if( NOT EXISTS "${EXEOS_CMAKE_PATH}" )
    message( FATAL_ERROR "EXEOS_CMAKE_PATH is not a valid path or it does not exist."  )
endif()


include( "${EXEOS_CMAKE_PATH}/_Exeos.Config.Workspace.cmake" )
include( "${EXEOS_CMAKE_PATH}/_Exeos.Config.Build.cmake" )
include( "${EXEOS_CMAKE_PATH}/_Exeos.Exec.Init.cmake" )
