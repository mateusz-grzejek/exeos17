
include_guard()

ebsCoreAddModuleRef( "VMSBase" )


macro( ebsBaseGetVariableRegListBuild outList )
    _ebsImplVMSGetInternalRegList( "BUILD" ${outList} )
endmacro()


macro( ebsBaseGetVariableRegListSysconf outList )
    _ebsImplVMSGetInternalRegList( "SYSCONF" ${outList} )
endmacro()


macro( ebsBaseGetVariableRegListWorkspace outList )
    _ebsImplVMSGetInternalRegList( "WORKSPACE" ${outList} )
endmacro()


function( ebsBaseGetVariableRegDump outList )

    _ebsVMSBaseImplGetInternalRegList( "BUILD" buildVarList )
    _ebsVMSBaseImplGetInternalRegList( "SYSCONF" sysconfVarList )
    _ebsVMSBaseImplGetInternalRegList( "WORKSPACE" workspaceVarList )

    set( ${outList} "${buildVarList};${sysconfVarList};${workspaceVarList}" PARENT_SCOPE )

endfunction()


function( ebsBaseCoreReset )
    _ebsImplVMSResetRegistry()
endfunction()


function( ebsBasePrintVariableRegDump )

    _ebsVMSBaseImplGetInternalRegList( "BUILD" buildVarList )
    _ebsVMSBaseImplGetInternalRegList( "SYSCONF" sysconfVarList )
    _ebsVMSBaseImplGetInternalRegList( "WORKSPACE" workspaceVarList )

    message( STATUS "[EBS Variable Registry Dump]" )

    message( STATUS "-- BUILD" )
    foreach( buildVarName ${buildVarList} )
        message( STATUS "---- ${buildVarName}  --->   ${${buildVarName}}" )
    endforeach()

    message( STATUS "-- SYSCONF" )
    foreach( sysconfVarName ${sysconfVarList} )
        message( STATUS "---- ${sysconfVarName}  --->   ${${sysconfVarName}}" )
    endforeach()

    message( STATUS "-- WORKSPACE" )
    foreach( workspaceVarName ${workspaceVarList} )
        message( STATUS "---- ${workspaceVarName}  --->   ${${workspaceVarName}}" )
    endforeach()

endfunction()
