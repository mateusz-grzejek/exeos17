
include_guard()

macro( ebsListAppendUnique listName value )
    list( FIND ${listName} ${value} refpos )
    if( refpos EQUAL -1 )
        list( APPEND ${listName} ${value} )
    endif()
endmacro()
