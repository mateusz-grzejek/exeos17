
include_guard()

ebsCoreAddModuleRef( "VMSBase" )

# List of variable categories within the SYSCONF module.
set( _EBS_VMS_SYSCONF_DEF_VARCATEGORYLIST
    "ENVHOST" # Host environment config, desribing system when CMake executes.
    "ENVTARGET" # Target configuration - either host or something else for cross-compiling.
    "PLATFORM" # Global, platform-specific configuration variables.
)


# ENVHOST variables are automagically initialized with proper values depending on the
# current environment and provided options. None of the is modifiable by the client.
set( _EBS_VMS_SYSCONF_DEF_VARLIST_ENVHOST
)

# List of SYSCONF::ENVHOST variables:
set( _EBS_VMS_SYSCONF_DEF_VARLIST_ENVHOST_INTERNAL
    ${_EBS_VMS_SYSCONF_DEF_ENVHOST_VARLIST}
    "COMPILER" # Name of the compiler used to build the projects in the workspace.
    "OSNAME"
    "OSVERSION"
    "TOOLSET_ARCH"
    "TOOLSET_ARCHBIT"
    "FLAG_GEN_MULTITARGET"
)


# List of SYSCONF::ENVTARGET variables, which can be modified by the client (user).
set( _EBS_VMS_SYSCONF_DEF_VARLIST_ENVTARGET
    "ARCH"
    "ARCHBIT"
    "BUILDCFG"
    "DBGINFO"
    "OS"
)

# List of SYSCONF::ENVTARGET variables:
set( _EBS_VMS_SYSCONF_DEF_VARLIST_ENVTARGET_INTERNAL
    ${_EBS_VMS_SYSCONF_DEF_VARLIST_ENVTARGET}
    "ARCHDEF32"
    "ARCHDEF64"
    "ARCHDEFBIT"
    "PLATFORM"
    "SUBSYSTEM"
    "FLAG_ARCH_ARMF"
    "FLAG_ARCH_X86F"
    "FLAG_OS_ANDROID"
    "FLAG_OS_APPLE"
    "FLAG_OS_POSIX"
    "FLAG_OS_WINFAMILY"
)

# List of SYSCONF::PLATFORM variables:
set( _EBS_VMS_SYSCONF_DEF_VARLIST_PLATFORM
    "ANDROID_API"
    "ANDROID_API_MIN"
    "ANDROID_NDK_PATH"
    "ANDROID_SDK_PATH"
    "WINPHONE_VERSION"
    "WINRT_VERSION"
)

# List of possible values for SYSCONF::ENVTARGET::ARCH variable.
set( _EBS_VMS_SYSCONF_DEF_VARVALUELIST_ENVTARGET_ARCH
	"ARM" # 32-bit ARM architecture (armv7)
	"ARM64" # 64-bit ARM architecture (armv64v8)
	"x86" # 32-bit x86 architecture
    "x86_64" # 64-bit, x86-based (AMD64) architecture
)

# List of possible values for SYSCONF::ENVTARGET::ARCHBIT variable.
set( _EBS_VMS_SYSCONF_DEF_VARVALUELIST_ENVTARGET_ARCHBIT
	"32" # 32-bit target architecture (CMAKE_SIZEOF_VOID_P == 4)
	"64" # 64-bit target architecture (CMAKE_SIZEOF_VOID_P == 8)
)

#
set( _EBS_VMS_SYSCONF_DEF_VARVALUELIST_ENVTARGET_OS
	"Android" # Android
	"Linux" # Linux family
	"iOS" # iOS 9+
	"MacOSX" # MacOS X
	"WinDesktop" # Win32 desktop
	"WinPhone" # WindowsPhone 8.1
	"WinRT" # WindowsRT (32-bit ARM based)
    "WinUWP" # WindowsStore, UWP-compatible platform
)

# Matches all 32-bit architectures: ARM, x86
set( _EBS_VMS_MATCH_STR_ARCH_32 "^ARM$|^x86$" )

# Matches all 64-bit architectures: ARM64, x86_64
set( _EBS_VMS_MATCH_STR_ARCH_64 "^ARM64$|^x86_64$" )

# Matches all ARM-based architectures: ARM, ARM64
set( _EBS_VMS_MATCH_STR_ARCH_ARMF "^ARM(64)*$" )

# Matches all x86-based architectures: x86, x86_64
set( _EBS_VMS_MATCH_STR_ARCH_X86F "^x86(_64)*$" )


function( _ebsVMSSysconfImplCheckVariableRequest pAllowInternal pVarCategory pVarName )

    # TODO

endfunction()


macro( _ebsVMSSysconfImplVarGet pVarCategory pVarName outValue )
    _ebsVMSSysconfImplCheckVariableRequest( TRUE ${pVarCategory} ${pVarName} )
    _ebsVMSBaseVarGet( "SYSCONF" ${pVarCategory} ${pVarName} ${outValue} )
endmacro()

macro( _ebsVMSSysconfImplVarAppend pAllowInternal pVarCategory pVarName pValue )
    _ebsVMSSysconfImplCheckVariableRequest( TRUE ${pVarCategory} ${pVarName} )
    _ebsVMSBaseVarAppend( ${pAllowInternal} "SYSCONF" ${pVarCategory} ${pVarName} ${pValue} )
endmacro()

macro( _ebsVMSSysconfImplVarSet pAllowInternal pVarCategory pVarName pValue )
    _ebsVMSSysconfImplCheckVariableRequest( TRUE ${pVarCategory} ${pVarName} )
    _ebsVMSBaseVarSet( ${pAllowInternal} "SYSCONF" ${pVarCategory} ${pVarName} ${pValue} )
endmacro()

macro( _ebsVMSSysconfImplVarUnset pAllowInternal pVarCategory pVarName )
    _ebsVMSSysconfImplCheckVariableRequest( TRUE ${pVarCategory} ${pVarName} )
    _ebsVMSBaseVarUnset( ${pAllowInternal} "SYSCONF" ${pVarCategory} ${pVarName} )
endmacro()


macro( _ebsVMSSysconfEnvhostGetInternal pVarName outValue )
    _ebsVMSSysconfImplVarGet( ${outValue} "ENVHOST" ${pVarName} )
endmacro()

macro( _ebsVMSSysconfEnvhostAppendInternal pVarName pValue )
    _ebsVMSSysconfImplVarAppend( TRUE "ENVHOST" ${pVarName} ${pValue} )
endmacro()

macro( _ebsVMSSysconfEnvhostSetInternal pVarName pValue )
    _ebsVMSSysconfImplVarSet( TRUE "ENVHOST" ${pVarName} ${pValue} )
endmacro()

macro( _ebsVMSSysconfEnvhostUnsetInternal pVarName )
    _ebsVMSSysconfImplVarUnset( TRUE "ENVHOST" ${pVarName} )
endmacro()


macro( _ebsVMSSysconfEnvtargetGetInternal pVarName outValue )
    _ebsVMSSysconfImplVarGet( "ENVTARGET" ${pVarName} ${outValue} )
endmacro()

macro( _ebsVMSSysconfEnvtargetAppendInternal pVarName pValue )
    _ebsVMSSysconfImplVarAppend( TRUE "ENVTARGET" ${pVarName} ${pValue} )
endmacro()

macro( _ebsVMSSysconfEnvtargetSetInternal pVarName pValue )
    _ebsVMSSysconfImplVarSet( TRUE "ENVTARGET" ${pVarName} ${pValue} )
endmacro()

macro( _ebsVMSSysconfEnvtargetUnsetInternal pVarName )
    _ebsVMSSysconfImplVarUnset( TRUE "ENVTARGET" ${pVarName} )
endmacro()


macro( _ebsVMSSysconfPlatformGetInternal pVarName outValue )
    _ebsVMSSysconfImplVarGet( "PLATFORM" ${pVarName} ${outValue} )
endmacro()

macro( _ebsVMSSysconfPlatformAppendInternal pVarName pValue )
    _ebsVMSSysconfImplVarAppend( TRUE "PLATFORM" ${pVarName} ${pValue} )
endmacro()

macro( _ebsVMSSysconfPlatformSetInternal pVarName pValue )
    _ebsVMSSysconfImplVarSet( TRUE "PLATFORM" ${pVarName} ${pValue} )
endmacro()

macro( _ebsVMSSysconfPlatformUnsetInternal pVarName )
    _ebsVMSSysconfImplVarUnset( TRUE "PLATFORM" ${pVarName} )
endmacro()
