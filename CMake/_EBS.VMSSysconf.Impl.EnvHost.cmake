
include_guard()

ebsCoreAddModuleRef( "VMSSysconf.Defs" )


#
function( _ebsVMSSysconfEnvHostAnalyzeCMakeEnv )

    if( NOT DEFINED EBS_VAR_SYSCONF_ENVHOST_FLAG_GEN_MULTITARGET )
        if( MSVC OR MSVC_IDE OR XCODE_VERSION )
            _ebsVMSSysconfEnvhostSetInternal( FLAG_GEN_MULTITARGET TRUE )
        else()
            _ebsVMSSysconfEnvhostSetInternal( FLAG_GEN_MULTITARGET FALSE )
        endif()
    endif()

endfunction()

#
function( _ebsVMSSysconfEnvHostDetectCompiler )

    if( MINGW )
        _ebsVMSSysconfEnvhostSetInternal( COMPILER "MinGW" )
    elseif ( "${CMAKE_CXX_COMPILER_ID}" STREQUAL "ARMCC" )
        _ebsVMSSysconfEnvhostSetInternal( COMPILER "ARMCC" )
    elseif ( "${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang" )
        _ebsVMSSysconfEnvhostSetInternal( COMPILER "Clang" )
    elseif ( "${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU" )
        _ebsVMSSysconfEnvhostSetInternal( COMPILER "GCC" )
    elseif ( "${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel" )
        _ebsVMSSysconfEnvhostSetInternal( COMPILER "ICC" )
    elseif ( "${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC" )
        _ebsVMSSysconfEnvhostSetInternal( COMPILER "MSVC" )
    elseif ( "${CMAKE_CXX_COMPILER_ID}" STREQUAL "NVIDIA" )
        _ebsVMSSysconfEnvhostSetInternal( COMPILER "NVCUDA" )
    endif()

    if( NOT EBS_VAR_SYSCONF_ENVHOST_COMPILER )
        message( FATAL_ERROR "Current compiler (${CMAKE_CXX_COMPILER_ID}) is not supported." )
    endif()
    
    if( CMAKE_GENERATOR_PLATFORM )
        if( "${CMAKE_GENERATOR}" MATCHES "Visual Studio" )
            if( "${CMAKE_GENERATOR_PLATFORM}" MATCHES "^ARM$" )
                _ebsVMSSysconfEnvhostSetInternal( TOOLSET_ARCH "ARM" )
            elseif( "${CMAKE_GENERATOR_PLATFORM}" MATCHES "ARM64" )
                _ebsVMSSysconfEnvhostSetInternal( TOOLSET_ARCH "ARM64" )
            elseif( "${CMAKE_GENERATOR_PLATFORM}" MATCHES "IA32|^x86$" )
                _ebsVMSSysconfEnvhostSetInternal( TOOLSET_ARCH "x86" )
            elseif( "${CMAKE_GENERATOR_PLATFORM}" MATCHES "Win64|x86_64" )
                _ebsVMSSysconfEnvhostSetInternal( TOOLSET_ARCH "x86_64" )
            endif()
        endif()
    else()
        if( "${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC" )
            if( "${CMAKE_MODULE_LINKER_FLAGS}" MATCHES "^ARM$" )
                _ebsVMSSysconfEnvhostSetInternal( TOOLSET_ARCH "ARM" )
            elseif( "${CMAKE_MODULE_LINKER_FLAGS}" MATCHES "ARM64" )
                _ebsVMSSysconfEnvhostSetInternal( TOOLSET_ARCH "ARM64" )
            elseif( "${CMAKE_MODULE_LINKER_FLAGS}" MATCHES "x64" )
                _ebsVMSSysconfEnvhostSetInternal( TOOLSET_ARCH "x86_64" )
            elseif( "${CMAKE_MODULE_LINKER_FLAGS}" MATCHES "x86" )
                _ebsVMSSysconfEnvhostSetInternal( TOOLSET_ARCH "x86" )
            endif()
        endif()
    endif()
    
    if( EBS_VAR_SYSCONF_ENVHOST_TOOLSET_ARCH )
        if( "${EBS_VAR_SYSCONF_ENVHOST_TOOLSET_ARCH}" MATCHES "${_EBS_MATCH_STR_ARCH_32}" )
            _ebsVMSSysconfEnvhostSetInternal( TOOLSET_ARCHBIT "32" )
        elseif( "${EBS_VAR_SYSCONF_ENVHOST_TOOLSET_ARCH}" MATCHES "${_EBS_MATCH_STR_ARCH_32}" )
            _ebsVMSSysconfEnvhostSetInternal( TOOLSET_ARCHBIT "64" )
        endif()
    endif()

endfunction()


#
function( _ebsVMSSysconfEnvHostDetectSystemAndVersion )
    # Get name of the host system.
    _ebsCoreGetSystemName( TRUE hostOSName )
    _ebsVMSSysconfEnvhostSetInternal( OSNAME "${hostOSName}" )

    if( NOT EBS_VAR_SYSCONF_ENVHOST_OSNAME )
        message( FATAL_ERROR "" )
    endif()

    if( EBS_VAR_SYSCONF_ENVHOST_OSNAME )
        if( ${EBS_VAR_SYSCONF_ENVHOST_OSNAME} STREQUAL "WinDesktop" )
            if( ${CMAKE_HOST_SYSTEM_VERSION} MATCHES "10\.[0\.[0-9]+" )
                _ebsVMSSysconfEnvhostSetInternal( OSVERSION "10.0" )
            elseif( ${CMAKE_HOST_SYSTEM_VERSION} MATCHES "6\.3\.96[0-9]+" )
                _ebsVMSSysconfEnvhostSetInternal( OSVERSION "8.1U1" )
            elseif( ${CMAKE_HOST_SYSTEM_VERSION} MATCHES "6\.3\.92[0-9]+" )
                _ebsVMSSysconfEnvhostSetInternal( OSVERSION "8.1" )
            elseif( ${CMAKE_HOST_SYSTEM_VERSION} MATCHES "6\.2\.92[0-9]+" )
                _ebsVMSSysconfEnvhostSetInternal( OSVERSION "8.0" )
            elseif( ${CMAKE_HOST_SYSTEM_VERSION} MATCHES "6\.1\.7601" )
                _ebsVMSSysconfEnvhostSetInternal( OSVERSION "7SP1" )
            elseif( ${CMAKE_HOST_SYSTEM_VERSION} MATCHES "6\.1\.7600" )
                _ebsVMSSysconfEnvhostSetInternal( OSVERSION "7" )
            elseif( ${CMAKE_HOST_SYSTEM_VERSION} MATCHES "6\.0\.6002" )
                _ebsVMSSysconfEnvhostSetInternal( OSVERSION "VistaSP2" )
            elseif( ${CMAKE_HOST_SYSTEM_VERSION} MATCHES "6\.0\.6001" )
                _ebsVMSSysconfEnvhostSetInternal( OSVERSION "VistaSP1" )
            elseif( ${CMAKE_HOST_SYSTEM_VERSION} MATCHES "6\.0\.6000" )
                _ebsVMSSysconfEnvhostSetInternal( OSVERSION "Vista" )
            endif()
        else()
            _ebsVMSSysconfEnvhostSetInternal( OSVERSION "${CMAKE_HOST_SYSTEM_VERSION}" )
        endif()
    endif()

    if( NOT EBS_VAR_SYSCONF_ENVHOST_OSNAME OR NOT EBS_VAR_SYSCONF_ENVHOST_OSVERSION )
        message( FATAL_ERROR "" )
    endif()

endfunction()


# 
function( _ebsVMSSysconfEnvHostInitialize )
    #
    _ebsVMSSysconfEnvHostAnalyzeCMakeEnv()

	#
	_ebsVMSSysconfEnvHostDetectCompiler()

	#
	_ebsVMSSysconfEnvHostDetectSystemAndVersion()
endfunction()
