
ebsCoreAddModuleRef( "VMSWorkspace.Defs" )


macro( ebsWorkspaceGetProjectGroupID pProjectID outGroupID )
    _ebsVMSWorkspaceVProjectGetInternal( ${pProjectID} "GROUP_ID" ${outGroupID} )
endmacro()


function( ebsWorkspaceGetProjectID pGroupID pProjectName outProjectID )

    if( pGroupID )
        _ebsVMSWorkspaceVGroupGetInternal( ${pGroupID} "PROJECT_ID_PREFIX" projectIDPrefix )
    else()
        set( projectIDPrefix "_" )
    endif()

    set( ${outProjectID} "${projectIDPrefix}${pProjectName}" PARENT_SCOPE )

endfunction()


function( ebsWorkspaceGetProjectRootDir pProjectID outRootDir )

    ebsWorkspaceGetProjectGroupID( ${pProjectID} projectGroupID )

    _ebsVMSWorkspaceVProjectGetInternal( ${pProjectID} "ROOT_DIR" projectRootDir )

    if( NOT projectRootDir AND projectGroupID )
        _ebsVMSWorkspaceVGroupGetInternal( ${projectGroupID} "ROOT_DIR" groupRootDir )
        _ebsVMSWorkspaceVGroupGetInternal( ${projectGroupID} "PROJECT_SUBDIR" groupProjectSubdir )
        set( projectRootDir "${groupRootDir}${groupProjectSubdir}" )
    endif()

    if( projectRootDir )
        _ebsVMSWorkspaceVProjectGetInternal( ${pProjectID} "NAME" projectName )
        
        string( REPLACE "@{GroupID}" "${projectGroupID}" projectRootDir ${projectRootDir} )
        string( REPLACE "@{ProjectName}" "${projectName}" projectRootDir ${projectRootDir} )
        
        set( ${outRootDir} ${projectRootDir} PARENT_SCOPE ) 
    endif()

endfunction()


function( ebsWorkspaceCheckProjectPlatformSupport pProjectID outIsSupported )

    ebsVMSSysconfEnvtargetGet( "OS" targetOS )

    _ebsVMSWorkspaceVProjectGetInternal( ${pProjectID} "TARGET_OS_SUPPORT_LIST" projectOSSupportList )
    if( ( "${projectOSSupportList}" STREQUAL "*" ) OR ( ${targetOS} IN_LIST projectOSSupportList ) )
        ebsWorkspaceGetProjectGroupID( ${pProjectID} projectGroupID )
        _ebsVMSWorkspaceVGroupGetInternal( ${projectGroupID} "TARGET_OS_SUPPORT_LIST" projectGroupOSSupportList )
        if( ( "${projectGroupOSSupportList}" STREQUAL "*" ) OR ( ${targetOS} IN_LIST projectGroupOSSupportList ) )
            set( ${outIsSupported} TRUE PARENT_SCOPE )
        endif()
    endif()

endfunction()


function( ebsWorkspaceRegisterGroup pGroupID )

    _ebsVMSWorkspaceRegistryAppendInternal( "GROUP_LIST" ${pGroupID} )

    _ebsVMSWorkspaceVGroupSetInternal( ${pGroupID} "DEPENDENCY_WHITELIST" "*" )
    _ebsVMSWorkspaceVGroupSetInternal( ${pGroupID} "TARGET_OS_SUPPORT_LIST" "*" )

endfunction()

macro( ebsWorkspaceSetGroupDependencyWhitelist pGroupID pWhitelist )
    _ebsVMSWorkspaceVGroupSetInternal( ${pGroupID} "DEPENDENCY_WHITELIST" "${pWhitelist}" )
endmacro()

macro( ebsWorkspaceSetGroupProjectIDPrefix pGroupID pProjectIDPrefix )
    _ebsVMSWorkspaceVGroupSetInternal( ${pGroupID} "PROJECT_ID_PREFIX" "${pProjectIDPrefix}" )
endmacro()

macro( ebsWorkspaceSetGroupRootDir pGroupID pRootDir )
    _ebsVMSWorkspaceVGroupSetInternal( ${pGroupID} "ROOT_DIR" "${pRootDir}" )
endmacro()

macro( ebsWorkspaceSetGroupProjectSubdir pGroupID pProjectSubdir )
    _ebsVMSWorkspaceVGroupSetInternal( ${pGroupID} "PROJECT_SUBDIR" "${pProjectSubdir}" )
endmacro()

macro( ebsWorkspaceSetGroupTargetPlatformSupport pGroupID pSupportedOSList )
    _ebsVMSWorkspaceVGroupSetInternal( ${pGroupID} "TARGET_OS_SUPPORT_LIST" "${pSupportedOSList}" )
endmacro()



function( ebsWorkspaceRegisterProject pGroupID pProjectName outProjectID )

    ebsWorkspaceGetProjectID( ${pGroupID} ${pProjectName} projectID )

    _ebsVMSWorkspaceRegistryAppendInternal( "PROJECT_LIST" ${projectID} )

    _ebsVMSWorkspaceVProjectSetInternal( ${projectID} "GROUP_ID" "${pGroupID}" )
    _ebsVMSWorkspaceVProjectSetInternal( ${projectID} "NAME" "${pProjectName}" )
    _ebsVMSWorkspaceVProjectSetInternal( ${projectID} "TARGET_OS_SUPPORT_LIST" "*" )

    set( ${outProjectID} ${projectID} PARENT_SCOPE )
    
endfunction()


macro( ebsWorkspaceSetProjectTargetPlatformSupport pProjectID pSupportedOSList )
    _ebsVMSWorkspaceVProjectSetInternal( ${pProjectID} "TARGET_OS_SUPPORT_LIST" "${pSupportedOSList}" )
endmacro()


macro( ebsWorkspaceGetRegisteredProjectList outProjectList )
    _ebsVMSWorkspaceRegistryGetInternal( "PROJECT_LIST" ${outProjectList} )
endmacro()


function( ebsWorkspacePrintRegisteredProjectsInfo )
    ebsWorkspaceGetRegisteredProjectList( regProjectList )
    message( "[Registered projects]" )
    foreach( projectID ${regProjectList} )
        _ebsVMSWorkspaceVProjectGetInternal( projectGroupID ${projectID} "GROUP_ID" )
        if( NOT projectGroupID )
            set( projectGroupID "<no group>" )
        endif()
        message( "-- ${projectGroupID}::${projectID}" )
    endforeach()
endfunction()

macro( ebsWorkspaceAddGroupDependency pGroupID pDependencyGroupID pProjectList )
endmacro()

macro( _ebsVMSWorkspaceAddProjectDependency pProjectID pDependencyGroupID pProjectList )
endmacro()

macro( _ebsVMSWorkspaceSetGroupDependencyControlList pGroupID pGroupList )
endmacro()



macro( ebsVMSWorkspaceRegistryGet pVarName outValue )
    _ebsVMSWorkspaceVarGet( "REGISTRY" "" ${pVarName} "" ${outValue} )
endmacro()

macro( ebsVMSWorkspaceRegistryAppend pAllowInternal pVarName pValue )
    _ebsVMSWorkspaceVarAppend( FALSE "REGISTRY" "" ${pVarName} "" "${pValue}" )
endmacro()

macro( ebsVMSWorkspaceRegistrySet pAllowInternal pVarName pValue )
    _ebsVMSWorkspaceVarSet( FALSE "REGISTRY" "" ${pVarName} "" "${pValue}" )
endmacro()

macro( ebsVMSWorkspaceRegistryUnset pAllowInternal pVarName )
    _ebsVMSWorkspaceVarUnset( FALSE "REGISTRY" "" ${pVarName} "" )
endmacro()
