
set( EXEOS_CCV_WORKSPACE_GROUP_LIST
    GIDCorelibs # core (external) libs, which are distributed with the engine to ensure compatibility (lua, png, etc.)
    #GIDExsComponents # core components of the engine
    #GIDExsGraphicsDriverCommon # common layer for graphics drivers (common GL, common D3D, DXGI, etc.)
    #GIDExsGraphicsDriverModules # graphics drivers (D3D11, GL4, VK1, etc.)
    #GIDTests # tests for all engine components
    #GIDToolset # projects which are part of the toolkit
    #GIDUtilities # utilities, external tools, not part of the engine itself
    #GIDWorkshop # sample apps, development apps, internal code samples
)


set( EXEOS_CCV_WORKSPACE_PROJECT_LIST_GIDCorelibs
    freetype
    lua
    png
    zlib )

set( EXEOS_CCV_WORKSPACE_PROJECT_LIST_GIDExsComponents
    Core
    Engine
    Graphics )

set( EXEOS_CCV_WORKSPACE_PROJECT_LIST_GIDExsGraphicsDriverCommon
    D3D
    DXGI
    GL )

set( EXEOS_CCV_WORKSPACE_PROJECT_LIST_GIDExsGraphicsDriverModules
    D3D11
    D3D12
    GL4
    MTL2
    VK1 )

set( EXEOS_CCV_WORKSPACE_PROJECT_LIST_GIDWorkshop
    BasicSample )


#
#
set( EXEOS_CCV_WORKSPACE_PROJECT_OS_SUPPORT_LIST_GIDExsGraphicsDriverCommon_D3D
    WinDesktop
    WinPhone
    WinRT
    WinUWP )

set( EXEOS_CCV_WORKSPACE_PROJECT_OS_SUPPORT_LIST_GIDExsGraphicsDriverCommon_DXGI
    WinDesktop
    WinPhone
    WinRT
    WinUWP )

set( EXEOS_CCV_WORKSPACE_PROJECT_OS_SUPPORT_LIST_GIDExsGraphicsDriverCommon_GL
    Android
    Linux
    iOS
    MacOSX
    WinDesktop )
    

#
#
set( EXEOS_CCV_WORKSPACE_PROJECT_OS_SUPPORT_LIST_GIDExsGraphicsDriverModules_D3D11
    WinDesktop
    WinPhone
    WinRT
    WinUWP )

set( EXEOS_CCV_WORKSPACE_PROJECT_OS_SUPPORT_LIST_GIDExsGraphicsDriverModules_D3D12
    WinDesktop
    WinRT
    WinUWP )

set( EXEOS_CCV_WORKSPACE_PROJECT_OS_SUPPORT_LIST_GIDExsGraphicsDriverModules_GL4
    Android
    Linux
    iOS
    MacOSX
    WinDesktop )

set( EXEOS_CCV_WORKSPACE_PROJECT_OS_SUPPORT_LIST_GIDExsGraphicsDriverModules_MTL2
    iOS
    MacOSX )

set( EXEOS_CCV_WORKSPACE_PROJECT_OS_SUPPORT_LIST_GIDExsGraphicsDriverModules_VK1
    Android
    Linux
    WinDesktop )


# EXEOS_CCV_WORKSPACE_BUILD_ENV_TARGET_GROUP_GID_LIST
#	
set( EXEOS_CCV_WORKSPACE_GROUP_DEPENDENCY_WHITELIST_GIDExsComponents
	GIDCorelibs
	GIDExsComponents )
	
set( EXEOS_CCV_WORKSPACE_GROUP_DEPENDENCY_WHITELIST_GIDExsGraphicsDriverCommon
	GIDCorelibs
	GIDExsComponents )
	
set( EXEOS_CCV_WORKSPACE_GROUP_DEPENDENCY_WHITELIST_GIDExsGraphicsDriverModules
	GIDCorelibs
	GIDExsComponents
	GIDExsGraphicsDriverCommon )
	

# EXEOS_CCV_WORKSPACE_GROUP_PROJECT_ID_PREFIX
# Group-specific prefix, prepended to a target id, which form the REFID of a target.
set( EXEOS_CCV_WORKSPACE_GROUP_PROJECT_ID_PREFIX_GIDCorelibs "Corelib_" )

set( EXEOS_CCV_WORKSPACE_GROUP_PROJECT_ID_PREFIX_GIDExsComponents "ExsComponent_" )

set( EXEOS_CCV_WORKSPACE_GROUP_PROJECT_ID_PREFIX_GIDExsGraphicsDriverCommon "ExsDrvGraphicsCommon_" )

set( EXEOS_CCV_WORKSPACE_GROUP_PROJECT_ID_PREFIX_GIDExsGraphicsDriverModules "ExsDrvGraphicsModule_" )

set( EXEOS_CCV_WORKSPACE_GROUP_PROJECT_ID_PREFIX_GIDWorkshop "Workshop_" )


#
#
set( EXEOS_CCV_WORKSPACE_GROUP_ROOT_DIR_GIDCorelibs "Source/Corelibs" )

set( EXEOS_CCV_WORKSPACE_GROUP_ROOT_DIR_GIDExsComponents "Source/Exs/Components" )

set( EXEOS_CCV_WORKSPACE_GROUP_ROOT_DIR_GIDExsGraphicsDriverCommon "Source/Exs/Drivers/Graphics/Common" )

set( EXEOS_CCV_WORKSPACE_GROUP_ROOT_DIR_GIDExsGraphicsDriverModules "Source/Exs/Drivers/Graphics" )

set( EXEOS_CCV_WORKSPACE_GROUP_ROOT_DIR_GIDWorkshop "Source/Workshop" )


#
#
set( EXEOS_CCV_WORKSPACE_GROUP_PROJECT_SUBDIR_GIDCorelibs "/@{ProjectName}" )

set( EXEOS_CCV_WORKSPACE_GROUP_PROJECT_SUBDIR_GIDExsComponents "/Exs/@{ProjectName}" )

set( EXEOS_CCV_WORKSPACE_GROUP_PROJECT_SUBDIR_GIDExsGraphicsDriverCommon "/@{ProjectName}" )

set( EXEOS_CCV_WORKSPACE_GROUP_PROJECT_SUBDIR_GIDExsGraphicsDriverModules "/@{ProjectName}" )

set( EXEOS_CCV_WORKSPACE_GROUP_PROJECT_SUBDIR_GIDWorkshop "/@{ProjectName}" )



# EXEOS_CCV_WORKSPACE_TARGET_OUTPUT_FILENAME_PREFIX
# String prepended to a name of the output file of targets from specific group.
set( EXEOS_CCV_WORKSPACE_TARGET_OUTPUT_FILENAME_PREFIX_GIDCorelibs "")
	
set( EXEOS_CCV_WORKSPACE_TARGET_OUTPUT_FILENAME_PREFIX_GIDExsComponents "Exs.")
	
set( EXEOS_CCV_WORKSPACE_TARGET_OUTPUT_FILENAME_PREFIX_GIDExsGraphicsDriverCommon "ExsDriverCommonLayer.Graphics.")
	
set( EXEOS_CCV_WORKSPACE_TARGET_OUTPUT_FILENAME_PREFIX_GIDExsGraphicsDriverModules "ExsDriver.Graphics.")
	
set( EXEOS_CCV_WORKSPACE_TARGET_OUTPUT_FILENAME_PREFIX_GIDWorkshop "")
