
include_guard()


if( NOT EBS_PATH )
    message( FATAL_ERROR "EBS_PATH has not been defined."  )
endif()

if( NOT EXISTS "${EBS_PATH}" )
    message( FATAL_ERROR "EBS_PATH is not a valid path or it does not exist."  )
endif()


set( _EBS_CONTROL_DEF_ CACHE INTERNAL "_EBS_CONTROL_DEF_" FORCE )


macro( ebsCoreAddModuleRef moduleName )
    include( "${EBS_PATH}/_EBS.${moduleName}.cmake" )
endmacro()


ebsCoreAddModuleRef( "Core" )
ebsCoreAddModuleRef( "VMS" )
ebsCoreAddModuleRef( "VPR" )
