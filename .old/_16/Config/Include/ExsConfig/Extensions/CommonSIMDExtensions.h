
#pragma once

#ifndef __Exs_Config_Extensions_CommonSIMDExtensions_H__
#define __Exs_Config_Extensions_CommonSIMDExtensions_H__


namespace Exs
{


#if ( EXS_SUPPORTED_EIS & EXS_EIS_SSE )


	// Computes dot product of two 3-component vectors of single-precision floating point numbers.
	// It is stored in three lowest memory slots ([95:0]) of the destination.
	inline __m128 _mm_dp_ps3(const __m128& m1, const __m128& m2)
	{
	#if ( EXS_SUPPORTED_EIS & EXS_EIS_SSE41 )

		return _mm_dp_ps(m1, m2, 0x77);

	#else

		__m128 mul_res = _mm_mul_ps(m1, m2);
		(reinterpret_cast<float*>(&mul_res))[3] = 0;
		
		const __m128 tmp_xyxy = _mm_shuffle_ps(mul_res, mul_res, 0x44);
		const __m128 tmp_zwwz = _mm_shuffle_ps(mul_res, mul_res, 0xBE);

		const __m128 tmp_sum = _mm_add_ps(tmp_xyxy, tmp_zwwz);

		return _mm_add_ps(tmp_sum, _mm_shuffle_ps(tmp_sum, tmp_sum, 0xB1));	

	#endif
	}

	
	// Computes dot product of two 3-component vectors of single-precision floating point numbers.
	// It is stored only in the lowest memory slot ([31:0]) of the destination.
	inline __m128 _mm_dp_ps3_s0(const __m128& m1, const __m128& m2)
	{
	#if ( EXS_SUPPORTED_EIS & EXS_EIS_SSE41 )

		return _mm_dp_ps(m1, m2, 0x71);

	#else

		__m128 mul_res = _mm_mul_ps(m1, m2);
		(reinterpret_cast<float*>(&mul_res))[3] = 0;
		
		const __m128 tmp_zwzw = _mm_movehl_ps(mul_res, mul_res);
		const __m128 tmp_sum = _mm_add_ps(mul_res, tmp_zwzw);

		return _mm_add_ps(tmp_sum, _mm_shuffle_ps(tmp_sum, tmp_sum, 0x01));

	#endif
	}

	
	// Computes dot product of two 4-component vectors of single-precision floating point numbers.
	// It is stored in all memory slots ([127:0]) of the destination.
	inline __m128 _mm_dp_ps4(const __m128& m1, const __m128& m2)
	{
	#if ( EXS_SUPPORTED_EIS & EXS_EIS_SSE41 )

		return _mm_dp_ps(m1, m2, 0xFF);

	#else

		const __m128 mul_res = _mm_mul_ps(m1, m2); // [ x1*x2, y1*y2, z1*z2, w1*w2 ]
		
		const __m128 tmp_xyxy = _mm_shuffle_ps(mul_res, mul_res, 0x44); // [ x1*x2, y1*y2, x1*x2, y1*y2 ]
		const __m128 tmp_zwwz = _mm_shuffle_ps(mul_res, mul_res, 0xBE); // [ z1*z2, w1*w2, w1*w2, z1*z2 ]
		
		const __m128 tmp_sum = _mm_add_ps(tmp_xyxy, tmp_zwwz);

		// 'tmp_sum' is now: [ x1*x2 + z1*z2,  y1*y2 + w1*w2,  x1*x2 + w1*w2,  y1*y2 + z1*z2 ]
		// So we need:       [ y1*y2 + w1*w2,  x1*x2 + z1*z2,  y1*y2 + z1*z2,  x1*x2 + w1*w2 ] (that's what shuffle below will produce)

		return _mm_add_ps(tmp_sum, _mm_shuffle_ps(tmp_sum, tmp_sum, 0xB1)); // [ y1*y2 + w1*w2, x1*x2 + z1*z2, y1*y2 + z1*z2, x1*x2 + w1*w2 ] (shuffled)

	#endif
	}

	
	// Computes dot product of two 4-component vectors of single-precision floating point numbers.
	// It is stored only in the lowest memory slot ([31:0]) of the destination.
	inline __m128 _mm_dp_ps4_s0(const __m128& m1, const __m128& m2)
	{
	#if ( EXS_SUPPORTED_EIS & EXS_EIS_SSE41 )

		return _mm_dp_ps(m1, m2, 0xF1);

	#else

		const __m128 mul_res = _mm_mul_ps(m1, m2); // [ x1*x2, y1*y2, z1*z2, w1*w2 ]
		
		const __m128 tmp_zwzw = _mm_movehl_ps(mul_res, mul_res); // [ z1*z2, w1*w2, z1*z2, w1*w2 ]
		const __m128 tmp_sum = _mm_add_ps(mul_res, tmp_zwzw);    // [ x1*x2 + z1*z2, y1*y2 + w1*w2, ..., ...]

		// Only one component should be set. Shuffle and add 'x1*x2 + z1*z2' to 'y1*y2 + w1*w2' (final result):

		return _mm_add_ps(tmp_sum, _mm_shuffle_ps(tmp_sum, tmp_sum, 0x01));

	#endif
	}


#endif


#if ( EXS_SUPPORTED_EIS & EXS_EIS_AVX )


	// Computes dot product of two 3-component vectors of double-precision floating point
	// numbers. It is stored in all memory slots ([255:0]) of the destination.
	inline __m256d _mm256_dp_pd3(const __m256d& m1, const __m256d& m2)
	{
		__m256d mul_res = _mm256_mul_pd(m1, m2); // [ x1*x2, y1*y2, z1*z2, w1*w2 ]

		(reinterpret_cast<double*>(&mul_res))[3] = 0;
		
		const __m128d tmp_xy = _mm256_extractf128_pd(mul_res, 0); // [ x1*x2, y1*y2 ]
		const __m128d tmp_zw = _mm256_extractf128_pd(mul_res, 1); // [ z1*z2, 0 ]

		const __m128d tmp_psum = _mm_add_pd(tmp_xy, tmp_zw); // [ x1*x2 + z1*z2, y1*y2 ]
		const __m128d tmp_tsum = _mm_add_pd(tmp_psum, _mm_shuffle_pd(tmp_psum, tmp_psum, 0x01));
		
		return _mm256_broadcastsd_pd(tmp_tsum);
	}

	
	// Computes dot product of two 3-component vectors of double-precision floating point
	// numbers. It is stored in all memory slots ([255:0]) of the destination.
	inline __m256d _mm256_dp_pd3_s0(const __m256d& m1, const __m256d& m2)
	{
		return _mm256_dp_pd3(m1, m2);
	}

	
	// Computes dot product of two 4-component vectors of double-precision floating point
	// numbers. It is stored in all memory slots ([255:0]) of the destination.
	inline __m256d _mm256_dp_pd4(const __m256d& m1, const __m256d& m2)
	{
		const __m256d mul_res = _mm256_mul_pd(m1, m2); // [ x1*x2, y1*y2, z1*z2, w1*w2 ]
		
		const __m128d tmp_xy = _mm256_extractf128_pd(mul_res, 0); // [ x1*x2, y1*y2 ]
		const __m128d tmp_zw = _mm256_extractf128_pd(mul_res, 1); // [ z1*z2, w1*w2 ]

		const __m128d tmp_psum = _mm_add_pd(tmp_xy, tmp_zw); // [ x1*x2 + z1*z2, y1*y2 + w1*w2 ]
		const __m128d tmp_tsum = _mm_add_pd(tmp_psum, _mm_shuffle_pd(tmp_psum, tmp_psum, 0x01));
		
		return _mm256_broadcastsd_pd(tmp_tsum);
	}

	
	// Computes dot product of two 4-component vectors of double-precision floating point
	// numbers. It is stored in all memory slots ([255:0]) of the destination.
	inline __m256d _mm256_dp_pd4_s0(const __m256d& m1, const __m256d& m2)
	{
		return _mm256_dp_pd4(m1, m2);
	}


#endif


}


#endif /* __Exs_Config_Extensions_CommonSIMDExtensions_H__ */
