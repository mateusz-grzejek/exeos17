
#pragma once

#ifndef __Exs_Config_Specific_H__
#define __Exs_Config_Specific_H__


#if ( EXS_COMPILER & EXS_COMPILER_INTEL )
#
#  include "Specific/Compiler/Intel/Common.h"
#
#elif ( EXS_COMPILER & EXS_COMPILER_MSVC )
#
#  include "Specific/Compiler/MSVC/Common.h"
#  include "Specific/Compiler/MSVC/Crtdefs.h"
#  include "Specific/Compiler/MSVC/AVXx32.h"
#  include "Specific/Compiler/MSVC/SSE2x32.h"
#
#elif ( EXS_COMPILER & EXS_COMPILER_MINGW )
#
#  include "Specific/Compiler/MinGW/Common.h"
#
#elif ( EXS_COMPILER & EXS_COMPILER_GCC )
#
#  include "Specific/Compiler/GCC/Common.h"
#  include "Specific/Compiler/GCC/Crtdefs.h"
#  include "Specific/Compiler/GCC/Cxx1y.h"
#
#elif ( EXS_COMPILER & EXS_COMPILER_CLANG )
#
#  include "Specific/Compiler/Clang/Common.h"
#  include "Specific/Compiler/Clang/Crtdefs.h"
#
#elif ( EXS_COMPILER & EXS_COMPILER_LLVM )
#
#  include "Specific/Compiler/Clang/Common.h"
#  include "Specific/Compiler/Clang/Crtdefs.h"
#
#endif


#if ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_WIN32 )
#
#  include "Specific/System/Win32/Types.h"
#  include "Specific/System/Win32/Filesystem.h"
#
#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_WP81 )
#
#  include "Specific/System/WinPhone81/Types.h"
#  include "Specific/System/WinPhone81/Filesystem.h"
#
#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_LINUX )
#
#  include "Specific/System/Linux/Types.h"
#  include "Specific/System/Linux/Filesystem.h"
#
#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_ANDROID )
#
#  include "Specific/System/Android/Types.h"
#
#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_OSX )
#
#  include "Specific/System/MacOSX/Types.h"
#  include "Specific/System/MacOSX/Filesystem.h"
#
#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_IOS )
#
#  include "Specific/System/iOS/Types.h"
#
#endif


#endif /* __Exs_Config_Specific_H__ */
