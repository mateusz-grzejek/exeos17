
#pragma once

#ifndef __Exs_Config_Common_Compiler_MinGW_H__
#define __Exs_Config_Common_Compiler_MinGW_H__


#define EXS_COMPILER_STR "MinGW GNU C++"


#include <signal.h>
#include <unistd.h>


#define EXS_FILE  __FILE__
#define EXS_FUNC  __FUNCTION__
#define EXS_LINE  __LINE__


#define EXS_ATTR_ALIGN(n)        __declspec(align(n))
#define EXS_ATTR_DEPRECATED      __declspec(deprecated)
#define EXS_ATTR_NO_RETURN       __declspec(noreturn)
#define EXS_ATTR_THREAD_LOCAL    __thread


#define EXS_ATTR_DLL_EXPORT      __declspec(dllexport)
#define EXS_ATTR_DLL_IMPORT      __declspec(dllimport)


#if ( EXS_CONFIG_BASE_FORCE_INLINE )
#  define ExsForceInline __attribute__((alwaysinline))
#else
#  define ExsForceInline __inline
#endif


#define gnoexcept noexcept
#define w64


#define EXS_DECLARE_NONCOPYABLE(Type) \
	private: \
		Type(const Type&) = delete; \
		Type& operator=(const Type&) = delete;


#pragma GCC diagnostic ignored "-Wunknown-pragmas"


#endif /* __Exs_Config_Common_Compiler_MinGW_H__ */
