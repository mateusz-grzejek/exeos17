
#pragma once

#ifndef __Exs_Config_Common_Compiler_Clang_H__
#define __Exs_Config_Common_Compiler_Clang_H__


#define EXS_COMPILER_STR "Clang"


#include <signal.h>
#include <unistd.h>


#define EXS_FILE  __FILE__
#define EXS_FUNC  __FUNCTION__
#define EXS_LINE  __LINE__


#define EXS_ATTR_ALIGN(n)                 alignas(n)
#define EXS_ATTR_DEPRECATED(msg, repl)    __attribute__((deprecated(msg,repl)))
#define EXS_ATTR_NO_RETURN                __attribute__((noreturn))
#define EXS_ATTR_THREAD_LOCAL             __thread

#if defined(__has_declspec_attribute)
#  if __has_declspec_attribute(dllexport) && __has_declspec_attribute(dllimport)
#    define EXS_ATTR_DLL_EXPORT __declspec(dllexport)
#    define EXS_ATTR_DLL_IMPORT __declspec(dllimport)
#  elif __has_declspec_attribute(__dllexport__) && __has_declspec_attribute(__dllimport__)
#    define EXS_ATTR_DLL_EXPORT __declspec(__dllexport__)
#    define EXS_ATTR_DLL_IMPORT __declspec(__dllimport__)
#  else
#    define EXS_ATTR_DLL_EXPORT
#    define EXS_ATTR_DLL_IMPORT
#  endif
#else
#  define EXS_ATTR_DLL_EXPORT
#  define EXS_ATTR_DLL_IMPORT
#endif

#if ( EXS_CONFIG_BASE_FORCE_INLINE )
#  if ( defined(__has_attribute) && __has_attribute(always_inline) )
#    define ExsForceInline __attribute__((always_inline))
#  else
#    define ExsForceInline inline
#  endif
#else
#  define ExsForceInline inline
#endif


#define gnoexcept noexcept
#define w64


#define EXS_DECLARE_NONCOPYABLE(Type) \
	private: \
		Type(const Type&) = delete; \
		Type& operator=(const Type&) = delete;


#pragma clang diagnostic ignored "-W#pragma-messages"
#pragma clang diagnostic ignored "-Wdynamic-class-memaccess"
#pragma clang diagnostic ignored "-Wmissing-braces"
#pragma clang diagnostic ignored "-Wunused-private-field"
#pragma clang diagnostic ignored "-Wunused-value"


#endif /* __Exs_Config_Common_Compiler_Clang_H__ */
