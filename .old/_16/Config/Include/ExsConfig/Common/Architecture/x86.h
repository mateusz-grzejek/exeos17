
#pragma once

#ifndef __Exs_Config_Common_Architecture_X86_H__
#define __Exs_Config_Common_Architecture_X86_H__


#define EXS_TARGET_ARCHITECTURE_STR	"x86-32 / IA32"


#define EXS_CONFIG_BASE_FORCE_EXTENDED_INSTRUCTION_SET  1
#define EXS_CONFIG_BASE_FORCE_EIS_AVX2                  1


#if !defined( EXS_MEMORY_BASE_ALIGNMENT )
#  define EXS_MEMORY_BASE_ALIGNMENT 4
#endif


#endif /* __Exs_Config_Common_Architecture_X86_H__ */
