
#pragma once

#ifndef __Exs_Config_Common_System_Win32_H__
#define __Exs_Config_Common_System_Win32_H__


#define EXS_TARGET_SYSTEM_STR "Microsoft Windows"


#include <direct.h>
#include <io.h>
#include <Windows.h>


#undef ChangeDisplaySettings
#undef CreateFile
#undef DispatchMessage
#undef FindResource
#undef GetClassInfo
#undef GetFreeSpace
#undef GetMessage
#undef GetObject
#undef LoadImage
#undef MessageBox
#undef OpenFile
#undef PeekMessage
#undef PostMessage
#undef RegisterClass
#undef RegisterClassEx
#undef SendMessage
#undef UnregisterClass
#undef Yield


#define EXS_UNICODE_CHARACTER     EXS_UNICODE_CHARACTER_WIDE
#define EXS_UNICODE_ENCODING      EXS_ENCODING_UTF16
#define EXS_WCHAR_STR_ENCODING    EXS_ENCODING_UTF16


#define EXS_ENV_DEFAULT_PATH_DELIMITER        '\\'
#define EXS_ENV_DEFAULT_PATH_DELIMITER_STR    "\\"
#define EXS_ENV_DYNAMIC_LIBRARY_PREFIX        ""
#define EXS_ENV_DYNAMIC_LIBRARY_EXTENSION     ".dll"


#define ExsCopyMemory(destPtr, srcPtr, size)    CopyMemory((destPtr), (srcPtr), (size))
#define ExsFillMemory(memPtr, size, value)      FillMemory((memPtr), (size), (value))
#define ExsMoveMemory(destPtr, srcPtr, size)    MoveMemory((destPtr), (srcPtr), (size))
#define ExsZeroMemory(memPtr, size)             ZeroMemory((memPtr), (size))


#endif /* __Exs_Config_Common_System_Win32_H__ */
