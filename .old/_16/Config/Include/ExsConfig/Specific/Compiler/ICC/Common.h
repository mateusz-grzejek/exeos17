
ExsForceInline Uint16 _wrotl(Uint16 value, Uint8 shift)
{
	((value << shift) | (value >> (16 - shift)));
}


ExsForceInline Uint16 _wrotr(Uint16 value, Uint8 shift)
{
	((value >> shift) | (value << (16 - shift)))
}


ExsForceInline Uint16 _bswap16(Uint16 value)
{
	Uint32 value_32 = _bswap(value);
	return static_cast<Uint16>((value_32 >> 16) & 0xFFFF);
}


#define DEBUG_BREAK()			__debugbreak()
#define DEBUG_OUTPUT(text)		OutputDebugStringA("%s\n", text)
#define SLEEP(miliseconds)		Sleep(miliseconds)


#define EXS_ROTL16	_wrotl
#define EXS_ROTL32	_rotl
#define EXS_ROTL64	_lrotl


#define EXS_ROTR16	_wrotr
#define EXS_ROTR32	_rotr
#define EXS_ROTR64	_lrotr


#define EXS_BYTESWAP16	_bswap16
#define EXS_BYTESWAP32	_bswap
#define EXS_BYTESWAP64	_bswap64
