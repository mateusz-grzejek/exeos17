

#define DEBUG_BREAK()			__asm("int $0x3");
#define DEBUG_OUTPUT(text)		printf("%s\n", text)
#define SLEEP(miliseconds)		Sleep(miliseconds)


#define EXS_ROTL16(value, shift)	((value << shift) | (value >> (16 - shift)))
#define EXS_ROTL32(value, shift)	((value << shift) | (value >> (32 - shift)))
#define EXS_ROTL64(value, shift)	((value << shift) | (value >> (64 - shift)))


#define EXS_ROTR16(value, shift)	((value >> shift) | (value << (16 - shift)))
#define EXS_ROTR32(value, shift)	((value >> shift) | (value << (32 - shift)))
#define EXS_ROTR64(value, shift)	((value >> shift) | (value << (64 - shift)))


#define EXS_BYTESWAP16	__builtin_bswap16
#define EXS_BYTESWAP32	__builtin_bswap32
#define EXS_BYTESWAP64	__builtin_bswap64
