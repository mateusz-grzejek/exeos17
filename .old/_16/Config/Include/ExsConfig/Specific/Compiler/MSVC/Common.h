
#define DEBUG_BREAK()         __debugbreak()
#define DEBUG_OUTPUT(text)    OutputDebugStringA(text)
#define SLEEP(miliseconds)    Sleep(miliseconds)


#define EXS_ROTL16 _rotl16
#define EXS_ROTL32 _rotl
#define EXS_ROTL64 _rotl64


#define EXS_ROTR16 _rotr16
#define EXS_ROTR32 _rotr
#define EXS_ROTR64 _rotr64


#define EXS_BYTESWAP16	_byteswap_ushort
#define EXS_BYTESWAP32	_byteswap_ulong
#define EXS_BYTESWAP64	_byteswap_uint64


#if ( EXS_CONFIG_BASE_ENABLE_INTRINSICS )
#
#  pragma intrinsic(_rotl16, _rotl, _rotl64)
#  pragma intrinsic(_rotr16, _rotr, _rotr64)
#  pragma intrinsic(_byteswap_ushort, _byteswap_ulong, _byteswap_uint64)
#
#endif
