
#if !( EXS_CONFIG_BASE_USE_ISOCPP11_PRINT_FORMAT_SPECIFIERS )
#
#  define EXS_STATE_PRINT_FORMAT_SPECIFIERS_DEFINED 1
#
#  define EXS_PFI32      "I32d"
#  define EXS_PFI32_HEX  "I32X"
#  define EXS_PFI32_OCT  "I32o"
#
#  define EXS_PFI64      "I64d"
#  define EXS_PFI64_HEX  "I64X"
#  define EXS_PFI64_OCT  "I64o"
#
#  define EXS_PFU32      "I32u"
#  define EXS_PFU32_HEX  "I32X"
#  define EXS_PFU32_OCT  "I32o"
#
#  define EXS_PFU64      "I64u"
#  define EXS_PFU64_HEX  "I64X"
#  define EXS_PFU64_OCT  "I64o"
#
#endif


#define fseek64      fseeko64
#define ftell64      ftello64
#define snwprintf    swprintf
