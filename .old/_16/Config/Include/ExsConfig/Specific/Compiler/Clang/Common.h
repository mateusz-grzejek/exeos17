
#define DEBUG_BREAK()         raise(SIGINT)
#define DEBUG_OUTPUT(text)    printf("%s\n", text)
#define SLEEP(miliseconds)    usleep(miliseconds * 1000)


#define EXS_ROTL16(x, n) ((x << n) | (x >> (16-n)))
#define EXS_ROTL32(x, n) ((x << n) | (x >> (32-n)))
#define EXS_ROTL64(x, n) ((x << n) | (x >> (64-n)))


#define EXS_ROTR16(x, n) ((x >> n) | (x << (16-n)))
#define EXS_ROTR32(x, n) ((x >> n) | (x << (32-n)))
#define EXS_ROTR64(x, n) ((x >> n) | (x << (64-n)))


#define EXS_BYTESWAP16(n) (((n & 0xFF00) >> 8) | ((n & 0x00FF) << 8))
#define EXS_BYTESWAP32(n) (((n & 0xFF000000) >> 24) | ((n & 0x00FF0000) >> 8) | ((n & 0x0000FF00) << 8) | ((n & 0x000000FF) << 24))
#define EXS_BYTESWAP64(n) ((long long)((EXS_BYTESWAP32(n) & 0xFFFFFFFF00000000) >> 32) | ((long long)(EXS_BYTESWAP32(n) & 0xFFFFFFFF) << 32))
