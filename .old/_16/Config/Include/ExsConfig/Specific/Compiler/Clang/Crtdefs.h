
#if !( EXS_CONFIG_BASE_USE_ISOCPP11_PRINT_FORMAT_SPECIFIERS )
#
#  define EXS_STATE_PRINT_FORMAT_SPECIFIERS_DEFINED 1
#
#  define EXS_PFINT32      "I32d"
#  define EXS_PFINT32_HEX  "I32X"
#  define EXS_PFINT32_OCT  "I32o"
#
#  define EXS_PFINT64      "I64d"
#  define EXS_PFINT64_HEX  "I64X"
#  define EXS_PFINT64_OCT  "I64o"
#
#  define EXS_PFUINT32      "I32u"
#  define EXS_PFUINT32_HEX  "I32X"
#  define EXS_PFUINT32_OCT  "I32o"
#
#  define EXS_PFUINT64      "I64u"
#  define EXS_PFUINT64_HEX  "I64X"
#  define EXS_PFUINT64_OCT  "I64o"
#
#endif


#define fseek64      fseeko64
#define ftell64      ftello64
#define snwprintf    swprintf


// inline FILE* fopen(const wchar_t* filename, const wchar_t* mode)
// {
// 	return _wfopen(filename, mode);
// }
//
//
// inline float strtof(const char* str, char** endPtr)
// {
// 	double dbl_value = strtod(str, endPtr);
// 	return static_cast<float>(dbl_value);
// }


inline float wcstof(const wchar_t* str, wchar_t** endPtr)
{
	double dbl_value = wcstod(str, endPtr);
	return static_cast<float>(dbl_value);
}


// inline Int64 strtoll(const char* str, char** endPtr, int radix)
// {
// 	return _strtoi64(str, endPtr, radix);
// }
//
//
// inline Uint64 strtoull(const char* str, char** endPtr, int radix)
// {
// 	return _strtoui64(str, endPtr, radix);
// }
//
//
//inline Int64 wcstoll(const wchar_t* str, wchar_t** endPtr, int radix)
//{
//	return 0;// _wcstoi64(str, endPtr, radix);
//}


//inline Uint64 wcstoull(const wchar_t* str, wchar_t** endPtr, int radix)
//{
//	return 0;// _wcstoui64(str, endPtr, radix);
//}
