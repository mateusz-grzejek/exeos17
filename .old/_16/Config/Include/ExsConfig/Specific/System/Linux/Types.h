
#define ExsNativeDLLHandleNULL 0
#define ExsNativeWindowHandleNULL 0

namespace Exs
{


	typedef void* NativeDLLHandle;
	typedef Window NativeWindowHandle;


	enum : XID
	{
		X11_Null_Drawable = X11C_None,
		X11_Null_Window = X11C_None
	};

	enum : int
	{
		X11_Invalid_Screen = INT_MAX
	};


};
