
struct ANativeWindow;

namespace Exs
{

	typedef void* NativeDLLHandle;
	typedef ANativeWindow* NativeWindowHandle;

}
