
#pragma once

#ifndef __Exs_Config_Version_H__
#define __Exs_Config_Version_H__


#define EXS_VERSION_MAJOR    0
#define EXS_VERSION_MINOR    0
#define EXS_VERSION_RELEASE  0
#define EXS_VERSION_BUILD    0

#define EXS_VER_PRODUCTVERSION      0,0,0,0
#define EXS_VER_PRODUCTVERSION_STR  "0.0.0.0"

#define EXS_VER_COMPANY_STR      "Exeos Development Group"
#define EXS_VER_COPYRIGHT_STR    "Copyright (c) 2015-2016 Mateusz Grzejek"
#define EXS_VER_PRODUCTNAME_STR  "Exeos 3D Engine"


#endif /* __Exs_Config_Version_H__ */
