
#pragma once

#ifndef __Exs_Config_Detection_H__
#define __Exs_Config_Detection_H__


#define EXS_TARGET_ARCHITECTURE_X86        0x0001
#define EXS_TARGET_ARCHITECTURE_X64        0x0002
#define EXS_TARGET_ARCHITECTURE_ARM        0x0004
#define EXS_TARGET_ARCHITECTURE_AARCH64    0x0004


#define EXS_TARGET_PLATFORM_TYPE_DESKTOP    0x1000
#define EXS_TARGET_PLATFORM_TYPE_MOBILE     0x2000

#define EXS_TARGET_PLATFORM_FLAG_WINDOWS     0x0001
#define EXS_TARGET_PLATFORM_FLAG_WINPHONE    0x0002
#define EXS_TARGET_PLATFORM_FLAG_POSIX       0x0004
#define EXS_TARGET_PLATFORM_FLAG_APPLE       0x0008


#define EXS_TARGET_SYSTEM_WIN32    (0x00010000 | EXS_TARGET_PLATFORM_TYPE_DESKTOP | EXS_TARGET_PLATFORM_FLAG_WINDOWS)
#define EXS_TARGET_SYSTEM_WP80     (0x00020000 | EXS_TARGET_PLATFORM_TYPE_MOBILE  | EXS_TARGET_PLATFORM_FLAG_WINPHONE)
#define EXS_TARGET_SYSTEM_WP81     (0x00040000 | EXS_TARGET_PLATFORM_TYPE_MOBILE  | EXS_TARGET_PLATFORM_FLAG_WINPHONE)
#define EXS_TARGET_SYSTEM_LINUX    (0x00100000 | EXS_TARGET_PLATFORM_TYPE_DESKTOP | EXS_TARGET_PLATFORM_FLAG_POSIX)
#define EXS_TARGET_SYSTEM_ANDROID  (0x00200000 | EXS_TARGET_PLATFORM_TYPE_MOBILE  | EXS_TARGET_PLATFORM_FLAG_POSIX)
#define EXS_TARGET_SYSTEM_OSX      (0x01000000 | EXS_TARGET_PLATFORM_TYPE_DESKTOP | EXS_TARGET_PLATFORM_FLAG_POSIX | EXS_TARGET_PLATFORM_FLAG_APPLE)
#define EXS_TARGET_SYSTEM_IOS      (0x02000000 | EXS_TARGET_PLATFORM_TYPE_MOBILE  | EXS_TARGET_PLATFORM_FLAG_POSIX | EXS_TARGET_PLATFORM_FLAG_APPLE)


#define EXS_COMPILER_MSVC       0x00010000
#define EXS_COMPILER_MSVC_2012  0x0001000B
#define EXS_COMPILER_MSVC_2013  0x0001000C
#define EXS_COMPILER_MSVC_2015  0x0001000E
#define EXS_COMPILER_MSVC_2017  0x00010011

#define EXS_COMPILER_ICC       0x00020000
#define EXS_COMPILER_ICC_12    0x000200C0
#define EXS_COMPILER_ICC_12_1  0x000200C1
#define EXS_COMPILER_ICC_13    0x000200D0
#define EXS_COMPILER_ICC_14    0x000200E0
#define EXS_COMPILER_ICC_15    0x000200F0

#define EXS_COMPILER_GCC      0x00040000
#define EXS_COMPILER_GCC_4_6  0x00040046
#define EXS_COMPILER_GCC_4_7  0x00040047
#define EXS_COMPILER_GCC_4_8  0x00040048
#define EXS_COMPILER_GCC_4_9  0x00040049
#define EXS_COMPILER_GCC_5_X  0x0004005F
#define EXS_COMPILER_GCC_6_X  0x0004006F

#define EXS_COMPILER_MINGW      0x00080000
#define EXS_COMPILER_MINGW_4_6  0x00080046
#define EXS_COMPILER_MINGW_4_7  0x00080047
#define EXS_COMPILER_MINGW_4_8  0x00080048
#define EXS_COMPILER_MINGW_4_9  0x00080049
#define EXS_COMPILER_MINGW_5_0  0x00080050

#define EXS_COMPILER_CLANG      0x00100000
#define EXS_COMPILER_CLANG_4_0  0x00100040
#define EXS_COMPILER_CLANG_4_1  0x00100041
#define EXS_COMPILER_CLANG_4_2  0x00100042
#define EXS_COMPILER_CLANG_5_0  0x00100050
#define EXS_COMPILER_CLANG_5_1  0x00100051
#define EXS_COMPILER_CLANG_6_0  0x00100060

#define EXS_COMPILER_LLVM      0x00400000
#define EXS_COMPILER_LLVM_3_2  0x00400032
#define EXS_COMPILER_LLVM_3_3  0x00400033
#define EXS_COMPILER_LLVM_3_4  0x00400034
#define EXS_COMPILER_LLVM_3_5  0x00400035
#define EXS_COMPILER_LLVM_3_6  0x00400036
#define EXS_COMPILER_LLVM_3_7  0x00400037
#define EXS_COMPILER_LLVM_3_8  0x00400038


#define EXS_ENDIANNESS_BE       0x4001
#define EXS_ENDIANNESS_LE       0x4002
#define EXS_ENDIANNESS_UNKNOWN  0x4000


#define EXS_UNICODE_CHARACTER_BYTE  0x4101
#define EXS_UNICODE_CHARACTER_WIDE  0x4102


#define EXS_ENCODING_ASCII  0x4201
#define EXS_ENCODING_UTF16  0x4203
#define EXS_ENCODING_UTF32  0x4204
#define EXS_ENCODING_UTF8   0x4205


#define EXS_EIS_NONE   0x0000
#define EXS_EIS_SSE    0x0001
#define EXS_EIS_SSE2   0x0002
#define EXS_EIS_SSE3   0x0004
#define EXS_EIS_SSE3X  0x0008
#define EXS_EIS_SSE41  0x0010
#define EXS_EIS_SSE42  0x0020
#define EXS_EIS_AVX    0x0100
#define EXS_EIS_AVX2   0x0200
#define EXS_EIS_NEON   0x1000


#define EXS_BUILD_TYPE_DEBUG    0x0001
#define EXS_BUILD_TYPE_RELEASE  0x0002


#if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
#  define EXS_BUILD_TYPE      EXS_BUILD_TYPE_DEBUG
#  define EXS_BUILD_TYPE_STR  "Debug"
#else
#  define EXS_BUILD_TYPE      EXS_BUILD_TYPE_RELEASE
#  define EXS_BUILD_TYPE_STR  "Release"
#endif


#if defined( _WIN32 ) || defined( _WINDOWS )
#
#  if defined( _EXS_BUILD_TARGET_WP80 )
#    define EXS_TARGET_SYSTEM  EXS_TARGET_SYSTEM_WP80
#  elif defined( _EXS_BUILD_TARGET_WP81 )
#    define EXS_TARGET_SYSTEM  EXS_TARGET_SYSTEM_WP81
#  else
#    define EXS_TARGET_SYSTEM  EXS_TARGET_SYSTEM_WIN32
#  endif
#
#  if !defined( NOCOMM )
#    define NOCOMM 1
#  endif
#
#  if !defined( NOCRYPT )
#    define NOCRYPT 1
#  endif
#
#  if !defined( NOGDICAPMASKS )
#    define NOGDICAPMASKS 1
#  endif
#
#  if !defined( NOCTLMGR )
#    define NOCTLMGR 1
#  endif
#
#  if !defined( NOMCX )
#    define NOMCX 1
#  endif
#
#  if !defined( NOMEMMGR )
#    define NOMEMMGR 1
#  endif
#
#  if !defined( NOMINMAX )
#    define NOMINMAX 1
#  endif
#
#  if !defined( NOOPENFILE )
#    define NOOPENFILE 1
#  endif
#
#  if !defined( NOSCROLL )
#    define NOSCROLL 1
#  endif
#
#  if !defined( NOSOUND )
#    define NOSOUND 1
#  endif
#
#  if !defined( NOTEXTMETRIC )
#    define NOTEXTMETRIC 1
#  endif
#
#  if !defined( WIN32_LEAN_AND_MEAN )
#    define WIN32_LEAN_AND_MEAN 1
#  endif
#
#elif defined ( __ANDROID__ )
#
#  define EXS_TARGET_SYSTEM  EXS_TARGET_SYSTEM_ANDROID
#
#elif defined( linux ) || defined( __linux__ )
#
#  define EXS_TARGET_SYSTEM  EXS_TARGET_SYSTEM_LINUX
#
#elif defined( __APPLE__ ) || defined( __apple )
#
#  include <TargetConditionals.h>
#
#  if( TARGET_OS_IPHONE )
#    define EXS_TARGET_SYSTEM  EXS_TARGET_SYSTEM_IOS
#  elif defined( __MACH__ ) || TARGET_OS_MAC
#    define EXS_TARGET_SYSTEM  EXS_TARGET_SYSTEM_OSX
#  endif
#
#else
#
#  error "Unsupported system!"
#
#endif


#if defined( _M_IX86 ) || defined( _X86_ ) || defined( __i386__ ) || defined( __IA32__ )
#
#  define EXS_TARGET_ARCHITECTURE  EXS_TARGET_ARCHITECTURE_X86
#  define EXS_ENDIANNESS           EXS_ENDIANNESS_LE
#  define EXS_ENDIANNESS_SWAP      EXS_ENDIANNESS_BE
#  define EXS_TARGET_64            0
#
#elif defined( _M_AMD64 ) || defined( _M_X64 ) || defined( __x86_64__ )
#
#  define EXS_TARGET_ARCHITECTURE  EXS_TARGET_ARCHITECTURE_X64
#  define EXS_ENDIANNESS           EXS_ENDIANNESS_LE
#  define EXS_ENDIANNESS_SWAP      EXS_ENDIANNESS_BE
#  define EXS_TARGET_64            1
#
#elif defined( _M_ARM ) || defined( _M_ARMT ) || defined( _ARM ) || defined( __arm__ )
#
#  define EXS_TARGET_ARCHITECTURE  EXS_TARGET_ARCHITECTURE_ARM
#  define EXS_ENDIANNESS           EXS_ENDIANNESS_BE
#  define EXS_ENDIANNESS_SWAP      EXS_ENDIANNESS_LE
#  define EXS_TARGET_64            0
#
#else
#
#  error "Unsupported architecture!"
#
#endif


#if defined( __INTEL_COMPILER )
#
#  if ( __INTEL_COMPILER == 1200 )
#    define EXS_COMPILER EXS_COMPILER_ICC_12
#  elif ( __INTEL_COMPILER == 1210 )
#    define EXS_COMPILER EXS_COMPILER_ICC_12_1
#  elif ( __INTEL_COMPILER == 1300 )
#    define EXS_COMPILER EXS_COMPILER_ICC_13
#  elif ( __INTEL_COMPILER == 1400 )
#    define EXS_COMPILER EXS_COMPILER_ICC_14
#  elif ( __INTEL_COMPILER == 1500 )
#    define EXS_COMPILER EXS_COMPILER_ICC_15
#  elif ( __INTEL_COMPILER > 1200 )
#    define EXS_COMPILER EXS_COMPILER_ICC
#  endif
#
#elif defined( _MSC_VER )
#
#  if ( _MSC_VER == 1700 )
#    define EXS_COMPILER  EXS_COMPILER_MSVC_2012
#  elif ( _MSC_VER == 1800 )
#    define EXS_COMPILER  EXS_COMPILER_MSVC_2013
#  elif ( _MSC_VER == 1900 )
#    define EXS_COMPILER  EXS_COMPILER_MSVC_2015
#  elif ( _MSC_VER == 2000 )
#    define EXS_COMPILER  EXS_COMPILER_MSVC_2017
#  elif ( _MSC_VER > 1600 )
#    define EXS_COMPILER  EXS_COMPILER_MSVC
#  endif
#
#  define _SCL_SECURE_NO_WARNINGS    1
#  define _CRT_SECURE_NO_WARNINGS    1
#  define _ITERATOR_DEBUG_LEVEL      0
#
#elif defined( __CLANG__ ) || defined( __clang__ )
#
#  if ( EXS_TARGET_SYSTEM & EXS_TARGET_PLATFORM_FLAG_APPLE )
#    if ( __clang_major__ == 4 ) && ( __clang_minor__ == 0 )
#      define EXS_COMPILER EXS_COMPILER_CLANG_4_0
#    elif ( __clang_major__ == 4 ) && ( __clang_minor__ == 1 )
#      define EXS_COMPILER EXS_COMPILER_CLANG_4_1
#    elif ( __clang_major__ == 4 ) && ( __clang_minor__ == 2 )
#      define EXS_COMPILER EXS_COMPILER_CLANG_4_2
#    elif ( __clang_major__ == 5 ) && ( __clang_minor__ == 0 )
#      define EXS_COMPILER EXS_COMPILER_CLANG_5_0
#    elif ( __clang_major__ == 5 ) && ( __clang_minor__ == 1 )
#      define EXS_COMPILER EXS_COMPILER_CLANG_5_1
#    elif ( __clang_major__ == 6 ) ( __clang_minor__ == 0 )
#      define EXS_COMPILER EXS_COMPILER_CLANG_6_0
#    elif ( __clang_major__ > 4 )
#      define EXS_COMPILER EXS_COMPILER_CLANG
#    endif
#  else
#    if ( __clang_major__ == 3 )
#      if ( __clang_minor__ == 2 )
#        define EXS_COMPILER EXS_COMPILER_LLVM_3_2
#      elif ( __clang_minor__ == 3 )
#        define EXS_COMPILER EXS_COMPILER_LLVM_3_3
#      elif ( __clang_minor__ == 4 )
#        define EXS_COMPILER EXS_COMPILER_LLVM_3_4
#      elif ( __clang_minor__ == 5 )
#        define EXS_COMPILER EXS_COMPILER_LLVM_3_5
#      elif ( __clang_minor__ == 6 )
#        define EXS_COMPILER EXS_COMPILER_LLVM_3_6
#      elif ( __clang_minor__ == 7 )
#        define EXS_COMPILER EXS_COMPILER_LLVM_3_6
#      elif ( __clang_minor__ >= 8 )
#        define EXS_COMPILER EXS_COMPILER_LLVM_3_8
#      endif
#    elif ( __clang_major__ > 3 )
#      define EXS_COMPILER EXS_COMPILER_LLVM
#    endif
#  endif
#
#elif defined( __GNUC__ )
#
#  include <cstdlib>
#
#  if defined( __MINGW32__ )
#    if ( __GNUC__ == 4 )
#      if ( __GNUC_MINOR__ == 6 )
#        define EXS_COMPILER EXS_COMPILER_MINGW_4_6
#      elif ( __GNUC_MINOR__ == 7 )
#        define EXS_COMPILER EXS_COMPILER_MINGW_4_7
#      elif ( __GNUC_MINOR__ == 8 )
#        define EXS_COMPILER EXS_COMPILER_MINGW_4_8
#      elif ( __GNUC_MINOR__ == 9 )
#        define EXS_COMPILER EXS_COMPILER_MINGW_4_9
#      elif ( __GNUC_MINOR__ > 6 )
#        define EXS_COMPILER EXS_COMPILER_MINGW
#      endif
#    elif ( __GNUC__ > 4 )
#      define EXS_COMPILER EXS_COMPILER_MINGW_5_0
#    endif
#  else
#    if ( __GNUC__ == 4 )
#      if ( __GNUC_MINOR__ == 6 )
#        define EXS_COMPILER EXS_COMPILER_GCC_4_6
#      elif ( __GNUC_MINOR__ == 7 )
#        define EXS_COMPILER EXS_COMPILER_GCC_4_7
#      elif ( __GNUC_MINOR__ == 8 )
#        define EXS_COMPILER EXS_COMPILER_GCC_4_8
#      elif ( __GNUC_MINOR__ == 9 )
#        define EXS_COMPILER EXS_COMPILER_GCC_4_9
#      elif ( __GNUC_MINOR__ > 6 )
#        define EXS_COMPILER EXS_COMPILER_GCC
#      endif
#    elif ( __GNUC__ == 5 )
#      define EXS_COMPILER EXS_COMPILER_GCC_5_X
#    elif ( __GNUC__ > 5 )
#      define EXS_COMPILER EXS_COMPILER_GCC_6_X
#    endif
#  endif
#
#else
#
#  error "Unsupported compiler!"
#
#endif


#if ( EXS_COMPILER & (EXS_COMPILER_MSVC | EXS_COMPILER_ICC) )
#
#  if defined( _MSC_EXTENSIONS )
#    define EXS_FEATURE_ANONYMOUS_UNIONS 1
#  else
#    define EXS_FEATURE_ANONYMOUS_UNIONS 0
#  endif
#
#else
#
#  define EXS_FEATURE_ANONYMOUS_UNIONS 0
#
#endif


#if ( EXS_CONFIG_BASE_ENABLE_EXTENDED_INSTRUCTION_SET )
#
#  if defined( EXS_CONFIG_BASE_FORCE_EXTENDED_INSTRUCTION_SET )
#
#    if defined( EXS_CONFIG_BASE_FORCE_EIS_NEON)
#      define EXS_SUPPORTED_EIS EXS_EIS_NEON
#    elif defined( EXS_CONFIG_BASE_FORCE_EIS_AVX2 )
#      define EXS_SUPPORTED_EIS (EXS_EIS_AVX2 | EXS_EIS_AVX | EXS_EIS_SSE42 | EXS_EIS_SSE41 | EXS_EIS_SSE3X | EXS_EIS_SSE3 | EXS_EIS_SSE2 | EXS_EIS_SSE)
#    elif defined( EXS_CONFIG_BASE_FORCE_EIS_AVX )
#      define EXS_SUPPORTED_EIS (EXS_EIS_AVX | EXS_EIS_SSE42 | EXS_EIS_SSE41 | EXS_EIS_SSE3X | EXS_EIS_SSE3 | EXS_EIS_SSE2 | EXS_EIS_SSE)
#    elif defined( EXS_CONFIG_BASE_FORCE_EIS_SSE4 )
#      define EXS_SUPPORTED_EIS (EXS_EIS_SSE42 | EXS_EIS_SSE41 | EXS_EIS_SSE3X | EXS_EIS_SSE3 | EXS_EIS_SSE2 | EXS_EIS_SSE)
#    elif defined( EXS_CONFIG_BASE_FORCE_EIS_SSE3 )
#      define EXS_SUPPORTED_EIS (EXS_EIS_SSE3 | EXS_EIS_SSE2 | EXS_EIS_SSE)
#    elif defined( EXS_CONFIG_BASE_FORCE_EIS_SSE2 )
#      define EXS_SUPPORTED_EIS (EXS_EIS_SSE2 | EXS_EIS_SSE)
#    elif defined( EXS_CONFIG_BASE_FORCE_EIS_SSE )
#      define EXS_SUPPORTED_EIS EXS_EIS_SSE
#    elif defined( EXS_CONFIG_BASE_FORCE_EIS_PURE )
#      define EXS_SUPPORTED_EIS 0
#    endif
#
#  else
#
#    if ( EXS_COMPILER & (EXS_COMPILER_CLANG | EXS_COMPILER_GCC | EXS_COMPILER_LLVM) ) || ( (EXS_COMPILER & EXS_COMPILER_ICC) && (EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_LINUX) )
#      if defined( __AVX2__ )
#        define EXS_SUPPORTED_EIS (EXS_EIS_AVX2 | EXS_EIS_AVX | EXS_EIS_SSE3X | EXS_EIS_SSE3 | EXS_EIS_SSE2 | EXS_EIS_SSE)
#      elif defined( __AVX__ )
#        define EXS_SUPPORTED_EIS (EXS_EIS_AVX | EXS_EIS_SSE3X | EXS_EIS_SSE3 | EXS_EIS_SSE2 | EXS_EIS_SSE)
#      elif defined( __SSE3__ )
#        define EXS_SUPPORTED_EIS (EXS_EIS_SSE3 | EXS_EIS_SSE2 | EXS_EIS_SSE)
#      elif defined( __SSE2__ )
#        define EXS_SUPPORTED_EIS (EXS_EIS_SSE2 | EXS_EIS_SSE)
#      else
#        define EXS_SUPPORTED_EIS EXS_EIS_NONE
#      endif
#    elif ( EXS_COMPILER & EXS_COMPILER_MSVC ) || ( (EXS_COMPILER & EXS_COMPILER_ICC) && (EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_WIN32) )
#      if defined( _M_ARM_FP )
#        define EXS_SUPPORTED_EIS EXS_EIS_NEON
#      elif defined( __AVX2__ )
#        define EXS_SUPPORTED_EIS (EXS_EIS_AVX2 | EXS_EIS_AVX | EXS_EIS_SSE3X | EXS_EIS_SSE3 | EXS_EIS_SSE3 | EXS_EIS_SSE2 | EXS_EIS_SSE)
#      elif defined( __AVX__ )
#        define EXS_SUPPORTED_EIS (EXS_EIS_AVX | EXS_EIS_SSE3X | EXS_EIS_SSE3 | EXS_EIS_SSE3 | EXS_EIS_SSE2 | EXS_EIS_SSE)
#      elif ( _M_IX86_FP == 2 )
#        define EXS_SUPPORTED_EIS (EXS_EIS_SSE2 | EXS_EIS_SSE)
#      elif ( _M_IX86_FP == 1 )
#        define EXS_SUPPORTED_EIS EXS_EIS_SSE
#      else
#        define EXS_SUPPORTED_EIS EXS_EIS_NONE
#      endif
#    elif ( EXS_COMPILER & EXS_COMPILER_GCC ) && ( EXS_TARGET_ARCHITECTURE & (EXS_TARGET_ARCHITECTURE_X86 | EXS_TARGET_ARCHITECTURE_AMD64) )
#      if defined( __AVX2__ )
#        define EXS_SUPPORTED_EIS (EXS_EIS_AVX2 | EXS_EIS_AVX | EXS_EIS_SSE42 | EXS_EIS_SSE41 | EXS_EIS_SSE3X | EXS_EIS_SSE3 | EXS_EIS_SSE2 | EXS_EIS_SSE)
#      elif defined( __AVX__ )
#        define EXS_SUPPORTED_EIS (EXS_EIS_AVX | EXS_EIS_SSE42 | EXS_EIS_SSE41 | EXS_EIS_SSE3X | EXS_EIS_SSE3 | EXS_EIS_SSE2 | EXS_EIS_SSE)
#      elif defined( __SSE4_1__ )
#        define EXS_SUPPORTED_EIS (EXS_EIS_SSE41 | EXS_EIS_SSE3X | EXS_EIS_SSE3 | EXS_EIS_SSE2 | EXS_EIS_SSE)
#      elif defined( __SSE3__ )
#        define EXS_SUPPORTED_EIS (EXS_EIS_SSE3 | EXS_EIS_SSE2 | EXS_EIS_SSE)
#      elif defined( __SSE2__ )
#        define EXS_SUPPORTED_EIS (EXS_EIS_SSE2 | EXS_EIS_SSE)
#      elif defined( __SSE__ )
#        define EXS_SUPPORTED_EIS EXS_EIS_SSE
#      else
#        define EXS_SUPPORTED_EIS EXS_EIS_NONE
#      endif
#    else
#      define EXS_SUPPORTED_EIS EXS_EIS_NONE
#    endif
#
#  endif
#
#endif


#endif /* __Exs_Config_Detection_H__ */
