
#pragma once

#ifndef __Exs_Config_Common_H__
#define __Exs_Config_Common_H__


#if ( EXS_TARGET_ARCHITECTURE == EXS_TARGET_ARCHITECTURE_X86 )
#
#  include "Common/Architecture/x86.h"
#
#elif ( EXS_TARGET_ARCHITECTURE == EXS_TARGET_ARCHITECTURE_X64 )
#
#  include "Common/Architecture/x64.h"
#
#elif ( EXS_TARGET_ARCHITECTURE == EXS_TARGET_ARCHITECTURE_ARM )
#
#  include "Common/Architecture/ARM.h"
#
#elif ( EXS_TARGET_ARCHITECTURE == EXS_TARGET_ARCHITECTURE_AARCH64 )
#
#  include "Common/Architecture/AArch64.h"
#
#endif


#if ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_WIN32 )
#
#  include "Common/System/Win32.h"
#
#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_WP81 )
#
#  include "Common/System/WinPhone81.h"
#
#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_LINUX )
#
#  include "Common/System/Linux.h"
#
#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_OSX )
#
#  include "Common/System/MacOSX.h"
#
#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_IOS )
#
#  include "Common/System/IOS.h"
#
#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_ANDROID )
#
#  include "Common/System/Android.h"
#
#endif


#if ( EXS_COMPILER & EXS_COMPILER_ICC )
#
#  include "Common/Compiler/ICC.h"
#
#elif ( EXS_COMPILER & EXS_COMPILER_MSVC )
#
#  include "Common/Compiler/MSVC.h"
#
#elif ( EXS_COMPILER & EXS_COMPILER_MINGW )
#
#  include "Common/Compiler/MinGW.h"
#
#elif ( EXS_COMPILER & EXS_COMPILER_GCC )
#
#  include "Common/Compiler/GCC.h"
#
#elif ( EXS_COMPILER & EXS_COMPILER_CLANG )
#
#  include "Common/Compiler/Clang.h"
#
#elif ( EXS_COMPILER & EXS_COMPILER_LLVM )
#
#  include "Common/Compiler/Clang.h"
#
#endif


#endif /* __Exs_Config_Common_H__ */
