
#pragma once

#ifndef __Exs_Config_Base_H__
#define __Exs_Config_Base_H__


#define EXS_CONFIG_BASE_ENABLE_ASYNCHRONOUS_TASK_EXECUTION  1
#define EXS_CONFIG_BASE_ENABLE_THREAD_ACCESS_RESTRICTION    1
#define EXS_CONFIG_BASE_ENABLE_TRACING                      1
#define EXS_CONFIG_BASE_ENABLE_EXTENDED_TRACE_INFO          1
#define EXS_CONFIG_BASE_USE_SPINLOCK_AS_LIGHT_MUTEX         1


// --- EXS_CONFIG_BASE_ENABLE_DEBUG ---
//
// Indicates debug build. Enables additional checks, extra functionality and many other things.

#if !defined( EXS_CONFIG_BASE_ENABLE_DEBUG )
#  if ( DEBUG || _DEBUG )
#    define EXS_CONFIG_BASE_ENABLE_DEBUG  1
#  else
#    define EXS_CONFIG_BASE_ENABLE_DEBUG  0
#  endif
#endif


// --- EXS_CONFIG_BASE_ENABLE_DEBUG_INFO ---
//
//

#if !defined( EXS_CONFIG_BASE_ENABLE_DEBUG_INFO )
#  if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
#    define EXS_CONFIG_BASE_ENABLE_DEBUG_INFO 1
#  else
#    define EXS_CONFIG_BASE_ENABLE_DEBUG_INFO 0
#  endif
#endif


// --- EXS_CONFIG_BASE_ENABLE_TRACING ---
//
// Enables trace outputs. Trace outputs are by default disabled in non-debug modes, but they can work in any
// build configuration. In debug build, however, traces are printed to debug output, while in non-debug builds
// they are forwarded to stdout.

#if !defined( EXS_CONFIG_BASE_ENABLE_TRACING )
#  if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
#    define EXS_CONFIG_BASE_ENABLE_TRACING 1
#  else
#    define EXS_CONFIG_BASE_ENABLE_TRACING 0
#  endif
#endif


// --- EXS_CONFIG_BASE_USE_ANSI_FILE_IO ---
//

#if !defined( EXS_CONFIG_BASE_USE_ANSI_FILE_IO )
#  define EXS_CONFIG_BASE_USE_ANSI_FILE_IO 1
#endif


// --- EXS_CONFIG_BASE_ENABLE_EXTENDED_INSTRUCTION_SET ---
//

#if !defined( EXS_CONFIG_BASE_ENABLE_EXTENDED_INSTRUCTION_SET )
#  define EXS_CONFIG_BASE_ENABLE_EXTENDED_INSTRUCTION_SET 1
#endif


// --- EXS_CONFIG_BASE_USE_ISOCPP11_EXTENDED_STORAGE_SPECIFIERS ---
//

#if !defined( EXS_CONFIG_BASE_USE_ISOCPP11_EXTENDED_STORAGE_SPECIFIERS )
#  define EXS_CONFIG_BASE_USE_ISOCPP11_EXTENDED_STORAGE_SPECIFIERS 1
#endif


// --- EXS_CONFIG_BASE_USE_ISOCPP11_PRINT_FORMAT_SPECIFIERS ---
//

#if !defined( EXS_CONFIG_BASE_USE_ISOCPP11_PRINT_FORMAT_SPECIFIERS )
#  define EXS_CONFIG_BASE_USE_ISOCPP11_PRINT_FORMAT_SPECIFIERS 1
#endif


// --- EXS_CONFIG_BASE_ENABLE_CHECKED_ITERATORS ---
//

#if !defined( EXS_CONFIG_BASE_ENABLE_CHECKED_ITERATORS )
#  if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
#    define EXS_CONFIG_BASE_ENABLE_CHECKED_ITERATORS 1
#  else
#    define EXS_CONFIG_BASE_ENABLE_CHECKED_ITERATORS 0
#  endif
#endif


// --- EXS_CONFIG_BASE_ENABLE_INTRINSICS ---
//
// Enables usage of compiler intrinsics whenever possible. Some compilers allow certain functions to be replaced
// with performance-optimized code (usually inserted inline to avoid call overhead). We use intrinsics in many
// places (memcmp, memcpy, strlen, strcpy, etc) and this option turns them globally on/off.

#if !defined( EXS_CONFIG_BASE_ENABLE_INTRINSICS )
#  define EXS_CONFIG_BASE_ENABLE_INTRINSICS 1
#endif


// --- EXS_CONFIG_BASE_FORCE_INLINE ---
//
// Defines how 'ExsInline' is expanded. If EXS_CONFIG_BASE_FORCE_INLINE is true, this macro is expanded to the
// compiler-specific keyword, that allows user to force function inlining (for example: __forceinline on MSVC
// and __attribute__((alwaysinline)) on GCC).
//
// NOTE: __inline is used on MSVC platform, because VC++ provides an option, that influences function inlining,
// depending on used keyword (inline and __inline are treated differently).

#if !defined( EXS_CONFIG_BASE_FORCE_INLINE )
#  define EXS_CONFIG_BASE_FORCE_INLINE 0
#endif


// --- EXS_CONFIG_BASE_USE_DOUBLE_PRECISION ---
//
// Specifies precision for default floating-point type. EXS_CONFIG_BASE_USE_DOUBLE_PRECISION means, that
// 'double' is used as typedef for internal 'Real' type. 'float' is used otherwise.

#if !defined( EXS_CONFIG_BASE_USE_DOUBLE_PRECISION )
#  define EXS_CONFIG_BASE_USE_DOUBLE_PRECISION 0
#endif


// --- EXS_CONFIG_BASE_USE_SPINLOCK_AS_LIGHT_MUTEX ---
//
// Defines, what type of mutex is used as lightweight mutex. In some cases, when it is known, that full mutex
// locking is too expensive for this particular task, light mutex can be used instead. Depending on this option,
// LightMutex will be either a SpinLock (true) or standard Mutex (false).

#if !defined( EXS_CONFIG_BASE_USE_SPINLOCK_AS_LIGHT_MUTEX )
#  define EXS_CONFIG_BASE_USE_SPINLOCK_AS_LIGHT_MUTEX 1
#endif


// --- EXS_CONFIG_BASE_OVERRIDE_MEMORY_ALLOCATION ---
//
// Defines how ExsMalloc, ExsRealloc and ExsFree are defined. They can be expanded to either internal memory
// routines, that usecustom allocation policy (true), or std:: routines (false).

#if !defined( EXS_CONFIG_BASE_OVERRIDE_MEMORY_ALLOCATION )
#  define EXS_CONFIG_BASE_OVERRIDE_MEMORY_ALLOCATION 1
#endif


// --- EXS_CONFIG_BASE_ENABLE_MEMORY_TRACKING ---
//
// Global switch to turn on/off memory tracking. Affects following options:
// > EXS_CONFIG_BASE_TRACK_MEMORY_ALLOCATIONS
// > EXS_CONFIG_BASE_TRACK_OPERATOR_NEW_ALLOCATIONS
// > EXS_CONFIG_BASE_TRACK_DIRECT_INTERNAL_MEMORY_ALLOCATIONS
// > EXS_CONFIG_BASE_TRACK_DIRECT_SYSTEM_MEMORY_ALLOCATIONS

#if !defined( EXS_CONFIG_BASE_ENABLE_MEMORY_TRACKING )
#  if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
#    define EXS_CONFIG_BASE_ENABLE_MEMORY_TRACKING 1
#  else
#    define EXS_CONFIG_BASE_ENABLE_MEMORY_TRACKING 0
#  endif
#endif

// --- EXS_CONFIG_BASE_ENABLE_ASYNCHRONOUS_TASK_EXECUTION ---
//

#if !defined( EXS_CONFIG_BASE_ENABLE_ASYNCHRONOUS_TASK_EXECUTION )
#  define EXS_CONFIG_BASE_ENABLE_ASYNCHRONOUS_TASK_EXECUTION 1
#endif


// --- EXS_CONFIG_BASE_TRACK_MEMORY_ALLOCATIONS ---
//
// Enables or disables tracking of allocations performed with ExsMalloc, ExsRealloc and ExsFree.

#if !defined( EXS_CONFIG_BASE_TRACK_MEMORY_ALLOCATIONS )
#
#  if ( EXS_CONFIG_BASE_ENABLE_DEBUG && EXS_CONFIG_BASE_ENABLE_MEMORY_TRACKING )
#    define EXS_CONFIG_BASE_TRACK_MEMORY_ALLOCATIONS 1
#  else
#    define EXS_CONFIG_BASE_TRACK_MEMORY_ALLOCATIONS 0
#  endif
#
#elif ( EXS_CONFIG_BASE_ENABLE_MEMORY_TRACKING )
#
#  undef EXS_CONFIG_BASE_TRACK_MEMORY_ALLOCATIONS
#  define EXS_CONFIG_BASE_TRACK_MEMORY_ALLOCATIONS 0
#
#endif


// --- EXS_CONFIG_BASE_TRACK_OPERATOR_NEW_ALLOCATIONS ---
//
// Enables or disables tracking of allocations performed by operator new/new[].

#if !defined( EXS_CONFIG_BASE_TRACK_OPERATOR_NEW_ALLOCATIONS )
#
#  if ( EXS_CONFIG_BASE_ENABLE_DEBUG && EXS_CONFIG_BASE_ENABLE_MEMORY_TRACKING )
#    define EXS_CONFIG_BASE_TRACK_OPERATOR_NEW_ALLOCATIONS 1
#  else
#    define EXS_CONFIG_BASE_TRACK_OPERATOR_NEW_ALLOCATIONS 0
#  endif
#
#elif ( EXS_CONFIG_BASE_ENABLE_MEMORY_TRACKING )
#
#  undef EXS_CONFIG_BASE_TRACK_OPERATOR_NEW_ALLOCATIONS
#  define EXS_CONFIG_BASE_TRACK_OPERATOR_NEW_ALLOCATIONS 0
#
#endif


// --- EXS_CONFIG_BASE_TRACK_DIRECT_INTERNAL_MEMORY_ALLOCATIONS ---
//
// Enables or disables tracking of allocations performed by direct calls to Exs* memory macros.

#if !defined( EXS_CONFIG_BASE_TRACK_DIRECT_INTERNAL_MEMORY_ALLOCATIONS )
#
#  if ( EXS_CONFIG_BASE_ENABLE_DEBUG && EXS_CONFIG_BASE_ENABLE_MEMORY_TRACKING )
#    define EXS_CONFIG_BASE_TRACK_DIRECT_INTERNAL_MEMORY_ALLOCATIONS 1
#  else
#    define EXS_CONFIG_BASE_TRACK_DIRECT_INTERNAL_MEMORY_ALLOCATIONS 0
#  endif
#
#elif ( EXS_CONFIG_BASE_ENABLE_MEMORY_TRACKING )
#
#  undef EXS_CONFIG_BASE_TRACK_DIRECT_INTERNAL_MEMORY_ALLOCATIONS
#  define EXS_CONFIG_BASE_TRACK_DIRECT_INTERNAL_MEMORY_ALLOCATIONS 0
#
#endif


// --- EXS_CONFIG_BASE_TRACK_DIRECT_SYSTEM_MEMORY_ALLOCATIONS ---
//
// Enables or disables tracking of allocations performed by direct calls to Sys* memory macros.

#if !defined( EXS_CONFIG_BASE_TRACK_DIRECT_SYSTEM_MEMORY_ALLOCATIONS )
#
#  if ( EXS_CONFIG_BASE_ENABLE_DEBUG && EXS_CONFIG_BASE_ENABLE_MEMORY_TRACKING )
#    define EXS_CONFIG_BASE_TRACK_DIRECT_SYSTEM_MEMORY_ALLOCATIONS 1
#  else
#    define EXS_CONFIG_BASE_TRACK_DIRECT_SYSTEM_MEMORY_ALLOCATIONS 0
#  endif
#
#elif ( EXS_CONFIG_BASE_ENABLE_MEMORY_TRACKING )
#
#  undef EXS_CONFIG_BASE_TRACK_DIRECT_SYSTEM_MEMORY_ALLOCATIONS
#  define EXS_CONFIG_BASE_TRACK_DIRECT_SYSTEM_MEMORY_ALLOCATIONS 0
#
#endif


// --- EXS_CONFIG_BASE_ENABLE_THREAD_ACCESS_GUARDS ---
//

#if !defined( EXS_CONFIG_BASE_ENABLE_THREAD_ACCESS_RESTRICTION )
#  if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
#    define EXS_CONFIG_BASE_ENABLE_THREAD_ACCESS_RESTRICTION 1
#  else
#    define EXS_CONFIG_BASE_ENABLE_THREAD_ACCESS_RESTRICTION 0
#  endif
#endif


#endif /* __Exs_Config_Base_H__ */
