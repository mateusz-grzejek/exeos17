
#pragma once

#ifndef __STDX_BASE_CONFIG_H__
#define __STDX_BASE_CONFIG_H__

#include <cassert>
#include <cstdlib>
#include <cstddef>
#include <cstdint>

#include <algorithm>
#include <type_traits>


#if defined( _M_IX86 ) || defined( _X86_ ) || defined( __i386__ ) || defined( __IA32__ )
#  define STDX_TARGET_64 0
#elif defined( _M_AMD64 ) || defined( _M_X64 ) || defined( __x86_64__ )
#  define STDX_TARGET_64 1
#elif defined( _M_ARM ) || defined( _M_ARMT ) || defined( _ARM ) || defined( __arm__ )
#  define STDX_TARGET_64 0
#else
#  error "Unsupported architecture!"
#endif


#endif /* __STDX_BASE_CONFIG_H__ */
