
#pragma once

#ifndef __STD_HASH_ARRAY_H__
#define __STD_HASH_ARRAY_H__

#include "assoc_array.h"


namespace stdx
{


	template <class _Key, class _Value, class _Hash>
	struct hash_array_traits
	{
		typedef _Key key_type;
		typedef _Value value_type;
		typedef _Hash hash_type;
		typedef typename hash_type::result_type hash_result_type;
		typedef key_value_pair<hash_result_type, _Value> element_type;
	};


	template < class _Key,
	           class _Value,
	           class _Hash = std::hash<_Key>,
	           class _Alloc = std::allocator<typename hash_array_traits<_Key, _Value, _Hash>::element_type> >
	class hash_array
	{
	public:
		typedef hash_array<_Key, _Value, _Hash, _Alloc> my_type;
		typedef hash_array_traits<_Key, _Value, _Hash> traits_type;

		typedef typename traits_type::key_type key_type;
		typedef typename traits_type::value_type value_type;
		typedef typename traits_type::element_type element_type;
		typedef typename traits_type::hash_type hash_type;
		typedef typename traits_type::hash_result_type hash_result_type;

		typedef assoc_array<hash_result_type, value_type, _Alloc> underlying_container_type;
		
		typedef typename underlying_container_type::iterator iterator;
		typedef typename underlying_container_type::const_iterator const_iterator;
		typedef typename underlying_container_type::reverse_iterator reverse_iterator;
		typedef typename underlying_container_type::const_reverse_iterator const_reverse_iterator;

		typedef typename underlying_container_type::size_type size_type;
		typedef typename underlying_container_type::difference_type difference_type;
		typedef typename underlying_container_type::allocator_type allocator_type;

	private:
		hash_type                  _hasher;
		underlying_container_type  _underlyingContainer;

	public:
		hash_array(hash_array&&) = default;
		hash_array(const hash_array&) = default;
		hash_array& operator=(hash_array&&) = default;
		hash_array& operator=(const hash_array&) = default;

		hash_array()
		{ }

		explicit hash_array(const allocator_type& allocator)
		: _underlyingContainer(allocator)
		{ }

		explicit hash_array(size_t capacity, const allocator_type& allocator = allocator_type())
		: _underlyingContainer(capacity, allocator)
		{ }

		value_type& operator[](const key_type& key)
		{
			auto key_hash = this->_hasher(key);
			return this->_underlyingContainer[key_hash];
		}

		value_type& at(const key_type& key)
		{
			auto key_hash = this->_hasher(key);
			return this->_underlyingContainer.at(key_hash);
		}

		const value_type& at(const key_type& key) const
		{
			auto key_hash = this->_hasher(key);
			return this->_underlyingContainer.at(key_hash);
		}

		iterator find(const key_type& key)
		{
			auto key_hash = this->_hasher(key);
			return this->_underlyingContainer.find(key_hash);
		}

		const_iterator find(const key_type& key) const
		{
			auto key_hash = this->_hasher(key);
			return this->_underlyingContainer.find(key_hash);
		}

		iterator begin()
		{
			return this->_underlyingContainer.begin();
		}

		const_iterator begin() const
		{
			return this->_underlyingContainer.begin();
		}

		iterator end()
		{
			return this->_underlyingContainer.end();
		}

		const_iterator end() const
		{
			return this->_underlyingContainer.end();
		}

		reverse_iterator rbegin()
		{
			return this->_underlyingContainer.rbegin();
		}

		const_reverse_iterator rbegin() const
		{
			return this->_underlyingContainer.rbegin();
		}

		reverse_iterator rend()
		{
			return this->_underlyingContainer.rend();
		}

		const_reverse_iterator rend() const
		{
			return this->_underlyingContainer.rend();
		}

		value_type& front()
		{
			return this->_underlyingContainer.front();
		}

		const value_type& front() const
		{
			return this->_underlyingContainer.front();
		}

		value_type& back()
		{
			return this->_underlyingContainer.back();
		}

		const value_type& back() const
		{
			return this->_underlyingContainer.back();
		}

		iterator insert(const key_type&& key, value_type&& value)
		{
			hash_result_type key_hash = this->_hasher(key);
			return this->_underlyingContainer.insert(key_hash, std::move(value));
		}

		iterator insert(const key_type& key, const value_type& value)
		{
			hash_result_type key_hash = this->_hasher(key);
			return this->_underlyingContainer.insert(key_hash, value);
		}

		iterator erase(iterator pos)
		{
			return this->_underlyingContainer.erase(pos);
		}

		iterator erase(iterator start, iterator end)
		{
			return this->_underlyingContainer.erase(start, end);
		}

		iterator remove(const key_type& key)
		{
			hash_result_type key_hash = this->_hasher(key);
			return this->_underlyingContainer.remove(key_hash);
		}

		void clear()
		{
			this->_underlyingContainer.clear();
		}

		void reserve(size_t capacity)
		{
			this->_underlyingContainer.reserve(capacity);
		}

		size_t capacity() const
		{
			return this->_underlyingContainer.capacity();
		}

		size_t size() const
		{
			return this->_underlyingContainer.size();
		}

		bool empty() const
		{
			return this->_underlyingContainer.empty();
		}

		void swap(my_type& other)
		{
			std::swap(this->_hasher, other._hasher);
			std::swap(this->_underlyingContainer, other._underlyingContainer);
		}
	};


	template <class _Key, class _Value, class _Hash, class _Alloc>
	inline auto begin(hash_array<_Key, _Value, _Hash, _Alloc>& container) -> decltype(container.begin())
	{
		return container.begin();
	}

	template <class _Key, class _Value, class _Hash, class _Alloc>
	inline auto begin(const hash_array<_Key, _Value, _Hash, _Alloc>& container) -> decltype(container.begin())
	{
		return container.begin();
	}

	template <class _Key, class _Value, class _Hash, class _Alloc>
	inline auto end(hash_array<_Key, _Value, _Hash, _Alloc>& container) -> decltype(container.end())
	{
		return container.end();
	}

	template <class _Key, class _Value, class _Hash, class _Alloc>
	inline auto end(const hash_array<_Key, _Value, _Hash, _Alloc>& container) -> decltype(container.end())
	{
		return container.end();
	}


}


#endif /* __STD_HASH_ARRAY_H__ */
