
#pragma once

#ifndef __STDX_REF_COUNTER_H__
#define __STDX_REF_COUNTER_H__

#include "common.h"
#include <atomic>


namespace stdx
{
	
	
	typedef size_t ref_counter_value_t;


	class ref_counter
	{
		ref_counter(const ref_counter&) = delete;
		ref_counter& operator=(const ref_counter&) = delete;
		
	private:
		ref_counter_value_t    _refsNum;

	public:
		ref_counter()
		: _refsNum(1)
		{ }

		explicit ref_counter(ref_counter_value_t c)
		: _refsNum(c)
		{ }

		ref_counter_value_t increment()
		{
			return ++this->_refsNum;
		}

		ref_counter_value_t decrement()
		{
			return --this->_refsNum;
		}

		ref_counter_value_t get_value() const
		{
			return this->_refsNum;
		}
	};


	class atomic_ref_counter
	{
		atomic_ref_counter(const atomic_ref_counter&) = delete;
		atomic_ref_counter& operator=(const atomic_ref_counter&) = delete;

	private:
		std::atomic<ref_counter_value_t>  _refsNum;

	public:
		atomic_ref_counter()
		: _refsNum(1)
		{ }

		explicit atomic_ref_counter(ref_counter_value_t c)
		: _refsNum(c)
		{ }

		ref_counter_value_t increment()
		{
			ref_counter_value_t refs_num = this->_refsNum.fetch_add(1, std::memory_order_relaxed);
			return refs_num + 1;
		}

		ref_counter_value_t decrement()
		{
			ref_counter_value_t refs_num = this->_refsNum.fetch_sub(1, std::memory_order_relaxed);
			return refs_num - 1;
		}

		ref_counter_value_t get_value() const
		{
			return this->_refsNum.load(std::memory_order_relaxed);
		}
	};


	class sync_ref_counter
	{
		sync_ref_counter(const sync_ref_counter&) = delete;
		sync_ref_counter& operator=(const sync_ref_counter&) = delete;

	private:
		std::atomic<ref_counter_value_t>  _refsNum;

	public:
		sync_ref_counter()
		: _refsNum(1)
		{ }

		explicit sync_ref_counter(ref_counter_value_t c)
		: _refsNum(c)
		{ }

		ref_counter_value_t increment()
		{
			ref_counter_value_t refs_num = this->_refsNum.fetch_add(1, std::memory_order_release);
			return refs_num + 1;
		}

		ref_counter_value_t increment_cnz()
		{
			ref_counter_value_t refsNum = this->_refsNum.load(std::memory_order_relaxed);

			while(true)
			{
				if(refsNum == 0)
					return 0;

				if(this->_refsNum.compare_exchange_strong(
					refsNum, refsNum + 1, std::memory_order_acq_rel, std::memory_order_relaxed))
				{
					return refsNum + 1;
				}
			}
		}

		ref_counter_value_t decrement()
		{
			ref_counter_value_t refs_num = this->_refsNum.fetch_sub(1, std::memory_order_release);
			return refs_num - 1;
		}

		ref_counter_value_t get_value() const
		{
			return this->_refsNum.load(std::memory_order_acquire);
		}
	};


}


#endif /* __STDX_REF_COUNTER_H__ */
