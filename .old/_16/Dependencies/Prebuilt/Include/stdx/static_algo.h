
#pragma once

#ifndef __STDX_STATIC_ALGO_H__
#define __STDX_STATIC_ALGO_H__

#include "common.h"
#include "type_traits.h"
#include <numeric>


namespace stdx
{


	template <intmax_t _Value>
	struct static_abs_value
	{
		static const intmax_t value = (_Value < 0) ? -_Value : _Value;
	};


	template <intmax_t _Value>
	struct static_sign
	{
		static const int32_t value = (_Value < 0) ? -1 : 1;
	};


	namespace impl
	{

		template <intmax_t _M, intmax_t _N>
		struct static_gcd_impl
		{
			static const intmax_t value = static_gcd_impl<_N, _M % _N>::value;
		};

		template <intmax_t _X>
		struct static_gcd_impl<_X, 0>
		{
			static const intmax_t value = _X;
		};

	}

	template <typename _Tp, _Tp _M, _Tp _N>
	struct static_gcd
	{
		static const _Tp value = impl::static_gcd_impl<static_abs_value<_M>::value, static_abs_value<_N>::value>::value;
	};


	namespace impl
	{

		template <typename _Tp, bool _Cond, _Tp _Val, _Tp _Pow, _Tp _Exp>
		struct static_pow2_round_impl;

		template <typename _Tp, _Tp _Val, _Tp _Pow, _Tp _Exp>
		struct static_pow2_round_impl<_Tp, false, _Val, _Pow, _Exp>
		{
			static const _Tp exponent = static_pow2_round_impl<_Tp, _Val <= (_Pow << 1), _Val, _Pow << 1, _Exp + 1>::exponent;
			static const _Tp value = static_pow2_round_impl<_Tp, _Val <= (_Pow << 1), _Val, _Pow << 1, _Exp + 1>::value;
		};

		template <typename _Tp, _Tp _Val, _Tp _Pow, _Tp _Exp>
		struct static_pow2_round_impl<_Tp, true, _Val, _Pow, _Exp>
		{
			static const _Tp exponent = _Exp;
			static const _Tp value = _Pow;
		};

	}


	template <typename _Tp, _Tp _Val>
	struct static_pow2_round
	{
		static const _Tp exponent = impl::static_pow2_round_impl<_Tp, _Val <= 1, _Val, 1, 0>::exponent;
		static const _Tp value = impl::static_pow2_round_impl<_Tp, _Val <= 1, _Val, 1, 0>::value;
	};


	template <int... Seq>
	struct static_integer_sequence
	{ };

	template <int N, int... Seq>
	struct static_integer_sequence_generator : static_integer_sequence_generator<N-1, N-1, Seq...>
	{ };

	template <int... Seq>
	struct static_integer_sequence_generator<0, Seq...>
	{
		typedef static_integer_sequence<Seq...> Type;
	};


	template <class _Tp, class... _Tn>
	struct static_type_counter
	{
		static const size_t value = 1 + static_type_counter<_Tn...>::value;
	};

	template <class _Tp>
	struct static_type_counter<_Tp>
	{
		static const size_t value = 1;
	};


	template <typename _Tp, _Tp _Num, _Tp _Den = 1>
	struct static_ratio : public std::ratio<static_cast<intmax_t>(_Num), static_cast<intmax_t>(_Den)>
	{
		typedef std::ratio<static_cast<intmax_t>(_Num), static_cast<intmax_t>(_Den)> base_type;
		typedef static_ratio<_Tp, _Num, _Den> type;

		template <typename _Fp>
		static constexpr _Fp get_value()
		{
			return static_cast<_Fp>(_Num) / static_cast<_Fp>(_Den);
		}
	};


}


#endif /* __STDX_STATIC_ALGO_H__ */
