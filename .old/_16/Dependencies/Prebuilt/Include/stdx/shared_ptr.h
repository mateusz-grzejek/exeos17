
#pragma once

#ifndef __STDX_SHARED_PTR_H__
#define __STDX_SHARED_PTR_H__

#include "common.h"
#include <memory>


namespace stdx
{

	
	template <class _Tx, class _Ty>
	inline std::shared_ptr<_Tx> const_shared_ptr_cast(const std::shared_ptr<_Ty>& ptr)
	{
		return std::const_pointer_cast<_Tx>(ptr);
	}

	template <class _Tx, class _Ty>
	inline std::shared_ptr<_Tx> dynamic_shared_ptr_cast(const std::shared_ptr<_Ty>& ptr)
	{
		return std::dynamic_pointer_cast<_Tx>(ptr);
	}

	template <class _Tx, class _Ty>
	inline std::shared_ptr<_Tx> static_shared_ptr_cast(const std::shared_ptr<_Ty>& ptr)
	{
		return std::static_pointer_cast<_Tx>(ptr);
	}


	namespace impl
	{

		template <class _Tx, class _Ty, bool _Debug>
		struct dbgsafe_shared_ptr_cast_impl;

		template <class _Tx, class _Ty>
		struct dbgsafe_shared_ptr_cast_impl<_Tx, _Ty, true>
		{
			static std::shared_ptr<_Tx> do_cast(const std::shared_ptr<_Ty>& ptr)
			{
				return dynamic_shared_ptr_cast<_Tx>(ptr);
			}
		};

		template <class _Tx, class _Ty>
		struct dbgsafe_shared_ptr_cast_impl<_Tx, _Ty, false>
		{
			static std::shared_ptr<_Tx> do_cast(const std::shared_ptr<_Ty>& ptr)
			{
				return static_shared_ptr_cast<_Tx>(ptr);
			}
		};

	}


	template <class _Tx, class _Ty>
	inline std::shared_ptr<_Tx> dbgsafe_shared_ptr_cast(const std::shared_ptr<_Ty>& ptr)
	{
		return impl::dbgsafe_shared_ptr_cast_impl<_Tx, _Ty, true>::do_cast(ptr);
	}


}


#endif /* __STDX_SHARED_PTR_H__ */
