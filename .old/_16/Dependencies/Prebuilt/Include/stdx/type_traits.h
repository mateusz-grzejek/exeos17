
#pragma  once

#ifndef __STDX_TYPE_TRAITS_H__
#define __STDX_TYPE_TRAITS_H__

#include "common.h"
#include "limits.h"


namespace stdx
{

	template <intmax_t _Val>
	struct int_type_by_value
	{
		typedef typename conditional_type<
			(_Val >= limits<int8_t>::min_value) && (_Val <= limits<int8_t>::max_value), int8_t, typename conditional_type<
				(_Val >= limits<int16_t>::min_value) && (_Val <= limits<int16_t>::max_value), int16_t, typename conditional_type<
					(_Val >= limits<int32_t>::min_value) && (_Val <= limits<int32_t>::max_value), int32_t, intmax_t>::type>::type>::type type;
	};


	template <uintmax_t _Val>
	struct uint_type_by_value
	{
		typedef typename conditional_type<
			_Val <= limits<uint8_t>::max_value, uint8_t, typename conditional_type<
				_Val <= limits<uint16_t>::max_value, uint16_t, typename conditional_type<
					_Val <= limits<uint32_t>::max_value, uint32_t, uintmax_t>::type>::type>::type type;
	};


	template <size_t _Size>
	struct int_type_by_size
	{
		typedef typename conditional_type<
			_Size == sizeof(int8_t), int8_t, typename conditional_type<
				_Size == sizeof(int16_t), int16_t, typename conditional_type<
					_Size == sizeof(int32_t), int32_t, intmax_t>::type>::type>::type type;
	};


	template <size_t _Size>
	struct uint_type_by_size
	{
		typedef typename conditional_type<
			_Size == sizeof(uint8_t), uint8_t, typename conditional_type<
				_Size == sizeof(uint16_t), uint16_t, typename conditional_type<
					_Size == sizeof(uint32_t), uint32_t, uintmax_t>::type>::type>::type type;
	};



}


#endif /* __STDX_TYPE_TRAITS_H__ */
