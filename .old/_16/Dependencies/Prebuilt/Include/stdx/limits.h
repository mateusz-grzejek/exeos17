
#pragma once

#ifndef __STDX_LIMITS_H__
#define __STDX_LIMITS_H__

#include "base_config.h"


namespace stdx
{


	template <typename _Tp>
	struct limits;

	template <>
	struct limits<int8_t>
	{
		typedef int8_t value_type;

		static const value_type min_value = CHAR_MIN;
		static const value_type max_value = CHAR_MAX;
		static constexpr value_type get_min() { return CHAR_MIN; }
		static constexpr value_type get_max() { return CHAR_MAX; }
	};

	template <>
	struct limits<uint8_t>
	{
		typedef uint8_t value_type;

		static const value_type min_value = 0;
		static const value_type max_value = UCHAR_MAX;
		static constexpr value_type get_min() { return 0; }
		static constexpr value_type get_max() { return UCHAR_MAX; }
	};

	template <>
	struct limits<int16_t>
	{
		typedef int16_t value_type;

		static const value_type min_value = SHRT_MIN;
		static const value_type max_value = SHRT_MAX;
		static constexpr value_type get_min() { return SHRT_MIN; }
		static constexpr value_type get_max() { return SHRT_MAX; }
	};

	template <>
	struct limits<uint16_t>
	{
	public:
		typedef uint16_t value_type;

		static const value_type min_value = 0;
		static const value_type max_value = USHRT_MAX;
		static constexpr value_type get_min() { return 0; }
		static constexpr value_type get_max() { return USHRT_MAX; }
	};

	template <>
	struct limits<int32_t>
	{
		typedef int32_t value_type;

		static const value_type min_value = LONG_MIN;
		static const value_type max_value = LONG_MAX;
		static constexpr value_type get_min() { return LONG_MIN; }
		static constexpr value_type get_max() { return LONG_MAX; }
	};

	template <>
	struct limits<uint32_t>
	{
	public:
		typedef uint32_t value_type;

		static const value_type min_value = 0;
		static const value_type max_value = ULONG_MAX;
		static constexpr value_type get_min() { return 0; }
		static constexpr value_type get_max() { return ULONG_MAX; }
	};

	template <>
	struct limits<int64_t>
	{
		typedef int64_t value_type;

		static const value_type min_value = LLONG_MIN;
		static const value_type max_value = LLONG_MAX;
		static constexpr value_type get_min() { return LLONG_MIN; }
		static constexpr value_type get_max() { return LLONG_MAX; }
	};

	template <>
	struct limits<uint64_t>
	{
		typedef uint64_t value_type;

		static const value_type min_value = 0;
		static const value_type max_value = ULLONG_MAX;
		static constexpr value_type get_min() { return 0; }
		static constexpr value_type get_max() { return ULLONG_MAX; }
	};

	template <>
	struct limits<float>
	{
		typedef float value_type;

		static constexpr long double get_min() { return std::numeric_limits<float>::min(); }
		static constexpr long double get_max() { return std::numeric_limits<float>::max(); }
		static constexpr long double GetEpsilon() { return std::numeric_limits<float>::epsilon(); }
		static constexpr long double GetInfinity() { return std::numeric_limits<float>::infinity(); }
	};

	template <>
	struct limits<double>
	{
		typedef double value_type;

		static constexpr long double get_min() { return std::numeric_limits<double>::min(); }
		static constexpr long double get_max() { return std::numeric_limits<double>::max(); }
		static constexpr long double GetEpsilon() { return std::numeric_limits<double>::epsilon(); }
		static constexpr long double GetInfinity() { return std::numeric_limits<double>::infinity(); }
	};

	template <>
	struct limits<long double>
	{
		typedef long double value_type;

		static constexpr long double get_min() { return std::numeric_limits<long double>::min(); }
		static constexpr long double get_max() { return std::numeric_limits<long double>::max(); }
		static constexpr long double GetEpsilon() { return std::numeric_limits<long double>::epsilon(); }
		static constexpr long double GetInfinity() { return std::numeric_limits<long double>::infinity(); }
	};


}


#endif /* __STDX_LIMITS_H__ */
