
#pragma once

#ifndef __STDX_COMMON_H__
#define __STDX_COMMON_H__

#include "base_config.h"


namespace stdx
{


	enum class numeric_base : uint32_t
	{
		decimal = 10,

		hexadecimal = 16,

		octal = 8
	};


	enum : size_t
	{
		invalid_position = static_cast<size_t>(-1),

		max_size = static_cast<size_t>(-1),
	};


	template <class _Key, class _Value>
	struct key_value_pair
	{
	public:
		typedef key_value_pair<_Key, _Value> my_type;
		typedef _Key key_type;
		typedef _Value value_type;

	public:
		key_type key;
		value_type value;

	public:
		key_value_pair(my_type&&) = default;
		key_value_pair(const my_type& origin) = default;

		my_type& operator=(my_type&&) = default;
		my_type& operator=(const my_type& origin) = default;

		key_value_pair()
		{ }

		key_value_pair(key_type&& key, value_type&& value)
		: key(std::move(key))
		, value(std::move(value))
		{ }

		key_value_pair(key_type&& key, const value_type& value)
		: key(std::move(key))
		, value(value)
		{ }

		key_value_pair(const key_type& key, value_type&& value)
		: key(key)
		, value(std::move(value))
		{ }

		key_value_pair(const key_type& key, const value_type& value)
		: key(key)
		, value(value)
		{ }
	};


	template <bool _Cond, class _Ttype, class _Ftype>
	struct conditional_type
	{
		typedef typename std::conditional<_Cond, _Ttype, _Ftype>::type type;
	};


	template <typename _T, bool _Cond, _T _Tval, _T _Fval>
	struct conditional_value;

	template <typename _T, _T _Tval, _T _Fval>
	struct conditional_value<_T, true, _Tval, _Fval>
	{
		static const _T value = _Tval;
	};

	template <typename _T, _T _Tval, _T _Fval>
	struct conditional_value<_T, false, _Tval, _Fval>
	{
		static const _T value = _Fval;
	};


	template <class _Tx, class _Ty = _Tx>
	inline constexpr typename std::common_type<_Tx, _Ty>::type get_max_of(const _Tx& left, const _Ty& right)
	{
		return left >= right ? left : right;
	}

	template <class _T0, class _T1, class... _Types>
	inline constexpr typename std::common_type<_T0, _T1, _Types...>::type get_max_of(const _T0& v0, const _T1& v1, _Types&&... vn)
	{
		return get_max_of(get_max_of(v0, v1), std::forward<_Types>(vn)...);
	}
	

	template <class _Tx, class _Ty = _Tx>
	inline constexpr typename std::common_type<_Tx, _Ty>::type get_min_of(const _Tx& left, const _Ty& right)
	{
		return left <= right ? left : right;
	}

	template <class _T0, class _T1, class... _Types>
	inline constexpr typename std::common_type<_T0, _T1, _Types...>::type get_min_of(const _T0& v0, const _T1& v1, _Types&&... vn)
	{
		return get_min_of(get_min_of(v0, v1), std::forward<_Types>(vn)...);
	}


	template <class _T>
	inline void safe_memmove(_T* memory, size_t memSize, size_t start, size_t count, ptrdiff_t offset)
	{
		assert( (memory != nullptr) && (memSize > 0) );
		assert( start + count + offset <= memSize );
		_T* source = memory + start;
		_T* dest = source + offset;
		memmove(dest, source, sizeof(_T) * count);
	}


}


#endif /* __STDX_COMMON_H__  */
