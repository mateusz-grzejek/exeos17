﻿
from ExWorkspaceDefParser import ExwItemGroup
from ExWorkspaceDefParser import ExwItemProject
from ExWorkspaceDefParser import ExwItemRoot
from ExWorkspaceDefParser import ExwParser

import io
import os
import subprocess
import sys


class ExwdLockHandle:

	def __init__(self, file, path):
		self.file = file
		self.path = path
		return

	pass


def exwdGenLockFileName(project : ExwItemProject):
	baseStr = project.subdirFull
	if baseStr[0] == "/":
		baseStr = baseStr[1:]
	result = baseStr.replace("/", "$")
	result += ("$" + project.name + ".plock")
	return result


def exwdOpenLock(lockFileName):
	if os.path.isfile(lockFileName):
		return None
	file = io.open(lockFileName, mode="w+")
	return ExwdLockHandle(file, lockFileName)


def exwdReleaseLock(lockHandle : ExwdLockHandle):
	lockHandle.file.close()
	os.remove(lockHandle.path)
	return


inputArgsNum = len(sys.argv)

inputDesc = {
	"command" : "",
	"arch" : "",
	"build" : "",
	"platform" : "",
	"project" : "",
	"platform" : "",
	"slnfile" : "",
	"execsln" : "",
	"exdef" : ""
}

appArgsList = sys.argv[1:]

if inputArgsNum > 0:
	for appArg in appArgsList:
		optData = appArg.split("=")
		if len(optData) < 2:
			pass
		optName = optData[0]
		optValue = optData[1]
		inputDesc[optName] = optValue
	pass
	
#if inputDesc["execsln"] == inputDesc["slnfile"]:
#	print("Executed from ExDef main solution (" + inputDesc["execsln"] + " == " + inputDesc["slnfile"] + "). Skipping...")
#	exit(0)
#	pass
	
print("Build target: " + inputDesc["platform"] + "/" + inputDesc["arch"] + "_" + inputDesc["build"]);

with io.open(inputDesc["exdef"], "r") as xmlfile:
	fileContent = xmlfile.read()
	exdef = ExwParser.parse(fileContent);
	pass

projectRefName = inputDesc["project"]
projectObj = exdef.findProject(projectRefName)

projectDependencies = projectObj.getDependenciesRefs().getList()
projectDependenciesLocks = []

execArgs = "\"" + inputDesc["slnfile"] + "\""
execArgs += " /" + inputDesc["command"]
execArgs += " \"" + inputDesc["build"] + "|" + inputDesc["arch"] + "\""

for dependency in projectDependencies:
	plockName = exwdGenLockFileName(dependency)
	plock = exwdOpenLock(plockName)
	if plock is None:
		print("Compilation of " + dependency.name + " is already in process. Skipping...")
		continue
	exeCmdArgs = execArgs + " /project \"" + dependency.subdirFull + "/" + dependency.name + "(" + inputDesc["platform"] + ").vcxproj\""
	exeCmd = "ExVscmdExec.bat " + exeCmdArgs
	# print("Executing: " + exeCmd)
	os.system(exeCmd)
	exwdReleaseLock(plock)
	pass
	


print(projectObj.name + ": Done.")
