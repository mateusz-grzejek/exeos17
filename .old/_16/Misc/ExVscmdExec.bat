
@echo off

SETLOCAL EnableDelayedExpansion

@set VSDEVENV="%VS140COMNTOOLS%..\IDE\devenv.com"
@set CMDX=%VSDEVENV%
@set CMDC=0

@for %%x in (%*) do (
	@set CMDX=!CMDX! %%x
	@set /A CMDC+=1
)

@if (!CMDC! GTR 0) do (
	%CMDX%
)
