﻿
import enum
import xml.etree.ElementPath as XMLPath
import xml.etree.ElementTree as XMLTree


class ExwItemType(enum.Enum):
	Group = 1
	Project = 2
	Unknown = 0
	pass


class ExwDependencyDesc:

	def __init__(self, typeStr, refloc):
		if typeStr == "group":
			self.type = ExwItemType.Group
		elif typeStr == "project":
			self.type = ExwItemType.Project
		else:
			self.type = ExwItemType.Unknown
		self.refloc = refloc
		return

	pass


class ExwDependenciesRefList:

	def __init__(self, parent):
		self.parent = parent
		self.dplist = []
		return

	def append(self, dependency):
		for dpref in self.dplist:
			if dependency.uuid == dpref.uuid:
				return
		self.dplist.append(dependency)
		return

	def getList(self):
		return self.dplist

	pass


class ExwItem:

	def __init__(self, type, root, parent, name, subdir = ""):
		self.type = type
		self.root = root
		self.parent = parent
		self.name = name
		self.dependenciesDesc = []

		if parent is None:
			self.subdirFull = ""
		else:
			self.subdirFull = parent.subdirFull
		
		self.subdir = subdir
		self.subdirFull += subdir

		if (len(self.subdir) > 0) and (self.subdir[0] == "/" or self.subdir[0] == "\\"):
			self.subdir = self.subdir[1:]

		if (len(self.subdirFull) > 0) and (self.subdirFull[0] == "/" or self.subdirFull[0] == "\\"):
			self.subdirFull = self.subdirFull[1:]

		return

	def addDependencyDesc(self, dependency):
		self.dependenciesDesc.append(dependency)
		return

	def getDependenciesRefs(self):
		dependenciesRefList = ExwDependenciesRefList(self)
		if self.parent is not None:
			parentDependencies = self.parent.getDependenciesRefs()
			parentDependenciesList = parentDependencies.getList()
			for pdref in parentDependenciesList:
				dependenciesRefList.append(pdref)
		for depDesc in self.dependenciesDesc:
			if depDesc.type == ExwItemType.Project:
				project = None
				if depDesc.refloc[0] == ":" and self.parent is not None:
					project = self.parent.findProject(depDesc.refloc)
				else:
					project = self.root.findProject(depDesc.refloc)
				if project is not None:
					dependenciesRefList.append(project)
			elif depDesc.type == ExwItemType.Group:
				group = None
				if depDesc.refloc[0] == ":" and self.parent is not None:
					group = self.parent.findGroup(depDesc.refloc[1:])
				else:
					group = self.root.findGroup(depDesc.refloc)
				if group is not None:
					groupSubprojects = group.getSubprojects()
					for gsref in groupSubprojects:
						dependenciesRefList.append(gsref)
		return dependenciesRefList

	pass


class ExwItemGroup(ExwItem):

	def __init__(self, root, parent, name, subdir = ""):
		super(ExwItemGroup, self).__init__(ExwItemType.Group, root, parent, name, subdir)
		self.groups =  { }
		self.projects = { }
		return

	def addGroup(self, group):
		self.groups[group.name] = group
		return

	def addProject(self, project):
		self.projects[project.name] = project
		return

	def findGroup(self, groupRefName):
		subgroups = groupRefName.split(":")
		group = self
		for sgName in subgroups:
			group = group.groups.get(sgName)
			if group is None:
				return None
			pass
		return group

	def findProject(self, projectRefName):
		lsindex = projectRefName.rfind(":")
		groupRefName = projectRefName[:lsindex]
		projectName = projectRefName[lsindex+1:]
		group = self
		if groupRefName != "":
			group = self.findGroup(groupRefName)
		if group is not None:
			return group.projects.get(projectName)
		return None

	def getSubprojects(self):
		subprojects = []
		for name, project in self.projects.items():
			subprojects.append(project)
			pass
		for name, subgroup in self.groups.items():
			subgroupProjects = subgroup.getSubprojects()
			subprojects += subgroupProjects
			pass
		return subprojects

	pass


class ExwItemProject(ExwItem):

	def __init__(self, root, parent, name, uuid, subdir = ""):
		super(ExwItemProject, self).__init__(ExwItemType.Project, root, parent, name, subdir)
		self.uuid = uuid
		return

	pass


class ExwItemRoot(ExwItemGroup):

	def __init__(self):
		super(ExwItemRoot, self).__init__(self, None, "")
		self.groupsIndex = { }
		self.projectsIndex = { }
		return

	pass


class ExwParser:

	@staticmethod
	def parseItemDependencies(exwItem, xmlItemDependenciesNode):
		for dependency in xmlItemDependenciesNode:
			if dependency.tag == "Dependency":
				dTypeStr = dependency.get("type")
				dRefloc = dependency.get("refloc")
				dependencyDescObj = ExwDependencyDesc(dTypeStr, dRefloc)
				exwItem.addDependencyDesc(dependencyDescObj)
		return

	@staticmethod
	def parseProject(exwProject, xmlProjectNode):
		for section in xmlProjectNode:
			if section.tag == "Dependencies":
				ExwParser.parseItemDependencies(exwProject, section)
		return

	@staticmethod
	def parseGroupProjects(exwRoot, exwGroup, xmlGroupProjectsNode):
		for entry in xmlGroupProjectsNode:
			if entry.tag == "Project":
				pName = entry.get("name")
				pSubdir = entry.get("subdir", default="/"+pName)
				pUuid = entry.get("uuid", default="")
				projectObj = ExwItemProject(exwRoot, exwGroup, pName, pUuid, pSubdir)
				ExwParser.parseProject(projectObj, entry)
				exwGroup.addProject(projectObj)
			elif entry.tag == "Group":
				gName = entry.get("name")
				gSubdir = entry.get("subdir")
				groupObj = ExwItemGroup(exwRoot, exwGroup, gName, gSubdir)
				ExwParser.parseGroup(exwRoot, groupObj, entry)
				exwGroup.addGroup(groupObj)

		return
	
	pass

	@staticmethod
	def parseGroup(exwRoot, exwGroup, xmlGroupNode):
		for section in xmlGroupNode:
			if section.tag == "Dependencies":
				ExwParser.parseItemDependencies(exwGroup, section)
			elif section.tag == "Projects":
				ExwParser.parseGroupProjects(exwRoot, exwGroup, section)
		return

	@staticmethod
	def parse(exwString):
		exwRoot = ExwItemRoot()
		xmlRoot = XMLTree.fromstring(exwString)
		if xmlRoot.tag != "XWDRoot":
			return None
		ExwParser.parseGroup(exwRoot, exwRoot, xmlRoot)
		return exwRoot

	pass
