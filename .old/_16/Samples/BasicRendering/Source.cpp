
#include <ExsCore/CoreEngineState.h>
#include <ExsCore/File.h>
#include <ExsCore/Plugins/PluginLoader.h>
#include <ExsCore/Plugins/PluginSystem.h>
#include <ExsCore/System/AppObject.h>
#include <ExsCore/System/AppEventLoop.h>
#include <ExsCore/System/EvtSysInput.h>
#include <ExsCore/System/KeyboardKeyCodes.h>
#include <ExsCore/System/SystemEventDispatcher.h>
#include <ExsCore/System/SystemEventReceiver.h>
#include <ExsCore/System/SystemWindow.h>

#include <ExsMath/Matrix4.h>
#include <ExsMath/MatrixUtils.h>
#include <ExsMath/Random.h>
#include <ExsMath/Vector4.h>
#include <ExsMath/VectorUtils.h>

#include <ExsRenderSystem/GraphicsDriver.h>
#include <ExsRenderSystem/GraphicsDriverCapabilities.h>
#include <ExsRenderSystem/GraphicsDriverPlugin.h>
#include <ExsRenderSystem/RenderSystem.h>
#include <ExsRenderSystem/RSContextCommandQueue.h>
#include <ExsRenderSystem/RSContextStateController.h>
#include <ExsRenderSystem/RSSwapChain.h>
#include <ExsRenderSystem/RSThreadState.h>

#include <ExsRenderSystem/Memory/RSMemorySystem.h>

#include <ExsRenderSystem/Resource/ConstantBuffer.h>
#include <ExsRenderSystem/Resource/IndexBuffer.h>
#include <ExsRenderSystem/Resource/Sampler.h>
#include <ExsRenderSystem/Resource/Shader.h>
#include <ExsRenderSystem/Resource/ShaderResourceView.h>
#include <ExsRenderSystem/Resource/Texture.h>
#include <ExsRenderSystem/Resource/VertexBuffer.h>

#include <ExsRenderSystem/State/BindingCacheTable.h>
#include <ExsRenderSystem/State/GraphicsPipelineStateObject.h>
#include <ExsRenderSystem/State/InputLayoutState.h>
#include <ExsRenderSystem/State/VertexArrayStateObject.h>
#include <ExsRenderSystem/State/VertexStreamState.h>

#include <ExsResManager/Importers/ImageLoader.h>
#include <ExsResManager/Importers/ResourceLoaderPlugin.h>
#include <ExsResManager/Resources/TrueTypeFont.h>
#include <ExsResManager/Resources/ImageData.h>
#include <ExsResManager/Resources/PixelFormatConverter.h>

#include <ExsEngine/Utils/GridGenerator.h>
#include <ExsUI/UIGeometry.h>
#include <ExsUI/UIRenderer.h>
#include <ExsUI/Controls/Text.h>

using namespace Exs;


class MainThread : public ActiveThread
{
public:
	MainThread(EXS_THR_CTOR_STDARGS_DECL_ACTIVE_THREAD)
	: ActiveThread(EXS_THR_CTOR_STDARGS_DEF_ACTIVE_THREAD, "Main_thread")
	{ }

	virtual void Entry() override final
	{ }
};

class MyApp : public AppObject
{
public:
	MyApp(CoreEngineStateRefHandle coreEngineState, AppGlobalStateRefHandle appGlobalState)
	: AppObject(coreEngineState, appGlobalState)
	{ }
};

class MyAppEventLoop : public AppEventLoop
{
public:
	MyAppEventLoop(AppObject* appObject, CoreEngineStateRefHandle coreEngineState, AppGlobalStateRefHandle appGlobalState)
	: AppEventLoop(appObject, coreEngineState, appGlobalState)
	{ }
};


int main(int argc, const char** argv)
{
	AppConfiguration appConfiguration { };
	appConfiguration.coreEngineFeatureMask = CoreEngineFeature_All;

	auto appObject = CreateAppObject<MyApp, MyAppEventLoop, MainThread>(appConfiguration);

	if(!appObject)
		return -1;

	auto coreEngineState = appObject->GetCoreEngineState();
	auto appGlobalState = appObject->GetAppGlobalState();

	auto* ilp = coreEngineState->pluginSystem->AddManager<PluginType_Resource_Loader>();
	auto pls = coreEngineState->pluginSystem->LoadPlugin("Plugins/ExsPlugin.CoreImageLoaders");
	auto* loaderPlugin = pls.first->GetAs<PluginType_Resource_Loader>();

	auto* rpm = coreEngineState->pluginSystem->AddManager<PluginType_GraphicsDriver>();
	coreEngineState->pluginSystem->LoadPlugin("GraphicsDrivers/ExsGraphicsDriver.GL3");
	PluginHandle gl3pb = coreEngineState->pluginSystem->GetPlugin("EXS:R:GL3");
	auto* gl3p = gl3pb.GetPlugin()->GetAs<PluginType_GraphicsDriver>();
	auto gl3r = gl3p->CreateGraphicsDriver();

	bool appUpdate = true;

	SystemEventReceiver eventReceiver { };
	eventReceiver.RegisterHandler(
		SystemEventCode::Input_Keyboard_Key_Pressed,
		BindEventHandler([appObject,&appUpdate](const SystemEvent& event) -> bool {
			ExsDebugAssert( event.GetCode() == SystemEventCode::Input_Keyboard_Key_Pressed );
			auto* keyPressedEvent = static_cast<const EvtSysInputKeyboardKeyPressed*>(&event);
			SystemKeyCode keyCode = keyPressedEvent->GetKeyCode();
			if(keyCode == KeyboardKey_Escape)
				appObject->SetExitRequest();
			else if(keyCode == KeyboardKey_Space)
				appUpdate = !appUpdate;
			return true;
		}));
	eventReceiver.RegisterHandler(
		SystemEventCode::Window_Command_Close,
		BindEventHandler([appObject](const SystemEvent& event) -> bool {
			ExsDebugAssert( event.GetCode() == SystemEventCode::Window_Command_Close );
			ExsTraceInfo(TRC_Core_System, "X11Input: Window closed!");
			appObject->SetExitRequest();
			return true;
		}));
	appObject->SetActiveEventReceiver(&eventReceiver);

	SystemWindowFrameDesc windowDesc { };
	windowDesc.position = Position(100,100);
	windowDesc.size = Size(960,540);
	windowDesc.title = "Exeos 3D Engine";
	auto xwindow = CreateSystemWindow(appGlobalState, windowDesc);
	xwindow->Show(true);

	auto* rconfig = gl3r->Configure(*xwindow);
	GraphicsDriverInitDesc rendererInitDesc { };
	rendererInitDesc.flags = GraphicsDriverInit_Default;
	rendererInitDesc.featureLevel = GraphicsDriverFeatureLevel::FL4D10G33SM4;
	rendererInitDesc.visualConfig.colorFormat = VisualColorFormat::R8G8B8A8;
	rendererInitDesc.visualConfig.depthStencilFormat = VisualDepthStencilFormat::D24S8;
	rendererInitDesc.visualConfig.msaaConfig = VisualMSAAConfig::Disabled;
	gl3r->Initialize(rendererInitDesc);
	rconfig = nullptr;

	auto* rs = gl3r->GetRenderSystem();
	auto* ts = gl3r->GetMainRSThreadState();
	auto* rsCmd = ts->GetContextCommandQueue();
	auto* rsSct = ts->GetContextStateController();

	auto* memorySystem = rs->GetMemorySystem();

	{
		RSResourceMemoryHeapDesc resHeapDesc;
		resHeapDesc.alignment = RSMemoryAlignment::Default;
		resHeapDesc.resourceType = RSMemoryHeapResourceType::All;
		resHeapDesc.size = UnitConverter<DataUnit::Megabyte>::Get<decltype(resHeapDesc.size)>(64);
		resHeapDesc.usage = RSMemoryHeapUsage::Default;
		auto* heap1 = memorySystem->CreateHeap<RSAllocatorDefault>(resHeapDesc);
		resHeapDesc.usage = RSMemoryHeapUsage::Dynamic;
		auto* heap2 = memorySystem->CreateHeap<RSAllocatorDefault>(resHeapDesc);
	
		ShaderHandle vshader;
		ShaderHandle pshader;
		ShaderHandle vshaderText;
		ShaderHandle pshaderText;

		VertexBufferHandle vbuffer;
		IndexBufferHandle ibuffer;
		ConstantBufferHandle cbuffer0;
		ConstantBufferHandle cbuffer1;
		Texture2DHandle texture1;
		Texture2DHandle texture2;
		SamplerHandle sampler1;
		SamplerHandle sampler2;
		ShaderResourceViewHandle texture1View;
		ShaderResourceViewHandle texture2View;

		VertexArrayStateObjectHandle vasObject;
		GraphicsPipelineStateObjectHandle gpsObject;

		std::unique_ptr<Font> ttfont;

		// Sample resources creation.
		{

			stdx::dynamic_data_array<Byte> fileData;
			fileData.clear();

			TrueTypeFontDesc fdesc;
			fdesc.fontSize = 20;
			fdesc.layerSize.x = 256;
			fdesc.layerSize.y = 256;
			fdesc.layersNum = 2;
			fdesc.preRenderAscii = true;
			fdesc.loadKerningTable = true;

			{
				auto fontFile = File::Open("../../../Assets/Fonts/calibri_normal.ttf", FileOpenMode::Open_Existing);
				fontFile.ReadAll(fdesc.ttfFileData);
			}

			ttfont = CreateTrueTypeFont(rs, fdesc);

			auto* imageLoaderBMP = loaderPlugin->GetLoaderByName<ResourceType_Image>("EXS_BMP");
			auto* imageLoaderPNG = loaderPlugin->GetLoaderByName<ResourceType_Image>("EXS_PNG");
			auto bmpLoader = imageLoaderBMP->CreateLoaderObject();
			auto pngLoader = imageLoaderPNG->CreateLoaderObject();

			ImageData imageData;

			Texture2DCreateInfo tex2DCreateInfo;
			tex2DCreateInfo.usageFlags = RSResourceUsage_Static;
			tex2DCreateInfo.internalFormat = GraphicDataFormat::B8G8R8X8UNORM;
			tex2DCreateInfo.memoryAccess = RSMemoryAccess::None;
			tex2DCreateInfo.mipmapLevelsNum = 1;

			auto imgfile = File::Open("../../../Assets/Textures/skyrim.bmp", FileOpenMode::Open_Existing);
			fileData.clear();
			imgfile.ReadAll(fileData);
			bmpLoader->LoadImage(fileData.data_ptr(), fileData.size_in_bytes(), &imageData, 0);

			tex2DCreateInfo.width = imageData.info.width;
			tex2DCreateInfo.height = imageData.info.height;
			tex2DCreateInfo.initDataDesc.dataPtr = imageData.pixelArray.data_ptr();
			tex2DCreateInfo.initDataDesc.dataSize = imageData.pixelArray.size_in_bytes();
			tex2DCreateInfo.initDataDesc.rowPitch = imageData.info.pixelSize * imageData.info.width;
			texture1 = rs->CreateTexture2D(tex2DCreateInfo);
			texture1View = rs->CreateTextureShaderResourceView(texture1);

			imgfile = File::Open("../../../Assets/Textures/box.bmp", FileOpenMode::Open_Existing);
			fileData.clear();
			imgfile.ReadAll(fileData);
			bmpLoader->LoadImage(fileData.data_ptr(), fileData.size_in_bytes(), &imageData, 0);

			tex2DCreateInfo.width = imageData.info.width;
			tex2DCreateInfo.height = imageData.info.height;
			tex2DCreateInfo.initDataDesc.dataPtr = imageData.pixelArray.data_ptr();
			tex2DCreateInfo.initDataDesc.dataSize = imageData.pixelArray.size_in_bytes();
			tex2DCreateInfo.initDataDesc.rowPitch = imageData.info.pixelSize * imageData.info.width;
			texture2 = rs->CreateTexture2D(tex2DCreateInfo);
			texture2View = rs->CreateTextureShaderResourceView(texture2);

			SamplerCreateInfo smpCreateInfo;
			smpCreateInfo.parameters.addressMode.x = TextureAddressMode::Clamp_To_Border;
			smpCreateInfo.parameters.addressMode.y = TextureAddressMode::Clamp_To_Border;
			smpCreateInfo.parameters.addressMode.z = TextureAddressMode::Clamp_To_Border;
			smpCreateInfo.parameters.filter.magFilter = TextureFilter::Linear;
			smpCreateInfo.parameters.filter.minFilter = TextureFilter::Linear;
			smpCreateInfo.parameters.filter.mipLevelSamplingFilter = TextureFilter::None;
			smpCreateInfo.parameters.anisotropyLevel = 4;
			smpCreateInfo.parameters.lodBias = 0.0f;
			smpCreateInfo.parameters.lodRange.first = 0;
			smpCreateInfo.parameters.lodRange.second = 0;
			smpCreateInfo.parameters.refComparisonEnabled = false;
			smpCreateInfo.parameters.refComparisonFunction = ComparisonFunction::Always;
			sampler1 = rs->CreateSampler(smpCreateInfo);
			sampler2 = rs->CreateSampler(smpCreateInfo);

			pls.first = nullptr;

			ShaderCreateInfo shaderCreateInfo { };
			auto vsfile = File::Open("../../../Assets/Shaders/simpleRotate.vshader", FileOpenMode::Open_Existing);
			auto psfile = File::Open("../../../Assets/Shaders/simpleRotate.pshader", FileOpenMode::Open_Existing);
		
			fileData.clear();
			vsfile.ReadAll(fileData);
			shaderCreateInfo.shaderType = ShaderType::Vertex;
			shaderCreateInfo.shaderCode = fileData.data_ptr();
			shaderCreateInfo.shaderCodeSize = fileData.size_in_bytes();
			vshader = rs->CreateShader(shaderCreateInfo);

			fileData.clear();
			psfile.ReadAll(fileData);
			shaderCreateInfo.shaderType = ShaderType::Pixel;
			shaderCreateInfo.shaderCode = fileData.data_ptr();
			shaderCreateInfo.shaderCodeSize = fileData.size_in_bytes();
			pshader = rs->CreateShader(shaderCreateInfo);
		
			vsfile = File::Open("../../../Assets/Shaders/textDefault.vshader", FileOpenMode::Open_Existing);
			psfile = File::Open("../../../Assets/Shaders/textDefault.pshader", FileOpenMode::Open_Existing);
		
			fileData.clear();
			vsfile.ReadAll(fileData);
			shaderCreateInfo.shaderType = ShaderType::Vertex;
			shaderCreateInfo.shaderCode = fileData.data_ptr();
			shaderCreateInfo.shaderCodeSize = fileData.size_in_bytes();
			vshaderText = rs->CreateShader(shaderCreateInfo);

			fileData.clear();
			psfile.ReadAll(fileData);
			shaderCreateInfo.shaderType = ShaderType::Pixel;
			shaderCreateInfo.shaderCode = fileData.data_ptr();
			shaderCreateInfo.shaderCodeSize = fileData.size_in_bytes();
			pshaderText = rs->CreateShader(shaderCreateInfo);

			struct Vertex
			{
				float x, y, z;
				float u, v;
			};

			const Vertex vertices[] =
			{
				{ -0.0f, -0.0f, -0.5f, 0.0f, 0.0f },
				{ +960.0f, -0.0f, -0.5f, 1.0f, 0.0f },
				{ +960.0f, +540.0f, -0.5f, 1.0f, 1.0f },
				{ -0.0f, +540.0f, -0.5f, 0.0f, 1.0f },
			
				// Front
				{ -0.25f, -0.25f, +0.25f, 0.0f, 0.0f },
				{ +0.25f, -0.25f, +0.25f, 1.0f, 0.0f },
				{ +0.25f, +0.25f, +0.25f, 1.0f, 1.0f },
				{ -0.25f, +0.25f, +0.25f, 0.0f, 1.0f },
			
				// Back
				{ +0.25f, -0.25f, -0.25f, 0.0f, 0.0f },
				{ -0.25f, -0.25f, -0.25f, 1.0f, 0.0f },
				{ -0.25f, +0.25f, -0.25f, 1.0f, 1.0f },
				{ +0.25f, +0.25f, -0.25f, 0.0f, 1.0f },
			
				// Left
				{ -0.25f, -0.25f, -0.25f, 0.0f, 0.0f },
				{ -0.25f, -0.25f, +0.25f, 1.0f, 0.0f },
				{ -0.25f, +0.25f, +0.25f, 1.0f, 1.0f },
				{ -0.25f, +0.25f, -0.25f, 0.0f, 1.0f },
			
				// Right
				{ +0.25f, -0.25f, +0.25f, 0.0f, 0.0f },
				{ +0.25f, -0.25f, -0.25f, 1.0f, 0.0f },
				{ +0.25f, +0.25f, -0.25f, 1.0f, 1.0f },
				{ +0.25f, +0.25f, +0.25f, 0.0f, 1.0f },
			
				// Top
				{ -0.25f, +0.25f, +0.25f, 0.0f, 0.0f },
				{ +0.25f, +0.25f, +0.25f, 1.0f, 0.0f },
				{ +0.25f, +0.25f, -0.25f, 1.0f, 1.0f },
				{ -0.25f, +0.25f, -0.25f, 0.0f, 1.0f },
			
				// Bottom
				{ +0.25f, -0.25f, +0.25f, 0.0f, 0.0f },
				{ -0.25f, -0.25f, +0.25f, 1.0f, 0.0f },
				{ -0.25f, -0.25f, -0.25f, 1.0f, 1.0f },
				{ +0.25f, -0.25f, -0.25f, 0.0f, 1.0f },
			};

			const Uint32 indices[] =
			{
				// Background
				0,  1,  2,  0,  2,  3,

				// Cube
				4,  5,  6,  4,  6,  7,
				8,  9,  10, 8,  10, 11,
				12, 13, 14, 12, 14, 15,
				16, 17, 18, 16, 18, 19,
				20, 21, 22, 20, 22, 23,
				24, 25, 26, 24, 26, 27
			};

			VertexBufferCreateInfo vbCreateInfo;
			vbCreateInfo.memoryAccess = RSMemoryAccess::None;
			vbCreateInfo.usageFlags = RSResourceUsage_Static;
			vbCreateInfo.sizeInBytes = sizeof(vertices);
			vbuffer = rs->CreateVertexBuffer(vbCreateInfo);
			vbuffer->CopySubdata(RSMemoryRange(0, sizeof(vertices)), vertices, sizeof(vertices));

			IndexBufferCreateInfo ibCreateInfo;
			ibCreateInfo.memoryAccess = RSMemoryAccess::None;
			ibCreateInfo.usageFlags = RSResourceUsage_Static;
			ibCreateInfo.sizeInBytes = sizeof(indices);
			ibuffer = rs->CreateIndexBuffer(ibCreateInfo);
			ibuffer->CopySubdata(RSMemoryRange(0, sizeof(indices)), indices, sizeof(indices));

			ConstantBufferCreateInfo cbCreateInfo;
			cbCreateInfo.usageFlags = RSResourceUsage_Dynamic;
			cbCreateInfo.sizeInBytes = UnitConverter<DataUnit::Megabyte>::Get<Uint32>(4);
			cbuffer0 = rs->CreateConstantBuffer(cbCreateInfo);
			cbuffer1 = rs->CreateConstantBuffer(cbCreateInfo);
		
			{
				GraphicsPipelineStateObjectCreateInfo gpsCreateInfo { };
				gpsCreateInfo.depthStencilConfiguration.depthTestState = ActiveState::Enabled;
				gpsCreateInfo.rasterizerConfiguration.settings.faceCullMode = CullMode::Default;
				gpsCreateInfo.rasterizerConfiguration.settings.frontFaceOrder = VerticesOrder::Counter_Clockwise;
				gpsCreateInfo.graphicsShaderConfiguration.vertexShader = vshader;
				gpsCreateInfo.graphicsShaderConfiguration.pixelShader = pshader;
			
				GraphicsShaderDataBinding attrBinding[] =
				{
					{ "inVecPosition", 0 },
					{ "inVecTexCoord", 1 },
					GraphicsShaderDataBindingEmpty
				};
			
				GraphicsShaderDataBinding samplerBinding[] =
				{
					{ "texture0", 0 },
					GraphicsShaderDataBindingEmpty
				};
			
				GraphicsShaderDataBinding ublockBinding[] =
				{
					{ "CB0CommonData", 0 },
					{ "CB1ExtData", 1 },
					GraphicsShaderDataBindingEmpty
				};
			
				gpsCreateInfo.graphicsShaderConfiguration.customAttribBindingArray = attrBinding;
				gpsCreateInfo.graphicsShaderConfiguration.customSamplerBindingArray = samplerBinding;
				gpsCreateInfo.graphicsShaderConfiguration.customUniformBlockBindingArray = ublockBinding;
			
				{
					auto& attrib0 = gpsCreateInfo.inputLayoutConfiguration.attribArray[IAVertexAttribLocation::Attrib_0];
					attrib0.state = IAVertexAttribState::Active;
					attrib0.name = "inVecPosition";
					attrib0.format = VertexAttribFormat::Vec3_Float;
					attrib0.streamIndex = IAVertexStreamIndex::Stream_0;
					attrib0.relativeOffset = 0;
				}
				{
					auto& attrib1 = gpsCreateInfo.inputLayoutConfiguration.attribArray[IAVertexAttribLocation::Attrib_1];
					attrib1.state = IAVertexAttribState::Active;
					attrib1.name = "inVecTexCoord";
					attrib1.format = VertexAttribFormat::Vec2_Float;
					attrib1.streamIndex = IAVertexStreamIndex::Stream_0;
					attrib1.relativeOffset = sizeof(float)*3;
				}
				auto& stream0 = gpsCreateInfo.inputLayoutConfiguration.streamArray[IAVertexStreamIndex::Stream_0];
				{
					stream0.state = IAVertexStreamState::Active;
					stream0.rate = IAVertexStreamRate::Per_Vertex;
					stream0.stride = sizeof(Vertex);
				}
				gpsCreateInfo.inputLayoutConfiguration.primitiveTopologyDescription.primitiveTopology = PrimitiveTopology::Triangle_List;
				gpsCreateInfo.inputLayoutConfiguration.primitiveTopologyDescription.tesselationPatchControlPointsNum = 1;
				gpsObject = rs->CreateGraphicsPipelineStateObject(gpsCreateInfo);
			}
		
			{
				VertexArrayStateObjectCreateInfo vasCreateInfo { };
				vasCreateInfo.vertexStreamConfiguration.indexBufferBinding.buffer = ibuffer;
				auto& vbinding0 = vasCreateInfo.vertexStreamConfiguration.vertexBufferBindingArray[IAVertexStreamIndex::Stream_0];
				{
					vbinding0.buffer = vbuffer;
					vbinding0.dataBaseOffset = 0;
					vbinding0.dataStride = sizeof(Vertex);
				}
				vasObject = rs->CreateVertexArrayStateObject(vasCreateInfo);
			}

			rsCmd->SetGraphicsPipelineState(gpsObject);
			rsCmd->SetVertexArrayState(vasObject);
		}

		Size winSize;
		xwindow->GetClientAreaSize(&winSize);
		float winAspect = (float)winSize.width/(float)winSize.height;

		struct CB0Data
		{
			Matrix4F projMat;
			Matrix4F viewMat;
		};

		struct CB1Data
		{
			Matrix4F rotateMat;
			Matrix4F translateMat;
		};

		CB0Data cb0Data =
		{
			Matrix4F(),
			Matrix4F()
		};

		CB1Data cb1Data =
		{
			Matrix4F(),
			Matrix4F()
		};
	
		cbuffer0->Update(0, cb0Data);
		cbuffer0->Flush();

		Viewport viewport { 0, 0, winSize.width, winSize.height };
		rsCmd->SetViewport(&viewport);
		Color clearColor { 0, 0, 0, 255 };
		rsCmd->SetClearColor(&clearColor);
		DepthRange depthRange { 0.1f, 64.0f };
		rsCmd->SetDepthRange(&depthRange);
	
		rsCmd->SetConstantBuffer(0, ShaderStage_All_Graphics, cbuffer0);
		rsCmd->SetConstantBuffer(1, ShaderStage_All_Graphics, cbuffer1);
	
		float rotAngle = 0.0f;
		float winWidth = static_cast<float>(winSize.width);
		float winHeight = static_cast<float>(winSize.height);

		UIGeometryManagerInitDesc tgmid;
		tgmid.storageCapacity = 65536;
		auto textGeometryManager = CreateUIGeometryManager(rs, tgmid);

		TextProperties tp1 { Vector3F(50.0f, 50.0f, -0.4f), 0xFFFFFFFF, ttfont.get() };
		TextProperties tp2 { Vector3F(50.0f, 120.0f, -0.4f), 0xFFFFFFFF, ttfont.get() };
		TextProperties tp3 { Vector3F(50.0f, 80.0f, -0.4f), 0xFFFFFFFF, ttfont.get() };

		textGeometryManager->BeginUpdate(GeometryUpdateMode::Append);
		auto txt1 = textGeometryManager->AddGeometry(256); textGeometryManager->UpdateTextGeometry(txt1, "xxxxxxx", tp1);
		auto txt2 = textGeometryManager->AddGeometry(256); textGeometryManager->UpdateTextGeometry(txt2, "Magda Mazur", tp2);
		auto txt3 = textGeometryManager->AddGeometry(256); textGeometryManager->UpdateTextGeometry(txt3, "Other text", tp3);
		textGeometryManager->EndUpdate();

		TrueTypeFontTextGraphicsDriver::InitDefaultGraphicsPipelineState(rs, vshaderText, pshaderText);
		TrueTypeFontTextGraphicsDriver::InitFontTextureSampler(rs);
		TrueTypeFontTextGraphicsDriver::InitSharedTextDataConstantBuffer(rs);

		auto textGraphicsDriver = TextGraphicsDriver::CreateGraphicsDriver(FontType::True_Type, rsCmd);

		auto strUpdateTime = MonotonicClock::Now();
		const std::string txtup[] = { "Mateusz Grzejek", "Exeos Development" };
		Uint32 txtidx = 0;

		appObject->RunEventLoop(
			AppEventLoopRun_Default,
			[&]() -> bool {
				if(!appUpdate)
					return true;

				rsCmd->Clear(RenderTargetBuffer_All);
				{
					rsCmd->SetGraphicsPipelineState(gpsObject);
					rsCmd->SetVertexArrayState(vasObject);
				
					cb0Data.projMat = Matrix<>::Ortho(0.0f, winWidth, 0.0f, winHeight);
					cbuffer0->Update(0, cb0Data);
					cbuffer0->Flush();
				
					cb1Data.rotateMat = Matrix4F();
					cb1Data.translateMat = Matrix4F();
					cbuffer1->Update(0, cb1Data);
					cbuffer1->Flush();
				
					rsCmd->SetSampler(0, ShaderStage_All_Graphics, sampler1);
					rsCmd->SetShaderResource(0, ShaderStage_All_Graphics, texture1View);
					rsCmd->DrawIndexed(0, 6, IndexBufferDataType::Uint32);
				
					cb0Data.projMat = Matrix<>::Perspective(EXS_MATH_SPC_DTR_45, winAspect, 0.1f, 100.0f);
					cbuffer0->Update(0, cb0Data);
					cbuffer0->Flush();
				
					cb1Data.rotateMat = Matrix<>::RotationY(rotAngle);
					cb1Data.translateMat = Matrix<>::Translation(0.0f, 0.0f, -0.5f);
					cbuffer1->Update(0, cb1Data);
					cbuffer1->Flush();
				
					rsCmd->SetSampler(0, ShaderStage_All_Graphics, sampler2);
					rsCmd->SetShaderResource(0, ShaderStage_All_Graphics, texture2View);
					rsCmd->DrawIndexed(24, 36, IndexBufferDataType::Uint32);

					rotAngle += 0.05f;
				}

				{
					auto currentTime = MonotonicClock::Now();
					auto diff = DurationCast<DurationPeriod::Millisecond>(currentTime - strUpdateTime);

					if(diff > Seconds(1))
					{
						textGeometryManager->Modify(*txt1, txtup[txtidx], *ttfont, Vector3F(50.0f, 50.0f, -0.4f));
						strUpdateTime = currentTime;
						txtidx = (txtidx + 1) % 2;
						textGeometryManager->FlushUpdateCache();
					}

					cb0Data.projMat = Matrix<>::Ortho(0.0f, winWidth, 0.0f, winHeight);
					cbuffer0->Update(0, cb0Data);
					cbuffer0->Flush();

					cb1Data.rotateMat = Matrix4F();
					cb1Data.translateMat = Matrix4F();
					cbuffer1->Update(0, cb1Data);
					cbuffer1->Flush();
				
					textGraphicsDriver->RenderText(*txt1);
					textGraphicsDriver->RenderText(*txt2);

					cb1Data.rotateMat = Matrix4F();
					cb1Data.translateMat = Matrix<>::Translation(240.0f, 0.0f, 0.0f);
					cbuffer1->Update(0, cb1Data);
					cbuffer1->Flush();
					
					textGraphicsDriver->RenderText(*txt1);
					textGraphicsDriver->RenderText(*txt2);

					cb1Data.rotateMat = Matrix<>::RotationZ(-0.12f);
					cb1Data.translateMat = Matrix4F();
					cbuffer1->Update(0, cb1Data);
					cbuffer1->Flush();
					
					textGraphicsDriver->RenderText(*txt3);
				}
				rsCmd->Present();

				return true;
		});

		TextGraphicsDriver::ReleaseSharedObjects();
	
		ttfont = nullptr;
		vshader = nullptr;
		pshader = nullptr;
		vshaderText = nullptr;
		pshaderText = nullptr;
		vbuffer = nullptr;
		ibuffer = nullptr;
		cbuffer0 = nullptr;
		cbuffer1 = nullptr;
		texture1 = nullptr;
		texture2 = nullptr;
		sampler1 = nullptr;
		sampler2 = nullptr;
		texture1View = nullptr;
		texture2View = nullptr;
		vasObject = nullptr;
		gpsObject = nullptr;

		textGraphicsDriver = nullptr;
		textGeometryManager = nullptr;
	}

	gl3r->Release();
	gl3r = nullptr;
	gl3pb = nullptr;

	coreEngineState->pluginSystem->UnloadAll();

	xwindow->Show(false);
	xwindow->Destroy();
	xwindow = nullptr;

	return 0;
}
