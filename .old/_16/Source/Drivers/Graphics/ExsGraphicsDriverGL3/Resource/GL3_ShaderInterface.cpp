
#include "_Precompiled.h"
#include <ExsRenderSystem/Resource/Shader.h>
#include <ExsGraphicsDriverGL3/Resource/GL3_ShaderInterface.h>


namespace Exs
{


	RSInternalHandle<GL3ShaderInterface> InitGL3ShaderInterface(const ShaderCreateInfo& createInfo)
	{
		GLenum oglShaderType = GLConstantMap::GetShaderType(createInfo.shaderType);
		auto shaderObject = std::make_unique<GLShaderObject>(oglShaderType);

		if (shaderObject->CompileSource(reinterpret_cast<const Byte*>(createInfo.shaderCode), createInfo.shaderCodeSize))
		{
			auto shaderInterface = CreateRSBaseObjectInterface<GL3ShaderInterface>(std::move(shaderObject));
			return shaderInterface;
		}

		return nullptr;
	}


}
