
#include "_Precompiled.h"
#include <ExsCore/Plugins/PluginSystem.h>
#include <ExsGraphicsDriverGL3/GL3_GraphicsDriverPlugin.h>

using namespace Exs;


extern "C" EXS_ATTR_DLL_EXPORT Plugin* PluginInit(const DynamicLibrary*, PluginManager* manager);
extern "C" EXS_ATTR_DLL_EXPORT void PluginRelease(const DynamicLibrary*, Plugin* plugin);


extern "C" EXS_ATTR_DLL_EXPORT PluginModuleData EXS_PLUGIN_MODULE_PROPERTIES_REF_NAME =
{
	"EXS:R:GL3",
	"OpenGL 3.x GraphicsDriver",
	"GL3,OGL3,OpenGL3",
	"{B7C46E49-57AD-4D39-A9F0-451108BC78AD}",
	PluginType_GraphicsDriver,
	EXS_TARGET_ARCHITECTURE,
	EXS_TARGET_ARCHITECTURE_STR,
	"0.0.0.1",
	{ }
};


extern "C" EXS_ATTR_DLL_EXPORT PluginModuleInterface EXS_PLUGIN_MODULE_INTERFACE_REF_NAME =
{
	PluginInit,
	PluginRelease
};


Plugin* PluginInit(const DynamicLibrary*, PluginManager* manager)
{
	PluginType pluginType = manager->GetType();

	if (pluginType != PluginType_GraphicsDriver)
		return nullptr;

	auto* GraphicsDriverManager = manager->GetAs<PluginType_GraphicsDriver>();
	ExsDebugAssert( GraphicsDriverManager != nullptr );

	GL3GraphicsDriverPlugin* gl3GraphicsDriverPlugin = new GL3GraphicsDriverPlugin(GraphicsDriverManager);
	return gl3GraphicsDriverPlugin;
}


void PluginRelease(const DynamicLibrary*, Plugin* plugin)
{
	GL3GraphicsDriverPlugin* gl3GraphicsDriverPlugin = dynamic_cast<GL3GraphicsDriverPlugin*>(plugin);
	ExsRuntimeAssert( gl3GraphicsDriverPlugin != nullptr );
	delete gl3GraphicsDriverPlugin;
}
