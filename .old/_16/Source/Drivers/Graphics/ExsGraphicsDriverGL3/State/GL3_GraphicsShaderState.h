
#pragma once

#ifndef __Exs_GraphicsDriver_GL3_GraphicsShaderState_H__
#define __Exs_GraphicsDriver_GL3_GraphicsShaderState_H__

#include "../GL3_Prerequisites.h"
#include <ExsCommonGL/Objects/GL_ShaderProgramObject.h>
#include <ExsCommonGL/State/GL_GraphicsShaderState.h>


namespace Exs
{


	struct GLShaderProgramDataBindingDesc
	{
		//
		const GraphicsShaderDataBinding* attribBindingArray = nullptr;

		//
		Size_t attribBindingsNum = 0;

		//
		const GraphicsShaderDataBinding* uniformBlockBindingArray = nullptr;

		//
		Size_t uniformBlockBindingsNum = 0;

		//
		const GraphicsShaderDataBinding* samplerBindingArray = nullptr;

		//
		Size_t samplerBindingsNum = 0;
	};


	class GL3GraphicsShaderStateDescriptor : public GLGraphicsShaderStateDescriptor
	{
		EXS_DECLARE_NONCOPYABLE(GL3GraphicsShaderStateDescriptor);

	private:
		std::unique_ptr<GLShaderProgramObject>    _glShaderProgramObject;

	public:
		//
		GL3GraphicsShaderStateDescriptor( RenderSystem* renderSystem,
		                                  RSBaseObjectID baseObjectID,
		                                  RSStateDescriptorPID descriptorPID,
		                                  const GraphicsShaderConfiguration& configuration,
		                                  const GLGraphicsShaderConfiguration& configurationGL,
		                                  std::unique_ptr<GLShaderProgramObject>&& shaderProgramObject)
		: GLGraphicsShaderStateDescriptor(renderSystem, baseObjectID, descriptorPID, configuration, configurationGL)
		, _glShaderProgramObject(std::move(shaderProgramObject))
		{ }


		const GLShaderProgramObject* GetGLShaderProgramObject() const
		{
			return this->_glShaderProgramObject.get();
		}
	};


	std::unique_ptr<GLShaderProgramObject> BuildGL3ShaderProgramObject( const GLGraphicsShaderConfiguration& graphicsShaderConfigGL,
	                                                                    const GLShaderProgramDataBindingDesc& dataBindingDesc);


};


#endif /* __Exs_GraphicsDriver_GL3_GraphicsShaderState_H__ */
