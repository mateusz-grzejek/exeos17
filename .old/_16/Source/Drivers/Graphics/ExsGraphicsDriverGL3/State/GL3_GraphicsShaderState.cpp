
#include "_Precompiled.h"
#include <ExsCommonGL/Objects/GL_ShaderProgramObject.h>
#include <ExsGraphicsDriverGL3/GL3_RenderSystem.h>
#include <ExsGraphicsDriverGL3/State/GL3_GraphicsShaderState.h>


namespace Exs
{


	std::unique_ptr<GLShaderProgramObject> BuildGL3ShaderProgramObject( const GLGraphicsShaderConfiguration& graphicsShaderConfigGL,
	                                                                    const GLShaderProgramDataBindingDesc& dataBindingDesc)
	{
		std::unique_ptr<GLShaderProgramObject> shaderProgramObject = std::make_unique<GLShaderProgramObject>();
		auto shaderHandleArray = BindArrayView(&(graphicsShaderConfigGL.handleArray[0]), Config::RS_CFG_Shader_Stages_Num_Graphics);

		for (auto& shaderHandle : shaderHandleArray)
		{
			if (shaderHandle == GL_Invalid_Handle)
				continue;

			shaderProgramObject->AttachShader(shaderHandle);
		}

		for (Size_t n = 0; n < dataBindingDesc.attribBindingsNum; ++n)
		{
			const auto& binding = dataBindingDesc.attribBindingArray[n];
			shaderProgramObject->SetAttribLocation(binding.baseName, binding.bindLocation);
		}

		if (!shaderProgramObject->Link())
			return nullptr;

		if (!shaderProgramObject->Validate())
			return nullptr;

		for (Size_t n = 0; n < dataBindingDesc.uniformBlockBindingsNum; ++n)
		{
			const auto& binding = dataBindingDesc.uniformBlockBindingArray[n];
			shaderProgramObject->SetUniformBlockBinding(binding.baseName, binding.bindLocation);
		}

		// Setting sampler requires
		shaderProgramObject->Bind();

		for (Size_t n = 0; n < dataBindingDesc.samplerBindingsNum; ++n)
		{
			const auto& binding = dataBindingDesc.samplerBindingArray[n];
			shaderProgramObject->SetSamplerSourceTextureUnit(binding.baseName, binding.bindLocation);
		}

		//
		GLShaderProgramObject::ClearBinding();

		return shaderProgramObject;
	}


}
