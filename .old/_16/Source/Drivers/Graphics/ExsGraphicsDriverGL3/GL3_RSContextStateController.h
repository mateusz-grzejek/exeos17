
#pragma once

#ifndef __Exs_GraphicsDriver_GL3_RSContextStateController_H__
#define __Exs_GraphicsDriver_GL3_RSContextStateController_H__

#include "GL3_Prerequisites.h"
#include <ExsCommonGL/GL_RSContextStateController.h>


namespace Exs
{


	class GLVertexStreamStateDescriptor;
	class GL3GraphicsShaderStateDescriptor;
	class GL3InputLayoutStateDescriptor;


	class GL3RSContextStateController : public GLRSContextStateController
	{
		friend class GL3RSContextCommandQueue;

	private:
		GLenum                       _primitiveTopology;
		const GLShaderProgramObject* _currentShaderProgramObject;

	public:
		GL3RSContextStateController(GL3RenderSystem* renderSystem, RSThreadState* rsThreadState, GLVertexArrayObjectCache* vertexArrayObjectCache);
		virtual ~GL3RSContextStateController();

		GLenum GetPrimitiveTopology() const;

	private:
		virtual bool ApplyInternalRenderState() override;

		void UpdateGraphicsShaderState(GL3GraphicsShaderStateDescriptor* graphicsShaderStateDescriptor);
		void UpdateVertexInputState(GL3InputLayoutStateDescriptor* inputLayoutStateDescriptor, GLVertexStreamStateDescriptor* vertexStreamStateDescriptor);
	};


	inline GLenum GL3RSContextStateController::GetPrimitiveTopology() const
	{
		return this->_primitiveTopology;
	}


};


#endif /* __Exs_GraphicsDriver_GL3_RSContextStateController_H__ */
