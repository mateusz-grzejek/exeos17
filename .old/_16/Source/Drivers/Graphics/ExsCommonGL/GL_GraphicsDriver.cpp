
#include "_Precompiled.h"
#include <ExsCommonGL/GL_GraphicsDriver.h>
#include <ExsCommonGL/GL_DebugOutput.h>
#include <ExsCommonGL/GL_Extensions.h>
#include <ExsCommonGL/GL_SystemLayer.h>
#include <ExsCore/System/SystemWindow.h>
#include <ExsRenderSystem/GraphicsDriverCapabilities.h>


namespace Exs
{


	GLAPIVersion TranslateFeatureLevelToGLAPIVersion(GraphicsDriverFeatureLevel GraphicsDriverFeatureLevel);


	GLGraphicsDriver::GLGraphicsDriver(const GraphicsDriverCapabilitiesDesc& capabilitiesDesc)
	: GraphicsDriver(capabilitiesDesc)
	{ }


	GLGraphicsDriver::~GLGraphicsDriver()
	{ }


	GraphicsDriverConfigInfo* GLGraphicsDriver::Configure(SystemWindow& targetWindow)
	{
		auto appGlobalState = targetWindow.GetAppGlobalState();
		ExsDebugAssert( appGlobalState );

		VisualConfig defaultVisualConfig =
		{
			VisualColorFormat::R8G8B8A8,
			VisualDepthStencilFormat::None,
			VisualMSAAConfig::Disabled
		};

		// Create temporary context which will be used for GL initialization. It is required on Win32 platforms and
		// even though it should NOT be necessary on X11, it is, because GLEW (which is one of possible options for
		// interface loading) requires this anyway.
		auto initContext = GLSystemLayer::CreateLegacyContext(targetWindow, defaultVisualConfig);

		if (!initContext)
			return nullptr;

		// Bind context as current for this thread.
		initContext->Bind();

		auto systemInfo = QuerySystemInfo();
		ExsTraceInfo(TRC_GraphicsDriver_GL, "OpenGL Info: %s, %s @ %s.",
			systemInfo.versionStr.c_str(), systemInfo.shadingLanguageVersionStr.c_str(), systemInfo.rendererName.c_str());

		if (!this->LoadInterface())
			return nullptr;

		SystemEnvData* systemEnvData = &(appGlobalState->systemEnvData);
		auto systemExtensionArray = GLSystemLayer::QuerySupportedSystemExtensions(initContext.get(), systemEnvData);
		this->_glExtensionManager = std::make_unique<GLExtensionManager>();
		this->_glExtensionManager->Initialize(systemExtensionArray);

		if (!this->CheckExtensionsSupport())
			return nullptr;

		auto configInfo = std::make_unique<GLGraphicsDriverConfigInfo>();
		configInfo->initContext = std::move(initContext);
		configInfo->capabilities = &(this->_capabilities);
		configInfo->targetWindow = &targetWindow;

		this->_configInfo = std::move(configInfo);
		return this->_configInfo.get();
	};


	Result GLGraphicsDriver::Initialize(const GraphicsDriverInitDesc& initDesc)
	{
		if (!this->_configInfo)
			return ExsResult(RSC_Err_Invalid_Operation);

		GLSystemCoreContextInitAttribs coreContextInitAttribs { };
		coreContextInitAttribs.flags = GLSystemContextInit_Default;
		coreContextInitAttribs.version = TranslateFeatureLevelToGLAPIVersion(initDesc.featureLevel);
		coreContextInitAttribs.visualConfig = initDesc.visualConfig;

		if (initDesc.flags.is_set(GraphicsDriverInit_Enable_Debug_Layer))
			coreContextInitAttribs.flags.set(GLSystemContextInit_Enable_Debug);

		if (initDesc.flags.is_set(GraphicsDriverInit_Force_Compatibility_Restriction))
			coreContextInitAttribs.flags.set(GLSystemContextInit_Forward_Compatible);

		GLGraphicsDriverConfigInfo* oglRenderConfigInfo = static_cast<GLGraphicsDriverConfigInfo*>(this->_configInfo.get());
		auto coreContext = GLSystemLayer::CreateCoreContext(*(oglRenderConfigInfo->targetWindow), coreContextInitAttribs);

		if (!coreContext)
			return ExsResultEx(RSC_Err_Failed, "Core context could not be created.");

		oglRenderConfigInfo->initContext->Unbind();
		oglRenderConfigInfo->initContext->Destroy();
		coreContext->Bind();

		this->_targetWindow = oglRenderConfigInfo->targetWindow;
		this->_glMainSystemContext = std::move(coreContext);
		this->_glDebugOutput = SetupDebugOutput(*(this->_glExtensionManager));
		this->_glSystemInfo = QuerySystemInfo();

		// Create RenderSystem and GraphicsDriver-specific (GL3/GL4/GLES3) RS objects.
		this->InitializeRSState();

		// Unbind context from the current thread.
		this->_glMainSystemContext->Unbind();

		// Acquire RSThreadState for the main rendering thread.
		this->AcquireMainThreadState();

		// Destroy ConfigInfo object. THis will also indicate, that GraphicsDriver has been initialized.
		this->_configInfo = nullptr;

		return ExsResult(RSC_Success);
	};


	std::vector<VisualMSAAConfig> GLGraphicsDriver::QuerySupportedMSAAConfigurations(const VisualConfig& visualConfig)
	{
		ExsDebugAssert( this->_configInfo );
		
		auto* oglGraphicsDriverConfigInfo = static_cast<GLGraphicsDriverConfigInfo*>(this->_configInfo.get());
		GLSystemContext* initContext = oglGraphicsDriverConfigInfo->initContext.get();
		SystemEnvData* systemEnvData = initContext->GetSystemEnvData();

		auto msaaConfigArray = GLSystemLayer::QuerySupportedMSAAConfigurations(initContext, systemEnvData, visualConfig);

		return msaaConfigArray;
	}
	
	
	std::vector<VideoModeConfig> GLGraphicsDriver::QuerySupportedVideoModes()
	{
		return std::vector<VideoModeConfig>();
	}


	Result GLGraphicsDriver::SetVideoMode(const VideoModeConfig& vmConfig, stdx::mask<VideoModeFlags> flags)
	{
		return ExsResult(RSC_Success);
	}


	void GLGraphicsDriver::Release()
	{
		this->ReleaseActiveObjects();
		this->ReleaseMainThreadState();
	}


	bool GLGraphicsDriver::CheckExtensionsSupport()
	{
		static const GLCoreExtensionID requiredExtensions[] =
		{
			GLCoreExtensionID::ARB_Create_Context,
			GLCoreExtensionID::ARB_Create_Context_Profile,
			GLCoreExtensionID::ARB_Framebuffer_Object,
			GLCoreExtensionID::EXT_Texture_Compression_S3TC,
		};

		for (auto& extensionID : requiredExtensions)
		{
			if (!this->_glExtensionManager->CheckSupport(extensionID))
				return false;
		}

		return true;
	}


	bool GLGraphicsDriver::LoadInterface()
	{
		GLenum glewInitResult = glewInit();
		if (glewInitResult != GLEW_NO_ERROR)
		{
			const GLubyte* errorStr = glewGetErrorString(glewInitResult);
			ExsTraceError(TRC_GraphicsDriver_GL, "Failed to initialize GLEW (error : %s).", reinterpret_cast<const char*>(errorStr));
			return false;
		}

		return true;
	}


	GLSystemInfo GLGraphicsDriver::QuerySystemInfo()
	{
		GLSystemInfo systemInfo { };
		systemInfo.versionNumber = GLStateQueries::GetVersionNumber();
		systemInfo.versionStr = GLStateQueries::GetVersionString();
		systemInfo.shadingLanguageVersionStr = GLStateQueries::GetShadingLanguageVersionString();
		systemInfo.vendorName = GLStateQueries::GetVendorName();
		systemInfo.rendererName = GLStateQueries::GetRendererName();

		return systemInfo;
	}


	std::unique_ptr<GLDebugOutput> GLGraphicsDriver::SetupDebugOutput(GLExtensionManager& extensionManager)
	{
	#if ( EXS_GL_FEATURE_DEBUG_OUTPUT )

		if (auto debugOutputInterface = GLDebugOutput::Initialize(extensionManager, GLDebugOutputVersion::KHR_Core))
		{
			GLDebugOutput::EnableDebugOutput(true);

			debugOutputInterface->EnableBreakOnEvent(true);
			debugOutputInterface->EnableEventsFiltering(false);
			debugOutputInterface->EnableSync(true);
			debugOutputInterface->ActivateCallback();

			return debugOutputInterface;
		}

	#endif

		return nullptr;
	}




	GLAPIVersion TranslateFeatureLevelToGLAPIVersion(GraphicsDriverFeatureLevel featureLevel)
	{
		switch (featureLevel)
		{
		case GraphicsDriverFeatureLevel::FL4D10G33SM4:
			return GLAPIVersion::GL_3_3;

		case GraphicsDriverFeatureLevel::FL5D11G42SM5:
			return GLAPIVersion::GL_4_2;

		case GraphicsDriverFeatureLevel::FL6D12G45SM5:
			return GLAPIVersion::GL_4_5;

		case GraphicsDriverFeatureLevel::Unspecified:
			return GLAPIVersion::Target;
		}

		return GLAPIVersion::Unknown;
	}

}
