
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_SystemLayer_H__
#define __Exs_GraphicsDriver_CommonGL_SystemLayer_H__

#include "GL_CommonSystemTypes.h"


namespace Exs
{


	class SystemWindow;


	///<summary>
	///</summary>
	enum GLSystemContextInitFlags : Enum
	{
		//
		GLSystemContextInit_Enable_Debug = 0x1000,

		//
		GLSystemContextInit_Forward_Compatible = 0x0040,

		//
		GLSystemContextInit_Default = 0
	};


	///<summary>
	///</summary>
	struct GLSystemCoreContextInitAttribs
	{
		//
		VisualConfig visualConfig;

		//
		GLAPIVersion version = GLAPIVersion::Target;

		//
		stdx::mask<GLSystemContextInitFlags> flags = GLSystemContextInit_Default;
	};


	class GLSystemContext
	{
		EXS_DECLARE_NONCOPYABLE(GLSystemContext);

	protected:
		SystemWindow*          _systemWindow;
		SystemEnvData*         _systemEnvData;
		GLSystemContextData    _contextData;

	public:
		GLSystemContext(SystemWindow* systemWindow, SystemEnvData* systemEnvData, const GLSystemContextData& contextData);
		virtual ~GLSystemContext();

		void Bind();
		void Unbind();
		void Destroy();
		void SwapBuffers();

		SystemWindow* GetSystemWindow() const;

		SystemEnvData* GetSystemEnvData() const;

		const GLSystemContextData& GetContextData() const;
	};


	inline SystemWindow* GLSystemContext::GetSystemWindow() const
	{
		return this->_systemWindow;
	}
	
	inline SystemEnvData* GLSystemContext::GetSystemEnvData() const
	{
		return this->_systemEnvData;
	}
	
	inline const GLSystemContextData& GLSystemContext::GetContextData() const
	{
		return this->_contextData;
	}


	namespace GLSystemLayer
	{
		std::unique_ptr<GLSystemContext> CreateLegacyContext(SystemWindow& systemWindow, const VisualConfig& visualConfig);

		std::unique_ptr<GLSystemContext> CreateCoreContext(SystemWindow& systemWindow, const GLSystemCoreContextInitAttribs& initAttribs);

		std::vector<std::string> QuerySupportedSystemExtensions(GLSystemContext* systemContext, SystemEnvData* systemEnvData);

		std::vector<VisualMSAAConfig> QuerySupportedMSAAConfigurations(GLSystemContext* systemContext, SystemEnvData* systemEnvData, const VisualConfig& visualConfig);
	};


}


#endif /* __Exs_GraphicsDriver_CommonGL_SystemLayer_H__ */
