
#include "_Precompiled.h"
#include <ExsCommonGL/GL_RSThreadState.h>
#include <ExsCommonGL/GL_RenderSystem.h>
#include <ExsCommonGL/GL_SystemLayer.h>


namespace Exs
{


	GLRSThreadState::GLRSThreadState( GLRenderSystem* renderSystem,
	                                  RSThreadManager* rsThreadManager,
	                                  RSThreadType rsThreadType,
	                                  RSThreadIndex rsThreadIndex,
	                                  GLSystemContext* oglSystemContext)
	: RSThreadState(renderSystem, rsThreadManager, rsThreadType, rsThreadIndex)
	, _glSystemContext(oglSystemContext)
	{
	}


	GLRSThreadState::~GLRSThreadState()
	{ }


	void GLRSThreadState::OnAttach()
	{
		// Bind OpenGL system context for the current thread.
		this->_glSystemContext->Bind();
	}


	void GLRSThreadState::OnDetach()
	{
		this->_glSystemContext->Unbind();
	}


}
