
#include "_Precompiled.h"
#include <ExsCommonGL/GL_Extensions.h>
#include <stdx/string_utils.h>


namespace Exs
{


	struct GLCoreExtensionNameMapping
	{
		GLCoreExtensionID    id;
		const char*          name;
	};


	static const GLCoreExtensionNameMapping extensionNameMappings[GL_Core_Extensions_Num] =
	{
	#if ( EXS_GL_PLATFORM_TYPE == EXS_GL_PLATFORM_TYPE_GL )
	  #if ( EXS_GL_SYSTEM_LAYER_TYPE == EXS_GL_SYSTEM_LAYER_TYPE_WGL )
		{ GLCoreExtensionID::ARB_Create_Context,           "WGL_ARB_create_context" },
		{ GLCoreExtensionID::ARB_Create_Context_Profile,   "WGL_ARB_create_context_profile" },
	  #elif ( EXS_GL_SYSTEM_LAYER_TYPE == EXS_GL_SYSTEM_LAYER_TYPE_GLX )
		{ GLCoreExtensionID::ARB_Create_Context,            "GLX_ARB_create_context" },
		{ GLCoreExtensionID::ARB_Create_Context_Profile,    "GLX_ARB_create_context_profile" },
	  #elif ( EXS_GL_SYSTEM_LAYER_TYPE == EXS_GL_SYSTEM_LAYER_TYPE_AGL )
		{ GLCoreExtensionID::ARB_Create_Context,            "AGL_ARB_create_context" },
		{ GLCoreExtensionID::ARB_Create_Context_Profile,    "AGL_ARB_create_context_profile" },
	  #endif
	#endif
		
		{ GLCoreExtensionID::ARB_Direct_State_Access,       "GL_ARB_direct_state_access" },
		{ GLCoreExtensionID::ARB_Framebuffer_Object,        "GL_ARB_framebuffer_object" },
		{ GLCoreExtensionID::ARB_Program_Interface_Query,   "GL_ARB_program_interface_query" },
		{ GLCoreExtensionID::ARB_Separate_Shader_Objects,   "GL_ARB_separate_shader_objects" },
		{ GLCoreExtensionID::ARB_Shader_Subroutine,         "GL_ARB_shader_subroutine" },
		{ GLCoreExtensionID::ARB_Vertex_Attrib_Binding,     "GL_ARB_vertex_attrib_binding" },

		{ GLCoreExtensionID::AMD_Debug_Output,              "GL_AMD_debug_output" },
		{ GLCoreExtensionID::ARB_Debug_Output,              "GL_ARB_debug_output" },
		{ GLCoreExtensionID::KHR_Debug,                     "GL_KHR_debug" },

		{ GLCoreExtensionID::EXT_Direct_State_Access,       "GL_EXT_direct_state_access" },
		{ GLCoreExtensionID::EXT_Texture_Compression_S3TC,  "GL_EXT_texture_compression_s3tc" },
	};




	GLExtensionManager::GLExtensionManager()
	{ }


	GLExtensionManager::~GLExtensionManager()
	{ }

	
	void GLExtensionManager::Initialize(const std::vector<std::string>& platformExtensionArray)
	{
		auto& extensionArray = this->_supportedExtensions;

		if (const char* glExtensionStr = reinterpret_cast<const char*>(glGetString(GL_EXTENSIONS)))
		{
			stdx::split_string(glExtensionStr, ' ',
				[&extensionArray](const char* str, size_t length) -> void {
					extensionArray.insert(std::string(str, length), true);
				});
		}

		if (!platformExtensionArray.empty())
		{
			for (auto& platformExtensionStr : platformExtensionArray)
			{
				extensionArray.insert(platformExtensionStr, true);
			}
		}

		for (Size_t coreExtensionNum = 0; coreExtensionNum < GL_Core_Extensions_Num; ++coreExtensionNum)
		{
			auto& coreExtension = extensionNameMappings[coreExtensionNum];
			GLCoreExtensionInfo coreExtensionInfo { coreExtension.id, coreExtension.name, false };

			if (extensionArray.find(coreExtension.name) != extensionArray.end())
				coreExtensionInfo.supportState = true;

			this->_coreExtensions.push_back(std::move(coreExtensionInfo));
		}
	}
	

	bool GLExtensionManager::CheckSupport(const char* extensionName) const
	{
		ExsDebugAssert( extensionName != nullptr );
		return this->_supportedExtensions.find(extensionName) != this->_supportedExtensions.end();
	}


	bool GLExtensionManager::CheckSupport(GLCoreExtensionID coreExtensionID) const
	{
		const GLCoreExtensionInfo& extensionInfo = this->GetCoreExtensionInfo(coreExtensionID);
		return extensionInfo.supportState;
	}


}
