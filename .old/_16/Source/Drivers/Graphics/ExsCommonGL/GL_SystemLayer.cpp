
#include "_Precompiled.h"
#include <ExsCommonGL/GL_SystemLayer.h>


namespace Exs
{


	GLSystemContext::GLSystemContext(SystemWindow* systemWindow, SystemEnvData* systemEnvData, const GLSystemContextData& contextData)
	: _systemWindow(systemWindow)
	, _systemEnvData(systemEnvData)
	, _contextData(contextData)
	{
		ExsTraceInfo(TRC_GraphicsDriver_GL, "GLSystemContext: Init (%p).", this);
	}


	GLSystemContext::~GLSystemContext()
	{
		ExsTraceInfo(TRC_GraphicsDriver_GL, "GLSystemContext: Release.");
	}


}
