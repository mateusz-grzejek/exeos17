
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_CommonSystemTypes_Win32_H__
#define __Exs_GraphicsDriver_CommonGL_CommonSystemTypes_Win32_H__


namespace Exs
{


	struct LinuxGLSystemContextDataGLX
	{
		// Handle of the OpenGL system context.
		GLXContext handle;
	};


}


#endif /* __Exs_GraphicsDriver_CommonGL_CommonSystemTypes_Win32_H__ */
