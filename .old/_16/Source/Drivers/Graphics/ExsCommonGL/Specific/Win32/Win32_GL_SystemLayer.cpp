
#include "_Precompiled.h"
#include <ExsCommonGL/GL_SystemLayer.h>
#include <ExsCore/PlatformUtils.h>
#include <ExsCore/System/SystemWindow.h>
#include <stdx/string_utils.h>


namespace Exs
{


	struct WGLPixelFormatInfo
	{
		int formatIndex;
		Uint16 msaaSamplesCount;
	};


	int Win32ChooseCorePixelFormat(HDC windowDC, const GLSystemCoreContextInitAttribs& initAttribs);
	bool Win32EnumeratePixelFormats(HDC windowDC, const VisualConfig& visualConfig, std::vector<int>* pixelFormatArray);



	
	void GLSystemContext::Bind()
	{
		ExsDebugAssert( this->_contextData.handle != nullptr );
		wglMakeCurrent(this->_contextData.windowDC, this->_contextData.handle);
	}


	void GLSystemContext::Unbind()
	{
		ExsDebugAssert( this->_contextData.handle != nullptr );
		wglMakeCurrent(nullptr, nullptr);
	}


	void GLSystemContext::Destroy()
	{
		if (this->_contextData.handle == nullptr)
			return;
		
		HGLRC currentContext = wglGetCurrentContext();

		if (this->_contextData.handle == currentContext)
			wglMakeCurrent(nullptr, nullptr);
		
		wglDeleteContext(this->_contextData.handle);
		::ReleaseDC(this->_contextData.window, this->_contextData.windowDC);
		
		this->_contextData.handle = nullptr;
		this->_contextData.window = nullptr;
		this->_contextData.windowDC = nullptr;
	}


	void GLSystemContext::SwapBuffers()
	{
		ExsDebugAssert( (this->_contextData.windowDC != nullptr) && (this->_contextData.handle != nullptr) );
		::SwapBuffers(this->_contextData.windowDC);
	}




	std::unique_ptr<GLSystemContext> GLSystemLayer::CreateLegacyContext(SystemWindow& systemWindow, const VisualConfig& visualConfig)
	{
		auto appGlobalState = systemWindow.GetAppGlobalState();
		auto* systemEnvData = &(appGlobalState->systemEnvData);

		HWND windowHandle = nullptr;
		HDC windowDC = ::GetDC(nullptr);

		if (!windowDC)
		{
			windowHandle = systemWindow.GetHandle();
			windowDC = ::GetDC(windowHandle);

			if (!windowDC)
			{
				ExsTraceError(TRC_GraphicsDriver_GL, "WGLSystemInitContext: failed to retrieve HDC for window (HWND = %p)", windowHandle);
				return nullptr;
			}
		}
		
		VisualColorDesc colorDesc = RSGetVisualColorDesc(visualConfig.colorFormat);
		VisualDepthStencilDesc depthStencilDesc = RSGetVisualDepthStencilDesc(visualConfig.depthStencilFormat);

		PIXELFORMATDESCRIPTOR pixelFormatDesc;
		ExsZeroMemory(&pixelFormatDesc, sizeof(PIXELFORMATDESCRIPTOR));
		pixelFormatDesc.nSize = sizeof(PIXELFORMATDESCRIPTOR);
		pixelFormatDesc.nVersion = 1;
		pixelFormatDesc.iLayerType = PFD_MAIN_PLANE;
		pixelFormatDesc.iPixelType = PFD_TYPE_RGBA;
		pixelFormatDesc.dwFlags = PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW | PFD_DOUBLEBUFFER;
		pixelFormatDesc.cRedBits = colorDesc.redBits;
		pixelFormatDesc.cGreenBits = colorDesc.greenBits;
		pixelFormatDesc.cBlueBits = colorDesc.blueBits;
		pixelFormatDesc.cAlphaBits = colorDesc.alphaBits;
		pixelFormatDesc.cDepthBits = depthStencilDesc.depthBufferSize;
		pixelFormatDesc.cStencilBits = depthStencilDesc.stencilBufferSize;
		
		int pixelFormatID = ::ChoosePixelFormat(windowDC, &pixelFormatDesc);
		BOOL spfResult = ::SetPixelFormat(windowDC, pixelFormatID, &pixelFormatDesc);
		
		if (spfResult == FALSE)
		{
			ExsTraceError(TRC_GraphicsDriver_GL, "WGLSystemInitContext: failed to initialize pixel format (returned id: %u)", pixelFormatID);
			return nullptr;
		}

		// Create OpenGL system context.
		HGLRC contextHandle = wglCreateContext(windowDC);

		if (contextHandle == nullptr)
		{
			ExsTraceError(TRC_GraphicsDriver_GL, "WGLSystemInitContext: failed to create GL context (wglCreateContext() has failed).");
			return nullptr;
		}
		
		GLSystemContextData contextData;
		contextData.window = windowHandle;
		contextData.windowDC = windowDC;
		contextData.handle = contextHandle;

		auto systemContextObject = std::make_unique<GLSystemContext>(&systemWindow, systemEnvData, contextData);
		return systemContextObject;
	}

	
	std::unique_ptr<GLSystemContext> GLSystemLayer::CreateCoreContext(SystemWindow& systemWindow, const GLSystemCoreContextInitAttribs& initAttribs)
	{
		auto appGlobalState = systemWindow.GetAppGlobalState();
		auto* systemEnvData = &(appGlobalState->systemEnvData);

		HWND windowHandle = systemWindow.GetHandle();
		HDC windowDC = ::GetWindowDC(windowHandle);

		if (!windowDC)
		{
			ExsTraceWarning(TRC_GraphicsDriver_GL, "WGLSystemCoreContext: failed to retrieve HDC for window (HWND = %p)", windowHandle);
			return nullptr;
		}

		int selectedPixelFormat = Win32ChooseCorePixelFormat(windowDC, initAttribs);

		if (selectedPixelFormat == -1)
		{
			ExsTraceError(TRC_GraphicsDriver_GL, "WGLSystemCoreContext: could not set core pixel format.");
			return nullptr;
		}

		GLVersionNumber versionNumber = initAttribs.version;
		stdx::mask<int> contextFlags = 0;

		if (initAttribs.flags.is_set(GLSystemContextInit_Enable_Debug))
			contextFlags.set(WGL_CONTEXT_DEBUG_BIT_ARB);

		if (initAttribs.flags.is_set(GLSystemContextInit_Forward_Compatible))
			contextFlags.set(WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB);

		const int contextAttribs[] =
		{
			WGL_CONTEXT_MAJOR_VERSION_ARB, versionNumber.major,
			WGL_CONTEXT_MINOR_VERSION_ARB, versionNumber.minor,
			WGL_CONTEXT_PROFILE_MASK_ARB,  WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
			WGL_CONTEXT_FLAGS_ARB,         contextFlags,
			0
		};

		HGLRC contextHandle = wglCreateContextAttribsARB(windowDC, nullptr, contextAttribs);
		if (!contextHandle)
		{
			ExsTraceWarning(TRC_GraphicsDriver_GL, "Failed to create <core> GL context (glXCreateContextAttribsARB() has failed).");
			return nullptr;
		}
		
		GLSystemContextData contextData;
		contextData.window = windowHandle;
		contextData.windowDC = windowDC;
		contextData.handle = contextHandle;

		auto systemContextObject = std::make_unique<GLSystemContext>(&systemWindow, systemEnvData, contextData);
		return systemContextObject;
	}

	
	std::vector<std::string> GLSystemLayer::QuerySupportedSystemExtensions(GLSystemContext* systemContext, SystemEnvData* systemEnvData)
	{
		const auto& contextData = systemContext->GetContextData();
		std::vector<std::string> extensionArray;

		if (const char* wglExtensionStr = wglGetExtensionsStringARB(contextData.windowDC))
		{
			stdx::split_string(wglExtensionStr, ' ',
				[&extensionArray](const char* str, size_t length) -> void {
					extensionArray.push_back(std::string(str, length));
				});
		}

		return extensionArray;
	}
	

	std::vector<VisualMSAAConfig> GLSystemLayer::QuerySupportedMSAAConfigurations(GLSystemContext* systemContext, SystemEnvData* systemEnvData, const VisualConfig& visualConfig)
	{
		const auto& contextData = systemContext->GetContextData();

		std::vector<int> pixelFormatArray { };
		std::vector<VisualMSAAConfig> msaaConfigArray { };

		// Get list of all pixel formats supported for specified visualConfig.
		if (!Win32EnumeratePixelFormats(contextData.windowDC, visualConfig, &pixelFormatArray))
		{
			ExsTraceError(TRC_GraphicsDriver_GL, "");
			return msaaConfigArray;
		}

		const Uint32 maxMSAALevel = RSGetVisualMSAAConfigSamplesNum(VisualMSAAConfig::Best);
		Uint32 msaaConfigurationsNum = 0;
		bool msaaSupportStateArray[maxMSAALevel + 1] = { false };

		for (auto& pixelFormatID : pixelFormatArray)
		{
			int samplesNumAttrib = WGL_SAMPLES_ARB;
			int samplesNumValue = -1;
			wglGetPixelFormatAttribivARB(contextData.windowDC, pixelFormatID, 0, 1, &samplesNumAttrib, &samplesNumValue);
			
			if ((samplesNumValue == 0) || (samplesNumValue > maxMSAALevel))
				continue;

			if (msaaSupportStateArray[samplesNumValue])
				continue;

			msaaSupportStateArray[samplesNumValue] = true;
			msaaConfigurationsNum += 1;
		}

		msaaConfigArray.clear();
		msaaConfigArray.reserve(msaaConfigurationsNum);

		for (Uint32 n = 1; n < maxMSAALevel; ++n)
		{
			if (msaaSupportStateArray[n])
			{
				VisualMSAAConfig msaaConfig = RSGetVisualMSAAConfigForSampleNum(n);
				msaaConfigArray.push_back(msaaConfig);
			}
		}

		if (msaaConfigArray.empty())
		{
			VisualMSAAConfig defaultMSAAConfig = RSGetVisualMSAAConfigForSampleNum(0);
			msaaConfigArray.push_back(defaultMSAAConfig);
		}

		return msaaConfigArray;
	}




	int Win32ChooseCorePixelFormat(HDC windowDC, const GLSystemCoreContextInitAttribs& initAttribs)
	{
		std::vector<int> pixelFormatArray;
		std::vector<WGLPixelFormatInfo> compatiblePixelFormatArray;

		if (!Win32EnumeratePixelFormats(windowDC, initAttribs.visualConfig, &pixelFormatArray))
			return -1;

		int formatAttribs[] =
		{
			WGL_STEREO_ARB,
			WGL_SAMPLES_ARB,
			WGL_RED_BITS_ARB,
			WGL_BLUE_BITS_ARB,
			WGL_GREEN_BITS_ARB,
			WGL_ALPHA_BITS_ARB,
			WGL_DEPTH_BITS_ARB,
			WGL_STENCIL_BITS_ARB
		};
		
		const UINT formatAttribsNum = ARRAYSIZE(formatAttribs);
		int formatAttribsValues[formatAttribsNum] = { -1 };

		for (auto& pixelFormat : pixelFormatArray)
		{
			wglGetPixelFormatAttribivARB(windowDC, pixelFormat, 0, formatAttribsNum, formatAttribs, formatAttribsValues);

			if (formatAttribsValues[0] == GL_TRUE) // Check stereo mode
				continue;

			WGLPixelFormatInfo pixelFormatInfo { };
			pixelFormatInfo.formatIndex = pixelFormat;
			pixelFormatInfo.msaaSamplesCount = static_cast<Uint16>(formatAttribsValues[1]);
			compatiblePixelFormatArray.push_back(pixelFormatInfo);
		}

		int selectedPixelFormat = -1;
		for (auto& pixelFormatInfo : compatiblePixelFormatArray)
		{
			int formatIndex = pixelFormatInfo.formatIndex;

			PIXELFORMATDESCRIPTOR pixelFormatDesc;
			ExsZeroMemory(&pixelFormatDesc, sizeof(PIXELFORMATDESCRIPTOR));
			::DescribePixelFormat(windowDC, formatIndex, sizeof(PIXELFORMATDESCRIPTOR), &pixelFormatDesc);

			if (::SetPixelFormat(windowDC, formatIndex, &pixelFormatDesc) == TRUE)
			{
				wglGetPixelFormatAttribivARB(windowDC, formatIndex, 0, formatAttribsNum, formatAttribs, formatAttribsValues);

				ExsTraceInfo(TRC_GraphicsDriver_GL, "Selected pixel format %u: R%uG%uB%uA%u, D%uS%u, MSAA x%u.",
					formatIndex, formatAttribsValues[2], formatAttribsValues[3], formatAttribsValues[4],
					formatAttribsValues[5], formatAttribsValues[6], formatAttribsValues[7], formatAttribsValues[1]);

				selectedPixelFormat = formatIndex;
				break;
			}
		}

		return selectedPixelFormat;
	}


	bool Win32EnumeratePixelFormats(HDC windowDC, const VisualConfig& visualConfig, std::vector<int>* pixelFormatArray)
	{
		VisualColorDesc colorDesc = RSGetVisualColorDesc(visualConfig.colorFormat);
		VisualDepthStencilDesc depthStencilDesc = RSGetVisualDepthStencilDesc(visualConfig.depthStencilFormat);
		VisualMSAADesc msaaDesc = RSGetVisualMSAADesc(visualConfig.msaaConfig);

		const UINT maxPixelFormatsNum = 256;
		INT corePixelFormats[maxPixelFormatsNum];
		UINT corePixelFormatsNum = 0;
		
		int formatAttribsValues[] =
		{
			WGL_SUPPORT_OPENGL_ARB,  GL_TRUE,
			WGL_DOUBLE_BUFFER_ARB,   GL_TRUE,
			WGL_DRAW_TO_WINDOW_ARB,  GL_TRUE,
			WGL_PIXEL_TYPE_ARB,      WGL_TYPE_RGBA_ARB,
			WGL_ACCELERATION_ARB,    WGL_FULL_ACCELERATION_ARB,
			WGL_RED_BITS_ARB,        colorDesc.redBits,
			WGL_GREEN_BITS_ARB,      colorDesc.greenBits,
			WGL_BLUE_BITS_ARB,       colorDesc.blueBits,
			WGL_ALPHA_BITS_ARB,      colorDesc.alphaBits,
			WGL_DEPTH_BITS_ARB,      depthStencilDesc.depthBufferSize,
			WGL_STENCIL_BITS_ARB,    depthStencilDesc.stencilBufferSize,
			WGL_SAMPLE_BUFFERS_ARB,  static_cast<int>(msaaDesc.count),
			WGL_SAMPLES_ARB,         static_cast<int>(msaaDesc.quality),
			0
		};

		ExsTraceInfo(TRC_GraphicsDriver_GL, "Requested pixel format configuration: R%uG%uB%uA%u, D%uS%u, MSAA x%u.",
			colorDesc.redBits, colorDesc.greenBits, colorDesc.blueBits, colorDesc.alphaBits,
			depthStencilDesc.depthBufferSize, depthStencilDesc.stencilBufferSize, msaaDesc.quality);

		if (!wglChoosePixelFormatARB(windowDC, formatAttribsValues, nullptr, maxPixelFormatsNum, corePixelFormats, &corePixelFormatsNum))
		{
			ExsTraceWarning(TRC_GraphicsDriver_GL, "Failed to enumerate pixel formats for requested configuration.");
			return false;
		}

		auto pixelFormatsArrayView = BindArrayView(corePixelFormats, corePixelFormatsNum);
		pixelFormatArray->reserve(corePixelFormatsNum);

		for (auto& pixelFormatIndex : pixelFormatsArrayView)
		{
			pixelFormatArray->push_back(pixelFormatIndex);
		}

		return true;
	}


}
