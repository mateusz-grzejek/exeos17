
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_CommonSystemTypes_Win32_H__
#define __Exs_GraphicsDriver_CommonGL_CommonSystemTypes_Win32_H__


namespace Exs
{


	struct Win32GLSystemContextData
	{
		// Win32 window the context is created for.
		HWND window;

		// DC (device context) of the window identified by 'window' handle.
		HDC windowDC;

		// Handle of the OpenGL system context.
		HGLRC handle;
	};


}


#endif /* __Exs_GraphicsDriver_CommonGL_CommonSystemTypes_Win32_H__ */
