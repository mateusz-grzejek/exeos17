
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_EGLSystemContext_Android_H__
#define __Exs_GraphicsDriver_CommonGL_EGLSystemContext_Android_H__

#include <ExsCore/Array.h>


namespace Exs
{


	class EGLSystemContext
	{
		EXS_DECLARE_NONCOPYABLE(EGLSystemContext);

	protected:
		EGLDisplay  _display;
		EGLContext  _handle;
		EGLSurface  _surface;

	public:
		EGLSystemContext();
		virtual ~EGLSystemContext();

		void Destroy();
		void SwapBuffers();

		EGLDisplay GetDisplay() const;
		EGLContext GetHandle() const;
		EGLSurface GetSurface() const;

		Array<String> QuerySupportedEGLExtensions() const;
		
		static EGLSystemContextRefPtr CreateForCurrentThread(const NativeWindow& window, const GLSystemContextInitAttribs& initAttribs);
		
	private:
		static EGLConfig ChooseFramebufferConfig(EGLDisplay display, const GLSystemContextInitAttribs& initAttribs);
		static bool EnumerateFramebufferConfigs(EGLDisplay display, Uint32 minMSAALevel, Array<EGLConfig>* outputArray);
	};


	inline EGLDisplay EGLSystemContext::GetDisplay() const
	{
		return this->_display;
	}

	inline EGLContext EGLSystemContext::GetHandle() const
	{
		return this->_handle;
	}

	inline EGLSurface EGLSystemContext::GetSurface() const
	{
		return this->_surface;
	}


}


#endif /* __Exs_GraphicsDriver_CommonGL_EGLSystemContext_Android_H__ */
