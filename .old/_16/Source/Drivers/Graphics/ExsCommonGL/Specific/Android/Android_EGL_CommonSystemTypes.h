
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_EGLCommonSystemTypes_Android_H__
#define __Exs_GraphicsDriver_CommonGL_EGLCommonSystemTypes_Android_H__


namespace Exs
{


	struct EGLDriverState
	{
		//
		EGLConfig config;

		//
		EGLDisplay display;

		//
		EGLSurface surface;

		//
		Version<EGLint> version;
	};


}


#endif /* __Exs_GraphicsDriver_CommonGL_EGLCommonSystemTypes_Android_H__ */
