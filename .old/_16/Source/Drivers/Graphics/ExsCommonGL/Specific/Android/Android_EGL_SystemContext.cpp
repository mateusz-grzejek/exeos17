
#include "_Precompiled.h"
#include "../../../SystemContext.h"
#include <ExsCore/NativeWindow.h>


namespace Exs
{


	struct EGLFramebufferConfigInfo
	{
		EGLConfig   config;
		Uint16      msaaSamplesCount;
	};




	EGLSystemContext::EGLSystemContext()
	: _display(EGL_NO_DISPLAY)
	, _handle(EGL_NO_CONTEXT)
	, _surface(EGL_NO_SURFACE)
	{ }


	EGLSystemContext::~EGLSystemContext()
	{
		ExsDebugAssert( this->_handle == nullptr );
	}


	void EGLSystemContext::Destroy()
	{
		if (this->_handle == EGL_NO_CONTEXT)
			return;

		EGLContext currentContext = eglGetCurrentContext();

		if (this->_handle == currentContext)
			eglMakeCurrent(this->_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
		
		eglDestroyContext(this->_display, this->_handle);
		eglDestroySurface(this->_display, this->_surface);
		eglTerminate(this->_display);
		
		this->_display = EGL_NO_DISPLAY;
		this->_handle = EGL_NO_CONTEXT;
		this->_surface = EGL_NO_SURFACE;
	}


	void EGLSystemContext::SwapBuffers()
	{
		ExsDebugAssert( this->_handle != EGL_NO_CONTEXT );
		eglSwapBuffers(this->_display, this->_surface);
	}


	EGLSystemContextRefPtr EGLSystemContext::CreateForCurrentThread(const NativeWindow& window, const GLSystemContextInitAttribs& initAttribs)
	{
		ANativeWindow* windowHandle = window.GetHandle();
		EGLDisplay defaultDisplay = eglGetDisplay(EGL_DEFAULT_DISPLAY);
		
		EGLint eglMajorVersion = -1;
		EGLint eglMinorVersion = -1;
		EGLBoolean initResult = eglInitialize(defaultDisplay, &eglMajorVersion, &eglMinorVersion);

		if (initResult == EGL_FALSE)
		{
			ExsEGLErrorCheck();
			return nullptr;
		}
	}


	EGLConfig EGLSystemContext::ChooseFramebufferConfig(EGLDisplay display, const GLSystemContextInitAttribs& initAttribs)
	{
		Array<EGLConfig> framebufferConfigs { };

		if (!EnumerateFramebufferConfigs(display, initAttribs.msaaSamplesCount, &framebufferConfigs))
			return nullptr;

		Array<EGLFramebufferConfigInfo> matchingConfigurations { };

		for (auto& framebufferConfig : framebufferConfigs)
		{
			EGLint msaaSamplesNum = -1;
			eglGetConfigAttrib(display, framebufferConfig, EGL_SAMPLES, &msaaSamplesNum);

			if (msaaSamplesNum < static_cast<int>(initAttribs.msaaSamplesCount))
				continue;

			EGLint configAttribs[] =
			{
				EGL_RED_SIZE,
				EGL_GREEN_SIZE,
				EGL_BLUE_SIZE,
				EGL_ALPHA_SIZE,
				EGL_DEPTH_SIZE,
				EGL_STENCIL_SIZE
			};
			
			const EGLint configAttribsNum = sizeof(configAttribs) / sizeof(configAttribs[0]);
			EGLint configAttribsValues[configAttribsNum] = { -1 };

			for (EGLint attribIndex = 0; attribIndex < configAttribsNum; ++attribIndex)
			{
				eglGetConfigAttrib(display, framebufferConfig, configAttribs[attribIndex], &(configAttribsValues[attribIndex]));
			}

			EGLFramebufferConfigInfo configInfo { };
			configInfo.config = framebufferConfig;
			configInfo.msaaSamplesCount = static_cast<Uint16>(msaaSamplesNum);
			matchingConfigurations.PushBack(configInfo);

			ExsTraceInfo(TRC_GraphicsDriver_GL, "Found matching pixel format 0x%p: R%uG%uB%uA%u / D%uS%u / MSAA x%u.",
				framebufferConfig, configAttribsValues[0], configAttribsValues[1], configAttribsValues[2],
				configAttribsValues[3], configAttribsValues[4], configAttribsValues[5], msaaSamplesNum);
		}

		EGLConfig selectedFramebufferConfig = nullptr;

		for (auto& configInfo : matchingConfigurations)
		{
			if ((configInfo.msaaSamplesCount > initAttribs.msaaSamplesCount) && initAttribs.msaaSamplesCountRestriction)
				continue;

			selectedFramebufferConfig = configInfo.config;
			break;
		}

		return selectedFramebufferConfig;
	}


	bool EGLSystemContext::EnumerateFramebufferConfigs(EGLDisplay display, Uint32 minMSAALevel, Array<EGLConfig>* outputArray)
	{
		const EGLint maxframebufferConfigsNum = 256;

		EGLConfig framebufferConfigs[maxframebufferConfigsNum];
		EGLint framebufferConfigsNum = 0;

		EGLint framebufferConfigAttribs[] =
		{
			EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
			EGL_COLOR_BUFFER_TYPE, EGL_RGB_BUFFER,
			EGL_RED_SIZE,        8,
			EGL_GREEN_SIZE,      8,
			EGL_BLUE_SIZE,       8,
			EGL_ALPHA_SIZE,      8,
			EGL_DEPTH_SIZE,      24,
			EGL_STENCIL_SIZE,    8,
			EGL_SAMPLE_BUFFERS,  (minMSAALevel == 0) ? 0 : 1,
			EGL_SAMPLES,         static_cast<int>(minMSAALevel),
			0
		};

		if (!eglChooseConfig(display, framebufferConfigAttribs, framebufferConfigs, maxframebufferConfigsNum, &framebufferConfigsNum))
		{
			ExsTraceError(TRC_GraphicsDriver_GL, "EGLSystemContext: failed to enumerate framebuffer configurations (eglChooseConfig() has failed).");
			return false;
		}

		auto framebufferConfigsArrayView = BindArrayView(framebufferConfigs, framebufferConfigsNum);
		outputArray->Reserve(framebufferConfigsNum);

		for (auto& framebufferConfig : framebufferConfigsArrayView)
		{
			outputArray->PushBack(framebufferConfig);
		}

		return true;
	}


	Array<String> EGLSystemContext::QuerySupportedEGLExtensions() const
	{
		// const char* wglExtensions = wglGetExtensionsStringARB(this->_windowDC);
		// 
		// auto extensionArray = StringUtils::Split< Array<String> >(
		// 	wglExtensions, " ",
		// 	[&](Array<String>& extArray, const Cstring<char>& extName) {
		// 		extArray.PushBack(extName);
		// });
		// 
		// return extensionArray;
	}


}
