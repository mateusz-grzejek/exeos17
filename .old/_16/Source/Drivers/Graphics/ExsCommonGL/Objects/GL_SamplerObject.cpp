
#include "_Precompiled.h"
#include <ExsCommonGL/Objects/GL_SamplerObject.h>
#include <ExsCommonGL/GL_ObjectsAllocator.h>


namespace Exs
{


	GLSamplerObject::GLSamplerObject()
	: GLObject(GLObjectType::Sampler)
	{
		this->_handle = GLObjectsAllocator::AllocateSampler();
	}


	GLSamplerObject::~GLSamplerObject()
	{
		if (this->_handle != GL_Invalid_Handle)
		{
			this->Release();
		}
	}


	bool GLSamplerObject::Release()
	{
		if (!GLObject::Release())
			return false;

		GLObjectsAllocator::DeallocateSampler(this->_handle);
		this->_handle = GL_Invalid_Handle;

		return true;
	}


	bool GLSamplerObject::ValidateHandle() const
	{
		GLboolean checkResult = glIsSampler(this->_handle);
		return checkResult == GL_TRUE;
	}


	void GLSamplerObject::SetWrapMode(GLenum wrapModeS, GLenum wrapModeT, GLenum wrapModeR)
	{
		ExsDebugAssert( this->CheckHandle() );
		
		glSamplerParameteri(this->_handle, GL_TEXTURE_WRAP_S, wrapModeS);
		ExsGLErrorCheck();
		
		glSamplerParameteri(this->_handle, GL_TEXTURE_WRAP_T, wrapModeT);
		ExsGLErrorCheck();
		
		glSamplerParameteri(this->_handle, GL_TEXTURE_WRAP_R, wrapModeR);
		ExsGLErrorCheck();
	}


	void GLSamplerObject::SetFilter(GLenum minFilter, GLenum magFilter)
	{
		ExsDebugAssert( this->CheckHandle() );
		
		glSamplerParameteri(this->_handle, GL_TEXTURE_MIN_FILTER, minFilter);
		ExsGLErrorCheck();

		glSamplerParameteri(this->_handle, GL_TEXTURE_MAG_FILTER, magFilter);
		ExsGLErrorCheck();
	}


	void GLSamplerObject::SetLODBias(float value)
	{
		ExsDebugAssert( this->CheckHandle() );

		glSamplerParameterf(this->_handle, GL_TEXTURE_LOD_BIAS, value);
		ExsGLErrorCheck();
	}


	void GLSamplerObject::SetLODRange(float min, float max)
	{
		ExsDebugAssert( this->CheckHandle() );

		glSamplerParameterf(this->_handle, GL_TEXTURE_MIN_LOD, min);
		ExsGLErrorCheck();

		glSamplerParameterf(this->_handle, GL_TEXTURE_MAX_LOD, max);
		ExsGLErrorCheck();
	}


	void GLSamplerObject::SetMaxAnisotropyLevel(float level)
	{
		ExsDebugAssert( this->CheckHandle() );

		glSamplerParameterf(this->_handle, GL_TEXTURE_MAX_ANISOTROPY_EXT, stdx::get_max_of(level, 1.0f));
		ExsGLErrorCheck();
	}


	void GLSamplerObject::SetCompareMode(GLboolean enable, GLenum func)
	{
		ExsDebugAssert( this->CheckHandle() );

		if (enable == GL_FALSE)
		{
			glSamplerParameteri(this->_handle, GL_TEXTURE_COMPARE_MODE, GL_NONE);
			ExsGLErrorCheck();
		}
		else
		{
			glSamplerParameteri(this->_handle, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
			ExsGLErrorCheck();
			
			glSamplerParameteri(this->_handle, GL_TEXTURE_COMPARE_FUNC, func);
			ExsGLErrorCheck();
		}
	}


	void GLSamplerObject::SetBorderColor(const Color& color)
	{
		ExsDebugAssert( this->CheckHandle() );

		GLfloat colorValues[4];
		colorValues[0] = static_cast<float>(color.rgba.red) / 255.0f;
		colorValues[1] = static_cast<float>(color.rgba.green) / 255.0f;
		colorValues[2] = static_cast<float>(color.rgba.blue) / 255.0f;
		colorValues[3] = static_cast<float>(color.rgba.alpha) / 255.0f;

		glSamplerParameterfv(this->_handle, GL_TEXTURE_BORDER_COLOR, colorValues);
		ExsGLErrorCheck();
	}


}
