
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_BufferObject_H__
#define __Exs_GraphicsDriver_CommonGL_BufferObject_H__

#include "../GL_Prerequisites.h"


namespace Exs
{


	struct GLBufferInitDesc
	{
		//
		GLenum bindTarget;

		//
		GLsizei size;

		//
		GLenum usageHint;
	};


	struct GLBufferData
	{
		const void* dataPtr;

		GLsizeiptr dataSize;
	};


	class GLBufferObject : public GLObject
	{
		EXS_DECLARE_NONCOPYABLE(GLBufferObject);

		friend class GLRenderSystem;

	protected:
		GLenum    _bindTarget;
		Size_t    _bufferSize;
		void*     _mappedMemory;

	public:
		GLBufferObject(GLenum bindTarget);
		GLBufferObject(GLBufferObject&& source);

		virtual ~GLBufferObject();

		GLBufferObject& operator=(GLBufferObject&& rhs);

		virtual bool Release() override;
		virtual bool ValidateHandle() const override;

		bool Initialize(const GLBufferInitDesc& initDesc, const GLBufferData& initData);

		void* Map(Uint offset, Size_t length, GLenum flags, bool restoreTargetBinding = false);
		void Unmap(bool restoreTargetBinding = false);
		void CopySubdata(Uint offset, Size_t size, const void* data, bool restoreTargetBinding = false);

		GLenum GetBindTarget() const;
		Size_t GetBufferSize() const;
		bool IsInitialized() const;

		void Swap(GLBufferObject& other);
	};


	inline GLenum GLBufferObject::GetBindTarget() const
	{
		return this->_bindTarget;
	}

	inline Size_t GLBufferObject::GetBufferSize() const
	{
		return this->_bufferSize;
	}

	inline bool GLBufferObject::IsInitialized() const
	{
		return this->_bufferSize > 0;
	}

	inline void GLBufferObject::Swap(GLBufferObject& other)
	{
		std::swap(this->_handle, other._handle);
		std::swap(this->_bindTarget, other._bindTarget);
		std::swap(this->_bufferSize, other._bufferSize);
		std::swap(this->_mappedMemory, other._mappedMemory);
	}


}


#endif /* __Exs_GraphicsDriver_CommonGL_BufferObject_H__ */
