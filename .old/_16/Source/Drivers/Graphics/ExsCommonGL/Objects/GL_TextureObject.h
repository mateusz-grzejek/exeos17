
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_TextureObject_H__
#define __Exs_GraphicsDriver_CommonGL_TextureObject_H__

#include "../GL_Prerequisites.h"
#include <ExsRenderSystem/Resource/TextureBase.h>


namespace Exs
{


	struct GLTextureInitDesc
	{
		//
		GLsizei width;

		//
		GLsizei height;

		//
		GLsizei depth;

		//
		GLsizei arraySize;

		//
		GLenum internalFormat;

		//
		GLenum pixelFormat;

		//
		GLenum pixelType;

		//
		GLuint mipmapLevelsNum;

		//
		GLsizei msaaSamplesNum;
	};


	struct GLTextureDataDesc
	{
		const void* dataPtr;

		GLsizei dataSize;

		GLenum pixelFormat;

		GLenum pixelType;

		GLint transferAlignment;
	};


	class GLTextureObject : public GLObject
	{
		EXS_DECLARE_NONCOPYABLE(GLTextureObject);
		
		friend class GLRenderSystem;

	private:
		GLenum  _bindTarget;
		GLenum  _internalFormat;

	public:
		GLTextureObject(GLenum bindTarget);
		GLTextureObject(GLTextureObject&& source);

		virtual ~GLTextureObject();

		GLTextureObject& operator=(GLTextureObject&& rhs);

		virtual bool Release() override;
		virtual bool ValidateHandle() const override;
		
		void UpdateSubdata2D(GLint xoff, GLint yoff, GLsizei width, GLsizei height, const GLTextureDataDesc& data);
		void UpdateSubdata2DArray(GLuint index, GLint xoff, GLint yoff, GLsizei width, GLsizei height, const GLTextureDataDesc& pixelData);
		void UpdateSubdata3D(GLint xoff, GLint yoff, GLint zoff, GLsizei width, GLsizei height, GLsizei depth, const GLTextureDataDesc& pixelData);
		void UpdateSubdataCubeMap(GLuint faceIndex, GLint xoff, GLint yoff, GLsizei width, GLsizei height, const GLTextureDataDesc& pixelData);
		
		void GenerateMipmapImages();

		GLenum GetBindTarget() const;
		GLenum GetInternalFormat() const;
		bool IsInitialized() const;

		void Swap(GLTextureObject& other);
		
		bool InitializeTexture2D(const GLTextureInitDesc& initDesc, const TextureData2DDesc& initData);
		bool InitializeTexture2DArray(const GLTextureInitDesc& initDesc, const TextureData2DArrayDesc& initData);
		bool InitializeTexture2DMultisample(const GLTextureInitDesc& initDesc);
		bool InitializeTexture3D(const GLTextureInitDesc& initDesc, const TextureData3DDesc& initData);
		bool InitializeTextureCubeMap(const GLTextureInitDesc& initDesc, const TextureDataCubeMapDesc& initData);

	private:
		static bool ValidateUnpackAlignment(GLint rowSize);
		static void RestoreDefaultUnpackAlignment();
	};
	

	inline GLenum GLTextureObject::GetBindTarget() const
	{
		return this->_bindTarget;
	}

	inline GLenum GLTextureObject::GetInternalFormat() const
	{
		return this->_internalFormat;
	}

	inline bool GLTextureObject::IsInitialized() const
	{
		return this->_internalFormat != 0;
	}

	inline void GLTextureObject::Swap(GLTextureObject& other)
	{
		std::swap(this->_handle, other._handle);
		std::swap(this->_bindTarget, other._bindTarget);
		std::swap(this->_internalFormat, other._internalFormat);
	}


}


#endif /* __Exs_GraphicsDriver_CommonGL_TextureObject_H__ */
