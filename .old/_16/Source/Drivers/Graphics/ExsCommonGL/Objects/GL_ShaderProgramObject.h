
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_ShaderProgramObject_H__
#define __Exs_GraphicsDriver_CommonGL_ShaderProgramObject_H__

#include "../GL_Prerequisites.h"


namespace Exs
{


	class GLShaderObject;


	enum : GLuint
	{
		GL_Invalid_Attrib_Location = stdx::limits<GLuint>::max_value
	};


	///<summary>
	///</summary>
	enum class GLShaderProgramParameter : Enum
	{
		//
		Delete_Status = 0xA841,

		//
		Link_Result = 0xA842,

		//
		Validation_Result = 0xA843
	};


	class GLShaderProgramObject : public GLObject
	{
		EXS_DECLARE_NONCOPYABLE(GLShaderProgramObject);

	public:
		GLShaderProgramObject();
		GLShaderProgramObject(GLShaderProgramObject&& source);

		virtual ~GLShaderProgramObject();

		GLShaderProgramObject& operator=(GLShaderProgramObject&& rhs);
		
		virtual bool Release() override;
		virtual bool ValidateHandle() const override;
		
		void AttachShader(GLuint shaderHandle);
		void AttachShader(const GLShaderObject& shader);

		void DetachAllShaders();
		void DetachShader(GLuint shaderHandle);
		void DetachShader(const GLShaderObject& shader);
		
	#if ( EXS_GL_FEATURE_PROGRAM_PIPELINE_OBJECT )
		void SetSeparableStagesSupport(bool enable);
	#endif

		void Bind();
		bool Link();
		bool Validate();

		void SetAttribLocation(const char* attribName, GLuint location);
		void SetSamplerSourceTextureUnit(const char* samplerName, GLuint texIndex);
		void SetUniformBlockBinding(const char* blockName, GLuint binding);
		
		GLint QueryStateParameter(GLShaderProgramParameter parameter) const;
		GLuint QueryVertexAttribLocation(const char* name) const;
		
		std::string GetInfoLog() const;
		Size_t GetAttachedShadersNum() const;
		Size_t GetInfoLogLength() const;
		
		bool HasAttachedShaders() const;
		bool IsInfoLogEmpty() const;

		void Swap(GLShaderProgramObject& other);

		static void ClearBinding();
	};
	

	inline GLShaderProgramObject& GLShaderProgramObject::operator=(GLShaderProgramObject&& rhs)
	{
		if (this != &rhs)
			GLShaderProgramObject(std::move(rhs)).Swap(*this);

		return *this;
	}

	inline bool GLShaderProgramObject::HasAttachedShaders() const
	{
		return this->GetAttachedShadersNum() > 0;
	}

	inline bool GLShaderProgramObject::IsInfoLogEmpty() const
	{
		return this->GetInfoLogLength() > 0;
	}

	inline void GLShaderProgramObject::Swap(GLShaderProgramObject& other)
	{
		std::swap(this->_handle, other._handle);
		std::swap(this->_objType, other._objType);
	}


}


#endif /* __Exs_GraphicsDriver_CommonGL_ShaderProgramObject_H__ */
