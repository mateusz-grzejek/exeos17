
#include "_Precompiled.h"
#include <ExsCommonGL/Objects/GL_TextureObject.h>
#include <ExsCommonGL/GL_ObjectsAllocator.h>
#include <ExsRenderSystem/Resource/TextureBase.h>


namespace Exs
{


	GLTextureObject::GLTextureObject(GLenum bindTarget)
	: GLObject(GLObjectType::Texture)
	, _bindTarget(bindTarget)
	, _internalFormat(0)
	{
		this->_handle = GLObjectsAllocator::AllocateTexture();
	}


	GLTextureObject::GLTextureObject(GLTextureObject&& source)
	: GLObject(GLObjectType::Texture)
	{
		this->Swap(source);
	}


	GLTextureObject::~GLTextureObject()
	{
		if (this->_handle != GL_Invalid_Handle)
		{
			this->Release();
		}
	}


	GLTextureObject& GLTextureObject::operator=(GLTextureObject&& rhs)
	{
		GLTextureObject(std::move(rhs)).Swap(*this);
		return *this;
	}


	bool GLTextureObject::Release()
	{
		if (!GLObject::Release())
			return false;

		GLObjectsAllocator::DeallocateTexture(this->_handle);
		this->_handle = GL_Invalid_Handle;

		return true;
	}


	bool GLTextureObject::ValidateHandle() const
	{
		GLboolean checkResult = glIsTexture(this->_handle);
		return checkResult == GL_TRUE;
	}


	void GLTextureObject::UpdateSubdata2D(GLint xoff, GLint yoff, GLsizei width, GLsizei height, const GLTextureDataDesc& data)
	{
		ExsDebugAssert( this->ValidateHandle() );
		ExsDebugAssert( this->_bindTarget == GL_TEXTURE_2D );
		
		bool restoreAlignment = ValidateUnpackAlignment(width);

		if (glTextureSubImage2D)
		{
			glTextureSubImage2D(this->_handle, 0, xoff, yoff, width, height, data.pixelFormat, data.pixelType, data.dataPtr);
			ExsGLErrorCheck();
		}
		else if (glTextureSubImage2DEXT)
		{
			glTextureSubImage2DEXT(this->_handle, this->_bindTarget, 0, xoff, yoff, width, height, data.pixelFormat, data.pixelType, data.dataPtr);
			ExsGLErrorCheck();
		}
		else
		{
			glBindTexture(this->_bindTarget, this->_handle);
			ExsGLErrorCheck();

			glTexSubImage2D(GL_TEXTURE_2D, 0, xoff, yoff, width, height, data.pixelFormat, data.pixelType, data.dataPtr);
			ExsGLErrorCheck();

			glBindTexture(this->_bindTarget, 0);
			ExsGLErrorCheck();
		}

		if (restoreAlignment)
			RestoreDefaultUnpackAlignment();
	}


	void GLTextureObject::UpdateSubdata2DArray(GLuint index, GLint xoff, GLint yoff, GLsizei width, GLsizei height, const GLTextureDataDesc& data)
	{
		ExsDebugAssert( this->ValidateHandle() );
		ExsDebugAssert( this->_bindTarget == GL_TEXTURE_2D_ARRAY );
		
		bool restoreAlignment = ValidateUnpackAlignment(width);

		if (glTextureSubImage3D)
		{
			glTextureSubImage3D(this->_handle, 0, xoff, yoff, index, width, height, 1, data.pixelFormat, data.pixelType, data.dataPtr);
			ExsGLErrorCheck();
		}
		else if (glTextureSubImage3DEXT)
		{
			glTextureSubImage3DEXT(this->_handle, this->_bindTarget, 0, xoff, yoff, index, width, height, 1, data.pixelFormat, data.pixelType, data.dataPtr);
			ExsGLErrorCheck();
		}
		else
		{
			glBindTexture(this->_bindTarget, this->_handle);
			ExsGLErrorCheck();

			glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, xoff, yoff, index, width, height, 1, data.pixelFormat, data.pixelType, data.dataPtr);
			ExsGLErrorCheck();

			glBindTexture(this->_bindTarget, 0);
			ExsGLErrorCheck();
		}

		if (restoreAlignment)
			RestoreDefaultUnpackAlignment();
	}


	void GLTextureObject::UpdateSubdata3D(GLint xoff, GLint yoff, GLint zoff, GLsizei width, GLsizei height, GLsizei depth, const GLTextureDataDesc& data)
	{
		ExsDebugAssert( this->ValidateHandle() );
		ExsDebugAssert( this->_bindTarget == GL_TEXTURE_3D );
		
		bool restoreAlignment = ValidateUnpackAlignment(width);

		if (glTextureSubImage3D)
		{
			glTextureSubImage3D(this->_handle, 0, xoff, yoff, zoff, width, height, depth, data.pixelFormat, data.pixelType, data.dataPtr);
			ExsGLErrorCheck();
		}
		else if (glTextureSubImage3DEXT)
		{
			glTextureSubImage3DEXT(this->_handle, this->_bindTarget, 0, xoff, yoff, zoff, width, height, depth, data.pixelFormat, data.pixelType, data.dataPtr);
			ExsGLErrorCheck();
		}
		else
		{
			glBindTexture(this->_bindTarget, this->_handle);
			ExsGLErrorCheck();

			glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, xoff, yoff, zoff, width, height, depth, data.pixelFormat, data.pixelType, data.dataPtr);
			ExsGLErrorCheck();

			glBindTexture(this->_bindTarget, 0);
			ExsGLErrorCheck();
		}

		if (restoreAlignment)
			RestoreDefaultUnpackAlignment();
	}


	void GLTextureObject::UpdateSubdataCubeMap(GLuint faceIndex, GLint xoff, GLint yoff, GLsizei width, GLsizei height, const GLTextureDataDesc& data)
	{
		ExsDebugAssert( this->ValidateHandle() );
		ExsDebugAssert( this->_bindTarget == GL_TEXTURE_2D );
		
		bool restoreAlignment = ValidateUnpackAlignment(width);

		//?

		if (restoreAlignment)
			RestoreDefaultUnpackAlignment();
	}


	void GLTextureObject::GenerateMipmapImages()
	{
		ExsDebugAssert( this->ValidateHandle() );

		if (glGenerateTextureMipmap)
		{
			glGenerateTextureMipmap(this->_handle);
			ExsGLErrorCheck();
		}
		else if (glGenerateTextureMipmapEXT)
		{
			glGenerateTextureMipmapEXT(this->_handle, this->_bindTarget);
			ExsGLErrorCheck();
		}
		else
		{
			glBindTexture(this->_bindTarget, this->_handle);
			ExsGLErrorCheck();

			glGenerateMipmap(this->_bindTarget);
			ExsGLErrorCheck();

			glBindTexture(this->_bindTarget, 0);
			ExsGLErrorCheck();
		}
	}
	
	
	bool GLTextureObject::InitializeTexture2D(const GLTextureInitDesc& initDesc, const TextureData2DDesc& initData)
	{
		ExsDebugAssert( !this->IsInitialized() );
		ExsDebugAssert( this->_bindTarget == GL_TEXTURE_2D );

		glBindTexture(GL_TEXTURE_2D, this->_handle);
		ExsGLErrorCheck();

		glTexImage2D(GL_TEXTURE_2D, 0, initDesc.internalFormat, initDesc.width, initDesc.height, 0, initDesc.pixelFormat, initDesc.pixelType, initData.dataPtr);
		ExsGLErrorCheck();

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
		ExsGLErrorCheck();
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, initDesc.mipmapLevelsNum - 1);
		ExsGLErrorCheck();

		glBindTexture(GL_TEXTURE_2D, 0);
		ExsGLErrorCheck();

		this->_internalFormat = initDesc.internalFormat;
		
		return true;
	}
	
	
	bool GLTextureObject::InitializeTexture2DArray(const GLTextureInitDesc& initDesc, const TextureData2DArrayDesc& initData)
	{
		ExsDebugAssert( !this->IsInitialized() );
		ExsDebugAssert( this->_bindTarget == GL_TEXTURE_2D_ARRAY );
		
		glBindTexture(GL_TEXTURE_2D_ARRAY, this->_handle);
		ExsGLErrorCheck();

		glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, initDesc.internalFormat, initDesc.width, initDesc.height, initDesc.arraySize, 0, initDesc.pixelFormat, initDesc.pixelType, nullptr);
		ExsGLErrorCheck();
		
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_BASE_LEVEL, 0);
		ExsGLErrorCheck();

		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAX_LEVEL, initDesc.mipmapLevelsNum - 1);
		ExsGLErrorCheck();

		for (Uint32 i = 0; i < initData.arraySize; ++i)
		{
			Uint32 layerIndex = initData.arrayIndex[i];
			const void* dataPtr = initData.arrayData[layerIndex].dataPtr;
			glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, layerIndex, initDesc.width, initDesc.height, 1, initDesc.pixelFormat, initDesc.pixelType, dataPtr);
			ExsGLErrorCheck();
		}

		glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
		ExsGLErrorCheck();

		this->_internalFormat = initDesc.internalFormat;
		
		return true;
	}
	
	
	bool GLTextureObject::InitializeTexture2DMultisample(const GLTextureInitDesc& initDesc)
	{
		ExsDebugAssert( !this->IsInitialized() );
		ExsDebugAssert( this->_bindTarget == GL_TEXTURE_2D_MULTISAMPLE );
		
	#if ( EXS_GL_FEATURE_MULTISAMPLE_TEXTURE )

		glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, this->_handle);
		ExsGLErrorCheck();

		glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, initDesc.msaaSamplesNum, initDesc.internalFormat, initDesc.width, initDesc.height, GL_FALSE);
		ExsGLErrorCheck();

		glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);
		ExsGLErrorCheck();

		this->_internalFormat = initDesc.internalFormat;
		
		return true;

	#else

		return false;

	#endif
	}
	
	
	bool GLTextureObject::InitializeTexture3D(const GLTextureInitDesc& initDesc, const TextureData3DDesc& initData)
	{
		ExsDebugAssert( !this->IsInitialized() );
		ExsDebugAssert( this->_bindTarget == GL_TEXTURE_3D );
		
		glBindTexture(GL_TEXTURE_3D, this->_handle);
		ExsGLErrorCheck();

		glTexImage3D(GL_TEXTURE_3D, 0, initDesc.internalFormat, initDesc.width, initDesc.height, initDesc.depth, 0, initDesc.pixelFormat, initDesc.pixelType, initData.dataPtr);
		ExsGLErrorCheck();
		
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_BASE_LEVEL, 0);
		ExsGLErrorCheck();

		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAX_LEVEL, initDesc.mipmapLevelsNum - 1);
		ExsGLErrorCheck();

		glBindTexture(GL_TEXTURE_3D, 0);
		ExsGLErrorCheck();

		this->_internalFormat = initDesc.internalFormat;
		
		return true;
	}
	
	
	bool GLTextureObject::InitializeTextureCubeMap(const GLTextureInitDesc& initDesc, const TextureDataCubeMapDesc& initData)
	{
		ExsDebugAssert( !this->IsInitialized() );
		ExsDebugAssert( this->_bindTarget == GL_TEXTURE_CUBE_MAP );
		
		glBindTexture(GL_TEXTURE_CUBE_MAP, this->_handle);
		ExsGLErrorCheck();
		
		for (Uint32 face = 0; face < 6; ++face)
		{
			const void* dataPtr = initData.faceData[face].dataPtr;
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + face, 0, initDesc.internalFormat, initDesc.width, initDesc.height, 0, initDesc.pixelFormat, initDesc.pixelType, dataPtr);
			ExsGLErrorCheck();
		}
		
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_BASE_LEVEL, 0);
		ExsGLErrorCheck();

		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAX_LEVEL, initDesc.mipmapLevelsNum - 1);
		ExsGLErrorCheck();

		glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
		ExsGLErrorCheck();

		this->_internalFormat = initDesc.internalFormat;
		
		return true;
	}


	bool GLTextureObject::ValidateUnpackAlignment(GLint rowSize)
	{
		GLint mod4rm = rowSize % 4;

		if (mod4rm == 0)
			return false;

		if (mod4rm == 2)
		{
			glPixelStorei(GL_UNPACK_ALIGNMENT, 2);
			ExsGLErrorCheck();
		}
		else
		{
			glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
			ExsGLErrorCheck();
		}

		return true;
	}


	void GLTextureObject::RestoreDefaultUnpackAlignment()
	{
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		ExsGLErrorCheck();
	}


}
