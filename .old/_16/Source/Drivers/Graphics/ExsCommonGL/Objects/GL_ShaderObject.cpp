
#include "_Precompiled.h"
#include <ExsCommonGL/Objects/GL_ShaderObject.h>
#include <ExsCommonGL/GL_ObjectsAllocator.h>
#include <stdx/memory_buffer.h>


namespace Exs
{


	GLShaderObject::GLShaderObject(GLenum shaderType)
	: GLObject(GLObjectType::Shader)
	, _shaderType(shaderType)
	{
		this->_handle = GLObjectsAllocator::AllocateShader(shaderType);
	}


	GLShaderObject::~GLShaderObject()
	{
		if (this->_handle != GL_Invalid_Handle)
		{
			this->Release();
		}
	}


	bool GLShaderObject::Release()
	{
		if (!GLObject::Release())
			return false;

		GLObjectsAllocator::DeallocateShader(this->_handle);
		this->_handle = GL_Invalid_Handle;

		return true;
	}


	bool GLShaderObject::ValidateHandle() const
	{
		GLboolean checkResult = glIsShader(this->_handle);
		return checkResult == GL_TRUE;
	}


	bool GLShaderObject::CompileSource(const Byte* source, Size_t length)
	{
		ExsDebugAssert( this->CheckHandle() );
		ExsDebugAssert( (source != nullptr) && (length > 0) );

		const GLchar* sourceBuffer = reinterpret_cast<const GLchar*>(source);
		const GLint sourceLength = truncate_cast<GLint>(length);

		glShaderSource(this->_handle, 1, &sourceBuffer, &sourceLength);
		ExsGLErrorCheck();

		glCompileShader(this->_handle);
		ExsGLErrorCheck();

		GLint compileStatus = 0;
		glGetShaderiv(this->_handle, GL_COMPILE_STATUS, &compileStatus);
		ExsGLErrorCheck();

		if (compileStatus != GL_TRUE)
		{
			std::string infoLog = this->GetInfoLog();
			ExsTraceWarning(TRC_GraphicsDriver_GL, "Compilation of shader %u has failed. Error: %s\n.", this->_handle, infoLog.c_str());
			return false;
		}

		return true;
	}


	GLint GLShaderObject::QueryStateParameter(GLShaderParameter parameter) const
	{
		ExsDebugAssert( this->CheckHandle() );

		GLint parameterValue = GL_Invalid_Value;

		switch(parameter)
		{
		case GLShaderParameter::Compilation_Result:
			glGetShaderiv(this->_handle, GL_COMPILE_STATUS, &parameterValue);
			break;
			
		case GLShaderParameter::Delete_Status:
			glGetShaderiv(this->_handle, GL_DELETE_STATUS, &parameterValue);
			break;
		}

		ExsGLErrorCheck();

		return parameterValue;
	}


	std::string GLShaderObject::GetInfoLog() const
	{
		ExsDebugAssert( this->CheckHandle() );
		
		std::string infoLog { };

		// Note: length returned by the GL includes null terminator!

		GLint infoLogLength = 0;
		glGetShaderiv(this->_handle, GL_INFO_LOG_LENGTH, &infoLogLength);
		ExsGLErrorCheck();

		if (infoLogLength > 0)
		{
			stdx::dynamic_memory_buffer<GLchar> infoLogBuffer(infoLogLength);
			glGetShaderInfoLog(this->_handle, infoLogLength, nullptr, infoLogBuffer.data_ptr());
			ExsGLErrorCheck();

			infoLog.assign(infoLogBuffer.data_ptr(), infoLogLength - 1);
		}

		return infoLog;
	}


	std::string GLShaderObject::GetSource() const
	{
		ExsDebugAssert( this->CheckHandle() );
		
		std::string source { };

		// Note: length returned by the GL includes null terminator!

		GLint sourceLength = 0;
		glGetShaderiv(this->_handle, GL_SHADER_SOURCE_LENGTH, &sourceLength);
		ExsGLErrorCheck();

		if (sourceLength > 0)
		{
			stdx::dynamic_memory_buffer<GLchar> sourceBuffer(sourceLength);
			glGetShaderSource(this->_handle, sourceLength, nullptr, sourceBuffer.data_ptr());
			ExsGLErrorCheck();
			
			source.assign(sourceBuffer.data_ptr(), sourceLength - 1);
		}

		return source;
	}


	Size_t GLShaderObject::GetInfoLogLength() const
	{
		ExsDebugAssert( this->CheckHandle() );

		GLint infoLogLength = 0;
		glGetShaderiv(this->_handle, GL_INFO_LOG_LENGTH, &infoLogLength);
		ExsGLErrorCheck();

		return infoLogLength;
	}


	Size_t GLShaderObject::GetSourceLength() const
	{
		ExsDebugAssert( this->CheckHandle() );

		GLint sourceLength = 0;
		glGetShaderiv(this->_handle, GL_SHADER_SOURCE_LENGTH, &sourceLength);
		ExsGLErrorCheck();

		return sourceLength;
	}


}
