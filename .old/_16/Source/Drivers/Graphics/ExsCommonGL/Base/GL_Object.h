
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_Object_H__
#define __Exs_GraphicsDriver_CommonGL_Object_H__


namespace Exs
{


	///<summary>
	/// Represents available types of OpenGL objects.
	///</summary>
	enum class GLObjectType : Enum
	{
		// Type for buffer objects (glGenBuffers).
		Buffer,

		// Type for framebuffer objects (glGenFamebuffers).
		Framebuffer,

		// Type for buffer objects (glGenProgramPipelines).
		Pipeline_State,

		// Type for buffer objects (glGenRenderbuffers).
		Renderbuffer,

		// Type for buffer objects (glGenSamplers).
		Sampler,

		// Type for buffer objects (glCreateShader).
		Shader,

		// Type for shader program objects (glCreateProgram).
		Shader_Program,

		// Type for texture objects (glGenTextures).
		Texture,

		// Type for transform feedback objects (glGenTransformFeedbacks).
		Transform_Feedback,

		// Type for vertex array objects (glGenVertexArrays).
		Vertex_Array
	};


	enum : GLuint
	{
		GL_Invalid_Handle = stdx::limits<GLuint>::max_value
	};


	class GLObject
	{
		EXS_DECLARE_NONCOPYABLE(GLObject);

	protected:
		GLObjectType    _objType; // Type of the object.
		GLuint          _handle; // OpenGL generated handle.

	protected:
		GLObject(GLObjectType type);
		virtual ~GLObject();

	public:
		///<summary>
		///</summary>
		virtual bool Release() = 0;

		///<summary>
		///</summary>
		virtual bool ValidateHandle() const = 0;

		///<summary>
		/// Returns OpenGL generated handle.
		///</summary>
		GLuint GetHandle() const;

		///<summary>
		/// Returns type of the internal OpenGL object.
		///</summary>
		GLObjectType GetObjectType() const;

		///<summary>
		///</summary>
		bool CheckHandle() const;
	};


	inline GLuint GLObject::GetHandle() const
	{
		return this->_handle;
	}

	inline GLObjectType GLObject::GetObjectType() const
	{
		return this->_objType;
	}

	inline bool GLObject::CheckHandle() const
	{
		return this->_handle != GL_Invalid_Handle;
	}


};


#endif /* __Exs_GraphicsDriver_CommonGL_Object_H__ */
