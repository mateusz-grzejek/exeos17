
#include "_Precompiled.h"


namespace Exs
{


	GLenum GLConstantMap::_TranslateBlendEquation(Enum blendEquation)
	{
		static const GLenum blendOperations[] =
		{
			GL_FUNC_ADD,
			GL_MIN,
			GL_MAX,
			GL_FUNC_SUBTRACT,
			GL_FUNC_REVERSE_SUBTRACT
		};

		return GetConstantArrayElement(blendOperations, blendEquation);
	}


	GLenum GLConstantMap::_TranslateBlendFactor(Enum blendFactor)
	{
		static const GLenum blendFactors[] =
		{
			GL_ZERO,
			GL_ONE,
			GL_CONSTANT_ALPHA,
			GL_CONSTANT_COLOR,
			GL_DST_ALPHA,
			GL_DST_COLOR,
			GL_SRC_ALPHA,
			GL_SRC_COLOR,
			GL_ONE_MINUS_DST_ALPHA,
			GL_ONE_MINUS_DST_COLOR,
			GL_ONE_MINUS_SRC_ALPHA,
			GL_ONE_MINUS_SRC_COLOR
		};

		return GetConstantArrayElement(blendFactors, blendFactor);
	}


	GLenum GLConstantMap::_TranslateBufferBindTarget(Enum bufferType)
	{
		static const GLenum bindTargets[] =
		{
			GL_UNIFORM_BUFFER,
			GL_ELEMENT_ARRAY_BUFFER,
			GL_ARRAY_BUFFER
		};

		return GetConstantArrayElement(bindTargets, bufferType);
	}


	GLenum GLConstantMap::_TranslateComparisonFunction(Enum function)
	{
		static const GLenum comparisonFunctions[] =
		{
			GL_ALWAYS,
			GL_EQUAL,
			GL_GREATER,
			GL_GEQUAL,
			GL_LESS,
			GL_LEQUAL,
			GL_NEVER,
			GL_NOTEQUAL
		};
		
		return GetConstantArrayElement(comparisonFunctions, function);
	}


	GLenum GLConstantMap::_TranslateCullMode(Enum cullMode)
	{
		static const GLenum cullModes[] =
		{
			GL_BACK,
			GL_FRONT,
			GL_NONE
		};

		return GetConstantArrayElement(cullModes, cullMode);
	}


	GLboolean GLConstantMap::_TranslateDepthWriteMask(Enum writeMask)
	{
		static const GLboolean writeMasks[] =
		{
			GL_FALSE,
			GL_TRUE
		};

		return GetConstantArrayElement(writeMasks, writeMask);
	}


	GLenum GLConstantMap::_TranslateFillMode(Enum fillMode)
	{
		static const GLenum fillModes[] =
		{
			GL_FILL,
			GL_LINE
		};

		return GetConstantArrayElement(fillModes, fillMode);
	}


	GLenum GLConstantMap::_TranslateGraphicDataBaseType(Enum baseType)
	{
		static const GLenum graphicDataBaseTypes[] =
		{
			GL_BYTE,
			GL_UNSIGNED_BYTE,
			GL_SHORT,
			GL_UNSIGNED_SHORT,
			GL_INT,
			GL_UNSIGNED_INT,
			GL_FLOAT,
			0
		};

		Uint8 baseTypeIndex = ExsEnumGetGraphicDataBaseTypeIndex(baseType);
		return GetConstantArrayElement(graphicDataBaseTypes, baseTypeIndex);
	}


	GLenum GLConstantMap::_TranslatePrimitiveTopology(Enum primitiveTopology)
	{
		static const GLenum primitiveTopologies[] =
		{
			GL_POINTS,
			GL_LINES,
			GL_LINES_ADJACENCY,
			GL_LINE_STRIP,
			GL_LINE_STRIP_ADJACENCY,
			GL_TRIANGLES,
			GL_TRIANGLES_ADJACENCY,
			GL_TRIANGLE_STRIP,
			GL_TRIANGLE_STRIP_ADJACENCY,
			GL_PATCHES
		};

		return GetConstantArrayElement(primitiveTopologies, primitiveTopology);
	}


	GLenum GLConstantMap::_TranslateShaderResourceBindTarget(Enum resType)
	{
		static const GLenum bindTargets[] =
		{
			GL_TEXTURE_BUFFER,
			GL_TEXTURE_2D,
			GL_TEXTURE_2D_ARRAY,
			GL_TEXTURE_2D_MULTISAMPLE,
			GL_TEXTURE_3D,
			GL_TEXTURE_CUBE_MAP
		};

		return GetConstantArrayElement(bindTargets, resType);
	}


	GLenum GLConstantMap::_TranslateShaderType(Enum shaderType)
	{
		static const GLenum shaderTypes[] =
		{
			GL_VERTEX_SHADER,
			GL_TESS_CONTROL_SHADER,
			GL_TESS_EVALUATION_SHADER,
			GL_GEOMETRY_SHADER,
			GL_FRAGMENT_SHADER
		};

		return GetConstantArrayElement(shaderTypes, shaderType);
	}


	GLenum GLConstantMap::_TranslateStencilOp(Enum stencilOp)
	{
		static const GLenum stencilOps[] =
		{
			GL_KEEP,
			GL_REPLACE,
			GL_INVERT,
			GL_ZERO,
			GL_INCR_WRAP,
			GL_INCR,
			GL_DECR_WRAP,
			GL_DECR
		};

		return GetConstantArrayElement(stencilOps, stencilOp);
	}


	GLenum GLConstantMap::_TranslateTextureAddressMode(Enum addressMode)
	{
		static const GLenum addressModes[] =
		{
			GL_CLAMP_TO_BORDER,
			GL_CLAMP_TO_EDGE,
			GL_MIRROR_CLAMP_TO_EDGE,
			GL_MIRRORED_REPEAT,
			GL_REPEAT
		};

		return GetConstantArrayElement(addressModes, addressMode);
	}


	GLenum GLConstantMap::_TranslateTextureInternalFormat(Enum format)
	{
		static const GLenum formats[] =
		{
			GL_R32F,
			GL_R32I,
			GL_R32UI,
			GL_RG32F,
			GL_RG32I,
			GL_RG32UI,
			GL_RGB32F,
			GL_RGB32I,
			GL_RGB32UI,
			GL_RGBA32F,
			GL_RGBA32I,
			GL_RGBA32UI,
			
			GL_R16F,
			GL_R16I,
			GL_R16_SNORM,
			GL_R16UI,
			GL_R16,
			GL_RG16F,
			GL_RG16I,
			GL_RG16_SNORM,
			GL_RG16UI,
			GL_RG16,
			GL_RGBA16F,
			GL_RGBA16I,
			GL_RGBA16_SNORM,
			GL_RGBA16UI,
			GL_RGBA16,
			
			GL_R8I,
			GL_R8_SNORM,
			GL_R8UI,
			GL_R8,
			GL_RG8I,
			GL_RG8_SNORM,
			GL_RG8UI,
			GL_RG8,
			GL_RGBA8I,
			GL_RGBA8_SNORM,
			GL_RGBA8UI,
			GL_RGBA8,

			GL_RGBA8_SNORM,
			GL_RGB8_SNORM,

			GL_SRGB8,
			GL_SRGB8_ALPHA8,

			GL_RGB5_A1,
			GL_RGB565,
			GL_RGB9_E5,
			GL_RGB10_A2UI,
			GL_RGB10_A2,
			GL_R11F_G11F_B10F,

			GL_DEPTH_COMPONENT16,
			GL_DEPTH24_STENCIL8,
			GL_DEPTH_COMPONENT32F,
			GL_DEPTH32F_STENCIL8,
			
			GL_R8
		};

		Uint8 formatIndex = ExsEnumGetGraphicDataFormatIndex(format);
		return GetConstantArrayElement(formats, formatIndex);
	}


	GLenum GLConstantMap::_TranslateTexturePixelFormat(Enum format)
	{
		static const GLenum pixelFormats[] =
		{
			GL_RED,
			GL_RED_INTEGER,
			GL_RED_INTEGER,
			GL_RG,
			GL_RG_INTEGER,
			GL_RG_INTEGER,
			GL_RGB,
			GL_RGB_INTEGER,
			GL_RGB_INTEGER,
			GL_RGBA,
			GL_RGBA_INTEGER,
			GL_RGBA_INTEGER,
			
			GL_RED,
			GL_RED_INTEGER,
			GL_RED,
			GL_RED_INTEGER,
			GL_RED,
			GL_RG,
			GL_RG_INTEGER,
			GL_RG,
			GL_RG_INTEGER,
			GL_RG,
			GL_RGBA,
			GL_RGBA_INTEGER,
			GL_RGBA,
			GL_RGBA_INTEGER,
			GL_RGBA,
			
			GL_RED_INTEGER,
			GL_RED,
			GL_RED_INTEGER,
			GL_RED,
			GL_RG_INTEGER,
			GL_RG,
			GL_RG_INTEGER,
			GL_RG,
			GL_RGBA_INTEGER,
			GL_RGBA,
			GL_RGBA_INTEGER,
			GL_RGBA,

			GL_BGRA,
			GL_BGR,

			GL_RGB,
			GL_RGBA,

			GL_RGBA,
			GL_RGB,
			GL_RGB,
			GL_RGBA_INTEGER,
			GL_RGBA,
			GL_RGB,

			GL_DEPTH_COMPONENT,
			GL_DEPTH_STENCIL,
			GL_DEPTH_COMPONENT,
			GL_DEPTH_STENCIL,

			GL_RED
		};

		Uint8 formatIndex = ExsEnumGetGraphicDataFormatIndex(format);
		return GetConstantArrayElement(pixelFormats, formatIndex);
	}


	GLenum GLConstantMap::_TranslateTexturePixelType(Enum format)
	{
		static const GLenum pixelTypes[] =
		{
			GL_FLOAT,
			GL_INT,
			GL_UNSIGNED_INT,
			GL_FLOAT,
			GL_INT,
			GL_UNSIGNED_INT,
			GL_FLOAT,
			GL_INT,
			GL_UNSIGNED_INT,
			GL_FLOAT,
			GL_INT,
			GL_UNSIGNED_INT,
			
			GL_FLOAT,
			GL_SHORT,
			GL_UNSIGNED_SHORT,
			GL_SHORT,
			GL_UNSIGNED_SHORT,
			GL_FLOAT,
			GL_SHORT,
			GL_UNSIGNED_SHORT,
			GL_SHORT,
			GL_UNSIGNED_SHORT,
			GL_FLOAT,
			GL_SHORT,
			GL_UNSIGNED_SHORT,
			GL_SHORT,
			GL_UNSIGNED_SHORT,
			
			GL_BYTE,
			GL_UNSIGNED_BYTE,
			GL_BYTE,
			GL_UNSIGNED_BYTE,
			GL_BYTE,
			GL_UNSIGNED_BYTE,
			GL_BYTE,
			GL_UNSIGNED_BYTE,
			GL_BYTE,          /* GL_UNSIGNED_INT_8_8_8_8 */
			GL_UNSIGNED_BYTE, /* GL_UNSIGNED_INT_8_8_8_8 */
			GL_BYTE,          /* GL_UNSIGNED_INT_8_8_8_8 */
			GL_UNSIGNED_BYTE, /* GL_UNSIGNED_INT_8_8_8_8 */

			GL_UNSIGNED_BYTE,
			GL_UNSIGNED_BYTE,

			GL_UNSIGNED_BYTE,
			GL_UNSIGNED_BYTE,

			GL_UNSIGNED_SHORT_5_5_5_1,
			GL_UNSIGNED_SHORT_5_6_5,
			GL_UNSIGNED_INT_5_9_9_9_REV,
			GL_UNSIGNED_INT_2_10_10_10_REV,
			GL_UNSIGNED_INT_2_10_10_10_REV,
			GL_UNSIGNED_INT_10F_11F_11F_REV,

			GL_HALF_FLOAT,
			GL_UNSIGNED_INT_24_8,
			GL_FLOAT,
			GL_FLOAT_32_UNSIGNED_INT_24_8_REV,
			
			GL_UNSIGNED_BYTE
		};

		Uint8 formatIndex = ExsEnumGetGraphicDataFormatIndex(format);
		return GetConstantArrayElement(pixelTypes, formatIndex);
	}


	GLenum GLConstantMap::_TranslateVerticesOrder(Enum order)
	{
		static const GLenum verticesOrders[] =
		{
			GL_CW,
			GL_CCW
		};

		return GetConstantArrayElement(verticesOrders, order);
	}


}
