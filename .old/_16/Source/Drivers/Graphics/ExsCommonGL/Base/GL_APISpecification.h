
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_APISpecification_H__
#define __Exs_GraphicsDriver_CommonGL_APISpecification_H__


#define EXS_GL_PROC

#if ( EXS_GL_PLATFORM_TYPE == EXS_GL_PLATFORM_TYPE_GL )
#  include "APISpec/GL.h"
#endif

#if ( EXS_GL_TARGET_VERSION == EXS_GL_TARGET_VERSION_GL3 )
#  include "APISpec/GL3.h"
#elif ( EXS_GL_TARGET_VERSION == EXS_GL_TARGET_VERSION_GL4 )
#  include "APISpec/GL4.h"
#elif ( EXS_GL_TARGET_VERSION == EXS_GL_TARGET_VERSION_GLES2 )
#  include "APISpec/GLES2.h"
#elif ( EXS_GL_TARGET_VERSION == EXS_GL_TARGET_VERSION_GLES3 )
#  include "APISpec/GLES3.h"
#endif

#if ( EXS_GL_SYSTEM_LAYER_TYPE == EXS_GL_SYSTEM_LAYER_TYPE_AGL )
#  include "APISpec/AGL.h"
#elif ( EXS_GL_SYSTEM_LAYER_TYPE == EXS_GL_SYSTEM_LAYER_TYPE_GLX )
#  include "APISpec/GLX.h"
#elif ( EXS_GL_SYSTEM_LAYER_TYPE == EXS_GL_SYSTEM_LAYER_TYPE_WGL )
#  include "APISpec/WGL.h"
#endif


#endif /* __Exs_GraphicsDriver_CommonGL_APISpecification_H__ */
