
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_APISpec_GL_H__
#define __Exs_GraphicsDriver_CommonGL_APISpec_GL_H__


namespace Exs
{

	
	// EXS_GL_PROC PFNGLACTIVETEXTUREPROC               glActiveTexture;
	// EXS_GL_PROC PFNGLBEGINQUERYPROC                  glBeginQuery;
	// EXS_GL_PROC PFNGLBEGINTRANSFORMFEEDBACKPROC      glBeginTransformFeedback;
	// EXS_GL_PROC PFNGLBINDATTRIBLOCATIONPROC          glBindAttribLocation;
	// EXS_GL_PROC PFNGLBINDBUFFERPROC                  glBindBuffer;
	// EXS_GL_PROC PFNGLBINDBUFFERBASEPROC              glBindBufferBase;
	// EXS_GL_PROC PFNGLBINDBUFFERRANGEPROC             glBindBufferRange;
	// EXS_GL_PROC PFNGLBINDFRAMEBUFFERPROC             glBindFramebuffer;
	// EXS_GL_PROC PFNGLBINDRENDERBUFFERPROC            glBindRenderbuffer;
	// EXS_GL_PROC PFNGLBINDSAMPLERPROC                 glBindSampler;
	// EXS_GL_PROC PFNGLBINDVERTEXARRAYPROC             glBindVertexArray;
	// EXS_GL_PROC PFNGLBLENDCOLORPROC                  glBlendColor;
	// EXS_GL_PROC PFNGLBLENDFUNCSEPARATEPROC           glBlendFuncSeparate;
	// EXS_GL_PROC PFNGLBUFFERDATAPROC                  glBufferData;
	// EXS_GL_PROC PFNGLBUFFERSTORAGEPROC               glBufferStorage;
	// EXS_GL_PROC PFNGLBUFFERSUBDATAPROC               glBufferSubData;
	// EXS_GL_PROC PFNGLCHECKFRAMEBUFFERSTATUSPROC      glCheckFramebufferStatus;
	// EXS_GL_PROC PFNGLCLEARPROC                       glClear;
	// EXS_GL_PROC PFNGLCLEARCOLORPROC                  glClearColor;
	// EXS_GL_PROC PFNGLCLEARDEPTHPROC                  glClearDepth;
	// EXS_GL_PROC PFNGLCLEARSTENCILPROC                glClearStencil;
	// EXS_GL_PROC PFNGLCOMPILESHADERPROC               glCompileShader;
	// EXS_GL_PROC PFNGLCREATEPROGRAMPROC               glCreateProgram;
	// EXS_GL_PROC PFNGLCREATESHADERPROC                glCreateShader;
	// EXS_GL_PROC PFNGLCULLFACEPROC                    glCullFace;
	// EXS_GL_PROC PFNGLDELETEBUFFERSPROC               glDeleteBuffers;
	// EXS_GL_PROC PFNGLDELETEFRAMEBUFFERSPROC          glDeleteFramebuffers;
	// EXS_GL_PROC PFNGLDELETEQUERIESPROC               glDeleteQueries;
	// EXS_GL_PROC PFNGLDELETERENDERBUFFERSPROC         glDeleteRenderbuffers;
	// EXS_GL_PROC PFNGLDELETESAMPLERSPROC              glDeleteSamplers;
	// EXS_GL_PROC PFNGLDELETESHADERPROC                glDeleteShader;
	// EXS_GL_PROC PFNGLDELETETEXTURESPROC              glDeleteTextures;
	// EXS_GL_PROC PFNGLDELETETRANSFORMFEEDBACKSPROC    glDeleteTransformFeedbacks;
	// EXS_GL_PROC PFNGLDELETEVERTEXARRAYSPROC          glDeleteVertexArrays;
	// EXS_GL_PROC PFNGLDEPTHFUNCPROC                   glDepthFunc;
	// EXS_GL_PROC PFNGLDEPTHMASKPROC                   glDepthMask;
	// EXS_GL_PROC PFNGLDEPTHRANGEPROC                  glDepthRange;
	// EXS_GL_PROC PFNGLDISABLEPROC                     glDisable;
	// EXS_GL_PROC PFNGLDISABLEVERTEXATTRIBARRAYPROC    glDisableVertexAttribArray;
	// EXS_GL_PROC PFNGLDRAWELEMENTSPROC                glDrawElements;
	// EXS_GL_PROC PFNGLDRAWELEMENTSBASEVERTEXPROC      glDrawElementsBaseVertex;
	// EXS_GL_PROC PFNGLDRAWRANGEELEMENTSPROC           glDrawRangeElements;
	// EXS_GL_PROC PFNGLDRAWRANGEELEMENTSBASEVERTEXPROC glDrawRangeElementsBaseVertex;
	// EXS_GL_PROC PFNGLDRAWTRANSFORMFEEDBACKPROC       glDrawTransformFeedback;
	// EXS_GL_PROC PFNGLENABLEPROC                      glEnable;
	// EXS_GL_PROC PFNGLENABLEVERTEXATTRIBARRAYPROC     glEnableVertexAttribArray;
	// EXS_GL_PROC PFNGLENDQUERYPROC                    glEndQuery;
	// EXS_GL_PROC PFNGLENDTRANSFORMFEEDBACKPROC        glEndTransformFeedback;
	// EXS_GL_PROC PFNGLFINISHPROC                      glFinish;
	// EXS_GL_PROC PFNGLFLUSHPROC                       glFlush;
	// EXS_GL_PROC PFNGLFLUSHMAPPEDBUFFERRANGEPROC      glFlushMappedBufferRange;
	// EXS_GL_PROC PFNGLFRAMEBUFFERPARAMETERIPROC       glFramebufferParameteri;
	// EXS_GL_PROC PFNGLFRAMEBUFFERRENDERBUFFERPROC     glFramebufferRenderbuffer;
	// EXS_GL_PROC PFNGLFRAMEBUFFERTEXTUREPROC          glFramebufferTexture;
	// EXS_GL_PROC PFNGLFRAMEBUFFERTEXTURE1DPROC        glFramebufferTexture1D;
	// EXS_GL_PROC PFNGLFRAMEBUFFERTEXTURE2DPROC        glFramebufferTexture2D;
	// EXS_GL_PROC PFNGLFRAMEBUFFERTEXTURE3DPROC        glFramebufferTexture3D;
	// EXS_GL_PROC PFNGLFRONTFACEPROC                   glFrontFace;
	// EXS_GL_PROC PFNGLGENBUFFERSPROC                  glGenBuffers;
	// EXS_GL_PROC PFNGLGENERATEMIPMAPPROC              glGenerateMipmap;
	// EXS_GL_PROC PFNGLGENFRAMEBUFFERSPROC             glGenFramebuffers;
	// EXS_GL_PROC PFNGLGENQUERIESPROC                  glGenQueries;
	// EXS_GL_PROC PFNGLGENRENDERBUFFERSPROC            glGenRenderbuffers;
	// EXS_GL_PROC PFNGLGENSAMPLERSPROC                 glGenSamplers;
	// EXS_GL_PROC PFNGLGENTEXTURESPROC                 glGenTextures;
	// EXS_GL_PROC PFNGLGENTRANSFORMFEEDBACKSPROC       glGenTransformFeedbacks;
	// EXS_GL_PROC PFNGLGENVERTEXARRAYSPROC             glGenVertexArrays;
	// EXS_GL_PROC PFNGLGETACTIVEATTRIBPROC             glGetActiveAttrib;
	// EXS_GL_PROC PFNGLGETACTIVEUNIFORMPROC            glGetActiveUniform;
	// EXS_GL_PROC PFNGLGETACTIVEUNIFORMBLOCKIVPROC     glGetActiveUniformBlockiv;
	// EXS_GL_PROC PFNGLGETACTIVEUNIFORMBLOCKNAMEPROC   glGetActiveUniformBlockName;
	// EXS_GL_PROC PFNGLGETATTACHEDSHADERSPROC          glGetAttachedShaders;
	// EXS_GL_PROC PFNGLGETATTRIBLOCATIONPROC           glGetAttribLocation;
	// EXS_GL_PROC PFNGLGETBUFFERPARAMETERIVPROC        glGetBufferParameteriv;
	// EXS_GL_PROC PFNGLGETBUFFERPARAMETERI64VPROC      glGetBufferParameteri64v;
	// EXS_GL_PROC PFNGLGETERRORPROC                    glGetError;
	// EXS_GL_PROC PFNGLGETINTEGERVPROC                 glGetIntegerv;
	// EXS_GL_PROC PFNGLGETSHADERIVPROC                 glGetShaderiv;
	// EXS_GL_PROC PFNGLGETSHADERINFOLOGPROC            glGetShaderInfoLog;
	// EXS_GL_PROC PFNGLGETSHADERIVPROC                 glGetShaderiv;
	// EXS_GL_PROC PFNGLGETSHADERSOURCEPROC             glGetShaderSource;
	// EXS_GL_PROC PFNGLGETUNIFORMFVPROC                glGetUniformfv;
	// EXS_GL_PROC PFNGLGETUNIFORMIVPROC                glGetUniformiv;
	// EXS_GL_PROC PFNGLGETUNIFORMLOCATIONPROC          glGetUniformLocation;


}


#endif /* __Exs_GraphicsDriver_CommonGL_APISpec_GL_H__ */
