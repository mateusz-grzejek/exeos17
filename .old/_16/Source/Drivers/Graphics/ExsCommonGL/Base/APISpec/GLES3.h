
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_APISpec_GLES3_H__
#define __Exs_GraphicsDriver_CommonGL_APISpec_GLES3_H__


#if !defined( APIENTRY)
#  define APIENTRY
#endif

#define GL_FILL 0
#define GL_LINE 0

#define GL_TESS_CONTROL_SHADER 0
#define GL_TESS_EVALUATION_SHADER 0
#define GL_GEOMETRY_SHADER 0

#define GL_TEXTURE_BUFFER 0

#define GL_TEXTURE_1D 0
#define GL_TEXTURE_1D_ARRAY 0
#define GL_TEXTURE_2D_MULTISAMPLE 0
#define GL_TEXTURE_2D_MULTISAMPLE_ARRAY 0
#define GL_TEXTURE_CUBE_MAP_ARRAY 0


#endif /* __Exs_GraphicsDriver_CommonGL_APISpec_GLES3_H__ */
