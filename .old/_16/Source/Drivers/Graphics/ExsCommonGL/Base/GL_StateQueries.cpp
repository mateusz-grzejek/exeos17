
#include "_Precompiled.h"
#include <ExsCommonGL/GL_Prerequisites.h>


namespace Exs
{


	GLVersionNumber GLStateQueries::GetVersionNumber()
	{
		GLint majorVersion = 0;
		glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);

		GLint minorVersion = 0;
		glGetIntegerv(GL_MINOR_VERSION, &minorVersion);

		return GLVersionNumber(majorVersion, minorVersion);
	}


	std::string GLStateQueries::GetVersionString()
	{
		const GLubyte* versionStr = glGetString(GL_VERSION);
		return std::string(reinterpret_cast<const GLchar*>(versionStr));
	}


	std::string GLStateQueries::GetShadingLanguageVersionString()
	{
		const GLubyte* versionStr = glGetString(GL_SHADING_LANGUAGE_VERSION);
		return std::string(reinterpret_cast<const GLchar*>(versionStr));
	}


	std::string GLStateQueries::GetRendererName()
	{
		const GLubyte* rendererName = glGetString(GL_RENDERER);
		return std::string(reinterpret_cast<const GLchar*>(rendererName));
	}


	std::string GLStateQueries::GetVendorName()
	{
		const GLubyte* vendorName = glGetString(GL_VENDOR);
		return std::string(reinterpret_cast<const GLchar*>(vendorName));
	}


}
