
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_StateQueries_H__
#define __Exs_GraphicsDriver_CommonGL_StateQueries_H__


namespace Exs
{


	///<summary>
	/// Interface for core OpenGL state/info querying.
	///</summary>
	class GLStateQueries
	{
	public:
		///<summary>
		/// Returns object containing current version of the OpenGL API.
		///</summary>
		static GLVersionNumber GetVersionNumber();

		///<summary>
		/// Returns string containing current version of the OpenGL API.
		///</summary>
		static std::string GetVersionString();

		///<summary>
		/// Returns string containing current version of the OpenGL Shading Language (GLSL).
		///</summary>
		static std::string GetShadingLanguageVersionString();

		///<summary>
		/// Returns string containing name of the renderer (usually - the GPU) used by the application.
		///</summary>
		static std::string GetRendererName();

		///<summary>
		/// Returns string containing name of the vendor.
		///</summary>
		static std::string GetVendorName();
	};


}


#endif /* __Exs_GraphicsDriver_CommonGL_StateQueries_H__ */
