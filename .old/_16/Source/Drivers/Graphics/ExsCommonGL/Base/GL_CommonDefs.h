
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_CommonDefs_H__
#define __Exs_GraphicsDriver_CommonGL_CommonDefs_H__


namespace Exs
{


	class GLRenderSystem;
	class GLSystemContext;


	enum : GLint
	{
		GL_Invalid_Value = stdx::limits<GLint>::max_value
	};


	enum : TraceCategoryID
	{
		TRC_GraphicsDriver_GL = EXS_GL_TARGET_VERSION
	};


	EXS_TRACE_SET_CATEGORY_NAME(TRC_GraphicsDriver_GL, "Renderer_" EXS_GL_TARGET_API_NAME);


}


#endif /* __Exs_GraphicsDriver_CommonGL_CommonDefs_H__ */
