
#include "_Precompiled.h"
#include <ExsCommonGL/GL_Prerequisites.h>


namespace Exs
{


	void GLErrorHandler::ClearErrorStack()
	{
		Size_t errorsNum = 0;

		while (true)
		{
			GLenum lastError = glGetError();

			if (lastError == GL_NO_ERROR)
				break;

			const char* errorStr = TranslateErrorCode(lastError);
			ExsTraceNotification(TRC_GraphicsDriver_GL, "GLErrorHandler: discarding error %u (%s).", lastError, errorStr);

			++errorsNum;
		}

		if (errorsNum == 0)
			ExsTraceNotification(TRC_GraphicsDriver_GL, "GLErrorHandler: nothing to clear, error stack is empty.");
		else
			ExsTraceNotification(TRC_GraphicsDriver_GL, "GLErrorHandler: %" EXS_PFU " error(s) were discarded.", errorsNum);
	}


	void GLErrorHandler::CheckLastError()
	{
		GLenum lastError = glGetError();

		if (lastError != GL_NO_ERROR)
		{
			const char* errorStr = TranslateErrorCode(lastError);
			ExsTraceError(TRC_GraphicsDriver_GL, "GLErrorHandler: GL error %u has been set (%s).", lastError, errorStr);
		}
	}


	bool GLErrorHandler::CheckLastError(GLenum errorCode)
	{
		GLenum lastError = glGetError();

		if (lastError != errorCode)
		{
			const char* checkedErrorStr = TranslateErrorCode(errorCode);
			const char* lastErrorStr = TranslateErrorCode(lastError);

			ExsTraceError(TRC_GraphicsDriver_GL, "GLErrorHandler: checked for %u (%s), but %u (%s) has been set instead.",
				errorCode, checkedErrorStr, lastError, lastErrorStr);

			return false;
		}

		return true;
	}


	const char* GLErrorHandler::TranslateErrorCode(GLenum errorCode)
	{
		switch(errorCode)
		{
	#if ( EXS_GL_PLATFORM_TYPE == EXS_GL_PLATFORM_TYPE_GL )
		ExsCaseReturnConstantString(GL_STACK_UNDERFLOW);
		ExsCaseReturnConstantString(GL_STACK_OVERFLOW);
	#endif
		ExsCaseReturnConstantString(GL_NO_ERROR);
		ExsCaseReturnConstantString(GL_INVALID_ENUM);
		ExsCaseReturnConstantString(GL_INVALID_VALUE);
		ExsCaseReturnConstantString(GL_INVALID_OPERATION);
		ExsCaseReturnConstantString(GL_INVALID_FRAMEBUFFER_OPERATION);
		ExsCaseReturnConstantString(GL_OUT_OF_MEMORY);

		}
		
		//This code should never be reached!
		ExsTraceError(TRC_GraphicsDriver_GL, "TranslateErrorCode: unknown GL error code was passed!");

		return "UNKNOWN_ERROR";
	}

	

	
#if ( EXS_GL_SYSTEM_LAYER_TYPE == EXS_GL_SYSTEM_LAYER_TYPE_EGL )

	void EGLErrorHandler::CheckLastError()
	{
		EGLint lastError = eglGetError();

		if (lastError != EGL_SUCCESS)
		{
			const char* errorStr = TranslateErrorCode(lastError);
			ExsTraceError(TRC_GraphicsDriver_GL, "EGLErrorHandler: EGL error %u has been set (%s).", lastError, errorStr);
		}
	}


	bool EGLErrorHandler::CheckLastError(EGLint errorCode)
	{
		EGLint lastError = eglGetError();

		if (lastError != errorCode)
		{
			const char* checkedErrorStr = TranslateErrorCode(errorCode);
			const char* lastErrorStr = TranslateErrorCode(lastError);

			ExsTraceError(TRC_GraphicsDriver_GL, "EGLErrorHandler: checked for %u (%s), but %u (%s) has been set instead.",
				errorCode, checkedErrorStr, lastError, lastErrorStr);

			return false;
		}

		return true;
	}


	const char* EGLErrorHandler::TranslateErrorCode(EGLint errorCode)
	{
		switch(errorCode)
		{
		ExsCaseReturnConstantString(EGL_NOT_INITIALIZED);
		ExsCaseReturnConstantString(EGL_BAD_ACCESS);
		ExsCaseReturnConstantString(EGL_BAD_ALLOC);
		ExsCaseReturnConstantString(EGL_BAD_ATTRIBUTE);
		ExsCaseReturnConstantString(EGL_BAD_CONFIG);
		ExsCaseReturnConstantString(EGL_BAD_CONTEXT);
		ExsCaseReturnConstantString(EGL_BAD_CURRENT_SURFACE);
		ExsCaseReturnConstantString(EGL_BAD_DISPLAY);
		ExsCaseReturnConstantString(EGL_BAD_MATCH);
		ExsCaseReturnConstantString(EGL_BAD_NATIVE_PIXMAP);
		ExsCaseReturnConstantString(EGL_BAD_NATIVE_WINDOW);
		ExsCaseReturnConstantString(EGL_BAD_PARAMETER);
		ExsCaseReturnConstantString(EGL_BAD_SURFACE);
		ExsCaseReturnConstantString(EGL_CONTEXT_LOST);
		}
		
		//This code should never be reached!
		ExsTraceError(TRC_GraphicsDriver_GL, "TranslateErrorCode: unknown EGL error code was passed!");

		return "UNKNOWN_ERROR";
	}

#endif


}
