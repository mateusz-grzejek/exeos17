
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_CommonTypes_H__
#define __Exs_GraphicsDriver_CommonGL_CommonTypes_H__


namespace Exs
{


	struct GLVersionNumber
	{
	public:
		GLint    major;
		GLint    minor;

	public:
		GLVersionNumber()
		: major(0)
		, minor(0)
		{ }

		GLVersionNumber(GLAPIVersion version)
		: major(ExsEnumGetGLVersionMajorNumber(version))
		, minor(ExsEnumGetGLVersionMinorNumber(version))
		{ }

		GLVersionNumber(GLint majorNum, GLint minorNum)
		: major(majorNum)
		, minor(minorNum)
		{ }

		GLVersionNumber& operator=(GLAPIVersion version)
		{
			this->major = ExsEnumGetGLVersionMajorNumber(version);
			this->minor = ExsEnumGetGLVersionMinorNumber(version);

			return *this;
		}
	};


	struct GLSystemInfo
	{
		GLVersionNumber versionNumber;
		std::string versionStr;
		std::string shadingLanguageVersionStr;
		std::string rendererName;
		std::string vendorName;
	};


}


#endif /* __Exs_GraphicsDriver_CommonGL_CommonTypes_H__ */
