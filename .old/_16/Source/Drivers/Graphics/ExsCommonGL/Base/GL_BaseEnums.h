
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_BaseEnums_H__
#define __Exs_GraphicsDriver_CommonGL_BaseEnums_H__


#define ExsEnumDeclareGLVersion(majorNumber, minorNumber) \
	(((static_cast<Uint16>(majorNumber) & 0xFF) << 8) | (static_cast<Uint16>(minorNumber) & 0xFF))

#define ExsEnumGetGLVersionMajorNumber(version) \
	static_cast<Uint8>((static_cast<Uint16>(version) >> 8) & 0xFF)

#define ExsEnumGetGLVersionMinorNumber(version) \
	static_cast<Uint8>(static_cast<Uint16>(version) & 0xFF)

#define ExsEnumIsGLVersionValidGL(major, minor) \
	(((major == 1) && (minor == 0)) || ((major == 3) && (minor <= 3)) || ((major == 4) && (minor <= 6)))

#define ExsEnumIsGLVersionValidGLES(major, minor) \
	(((major == 1) && (minor == 0)) || ((major == 2) && (minor == 0)) || ((major == 3) && (minor <= 1)))


namespace Exs
{


	enum class GLAPIVersion : Uint16
	{
		Unknown = ExsEnumDeclareGLVersion(0, 0),

	#if ( EXS_GL_PLATFORM_TYPE == EXS_GL_PLATFORM_TYPE_GL )

		GL_3_0 = ExsEnumDeclareGLVersion(3, 0),
		GL_3_1 = ExsEnumDeclareGLVersion(3, 1),
		GL_3_2 = ExsEnumDeclareGLVersion(3, 2),
		GL_3_3 = ExsEnumDeclareGLVersion(3, 3),
		GL_4_0 = ExsEnumDeclareGLVersion(4, 0),
		GL_4_1 = ExsEnumDeclareGLVersion(4, 1),
		GL_4_2 = ExsEnumDeclareGLVersion(4, 2),
		GL_4_3 = ExsEnumDeclareGLVersion(4, 3),
		GL_4_4 = ExsEnumDeclareGLVersion(4, 4),
		GL_4_5 = ExsEnumDeclareGLVersion(4, 5),

	  #if ( EXS_GL_TARGET_VERSION == EXS_GL_TARGET_VERSION_GL3 )
		Target = GL_3_3,
	  #elif ( EXS_GL_TARGET_VERSION == EXS_GL_TARGET_VERSION_GL4 )
		Target = GL_4_3,
	  #endif

	#elif ( EXS_GL_PLATFORM_TYPE == EXS_GL_PLATFORM_TYPE_GLES )

		GLES_1_0    = ExsEnumDeclareGLVersion(1, 0),
		GLES_2_0    = ExsEnumDeclareGLVersion(2, 0),
		GLES_3_0    = ExsEnumDeclareGLVersion(3, 0),
		GLES_3_1    = ExsEnumDeclareGLVersion(3, 1),

	  #if ( EXS_GL_TARGET_VERSION == EXS_GL_TARGET_VERSION_GLES2 )
		Target = GLES_2_0,
	  #elif ( EXS_GL_TARGET_VERSION == EXS_GL_TARGET_VERSION_GLES3 )
		Target = GLES_3_0,
	  #endif

	#endif

		Unspecified = stdx::limits<Uint16>::max_value
	};


}


#endif /* __Exs_GraphicsDriver_CommonGL_BaseEnums_H__ */
