
#include "_Precompiled.h"
#include <ExsCommonGL/GL_Prerequisites.h>


namespace Exs
{


	GLObject::GLObject(GLObjectType type)
	: _objType(type)
	, _handle(GL_Invalid_Handle)
	{ }


	GLObject::~GLObject()
	{ }


	bool GLObject::Release()
	{
		return this->_handle != GL_Invalid_Handle;
	}


	bool GLObject::ValidateHandle() const
	{
		return false;
	}


}
