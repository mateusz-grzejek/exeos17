
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_RSSwapChain_H__
#define __Exs_GraphicsDriver_CommonGL_RSSwapChain_H__

#include "GL_Prerequisites.h"
#include <ExsRenderSystem/RSSwapChain.h>


namespace Exs
{


	class GLRSSwapChain : public RSSwapChain
	{
	private:
		GLSystemContext*    _glCoreContext;

	public:
		GLRSSwapChain(RSDrawSurface* drawSurface, GLSystemContext* coreContext);
		virtual ~GLRSSwapChain();

		//
		virtual Result Present(Enum flags) override final;
	};


}



#endif /* __Exs_GraphicsDriver_CommonGL_RSSwapChain_H__ */
