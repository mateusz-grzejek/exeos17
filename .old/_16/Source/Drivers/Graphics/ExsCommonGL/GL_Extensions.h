
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_Extensions_H__
#define __Exs_GraphicsDriver_CommonGL_Extensions_H__

#include "GL_Prerequisites.h"
#include <stdx/assoc_array.h>


namespace Exs
{


	enum class GLCoreExtensionID : Enum
	{
	#if ( EXS_GL_PLATFORM_TYPE == EXS_GL_PLATFORM_TYPE_GL )
		ARB_Create_Context,
		ARB_Create_Context_Profile,
	#endif

		ARB_Direct_State_Access,
		ARB_Framebuffer_Object,
		ARB_Program_Interface_Query,
		ARB_Separate_Shader_Objects,
		ARB_Shader_Subroutine,
		ARB_Vertex_Attrib_Binding,

		AMD_Debug_Output,
		ARB_Debug_Output,
		KHR_Debug,

		EXT_Direct_State_Access,
		EXT_Texture_Compression_S3TC,

		Unknown
	};


	enum : Size_t
	{
		GL_Core_Extensions_Num = static_cast<Size_t>(GLCoreExtensionID::Unknown)
	};


	struct GLCoreExtensionInfo
	{
		GLCoreExtensionID    refID;
		const char*          name;
		bool                 supportState;
	};


	class GLExtensionManager
	{
	private:
		std::vector<GLCoreExtensionInfo>        _coreExtensions;
		stdx::assoc_array<std::string, bool>    _supportedExtensions;

	public:
		GLExtensionManager();
		~GLExtensionManager();

		void Initialize(const std::vector<std::string>& platformExtensionArray);
		
		bool CheckSupport(const char* extensionName) const;
		bool CheckSupport(GLCoreExtensionID coreExtensionID) const;

		const GLCoreExtensionInfo& GetCoreExtensionInfo(GLCoreExtensionID coreExtensionID) const;
		const std::vector<GLCoreExtensionInfo>& GetCoreExtensionList() const;
	};


	inline const GLCoreExtensionInfo& GLExtensionManager::GetCoreExtensionInfo(GLCoreExtensionID coreExtensionID) const
	{
		Size_t coreExtensionIndex = static_cast<Size_t>(coreExtensionID);
		const GLCoreExtensionInfo& coreExtensionInfo = this->_coreExtensions[coreExtensionIndex];
		
		return coreExtensionInfo;
	}

	inline const std::vector<GLCoreExtensionInfo>& GLExtensionManager::GetCoreExtensionList() const
	{
		return this->_coreExtensions;
	}


}


#endif /* __Exs_GraphicsDriver_CommonGL_Extensions_H__ */
