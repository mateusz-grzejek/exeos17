
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_Shader_H__
#define __Exs_GraphicsDriver_CommonGL_Shader_H__

#include "../Objects/GL_ShaderObject.h"
#include <ExsRenderSystem/Resource/ShaderBase.h>


namespace Exs
{


	class GLShaderInterface : public RSBaseObjectInterface
	{
	private:
		std::unique_ptr<GLShaderObject>  _glShaderObject;
		GLuint                           _glShaderObjectHandle;

	public:
		GLShaderInterface(GLShaderInterface&&) = default;
		GLShaderInterface(const GLShaderInterface&) = default;

		GLShaderInterface& operator=(GLShaderInterface&&) = default;
		GLShaderInterface& operator=(const GLShaderInterface&) = default;

		GLShaderInterface(std::unique_ptr<GLShaderObject>&& shaderObject)
		: RSBaseObjectInterface()
		, _glShaderObject(std::move(shaderObject))
		, _glShaderObjectHandle(_glShaderObject->GetHandle())
		{ }

		virtual ~GLShaderInterface()
		{ }

		GLShaderObject* GetGLShaderObject() const
		{
			return this->_glShaderObject.get();
		}

		GLuint GetGLShaderObjectHandle() const
		{
			return this->_glShaderObjectHandle;
		}

		bool IsInitialized() const
		{
			return this->_glShaderObject ? (this->_glShaderObjectHandle > 0) : false;
		}
	};


}


#endif /* __Exs_GraphicsDriver_CommonGL_Shader_H__ */
