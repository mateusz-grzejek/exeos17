
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_GLTextureInterface_H__
#define __Exs_GraphicsDriver_CommonGL_GLTextureInterface_H__

#include "../Objects/GL_TextureObject.h"
#include <ExsRenderSystem/Resource/TextureBase.h>


namespace Exs
{


	class GLTextureInterface : public RSBaseObjectInterface
	{
	private:
		std::unique_ptr<GLTextureObject>  _glTextureObject;
		GLuint                            _glTextureObjectHandle;

	public:
		GLTextureInterface(GLTextureInterface&&) = default;
		GLTextureInterface(const GLTextureInterface&) = default;

		GLTextureInterface& operator=(GLTextureInterface&&) = default;
		GLTextureInterface& operator=(const GLTextureInterface&) = default;

		GLTextureInterface(std::unique_ptr<GLTextureObject>&& bufferObject)
		: RSBaseObjectInterface()
		, _glTextureObject(std::move(bufferObject))
		, _glTextureObjectHandle(_glTextureObject->GetHandle())
		{ }

		virtual ~GLTextureInterface()
		{ }

		GLTextureObject* GetGLTextureObject() const
		{
			return this->_glTextureObject.get();
		}

		GLuint GetGLTextureObjectHandle() const
		{
			return this->_glTextureObjectHandle;
		}

		bool IsInitialized() const
		{
			return this->_glTextureObject ? (this->_glTextureObjectHandle > 0) : false;
		}
	};

	
	RSInternalHandle<GLTextureInterface> InitGLTextureInterface(TextureType textureType);


}


#endif /* __Exs_GraphicsDriver_CommonGL_GLTextureInterface_H__ */
