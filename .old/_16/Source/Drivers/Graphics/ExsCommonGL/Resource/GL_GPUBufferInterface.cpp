
#include "_Precompiled.h"
#include <ExsCommonGL/Resource/GL_GPUBufferInterface.h>


namespace Exs
{


	RSInternalHandle<GLGPUBufferInterface> InitGLGPUBufferInterface(GPUBufferType bufferType)
	{
		switch(bufferType)
		{
		case GPUBufferType::Constant_Buffer:
			{
				auto bufferObject = std::make_unique<GLBufferObject>(GL_UNIFORM_BUFFER);
				return CreateRSBaseObjectInterface<GLGPUBufferInterface>(std::move(bufferObject));
			}
			
		case GPUBufferType::Index_Buffer:
			{
				auto bufferObject = std::make_unique<GLBufferObject>(GL_ELEMENT_ARRAY_BUFFER);
				return CreateRSBaseObjectInterface<GLGPUBufferInterface>(std::move(bufferObject));
			}
			
		case GPUBufferType::Vertex_Buffer:
			{
				auto bufferObject = std::make_unique<GLBufferObject>(GL_ARRAY_BUFFER);
				return CreateRSBaseObjectInterface<GLGPUBufferInterface>(std::move(bufferObject));
			}
		}

		return nullptr;
	}


}
