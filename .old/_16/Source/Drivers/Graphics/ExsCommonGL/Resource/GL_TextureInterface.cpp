
#include "_Precompiled.h"
#include <ExsCommonGL/Resource/GL_TextureInterface.h>


namespace Exs
{


	RSInternalHandle<GLTextureInterface> InitGLTextureInterface(TextureType textureType)
	{
		switch(textureType)
		{
		case TextureType::Tex_2D:
			{
				auto textureObject = std::make_unique<GLTextureObject>(GL_TEXTURE_2D);
				return CreateRSBaseObjectInterface<GLTextureInterface>(std::move(textureObject));
			}
			
		case TextureType::Tex_2D_Array:
			{
				auto textureObject = std::make_unique<GLTextureObject>(GL_TEXTURE_2D_ARRAY);
				return CreateRSBaseObjectInterface<GLTextureInterface>(std::move(textureObject));
			}
			
		case TextureType::Tex_2D_Multisample:
			{
				auto textureObject = std::make_unique<GLTextureObject>(GL_TEXTURE_2D_MULTISAMPLE);
				return CreateRSBaseObjectInterface<GLTextureInterface>(std::move(textureObject));
			}
			
		case TextureType::Tex_3D:
			{
				auto textureObject = std::make_unique<GLTextureObject>(GL_TEXTURE_3D);
				return CreateRSBaseObjectInterface<GLTextureInterface>(std::move(textureObject));
			}
			
		case TextureType::Tex_Cube_Map:
			{
				auto textureObject = std::make_unique<GLTextureObject>(GL_TEXTURE_CUBE_MAP);
				return CreateRSBaseObjectInterface<GLTextureInterface>(std::move(textureObject));
			}
		}

		return nullptr;
	}


}
