
#include "_Precompiled.h"
#include <ExsCommonGL/Resource/GL_GPUBufferStorage.h>


namespace Exs
{


	GLenum SelectGLBufferMapMode(RSResourceMapMode mapMode, RSMemoryAccess memoryAccess);


	GLGPUBufferStorage::GLGPUBufferStorage(const RSResourceMemoryInfo& memoryInfo, GLBufferObject* bufferObject)
	: GPUBufferStorage(memoryInfo, bufferObject->GetBufferSize())
	, _glBufferObject(bufferObject)
	, _glBufferObjectHandle(bufferObject->GetHandle())
	{ }


	GLGPUBufferStorage::~GLGPUBufferStorage()
	{ }


	bool GLGPUBufferStorage::MapMemory(RSResourceMapMode mapMode, RSMappedMemoryInfo* mappedMemoryInfo)
	{
		ExsDebugAssert( this->IsInitialized() );
		ExsDebugAssert( !this->IsMapped() );

		GLenum mapFlags = SelectGLBufferMapMode(mapMode, this->_memoryInfo.access);

		// GL_Invalid_Value is returned if map mode contains flags, that cannot be used due to missing
		// access rights set during initialization process.

		if (mapFlags != GL_Invalid_Value)
		{
			if (void* mappedMemoryPtr = this->_glBufferObject->Map(0, this->_size, mapFlags))
			{
				this->_mappedMemoryInfo.ptr = pointer_cast<Byte*>(mappedMemoryPtr);
				this->_mappedMemoryInfo.range.offset = 0;
				this->_mappedMemoryInfo.range.length = this->_size;
				this->_mappedMemoryInfo.access = static_cast<Enum>(mapMode) & (AccessMode_Read | AccessMode_Write);

				if (mappedMemoryInfo != nullptr)
					*mappedMemoryInfo = this->_mappedMemoryInfo;

				return true;
			}
		}

		return false;
	}


	bool GLGPUBufferStorage::MapMemory(const RSMemoryRange& range, RSResourceMapMode mapMode, RSMappedMemoryInfo* mappedMemoryInfo)
	{
		ExsDebugAssert( this->IsInitialized() );
		ExsDebugAssert( !this->IsMapped() );

		if (range.length == 0)
			return false;

		if ((range.offset >= this->_size) || (range.offset + range.length > this->_size))
			return false;

		GLenum mapFlags = SelectGLBufferMapMode(mapMode, this->_memoryInfo.access);

		// GL_Invalid_Value is returned if map mode contains flags, that cannot be used due to missing
		// access rights set during initialization process.

		if (mapFlags != GL_Invalid_Value)
		{
			if (void* mappedMemoryPtr = this->_glBufferObject->Map(range.offset, range.length, mapFlags))
			{
				this->_mappedMemoryInfo.ptr = pointer_cast<Byte*>(mappedMemoryPtr);
				this->_mappedMemoryInfo.range.offset = range.offset;
				this->_mappedMemoryInfo.range.length = range.length;
				this->_mappedMemoryInfo.access = static_cast<Enum>(mapMode) & (AccessMode_Read | AccessMode_Write);
				
				if (mappedMemoryInfo != nullptr)
					*mappedMemoryInfo = this->_mappedMemoryInfo;

				return true;
			}
		}

		return false;
	}


	void GLGPUBufferStorage::UnmapMemory()
	{
		ExsDebugAssert( this->IsInitialized() );
		ExsDebugAssert( this->IsMapped() );

		this->_glBufferObject->Unmap();
		this->_mappedMemoryInfo.Reset();
	}


	void GLGPUBufferStorage::CopySubdata(Uint offset, Size_t length, const void* data, Size_t dataSize)
	{
		if ((length == 0) || (offset >= this->_size))
			return;

		Size_t maxUpdateSize = stdx::get_min_of(this->_size - offset, length);
		Size_t updateSize = stdx::get_min_of(maxUpdateSize, dataSize);
		this->_glBufferObject->CopySubdata(offset, updateSize, data);
	}


	void GLGPUBufferStorage::FlushMappedMemory()
	{
	}


	void GLGPUBufferStorage::FlushMappedRange(const RSMemoryRange& range)
	{
	}

	


	RSInternalHandle<GLGPUBufferStorage> InitGLGPUBufferStorage(GLGPUBufferInterface* interface, const GPUBufferStorageInitDesc& initDesc, const GPUBufferDataDesc& initData)
	{
		stdx::mask<Enum> memoryAccessMask = static_cast<Enum>(initDesc.memoryInfo.access);
		stdx::mask<Enum> usageFlagsMask = static_cast<Enum>(initDesc.usageFlags);

		auto* bufferObject = interface->GetGLBufferObject();

		GLBufferInitDesc bufferInitDesc { };
		bufferInitDesc.bindTarget = GLConstantMap::GetBufferBindTarget(initDesc.bufferType);
		bufferInitDesc.usageHint = GLUtils::ChooseBufferUsageMode(bufferInitDesc.bindTarget, usageFlagsMask);
		bufferInitDesc.size = static_cast<GLsizei>(initDesc.bufferSize);

		GLBufferData bufferInitData;
		bufferInitData.dataPtr = initData.dataPtr;
		bufferInitData.dataSize = initData.dataSize;

		if (!bufferObject->Initialize(bufferInitDesc, bufferInitData))
			return nullptr;

		return CreateRSResourceStorage<GLGPUBufferStorage>(initDesc.memoryInfo, bufferObject);
	}




	GLenum SelectGLBufferMapMode(RSResourceMapMode mapMode, RSMemoryAccess memoryAccess)
	{
		stdx::mask<AccessModeFlags> memoryAccessMask = static_cast<AccessModeFlags>(memoryAccess);
		stdx::mask<RSMemoryMapFlags> mapModeMask = static_cast<RSMemoryMapFlags>(mapMode);

		stdx::mask<GLenum> mapFlags = 0;

		if (mapModeMask.is_set(RSMemoryMap_Access_Read) && !memoryAccessMask.is_set(AccessMode_Read))
			return GL_Invalid_Value;

		if (mapModeMask.is_set(RSMemoryMap_Access_Write) && !memoryAccessMask.is_set(AccessMode_Write))
			return GL_Invalid_Value;

		if (mapModeMask.is_set(RSMemoryMap_Access_Read))
			mapFlags.set(GL_MAP_READ_BIT);

		if (mapModeMask.is_set(RSMemoryMap_Access_Write))
			mapFlags.set(GL_MAP_WRITE_BIT);

		if (mapModeMask.is_set(RSMemoryMap_No_Overwrite))
			mapFlags.set(GL_MAP_UNSYNCHRONIZED_BIT);

		if (mapModeMask.is_set(RSMemoryMap_Invalidate_All))
			mapFlags.set(GL_MAP_INVALIDATE_BUFFER_BIT);
		
		if (mapModeMask.is_set(RSMemoryMap_Invalidate_Range))
			mapFlags.set(GL_MAP_INVALIDATE_RANGE_BIT);

		return mapFlags;
	}


}
