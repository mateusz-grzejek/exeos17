
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_GPUBufferStorage_H__
#define __Exs_GraphicsDriver_CommonGL_GPUBufferStorage_H__

#include "GL_GPUBufferInterface.h"
#include <ExsRenderSystem/Resource/GPUBufferStorage.h>


namespace Exs
{


	///<summary>
	///</summary>
	class GLGPUBufferStorage : public GPUBufferStorage
	{
	private:
		GLBufferObject*  _glBufferObject;
		GLuint           _glBufferObjectHandle;

	public:
		GLGPUBufferStorage(const RSResourceMemoryInfo& memoryInfo, GLBufferObject* bufferObject);
		virtual ~GLGPUBufferStorage();

		virtual bool MapMemory(RSResourceMapMode mapMode, RSMappedMemoryInfo* mappedMemoryInfo) override final;
		virtual bool MapMemory(const RSMemoryRange& range, RSResourceMapMode mapMode, RSMappedMemoryInfo* mappedMemoryInfo) override final;
		virtual void UnmapMemory() override final;
		virtual void CopySubdata(Uint offset, Size_t length, const void* data, Size_t dataSize) override final;

		virtual void FlushMappedMemory() override final;
		virtual void FlushMappedRange(const RSMemoryRange& range) override final;

		GLuint GetGLBufferObjectHandle() const;
		bool IsInitialized() const;
	};


	inline GLuint GLGPUBufferStorage::GetGLBufferObjectHandle() const
	{
		return this->_glBufferObjectHandle;
	}

	inline bool GLGPUBufferStorage::IsInitialized() const
	{
		return this->_glBufferObject ? true : false;
	}


	RSInternalHandle<GLGPUBufferStorage> InitGLGPUBufferStorage(GLGPUBufferInterface* interface, const GPUBufferStorageInitDesc& initDesc, const GPUBufferDataDesc& initData);


}


#endif /* __Exs_GraphicsDriver_CommonGL_GPUBufferStorage_H__ */
