
#include "_Precompiled.h"
#include <ExsCommonGL/GL_RenderSystem.h>
#include <ExsCommonGL/Resource/GL_SamplerInterface.h>
#include <ExsRenderSystem/Resource/Sampler.h>


namespace Exs
{


	RSInternalHandle<GLSamplerInterface> InitializeGLSamplerInterface(const SamplerParameters& parameters)
	{
		GLenum wrapModeS = GLConstantMap::GetTextureAddressMode(parameters.addressMode.x);
		GLenum wrapModeT = GLConstantMap::GetTextureAddressMode(parameters.addressMode.y);
		GLenum wrapModeR = GLConstantMap::GetTextureAddressMode(parameters.addressMode.z);
		GLenum cmpFunction = GLConstantMap::GetComparisonFunction(parameters.refComparisonFunction);
		
		GLenum filterMin = 0;
		GLenum filterMag = (parameters.filter.magFilter == TextureFilter::Linear) ? GL_LINEAR : GL_NEAREST;

		if (parameters.filter.mipLevelSamplingFilter == TextureFilter::Linear)
			filterMin = (parameters.filter.minFilter == TextureFilter::Linear) ? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST_MIPMAP_LINEAR;
		else if (parameters.filter.mipLevelSamplingFilter == TextureFilter::Point)
			filterMin = (parameters.filter.minFilter == TextureFilter::Linear) ? GL_LINEAR_MIPMAP_NEAREST : GL_NEAREST_MIPMAP_NEAREST;
		else if (parameters.filter.mipLevelSamplingFilter == TextureFilter::None)
			filterMin = (parameters.filter.minFilter == TextureFilter::Linear) ? GL_LINEAR : GL_NEAREST;

		auto samplerObject = std::make_unique<GLSamplerObject>();
		samplerObject->SetWrapMode(wrapModeS, wrapModeT, wrapModeR);
		samplerObject->SetFilter(filterMin, filterMag);
		samplerObject->SetMaxAnisotropyLevel(static_cast<float>(parameters.anisotropyLevel));
		samplerObject->SetLODBias(parameters.lodBias);
		samplerObject->SetLODRange(parameters.lodRange.first, parameters.lodRange.second);
		samplerObject->SetCompareMode(parameters.refComparisonEnabled ? GL_TRUE : GL_FALSE, cmpFunction);
		samplerObject->SetBorderColor(parameters.borderColor);

		return CreateRSBaseObjectInterface<GLSamplerInterface>(std::move(samplerObject));
	}


}
