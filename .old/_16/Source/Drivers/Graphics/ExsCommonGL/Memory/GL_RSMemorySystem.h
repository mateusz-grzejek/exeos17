
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_RSMemorySystem_H__
#define __Exs_GraphicsDriver_CommonGL_RSMemorySystem_H__

#include "../GL_Prerequisites.h"
#include <ExsRenderSystem/Memory/RSMemorySystem.h>


namespace Exs
{


	class GLRSMemorySystem : public RSMemorySystem
	{
	public:
		///<summary>
		///</summary>
		GLRSMemorySystem(GLRenderSystem* renderSystem);

		///<summary>
		///</summary>
		virtual ~GLRSMemorySystem();

		///<summary>
		///</summary>
		Result Initialize();

		///<summary>
		///</summary>
		RSMemoryHandle AllocateResourceMemory(const RSResourceMemoryAllocationDesc& allocDesc);

		///<summary>
		///</summary>
		void FreeMemory(RSMemoryHandle memoryHandle);

	private:
		//
		virtual RSMemoryHeap* _CreateResourceHeap(const RSResourceMemoryHeapDesc& heapDesc) override final;

		//
		void _EnumerateDeviceHeaps();
	};


}


#endif /* __Exs_GraphicsDriver_CommonGL_RSMemorySystem_H__ */
