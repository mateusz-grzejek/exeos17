
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_DepthStencilState_H__
#define __Exs_GraphicsDriver_CommonGL_DepthStencilState_H__

#include "../GL_Prerequisites.h"
#include <ExsRenderSystem/State/DepthStencilState.h>


namespace Exs
{


	ExsDeclareRefPtrClass(GLDepthStencilStateDescriptor);


	struct GLDepthStencilConfiguration
	{
		struct Depth
		{
			GLenum comparisonFunc;
			GLboolean writeMask;
		};

		struct Stencil
		{
			struct ComparisonFunc
			{
				GLenum backFace;
				GLenum frontFace;
				GLint refValue;
				GLuint refMask;
			};

			struct Operation
			{
				GLenum stencilFail;
				GLenum stencilPassDepthFail;
				GLenum stencilPassDepthPass;
			};

			ComparisonFunc comparisonFunc;
			Operation opBackFace;
			Operation opFrontFace;
			GLuint writeMask;
		};

		Depth      depthSettings;
		bool       depthTestState;
		Stencil    stencilSettings;
		bool       stencilTestState;
	};


	class GLDepthStencilStateDescriptor : public DepthStencilStateDescriptor
	{
		EXS_DECLARE_NONCOPYABLE(GLDepthStencilStateDescriptor);

	private:
		GLDepthStencilConfiguration    _glConfiguration;

	public:
		GLDepthStencilStateDescriptor( RenderSystem* renderSystem,
		                               RSBaseObjectID baseObjectID,
		                               RSStateDescriptorPID descriptorPID,
		                               const DepthStencilConfiguration& configuration,
		                               const GLDepthStencilConfiguration& configurationGL)
		: DepthStencilStateDescriptor(renderSystem, baseObjectID, descriptorPID, configuration)
		, _glConfiguration(configurationGL)
		{ }

		const GLDepthStencilConfiguration& GetGLConfiguration() const
		{
			return this->_glConfiguration;
		}
	};


	bool TranslateGLDepthStencilConfiguration(const DepthStencilConfiguration& configuration, GLDepthStencilConfiguration* translatedGLConfig);


};


#endif /* __Exs_GraphicsDriver_CommonGL_DepthStencilState_H__ */
