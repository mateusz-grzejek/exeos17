
#include "_Precompiled.h"
#include <ExsCommonGL/State/GL_BlendState.h>
#include <ExsCommonGL/GL_RenderSystem.h>


namespace Exs
{


	bool TranslateGLBlendConfiguration(const BlendConfiguration& blendConfig, GLBlendConfiguration* translatedGLConfig)
	{
		ExsDebugAssert( translatedGLConfig != nullptr );
		
		translatedGLConfig->state = (blendConfig.state == ActiveState::Enabled);
		
		if (blendConfig.state == ActiveState::Enabled)
		{
			translatedGLConfig->settings.constantColor.r = static_cast<float>(blendConfig.settings.constantFactor.rgba.red) / 255.0f;
			translatedGLConfig->settings.constantColor.g = static_cast<float>(blendConfig.settings.constantFactor.rgba.green) / 255.0f;
			translatedGLConfig->settings.constantColor.b = static_cast<float>(blendConfig.settings.constantFactor.rgba.blue) / 255.0f;
			translatedGLConfig->settings.constantColor.a = static_cast<float>(blendConfig.settings.constantFactor.rgba.alpha) / 255.0f;
		
			translatedGLConfig->settings.equation.color = GLConstantMap::GetBlendEquation(blendConfig.settings.colorComponent.equation);
			translatedGLConfig->settings.equation.alpha = GLConstantMap::GetBlendEquation(blendConfig.settings.alphaComponent.equation);
		
			translatedGLConfig->settings.factor.colorSrc = GLConstantMap::GetBlendFactor(blendConfig.settings.colorComponent.srcFactor);
			translatedGLConfig->settings.factor.colorTgt = GLConstantMap::GetBlendFactor(blendConfig.settings.colorComponent.tgtFactor);
			translatedGLConfig->settings.factor.alphaSrc = GLConstantMap::GetBlendFactor(blendConfig.settings.alphaComponent.srcFactor);
			translatedGLConfig->settings.factor.alphaTgt = GLConstantMap::GetBlendFactor(blendConfig.settings.alphaComponent.tgtFactor);
		}

		return true;
	}


}
