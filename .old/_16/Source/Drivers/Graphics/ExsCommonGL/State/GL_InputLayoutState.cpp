
#include "_Precompiled.h"
#include <ExsCommonGL/State/GL_InputLayoutState.h>
#include <ExsCommonGL/GL_Extensions.h>
#include <ExsCommonGL/GL_RenderSystem.h>
#include <ExsCommonGL/Objects/GL_ShaderProgramObject.h>


namespace Exs
{


	bool TranslateGLInputLayoutConfiguration( const InputLayoutConfiguration& inputLayoutConfig,
	                                          const GLShaderProgramObject& vertexShaderProgram,
	                                          const GLExtensionManager& extensionManager,
	                                          GLInputLayoutConfiguration* translatedGLConfig)
	{
		ExsDebugAssert( translatedGLConfig != nullptr );

		for (Size_t attribNum = 0; attribNum < Config::RS_IA_Max_Vertex_Input_Attribs_Num; ++attribNum)
		{
			auto& attribDesc = inputLayoutConfig.attribArray[attribNum];
			auto& attribGL = translatedGLConfig->attribArray[attribNum];

			if (attribDesc.state == IAVertexAttribState::Inactive)
			{
				attribGL.active = GL_FALSE;
				continue;
			}

			// Check for explicit attrib binding support.
			if (extensionManager.CheckSupport(GLCoreExtensionID::ARB_Vertex_Attrib_Binding))
			{
				// If it is supported, attributes have the same locations as their indices.
				attribGL.attribLocation = static_cast<GLint>(attribNum);
			}
			else
			{
				// Otherwise, we query location from GLSL to ensure correct locations.
				attribGL.attribLocation = vertexShaderProgram.QueryVertexAttribLocation(attribDesc.name.c_str());
			}

			attribGL.active = GL_TRUE;
			attribGL.baseType = GLConstantMap::GetGraphicDataBaseType(ExsEnumGetGraphicDataFormatBaseType(attribDesc.format));
			attribGL.componentsNum = ExsEnumGetVertexAttribFormatComponentsNum(attribDesc.format);
			attribGL.normalized = ExsEnumIsVertexAttribFormatNormalized(attribDesc.format);
			attribGL.relativeOffset = truncate_cast<GLsizei>(attribDesc.relativeOffset);
		}

		for (Size_t streamNum = 0; streamNum < Config::RS_IA_Max_Vertex_Input_Streams_Num; ++streamNum)
		{
			auto& streamDesc = inputLayoutConfig.streamArray[streamNum];
			auto& streamGL = translatedGLConfig->streamArray[streamNum];

			if (streamDesc.state == IAVertexStreamState::Inactive)
			{
				streamGL.active = GL_FALSE;
				continue;
			}

			streamGL.active = GL_TRUE;
			streamGL.divisor = ((streamDesc.rate == IAVertexStreamRate::Per_Instance) ? 1 : 0);
			streamGL.stride = static_cast<GLsizei>(streamDesc.stride);
		}

		translatedGLConfig->primitiveTopology = GLConstantMap::GetPrimitiveTopology(inputLayoutConfig.primitiveTopologyDescription.primitiveTopology);

		return true;
	}


}
