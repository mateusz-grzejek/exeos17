
#include "_Precompiled.h"
#include <ExsRenderSystem/Resource/Shader.h>
#include <ExsCommonGL/State/GL_GraphicsShaderState.h>
#include <ExsCommonGL/GL_RenderSystem.h>
#include <ExsCommonGL/Objects/GL_ShaderObject.h>
#include <ExsCommonGL/Resource/GL_ShaderInterface.h>


namespace Exs
{


	bool TranslateGLGraphicsShaderConfiguration(const GraphicsShaderConfiguration& graphicsShaderConfig, GLGraphicsShaderConfiguration* translatedGLConfig)
	{
		ExsDebugAssert( translatedGLConfig != nullptr );

		if (!graphicsShaderConfig.activeStageMask.is_set(ShaderStage_Vertex | ShaderStage_Pixel))
			return false;

		ExsDebugAssert( graphicsShaderConfig.vertexShader );
		ExsDebugAssert( graphicsShaderConfig.vertexShader->GetShaderType() == ShaderType::Vertex );

		auto vertexShaderState = graphicsShaderConfig.vertexShader->GetInterface()->As<GLShaderInterface>();
		auto* vertexShaderObject = vertexShaderState->GetGLShaderObject();
		translatedGLConfig->vertexShader = vertexShaderObject;
		translatedGLConfig->vertexShaderHandle = vertexShaderObject->GetHandle();

		ExsDebugAssert( graphicsShaderConfig.pixelShader );
		ExsDebugAssert( graphicsShaderConfig.pixelShader->GetShaderType() == ShaderType::Pixel );
		
		auto pixelShaderState = graphicsShaderConfig.pixelShader->GetInterface()->As<GLShaderInterface>();
		auto* pixelShaderObject = pixelShaderState->GetGLShaderObject();
		translatedGLConfig->pixelShader = pixelShaderObject;
		translatedGLConfig->pixelShaderHandle = pixelShaderObject->GetHandle();

		if (graphicsShaderConfig.activeStageMask.is_set(ShaderStage_Tesselation_Control))
		{
			ExsDebugAssert( graphicsShaderConfig.tesselationControlShader );
			ExsDebugAssert( graphicsShaderConfig.tesselationControlShader->GetShaderType() == ShaderType::Tesselation_Control );
			
			auto tesselationControlShaderState = graphicsShaderConfig.pixelShader->GetInterface()->As<GLShaderInterface>();
			auto* tesselationControlShaderObject = tesselationControlShaderState->GetGLShaderObject();
			translatedGLConfig->tesselationControlShader = tesselationControlShaderObject;
			translatedGLConfig->tesselationControlShaderHandle = tesselationControlShaderObject->GetHandle();
		}
		else
		{
			translatedGLConfig->tesselationControlShader = nullptr;
			translatedGLConfig->tesselationControlShaderHandle = GL_Invalid_Handle;
		}

		if (graphicsShaderConfig.activeStageMask.is_set(ShaderStage_Tesselation_Evaluation))
		{
			ExsDebugAssert( graphicsShaderConfig.tesselationEvaluationShader );
			ExsDebugAssert( graphicsShaderConfig.tesselationControlShader->GetShaderType() == ShaderType::Tesselation_Evaluation );
			
			auto tesselationEvaluationShaderState = graphicsShaderConfig.tesselationEvaluationShader->GetInterface()->As<GLShaderInterface>();
			auto* tesselationEvaluationShaderObject = tesselationEvaluationShaderState->GetGLShaderObject();
			translatedGLConfig->tesselationEvaluationShader = tesselationEvaluationShaderObject;
			translatedGLConfig->tesselationEvaluationShaderHandle = tesselationEvaluationShaderObject->GetHandle();
		}
		else
		{
			translatedGLConfig->tesselationEvaluationShader = nullptr;
			translatedGLConfig->tesselationEvaluationShaderHandle = GL_Invalid_Handle;
		}

		if (graphicsShaderConfig.activeStageMask.is_set(ShaderStage_Geometry))
		{
			ExsDebugAssert( graphicsShaderConfig.geometryShader );
			ExsDebugAssert( graphicsShaderConfig.geometryShader->GetShaderType() == ShaderType::Geometry );

			auto geometryShaderState = graphicsShaderConfig.geometryShader->GetInterface()->As<GLShaderInterface>();
			auto* geometryShaderObject = geometryShaderState->GetGLShaderObject();
			translatedGLConfig->geometryShader = geometryShaderObject;
			translatedGLConfig->geometryShaderHandle = geometryShaderObject->GetHandle();
		}
		else
		{
			translatedGLConfig->geometryShader = nullptr;
			translatedGLConfig->geometryShaderHandle = GL_Invalid_Handle;
		}

		return true;
	}


}
