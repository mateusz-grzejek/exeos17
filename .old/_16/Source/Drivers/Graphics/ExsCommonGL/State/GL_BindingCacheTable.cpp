
#include "_Precompiled.h"
#include <ExsRenderSystem/Resource/ConstantBuffer.h>
#include <ExsRenderSystem/Resource/Sampler.h>
#include <ExsRenderSystem/Resource/ShaderResourceView.h>
#include <ExsCommonGL/Resource/GL_GPUBufferInterface.h>
#include <ExsCommonGL/Resource/GL_SamplerInterface.h>
#include <ExsCommonGL/Resource/GL_TextureInterface.h>
#include <ExsCommonGL/State/GL_BindingCacheTable.h>


namespace Exs
{


	std::shared_ptr<GLConstantBufferBindingCacheTable> CreateGLConstantBufferBindingCacheTable(const ConstantBufferBinding* srcBindings, Size_t srcBindingsNum)
	{
		GLConstantBufferBindingCacheTable::BindingArray bindings;
		GLConstantBufferBindingCacheTable::GLBindingArray oglBindings;

		auto srcBindingsArrayView = BindArrayView(srcBindings, srcBindingsNum);
		Uint32 activeBindingsNum = 0;

		for (auto& srcBinding : srcBindingsArrayView)
		{
			if (srcBinding.cbufferIndex >= Config::RS_IA_Max_Constant_Buffers_Num)
				continue;

			if (!srcBinding.constantBuffer)
				continue;

			auto* cbufferState = srcBinding.constantBuffer->GetInterface()->As<GLGPUBufferInterface>();

			bindings[srcBinding.cbufferIndex].object = srcBinding.constantBuffer;
			bindings[srcBinding.cbufferIndex].stageAccess = srcBinding.stageAccess;
			oglBindings[srcBinding.cbufferIndex].bindIndex = srcBinding.cbufferIndex;
			oglBindings[srcBinding.cbufferIndex].bufferHandle = cbufferState->GetGLBufferObjectHandle();

			++activeBindingsNum;
		}

		return std::make_shared<GLConstantBufferBindingCacheTable>(bindings, oglBindings, activeBindingsNum);
	}


	std::shared_ptr<GLSamplerBindingCacheTable> CreateGLSamplerBindingCacheTable(const SamplerBinding* srcBindings, Size_t srcBindingsNum)
	{
		GLSamplerBindingCacheTable::BindingArray bindings;
		GLSamplerBindingCacheTable::GLBindingArray oglBindings;

		auto srcBindingsArrayView = BindArrayView(srcBindings, srcBindingsNum);
		Uint32 activeBindingsNum = 0;

		for (auto& srcBinding : srcBindingsArrayView)
		{
			if (srcBinding.samplerIndex >= Config::RS_IA_Max_Samplers_Num)
				continue;

			if (srcBinding.textureIndex >= Config::RS_IA_Max_Shader_Resources_Num)
				continue;

			if (!srcBinding.sampler)
				continue;

			auto* samplerState = srcBinding.sampler->GetInterface()->As<GLSamplerInterface>();

			bindings[srcBinding.samplerIndex].object = srcBinding.sampler;
			bindings[srcBinding.samplerIndex].stageAccess = srcBinding.stageAccess;
			oglBindings[srcBinding.samplerIndex].textureUnit = srcBinding.textureIndex;
			oglBindings[srcBinding.samplerIndex].samplerHandle = samplerState->GetGLSamplerObjectHandle();

			++activeBindingsNum;
		}

		return std::make_shared<GLSamplerBindingCacheTable>(bindings, oglBindings, activeBindingsNum);
	}


	std::shared_ptr<GLShaderResourceBindingCacheTable> CreateGLShaderResourceBindingCacheTable(const ShaderResourceBinding* srcBindings, Size_t srcBindingsNum)
	{
		GLShaderResourceBindingCacheTable::BindingArray bindings;
		GLShaderResourceBindingCacheTable::GLBindingArray oglBindings;

		auto srcBindingsArrayView = BindArrayView(srcBindings, srcBindingsNum);
		Uint32 activeBindingsNum = 0;

		for (auto& srcBinding : srcBindingsArrayView)
		{
			if (srcBinding.resourceIndex >= Config::RS_IA_Max_Shader_Resources_Num)
				continue;

			if (!srcBinding.resourceView)
				continue;

			auto* resource = srcBinding.resourceView->GetResource();
			auto srvResType = srcBinding.resourceView->GetResourceType();
			
			if (srvResType == ShaderResourceViewResType::Buffer)
			{
				//TODO: SUpport texture buffers!
				continue;
			}
			else
			{
				auto* textureState = resource->GetInterface()->As<GLTextureInterface>();
				oglBindings[srcBinding.resourceIndex].textureHandle = textureState->GetGLTextureObjectHandle();
			}

			bindings[srcBinding.resourceIndex].object = srcBinding.resourceView;
			bindings[srcBinding.resourceIndex].stageAccess = srcBinding.stageAccess;
			oglBindings[srcBinding.resourceIndex].textureUnit = srcBinding.resourceIndex;
			oglBindings[srcBinding.resourceIndex].bindTarget = GLConstantMap::GetShaderResourceBindTarget(srvResType);

			++activeBindingsNum;
		}

		return std::make_shared<GLShaderResourceBindingCacheTable>(bindings, oglBindings, activeBindingsNum);
	}


}
