
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_VertexArrayObjectCache_H__
#define __Exs_GraphicsDriver_CommonGL_VertexArrayObjectCache_H__

#include "../GL_Prerequisites.h"
#include <stdx/sorted_array.h>


namespace Exs
{

	struct GLInputLayoutConfiguration;
	struct GLVertexStreamConfiguration;

	class GLInputLayoutStateDescriptor;
	class GLVertexStreamStateDescriptor;
	class GLVertexArrayObject;


	class GLVertexArrayObjectCache
	{
	private:
		struct VertexArrayObjectRefID
		{
			RSStateDescriptorPID inputLayoutSDPID;
			RSStateDescriptorPID vertexStreamSDPID;
		};

		struct VertexArrayObjectInfo
		{
			VertexArrayObjectRefID refID;
			std::unique_ptr<GLVertexArrayObject> vertexArrayObject;
			GLInputLayoutStateDescriptor* inputLayoutDescriptor;
			GLVertexStreamStateDescriptor* vertexStreamDescriptor;
		};

		struct VertexArrayObjectComparator
		{
			bool operator()(const VertexArrayObjectInfo& left, const VertexArrayObjectInfo& right) const
			{
				return (left.refID.inputLayoutSDPID < right.refID.inputLayoutSDPID) ||
						((left.refID.inputLayoutSDPID == right.refID.inputLayoutSDPID) && (left.refID.vertexStreamSDPID < right.refID.vertexStreamSDPID));
			}

			bool operator()(const VertexArrayObjectInfo& left, const VertexArrayObjectRefID& right) const
			{
				return (left.refID.inputLayoutSDPID < right.inputLayoutSDPID) ||
						((left.refID.inputLayoutSDPID == right.inputLayoutSDPID) && (left.refID.vertexStreamSDPID < right.vertexStreamSDPID));
			}

			bool operator()(const VertexArrayObjectInfo& left, RSStateDescriptorPID right) const
			{
				return left.refID.inputLayoutSDPID < right;
			}
		};

		typedef stdx::sorted_array<VertexArrayObjectInfo, VertexArrayObjectComparator> VertexArrayObjectArray;

	private:
		VertexArrayObjectArray    _vertexArrayObjects;

	public:
		GLVertexArrayObjectCache();
		~GLVertexArrayObjectCache();

		///
		GLVertexArrayObject* GetVertexArrayObject( GLInputLayoutStateDescriptor* inputLayoutStateDescriptor,
		                                           GLVertexStreamStateDescriptor* vertexStreamStateDescriptor);

		///
		void InvalidateInputLayoutStateDescriptor(RSStateDescriptorPID inputLayoutSDPID);

		///
		void InvalidateVertexStreamStateDescriptor(RSStateDescriptorPID vertexStreamSDPID);

	private:
		///
		GLVertexArrayObject* _Find(RSStateDescriptorPID inputLayoutSDPID, RSStateDescriptorPID vertexStreamSDPID) const;

		///
		GLVertexArrayObject* _CreateNew( const GLInputLayoutConfiguration& inputLayoutConfig,
		                                 const GLVertexStreamConfiguration& vertexStreamConfig);
	};


};


#endif /* __Exs_GraphicsDriver_CommonGL_VertexArrayObjectCache_H__ */
