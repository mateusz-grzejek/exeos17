
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_BindingCacheTable_H__
#define __Exs_GraphicsDriver_CommonGL_BindingCacheTable_H__

#include "../GL_Prerequisites.h"
#include <ExsRenderSystem/State/BindingCacheTable.h>


namespace Exs
{

	
	struct ConstantBufferBinding;
	struct SamplerBinding;
	struct ShaderResourceBinding;
	
	
	class GLConstantBufferBindingCacheTable : public ConstantBufferBindingCacheTable
	{
	public:
		struct GLBinding
		{
			GLuint bindIndex = 0;
			GLuint bufferHandle = 0;
		};

		typedef std::array<GLBinding, Config::RS_IA_Max_Samplers_Num> GLBindingArray;

	private:
		GLBindingArray  _glBindings;

	public:
		GLConstantBufferBindingCacheTable(const BindingArray& bindings, const GLBindingArray& bindingsGL, Uint32 activeBindingsNum)
		: ConstantBufferBindingCacheTable(bindings, activeBindingsNum)
		, _glBindings(bindingsGL)
		{ }

		const GLBindingArray& GetGLBindings() const
		{
			return this->_glBindings;
		}
	};
	
	
	class GLSamplerBindingCacheTable : public SamplerBindingCacheTable
	{
	public:
		struct GLBinding
		{
			GLuint samplerHandle = 0;
			GLuint textureUnit = 0;
		};

		typedef std::array<GLBinding, Config::RS_IA_Max_Samplers_Num> GLBindingArray;

	private:
		GLBindingArray  _glBindings;

	public:
		GLSamplerBindingCacheTable(const BindingArray& bindings, const GLBindingArray& bindingsGL, Uint32 activeBindingsNum)
		: SamplerBindingCacheTable(bindings, activeBindingsNum)
		, _glBindings(bindingsGL)
		{ }

		const GLBindingArray& GetGLBindings() const
		{
			return this->_glBindings;
		}
	};
	
	
	class GLShaderResourceBindingCacheTable : public ShaderResourceBindingCacheTable
	{
	public:
		struct GLBinding
		{
			GLenum bindTarget = 0;
			GLenum textureHandle = 0;
			GLenum textureUnit = 0;
		};

		typedef std::array<GLBinding, Config::RS_IA_Max_Shader_Resources_Num> GLBindingArray;

	private:
		GLBindingArray  _glBindings;

	public:
		GLShaderResourceBindingCacheTable(const BindingArray& bindings, const GLBindingArray& bindingsGL, Uint32 activeBindingsNum)
		: ShaderResourceBindingCacheTable(bindings, activeBindingsNum)
		, _glBindings(bindingsGL)
		{ }

		const GLBindingArray& GetGLBindings() const
		{
			return this->_glBindings;
		}
	};

	
	std::shared_ptr<GLConstantBufferBindingCacheTable> CreateGLConstantBufferBindingCacheTable(const ConstantBufferBinding* srcBindings, Size_t srcBindingsNum);
	std::shared_ptr<GLSamplerBindingCacheTable> CreateGLSamplerBindingCacheTable(const SamplerBinding* srcBindings, Size_t srcBindingsNum);
	std::shared_ptr<GLShaderResourceBindingCacheTable> CreateGLShaderResourceBindingCacheTable(const ShaderResourceBinding* srcBindings, Size_t srcBindingsNum);


}


#endif /* __Exs_GraphicsDriver_CommonGL_BindingCacheTable_H__ */

