
#include "_Precompiled.h"
#include <ExsCommonGL/State/GL_VertexArrayObjectCache.h>
#include <ExsCommonGL/State/GL_InputLayoutState.h>
#include <ExsCommonGL/State/GL_VertexStreamState.h>
#include <ExsCommonGL/Objects/GL_VertexArrayObject.h>


namespace Exs
{


	GLVertexArrayObjectCache::GLVertexArrayObjectCache()
	{ }


	GLVertexArrayObjectCache::~GLVertexArrayObjectCache()
	{ }


	GLVertexArrayObject* GLVertexArrayObjectCache::GetVertexArrayObject( GLInputLayoutStateDescriptor* inputLayoutStateDescriptor,
	                                                                     GLVertexStreamStateDescriptor* vertexStreamStateDescriptor)
	{
		RSStateDescriptorPID inputLayoutSDPID = inputLayoutStateDescriptor->GetDescriptorPID();
		RSStateDescriptorPID vertexStreamSDPID = vertexStreamStateDescriptor->GetDescriptorPID();

		// Check if VAO for this descriptors has been created before.
		GLVertexArrayObject* vertexArrayObject = this->_Find(inputLayoutSDPID, vertexStreamSDPID);

		if (vertexArrayObject == nullptr)
		{
			const auto& inputLayoutConfig = inputLayoutStateDescriptor->GetGLConfiguration();
			const auto& vertexStreamConfig = vertexStreamStateDescriptor->GetGLConfiguration();
			vertexArrayObject = this->_CreateNew(inputLayoutConfig, vertexStreamConfig);
		}

		return vertexArrayObject;
	}


	void GLVertexArrayObjectCache::InvalidateInputLayoutStateDescriptor(RSStateDescriptorPID inputLayoutSDPID)
	{
		auto vaoInfoRef = this->_vertexArrayObjects.lower_bound(inputLayoutSDPID);
		if (vaoInfoRef != this->_vertexArrayObjects.end())
		{
			while (vaoInfoRef->refID.inputLayoutSDPID == inputLayoutSDPID)
			{
				vaoInfoRef = this->_vertexArrayObjects.erase(vaoInfoRef);
			}
		}
	}


	void GLVertexArrayObjectCache::InvalidateVertexStreamStateDescriptor(RSStateDescriptorPID vertexStreamSDPID)
	{
		for (auto vaoInfoRef = this->_vertexArrayObjects.begin() ; vaoInfoRef != this->_vertexArrayObjects.end(); ++vaoInfoRef)
		{
			if (vaoInfoRef->refID.vertexStreamSDPID == vertexStreamSDPID)
			{
				vaoInfoRef = this->_vertexArrayObjects.erase(vaoInfoRef);
			}
		}
	}


	GLVertexArrayObject* GLVertexArrayObjectCache::_Find(RSStateDescriptorPID inputLayoutSDPID, RSStateDescriptorPID vertexStreamSDPID) const
	{
		VertexArrayObjectRefID vaoID { inputLayoutSDPID, vertexStreamSDPID };
		auto vaoInfoRef = this->_vertexArrayObjects.lower_bound(vaoID);

		if (vaoInfoRef != this->_vertexArrayObjects.end())
		{
			if ((vaoInfoRef->refID.inputLayoutSDPID == inputLayoutSDPID) && (vaoInfoRef->refID.vertexStreamSDPID == vertexStreamSDPID))
				return vaoInfoRef->vertexArrayObject.get();
		}

		return nullptr;
	}


	GLVertexArrayObject* GLVertexArrayObjectCache::_CreateNew( const GLInputLayoutConfiguration& inputLayoutConfig,
	                                                           const GLVertexStreamConfiguration& vertexStreamConfig)
	{
		auto vertexArrayObject = std::make_unique<GLVertexArrayObject>();
		GLuint vertexArrayObjectHandle = vertexArrayObject->GetHandle();

		// Bind vertex array object. It will store all state data.
		glBindVertexArray(vertexArrayObjectHandle);
		ExsGLErrorCheck();

		// Index buffer binding is also part of VAO state. Save it.
		if (vertexStreamConfig.indexBufferBinding.bufferHandle != GL_Invalid_Handle)
		{
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexStreamConfig.indexBufferBinding.bufferHandle);
			ExsGLErrorCheck();
		}

		Size_t activeAttribsNum = 0;

		for (Size_t attribNum = 0; attribNum < Config::RS_IA_Max_Vertex_Input_Attribs_Num; ++attribNum)
		{
			const GLInputLayoutConfiguration::GLVertexAttributeDesc& attribDesc = inputLayoutConfig.attribArray[attribNum];

			if (attribDesc.active == GL_FALSE)
				continue;

			const GLInputLayoutConfiguration::GLVertexStreamDesc& attribStreamDesc = inputLayoutConfig.streamArray[attribDesc.streamIndex];
			const GLVertexStreamConfiguration::GLVertexBufferBinding& attribVertexBufferBinding = vertexStreamConfig.vertexBufferBindingArray[attribDesc.streamIndex];

			glBindBuffer(GL_ARRAY_BUFFER, attribVertexBufferBinding.bufferHandle);
			ExsGLErrorCheck();

			glEnableVertexAttribArray(attribDesc.attribLocation);
			ExsGLErrorCheck();

			GLsizei stride = truncate_cast<GLsizei>(attribStreamDesc.stride);
			const void* offset = reinterpret_cast<const void*>(attribDesc.relativeOffset + attribVertexBufferBinding.dataBaseOffset);

			glVertexAttribPointer(attribDesc.attribLocation, attribDesc.componentsNum, attribDesc.baseType, attribDesc.normalized, stride, offset);
			ExsGLErrorCheck();

			activeAttribsNum += 1;
		}

		glBindVertexArray(0);
		ExsGLErrorCheck();

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		ExsGLErrorCheck();

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		ExsGLErrorCheck();

		if (activeAttribsNum == 0)
		{
			ExsTraceWarning(TRC_GraphicsDriver_GL, "");
			return nullptr;
		}

		return vertexArrayObject.release();
	};


}
