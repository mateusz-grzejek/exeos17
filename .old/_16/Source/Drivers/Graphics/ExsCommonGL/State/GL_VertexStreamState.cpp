
#include "_Precompiled.h"
#include <ExsCommonGL/GL_RenderSystem.h>
#include <ExsCommonGL/Objects/GL_BufferObject.h>
#include <ExsCommonGL/State/GL_VertexStreamState.h>
#include <ExsCommonGL/Resource/GL_GPUBufferStorage.h>
#include <ExsRenderSystem/Resource/IndexBuffer.h>
#include <ExsRenderSystem/Resource/VertexBuffer.h>


namespace Exs
{


	bool TranslateGLVertexStreamConfiguration(const VertexStreamConfiguration& configuration, GLVertexStreamConfiguration* translatedGLConfig)
	{
		ExsDebugAssert( translatedGLConfig != nullptr );

		for (Size_t streamNum = 0; streamNum < Config::RS_IA_Max_Vertex_Input_Streams_Num; ++streamNum)
		{
			auto& bindingDesc = configuration.vertexBufferBindingArray[streamNum];
			auto& bindingGL = translatedGLConfig->vertexBufferBindingArray[streamNum];

			if (!bindingDesc.buffer)
			{
				bindingGL.bufferHandle = GL_Invalid_Handle;
				continue;
			}

			const GLGPUBufferStorage* bufferStoragerGL = bindingDesc.buffer->GetStorage()->As<GLGPUBufferStorage>();
			bindingGL.bufferHandle = bufferStoragerGL->GetGLBufferObjectHandle();
			bindingGL.dataBaseOffset = static_cast<GLsizei>(bindingDesc.dataBaseOffset);
		}

		const GLGPUBufferStorage* indexBufferStoragerGL = configuration.indexBufferBinding.buffer->GetStorage()->As<GLGPUBufferStorage>();
		translatedGLConfig->indexBufferBinding.bufferHandle = indexBufferStoragerGL->GetGLBufferObjectHandle();
		translatedGLConfig->indexBufferBinding.dataBaseOffset = static_cast<GLsizei>(configuration.indexBufferBinding.dataBaseOffset);

		return true;
	}


}
