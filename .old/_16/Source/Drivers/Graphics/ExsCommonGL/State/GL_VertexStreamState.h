
#pragma once

#ifndef __Exs_GraphicsDriver_CommonGL_VertexStreamState_H__
#define __Exs_GraphicsDriver_CommonGL_VertexStreamState_H__

#include "../GL_Prerequisites.h"
#include <ExsRenderSystem/State/VertexStreamState.h>


namespace Exs
{


	struct GLVertexStreamConfiguration
	{
		struct GLVertexBufferBinding
		{
			GLuint bufferHandle;
			GLsizei dataBaseOffset;
		};

		struct IndexBufferBinding
		{
			GLuint bufferHandle;
			GLsizei dataBaseOffset;
		};

		typedef std::array<GLVertexBufferBinding, Config::RS_IA_Max_Vertex_Input_Streams_Num> VertexBufferBindingArray;

		IndexBufferBinding indexBufferBinding;
		VertexBufferBindingArray vertexBufferBindingArray;
	};


	class GLVertexStreamStateDescriptor : public VertexStreamStateDescriptor
	{
		EXS_DECLARE_NONCOPYABLE(GLVertexStreamStateDescriptor);

	protected:
		GLVertexStreamConfiguration    _glConfiguration;

	public:
		GLVertexStreamStateDescriptor( RenderSystem* renderSystem,
		                               RSBaseObjectID baseObjectID,
		                               RSStateDescriptorPID descriptorPID,
		                               const VertexStreamConfiguration& configuration,
		                               const GLVertexStreamConfiguration& configurationGL)
		: VertexStreamStateDescriptor(renderSystem, baseObjectID, descriptorPID, configuration)
		, _glConfiguration(configurationGL)
		{ }

		const GLVertexStreamConfiguration& GetGLConfiguration() const
		{
			return this->_glConfiguration;
		}
	};


	bool TranslateGLVertexStreamConfiguration(const VertexStreamConfiguration& configuration, GLVertexStreamConfiguration* translatedGLConfig);


};


#endif /* __Exs_GraphicsDriver_CommonGL_VertexStreamState_H__ */
