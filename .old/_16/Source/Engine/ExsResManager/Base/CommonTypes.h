
#pragma once

#ifndef __Exs_ResManager_CommonTypes_H__
#define __Exs_ResManager_CommonTypes_H__


namespace Exs
{


	struct ResourceFileFormatInfo
	{
		//
		ResourceType resourceType;

		//
		std::string name;

		//
		std::string mimeType;

		//
		std::vector<std::string> extensions;
	};


}


#endif /* __Exs_ResManager_CommonTypes_H__ */
