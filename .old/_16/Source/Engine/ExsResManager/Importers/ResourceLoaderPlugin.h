
#pragma once

#ifndef __Exs_ResManager_ImageLoaderPlugin_H__
#define __Exs_ResManager_ImageLoaderPlugin_H__

#include "ResourceLoader.h"
#include <ExsCore/Plugins/Plugin.h>
#include <ExsCore/Plugins/PluginManager.h>
#include <stdx/sorted_array.h>


namespace Exs
{
	
	
	class ResourceLoaderPlugin;
	class ResourceLoaderPluginManager;

	ExsDeclareRefPtrClass(ResourceLoader);


	enum : PluginType
	{
		// Represents type tag of image importer plugins.
		PluginType_Resource_Loader = 0x045091
	};


	EXS_PLUGIN_REGISTER_PLUGIN_TYPE(PluginType_Resource_Loader, ResourceLoaderPluginManager, ResourceLoaderPlugin);

	
	///<summary>
	/// Represents plugin containing implementation of image loading intrfaces. Each plugin can implement one or more
	/// loaders which are exposed using <c>ImageLoader</c> interface. Particular importer can be retrieved using ID
	/// of image format it supports.
	///</summary>
	class EXS_LIBCLASS_RESMANAGER ResourceLoaderPlugin : public Plugin
	{
	public:
		struct LoaderEntry
		{
			ResourceType resourceType;
			ResourceLoaderRefPtr loader;
			std::string loaderName;
		};

		typedef std::vector<LoaderEntry> LoaderList;

	protected:
		LoaderList  _loaders;

	public:
		ResourceLoaderPlugin(ResourceLoaderPluginManager* manager);
		virtual ~ResourceLoaderPlugin();

		void RegisterLoader(const ResourceLoaderRefPtr& loader);

		template <ResourceType Resource_type_tag>
		typename ResourceTypeProperties<Resource_type_tag>::LoaderType* GetLoaderByName(const char* name) const;
	};


	template <ResourceType Resource_type_tag>
	inline typename ResourceTypeProperties<Resource_type_tag>::LoaderType* ResourceLoaderPlugin::GetLoaderByName(const char* name) const
	{
		typedef typename ResourceTypeProperties<Resource_type_tag>::LoaderType LoaderType;

		auto loaderRef = std::find_if (
			this->_loaders.begin(),
			this->_loaders.end(),
			[name](const LoaderEntry& entry) -> bool {
				return (entry.resourceType == Resource_type_tag) && (entry.loaderName == name);
			});

		if (loaderRef != this->_loaders.end())
			return dbgsafe_ptr_cast<LoaderType*>(loaderRef->loader.get());

		return nullptr;
	}
	
	
	///<summary>
	/// Implements plugin component functionality for image importer plugins.
	///</summary>
	class EXS_LIBCLASS_RESMANAGER ResourceLoaderPluginManager : public PluginManager
	{
	public:
		ResourceLoaderPluginManager(PluginSystem* pluginSystem);
		virtual ~ResourceLoaderPluginManager();

	protected:
		virtual void OnRegisterPlugin(Plugin* plugin) override;
		virtual void OnUnregisterPlugin(Plugin* plugin) override;
	};


}


#endif /* __Exs_ResManager_ImageLoaderPlugin_H__ */
