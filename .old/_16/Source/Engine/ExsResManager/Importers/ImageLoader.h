
#pragma once

#ifndef __Exs_ResManager_ImageLoader_H__
#define __Exs_ResManager_ImageLoader_H__

#include "ResourceLoader.h"


namespace Exs
{


	struct ImageData;
	
	class ImageLoader;
	class ResourceLoaderPlugin;

	ExsDeclareRefPtrClass(ImageLoaderObject);

	//
	typedef std::function<ImageLoaderObjectRefPtr(ImageLoader*, const PluginHandle&)> ImageLoaderObjectCreateProc;

	
	///<summary>
	/// ImageLoader represents importer object used to load various formats of images. These objects are created and managed
	/// by actual plugins. They serve as a wrappers for actual loaders (instances of types implementing <c>ImageLoader</c>),
	/// which are created using <c>CreateLoader()</c> method. Every ImageLoader "owns" single ImageLoaderObject implementation.
	///</summary>
	class EXS_LIBCLASS_RESMANAGER ImageLoader : public ResourceLoader
	{
		friend class ImageLoaderObject;

	private:
		ResourceLoaderPlugin*          _plugin;
		ImageLoaderObjectCreateProc    _loaderObjectCreateProc;
		std::atomic<Size_t>            _activeLoaderObjectsNum;

	public:
		ImageLoader(ResourceLoaderPlugin* plugin, const char* name, const ImageLoaderObjectCreateProc& loaderObjectCreateProc);
		~ImageLoader();

		///<summary>
		/// Creates new instance of image loader which can be used to load image format supported by this importer. Every
		/// new copy increments use counter of its parent plugin by 1. Decrementation occurs after the loader is destroyed.
		///</summary>
		ImageLoaderObjectRefPtr CreateLoaderObject();
		
		///<summary>
		///</summary>
		Size_t GetActiveLoaderObjectsNum() const;

	private:
		void OnLoaderObjectCreate(ImageLoader* loader);
		void OnLoaderObjectDestroy(ImageLoader* loader);
	};


	inline Size_t ImageLoader::GetActiveLoaderObjectsNum() const
	{
		return this->_activeLoaderObjectsNum.load(std::memory_order_relaxed);
	}


	template <class Loader_object_t, class... Formats>
	inline std::shared_ptr<ImageLoader> CreateImageLoaderObject(ResourceLoaderPlugin* plugin, const char* name, Formats&&... formats)
	{
		auto loaderObject = std::make_shared<ImageLoader>(plugin, name,
			[](ImageLoader* loader, const PluginHandle& pluginHandle) -> ImageLoaderObjectRefPtr {
				return std::make_shared<Loader_object_t>(loader, pluginHandle);
			});
		RegisterResourceLoaderFileFormats(*loaderObject, std::forward<Formats>(formats)...);
		return loaderObject;
	}

	
	///<summary>
	///</summary>
	class EXS_LIBCLASS_RESMANAGER ImageLoaderObject : public ResourceLoaderObject
	{
		EXS_DECLARE_NONCOPYABLE(ImageLoaderObject);

	protected:
		ImageLoader*  _loader;

	public:
		ImageLoaderObject(ImageLoader* loader, const PluginHandle& pluginHandle);
		virtual ~ImageLoaderObject();

		///<summary>
		/// Loads image from specified data.
		///</summary>
		///
		///<param name="data"> Pointer to encoded image data. </param>
		///<param name="size"> Size of the data, in bytes. </param>
		///<param name="imageData"> Pointer to the output, where decoded data will be stored. </param>
		///<param name="flags"> Additional flags (currently unused). </param>
		virtual Result LoadImage(const Byte* data, Size_t size, ImageData* imageData, stdx::mask<Enum> flags) = 0;
		
		///<summary>
		/// Return importer, by which this loader was created.
		///</summary>
		ImageLoader* GetLoader() const;
	};


}


#endif /* __Exs_ResManager_ImageLoader_H__ */
