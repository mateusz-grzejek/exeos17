
#pragma once

#ifndef __Exs_ResManager_ResourceLoader_H__
#define __Exs_ResManager_ResourceLoader_H__

#include "../Prerequisites.h"


namespace Exs
{


	struct ResourceFileFormatSpecification
	{
		//
		const char* name;

		//
		const char* mimeType;

		//
		const char* supportedExtensions;
	};

	
	///<summary>
	///</summary>
	class ResourceLoaderObject
	{
	private:
		PluginHandle  _pluginHandle;

	public:
		ResourceLoaderObject(const PluginHandle& pluginHandle)
		: _pluginHandle(pluginHandle)
		{ }

		virtual ~ResourceLoaderObject()
		{ }
	};

	
	///<summary>
	/// Common base class for all importer modules supported by this library.
	///</summary>
	class EXS_LIBCLASS_RESMANAGER ResourceLoader
	{
	private:
		typedef std::vector<ResourceFileFormatInfo> FileFormatList;

	private:
		ResourceType    _resourceType; //
		std::string     _name; //
		FileFormatList  _fileFormatList; //

	public:
		ResourceLoader(ResourceType resourceType, const char* name);
		virtual ~ResourceLoader();
		
		///<summary>
		///</summary>
		void RegisterFileFormat(const ResourceFileFormatSpecification& formatSpecification);
		
		///<summary>
		///</summary>
		ResourceType GetResourceType() const;
		
		///<summary>
		///</summary>
		const std::string& GetName() const;
		
		///<summary>
		///</summary>
		const FileFormatList& GetFileFormatList() const;
	};


	inline ResourceType ResourceLoader::GetResourceType() const
	{
		return this->_resourceType;
	}
	
	inline const std::string& ResourceLoader::GetName() const
	{
		return this->_name;
	}

	inline const ResourceLoader::FileFormatList& ResourceLoader::GetFileFormatList() const
	{
		return this->_fileFormatList;
	}


	template <class... Formats>
	inline void RegisterResourceLoaderFileFormats(ResourceLoader& loader, const ResourceFileFormatSpecification& formatSpecification, Formats&... formats)
	{
		loader.RegisterFileFormat(formatSpecification);
		RegisterResourceLoaderFileFormats(loader, std::forward<Formats>(formats)...);
	}


	inline void RegisterResourceLoaderFileFormats(ResourceLoader& loader)
	{ }


}


#endif /* __Exs_ResManager_ResourceLoader_H__ */
