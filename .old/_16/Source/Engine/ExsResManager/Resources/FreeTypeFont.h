
#pragma once

#ifndef __Exs_ResManager_FreeTypeFont_H__
#define __Exs_ResManager_FreeTypeFont_H__

#include "Font.h"
#include "../Resources/AtlasComposer.h"


namespace Exs
{


	class FreeTypeLoader;

	typedef stdx::dynamic_data_array<Byte> FontFileData;


	struct FreeTypeFontDesc
	{
		Uint32 fontSize;

		mutable FontFileData ttfFileData;

		Vector2U32 layerSize;
		
		Uint32 layersNum;
		
		Uint32 ttfFaceIndex = 0;
		
		bool loadKerningTable = false;
		
		bool preRenderAscii = false;
	};

	
	///<summary>
	///</summary>
	class FreeTypeFontLayer : public FontLayer
	{
	private:
		Uint32         _layerIndex;
		AtlasComposer  _composer;

	public:
		FreeTypeFontLayer(Font* font, Size_t layerIndex, const Vector2U32& dimensions, const AtlasComposerConfig& composerConfig);

		/// Inserts glyph into the layer. On success, destOffset contains position within the layer.
		bool InsertGlyph(CodePoint cp, const Vector2U32& glyphRect, Vector2U32* destOffset);

	private:
		/// @override FontLayer::Reset.
		virtual void OnLayerReset() override;
	};

	
	///<summary>
	///</summary>
	class FreeTypeFont : public Font
	{
	private:
		std::unique_ptr<FreeTypeLoader>  _loader;
		Vector2U32                       _defaultLayerDimensions;
		ShaderResourceViewHandle         _textureArraySRV;
		Texture2DArray*                  _textureArray;

	public:
		FreeTypeFont(std::unique_ptr<FreeTypeLoader>&& loader, const Vector3U32& dimensions, const ShaderResourceViewHandle& textureArraySRV);

		virtual ~FreeTypeFont();

		///<summary>
		/// Explicitly loads glyphs for specified range of code points. It has the same behaviour
		/// as calling GetGlyph() ina a loop for each code point in specified range.
		///</summary>
		void LoadGlyphRange(CodePoint start, CodePoint end);
		
		///<summary>
		///</summary>
		void LoadKerningTable(CodePoint start, CodePoint end);
		
		///<summary>
		///</summary>
		void ResetKerningTable();

	private:
		/// @override Font::_LoadGlyph
		virtual const FontGlyph* _LoadGlyph(CodePoint codePoint) override final;
		
		/// @override Font::_LoadGlyphMetrics
		virtual const FontGlyphMetrics* _LoadGlyphMetrics(CodePoint codePoint) override final;
		
		/// @override Font::_LoadKerning
		virtual Int32 _LoadKerning(CodePoint left, CodePoint right) override final;

		// Creates new FReeType font layer of specified dimensions.
		FreeTypeFontLayer* _CreateNewLayer(const Vector2U32& layerDimensions);

		// Inserts glyph to the layer. If all layers are full, creates new if possible. Returns targetLayer and offset.
		std::pair<FreeTypeFontLayer*, Vector2U32> _InsertGlyph(CodePoint codePoint, const Vector2U32& glyphSize);

		// Updates glyph image to the texture.
		void _UpdateGlyphImage(Uint32 layerIndex, const Vector2U32& glyphOffset, const void* ftGlyphInfo);
	};


	EXS_LIBAPI_RESMANAGER std::unique_ptr<Font> CreateFreeTypeFont(RenderSystem* renderSystem, const FreeTypeFontDesc& fontDesc);


}


#endif /* __Exs_ResManager_FreeTypeFont_H__ */
