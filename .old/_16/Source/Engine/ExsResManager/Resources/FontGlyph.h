
#pragma once

#ifndef __Exs_ResManager_FontGlyph_H__
#define __Exs_ResManager_FontGlyph_H__

#include "FontBase.h"


namespace Exs
{


	class Font;

	
	///<summary>
	///</summary>
	struct FontGlyphBitmapInfo
	{
		// Number of layer within a font where bitmap is located.
		Uint32 layerIndex;

		// Offset from the beginning of the layer, in normalized [0;1] range.
		Vector2F offset;
		
		// Size of the bitmap, in normalized [0;1] range.
		Vector2F size;
	};

	
	///<summary>
	///</summary>
	struct FontGlyphMetrics
	{
		// Advance vector for the glyph, in font-specific units.
		Vector2I32 advance;
	
		// Bearing vector for the glyph, in font-specific units.
		Vector2I32 bearing;
	
		// Size the glyph, in font-specific units.
		Vector2U32 size;
	};

	
	///<summary>
	///</summary>
	struct FontGlyph
	{
		// Code point.
		CodePoint codePoint;

		// Glyph's bitmap info.
		FontGlyphBitmapInfo bitmapInfo;

		// Metrics of a glyph.
		FontGlyphMetrics metrics;

		// Position within layer.
		Vector2U32 position;
	};


}


#endif /* __Exs_ResManager_FontGlyph_H__ */
