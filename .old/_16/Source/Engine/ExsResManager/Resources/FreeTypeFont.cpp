
#include "_Precompiled.h"
#include <ExsRenderSystem/RenderSystem.h>
#include <ExsRenderSystem/Resource/ShaderResourceView.h>
#include <ExsRenderSystem/Resource/Texture.h>
#include <ExsResManager/Resources/FreeTypeFont.h>
#include <ExsResManager/Resources/FreeTypeInterface.h>
#include <ExsResManager/Resources/PixelFormatConverter.h>


namespace Exs
{


	FreeTypeFontLayer::FreeTypeFontLayer(Font* font, Size_t layerIndex, const Vector2U32& dimensions, const AtlasComposerConfig& composerConfig)
	: FontLayer(font, layerIndex, dimensions)
	, _composer(composerConfig)
	{ }


	bool FreeTypeFontLayer::InsertGlyph(CodePoint cp, const Vector2U32& glyphRect, Vector2U32* destOffset)
	{
		return this->_composer.AddRect(glyphRect, destOffset);
	}


	void FreeTypeFontLayer::OnLayerReset()
	{
		FontLayer::OnLayerReset();
		this->_composer.Reset();
	}




	FreeTypeFont::FreeTypeFont(std::unique_ptr<FreeTypeLoader>&& loader, const Vector3U32& dimensions, const ShaderResourceViewHandle& textureArraySRV)
	: Font(FontType::Free_Type, loader->GetFontSize(), loader->GetLineHeight(), loader->GetFontName(), dimensions.z)
	, _loader(std::move(loader))
	, _defaultLayerDimensions(Vector2U32(dimensions.x, dimensions.y))
	, _textureArraySRV(textureArraySRV)
	, _textureArray(textureArraySRV->GetResource()->As<Texture2DArray>())
	{ }


	FreeTypeFont::~FreeTypeFont()
	{ }


	void FreeTypeFont::LoadGlyphRange(CodePoint start, CodePoint end)
	{
		for (CodePoint cp = start; cp <= end; cp++)
		{
			ExsDebugCode(
				auto* fglyph = this->GetGlyph(cp);
				ExsDebugAssert( fglyph == nullptr );
			);

			this->_LoadGlyph(cp);
		}
	}
	

	void FreeTypeFont::LoadKerningTable(CodePoint start, CodePoint end)
	{
		for (CodePoint c1 = start; c1 <= end; ++c1)
		{
			for (CodePoint c2 = start; c2 <= end; ++c2)
			{
				if (Int32 kerning = this->_loader->LoadKerning(c1, c2))
				{
					FontKerningInfo kerningInfo;
					kerningInfo.charLeft = c1;
					kerningInfo.charRight = c2;
					kerningInfo.kerning = kerning;
					this->_kernings.insert(kerningInfo);
				}
			}
		}
	}


	void FreeTypeFont::ResetKerningTable()
	{
		this->_kernings.clear();
	}


	const FontGlyph* FreeTypeFont::_LoadGlyph(CodePoint codePoint)
	{
		if (const auto* loadedGlyph = this->_loader->LoadGlyph(codePoint))
		{
			const auto& glyphBitmap = loadedGlyph->image->bitmap;
			auto glyphInfo = this->_InsertGlyph(codePoint, Vector2U32(glyphBitmap.width, glyphBitmap.rows));

			if (glyphInfo.first != nullptr)
			{
				Uint32 layerIndex = glyphInfo.first->GetLayerIndex();
				const auto& layerDimensions = glyphInfo.first->GetDimensions();

				// Add glyph to the shared index and layer-specific list.
				FontGlyph* fontGlyph = this->AddGlyph(codePoint, layerIndex, glyphInfo.first);
				ExsDebugAssert( fontGlyph != nullptr );

				fontGlyph->codePoint = codePoint;
				fontGlyph->metrics.advance.x = loadedGlyph->metrics.horiAdvance;
				fontGlyph->metrics.advance.y = loadedGlyph->metrics.vertAdvance;
				fontGlyph->metrics.bearing.x = loadedGlyph->metrics.horiBearingX;
				fontGlyph->metrics.bearing.y = loadedGlyph->metrics.horiBearingY;
				fontGlyph->metrics.size.x = loadedGlyph->metrics.width;
				fontGlyph->metrics.size.y = loadedGlyph->metrics.height;

				fontGlyph->bitmapInfo.layerIndex = layerIndex;
				fontGlyph->bitmapInfo.offset.x = static_cast<float>(glyphInfo.second.x) / static_cast<float>(layerDimensions.x);
				fontGlyph->bitmapInfo.offset.y = static_cast<float>(glyphInfo.second.y) / static_cast<float>(layerDimensions.y);
				fontGlyph->bitmapInfo.size.x = static_cast<float>(glyphBitmap.width) / static_cast<float>(layerDimensions.x);
				fontGlyph->bitmapInfo.size.y = static_cast<float>(glyphBitmap.rows) / static_cast<float>(layerDimensions.y);

				if ((glyphBitmap.width > 0) && (glyphBitmap.buffer != nullptr))
				{
					// If glyph bitmap is not empty (it can happen if glyph represents a whitespace
					// character like tab or space), update texture subregion with renderer bitmap.

					this->_UpdateGlyphImage(layerIndex, glyphInfo.second, loadedGlyph);
				}

				return fontGlyph;
			}
		}

		return nullptr;
	}


	const FontGlyphMetrics* FreeTypeFont::_LoadGlyphMetrics(CodePoint codePoint)
	{
		return nullptr;
	}
	
	
	Int32 FreeTypeFont::_LoadKerning(CodePoint left, CodePoint right)
	{
		return 0;
	}
	

	FreeTypeFontLayer* FreeTypeFont::_CreateNewLayer(const Vector2U32& layerDimensions)
	{
		// Fetch current number of layers and check if we can create another one.
		Size_t currentLayersNum = this->_layers.size();

		// If not, simply return nullptr to indicate, that layer could not be created.
		if (currentLayersNum == this->_maxLayersNum)
			return nullptr;

		AtlasComposerConfig composerConfig;
		composerConfig.targetDimensions = layerDimensions;
		composerConfig.lineHeight = this->_fontLineHeight / 64;
		composerConfig.padding = Vector4U32( this->_fontSize / 3 );

		return this->AddLayer<FreeTypeFontLayer>(layerDimensions, composerConfig);
	}


	std::pair<FreeTypeFontLayer*, Vector2U32> FreeTypeFont::_InsertGlyph(CodePoint codePoint, const Vector2U32& glyphSize)
	{
		std::pair<FreeTypeFontLayer*, Vector2U32> insertInfo;
		insertInfo.first = nullptr;

		// Iterate overr all font layers and try to insert new glyph to one of them.
		// No additional policy is currently used - just pich the first which has space.

		for (auto& layerPtr : this->_layers)
		{
			auto* ftlayer = dbgsafe_ptr_cast<FreeTypeFontLayer*>(layerPtr.get());
			if (ftlayer->InsertGlyph(codePoint, glyphSize, &(insertInfo.second)))
			{
				insertInfo.first = ftlayer;
				break;
			}
		}

		// If insertInfo.first is still nullptr it means, that no layer has enough space
		// to store additional glyph.

		if (insertInfo.first == nullptr)
		{
			// Try to create new layer. On success, _CreateNewLayer() returns pointer to the created layer.
			if (auto* newLayer = this->_CreateNewLayer(this->_defaultLayerDimensions))
			{
				if (newLayer->InsertGlyph(codePoint, glyphSize, &(insertInfo.second)))
				{
					insertInfo.first = newLayer;
				}
			}
		}

		return insertInfo;
	}


	void FreeTypeFont::_UpdateGlyphImage(Uint32 layerIndex, const Vector2U32& glyphOffset, const void* ftGlyphInfo)
	{
		const FTGlyphInfo* glyphInfo = static_cast<const FTGlyphInfo*>(ftGlyphInfo);
		Uint32 pixelsNum = glyphInfo->bitmap.width * glyphInfo->bitmap.rows;

		TextureRange updateRange;
		updateRange.offset = Vector3U32(glyphOffset, 0);
		updateRange.size = Vector3U32(glyphInfo->bitmap.width, glyphInfo->bitmap.rows, 0);

		if (glyphInfo->bitmap.pixel_mode == FT_PIXEL_MODE_GRAY)
		{
			TextureData2DDesc dataDesc;
			dataDesc.dataSize = pixelsNum;
			dataDesc.rowPitch = glyphInfo->bitmap.pitch;
			dataDesc.dataPtr = glyphInfo->bitmap.buffer;

			this->_textureArray->UpdateSubtexture(layerIndex, updateRange, dataDesc);
		}
		else if (glyphInfo->bitmap.pixel_mode == FT_PIXEL_MODE_MONO)
		{
			stdx::dynamic_memory_buffer<Byte> a8Data;
			a8Data.resize(pixelsNum);

			PixelFormatConverter<ImagePixelFormat::Mono, ImagePixelFormat::Gray>::Convert(
				glyphInfo->bitmap.buffer, glyphInfo->bitmap.rows, glyphInfo->bitmap.width, a8Data.data_ptr());

			TextureData2DDesc dataDesc;
			dataDesc.dataSize = pixelsNum;
			dataDesc.rowPitch = glyphInfo->bitmap.width;
			dataDesc.dataPtr = a8Data.data_ptr();

			this->_textureArray->UpdateSubtexture(layerIndex, updateRange, dataDesc);
		}
		else if (glyphInfo->bitmap.pixel_mode == FT_PIXEL_MODE_BGRA)
		{
			stdx::dynamic_memory_buffer<Byte> a8Data;
			a8Data.resize(pixelsNum);

			PixelFormatConverter<ImagePixelFormat::Color_BGRA, ImagePixelFormat::Gray>::Convert(
				glyphInfo->bitmap.buffer, pixelsNum, a8Data.data_ptr());

			TextureData2DDesc dataDesc;
			dataDesc.dataSize = pixelsNum;
			dataDesc.rowPitch = glyphInfo->bitmap.width;
			dataDesc.dataPtr = a8Data.data_ptr();

			this->_textureArray->UpdateSubtexture(layerIndex, updateRange, dataDesc);
		}
		else
		{
			ExsDebugInterrupt();
		}
	}




	std::unique_ptr<Font> CreateFreeTypeFont(RenderSystem* renderSystem, const FreeTypeFontDesc& fontDesc)
	{
		// Create FT loader used by tis font to load requested glyphs.
		auto freeTypeLoader = CreateFreeTypeLoader(std::move(fontDesc.ttfFileData), fontDesc.fontSize, fontDesc.ttfFaceIndex);
		
		if (!freeTypeLoader)
		{
			return nullptr;
		}

		Texture2DArrayCreateInfo textureCreateInfo;
		textureCreateInfo.usageFlags = RSResourceUsage_Dynamic;
		textureCreateInfo.internalFormat = GraphicDataFormat::A8UNORM;
		textureCreateInfo.memoryAccess = RSMemoryAccess::Write_Only;
		textureCreateInfo.width = fontDesc.layerSize.x;
		textureCreateInfo.height = fontDesc.layerSize.y;
		textureCreateInfo.arraySize = fontDesc.layersNum;
		textureCreateInfo.mipmapLevelsNum = 1;
		textureCreateInfo.initDataDesc.arrayData = nullptr;
		textureCreateInfo.initDataDesc.arrayIndex = nullptr;
		textureCreateInfo.initDataDesc.arraySize = 0;

		auto fontTextureArray = renderSystem->CreateTexture2DArray(textureCreateInfo);
		auto fontTextureArraySRV = renderSystem->CreateTextureShaderResourceView(fontTextureArray);

		Vector3U32 fontSizeDesc(fontDesc.layerSize, fontDesc.layersNum);
		auto trueTypeFont = std::make_unique<FreeTypeFont>(std::move(freeTypeLoader), fontSizeDesc, fontTextureArraySRV);

		if (fontDesc.preRenderAscii)
			trueTypeFont->LoadGlyphRange(32, 126);

		if (fontDesc.loadKerningTable)
			trueTypeFont->LoadKerningTable(32, 126);

		return std::move(trueTypeFont);
	}


}
