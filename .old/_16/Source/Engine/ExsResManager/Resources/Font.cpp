
#include "_Precompiled.h"
#include <ExsResManager/Resources/Font.h>
#include <ExsResManager/Resources/FreeTypeFont.h>
#include <ExsRenderSystem/RenderSystem.h>
#include <ExsRenderSystem/Resource/ShaderResourceView.h>
#include <ExsRenderSystem/Resource/Texture.h>


namespace Exs
{


	FontLayer::FontLayer(Font* font, Size_t layerIndex, const Vector2U32& dimensions)
	: _font(font)
	, _layerIndex(truncate_cast<Uint32>(layerIndex))
	, _dimensions(dimensions)
	{ }


	void FontLayer::Reset()
	{
		this->_font->ResetLayer(this);
		this->OnLayerReset();
		this->_glyphs.clear();
	}


	void FontLayer::OnLayerReset()
	{ }




	Font::Font(FontType type, Uint32 fontSize, Uint32 fontLineHeight, const std::string& name, Size_t maxLayersNum)
	: _fontType(type)
	, _fontSize(fontSize)
	, _fontLineHeight(fontLineHeight)
	, _name(name)
	, _maxLayersNum(maxLayersNum)
	{
		// Fonts cannot be empty - they must have at least one glyph layer.
		ExsDebugAssert( maxLayersNum > 0 );

		if (maxLayersNum != Invalid_Length)
		{
			// If specified, reserve space to avoid reallocations.
			this->_layers.reserve(maxLayersNum);
		}
	}
	

	Font::~Font()
	{ }


	void Font::ResetLayer(FontLayer* layer)
	{
		// Fetch list of all glyphs stored in the layer.
		const auto& layerGlyphs = layer->GetGlyphs();

		for (auto& glyph : layerGlyphs)
		{
			// Erase layer from the internal list.
			this->_EraseGlyph(glyph);
		}
	}


	FontGlyph* Font::AddGlyph(CodePoint codePoint, Uint32 layerIndex, FontLayer* layerPtr)
	{
		ExsDebugAssert( layerPtr->GetLayerIndex() == layerIndex );

		auto& layer = this->_layers[layerIndex];
		ExsDebugAssert( layer.get() == layerPtr );

		// Add glyph to the layer. This is a simple insert to the vector of glyphs.
		layer->AddGlyph(codePoint);

		// Create GlyphData object which will store all data of the new glyph.
		// '.glyph' member is not initialized as this the job of the loading function.

		GlyphData glyphData;
		glyphData.codePoint = codePoint;
		glyphData.layerIndex = layerIndex;

		// Insert glyph to the internal index of glyphs.

		auto glyphDataRef = this->_glyphs.insert(std::pair<CodePoint, GlyphData>(codePoint, glyphData));
		return &(glyphDataRef.first->second.glyph);
	}


	const FontGlyph* Font::_LoadGlyph(CodePoint codePoint)
	{
		return nullptr;
	}
	
	
	const FontGlyphMetrics* Font::_LoadGlyphMetrics(CodePoint codePoint)
	{
		return nullptr;
	}
	
	
	Int32 Font::_LoadKerning(CodePoint left, CodePoint right)
	{
		return 0;
	}


	void Font::_EraseGlyph(CodePoint codePoint)
	{
		auto glyphDataRef = this->_glyphs.find(codePoint);
		ExsDebugAssert( glyphDataRef != this->_glyphs.end() );

		this->_glyphs.erase(glyphDataRef);
	}


}
