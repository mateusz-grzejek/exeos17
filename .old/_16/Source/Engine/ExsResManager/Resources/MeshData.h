
#pragma once

#ifndef __Exs_ResManager_MeshData_H__
#define __Exs_ResManager_MeshData_H__

#include "MeshBase.h"


namespace Exs
{


	template <class Vertex_t = Vertex3DDefault>
	struct MeshData : public ResourceData
	{
	public:
		typedef Vertex_t VertexType;
		typedef Uint32 IndexType;

		typedef Array<Vertex_t> Vertices;
		typedef Array<Uint32> Indices;

	public:
		Vertices    vertices;
		Indices     indices;
		Size_t      verticesNum;
		Size_t      verticesSize;
		Size_t      indicesNum;
		Size_t      indicesSize;
		bool        hasNormals;

	public:
		MeshData()
		: verticesNum(0)
		, verticesSize(0)
		, indicesNum(0)
		, indicesSize(0)
		, hasNormals(false)
		{ }
		
		MeshData(MeshData&&) = default;
		MeshData(const MeshData&) = default;
		
		MeshData& operator=(MeshData&&) = default;
		MeshData& operator=(const MeshData&) = default;

		void Swap(MeshData& other)
		{
			swap(this->vertices, other.vertices);
			swap(this->indices, other.indices);
			swap(this->verticesNum, other.verticesNum);
			swap(this->verticesSize, other.verticesSize);
			swap(this->indicesNum, other.indicesNum);
			swap(this->indicesSize, other.indicesSize);
			swap(this->hasNormals, other.hasNormals);
		}
	};


}


#endif /* __Exs_ResManager_MeshData_H__ */
