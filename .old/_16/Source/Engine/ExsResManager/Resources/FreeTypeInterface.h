
#pragma once

#ifndef __Exs_ResManager_FreeTypeLoader_H__
#define __Exs_ResManager_FreeTypeLoader_H__

#include "../Prerequisites.h"
#include <ft2build.h>
#include <freetype/freetype.h>
#include <freetype/ftbitmap.h>
#include <freetype/ftglyph.h>
#include <unordered_map>


namespace Exs
{

	
	///<summary>
	/// Represents glyph data loaded from FreeType.
	///</summary>
	struct FTGlyphData
	{
		FT_BitmapGlyph image = nullptr;

		FT_Glyph_Metrics metrics;
	};

	
	typedef std::remove_pointer<FT_GlyphSlot>::type FTGlyphInfo;

	typedef stdx::dynamic_data_array<FT_Byte> FTFaceData;


	template <typename T>
	using FTHandle = std::shared_ptr<T>;


	class FreeTypeLoader
	{
		EXS_DECLARE_NONCOPYABLE(FreeTypeLoader);

	private:
		struct GlyphInfo
		{
			FT_Glyph glyph = nullptr;

			FTGlyphData data;
		};

		typedef std::unordered_map<CodePoint, GlyphInfo> GlyphCache;

	private:
		FT_Library    _ftLibrary;
		FT_Face       _ftFace;
		FTFaceData    _ftFaceData;
		Uint32        _fontSize;
		Uint32        _lineHeight;
		GlyphCache    _glyphCache;

	public:
		FreeTypeLoader(FT_Library ftLibrary, FT_Face ftFace, FTFaceData&& faceData, Uint32 fontSize);
		~FreeTypeLoader();

		const FTGlyphData* LoadGlyph(CodePoint codePoint);

		const FT_Glyph_Metrics* LoadGlyphMetrics(CodePoint codePoint);

		Int32 LoadKerning(CodePoint left, CodePoint right);

		const char* GetFontName() const;

		Uint32 GetFontSize() const;

		Uint32 GetLineHeight() const;

	private:
		//
		GlyphInfo* _GetGlyphInfo(CodePoint codePoint);

		//
		bool _InternalLoad(CodePoint codePoint, GlyphInfo* glyphInfo);
	};


	inline const char* FreeTypeLoader::GetFontName() const
	{
		return this->_ftFace->family_name;
	}

	inline Uint32 FreeTypeLoader::GetFontSize() const
	{
		return this->_fontSize;
	}

	inline Uint32 FreeTypeLoader::GetLineHeight() const
	{
		return this->_ftFace->height;
	}


	EXS_LIBAPI_RESMANAGER std::unique_ptr<FreeTypeLoader> CreateFreeTypeLoader(FTFaceData&& faceData, Uint32 fontSize, Uint32 faceIndex);


}


#endif /* __Exs_ResManager_FreeTypeLoader_H__ */
