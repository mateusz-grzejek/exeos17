
#pragma once

#ifndef __Exs_ResManager_AtlasComposer_H__
#define __Exs_ResManager_AtlasComposer_H__

#include "../Prerequisites.h"


namespace Exs
{


	struct AtlasComposerConfig
	{
		Vector2U32 targetDimensions;

		Uint32 lineHeight;

		Vector4U32 padding;
	};


	class AtlasComposer
	{
	private:
		const Vector2U32    _targetDimensions;
		const Vector4U32    _padding;
		const Uint32        _lineHeight;
		const Uint32        _maxWidth;
		const Uint32        _maxHeight;
		Vector2U32          _currentOffset;
		Uint32              _rectsNum;

	public:
		AtlasComposer(const Vector2U32& targetDimensions, Uint32 lineHeight, const Vector4U32& padding = Vector4U32(0))
		: _targetDimensions(targetDimensions)
		, _padding(padding)
		, _lineHeight(lineHeight + (padding.y + padding.w))
		, _maxWidth(targetDimensions.x - (padding.x + padding.z))
		, _maxHeight(lineHeight)
		, _currentOffset(0, 0)
		, _rectsNum(0)
		{ }

		AtlasComposer(const AtlasComposerConfig& config)
		: AtlasComposer(config.targetDimensions, config.lineHeight, config.padding)
		{ }

		bool AddRect(const Vector2U32& rectSize, Vector2U32* insertPos)
		{
			if ((rectSize.x > this->_maxWidth) || (rectSize.y > this->_maxHeight))
				return false;

			Uint32 destWidth = this->_padding.x + rectSize.x + this->_padding.z;
			Uint32 destHeight = this->_padding.y + rectSize.y + this->_padding.w;

			Uint32 hspaceLeft = this->_targetDimensions.x - this->_currentOffset.x;
			

			if (hspaceLeft < destWidth)
			{
				Uint32 vspaceLeft = this->_targetDimensions.y - (this->_currentOffset.y + this->_lineHeight);

				if (vspaceLeft < this->_lineHeight)
					return false;

				this->_currentOffset.x = 0;
				this->_currentOffset.y += this->_lineHeight;
			}
			
			insertPos->x = this->_currentOffset.x + this->_padding.x;
			insertPos->y = this->_currentOffset.y + this->_padding.y;

			this->_currentOffset.x += destWidth;
			++this->_rectsNum;

			return true;
		}

		void Reset()
		{
			this->_currentOffset.x = 0;
			this->_currentOffset.y = 0;
			this->_rectsNum = 0;
		}

		bool CheckSpace(const Vector2U32& rectSize)
		{
			if ((rectSize.x > this->_maxWidth) || (rectSize.y > this->_maxHeight))
				return false;

			Uint32 destWidth = this->_padding.x + rectSize.x + this->_padding.z;
			Uint32 hspaceLeft = this->_targetDimensions.x - this->_currentOffset.x;

			if ((hspaceLeft < destWidth) && (this->_currentOffset.y + this->_lineHeight > this->_targetDimensions.y))
				return false;

			return true;
		}
	};


}


#endif /* __Exs_ResManager_AtlasComposer_H__ */
