
#include "_Precompiled.h"
#include <ExsCore/Singleton.h>
#include <ExsCore/Threading/ThreadLocalStorage.h>


namespace Exs
{


	void** GetSingletonStorageTLS(Enum singletonSlot)
	{
		ExsDebugAssert( singletonSlot < Config::TLS_Singleton_Array_Size );

		ThreadLocalStorage* tls = CurrentThread::GetLocalStorage();
		return &(tls->singletons[singletonSlot]);
	}


	void RegisterThreadLocalSingleton(void* singletonPtr, SingletonDestroyProc destroyProc)
	{
		ThreadLocalStorage* tls = CurrentThread::GetLocalStorage();

		// ThreadSingletonsManager* singletons_manager = tls->singletons_manager;
		// singletons_manager->RegisterSingleton(singletonPtr, destroyProc);
		
		EXS_UNUSED( singletonPtr );
		EXS_UNUSED( destroyProc );
	}


}
