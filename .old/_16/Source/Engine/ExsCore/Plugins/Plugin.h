
#pragma once

#ifndef __Exs_Core_Plugin_H__
#define __Exs_Core_Plugin_H__

#include "PluginBase.h"
#include "../System/DynamicLibrary.h"


namespace Exs
{


	class EXS_LIBCLASS_CORE Plugin
	{
		friend class PluginHandle;
		friend class PluginSystem;

	protected:
		PluginSystem*             _pluginSystem;
		PluginManager*            _manager;
		PluginID                  _id;
		PluginType                _type;
		PluginHandle              _handle;
		std::string               _name;
		std::string               _moduleName;
		stdx::sync_ref_counter    _useCount;
		bool                      _regState;
		
	protected:
		///<param name="manager"> Manager which this plugin is managed by. </param>
		Plugin(PluginManager* manager);

		virtual ~Plugin();
		
	public:
		///<summary>
		/// Casts the plugin to a plugin type registered under Plugin_type_tag. Returns pointer to the
		/// plugin on success or nullptr otherwise (i.e. plugin type does not match).
		///</summary>
		template <PluginType Plugin_type_tag>
		typename PluginTypeProperties<Plugin_type_tag>::PluginType* GetAs();
		
		///<summary>
		/// Casts the plugin to a plugin type registered under Plugin_type_tag. Returns pointer to the
		/// plugin on success or nullptr otherwise (i.e. plugin type does not match).
		///</summary>
		template <PluginType Plugin_type_tag>
		const typename PluginTypeProperties<Plugin_type_tag>::PluginType* GetAs() const;
		
		///<summary>
		/// Casts the plugin to a type registered under Plugin_type_tag. Returns pointer to the
		/// plugin on success or nullptr otherwise (i.e. plugin type does not match).
		///</summary>
		Result Unload();
		
		///<summary>
		/// Returns name of the module which contains this plugin (i.e. name of dynamic library file).
		///</summary>
		const std::string& GetModuleName() const;
		
		///<summary>
		/// Returns name of the plugin, as specified by the plugin module.
		///</summary>
		const std::string& GetName() const;
		
		///<summary>
		/// Returns plugin manager which this plugin is registered in (that is, manager associated with this->_type).
		///</summary>
		PluginManager* GetManager() const;
		
		///<summary>
		/// Returns ID of the plugin.
		///</summary>
		PluginID GetID() const;
		
		///<summary>
		/// Returns type of the plugin.
		///</summary>
		PluginType GetType() const;
		
		///<summary>
		/// Returns type of the plugin.
		///</summary>
		const PluginHandle& GetHandle() const;
		
		///<summary>
		/// Returns current value of use counter of the plugin. Use counter is incremented each time new handle
		/// to the plugin is created. Plugin cannot be unloaded as long as this value is greater than 1.
		///</summary>
		Uint32 GetUseCount() const;

	private:
		// Increments use counter if it is not zero. Returns current value of reference counter (0 on failure).
		Uint32 AddUseRef();

		// Decrements use counter. Does not unload plugin, as last reference is always held by PluginSystem.
		Uint32 RemoveUseRef();

		//
		void SetDesc(PluginID id, const std::string& moduleName, const std::string& name);

		//
		void SetHandle(const PluginHandle& pluginHandle);

		//
		void ReleaseHandle();
	};


	template <PluginType Plugin_type_tag>
	inline typename PluginTypeProperties<Plugin_type_tag>::PluginType* Plugin::GetAs()
	{
		typedef typename PluginTypeProperties<Plugin_type_tag>::PluginType PluginType;
		return (this->_type == Plugin_type_tag) ? static_cast<PluginType*>(this) : nullptr;
	}
	
	template <PluginType Plugin_type_tag>
	inline const typename PluginTypeProperties<Plugin_type_tag>::PluginType* Plugin::GetAs() const
	{
		typedef typename PluginTypeProperties<Plugin_type_tag>::PluginType PluginType;
		return (this->_type == Plugin_type_tag) ? static_cast<const PluginType*>(this) : nullptr;
	}

	inline const std::string& Plugin::GetModuleName() const
	{
		return this->_moduleName;
	}

	inline const std::string& Plugin::GetName() const
	{
		return this->_name;
	}

	inline PluginManager* Plugin::GetManager() const
	{
		return this->_manager;
	}

	inline PluginID Plugin::GetID() const
	{
		return this->_id;
	}

	inline PluginType Plugin::GetType() const
	{
		return this->_type;
	}

	inline const PluginHandle& Plugin::GetHandle() const
	{
		return this->_handle;
	}

	inline Uint32 Plugin::GetUseCount() const
	{
		auto refs_num = this->_useCount.get_value();
		return truncate_cast<Uint32>(refs_num);
	}

	inline Uint32 Plugin::AddUseRef()
	{
		auto refs_num = this->_useCount.increment_cnz();
		return truncate_cast<Uint32>(refs_num);
	}

	inline Uint32 Plugin::RemoveUseRef()
	{
		auto refs_num = this->_useCount.decrement();
		return truncate_cast<Uint32>(refs_num);
	}

	inline void Plugin::SetDesc(PluginID id, const std::string& moduleName, const std::string& name)
	{
		this->_id = id;
		this->_moduleName = moduleName;
		this->_name = name;
	}

	inline void Plugin::SetHandle(const PluginHandle& pluginHandle)
	{
		this->_handle = pluginHandle;
	}
	
	inline void Plugin::ReleaseHandle()
	{
		this->_handle.Release();
	}


}


#endif /* __Exs_Core_Plugin_H__ */
