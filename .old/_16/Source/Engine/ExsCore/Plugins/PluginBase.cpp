
#include "_Precompiled.h"
#include <ExsCore/Plugins/Plugin.h>


namespace Exs
{


	bool PluginHandle::AddPluginUseRef(Plugin* plugin)
	{
		Uint32 refCount = plugin->AddUseRef();
		return refCount > 0;
	}


	void PluginHandle::RemovePluginUseRef(Plugin* plugin)
	{
		plugin->RemoveUseRef();
	}


	Uint32 PluginHandle::GetPluginUseRef(Plugin* plugin)
	{
		return plugin->GetUseCount();
	}


}
