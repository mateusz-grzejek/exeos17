
#include "_Precompiled.h"
#include <ExsCore/Plugins/PluginManager.h>
#include <ExsCore/Plugins/Plugin.h>
#include <ExsCore/Plugins/PluginLoader.h>
#include <ExsCore/Plugins/PluginSystem.h>


namespace Exs
{


	PluginManager::PluginManager(PluginSystem* pluginSystem, PluginType type, const std::string& name)
	: _pluginSystem(pluginSystem)
	, _type(type)
	, _name(name)
	{ }


	PluginManager::~PluginManager()
	{ }


	Result PluginManager::RegisterPlugin(Plugin* plugin)
	{
		ExsDebugAssert( plugin != nullptr );

		PluginID pluginID = plugin->GetID();
		ExsDebugAssert( pluginID != Invalid_Plugin_ID );

		PluginInfo pluginInfo;
		pluginInfo.plugin = plugin;

		this->_plugins.insert(pluginID, pluginInfo);
		this->OnRegisterPlugin(plugin);

		return ExsResult(RSC_Success);
	}


	Result PluginManager::UnregisterPlugin(Plugin* plugin)
	{
		ExsDebugAssert( plugin != nullptr );
		PluginID pluginID = plugin->GetID();

		auto pluginRef = this->_plugins.find(pluginID);
		ExsDebugAssert( pluginRef != this->_plugins.end() );

		this->OnUnregisterPlugin(plugin);
		this->_plugins.erase(pluginRef);

		return ExsResult(RSC_Success);
	}


	void PluginManager::OnRegisterPlugin(Plugin* plugin)
	{ }


	void PluginManager::OnUnregisterPlugin(Plugin* plugin)
	{ }


}
