
#include "_Precompiled.h"
#include <ExsCore/Plugins/PluginLoader.h>
#include <ExsCore/Plugins/Plugin.h>
#include <ExsCore/Plugins/PluginManager.h>


namespace Exs
{


	Result PluginValidator::ValidateModuleData(const PluginModuleData* moduleData)
	{
		return ExsResult(RSC_Success);
	}


	Result PluginValidator::ValidateModuleInterface(const PluginModuleInterface* moduleInterface)
	{
		return ExsResult(RSC_Success);
	}
	


	
	PluginLoader::PluginLoader(PluginSystem* pluginSystem)
	: _pluginSystem(pluginSystem)
	{ }


	PluginLoader::~PluginLoader()
	{ }


	Plugin* PluginLoader::Initialize(const PluginCoreData& pluginCoreData, PluginManager* manager, PluginValidator* validator)
	{
		const char* pluginName = pluginCoreData.moduleData->name;

		// Validate module data and interface using specified validator object.
		Result validationResult = this->Validate(pluginCoreData.moduleData, pluginCoreData.moduleInterface, validator);

		if (validationResult != RSC_Success)
		{
			ExsTraceError(TRC_Core_Plugins, "%s: validation has failed.", pluginName);
			return nullptr;
		}

		// Create plugin object using procedure exposed by the module. This executes inside the DLL library.
		Plugin* pluginPtr = pluginCoreData.moduleInterface->onInitProc(&(pluginCoreData.dllLibray), manager);

		if (pluginPtr == nullptr)
		{
			ExsTraceError(TRC_Core_Plugins, "%s: initialization has failed (onInitProc() returned NULL).", pluginName);
			return nullptr;
		}

		this->OnPluginInitialize(pluginCoreData, pluginPtr);
		
		return pluginPtr;
	}


	void PluginLoader::Release(const PluginCoreData& pluginCoreData, Plugin* plugin)
	{
		this->OnPluginRelease(pluginCoreData, plugin);
		
		// Release plugin object using procedure exposed by the module. This executes inside the DLL library.
		pluginCoreData.moduleInterface->onReleaseProc(&(pluginCoreData.dllLibray), plugin);
	}


	Result PluginLoader::LoadPlugin(const std::string& pluginFile, PluginCoreData* pluginCoreData)
	{
		// Load dynamic library with plugin.
		DynamicLibrary pluginDLL = DynamicLibrary::Load(pluginFile, DLLPathType::Base);

		if (!pluginDLL.IsLoaded())
		{
			ExsTraceNotification(TRC_Core_Plugins, "%s: not found.", pluginFile.c_str());
			return ExsResult(RSC_Err_DLL_Missing);
		}

		// Retrieve plugin data, containing base information about plugin: its name, version, supported architecture, etc.
		// It is used to validate and initialize plugin object.
		const PluginModuleData* pluginModuleData =
			pluginDLL.RetrieveSymbolAs<PluginModuleData*>(EXS_PLUGIN_MODULE_PROPERTIES_REF_NAME_STR);

		// Retrieve plugin interface, containing procedures exposed by the library: init and release proc, etc.
		// It is used to create and destroy plugin objects (it is the responsibility of the module to do it).
		const PluginModuleInterface* pluginModuleInterface =
			pluginDLL.RetrieveSymbolAs<PluginModuleInterface*>(EXS_PLUGIN_MODULE_INTERFACE_REF_NAME_STR);

		// If any of the above is missing, plugin is considered invalid.
		if ((pluginModuleData == nullptr) || (pluginModuleInterface == nullptr))
		{
			ExsTraceWarning(TRC_Core_Plugins, "%s: missing module data (info and/or interface).", pluginFile.c_str());
			return ExsResult(RSC_Plugin_Err_Missing_Module_Data);
		}
		
		pluginCoreData->dllLibray = pluginDLL;
		pluginCoreData->moduleData = pluginModuleData;
		pluginCoreData->moduleInterface = pluginModuleInterface;

		return ExsResult(RSC_Success);
	}


	Result PluginLoader::Validate(const PluginModuleData* moduleData, const PluginModuleInterface* moduleInterface, PluginValidator* validator)
	{
		// Required data validation:
		// - target architecture.
		// Required module validation:
		// - init proc may not be NULL,
		// - release proc may not be NULL.

		if (moduleData->archID != EXS_TARGET_ARCHITECTURE)
			return ExsResult(RSC_Err_Arch_Not_Compatible);

		if (moduleInterface->onInitProc == nullptr)
			return ExsResult(RSC_Err_DLL_Module_Invalid);
		
		if (moduleInterface->onReleaseProc == nullptr)
			return ExsResult(RSC_Err_DLL_Module_Invalid);

		// If validator is not empty, perform additional validation.
		if (validator != nullptr)
		{
			Result dataValidationResult = validator->ValidateModuleData(moduleData);
			if (dataValidationResult != RSC_Success)
			{
				ExsTraceError(TRC_Core_Plugins, "%s: invalid plugin module data.", moduleData->name);
				return dataValidationResult;
			}

			Result interfaceValidationResult = validator->ValidateModuleInterface(moduleInterface);
			if (interfaceValidationResult != RSC_Success)
			{
				ExsTraceError(TRC_Core_Plugins, "%s: invalid plugin module interface.", moduleData->name);
				return interfaceValidationResult;
			}
		}

		return ExsResult(RSC_Success);
	}


	void PluginLoader::OnPluginInitialize(const PluginCoreData& pluginCoreData, Plugin* plugin)
	{ }


	void PluginLoader::OnPluginRelease(const PluginCoreData& pluginCoreData, Plugin* plugin)
	{ }


}
