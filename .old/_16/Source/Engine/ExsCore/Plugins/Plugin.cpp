
#include "_Precompiled.h"
#include <ExsCore/Plugins/Plugin.h>
#include <ExsCore/Plugins/PluginSystem.h>
#include <ExsCore/Plugins/PluginManager.h>


namespace Exs
{


	Plugin::Plugin(PluginManager* manager)
	: _pluginSystem(manager->GetPluginSystem())
	, _manager(manager)
	, _id(Invalid_Plugin_ID)
	, _type(manager->GetType())
	, _useCount(1)
	, _regState(false)
	{ }


	Plugin::~Plugin()
	{ }


	Result Plugin::Unload()
	{
		Result unloadResult = this->_pluginSystem->UnloadPlugin(this->_id);

		if (unloadResult != RSC_Success)
			return unloadResult;

		return ExsResult(RSC_Success);
	}


}
