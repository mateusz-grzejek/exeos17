
#include "_Precompiled.h"
#include <ExsCore/Exception.h>
#include <ExsCore/Plugins/PluginSystem.h>
#include <ExsCore/Plugins/Plugin.h>
#include <ExsCore/Plugins/PluginLoader.h>
#include <ExsCore/Plugins/PluginManager.h>
#include <stdx/string_utils.h>


namespace Exs
{


	PluginSystem::PluginSystem()
	{ }


	PluginSystem::~PluginSystem()
	{
		this->UnloadAll();
	}


	std::pair<PluginHandle, Result> PluginSystem::LoadPlugin(const char* pluginFile, PluginLoader* loader, PluginValidator* validator)
	{
		// If no loader was specified, use the default one. It loads plugin using default loading policy.
		if (loader == nullptr)
		{
			if (!this->_defaultLoader)
			{
				// If default loader has not been used yet, create it now.
				this->_defaultLoader = std::make_unique<PluginLoader>(this);
			}

			loader = this->_defaultLoader.get();
		}

		// This will store ref data of our plugin.
		PluginCoreData pluginCoreData { };

		// Loads plugin module - loads DLL library and checks if all required data (public exports and interfaces) are
		// present in that library. Returns RSC_Succes on succes or relevant error code in case of failure.
		Result loadResult = loader->LoadPlugin(pluginFile, &pluginCoreData);

		if (loadResult != RSC_Success)
		{
			ExsTraceError(TRC_Core_Plugins, "%s: plugin loading has failed.", pluginFile);
			return std::pair<PluginHandle, Result>(nullptr, loadResult);
		}

		// Fetch manager registered for this plugin type. All loaded plugins must have their managers already registered
		// by the time they are loaded. If manager is not registsred, plugin cannot be loaded.
		PluginManager* manager = this->GetManager(pluginCoreData.moduleData->type);

		if (manager == nullptr)
		{
			ExsTraceError(TRC_Core_Plugins, "Manager for plugin %s has not been found.", pluginFile);
			return std::pair<PluginHandle, Result>(nullptr, ExsResult(RSC_Plugin_Manager_Not_Found));
		}

		// Initialize plugin. Initialization will also validate plugin internals using specified validator object.
		Plugin* pluginObject = loader->Initialize(pluginCoreData, manager, validator);

		if (pluginObject == nullptr)
		{
			ExsTraceError(TRC_Core_Plugins, "Initialization of plugin %s has failed.", pluginFile);
			return std::pair<PluginHandle, Result>(nullptr, ExsResult(RSC_Plugin_Initialization_Failed));
		}

		// Generate plugin id and create first handle object. Initialized handle will NOT increment
		// use counter of the plugin (it is initial set to 1 when plugin object is created), so plugin
		// system holds initially the only active handle to the plugin.

		PluginID pluginID = PluginUtils::CreatePluginID(pluginCoreData.moduleData->name);
		PluginHandle pluginHandle = PluginHandle::Create(pluginObject, pluginID);
		ExsDebugAssert( pluginObject->GetUseCount() == 1 );

		InternalPluginData internalPluginData;
		internalPluginData.handle = pluginHandle;
		internalPluginData.name = pluginCoreData.moduleData->name;
		internalPluginData.loader = loader;
		internalPluginData.manager = manager;
		internalPluginData.plugin = pluginObject;
		internalPluginData.refData = pluginCoreData;

		auto loadedStateRef = this->_plugins.insert(pluginID, std::move(internalPluginData));
		this->_pluginsLoadOrderList.push_back(pluginID);

		stdx::split_string(
			pluginCoreData.moduleData->keywords, ',',
			[loadedStateRef](const char* str, size_t length) -> void {
				loadedStateRef->value.keywords.push_back(std::string(str, length));
			});

		pluginObject->SetDesc(pluginID, pluginCoreData.dllLibray.GetFileName(), pluginCoreData.moduleData->name);
		pluginObject->SetHandle(pluginHandle);
		manager->RegisterPlugin(pluginObject);

		ExsTraceInfo(TRC_Core_Plugins, "Plugin loaded: %s ver. %s (%s). PID: 0x%" EXS_PFU32_HEX ".",
			pluginObject->GetModuleName().c_str(), pluginCoreData.moduleData->versionStr, pluginCoreData.moduleData->archName, pluginID);

		return std::pair<PluginHandle, Result>(pluginHandle, ExsResult(RSC_Success));
	}


	Result PluginSystem::UnloadPlugin(PluginID pluginID)
	{
		auto pluginRef = this->_plugins.find(pluginID);

		if (pluginRef == this->_plugins.end())
			return ExsResult(RSC_Err_Not_Found);

		auto pluginOrderedRef = std::find_if (
			this->_pluginsLoadOrderList.begin(),
			this->_pluginsLoadOrderList.end(),
			[pluginID](const PluginID& id) -> bool {
				return id == pluginID;
			});

		if (pluginOrderedRef == this->_pluginsLoadOrderList.end())
			ExsExceptionThrowEx(EXC_Invalid_State, "");

		InternalPluginData& internalPluginData = pluginRef->value;
		Plugin* pluginObject = internalPluginData.plugin;

		// Check use count of the plugin. Plugin cannot be unloaded if it is still used somewhere.
		// Note, that this check is not thread-safe, as this counter may change in the meanwhile.
		// It is the responsibility of the application to ensure, that plugin is not used by other
		// threads by the time it is being unloaded.
		Uint32 pluginUseCount = pluginObject->GetUseCount();

		// Currently stored plugin use refs:
		// - handle in plugin object
		// - plugin data in internal list
		const Uint32 currentlyAcquiredPluginUseRefsNum = 2;

		if (pluginUseCount > currentlyAcquiredPluginUseRefsNum)
		{
			ExsTraceWarning(TRC_Core_Plugins,
				"Plugin 0x%" EXS_PFU64_HEX " still in use (extra refs: %u).", pluginID, pluginUseCount - currentlyAcquiredPluginUseRefsNum);
			return ExsResult(RSC_Err_Still_In_Use);
		}

		// Copy DLL object to prevent automatic unload when plugin entry is erased from the container
		// (DynamicLibrary object is ref-counted and container stores its only active instance).
		DynamicLibrary pluginDLL = internalPluginData.refData.dllLibray;
		std::string pluginName = pluginObject->GetName();

		// Unregister plugin from its manager (simply removes plugin from the list).
		internalPluginData.manager->UnregisterPlugin(pluginObject);

		// Release handle stored by the plugin object. Use refs -= 1.
		pluginObject->ReleaseHandle();
		
		// Erase plugin data from the list of registered plugins. Use refs -= 1.
		this->_plugins.erase(pluginRef);
		this->_pluginsLoadOrderList.erase(pluginOrderedRef);

		// There should be no active references left. If this fails, we have a desynchro error.
		if (pluginObject->GetUseCount() > 0)
			ExsExceptionThrowEx(EXC_Invalid_State, "");

		// Release plugin. This will destroy Plugin object by calling its release procedure.
		internalPluginData.loader->Release(internalPluginData.refData, pluginObject);

		// Explicitly unload DLL module from the memory.
		pluginDLL.Release();

		// Nice to have some info if unloading process completes successfuly.
		ExsTraceInfo(TRC_Core_Plugins, "Plugin unloaded: %s (0x%" EXS_PFU64_HEX ").", pluginName.c_str(), pluginID);

		return ExsResult(RSC_Success);
	}


	Result PluginSystem::UnloadAll()
	{
		if (!this->_plugins.empty())
		{
			auto currentID = this->_pluginsLoadOrderList.rbegin();
			auto end = this->_pluginsLoadOrderList.rend();

			for ( ; currentID != end; ++currentID)
			{
				Result unloadingResult = this->UnloadPlugin(*currentID);
				ExsDebugAssert( unloadingResult == RSC_Success );
			}
		}

		ExsDebugAssert( this->_plugins.empty() );
		ExsDebugAssert( this->_pluginsLoadOrderList.empty() );

		return ExsResult(RSC_Success);
	}


	PluginManager* PluginSystem::GetManager(PluginType type) const
	{
		auto managerRef = this->_managers.find(type);
		return (managerRef != this->_managers.end()) ? managerRef->value.get() : nullptr;
	}


	PluginHandle PluginSystem::GetPlugin(PluginID pluginID) const
	{
		auto pluginRef = this->_plugins.find(pluginID);
		return (pluginRef != this->_plugins.end()) ? pluginRef->value.handle : nullptr;
	}


	PluginHandle PluginSystem::GetPlugin(const char* name) const
	{
		for (auto& pluginInfo : this->_plugins)
		{
			if (pluginInfo.value.name == name)
				return pluginInfo.value.handle;
		}

		return nullptr;
	}


}
