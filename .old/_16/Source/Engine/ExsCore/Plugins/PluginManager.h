
#pragma once

#ifndef __Exs_Core_PluginManager_H__
#define __Exs_Core_PluginManager_H__

#include "PluginBase.h"
#include <stdx/assoc_array.h>


namespace Exs
{


	///<summary>
	/// This class is reponsible for managing plugins of a single type. It is never instatiated directly - specific components
	/// should be implemented as separate types which inherit this class.
	///</summary>
	class EXS_LIBCLASS_CORE PluginManager
	{
		friend class PluginSystem;

	private:
		struct PluginInfo
		{
			Plugin* plugin;
		};

		typedef stdx::assoc_array<PluginID, PluginInfo> PluginsList;

	protected:
		PluginSystem*    _pluginSystem; // Pointer to the main management system.
		PluginType    _type; // Type of contained plugins.
		PluginsList      _plugins; // List of all plugins.
		std::string      _name; // Name of this manager.
		
	protected:
		///<param name="pluginSystem"> Instance of main plugin system interface. </param>
		///<param name="pluginsType"> Type of plugins stored in this manager. </param>
		///<param name="name"> Name of this manager (should not be empty, although it is not a restriction). </param>
		PluginManager(PluginSystem* pluginSystem, PluginType type, const std::string& name);
		
		virtual ~PluginManager();
		
	public:
		///<summary>
		/// Casts the manager to a manager type registered under Plugin_type_tag. Returns pointer to the
		/// manager on success or nullptr otherwise (i.e. manager type does not match).
		///</summary>
		template <PluginType Plugin_type_tag>
		typename PluginTypeProperties<Plugin_type_tag>::ManagerType* GetAs();
		
		///<summary>
		/// Casts the manager to a manager type registered under Plugin_type_tag. Returns pointer to the
		/// manager on success or nullptr otherwise (i.e. manager type does not match).
		///</summary>
		template <PluginType Plugin_type_tag>
		const typename PluginTypeProperties<Plugin_type_tag>::ManagerType* GetAs() const;

		///<summary>
		/// Returns name of this manager. Manager-specific, no guarantees regarding content of this string.
		///</summary>
		const std::string& GetName() const;
		
		///<summary>
		/// Returns list of all plugins currently registered in this manager.
		///</summary>
		const PluginsList& GetPluginsList() const;
		
		///<summary>
		/// Returns type of plugins stored in this manager.
		///</summary>
		PluginType GetType() const;
		
		///<summary>
		/// Returns pointer to the instance of main plugin system.
		///</summary>
		PluginSystem* GetPluginSystem() const;
		
		///<summary>
		/// Returns number of plugins currently registered in this manager.
		///</summary>
		Size_t GetPluginsNum() const;
		
	friendapi:
		// Register plugin and adds it to the list of plugins managed by this manager.
		// Used by PluginSystem during plugin loading.
		Result RegisterPlugin(Plugin* plugin);
		
		// Unregister plugin and removes it from the list of plugins managed by this manager.
		// Used by PluginSystem during plugin unloading.
		Result UnregisterPlugin(Plugin* plugin);

	private:
		// Called during plugin registration, after it is added to the list.
		virtual void OnRegisterPlugin(Plugin* plugin);

		// Called during plugin unregistration, before it removed from the list.
		virtual void OnUnregisterPlugin(Plugin* plugin);
	};
	

	template <PluginType Plugin_type_tag>
	inline typename PluginTypeProperties<Plugin_type_tag>::ManagerType* PluginManager::GetAs()
	{
		typedef typename PluginTypeProperties<Plugin_type_tag>::ManagerType ManagerType;
		return (this->_type == Plugin_type_tag) ? static_cast<ManagerType*>(this) : nullptr;
	}
	
	template <PluginType Plugin_type_tag>
	inline const typename PluginTypeProperties<Plugin_type_tag>::ManagerType* PluginManager::GetAs() const
	{
		typedef typename PluginTypeProperties<Plugin_type_tag>::ManagerType ManagerType;
		return (this->_type == Plugin_type_tag) ? static_cast<const ManagerType*>(this) : nullptr;
	}

	inline const std::string& PluginManager::GetName() const
	{
		return this->_name;
	}

	inline const PluginManager::PluginsList& PluginManager::GetPluginsList() const
	{
		return this->_plugins;
	}

	inline PluginType PluginManager::GetType() const
	{
		return this->_type;
	}

	inline PluginSystem* PluginManager::GetPluginSystem() const
	{
		return this->_pluginSystem;
	}

	inline Size_t PluginManager::GetPluginsNum() const
	{
		return this->_plugins.size();
	}


}


#endif /* __Exs_Core_PluginManager_H__ */
