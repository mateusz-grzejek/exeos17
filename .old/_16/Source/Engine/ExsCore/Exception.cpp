
#include "_Precompiled.h"
#include <ExsCore/Exception.h>
#include <sstream>


namespace Exs
{


	Exception::Exception(ExceptionType type, const char* baseTypeName, const ExceptionInfo& info) noexcept
	: std::exception()
	, _type(type)
	, _baseTypeName(baseTypeName)
	, _code(info.code)
	, _name(info.name)
	, _description(info.description ? info.description : "<empty>")
	, _srcLocationInfo(info.srcInfo)
	{ }


	Exception::~Exception()
	{ }


	std::string Exception::ToString() const
	{
		std::ostringstream strStream;

		strStream << "[Exception]\n";
		strStream << "- Code: " << std::hex << this->_code << std::dec << "\n";
		strStream << "- Type: " << this->_baseTypeName << "\n";
		strStream << "- Name: " << this->_name << "\n";
		strStream << "- Description: " << this->_description << "\n";
		strStream << "- Source: " << this->_srcLocationInfo;

		return strStream.str();
	}
	
	
	
	
	void DefaultExceptionHandler(const Exception& exception)
	{
		std::string exceptionString = exception.ToString();
		ExsTraceException(TRC_None, exceptionString.c_str());
	}
	
	
	void DefaultExceptionHandler(const std::exception& exception)
	{
		const char* exceptionString = exception.what();
		ExsTraceException(TRC_None, exceptionString);
	}


}
