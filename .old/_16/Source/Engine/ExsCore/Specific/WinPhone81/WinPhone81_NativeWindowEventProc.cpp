
#include <ExsCore/NativeWindow.h>
#include <ExsCore/EvtSystemAppCommand.h>
#include <ExsCore/EvtSystemInputKeyboard.h>
#include <ExsCore/EvtSystemInputMouse.h>
#include <ExsCore/EvtSystemInputTouch.h>
#include <ExsCore/EvtSystemWindowNotification.h>


namespace Exs
{


	bool NativeWindow::ProcessEvtCommon(UINT msgCode, WPARAM wParam, LPARAM lParam)
	{
		return true;
	}


	bool NativeWindow::ProcessEvtKeyboard(UINT msgCode, WPARAM wParam, LPARAM lParam)
	{
		return true;
	}


	bool NativeWindow::ProcessEvtMouse(UINT msgCode, WPARAM wParam, LPARAM lParam)
	{
		return true;
	}


}
