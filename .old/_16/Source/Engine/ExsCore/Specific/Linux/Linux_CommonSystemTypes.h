
#pragma once

#ifndef __Exs_Core_CommonSystemTypes_Linux_H__
#define __Exs_Core_CommonSystemTypes_Linux_H__


namespace Exs
{


	///<summary>
	///</summary>
	struct X11SystemEnvData
	{
		Display* display;

		int screen;

		Window rootWindow;

		Atom wmpDeleteWindow;
	};


	///<summary>
	///</summary>
	struct X11SystemEventInfo
	{
		const XEvent* xEvent;

		const X11SystemEnvData* x11SystemEnvData;
	};


	///<summary>
	///</summary>
	struct X11SystemWindowData
	{
		//
		Colormap colorMap;

		//
		long eventMask;
	};


}


#endif /* __Exs_Core_CommonSystemTypes_Linux_H__ */
