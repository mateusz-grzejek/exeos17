
#include <ExsCore/System/SystemEventDispatcher.h>
#include <ExsCore/System/SystemEventReceiver.h>
#include <ExsCore/System/EvtSysApp.h>
#include <ExsCore/System/EvtSysInput.h>
#include <ExsCore/System/EvtSysWindow.h>
#include <ExsCore/System/KeyboardKeyCodes.h>
#include <X11/Xlib.h>
#include "Linux_CommonSystemTypes.h"


namespace Exs
{


	bool X11TranslateAndDispatchAppEvent(SystemEventDispatcher* eventDispatcher, const X11SystemEventInfo& eventInfo);
	bool X11TranslateAndDispatchInputEvent(SystemEventDispatcher* eventDispatcher, const X11SystemEventInfo& eventInfo);
	bool X11TranslateAndDispatchWindowEvent(SystemEventDispatcher* eventDispatcher, const X11SystemEventInfo& eventInfo);

	SystemKeyCode X11GetSystemKeyCode(KeySym xKeySym);
	

	bool SystemEventDispatcher::TranslateAndDispatch(const SystemEventInfo& eventInfo)
	{
		if (X11TranslateAndDispatchInputEvent(this, eventInfo))
			return true;

		if (X11TranslateAndDispatchAppEvent(this, eventInfo))
			return true;

		if (X11TranslateAndDispatchWindowEvent(this, eventInfo))
			return true;

		return false;
	}


	bool X11TranslateAndDispatchAppEventCommand(SystemEventDispatcher* eventDispatcher, const X11SystemEventInfo& eventInfo)
	{
		return false;
	}


	bool X11TranslateAndDispatchInputEvent(SystemEventDispatcher* eventDispatcher, const X11SystemEventInfo& eventInfo)
	{
		switch(eventInfo.xEvent->type)
		{
			case KeyPress:
				{
					KeySym xKeySym = XKeycodeToKeysym(eventInfo.x11SystemEnvData->display, eventInfo.xEvent->xkey.keycode, 0);
					SystemKeyCode keyCode = X11GetSystemKeyCode(xKeySym);
					EvtSysInputKeyboardKeyPressed event { keyCode, nullptr, 1 };
					eventDispatcher->DispatchEvent(event);
				}
				break;

			default:
				return false;
		}

		return true;
	}


	bool X11TranslateAndDispatchWindowEvent(SystemEventDispatcher* eventDispatcher, const X11SystemEventInfo& eventInfo)
	{
		switch(eventInfo.xEvent->type)
		{
		case ClientMessage:
			{
				// Type of wm protocol message is stored in data.l[0].
				long wmpMessageType = eventInfo.xEvent->xclient.data.l[0];

				if (wmpMessageType == eventInfo.x11SystemEnvData->wmpDeleteWindow)
				{
					EvtSysWindowCommandClose event { };
					eventDispatcher->DispatchEvent(event);
				};
			}
			break;

		default:
			return false;
		}

		return true;
	}


	static const SystemKeyCode asciiKeyCodeMap_20_7a[] =
	{
		/* 0x0020 */ KeyboardKey_Space,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x0030 */ KeyboardKey_Num_0,
		/* 0x0031 */ KeyboardKey_Num_1,
		/* 0x0032 */ KeyboardKey_Num_2,
		/* 0x0033 */ KeyboardKey_Num_3,
		/* 0x0034 */ KeyboardKey_Num_4,
		/* 0x0035 */ KeyboardKey_Num_5,
		/* 0x0036 */ KeyboardKey_Num_6,
		/* 0x0037 */ KeyboardKey_Num_7,
		/* 0x0038 */ KeyboardKey_Num_8,
		/* 0x0039 */ KeyboardKey_Num_9,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x0041 */ KeyboardKey_Char_A,
		/* 0x0042 */ KeyboardKey_Char_B,
		/* 0x0043 */ KeyboardKey_Char_C,
		/* 0x0044 */ KeyboardKey_Char_D,
		/* 0x0045 */ KeyboardKey_Char_E,
		/* 0x0046 */ KeyboardKey_Char_F,
		/* 0x0047 */ KeyboardKey_Char_G,
		/* 0x0048 */ KeyboardKey_Char_H,
		/* 0x0049 */ KeyboardKey_Char_I,
		/* 0x004a */ KeyboardKey_Char_J,
		/* 0x004b */ KeyboardKey_Char_K,
		/* 0x004c */ KeyboardKey_Char_L,
		/* 0x004d */ KeyboardKey_Char_M,
		/* 0x004e */ KeyboardKey_Char_N,
		/* 0x004f */ KeyboardKey_Char_O,
		/* 0x0050 */ KeyboardKey_Char_P,
		/* 0x0051 */ KeyboardKey_Char_Q,
		/* 0x0052 */ KeyboardKey_Char_R,
		/* 0x0053 */ KeyboardKey_Char_S,
		/* 0x0054 */ KeyboardKey_Char_T,
		/* 0x0055 */ KeyboardKey_Char_U,
		/* 0x0056 */ KeyboardKey_Char_V,
		/* 0x0057 */ KeyboardKey_Char_W,
		/* 0x0058 */ KeyboardKey_Char_X,
		/* 0x0059 */ KeyboardKey_Char_Y,
		/* 0x005a */ KeyboardKey_Char_Z,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x00 */ KeyboardKey_Unknown,
		/* 0x0061 */ KeyboardKey_Char_A,
		/* 0x0062 */ KeyboardKey_Char_B,
		/* 0x0063 */ KeyboardKey_Char_C,
		/* 0x0064 */ KeyboardKey_Char_D,
		/* 0x0065 */ KeyboardKey_Char_E,
		/* 0x0066 */ KeyboardKey_Char_F,
		/* 0x0067 */ KeyboardKey_Char_G,
		/* 0x0068 */ KeyboardKey_Char_H,
		/* 0x0069 */ KeyboardKey_Char_I,
		/* 0x006a */ KeyboardKey_Char_J,
		/* 0x006b */ KeyboardKey_Char_K,
		/* 0x006c */ KeyboardKey_Char_L,
		/* 0x006d */ KeyboardKey_Char_M,
		/* 0x006e */ KeyboardKey_Char_N,
		/* 0x006f */ KeyboardKey_Char_O,
		/* 0x0070 */ KeyboardKey_Char_P,
		/* 0x0071 */ KeyboardKey_Char_Q,
		/* 0x0072 */ KeyboardKey_Char_R,
		/* 0x0073 */ KeyboardKey_Char_S,
		/* 0x0074 */ KeyboardKey_Char_T,
		/* 0x0075 */ KeyboardKey_Char_U,
		/* 0x0076 */ KeyboardKey_Char_V,
		/* 0x0077 */ KeyboardKey_Char_W,
		/* 0x0078 */ KeyboardKey_Char_X,
		/* 0x0079 */ KeyboardKey_Char_Y,
		/* 0x007a */ KeyboardKey_Char_Z,
	};

	static const SystemKeyCode extraKeyCodeMap_50_57[] =
	{
		/* 0xff50 */ KeyboardKey_Home,
		/* 0xff51 */ KeyboardKey_Arrow_Left,
		/* 0xff52 */ KeyboardKey_Arrow_Up,
		/* 0xff53 */ KeyboardKey_Arrow_Right,
		/* 0xff54 */ KeyboardKey_Arrow_Down,
		/* 0xff55 */ KeyboardKey_Page_Up,
		/* 0xff56 */ KeyboardKey_Page_Down,
		/* 0xff57 */ KeyboardKey_End
	};

	static const SystemKeyCode extraKeyCodeMap_aa_b9[] =
	{
		/* 0xffaa */ KeyboardKey_Numpad_Multiply,
		/* 0xffab */ KeyboardKey_Numpad_Add,
		/* 0xffac */ KeyboardKey_Unknown,
		/* 0xffad */ KeyboardKey_Numpad_Substract,
		/* 0xffae */ KeyboardKey_Numpad_Decimal,
		/* 0xffaf */ KeyboardKey_Numpad_Divide,
		/* 0xffb0 */ KeyboardKey_Numpad_0,
		/* 0xffb1 */ KeyboardKey_Numpad_1,
		/* 0xffb2 */ KeyboardKey_Numpad_2,
		/* 0xffb3 */ KeyboardKey_Numpad_3,
		/* 0xffb4 */ KeyboardKey_Numpad_4,
		/* 0xffb5 */ KeyboardKey_Numpad_5,
		/* 0xffb6 */ KeyboardKey_Numpad_6,
		/* 0xffb7 */ KeyboardKey_Numpad_7,
		/* 0xffb8 */ KeyboardKey_Numpad_8,
		/* 0xffb9 */ KeyboardKey_Numpad_9,
	};

	static const SystemKeyCode extraKeyCodeMap_be_c9[] =
	{
		/* 0xffbe */ KeyboardKey_F1,
		/* 0xffbf */ KeyboardKey_F2,
		/* 0xffc0 */ KeyboardKey_F3,
		/* 0xffc1 */ KeyboardKey_F4,
		/* 0xffc2 */ KeyboardKey_F5,
		/* 0xffc3 */ KeyboardKey_F6,
		/* 0xffc4 */ KeyboardKey_F7,
		/* 0xffc5 */ KeyboardKey_F8,
		/* 0xffc6 */ KeyboardKey_F9,
		/* 0xffc7 */ KeyboardKey_F10,
		/* 0xffc8 */ KeyboardKey_F11,
		/* 0xffc9 */ KeyboardKey_F12
	};

	static const SystemKeyCode extraKeyCodeMap_e1_ea[] =
	{
		/* 0xffe1 */ KeyboardKey_Shift_Left,
		/* 0xffe2 */ KeyboardKey_Shift_Right,
		/* 0xffe3 */ KeyboardKey_Control_Left,
		/* 0xffe4 */ KeyboardKey_Control_Right,
		/* 0xffe5 */ KeyboardKey_Caps_Lock,
		/* 0xffe6 */ KeyboardKey_Unknown,
		/* 0xffe7 */ KeyboardKey_Unknown,
		/* 0xffe8 */ KeyboardKey_Unknown,
		/* 0xffe9 */ KeyboardKey_Alt_Left,
		/* 0xffea */ KeyboardKey_Alt_Right
	};


	SystemKeyCode X11GetSystemKeyCode(KeySym xKeySym)
	{
		if ((xKeySym >= 0x0020) && (xKeySym <= 0x007a))
		{
			return GetConstantArrayElement(asciiKeyCodeMap_20_7a, static_cast<Uint32>(xKeySym) - 0x0020);
		}

		if ((xKeySym >= 0xff50) && (xKeySym <= 0xffea))
		{
			if ((xKeySym >= 0xff50) && (xKeySym <= 0xff57))
				return GetConstantArrayElement(extraKeyCodeMap_50_57, static_cast<Uint32>(xKeySym) - 0xff50);

			if ((xKeySym >= 0xffaa) && (xKeySym <= 0xffb9))
				return GetConstantArrayElement(extraKeyCodeMap_aa_b9, static_cast<Uint32>(xKeySym) - 0xffaa);

			if ((xKeySym >= 0xffbe) && (xKeySym <= 0xffc9))
				return GetConstantArrayElement(extraKeyCodeMap_be_c9, static_cast<Uint32>(xKeySym) - 0xffbe);

			if ((xKeySym >= 0xffe1) && (xKeySym <= 0xffea))
				return GetConstantArrayElement(extraKeyCodeMap_e1_ea, static_cast<Uint32>(xKeySym) - 0xffe1);
		}

		if (xKeySym == XK_Tab)
			return KeyboardKey_Tab;
		else if (xKeySym == XK_Return)
			return KeyboardKey_Enter;
		else if (xKeySym == XK_Escape)
			return KeyboardKey_Escape;
		else if (xKeySym == XK_BackSpace)
			return KeyboardKey_Backspace;
		else if (xKeySym == XK_Insert)
			return KeyboardKey_Insert;
		else if (xKeySym == XK_Delete)
			return KeyboardKey_Delete;

		return KeyboardKey_Unknown;
	}


}
