
#include <ExsCore/System/AppObject.h>
#include <ExsCore/System/SystemWindow.h>
#include <ExsCore/Threading/ThreadLauncher.h>


namespace Exs
{


	bool AppObjectInitializer::InitializeSystemData(SystemEnvData* systemEnvData)
	{
		ExsTraceInfo(TRC_Core_System, "InitializeSystemData(%p).", systemEnvData);

		// Initialize Xlib for multi-threaded usage.
		int status = XInitThreads();

		if (status == False)
		{
			ExsTraceError(TRC_Core_System, "XInitThreads() has failed!");
			return false;
		}

		if (Display* display = XOpenDisplay(nullptr))
		{
			const char* displayString = XDisplayString(display);
			const char* serverVendor = XServerVendor(display);
			int connectionNumber = XConnectionNumber(display);

			ExsTraceInfo(TRC_Core_System, "Display: %p", display);
			ExsTraceInfo(TRC_Core_System, "ID: %d (%s)", connectionNumber, displayString);
			ExsTraceInfo(TRC_Core_System, "Vendor: %s", serverVendor);

			systemEnvData->display = display;
			systemEnvData->screen = XDefaultScreen(display);
			systemEnvData->rootWindow = XRootWindow(display, systemEnvData->screen);
			systemEnvData->wmpDeleteWindow = XInternAtom(display, "WM_DELETE_WINDOW", False);

			return true;
		}

		return false;
	}

	void AppObjectInitializer::ReleaseSystemData(SystemEnvData* systemEnvData)
	{
		ExsTraceInfo(TRC_Core_System, "AppObjectInitializer: ReleaseSystemData(%p).", systemEnvData);

		if (systemEnvData->display != nullptr)
		{
			// XCloseDisplay(systemEnvData->display);

			systemEnvData->display = nullptr;
			systemEnvData->rootWindow = X11_Null_Window;
			systemEnvData->screen = X11_Invalid_Screen;
			systemEnvData->wmpDeleteWindow = X11C_None;
		}
	}


}
