
#include <ExsCore/NativeWindow.h>
#include <ExsCore/PlatformUtils.h>


namespace Exs
{


	NativeWindow::NativeWindow(SystemEventDispatcher* eventDispatcher)
	: NativeWindowBase(eventDispatcher)
	{ }
	

	NativeWindow::~NativeWindow()
	{
		this->Release();
	}


	Result NativeWindow::Initialize(ANativeWindow* aNativeWindow)
	{
		if (this->_handle != nullptr)
			return RSC_Err_Invalid_Operation;

		this->_handle = aNativeWindow;

		return RSC_Success;
	}


	void NativeWindow::Release()
	{
	}
	
	
	Result NativeWindow::Resize(const Size& size, WindowAreaType areaType)
	{
		return RSC_Success;
	}


	Size NativeWindow::GetClientAreaSize() const
	{
		return Size(0, 0);
	}


	bool NativeWindow::IsValid() const
	{
		return false;
	}

	
	bool NativeWindow::IsVisible() const
	{
		return false;
	}


}
