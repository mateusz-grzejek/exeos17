
#include <ExsCore/Prerequisites.h>
#include "Android_AppThread.h"
#include <ExsCore/AppObject.h>
#include <ExsCore/CoreAppWindow.h>
#include <ExsCore/NativeWindow.h>
#include <ExsCore/ThreadLauncher.h>
#include <android/native_activity.h>
#include <android/native_window.h>


namespace Exs
{


	AppObject::AppObject(const SharedExecutionStateRefPtr& sharedExecutionState, ANativeActivity* activity)
	: AppObjectBase(sharedExecutionState)
	, _nativeActivity(activity)
	{
		this->InitializeActivity();
	}


	AppObject::~AppObject()
	{ }


	Result AppObject::StartAppThread(const std::function<Result()>& onUpdateCallback)
	{
		UniqueSyncObject syncObject { };
		ThreadLaunchInfo launchInfo { &syncObject, true };

		auto launchResult = ThreadLauncher::Launch<AndroidAppThread>(this->_sharedExecutionState.GetPtr(), &launchInfo, this);

		if (launchResult.second != RSC_Success)
			return RSC_Err_Failed;

		launchResult.first->SetRunPermission();
		launchResult.first->WaitForReadyState(&syncObject);

		return RSC_Success;
	}


	Result AppObject::CreateCoreWindow(ANativeWindow* aNativeWindow)
	{
		auto nativeWindow = MakeSharedObject<NativeWindow>(&(this->_eventDispatcher));
		nativeWindow->Initialize(aNativeWindow);

		auto coreWindow = MakeSharedObject<CoreAppWindow>(nativeWindow);
		this->_coreWindow = coreWindow;

		return RSC_Success;
	}


	Result AppObject::DestroyCoreWindow()
	{
		if (!this->_coreWindow)
			return RSC_Err_Invalid_Operation;

		// NativeWindow* coreNativeWindow = this->_coreWindow->GetNativeWindow();
		// coreNativeWindow->Release();

		this->_coreWindow = nullptr;

		return RSC_Success;
	}


	void AppObject::InitializeActivity()
	{
		this->_nativeActivity->instance = this;
		this->_nativeActivity->callbacks->onStart = OnStart;
		this->_nativeActivity->callbacks->onStop = OnStop;
		this->_nativeActivity->callbacks->onPause = OnPause;
		this->_nativeActivity->callbacks->onResume = OnResume;
		this->_nativeActivity->callbacks->onDestroy = OnDestroy;
		this->_nativeActivity->callbacks->onLowMemory = OnLowMemory;
		this->_nativeActivity->callbacks->onConfigurationChanged = OnConfigurationChanged;
		this->_nativeActivity->callbacks->onWindowFocusChanged = OnWindowFocusChanged;
		this->_nativeActivity->callbacks->onNativeWindowCreated = OnNativeWindowCreated;
		this->_nativeActivity->callbacks->onNativeWindowDestroyed = OnNativeWindowDestroyed;
		this->_nativeActivity->callbacks->onNativeWindowResized = OnNativeWindowResized;
		this->_nativeActivity->callbacks->onNativeWindowRedrawNeeded = OnNativeWindowRedrawNeeded;
		this->_nativeActivity->callbacks->onInputQueueCreated = OnInputQueueCreated;
		this->_nativeActivity->callbacks->onInputQueueDestroyed = OnInputQueueDestroyed;
		this->_nativeActivity->callbacks->onContentRectChanged = OnContentRectChanged;
		this->_nativeActivity->callbacks->onSaveInstanceState = OnSaveInstanceState;
	}


	void AppObject::OnStart(ANativeActivity* activity)
	{
		// AppObject* appObject = reinterpret_cast<AppObject*>(activity->instance);
	}


	void AppObject::OnStop(ANativeActivity* activity)
	{
	}


	void AppObject::OnPause(ANativeActivity* activity)
	{
	}


	void AppObject::OnResume(ANativeActivity* activity)
	{
	}


	void AppObject::OnDestroy(ANativeActivity* activity)
	{
	}


	void AppObject::OnLowMemory(ANativeActivity* activity)
	{
	}


	void AppObject::OnConfigurationChanged(ANativeActivity* activity)
	{
	}


	void AppObject::OnWindowFocusChanged(ANativeActivity* activity, int focusState)
	{
	}


	void AppObject::OnNativeWindowCreated(ANativeActivity* activity, ANativeWindow* window)
	{
	}


	void AppObject::OnNativeWindowDestroyed(ANativeActivity* activity, ANativeWindow* window)
	{
	}


	void AppObject::OnNativeWindowResized(ANativeActivity* activity, ANativeWindow* window)
	{
	}


	void AppObject::OnNativeWindowRedrawNeeded(ANativeActivity* activity, ANativeWindow* window)
	{
	}


	void AppObject::OnInputQueueCreated(ANativeActivity* activity, AInputQueue* queue)
	{
	}


	void AppObject::OnInputQueueDestroyed(ANativeActivity* activity, AInputQueue* queue)
	{
	}


	void AppObject::OnContentRectChanged(ANativeActivity* activit, const ARect* recty)
	{
	}


	void* AppObject::OnSaveInstanceState(ANativeActivity* activity, size_t* outSize)
	{
		return nullptr;
	}


}
