
#include <ExsCore/Prerequisites.h>
#include "Android_AppThread.h"


namespace Exs
{


	AndroidAppThread::AndroidAppThread( ThreadUID parentUID,
	                                    ThreadBaseInfo* threadBaseInfo,
	                                    SharedExecutionState* sharedExecutionState,
	                                    AppObject* appObject)
	: ActiveThread(parentUID, threadBaseInfo, sharedExecutionState, "AndroidAppThread")
	, _appObject(appObject)
	{ }
	

	AndroidAppThread::~AndroidAppThread()
	{ }


	void AndroidAppThread::Entry()
	{
	}


}
