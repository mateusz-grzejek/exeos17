
#pragma once

#ifndef __Exs_Core_AppObject_Android_H__
#define __Exs_Core_AppObject_Android_H__


struct AInputQueue;
struct ANativeActivity;
struct ANativeWindow;
struct ARect;


namespace Exs
{


	class AndroidAppThread;


	class EXS_LIBCLASS_CORE AppObject : public AppObjectBase
	{
		friend class AndroidAppThread;

	private:
		ANativeActivity*     _nativeActivity;
		AndroidAppThread*    _appThread;

	public:
		AppObject(const SharedExecutionStateRefPtr& sharedExecutionState, ANativeActivity* activity);
		virtual ~AppObject();

		Result StartAppThread(const std::function<Result()>& onUpdateCallback);
		
		Result CreateCoreWindow(ANativeWindow* aNativeWindow);
		Result DestroyCoreWindow();

	private:
		using AppObjectBase::RunEventLoop;

		void InitializeActivity();

		static void OnStart(ANativeActivity* activity);
		static void OnStop(ANativeActivity* activity);
		static void OnPause(ANativeActivity* activity);
		static void OnResume(ANativeActivity* activity);
		static void OnDestroy(ANativeActivity* activity);
		static void OnLowMemory(ANativeActivity* activity);
		static void OnConfigurationChanged(ANativeActivity* activity);
		static void OnWindowFocusChanged(ANativeActivity* activity, int focusState);
		static void OnNativeWindowCreated(ANativeActivity* activity, ANativeWindow* window);
		static void OnNativeWindowDestroyed(ANativeActivity* activity, ANativeWindow* window);
		static void OnNativeWindowResized(ANativeActivity* activity, ANativeWindow* window);
		static void OnNativeWindowRedrawNeeded(ANativeActivity* activity, ANativeWindow* window);
		static void OnInputQueueCreated(ANativeActivity* activity, AInputQueue* queue);
		static void OnInputQueueDestroyed(ANativeActivity* activity, AInputQueue* queue);
		static void OnContentRectChanged(ANativeActivity* activit, const ARect* recty);
		static void* OnSaveInstanceState(ANativeActivity* activity, size_t* outSize);
	};


}


#endif /* __Exs_Core_AppObject_Android_H__ */
