
#include <ExsCore/SystemEventDispatcher.h>
#include <ExsCore/SystemEventReceiver.h>
#include <ExsCore/EvtSysAppCommand.h>
#include <ExsCore/EvtSysInputKeyboard.h>
#include <ExsCore/EvtSysInputMouse.h>
#include <ExsCore/EvtSysInputTouch.h>
#include <ExsCore/EvtSysWindowCommand.h>
#include <ExsCore/EvtSysWindowNotification.h>


namespace Exs
{


	SystemEventDispatcher::SystemEventDispatcher(SystemEventReceiver* eventReceiver)
	: SystemEventDispatcherBase(eventReceiver)
	{ }


	SystemEventDispatcher::~SystemEventDispatcher()
	{ }


	void SystemEventDispatcher::DispatchEvent(const SystemEventDataAndroid* eventMessage)
	{
		TranslateAndDispatch(eventMessage, this->_eventReceiver);
	}


	void SystemEventDispatcher::TranslateAndDispatch(const SystemEventDataAndroid* eventMessage, SystemEventReceiver* eventReceiver)
	{
	}


}
