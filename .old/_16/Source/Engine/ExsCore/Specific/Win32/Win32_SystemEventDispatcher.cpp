
#include "_Precompiled.h"
#include <ExsCore/System/SystemEventDispatcher.h>
#include <ExsCore/System/EvtSysApp.h>
#include <ExsCore/System/EvtSysInput.h>
#include <ExsCore/System/EvtSysWindow.h>
#include <Windowsx.h> // For GET_{X|Y}_LPARAM() macros.


namespace Exs
{


	bool Win32TranslateAndDispatchAppEvent(SystemEventDispatcher* eventDispatcher, const Win32SystemEventInfo& eventInfo);
	bool Win32TranslateAndDispatchInputEvent(SystemEventDispatcher* eventDispatcher, const Win32SystemEventInfo& eventInfo);
	bool Win32TranslateAndDispatchWindowEvent(SystemEventDispatcher* eventDispatcher, const Win32SystemEventInfo& eventInfo);

	bool Win32TranslateAndDispatchAppEventCommand(SystemEventDispatcher* eventDispatcher, const Win32SystemEventInfo& eventInfo);
	bool Win32TranslateAndDispatchInputEventKeyboard(SystemEventDispatcher* eventDispatcher, const Win32SystemEventInfo& eventInfo);
	bool Win32TranslateAndDispatchInputEventMouse(SystemEventDispatcher* eventDispatcher, const Win32SystemEventInfo& eventInfo);
	bool Win32TranslateAndDispatchInputEventTouch(SystemEventDispatcher* eventDispatcher, const Win32SystemEventInfo& eventInfo);
	bool Win32TranslateAndDispatchWindowEventNotification(SystemEventDispatcher* eventDispatcher, const Win32SystemEventInfo& eventInfo);

	SystemKeyCode GetSystemKeyCode(WPARAM win32KeyID);


	bool SystemEventDispatcher::TranslateAndDispatch(const SystemEventInfo& eventInfo)
	{
		if (Win32TranslateAndDispatchInputEvent(this, eventInfo))
			return true;

		if (Win32TranslateAndDispatchAppEvent(this, eventInfo))
			return true;

		if (Win32TranslateAndDispatchWindowEvent(this, eventInfo))
			return true;

		return false;
	}




	bool Win32TranslateAndDispatchAppEvent(SystemEventDispatcher* eventDispatcher, const Win32SystemEventInfo& eventInfo)
	{
		return Win32TranslateAndDispatchAppEventCommand(eventDispatcher, eventInfo);
	}


	bool Win32TranslateAndDispatchInputEvent(SystemEventDispatcher* eventDispatcher, const Win32SystemEventInfo& eventInfo)
	{
		if ((eventInfo.messageCode >= WM_MOUSEFIRST) && (eventInfo.messageCode <= WM_MOUSELAST))
		{
			return Win32TranslateAndDispatchInputEventMouse(eventDispatcher, eventInfo);
		}
		else if ((eventInfo.messageCode >= WM_KEYFIRST) && (eventInfo.messageCode <= WM_KEYLAST))
		{
			return Win32TranslateAndDispatchInputEventKeyboard(eventDispatcher, eventInfo);
		}
		else if (eventInfo.messageCode == WM_TOUCH)
		{
			return Win32TranslateAndDispatchInputEventTouch(eventDispatcher, eventInfo);
		}

		return false;
	}


	bool Win32TranslateAndDispatchWindowEvent(SystemEventDispatcher* eventDispatcher, const Win32SystemEventInfo& eventInfo)
	{
		return Win32TranslateAndDispatchWindowEventNotification(eventDispatcher, eventInfo);
	}


	bool Win32TranslateAndDispatchAppEventCommand(SystemEventDispatcher* eventDispatcher, const Win32SystemEventInfo& eventInfo)
	{
		return false;
	}


	bool Win32TranslateAndDispatchInputEventKeyboard(SystemEventDispatcher* eventDispatcher, const Win32SystemEventInfo& eventInfo)
	{
		switch(eventInfo.messageCode)
		{
		case WM_KEYDOWN:
			{
				SystemKeyCode keyCode = GetSystemKeyCode(eventInfo.wParam);
				Uint32 repeatCount = static_cast<Uint32>(eventInfo.lParam & 0xFFFF);
				EvtSysInputKeyboardKeyPressed event { keyCode, nullptr, repeatCount };
				eventDispatcher->DispatchEvent(event);
			}
			break;

		case WM_KEYUP:
			{
				SystemKeyCode keyCode = GetSystemKeyCode(eventInfo.wParam);
				EvtSysInputKeyboardKeyReleased event { keyCode, nullptr };
				eventDispatcher->DispatchEvent(event);
			}
			break;

		default:
			return false;
		}

		return true;
	}


	bool Win32TranslateAndDispatchInputEventMouse(SystemEventDispatcher* eventDispatcher, const Win32SystemEventInfo& eventInfo)
	{
		Position cursorPos { GET_X_LPARAM(eventInfo.lParam), GET_Y_LPARAM(eventInfo.lParam) };

		switch(eventInfo.messageCode)
		{
		case WM_MOUSEMOVE:
			{
				EvtSysInputMouseMove event { cursorPos };
				eventDispatcher->DispatchEvent(event);
			}
			break;

		case WM_LBUTTONDOWN:
			{
				EvtSysInputMouseButtonPressed event { MouseButtonID::Left, cursorPos };
				eventDispatcher->DispatchEvent(event);
			}
			break;

		case WM_LBUTTONUP:
			{
				EvtSysInputMouseButtonReleased event { MouseButtonID::Left, cursorPos };
				eventDispatcher->DispatchEvent(event);
			}
			break;

		case WM_LBUTTONDBLCLK:
			{
				EvtSysInputMouseButtonDblclick event { MouseButtonID::Left, cursorPos };
				eventDispatcher->DispatchEvent(event);
			}
			break;

		case WM_RBUTTONDOWN:
			{
				EvtSysInputMouseButtonPressed event { MouseButtonID::Right, cursorPos };
				eventDispatcher->DispatchEvent(event);
			}
			break;

		case WM_RBUTTONUP:
			{
				EvtSysInputMouseButtonReleased event { MouseButtonID::Right, cursorPos };
				eventDispatcher->DispatchEvent(event);
			}
			break;

		case WM_RBUTTONDBLCLK:
			{
				EvtSysInputMouseButtonDblclick event { MouseButtonID::Right, cursorPos };
				eventDispatcher->DispatchEvent(event);
			}
			break;

		case WM_MBUTTONDOWN:
			{
				EvtSysInputMouseButtonPressed event { MouseButtonID::Middle, cursorPos };
				eventDispatcher->DispatchEvent(event);
			}
			break;

		case WM_MBUTTONUP:
			{
				EvtSysInputMouseButtonReleased event { MouseButtonID::Middle, cursorPos };
				eventDispatcher->DispatchEvent(event);
			}
			break;

		case WM_MBUTTONDBLCLK:
			{
				EvtSysInputMouseButtonDblclick event { MouseButtonID::Middle, cursorPos };
				eventDispatcher->DispatchEvent(event);
			}
			break;

		case WM_MOUSEWHEEL:
			{
				Int16 scrollDelta = GET_WHEEL_DELTA_WPARAM(eventInfo.wParam);
				EvtSysInputMouseWheelScroll event { cursorPos, scrollDelta };
				eventDispatcher->DispatchEvent(event);
			}
			break;

		case WM_XBUTTONDOWN:
			{
				MouseButtonID xbuttonID = (GET_XBUTTON_WPARAM(eventInfo.wParam) == XBUTTON1) ? MouseButtonID::Xbtn1 : MouseButtonID::Xbtn2;
				EvtSysInputMouseButtonPressed event { xbuttonID, cursorPos };
				eventDispatcher->DispatchEvent(event);
			}
			break;

		case WM_XBUTTONUP:
			{
				MouseButtonID xbuttonID = (GET_XBUTTON_WPARAM(eventInfo.wParam) == XBUTTON1) ? MouseButtonID::Xbtn1 : MouseButtonID::Xbtn2;
				EvtSysInputMouseButtonReleased event { xbuttonID, cursorPos };
				eventDispatcher->DispatchEvent(event);
			}
			break;

		case WM_XBUTTONDBLCLK:
			{
				MouseButtonID xbuttonID = (GET_XBUTTON_WPARAM(eventInfo.wParam) == XBUTTON1) ? MouseButtonID::Xbtn1 : MouseButtonID::Xbtn2;
				EvtSysInputMouseButtonDblclick event { xbuttonID, cursorPos };
				eventDispatcher->DispatchEvent(event);
			}
			break;

		default:
			return false;
		}

		return true;
	}


	bool Win32TranslateAndDispatchInputEventTouch(SystemEventDispatcher* eventDispatcher, const Win32SystemEventInfo& eventInfo)
	{
		return false;
	}


	bool Win32TranslateAndDispatchWindowEventNotification(SystemEventDispatcher* eventDispatcher, const Win32SystemEventInfo& eventInfo)
	{
		switch(eventInfo.messageCode)
		{
		case WM_CLOSE:
			{
			}
			break;

		case WM_DESTROY:
			{
				EvtSysWindowCommandClose event { };
				eventDispatcher->DispatchEvent(event);
			}
			break;

		case WM_MOVE:
			{
				Position newPos(LOWORD(eventInfo.lParam), HIWORD(eventInfo.lParam));
				EvtSysWindowNotificationMove event { newPos };
				eventDispatcher->DispatchEvent(event);
			}
			break;

		case WM_SIZE:
			{
				Size newSize(LOWORD(eventInfo.lParam), HIWORD(eventInfo.lParam));
				EvtSysWindowNotificationResize event { newSize };
				eventDispatcher->DispatchEvent(event);
			}
			break;

		case WM_SHOWWINDOW:
			{
				bool isVisible = ((eventInfo.wParam == FALSE) ? false : true);
				EvtSysWindowNotificationVisibilityChange event { isVisible };
				eventDispatcher->DispatchEvent(event);
			}
			break;

		default:
			return false;
		}

		return true;
	}


	static const SystemKeyCode asciiKeyCodeMap_08_7B[] =
	{
		/* 0x0008 */ KeyboardKey_Backspace,
		/* 0x00xx */ KeyboardKey_Tab,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Enter,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Shift_Left,
		/* 0x00xx */ KeyboardKey_Control_Left,
		/* 0x00xx */ KeyboardKey_Alt_Left,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x0014 */ KeyboardKey_Caps_Lock,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x001B */ KeyboardKey_Escape,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x0020 */ KeyboardKey_Space,
		/* 0x0021 */ KeyboardKey_Page_Up,
		/* 0x0022 */ KeyboardKey_Page_Down,
		/* 0x0023 */ KeyboardKey_End,
		/* 0x0024 */ KeyboardKey_Home,
		/* 0x0025 */ KeyboardKey_Arrow_Left,
		/* 0x0026 */ KeyboardKey_Arrow_Up,
		/* 0x0027 */ KeyboardKey_Arrow_Right,
		/* 0x0028 */ KeyboardKey_Arrow_Down,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x002D */ KeyboardKey_Insert,
		/* 0x002E */ KeyboardKey_Delete,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x0030 */ KeyboardKey_Num_0,
		/* 0x0031 */ KeyboardKey_Num_1,
		/* 0x0032 */ KeyboardKey_Num_2,
		/* 0x0033 */ KeyboardKey_Num_3,
		/* 0x0034 */ KeyboardKey_Num_4,
		/* 0x0035 */ KeyboardKey_Num_5,
		/* 0x0036 */ KeyboardKey_Num_6,
		/* 0x0037 */ KeyboardKey_Num_7,
		/* 0x0038 */ KeyboardKey_Num_8,
		/* 0x0039 */ KeyboardKey_Num_9,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x0041 */ KeyboardKey_Char_A,
		/* 0x0042 */ KeyboardKey_Char_B,
		/* 0x0043 */ KeyboardKey_Char_C,
		/* 0x0044 */ KeyboardKey_Char_D,
		/* 0x0045 */ KeyboardKey_Char_E,
		/* 0x0046 */ KeyboardKey_Char_F,
		/* 0x0047 */ KeyboardKey_Char_G,
		/* 0x0048 */ KeyboardKey_Char_H,
		/* 0x0049 */ KeyboardKey_Char_I,
		/* 0x004A */ KeyboardKey_Char_J,
		/* 0x004B */ KeyboardKey_Char_K,
		/* 0x004C */ KeyboardKey_Char_L,
		/* 0x004D */ KeyboardKey_Char_M,
		/* 0x004E */ KeyboardKey_Char_N,
		/* 0x004F */ KeyboardKey_Char_O,
		/* 0x0050 */ KeyboardKey_Char_P,
		/* 0x0051 */ KeyboardKey_Char_Q,
		/* 0x0052 */ KeyboardKey_Char_R,
		/* 0x0053 */ KeyboardKey_Char_S,
		/* 0x0054 */ KeyboardKey_Char_T,
		/* 0x0055 */ KeyboardKey_Char_U,
		/* 0x0056 */ KeyboardKey_Char_V,
		/* 0x0057 */ KeyboardKey_Char_W,
		/* 0x0058 */ KeyboardKey_Char_X,
		/* 0x0059 */ KeyboardKey_Char_Y,
		/* 0x005A */ KeyboardKey_Char_Z,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x00xx */ KeyboardKey_Unknown,
		/* 0x0060 */ KeyboardKey_Numpad_0,
		/* 0x0061 */ KeyboardKey_Numpad_1,
		/* 0x0062 */ KeyboardKey_Numpad_2,
		/* 0x0063 */ KeyboardKey_Numpad_3,
		/* 0x0064 */ KeyboardKey_Numpad_4,
		/* 0x0065 */ KeyboardKey_Numpad_5,
		/* 0x0066 */ KeyboardKey_Numpad_6,
		/* 0x0067 */ KeyboardKey_Numpad_7,
		/* 0x0068 */ KeyboardKey_Numpad_8,
		/* 0x0069 */ KeyboardKey_Numpad_9,
		/* 0x006A */ KeyboardKey_Numpad_Multiply,
		/* 0x006B */ KeyboardKey_Numpad_Add,
		/* 0x006C */ KeyboardKey_Unknown,
		/* 0x006D */ KeyboardKey_Numpad_Substract,
		/* 0x006E */ KeyboardKey_Numpad_Decimal,
		/* 0x006F */ KeyboardKey_Numpad_Divide,
		/* 0x0070 */ KeyboardKey_F1,
		/* 0x0071 */ KeyboardKey_F2,
		/* 0x0072 */ KeyboardKey_F3,
		/* 0x0073 */ KeyboardKey_F4,
		/* 0x0074 */ KeyboardKey_F5,
		/* 0x0075 */ KeyboardKey_F6,
		/* 0x0076 */ KeyboardKey_F7,
		/* 0x0077 */ KeyboardKey_F8,
		/* 0x0078 */ KeyboardKey_F9,
		/* 0x0079 */ KeyboardKey_F10,
		/* 0x007A */ KeyboardKey_F11,
		/* 0x007B */ KeyboardKey_F12
	};


	static const SystemKeyCode asciiKeyCodeMap_A0_A5[] =
	{
		/* 0x00A0 */ KeyboardKey_Shift_Left,
		/* 0x00A1 */ KeyboardKey_Shift_Right,
		/* 0x00A2 */ KeyboardKey_Control_Left,
		/* 0x00A3 */ KeyboardKey_Control_Right,
		/* 0x00A4 */ KeyboardKey_Alt_Left,
		/* 0x00A5 */ KeyboardKey_Alt_Right,
	};


	SystemKeyCode GetSystemKeyCode(WPARAM win32KeyID)
	{
		if ((win32KeyID >= 0x0008) && (win32KeyID <= 0x007B))
		{
			return GetConstantArrayElement(asciiKeyCodeMap_08_7B, win32KeyID - 0x0008);
		}

		if ((win32KeyID >= 0x00A0) && (win32KeyID <= 0x00A5))
		{
			return GetConstantArrayElement(asciiKeyCodeMap_A0_A5, win32KeyID - 0x00A0);
		}
		
		return KeyboardKey_Unknown;
	}


}
