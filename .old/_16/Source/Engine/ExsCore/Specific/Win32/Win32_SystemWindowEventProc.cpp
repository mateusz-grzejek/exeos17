
#include "_Precompiled.h"
#include <ExsCore/System/SystemEventDispatcher.h>
#include <ExsCore/System/SystemWindow.h>


namespace Exs
{


	LRESULT __stdcall Win32SystemEventProc(HWND wndHandle, UINT msgCode, WPARAM wParam, LPARAM lParam)
	{
		LONG_PTR windowUserData = ::GetWindowLongPtrA(wndHandle, GWLP_USERDATA);
		ExsRuntimeAssert( windowUserData != 0 );

		Win32SystemEventInfo systemEventInfo { };
		systemEventInfo.windowHandle = wndHandle;
		systemEventInfo.messageCode = msgCode;
		systemEventInfo.wParam = wParam;
		systemEventInfo.lParam = lParam;

		SystemEventDispatcher* eventDispatcher = reinterpret_cast<SystemEventDispatcher*>(windowUserData);
		eventDispatcher->TranslateAndDispatch(systemEventInfo);

		return ::DefWindowProcA(wndHandle, msgCode, wParam, lParam);
	}


	LRESULT __stdcall Win32RedirectingEventProc(HWND wndHandle, UINT msgCode, WPARAM wParam, LPARAM lParam)
	{
		switch(msgCode)
		{
		case WM_NCCREATE:
			{
				CREATESTRUCT* createStruct = reinterpret_cast<CREATESTRUCT*>(lParam);
				ExsRuntimeAssert( createStruct->lpCreateParams != nullptr );

				SystemEventDispatcher* eventDispatcher = reinterpret_cast<SystemEventDispatcher*>(createStruct->lpCreateParams);
				LONG_PTR eventDispatcherAddress = reinterpret_cast<LONG_PTR>(eventDispatcher);
				LONG_PTR wndEventProcAddress = reinterpret_cast<LONG_PTR>(Win32SystemEventProc);

				::SetWindowLongPtrA(wndHandle, GWLP_USERDATA, eventDispatcherAddress);
				::SetWindowLongPtrA(wndHandle, GWLP_WNDPROC, wndEventProcAddress);
				::SetWindowPos(wndHandle, nullptr, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED);
			}
			break;

		default:
			break;
		}

		return ::DefWindowProcA(wndHandle, msgCode, wParam, lParam);
	}


}
