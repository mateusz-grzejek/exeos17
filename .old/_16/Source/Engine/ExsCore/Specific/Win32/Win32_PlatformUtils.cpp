
#include "_Precompiled.h"
#include <ExsCore/PlatformUtils.h>


namespace Exs
{


	void Win32HandleResultError(HRESULT result);
	void Win32HandleSystemError(DWORD errorCode);


	bool PlatformUtils::Win32CheckLastError()
	{
		DWORD errorCode = ::GetLastError();

		if (errorCode != 0)
		{
			Win32HandleSystemError(errorCode);
			return false;
		}

		return true;
	}


	bool PlatformUtils::Win32CheckResult(HRESULT result)
	{
		if ((result != S_OK) && (result != S_FALSE))
		{
			Win32HandleResultError(result);
			return false;
		}

		return true;
	}




	void Win32HandleResultError(HRESULT result)
	{
	}


	void Win32HandleSystemError(DWORD errorCode)
	{
	}


}
