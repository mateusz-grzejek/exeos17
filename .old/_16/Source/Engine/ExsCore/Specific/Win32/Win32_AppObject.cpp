
#include "_Precompiled.h"
#include <ExsCore/System/AppObject.h>


namespace Exs
{


	bool AppObjectInitializer::InitializeSystemData(SystemEnvData* systemEnvData)
	{
		ExsTraceInfo(TRC_Core_System, "InitializeSystemData(%p).", systemEnvData);
		systemEnvData->appInstance = ::GetModuleHandleW(nullptr);
		return true;
	}


	void AppObjectInitializer::ReleaseSystemData(SystemEnvData* systemEnvData)
	{
		ExsTraceInfo(TRC_Core_System, "AppObjectInitializer: ReleaseSystemData(%p).", systemEnvData);
		systemEnvData->appInstance = nullptr;
	}


}
