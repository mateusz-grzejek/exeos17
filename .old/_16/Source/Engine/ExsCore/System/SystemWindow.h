
#pragma once

#ifndef __Exs_Core_SystemWindow_H__
#define __Exs_Core_SystemWindow_H__

#include "AppCommon.h"
#include "SystemWindowCommon.h"
#include "SystemEventBase.h"


namespace Exs
{


	ExsDeclareRefPtrClass(SystemWindow);
	ExsDeclareSharedDataRefHandle(AppGlobalState);


	///<summary>
	///</summary>
	class EXS_LIBCLASS_CORE SystemWindow
	{
		EXS_DECLARE_NONCOPYABLE(SystemWindow);

	public:
		typedef NativeWindowHandle Handle;

	protected:
		Handle                     _handle;
		AppGlobalStateRefHandle    _appGlobalState;
		SystemEnvData*             _systemEnvData;
		SystemWindowData           _windowData;

	public:
		SystemWindow(NativeWindowHandle handle, AppGlobalStateRefHandle appGlobalState, const SystemWindowData& windowData);
		virtual ~SystemWindow();
		
		///<summary>
		///</summary>
		bool Destroy();
		
		///<summary>
		///</summary>
		bool Show(bool show);

		///<summary>
		///</summary>
		bool Resize(const Size& size);
		
		///<summary>
		///</summary>
		bool SetStyle(SystemWindowStyle style);
		
		///<summary>
		///</summary>
		bool SetTitle(const std::string& title);

		///<summary>
		///</summary>
		const AppGlobalStateRefHandle& GetAppGlobalState() const;

		///<summary>
		///</summary>
		Handle GetHandle() const;
		
		///<summary>
		///</summary>
		bool GetClientAreaSize(Size* size) const;
		
		///<summary>
		///</summary>
		bool GetSize(Size* size) const;
		
		///<summary>
		///</summary>
		bool IsValid() const;
	};


	inline const AppGlobalStateRefHandle& SystemWindow::GetAppGlobalState() const
	{
		return this->_appGlobalState;
	}

	inline SystemWindow::Handle SystemWindow::GetHandle() const
	{
		return this->_handle;
	}

	inline bool SystemWindow::IsValid() const
	{
		return this->_handle != ExsNativeWindowHandleNULL;
	}


}


#endif /* __Exs_Core_SystemWindow_H__ */
