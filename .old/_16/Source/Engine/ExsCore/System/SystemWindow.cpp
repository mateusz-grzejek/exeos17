
#include "_Precompiled.h"
#include <ExsCore/System/SystemWindow.h>
#include <ExsCore/System/SystemEventReceiver.h>


namespace Exs
{


	SystemWindow::SystemWindow(NativeWindowHandle handle, AppGlobalStateRefHandle appGlobalState, const SystemWindowData& windowData)
	: _handle(handle)
	, _appGlobalState(appGlobalState)
	, _systemEnvData(&(appGlobalState->systemEnvData))
	, _windowData(windowData)
	{ }


	SystemWindow::~SystemWindow()
	{
		ExsDebugAssert( this->_handle == ExsNativeWindowHandleNULL );
	}


}
