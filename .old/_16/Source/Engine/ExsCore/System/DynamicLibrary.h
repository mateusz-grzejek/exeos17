
#pragma once

#ifndef __Exs_Core_DynamicLibrary_H__
#define __Exs_Core_DynamicLibrary_H__

#include "../Prerequisites.h"
#include <stdx/intrusive_ptr.h>


namespace Exs
{

	
	///<summary>
	/// Represents possible format of path strings, that can be passed to DynamicLibrary::Load().
	///</summary>
	enum class DLLPathType : Enum
	{
		// Base path, trailed with base filename without prefix or filename extension.
		Base,

		// Full path, used directly as specified.
		Full
	};

	
	///<summary>
	/// System-level class, representing dynamic library in the current environment. It simplifies the process
	/// of loading and allow easy querying of addresses of symbols. It also hides OS-specific differences in
	/// naming conventions (like prefixes and extensions). Also, this object works as a ref-counted handle:
	/// each copy increments number of active refs, preventing the DLL library from being unloaded as long
	/// as there are any existing references to object, that wraps that library.
	///</summary>
	class EXS_LIBCLASS_CORE DynamicLibrary
	{
	public:
		typedef NativeDLLHandle Handle;

	private:
		// Internal loader class, used to implement intrusive reference counting.
		struct LoadGuard : public stdx::ref_counted_base<stdx::atomic_ref_counter>
		{
		public:
			Handle handle;

		public:
			LoadGuard(Handle handle);
			~LoadGuard();
		};

		typedef stdx::intrusive_ptr<LoadGuard> LoadGuardRefPtr;

	private:
		LoadGuardRefPtr  _loadGuard; // Load guard, stored as ref-counted pointer.
		std::string      _fileName; // File name (only!) of the library.
		std::string      _path; // Full path of the library file (as specified!).

	public:
		DynamicLibrary();
		~DynamicLibrary();

		explicit operator bool() const;

		///<summary>
		/// Retrieves address of symbol from the library using specified name. Address is returned
		/// as unspecified (void) pointer, without any modifications. If symName is an empty string
		/// or no library is loaded within this object, nullptr is returned. If symName is nullptr,
		/// the behavior is undefined.
		///</summary>
		///<param name="symName"> Name of the symbol to be retrieved. </param>
		void* RetrieveSymbol(const char* symName) const;
		
		///<summary>
		/// Retrieves address of symbol from the library using specified name. Address is returned
		/// as pointer of specified (Return_t) type, using static_cast conversion. If symName is
		/// an empty string or no library is loaded within this object, nullptr is returned.
		/// If symName is nullptr, the behavior is undefined.
		///</summary>
		///<param name="symName"> Name of the symbol to be retrieved. </param>
		template <class Return_t>
		Return_t RetrieveSymbolAs(const char* symName) const;
		
		///<summary>
		/// Releases this object by decrementing ref counter of stored loader object. If this counter
		/// reaches 0 after this operation, DLL is physically unloaded from the memory.
		///</summary>
		void Release();
		
		///<summary>
		/// Returns native, system-specific handle to the DLL module.
		///</summary>
		Handle GetHandle() const;

		///<summary>
		/// Returns use counter of the internal loader which equals to number of instances of this class,
		/// that wraps loader managing the same DLL library.
		///</summary>
		Uint GetUseCount() const;
		
		///<summary>
		/// Returns file name (only) of the library managed by this object. If no library is loaded
		/// (or it was already unloaded using Release() method), empty string is returned.
		///</summary>
		const std::string& GetFileName() const;
		
		///<summary>
		/// Returns full path (as specified) of the library managed by this object. If no library is loaded
		/// (or it was already unloaded using Release() method), empty string is returned.
		///</summary>
		const std::string& GetPath() const;
		
		///<summary>
		/// Returns true if this object has a library loaded or false otherwise.
		///</summary>
		bool IsLoaded() const;
		
		///<summary>
		/// Swaps this object with 'other'.
		///</summary>
		void Swap(DynamicLibrary& other);
		
		///<summary>
		/// Loads DLL library located at the specified path which is of type 'pathType'. Paths can be
		/// specified in two ways: as base pths and full paths. 'Base' means, that the last part of the
		/// path [Dir1/Dir2/.../DirN/]Name is name of the library without prefix or extension. These paths
		/// are translated to os-specific ones. For example, Load("Bin/Plugins/Plugin1") will load
		/// Bin/Plugins/libPlugin1.so on Linux systems, Bin/Plugins/Plugin1.dll on Windows family, etc.
		/// This allows writing platform-independent code that relies on DLL loading. Full paths are paths,
		/// that are assumed valid and no translation is performed.
		///</summary>
		static DynamicLibrary Load(const std::string& path, DLLPathType pathType = DLLPathType::Base);

	private:
		DynamicLibrary(LoadGuard* loadGuard, std::string&& fileName, std::string&& path);

		// Converts base path to a full path with optional prefix and extension.
		static std::string ConvertBasePath(const std::string& name);
	};


	inline DynamicLibrary::operator bool() const
	{
		return this->IsLoaded();
	}

	template <class Return_t>
	inline Return_t DynamicLibrary::RetrieveSymbolAs(const char* symName) const
	{
		void* symbolPtr = this->RetrieveSymbol(symName);
		return pointer_cast<Return_t>(symbolPtr);
	}

	inline DynamicLibrary::Handle DynamicLibrary::GetHandle() const
	{
		return this->_loadGuard ? this->_loadGuard->handle : ExsNativeDLLHandleNULL;
	}

	inline Uint DynamicLibrary::GetUseCount() const
	{
		return this->_loadGuard ? this->_loadGuard->get_refs_num() : 0;
	}

	inline const std::string& DynamicLibrary::GetFileName() const
	{
		return this->_fileName;
	}

	inline const std::string& DynamicLibrary::GetPath() const
	{
		return this->_path;
	}

	inline bool DynamicLibrary::IsLoaded() const
	{
		return this->_loadGuard ? true : false;
	}

	inline void DynamicLibrary::Swap(DynamicLibrary& other)
	{
		std::swap(this->_loadGuard, other._loadGuard);
		std::swap(this->_fileName, other._fileName);
		std::swap(this->_path, other._path);
	}


}


#endif /* __Exs_Core_DynamicLibrary_H__ */
