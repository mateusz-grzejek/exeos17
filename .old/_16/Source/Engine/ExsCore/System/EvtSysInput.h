
#pragma once

#ifndef __Exs_Core_EvtSysInput_H__
#define __Exs_Core_EvtSysInput_H__

#include "SystemEvent.h"
#include "KeyboardKeyCodes.h"


#define ExsEnumDeclareMouseButtonID(marker, iid) \
	static_cast<Enum>(((Enum)marker << 8) | (Enum)iid)

#define ExsEnumGetMouseButtonIDFlag(mouseButtonID) \
	static_cast<Uint8>((static_cast<Uint16>(mouseButtonID) & EXS_EVT_SYS_MOUSE_BUTTON_ID_FLAG_MASK) >> 8)

#define ExsEnumGetMouseButtonIndex(mouseButtonID) \
	static_cast<Uint8>((static_cast<Uint16>(mouseButtonID) & EXS_EVT_SYS_MOUSE_BUTTON_ID_INDEX_MASK)


namespace Exs
{


	class KeyboardState;

	
	///<summary>
	///</summary>
	enum MouseButtonIDFlags : Uint8
	{
		//
		MouseButtonID_Left = 0x01,

		//
		MouseButtonID_Right = 0x08,

		//
		MouseButtonID_Middle = 0x04,

		//
		MouseButtonID_Xbtn1 = 0x10,

		//
		MouseButtonID_Xbtn2 = 0x20
	};

	
	///<summary>
	///</summary>
	enum class MouseButtonID : Uint16
	{
		//
		Left = ExsEnumDeclareMouseButtonID(MouseButtonID_Left, 0),

		//
		Right = ExsEnumDeclareMouseButtonID(MouseButtonID_Right, 1),

		//
		Middle = ExsEnumDeclareMouseButtonID(MouseButtonID_Middle, 2),

		//
		Xbtn1 = ExsEnumDeclareMouseButtonID(MouseButtonID_Xbtn1, 3),

		//
		Xbtn2 = ExsEnumDeclareMouseButtonID(MouseButtonID_Xbtn2, 4)
	};


	enum : Size_t
	{
		//
		Evt_Mouse_Buttons_Num = 5,

		//
		Evt_Touch_Max_Points_Num = 8
	};

	
	///<summary>
	///</summary>
	class EvtSysInputKeyboard : public EvtSysInput
	{
	protected:
		SystemKeyCode           _keyCode;
		const KeyboardState*    _kbState;
		
	protected:
		EvtSysInputKeyboard(SystemEventCode code, SystemKeyCode keyCode, const KeyboardState* kbState)
		: EvtSysInput(code)
		, _keyCode(keyCode)
		, _kbState(kbState)
		{
			ExsDebugAssert( ExsEnumGetSystemEventSubtype(code) == SystemEventSubtype::Input_Keyboard );
		}

	public:
		const KeyboardState* GetKeyboardState() const
		{
			return this->_kbState;
		}

		SystemKeyCode GetKeyCode() const
		{
			return this->_keyCode;
		}
	};

	
	///<summary>
	///</summary>
	class EvtSysInputKeyboardKeyPressed : public EvtSysInputKeyboard
	{
	protected:
		Uint32    _repeatCount;

	public:
		EvtSysInputKeyboardKeyPressed(SystemKeyCode keyCode, const KeyboardState* kbState, Uint32 repeatCount = 1)
		: EvtSysInputKeyboard(SystemEventCode::Input_Keyboard_Key_Pressed, keyCode, kbState)
		, _repeatCount(repeatCount)
		{ }

		Uint32 GetRepeatCount() const
		{
			return this->_repeatCount;
		}
	};

	
	///<summary>
	///</summary>
	class EvtSysInputKeyboardKeyReleased : public EvtSysInputKeyboard
	{
	public:
		EvtSysInputKeyboardKeyReleased(SystemKeyCode keyCode, const KeyboardState* kbState)
		: EvtSysInputKeyboard(SystemEventCode::Input_Keyboard_Key_Released, keyCode, kbState)
		{ }
	};



	
	///<summary>
	///</summary>
	class EvtSysInputMouse : public EvtSysInput
	{
	protected:
		Position    _cursorPosition;

	protected:
		EvtSysInputMouse(SystemEventCode code, const Position& cursorPosition)
		: EvtSysInput(code)
		, _cursorPosition(cursorPosition)
		{
			ExsDebugAssert( ExsEnumGetSystemEventSubtype(code) == SystemEventSubtype::Input_Mouse );
		}

	public:
		const Position& GetCursorPosition() const
		{
			return this->_cursorPosition;
		}
	};

	
	///<summary>
	///</summary>
	class EvtSysInputMouseButton : public EvtSysInputMouse
	{
	protected:
		MouseButtonID    _buttonID;

	public:
		EvtSysInputMouseButton(SystemEventCode code, MouseButtonID buttonID, const Position& cursorPosition)
		: EvtSysInputMouse(code, cursorPosition)
		, _buttonID(buttonID)
		{ }

		MouseButtonID GetButtonID() const
		{
			return this->_buttonID;
		}

		Uint8 GetButtonFlag() const
		{
			return ExsEnumGetMouseButtonIDFlag(this->_buttonID);
		}
	};

	
	///<summary>
	///</summary>
	class EvtSysInputMouseButtonDblclick : public EvtSysInputMouseButton
	{
	public:
		EvtSysInputMouseButtonDblclick(MouseButtonID buttonID, const Position& cursorPosition)
		: EvtSysInputMouseButton(SystemEventCode::Input_Mouse_Button_Dblclick, buttonID, cursorPosition)
		{ }
	};

	
	///<summary>
	///</summary>
	class EvtSysInputMouseButtonPressed : public EvtSysInputMouseButton
	{
	public:
		EvtSysInputMouseButtonPressed(MouseButtonID buttonID, const Position& cursorPosition)
		: EvtSysInputMouseButton(SystemEventCode::Input_Mouse_Button_Pressed, buttonID, cursorPosition)
		{ }
	};

	
	///<summary>
	///</summary>
	class EvtSysInputMouseButtonReleased : public EvtSysInputMouseButton
	{
	public:
		EvtSysInputMouseButtonReleased(MouseButtonID buttonID, const Position& cursorPosition)
		: EvtSysInputMouseButton(SystemEventCode::Input_Mouse_Button_Released, buttonID, cursorPosition)
		{ }
	};

	
	///<summary>
	///</summary>
	class EvtSysInputMouseMove : public EvtSysInputMouse
	{
	public:
		EvtSysInputMouseMove(const Position& cursorPosition)
		: EvtSysInputMouse(SystemEventCode::Input_Mouse_Move, cursorPosition)
		{ }
	};

	
	///<summary>
	///</summary>
	class EvtSysInputMouseWheelScroll : public EvtSysInputMouse
	{
	protected:
		Int16    _scrollDelta;
		
	public:
		EvtSysInputMouseWheelScroll(const Position& cursorPosition, Int16 scrollDelta)
		: EvtSysInputMouse(SystemEventCode::Input_Mouse_Wheel_Scroll, cursorPosition)
		, _scrollDelta(scrollDelta)
		{ }

		Int16 GetScrollDelta() const
		{
			return this->_scrollDelta;
		}
	};



	
	///<summary>
	///</summary>
	class EvtSysInputTouch : public EvtSysInput
	{
	protected:
		EvtSysInputTouch(SystemEventCode code)
		: EvtSysInput(code)
		{
			ExsDebugAssert( ExsEnumGetSystemEventSubtype(code) == SystemEventSubtype::Input_Touch );
		}
	};


}


#endif /* __Exs_Core_EvtSysInput_H__ */
