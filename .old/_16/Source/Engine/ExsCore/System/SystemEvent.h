
#pragma once

#ifndef __Exs_Core_SystemEvent_H__
#define __Exs_Core_SystemEvent_H__

#include "SystemEventBase.h"
#include "Event.h"


namespace Exs
{


	class SystemEvent : public Event<SystemEventCode>
	{
	public:
		SystemEvent(SystemEventCode code)
		: Event(code)
		{ }

		SystemEventType GetType() const
		{
			return ExsEnumGetSystemEventType(this->_code);
		}

		SystemEventSubtype GetSubtype() const
		{
			return ExsEnumGetSystemEventSubtype(this->_code);
		}
	};


	class EvtSysApplication : public SystemEvent
	{
	public:
		EvtSysApplication(SystemEventCode code)
		: SystemEvent(code)
		{
			ExsDebugAssert( ExsEnumGetSystemEventType(code) == SystemEventType::App );
		}
	};


	class EvtSysInput : public SystemEvent
	{
	public:
		EvtSysInput(SystemEventCode code)
		: SystemEvent(code)
		{
			ExsDebugAssert( ExsEnumGetSystemEventType(code) == SystemEventType::Input );
		}
	};


	class EvtSysWindow : public SystemEvent
	{
	public:
		EvtSysWindow(SystemEventCode code)
		: SystemEvent(code)
		{
			ExsDebugAssert( ExsEnumGetSystemEventType(code) == SystemEventType::Window );
		}
	};


}


#endif /* __Exs_Core_SystemEvent_H__ */
