
#pragma once

#ifndef __Exs_Core_SystemEventDispatcher_H__
#define __Exs_Core_SystemEventDispatcher_H__

#include "CommonSystemTypes.h"
#include "SystemEventBase.h"


namespace Exs
{


	class SystemEventReceiver;


	///<summary>
	///</summary>
	class SystemEventDispatcher
	{
	protected:
		SystemEventReceiver*  _eventReceiver;

	public:
		SystemEventDispatcher();
		~SystemEventDispatcher();
		
		///<summary>
		///</summary>
		bool TranslateAndDispatch(const SystemEventInfo& eventInfo);
		
		///<summary>
		///</summary>
		template <class Event_t>
		bool DispatchEvent(const Event_t& event);

		///<summary>
		///</summary>
		bool SetReceiver(SystemEventReceiver* eventReceiver, SystemEventReceiver** prevReceiver = nullptr);

		///<summary>
		///</summary>
		bool IsReceiverSet() const;

	private:
		// Posts event directly to the receiver (if it has been set and is active). Returns true on success
		// (receiver is active and event was handled successfuly) or false otherwise.
		bool PostEvent(const SystemEvent& event);
	};


	template <class Event_t>
	inline bool SystemEventDispatcher::DispatchEvent(const Event_t& event)
	{
		return this->PostEvent(event);
	}

	inline bool SystemEventDispatcher::IsReceiverSet() const
	{
		return this->_eventReceiver != nullptr;
	}


}


#endif /* __Exs_Core_SystemEventDispatcher_H__ */
