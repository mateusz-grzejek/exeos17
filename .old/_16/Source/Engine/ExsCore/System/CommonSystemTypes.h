
#pragma once

#ifndef __Exs_Core_CommonSystemTypes_H__
#define __Exs_Core_CommonSystemTypes_H__

#include "../Prerequisites.h"


namespace Exs
{

	
#if ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_WIN32 )
	struct Win32SystemEnvData;
	typedef Win32SystemEnvData SystemEnvData;
	struct Win32SystemEventInfo;
	typedef Win32SystemEventInfo SystemEventInfo;
	struct Win32SystemWindowData;
	typedef Win32SystemWindowData SystemWindowData;
#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_WP81 )
#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_LINUX )
	struct X11SystemEnvData;
	typedef X11SystemEnvData SystemEnvData;
	struct X11SystemEventInfo;
	typedef X11SystemEventInfo SystemEventInfo;
	struct X11SystemWindowData;
	typedef X11SystemWindowData SystemWindowData;
#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_ANDROID )
#endif


};


#if ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_WIN32 )
#  include "../Specific/Win32/Win32_CommonSystemTypes.h"
#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_WP81 )
#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_LINUX )
#  include "../Specific/Linux/Linux_CommonSystemTypes.h"
#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_ANDROID )
#endif


#endif /* __Exs_Core_CommonSystemTypes_H__ */
