
#include "_Precompiled.h"
#include <ExsCore/System/AppObject.h>
#include <ExsCore/System/AppEventLoop.h>
#include <ExsCore/System/SystemEventReceiver.h>


namespace Exs
{


	AppObject::AppObject(CoreEngineStateRefHandle coreEngineState, AppGlobalStateRefHandle appGlobalState)
	: _coreEngineState(coreEngineState)
	, _appGlobalState(appGlobalState)
	, _activeEventReceiver(nullptr)
	{ }


	AppObject::~AppObject()
	{
		// Event loop holds active handle to SharedAppState. Destroy the loop, so handle can be released.
		this->_eventLoop->ReleaseSharedStateRefs();
		this->_eventLoop = nullptr;

		if (this->_appGlobalState.IsShared())
		{
			Uint shareCount = this->_appGlobalState.GetShareCount();
			ExsTraceWarning(TRC_Core_System, "SharedAppState is still shared (share count = %" EXS_PFU ").", shareCount);
		}

		AppObjectInitializer::ReleaseSystemData(&(this->_appGlobalState->systemEnvData));
	}


	void AppObject::SetExitRequest()
	{
		this->_appGlobalState->executionState.Set(AppExecutionState_Terminate_Request);
	}


	void AppObject::SetExecutionArgs(const char** argv, Uint32 argc)
	{
		this->_executionArgs.ParseArgsArray(argv, argc);
	}


	bool AppObject::SetActiveEventReceiver(SystemEventReceiver* eventReceiver)
	{
		// Set receiver via event dispatcher which is also responsible for activation of this receiver.
		if (this->_eventLoop->SetEventReceiver(eventReceiver))
		{
			// If receiver has been activated, its state is active.
			if (eventReceiver->IsActive())
			{
				// Ssve receiver and return true (success).
				this->_activeEventReceiver = eventReceiver;
				return true;
			}
		}

		return false;
	}


	bool AppObject::RunEventLoop(stdx::mask<AppEventLoopRunFlags> flags, const AppUpdateCallback& updateCallback)
	{
		if (this->_activeEventReceiver == nullptr)
			return false;

		if (!this->_eventLoop->Run(flags, updateCallback))
			return false;

		return true;
	}


	void AppObject::BindEventLoop(const AppEventLoopRefPtr& eventLoop)
	{
		ExsDebugAssert( !this->_eventLoop );
		this->_eventLoop = eventLoop;
	}


	void AppObject::SetAppArgDefinitions(const AppConfiguration& appConfiguration)
	{
		for (auto& argDefinition : appConfiguration.executionArgDefs)
		{
			this->_executionArgs.AddArgDefinition(argDefinition);
		}
	}


}
