
#include "_Precompiled.h"
#include <ExsCore/System/AppEventLoop.h>
#include <ExsCore/System/AppObject.h>
#include <ExsCore/System/SystemWindow.h>


namespace Exs
{


	AppEventLoop::AppEventLoop(AppObject* appObject, CoreEngineStateRefHandle coreEngineState, AppGlobalStateRefHandle appGlobalState)
	: _appObject(appObject)
	, _coreEngineState(coreEngineState)
	, _appGlobalState(appGlobalState)
	{ }


	AppEventLoop::~AppEventLoop()
	{ }


	bool AppEventLoop::SetEventReceiver(SystemEventReceiver* eventReceiver)
	{
		return this->_eventDispatcher.SetReceiver(eventReceiver);
	}


	bool AppEventLoop::OnBegin()
	{
		return true;
	}


	void AppEventLoop::OnEnd()
	{ }


	bool AppEventLoop::OnUpdate()
	{
		return true;
	}


	bool AppEventLoop::Run(stdx::mask<AppEventLoopRunFlags> flags, const AppUpdateCallback& updateCallback)
	{
		ExsDebugAssert( this->_appObject != nullptr );

		if (!this->OnBegin())
			return false;

		std::function<bool()> internalCallback;
		auto internalOnUpdateExecCall = std::bind<bool>(InternalOnUpdateExec, this);

		if (updateCallback)
		{
			internalCallback = std::bind<bool>([internalOnUpdateExecCall, updateCallback]() -> bool {
				return internalOnUpdateExecCall() && updateCallback();
			});
		}
		else
		{
			internalCallback = std::bind<bool>([internalOnUpdateExecCall]() -> bool {
				return internalOnUpdateExecCall();
			});
		}

		this->EnterSystemLoop(internalCallback);
		this->OnEnd();

		return true;
	}


	void AppEventLoop::ReleaseSharedStateRefs()
	{
		ExsDebugAssert( this->_appGlobalState );
		this->_appGlobalState = nullptr;

		ExsDebugAssert( this->_coreEngineState );
		this->_coreEngineState = nullptr;
	}


	bool AppEventLoop::InternalOnUpdateExec(AppEventLoop* eventLoop)
	{
		return eventLoop->OnUpdate();
	}


}
