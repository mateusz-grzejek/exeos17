
#pragma once

#ifndef __Exs_Core_SystemEventBase_H__
#define __Exs_Core_SystemEventBase_H__

#include "EventBase.h"


#define EXS_EVT_SYS_CODE_NCID_RESERVED    0xA3000000
#define EXS_EVT_SYS_CODE_TYPE_MASK        0x00FF0000
#define EXS_EVT_SYS_CODE_SUBTYPE_MASK     0x00FFFF00
#define EXS_EVT_SYS_CODE_INDEX_MASK       0x000000FF


#define EXS_EVT_SYS_MOUSE_BUTTON_ID_FLAG_MASK   0xFF00
#define EXS_EVT_SYS_MOUSE_BUTTON_ID_INDEX_MASK  0x00FF


#define ExsEnumDeclareSystemEventCode(eventType, index) \
	(EXS_RESULT_CODE_NCID_RESERVED | ((Enum)eventType << 8) | (Enum)index)

#define ExsEnumGetSystemEventType(eventCode) \
	static_cast<SystemEventType>((static_cast<Enum>(eventCode) & EXS_EVT_SYS_CODE_TYPE_MASK) >> 8)

#define ExsEnumGetSystemEventSubtype(eventCode) \
	static_cast<SystemEventSubtype>((static_cast<Enum>(eventCode) & EXS_EVT_SYS_CODE_SUBTYPE_MASK) >> 8)

#define ExsEnumGetSystemEventIndex(eventCode) \
	static_cast<Uint8>(static_cast<Enum>(eventCode) & EXS_EVT_SYS_CODE_INDEX_MASK)

#define ExsEnumValidateSystemEventCode(eventCode) \
	EXS_NCID_GET_RESERVED(eventCode) == EXS_EVT_SYS_CODE_NCID_RESERVED


namespace Exs
{


	class SystemEvent;

	// Represents code of a system event.
	typedef Uint32 SystemKeyCode;

	// Type for handlers of system events, which is a functor object accepting reference to the event
	// as its only parameter and returning bool. Handler for every event type has the same signature.
	typedef std::function<bool(const SystemEvent&)> SystemEventHandler;

	
	///<summary>
	///</summary>
	enum class SystemEventType : Enum
	{
		App = 0x0100,
		Input = 0x0200,
		Window = 0x0300
	};

	enum : Size_t
	{
		System_Event_Types_Num = 3
	};

	
	///<summary>
	///</summary>
	enum class SystemEventSubtype : Uint16
	{
		App_Command = static_cast<Enum>(SystemEventType::App) | 0x10,
		Input_Keyboard = static_cast<Enum>(SystemEventType::Input) | 0x10,
		Input_Mouse = static_cast<Enum>(SystemEventType::Input) | 0x20,
		Input_Touch = static_cast<Enum>(SystemEventType::Input) | 0x30,
		Window_Command = static_cast<Enum>(SystemEventType::Window) | 0x10,
		Window_Notification = static_cast<Enum>(SystemEventType::Window) | 0x20,
	};
	
	///<summary>
	///</summary>
	enum : Size_t
	{
		System_Event_Subtypes_Num = 7
	};

	
	///<summary>
	///</summary>
	enum class SystemEventCode : Enum
	{
		App_Command_Initialize = ExsEnumDeclareSystemEventCode(SystemEventSubtype::App_Command, 0x01),
		App_Command_Terminate,
		App_Command_Focus_Gained,
		App_Command_Focus_Lost,

		Input_Keyboard_Key_Pressed = ExsEnumDeclareSystemEventCode(SystemEventSubtype::Input_Keyboard, 0x11),
		Input_Keyboard_Key_Released,

		Input_Mouse_Button_Dblclick = ExsEnumDeclareSystemEventCode(SystemEventSubtype::Input_Mouse, 0x21),
		Input_Mouse_Button_Pressed,
		Input_Mouse_Button_Released,
		Input_Mouse_Move,
		Input_Mouse_Wheel_Scroll,
		Input_Touch,

		Window_Command_Close = ExsEnumDeclareSystemEventCode(SystemEventSubtype::Window_Command, 0x31),

		Window_Notification_Move = ExsEnumDeclareSystemEventCode(SystemEventSubtype::Window_Notification, 0x41),
		Window_Notification_Resize,
		Window_Notification_Visibility_Change,
	};

	enum : Size_t
	{
		System_Event_Codes_Num = 128
	};


	///<summary>
	///</summary>
	class SystemEventHandlerObject
	{
	public:
		virtual bool HandleEvent(const SystemEvent& event) = 0;
	};

	
	///<summary>
	/// Creates SystemEventHandler from specified callable and additional arguments which are bound with this callable.
	/// First argument of the callable must be <c>const SystemEvent&</c> and its return type must be <c>bool</c>.
	///</summary>
	template <class C, class... Args>
	inline SystemEventHandler BindEventHandler(C&& callable, Args&&... optArgs)
	{
		return std::bind<bool>(std::forward<C>(callable), std::placeholders::_1, std::forward<Args>(optArgs)...);
	}

	
	///<summary>
	/// Creates SystemEventHandler from given handler object. Handler object is stored using specified pointer (no copy is done)
	/// and it is the responsibility of the client to ensure, that this object is valid as long as newly created handler is used.
	/// To avoid this limitation (if it considered as such), use <c>BindEventHandlerObject{Handler, Args...}</c>.
	///</summary>
	inline SystemEventHandler BindEventHandlerObject(SystemEventHandlerObject* handler)
	{
		return std::bind<bool>([handler](const SystemEvent& event) -> bool {
			handler->HandleEvent(event);
		}, std::placeholders::_1);
	}

	
	///<summary>
	/// Creates SystemEventHandler from new SystemEventHandlerObject object of type <c>Handler</c>. Created handler object
	/// is initialized and managed by the newly created handler functor and is not accessible to the client at all.
	///</summary>
	template <class Handler, class... Args>
	SystemEventHandler BindEventHandlerObject(Args&&... cargs)
	{
		auto handlerObject = std::make_shared<Handler>(std::forward<Args>(cargs)...);
		return std::bind<bool>([handlerObject](const SystemEvent& event) -> bool {
			handlerObject->HandleEvent(event);
		}, std::placeholders::_1);
	}


}


#endif /* __Exs_Core_SystemEventBase_H__ */
