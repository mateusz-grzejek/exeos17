
#include "_Precompiled.h"
#include <ExsCore/System/SystemEventReceiver.h>
#include <ExsCore/System/SystemEvent.h>


namespace Exs
{


	SystemEventReceiver::SystemEventReceiver()
	: _dispatcher(nullptr)
	, _state(State_Default)
	{ }


	SystemEventReceiver::~SystemEventReceiver()
	{ }


	bool SystemEventReceiver::PostEvent(const SystemEvent& event)
	{
		SystemEventCode eventCode = event.GetCode();
		Uint8 eventCodeIndex = ExsEnumGetSystemEventIndex(eventCode);
		auto& eventHandlerInfo = this->_handlersArray[eventCodeIndex];

		if (eventHandlerInfo.handler && eventHandlerInfo.flags.is_set(HandlerState_Active))
		{
			eventHandlerInfo.handler(event);
			return true;
		}

		return false;
	}


	void SystemEventReceiver::RegisterHandler(SystemEventCode eventCode, SystemEventHandler&& handler)
	{
		ExsDebugAssert( !this->IsActive() );

		Uint8 eventCodeIndex = ExsEnumGetSystemEventIndex(eventCode);
		auto& eventHandlerInfo = this->_handlersArray[eventCodeIndex];
		eventHandlerInfo.handler = std::move(handler);
		eventHandlerInfo.flags = HandlerState_Default;
	}


	void SystemEventReceiver::RegisterHandler(SystemEventCode eventCode, const SystemEventHandler& handler)
	{
		ExsDebugAssert( !this->IsActive() );

		Uint8 eventCodeIndex = ExsEnumGetSystemEventIndex(eventCode);
		auto& eventHandlerInfo = this->_handlersArray[eventCodeIndex];
		eventHandlerInfo.handler = handler;
		eventHandlerInfo.flags = HandlerState_Default;
	}


	bool SystemEventReceiver::Activate(SystemEventDispatcher* dispatcher)
	{
		ExsDebugAssert( !this->IsActive() && (this->_dispatcher == nullptr) );

		if (!this->OnActivate(dispatcher))
			return false;

		this->_dispatcher = dispatcher;
		this->_state.set(State_Active);

		return true;
	}


	void SystemEventReceiver::Deactivate(SystemEventDispatcher* dispatcher)
	{
		ExsDebugAssert( this->IsActive() && (this->_dispatcher != nullptr) && (this->_dispatcher == dispatcher) );

		this->OnDeactivate(dispatcher);
		this->_state.unset(State_Active);
		this->_dispatcher = nullptr;
	}


	bool SystemEventReceiver::OnActivate(SystemEventDispatcher* dispatcher)
	{
		return true;
	}


	void SystemEventReceiver::OnDeactivate(SystemEventDispatcher* dispatcher)
	{ }


}
