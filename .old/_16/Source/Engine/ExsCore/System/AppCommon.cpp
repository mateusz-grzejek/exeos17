
#include "_Precompiled.h"
#include <ExsCore/CoreEngineState.h>
#include <ExsCore/System/AppCommon.h>


namespace Exs
{


	AppGlobalStateRefHandle CreateAppGlobalState(const CoreEngineStateRefHandle& coreEngineState)
	{
		auto state = std::make_shared<AppGlobalState>();

		ExsEnterCriticalSection(state->accessLock);
		{
			state->appObject = nullptr;
			state->eventDispatcher = nullptr;
			state->mainThread = nullptr;
		}
		ExsLeaveCriticalSection();

		return AppGlobalStateRefHandle(state);
	}


}
