
#pragma once

#ifndef __Exs_Core_EvtSysWindow_H__
#define __Exs_Core_EvtSysWindow_H__

#include "SystemEvent.h"


namespace Exs
{

	
	///<summary>
	///</summary>
	class EvtSysWindowCommand : public EvtSysWindow
	{
	public:
		EvtSysWindowCommand(SystemEventCode code)
		: EvtSysWindow(code)
		{ }
	};

	
	///<summary>
	///</summary>
	class EvtSysWindowCommandClose : public EvtSysWindowCommand
	{
	public:
		EvtSysWindowCommandClose()
		: EvtSysWindowCommand(SystemEventCode::Window_Command_Close)
		{ }
	};

	

	
	///<summary>
	///</summary>
	class EvtSysWindowNotification : public EvtSysWindow
	{
	public:
		EvtSysWindowNotification(SystemEventCode code)
		: EvtSysWindow(code)
		{ }
	};

	
	///<summary>
	///</summary>
	class EvtSysWindowNotificationMove : public EvtSysWindowNotification
	{
	protected:
		Position    _position;

	public:
		EvtSysWindowNotificationMove(const Position& position)
		: EvtSysWindowNotification(SystemEventCode::Window_Notification_Move)
		, _position(position)
		{ }

		const Position& GetPosition() const
		{
			return this->_position;
		}
	};

	
	///<summary>
	///</summary>
	class EvtSysWindowNotificationResize : public EvtSysWindowNotification
	{
	protected:
		Size    _size;

	public:
		EvtSysWindowNotificationResize(const Size& size)
		: EvtSysWindowNotification(SystemEventCode::Window_Notification_Resize)
		, _size(size)
		{ }

		const Size& GetSize() const
		{
			return this->_size;
		}
	};

	
	///<summary>
	///</summary>
	class EvtSysWindowNotificationVisibilityChange : public EvtSysWindowNotification
	{
	protected:
		bool    _visible;

	public:
		EvtSysWindowNotificationVisibilityChange(bool visible)
		: EvtSysWindowNotification(SystemEventCode::Window_Notification_Visibility_Change)
		, _visible(visible)
		{ }

		bool IsVisible() const
		{
			return this->_visible;
		}
	};


}


#endif /* __Exs_Core_EvtSysWindow_H__ */
