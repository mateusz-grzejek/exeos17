
#pragma once

#ifndef __Exs_Core_AppCommon_H__
#define __Exs_Core_AppCommon_H__

#include "CommonSystemTypes.h"
#include "../Threading/ExternalThreadWrapper.h"


namespace Exs
{


	class AppEventLoop;
	class AppObject;
	class SystemEventDispatcher;
	
	ExsDeclareSharedDataRefHandle(AppGlobalState);
	ExsDeclareSharedDataRefHandle(CoreEngineState);
	
	// Type of callback used by event loop. Callback of this type can be specified dynamically during creation of loop
	// which will use it as an additional update procedure.
	typedef std::function<bool()> AppInternalCallback;

	// Type of callback used by event loop. Callback of this type can be specified dynamically during creation of loop
	// which will use it as an additional update procedure.
	typedef std::function<bool()> AppUpdateCallback;


	///<summary>
	///</summary>
	enum AppExecutionStateFlags : Enum
	{
		//
		AppExecutionState_Idle = 0x4000,

		//
		AppExecutionState_Terminate_Request = 0x8000
	};


	///<summary>
	/// Flags used to control how application loop is started and executed.
	///</summary>
	enum AppEventLoopRunFlags : Enum
	{
		// Loop is started in normal (default) mode. This is the default behaviour.
		AppEventLoopRun_Default = 0,
	};


	///<summary>
	///</summary>
	class AppExecutionState
	{
	private:
		stdx::atomic_mask<Enum>  _state;

	public:
		AppExecutionState()
		: _state(0)
		{ }

		///<summary>
		///</summary>
		void Set(Enum stateFlags, std::memory_order order = std::memory_order_relaxed)
		{
			this->_state.set(stateFlags, order);
		}

		///<summary>
		///</summary>
		void Clear(Enum stateFlags, std::memory_order order = std::memory_order_relaxed)
		{
			this->_state.unset(stateFlags, order);
		}

		///<summary>
		///</summary>
		bool IsSet(Enum stateFlags, std::memory_order order = std::memory_order_relaxed) const
		{
			return this->_state.is_set(stateFlags, order);
		}
	};


	///<summary>
	///</summary>
	struct AppGlobalState : public SharedData
	{
		//
		AppObject* appObject;

		//
		SystemEventDispatcher* eventDispatcher;

		//
		AppExecutionState executionState;

		//
		ExternalThreadHandle mainThread;

		//
		SystemEnvData systemEnvData;
	};


	EXS_LIBAPI_CORE AppGlobalStateRefHandle CreateAppGlobalState(const CoreEngineStateRefHandle& coreEngineState);


}


#endif /* __Exs_Core_AppCommon_H__ */
