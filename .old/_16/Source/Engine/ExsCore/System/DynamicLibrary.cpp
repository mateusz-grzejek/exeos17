
#include "_Precompiled.h"
#include <ExsCore/System/DynamicLibrary.h>
#include <ExsCore/System/SystemUtils.h>
#include <stdx/string_utils.h>


namespace Exs
{


	DynamicLibrary::LoadGuard::LoadGuard(Handle handle)
	: handle(handle)
	{ }


	DynamicLibrary::LoadGuard::~LoadGuard()
	{
		if (this->handle != nullptr)
		{
			System::UnloadDynamicLibrary(this->handle);
			this->handle = nullptr;
		}
	}




	DynamicLibrary::DynamicLibrary()
	{ }


	DynamicLibrary::DynamicLibrary(LoadGuard* loadGuard, std::string&& fileName, std::string&& path)
	: _loadGuard(loadGuard)
	, _fileName(std::move(fileName))
	, _path(std::move(path))
	{ }


	DynamicLibrary::~DynamicLibrary()
	{ }


	void DynamicLibrary::Release()
	{
		this->_loadGuard = nullptr;
		this->_fileName.clear();
		this->_fileName.shrink_to_fit();
		this->_path.clear();
		this->_path.shrink_to_fit();
	}


	void* DynamicLibrary::RetrieveSymbol(const char* symName) const
	{
		ExsDebugAssert( symName != nullptr );

		if (!this->IsLoaded() || (*symName == '\0'))
			return nullptr;

		Handle dllHandle = this->_loadGuard->handle;
		void* symbolPtr = System::RetrieveDynamicLibrarySymbol(dllHandle, symName);

		return symbolPtr;
	}


	DynamicLibrary DynamicLibrary::Load(const std::string& path, DLLPathType pathType)
	{
		DynamicLibrary library { };

		if (!path.empty())
		{
			std::string dllPath = (pathType == DLLPathType::Base) ? ConvertBasePath(path) : path;
			NativeDLLHandle dllHandle = System::LoadDynamicLibrary(dllPath.c_str());

			if (dllHandle != nullptr)
			{
				size_t dpos = dllPath.find_last_of(EXS_ENV_DEFAULT_PATH_DELIMITER);
				std::string moduleFileName = (dpos != std::string::npos) ? dllPath.substr(dpos) : dllPath;
				LoadGuard* loadGuard = new LoadGuard(dllHandle);
				library = std::move(DynamicLibrary(loadGuard, std::move(moduleFileName), std::move(dllPath)));
			}
		}

		return library;
	}


	std::string DynamicLibrary::ConvertBasePath(const std::string& name)
	{
		size_t lastPathDelim = name.find_last_of(EXS_ENV_DEFAULT_PATH_DELIMITER);

		std::string path;
		std::string fileName;

		if (lastPathDelim != std::string::npos)
		{
			path = name.substr(0, lastPathDelim + 1);
			fileName = name.substr(lastPathDelim + 1);
		}
		else
		{
			fileName = name;
		}

		std::string result;
		result.reserve(name.length() + 10);
		result.append(path);
		result.append(EXS_ENV_DYNAMIC_LIBRARY_PREFIX);
		result.append(fileName);
		result.append(EXS_ENV_DYNAMIC_LIBRARY_EXTENSION);

		return result;
	}


}
