
#pragma once

#ifndef __Exs_Core_AppExecutionArgs_H__
#define __Exs_Core_AppExecutionArgs_H__

#include "../Prerequisites.h"
#include <stdx/assoc_array.h>


namespace Exs
{


	///<summary>
	/// Represents possible types of app execution arguments.
	///</summary>
	enum class AppExecutionArgType : Enum
	{
		/// Argument is a boolean value (true/false).
		Boolean = 1,

		/// Argument is a string literal.
		String,

		/// Argument is a 32-bit numeric value.
		Value,

		/// Unknown type.
		Unknown = 0
	};


	///<summary>
	///</summary>
	struct AppExecutionArgData
	{
		struct Content
		{
			bool boolean = false;
			std::string string = nullptr;
			Int32 value = 0;
		};

		AppExecutionArgType type = AppExecutionArgType::Unknown;
		std::string name;
		Content content;
	};


	///<summary>
	///</summary>
	struct AppExecutionArgDef
	{
		const char* name;
		AppExecutionArgType type;
	};


	template <AppExecutionArgType>
	struct AppExecutionArgTypeTraits;


	template <>
	struct AppExecutionArgTypeTraits<AppExecutionArgType::Boolean>
	{
		typedef bool Type;

		static bool Get(const AppExecutionArgData& argData)
		{
			ExsDebugAssert( (argData.type == AppExecutionArgType::Boolean) || (argData.type == AppExecutionArgType::Unknown) );
			return argData.content.boolean;
		}

		static bool GetDefault()
		{
			return false;
		}
	};

	template <>
	struct AppExecutionArgTypeTraits<AppExecutionArgType::String>
	{
		typedef std::string Type;

		static std::string Get(const AppExecutionArgData& argData)
		{
			ExsDebugAssert( (argData.type == AppExecutionArgType::String) || (argData.type == AppExecutionArgType::Unknown) );
			return argData.content.string;
		}

		static std::string GetDefault()
		{
			return nullptr;
		}
	};

	template <>
	struct AppExecutionArgTypeTraits<AppExecutionArgType::Value>
	{
		typedef Int32 Type;

		static Int32 Get(const AppExecutionArgData& argData)
		{
			ExsDebugAssert( (argData.type == AppExecutionArgType::Value) || (argData.type == AppExecutionArgType::Unknown) );
			return argData.content.value;
		}

		static Int32 GetDefault()
		{
			return 0;
		}
	};


	///<summary>
	///</summary>
	class EXS_LIBCLASS_CORE AppExecutionArgs
	{
	private:
		stdx::assoc_array<std::string, AppExecutionArgData> _argsArray;

	public:
		AppExecutionArgs();
		~AppExecutionArgs();

		bool AddArgDefinition(const char* name, AppExecutionArgType type);
		bool AddArgDefinition(const AppExecutionArgDef& argDef);

		void ParseArgsArray(const char** argv, Uint32 argc);

		void Reset();

		template <AppExecutionArgType ArgType>
		typename AppExecutionArgTypeTraits<ArgType>::Type GetArgValue(const char* argName) const;
	};


	template <AppExecutionArgType ArgType>
	inline typename AppExecutionArgTypeTraits<ArgType>::Type AppExecutionArgs::GetArgValue(const char* argName) const
	{
		auto argDataRef = this->_argsArray.find(argName);

		if ((argDataRef != this->_argsArray.end()) && (argDataRef->value.type == ArgType))
			return AppExecutionArgTypeTraits<ArgType>::Get(argDataRef->value);

		return AppExecutionArgTypeTraits<ArgType>::GetDefault();
	}


};


#endif /* __Exs_Core_AppExecutionArgs_H__ */
