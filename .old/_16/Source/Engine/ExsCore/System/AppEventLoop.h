
#pragma once

#ifndef __Exs_Core_AppEventLoop_H__
#define __Exs_Core_AppEventLoop_H__

#include "AppCommon.h"
#include "SystemEventDispatcher.h"


namespace Exs
{


	class AppObject;
	class SystemEventDispatcher;

	ExsDeclareSharedDataRefHandle(AppGlobalState);
	ExsDeclareSharedDataRefHandle(CoreEngineState);


	enum : Enum
	{
		// Indicates end of loop execution. It should be returned by the OnUpdate() method or update callback.
		RSC_App_Main_Loop_Execution_Abort = 0x98576
	};


	///<summary>
	///</summary>
	class EXS_LIBCLASS_CORE AppEventLoop
	{
		friend class AppObject;
		friend class AppObjectInitializer;

	private:
		AppObject*                  _appObject;
		CoreEngineStateRefHandle    _coreEngineState;
		AppGlobalStateRefHandle     _appGlobalState;
		SystemEventDispatcher       _eventDispatcher;

	public:
		AppEventLoop(AppObject* appObject, CoreEngineStateRefHandle coreEngineState, AppGlobalStateRefHandle appGlobalState);
		virtual ~AppEventLoop();

		///<summary>
		///</summary>
		bool SetEventReceiver(SystemEventReceiver* eventReceiver);

		///<summary>
		///</summary>
		AppObject* GetAppObject() const;

	friendapi:
		// Runs event loop with specified update callback (in addition to OnUpdate()). If 'AppEventLoopRun_No_OnUpdate' is specified, internal
		// OnUpdate() method will not be called. If both update methods are enabled, ?????.
		bool Run(stdx::mask<AppEventLoopRunFlags> flags, const AppUpdateCallback& updateCallback = nullptr);

	private:
		// Starts execution of the main loop. Implemented at the system-level loop class.
		virtual void EnterSystemLoop(const AppInternalCallback& internalCallback);

		// Executed immediately after loop is Run()'ed and state is successfuly validated.
		virtual bool OnBegin();

		// Executed after loop finishes execution. Result of the execution is passed as an argument.
		virtual void OnEnd();

		// Called on every single loop execution. Empty by default, not executed at all if AppEventLoopRun_No_OnUpdate flag is specified.
		virtual bool OnUpdate();

		//
		void ReleaseSharedStateRefs();

		// Internal helper used to bind call to protected ::OnUpdate.
		static bool InternalOnUpdateExec(AppEventLoop* eventLoop);
	};


	inline AppObject* AppEventLoop::GetAppObject() const
	{
		return this->_appObject;
	}


}


#endif /* __Exs_Core_AppEventLoop_H__ */
