
#pragma once

#ifndef __Exs_Core_CommonThreadDefs_H__
#define __Exs_Core_CommonThreadDefs_H__


#define ExsEnumDeclareThreadExecutionState(id, isActive, isRunning, isSuspended, isFinished) \
	((static_cast<Uint32>(id) << 16) | (static_cast<Uint32>(isActive & 0xF) << 12) | (static_cast<Uint32>(isRunning & 0xF) << 8)\
	| (static_cast<Uint32>(isSuspended & 0xF) << 4) | static_cast<Uint32>(isFinished & 0xF))

#define ExsEnumGetThreadExecutionStateIsActive(executionState) \
	(((static_cast<Uint32>(executionState) >> 12) & 0xF) != 0)

#define ExsEnumGetThreadExecutionStateIsRunning(executionState) \
	(((static_cast<Uint32>(executionState) >> 8) & 0xF) != 0)

#define ExsEnumGetThreadExecutionStateIsSuspended(executionState) \
	(((static_cast<Uint32>(executionState) >> 4) & 0xF) != 0)

#define ExsEnumGetThreadExecutionStateIsFinished(executionState) \
	((static_cast<Uint32>(executionState) & 0xF) != 0)


namespace Exs
{


	struct ThreadLocalStorage;

	class Message;
	class SystemThread;
	class Thread;
	class Transceiver;

	typedef void* ThreadHandle;
	typedef Uint16 ThreadIndex;
	typedef Enum ThreadUID;
	typedef Uint16 ThreadIndex;


	enum class ThreadExecutionState : Enum
	{
		Init = ExsEnumDeclareThreadExecutionState(0x3201, 0, 0, 0, 0),

		Active = ExsEnumDeclareThreadExecutionState(0x3202, 1, 0, 0, 0),

		Running = ExsEnumDeclareThreadExecutionState(0x3203, 1, 1, 0, 0),

		Idle_Sleep = ExsEnumDeclareThreadExecutionState(0x3204, 1, 0, 1, 0),

		Idle_Wait = ExsEnumDeclareThreadExecutionState(0x3205, 1, 0, 1, 0),

		Finished = ExsEnumDeclareThreadExecutionState(0x3206, 1, 0, 0, 1),

		Interrupted_Exit = ExsEnumDeclareThreadExecutionState(0x3207, 1, 0, 0, 1),

		Interrupted_Terminate = ExsEnumDeclareThreadExecutionState(0x3208, 1, 0, 0, 1),

		Unknown = ExsEnumDeclareThreadExecutionState(0, 0, 0, 0, 0)
	};


	enum class ThreadType : Enum
	{
		Active = 0x2C,

		Temporary = 0x7E,

		Unknown = 0
	};


	namespace Config
	{

		enum : Uint32
		{
			// The longest timeout, for which waiting for response may be continued.
			MSG_Response_Max_Wait_Timeout = 500,

			// Default state for option, that indicates, whether waiting for response should be taken if receiver is not
			// present/active.
			MSG_Response_Wait_For_Recipient_Default = 0,

			//
			THR_Default_Yield_Spin_Count = 16,

			// Default timeout for waiting for messages, in milliseconds. If this value is zero, default thread's wait
			// functions will return immediately if there are no messages.
			THR_Default_Message_Wait_Timeout = 66,

			//
			THR_Max_Active_Threads_Num = 16,

			//
			THR_Max_Temporary_Threads_Num = 64,

			//
			THR_Max_Total_Threads_Num = THR_Max_Active_Threads_Num + THR_Max_Temporary_Threads_Num,

			// Max number of thread-local singletons.
			TLS_Singleton_Array_Size = 32,

			//
			TLS_Singleton_Array_User_Base_Index = 0,

			// Size of the additional thread-local array for user data.
			TLS_Ext_Data_Array_Size = 32,

			//
			TLS_Ext_Data_Array_User_Base_Index = 16
		};

	}


	enum : ThreadIndex
	{
		Thread_Index_Auto = stdx::limits<ThreadIndex>::max_value
	};


	inline void YieldExecution(Uint32 yieldCount = stdx::limits<Uint32>::max_value)
	{
		if (yieldCount < Config::THR_Default_Yield_Spin_Count)
		{
			return;
		}
		else
		{
			std::this_thread::yield();
		}
	}


	///<summary>
	///</summary>
	struct ThreadBaseInfo
	{
		///
		ThreadIndex regIndex = stdx::limits<ThreadIndex>::max_value;

		///
		ThreadHandle threadHandle = nullptr;

		///
		ThreadType threadType = ThreadType::Unknown;

		///
		ThreadUID threadUID = 0;
	};

	
	///<summary>
	///</summary>
	class EXS_LIBCLASS_CORE CurrentThread
	{
	public:
		///<summary>  </summary>
		static ThreadLocalStorage* GetLocalStorage();
		
		///<summary>  </summary>
		static Result ValidateState();

		///<summary>  </summary>
		static ThreadHandle GetHandle();
		
		///<summary>  </summary>
		static ThreadUID GetUID();
		
		///<summary>  </summary>
		static Thread* GetThread();
		
		///<summary>  </summary>
		static void SleepFor(const Milliseconds& duration);
		
		///<summary>  </summary>
		EXS_ATTR_NO_RETURN static void Exit(Enum exitCode);
		
		///<summary>  </summary>
		EXS_ATTR_NO_RETURN static void Terminate(Enum terminateCode);
	};


	///<summary>
	/// Represents current thread of execution. Every instance of this class is filled (during construction process) with
	/// informations about current thread.
	///</summary>
	///
	///<remarks>
	/// Note, that this class cannot be used as a "normal" object (at least not in arbitrary context). Every member
	/// function of this class can be called *only* by the thread which created its particular instance used to access
	/// those functions. This restriction applies also to destructor. The idea behind this class is performance - since
	/// working with "current" thread usually involves querying thread-specific state (which may not be efficient on some
	/// platforms) caching this info inside an instance of this class allows writing more efficient code (especially
	/// if there is more than one operation to be executed). Also, note, that above restrictions do not forbid you from
	/// storing instances of this class as long as you can safely ensure, that only one (parent) thread will use it.
	///</remarks>
	class EXS_LIBCLASS_CORE CurrentThreadObject
	{
	private:
		ThreadLocalStorage*    _tls;
		ThreadHandle           _handle;
		ThreadUID              _uid;
		Thread*                _thread;
		Transceiver*           _transceiver;

	public:
		CurrentThreadObject();
		~CurrentThreadObject();

		operator bool() const;
		
		///<summary>  </summary>
		void SleepFor(const Milliseconds& duration);
		
		///<summary>  </summary>
		ThreadHandle GetHandle();

		///<summary>  </summary>
		ThreadLocalStorage* GetLocalStorage();

		///<summary>  </summary>
		Thread* GetThread();

		///<summary>  </summary>
		ThreadUID GetUID();

	private:
		void _Initialize();
		void _Release();
	};


	inline CurrentThreadObject::operator bool() const
	{
		return this->_tls != nullptr;
	}

	inline ThreadHandle CurrentThreadObject::GetHandle()
	{
		return this->_handle;
	}

	inline ThreadLocalStorage* CurrentThreadObject::GetLocalStorage()
	{
		return this->_tls;
	}

	inline Thread* CurrentThreadObject::GetThread()
	{
		return this->_thread;
	}

	inline ThreadUID CurrentThreadObject::GetUID()
	{
		return this->_uid;
	}


}


#endif /* __Exs_Core_CommonThreadDefs_H__ */
