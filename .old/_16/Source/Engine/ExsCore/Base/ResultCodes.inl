
namespace Exs
{


	enum : Uint16
	{
		RSCT_None,
		RSCT_Common,
		RSCT_Input,
		RSCT_Messaging,
		RSCT_System,
		RSCT_Tasks,
		RSCT_Threads
	};


	enum : Result_code_t
	{
		// Uninitialized RSC.
		RSC_None = stdx::limits<Result_code_t>::max_value,

		// Success.
		RSC_Success = ExsEnumDeclareResultCode(RSCT_None, 0x000),

		// Operation has failed.
		RSC_Err_Failed = ExsEnumDeclareResultCode(RSCT_None, 0x002),

		// Requested operation could not be completed, because required state has not been initialized yet.
		RSC_Err_Not_Initialized = ExsEnumDeclareResultCode(RSCT_None, 0x004),

		// Requested object or value was not found.
		RSC_Err_Not_Found = ExsEnumDeclareResultCode(RSCT_None, 0x006),

		// Used/requested feature or option is not supported on current platform.
		RSC_Err_Not_Supported = ExsEnumDeclareResultCode(RSCT_None, 0x007),

		//
		RSC_Err_Out_Of_Range = ExsEnumDeclareResultCode(RSCT_None, 0x008),

		// Requested action could not be completed, because associated object or value is still in use.
		RSC_Err_Still_In_Use = ExsEnumDeclareResultCode(RSCT_None, 0x009),

		// Unknown error has occured.
		RSC_Err_Unknown,

		/////////////////////////// RSCT_Common ///////////////////////////

		//
		RSC_Err_Already_Registered,

		// Specified external binary resource (library, plugin, etc.) has been compiled for achitecture which
		// is not compatible with the one, that is targeted by the current build of the engine.
		RSC_Err_Arch_Not_Compatible = ExsEnumDeclareResultCode(RSCT_Common, 0x001),

		//
		RSC_Err_Invalid_Access,

		// Specified object handle is invalid.
		RSC_Err_Invalid_Handle,

		//
		RSC_Err_Invalid_Operation,

		//
		RSC_Err_Invalid_State,

		//
		RSC_Err_Invalid_Value,

		//
		RSC_Err_Registration_State_Mismatch,

		// Waiting object has been signaled using explicit abort request (all threads were singalled).
		RSC_Wait_Error,

		// Waiting object has not been signaled due to explicit abort request.
		RSC_Wait_Res_Abort,

		// Waiting object has not been signaled due to explicit abort request.
		RSC_Wait_Res_Interrupt,

		// Waiting object has not been signaled due to timeout.
		RSC_Wait_Res_Timeout,

		// Specified/requested version of the component is not supported by current version of the engine.
		RSC_Err_Version_Not_Supported,

		/////////////////////////// RSCT_Input ///////////////////////////

		// DLL has been already loaded.
		RSC_Err_DLL_Already_Loaded = ExsEnumDeclareResultCode(RSCT_Input, 0x001),

		// Requested DLL file could not be found.
		RSC_Err_DLL_Missing,

		//
		RSC_Err_DLL_Module_Invalid,

		// Cannot open specified file.
		RSC_Err_File_Cannot_Open,

		// Byte squence is not a valid UTF sequence.
		RSC_Err_Unicode_Invalid_Byte_Sequence,

		// Specified/assembled code point is not a valid UC code point.
		RSC_Err_Unicode_Invalid_Code_Point,

		/////////////////////////// RSCT_Messaging ///////////////////////////

		// Message could not be delivered and has been discarded.
		RSC_Msg_Message_Discarded = ExsEnumDeclareResultCode(RSCT_Messaging, 0x001),

		//
		RSC_Msg_Message_Enqueued,

		// Specified recipient has disabled receiving of new messages.
		RSC_Msg_Receiver_Inactive,

		// Could not fetch message from requested inbox, because it is empty.
		RSC_Msg_Receiver_Inbox_Empty,

		//
		RSC_Err_Transceiver_Activation_Failed,

		/////////////////////////// RSCT_Tasks ///////////////////////////

		//
		RSC_Task_Already_Launched = ExsEnumDeclareResultCode(RSCT_Tasks, 0x001),

		/////////////////////////// RSCT_Threads ///////////////////////////

		//
		RSC_Thread_Unknown_Type = ExsEnumDeclareResultCode(RSCT_Threads, 0x001),

		//
		RSC_Thread_Subthreads_Not_Allowed
	};


}
