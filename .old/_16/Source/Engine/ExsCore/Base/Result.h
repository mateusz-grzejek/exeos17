
#pragma once

#ifndef __Exs_Core_Result_H__
#define __Exs_Core_Result_H__


#define EXS_RESULT_CODE_NCID_RESERVED    0x18000000
#define EXS_RESULT_CODE_CATEGORY_MASK    0x00FF0000
#define EXS_RESULT_CODE_IID_MASK         0x0000FFFF

#define ExsEnumDeclareResultCode(category, iid) \
	(EXS_RESULT_CODE_NCID_RESERVED | (((Result_code_t)(category) & 0xFF) << 16) | ((Result_code_t)(iid) & 0xFFFF))

#define ExsEnumGetResultCategory(resultCode) \
	(Uint8)(((Result_code_t)(resultCode) & EXS_RESULT_CODE_CATEGORY_MASK) >> 16)

#define ExsEnumValidateResultCode(resultCode) \
	EXS_NCID_GET_RESERVED((Result_code_t)(resultCode)) == EXS_RESULT_CODE_NCID_RESERVED


#if !( EXS_CONFIG_BASE_ENABLE_EXTENDED_RESULT_INFO )
#
#  if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
#    define EXS_CONFIG_BASE_ENABLE_EXTENDED_RESULT_INFO 1
#  else
#    define EXS_CONFIG_BASE_ENABLE_EXTENDED_RESULT_INFO 1
#  endif
#
#endif


#if ( EXS_CONFIG_BASE_ENABLE_EXTENDED_RESULT_INFO )
#
#  define ExsResult(code)          Result((Result_code_t)(code), #code, "")
#  define ExsResultEx(code, info)  Result((Result_code_t)(code), #code, info)
#
#else
#
#  define ExsResult(code)          code
#  define ExsResultEx(code, info)  code
#
#endif


namespace Exs
{


	typedef Enum Result_code_t;


	///<summary>
	///</summary>
	enum class ResultType : Uint8
	{
		Success = 0x01,
		Notification = 0x02,
		Warning = 0x03,
		Error = 0x04,
		Unknown = 0
	};


#if ( EXS_CONFIG_BASE_ENABLE_EXTENDED_RESULT_INFO )

	struct Result
	{
	public:
		Result_code_t  code;

	private:
		const char*  _codeName;
		const char*  _info;

	public:
		Result()
		: code(0)
		, _codeName("")
		, _info("")
		{ }

		explicit Result(Result_code_t code)
		: code(code)
		, _codeName("")
		, _info("")
		{ }

		Result(Result_code_t code, const char* codeName, const char* info = nullptr)
		: code(code)
		, _codeName((codeName != nullptr) ? codeName : "")
		, _info((info != nullptr) ? info : "")
		{ }

		Result& operator=(Result_code_t rhs)
		{
			this->code = rhs;
			this->_codeName = this->_info = "";
			return *this;
		}

		explicit operator Result_code_t() const
		{
			return this->code;
		}

		void Swap(Result& other)
		{
			std::swap(this->code, other.code);
			std::swap(this->_codeName, other._codeName);
			std::swap(this->_info, other._info);
		}
	};


	inline bool operator==(const Result& lhs, const Result& rhs)
	{
		return lhs.code == rhs.code;
	}

	inline bool operator==(const Result& lhs, Result_code_t rhs)
	{
		return lhs.code == rhs;
	}

	inline bool operator==(Result_code_t lhs, const Result& rhs)
	{
		return lhs == rhs.code;
	}

	inline bool operator!=(const Result& lhs, const Result& rhs)
	{
		return lhs.code != rhs.code;
	}

	inline bool operator!=(const Result& lhs, Result_code_t rhs)
	{
		return lhs.code != rhs;
	}

	inline bool operator!=(Result_code_t lhs, const Result& rhs)
	{
		return lhs != rhs.code;
	}

	inline bool operator<(const Result& lhs, const Result& rhs)
	{
		return lhs.code < rhs.code;
	}

	inline bool operator<(const Result& lhs, Result_code_t rhs)
	{
		return lhs.code < rhs;
	}

	inline bool operator<(Result_code_t lhs, const Result& rhs)
	{
		return lhs < rhs.code;
	}

	inline bool operator<=(const Result& lhs, const Result& rhs)
	{
		return lhs.code <= rhs.code;
	}

	inline bool operator<=(const Result& lhs, Result_code_t rhs)
	{
		return lhs.code <= rhs;
	}

	inline bool operator<=(Result_code_t lhs, const Result& rhs)
	{
		return lhs <= rhs.code;
	}

	inline bool operator>(const Result& lhs, const Result& rhs)
	{
		return lhs.code > rhs.code;
	}

	inline bool operator>(const Result& lhs, Result_code_t rhs)
	{
		return lhs.code > rhs;
	}

	inline bool operator>(Result_code_t lhs, const Result& rhs)
	{
		return lhs > rhs.code;
	}

	inline bool operator>=(const Result& lhs, const Result& rhs)
	{
		return lhs.code >= rhs.code;
	}

	inline bool operator>=(const Result& lhs, Result_code_t rhs)
	{
		return lhs.code >= rhs;
	}

	inline bool operator>=(Result_code_t lhs, const Result& rhs)
	{
		return lhs >= rhs.code;
	}


	inline void swap(Result& left, Result& right)
	{
		left.Swap(right);
	}

#else

	typedef Enum Result;

#endif


}


#endif /* __Exs_Core_Result_H__ */
