
#include "_Precompiled.h"
#include <ExsCore/Prerequisites.h>


#if ( EXS_TARGET_SYSTEM & (EXS_TARGET_PLATFORM_FLAG_WINDOWS | EXS_TARGET_PLATFORM_FLAG_WINPHONE) )

LONGLONG GetPerfCounterFrequency()
{
	LARGE_INTEGER counterFrequency;
	QueryPerformanceFrequency(&counterFrequency);
	return counterFrequency.QuadPart;
}

static const LONGLONG perfCounterFrequency = GetPerfCounterFrequency();

#endif


namespace Exs
{


	Duration_value_t TimeStamp::_conv(Uint64 value, Int rnum, Int rdenom)
	{
	#if ( EXS_TARGET_SYSTEM & (EXS_TARGET_PLATFORM_FLAG_WINDOWS | EXS_TARGET_PLATFORM_FLAG_WINPHONE) )

		LONGLONG timestampValue = (value * rdenom / rnum) / perfCounterFrequency;
		return static_cast<Duration_value_t>(timestampValue);

	#elif ( EXS_TARGET_SYSTEM & EXS_TARGET_PLATFORM_FLAG_POSIX )

		return 0;

	#else

		return 0;

	#endif
	}


	TimeStamp PerfCounter::GetCurrentTimeStamp()
	{
	#if ( EXS_TARGET_SYSTEM & (EXS_TARGET_PLATFORM_FLAG_WINDOWS | EXS_TARGET_PLATFORM_FLAG_WINPHONE) )

		LARGE_INTEGER perfCounter;
		QueryPerformanceCounter(&perfCounter);
		return TimeStamp(static_cast<Uint64>(perfCounter.QuadPart));

	#elif ( EXS_TARGET_SYSTEM & EXS_TARGET_PLATFORM_FLAG_POSIX )

		timespec timeData { };
		clock_gettime(CLOCK_REALTIME, &timeData);
		return TimeStamp((static_cast<Uint64>(timeData.tv_sec) * 1000000000) + static_cast<Uint64>(timeData.tv_nsec));

	#else

		return TimeStamp(0);

	#endif
	}


}
