
#include "_Precompiled.h"
#include <ExsCore/Prerequisites.h>
#include <zlib/zlib.h>

#define EXS_HASH_INIT_DEFAULT_ADLER32 1
#define EXS_HASH_INIT_DEFAULT_CRC32   0xFFFFFFFF
#define EXS_HASH_INIT_DEFAULT_DJB2    0x1505
#define EXS_HASH_INIT_DEFAULT_FNV1A   0xCBF29CE484222325
#define EXS_HASH_INIT_DEFAULT_SDBM    0

#define EXS_HASH_INIT_DEFAULT_MD50    0x01234567
#define EXS_HASH_INIT_DEFAULT_MD51    0x89ABCDEF
#define EXS_HASH_INIT_DEFAULT_MD52    0xFEDCBA98
#define EXS_HASH_INIT_DEFAULT_MD53    0x76543210

#define EXS_MD5_F(b, c, d) (((b) & (c)) | ((~b) & (d)))
#define EXS_MD5_G(b, c, d) (((b) & (d)) | ((c) & (~d)))
#define EXS_MD5_H(b, c, d) ((b) ^ (c) ^ (d))
#define EXS_MD5_I(b, c, d) ((c) ^ ((b) | (~d)))

#define EXS_MD5_XF(a, b, c, d, word, shift, md5const)\
	a += EXS_MD5_F(b, c, d) + word + md5const;\
	a = EXS_ROTL32(a, shift) + b;

#define EXS_MD5_XG(a, b, c, d, word, shift, md5const)\
	a += EXS_MD5_G(b, c, d) + word + md5const;\
	a = EXS_ROTL32(a, shift) + b;

#define EXS_MD5_XH(a, b, c, d, word, shift, md5const)\
	a += EXS_MD5_H(b, c, d) + word + md5const;\
	a = EXS_ROTL32(a, shift) + b;

#define EXS_MD5_XI(a, b, c, d, word, shift, md5const)\
	a += EXS_MD5_I(b, c, d) + word + md5const;\
	a = EXS_ROTL32(a, shift) + b;

#define EXS_MD5_SHIFT_R1_1	7
#define EXS_MD5_SHIFT_R1_2	12
#define EXS_MD5_SHIFT_R1_3	17
#define EXS_MD5_SHIFT_R1_4	22
#define EXS_MD5_SHIFT_R2_1	5
#define EXS_MD5_SHIFT_R2_2	9
#define EXS_MD5_SHIFT_R2_3	14
#define EXS_MD5_SHIFT_R2_4	20
#define EXS_MD5_SHIFT_R3_1	4
#define EXS_MD5_SHIFT_R3_2	11
#define EXS_MD5_SHIFT_R3_3	16
#define EXS_MD5_SHIFT_R3_4	23
#define EXS_MD5_SHIFT_R4_1	6
#define EXS_MD5_SHIFT_R4_2	10
#define EXS_MD5_SHIFT_R4_3	15
#define EXS_MD5_SHIFT_R4_4	21


namespace Exs
{


	Uint32 HashCodeTraits<HashAlgorithm::Adler32>::Update(Uint32 hash, const void* data, Size_t length)
	{
		Uint32 result = hash;

		if (length > 0)
		{
			const Byte* inputBytes = reinterpret_cast<const Byte*>(data);
			result = adler32(hash, inputBytes, truncate_cast<uInt>(length));
		}

		return result;
	}


	Uint32 HashCodeTraits<HashAlgorithm::Adler32>::Compute(const void* data, Size_t length)
	{
		return Update(EXS_HASH_INIT_DEFAULT_ADLER32, data, length);
	}


	Uint32 HashCodeTraits<HashAlgorithm::CRC32>::Update(Uint32 hash, const void* data, Size_t length)
	{
		Uint32 result = hash;

		if (length > 0)
		{
			const Byte* inputBytes = reinterpret_cast<const Byte*>(data);

		#if ( EXS_SUPPORTED_EIS & EXS_EIS_SSE42 )
			for ( ; length >= 4; bytes += 4, length -= 4)
			{
				result = _mm_crc32_u32(hash, *(reinterpret_cast<const Uint32*>(bytes)));
			}
		#endif

			result = crc32(hash, inputBytes, truncate_cast<uInt>(length));
		}

		return result;
	}


	Uint32 HashCodeTraits<HashAlgorithm::CRC32>::Compute(const void* data, Size_t length)
	{
		return Update(EXS_HASH_INIT_DEFAULT_CRC32, data, length);
	}


	Uint32 HashCodeTraits<HashAlgorithm::DJB2>::Update(Uint32 hash, const void* data, Size_t length)
	{
		Uint32 result = hash;

		if (length > 0)
		{
			const Byte* inputBytes = reinterpret_cast<const Byte*>(data);

			while (length > 0)
			{
				result = ((result << 5) + result) ^ (*inputBytes);
				++inputBytes;
				--length;
			}
		}

		return result;
	}


	Uint32 HashCodeTraits<HashAlgorithm::DJB2>::Compute(const void* data, Size_t length)
	{
		return Update(EXS_HASH_INIT_DEFAULT_DJB2, data, length);
	}


	Uint64 HashCodeTraits<HashAlgorithm::FNV1A>::Update(Uint64 hash, const void* data, Size_t length)
	{
		Uint64 result = hash;

		if (length > 0)
		{
			const Uint64 fnvPrimeValue = 0x100000001B3;
			const Uint64 u64Mask = std::numeric_limits<Uint64>::max() << 8;
			const Byte* inputBytes = reinterpret_cast<const Byte*>(data);

			while (length > 0)
			{
				result = result * fnvPrimeValue;
				result = (result & u64Mask) | ((result & 0xFF) ^ (*inputBytes));
				++inputBytes;
				--length;
			}
		}

		return result;
	}


	Uint64 HashCodeTraits<HashAlgorithm::FNV1A>::Compute(const void* data, Size_t length)
	{
		return Update(EXS_HASH_INIT_DEFAULT_FNV1A, data, length);
	}


	Uint32 HashCodeTraits<HashAlgorithm::SDBM>::Update(Uint32 hash, const void* data, Size_t length)
	{
		Uint32 result = hash;

		if (length > 0)
		{
			const Byte* inputBytes = reinterpret_cast<const Byte*>(data);

			while (length > 0)
			{
				result = (*inputBytes) + (result << 6) + (result << 16) - result;
				++inputBytes;
				--length;
			}
		}

		return result;
	}


	Uint32 HashCodeTraits<HashAlgorithm::SDBM>::Compute(const void* data, Size_t length)
	{
		return Update(EXS_HASH_INIT_DEFAULT_SDBM, data, length);
	}


	void HashCodeTraits<HashAlgorithm::MD5>::Update(const Uint32* hash, const void* data, Size_t length, Uint32* output)
	{
		if (length == 0)
			return;

		Uint32& hash1 = output[0];
		Uint32& hash2 = output[1];
		Uint32& hash3 = output[2];
		Uint32& hash4 = output[3];

		hash1 = hash[0];
		hash2 = hash[1];
		hash3 = hash[2];
		hash4 = hash[3];

		Uint32 length32 = truncate_cast<Uint32>(length);
		Uint64 inputLengthBits = length32 * 8 + 1; //Input + 1bit at the end.
		Uint64 inputLengthBitsMod512 = inputLengthBits % 512;

		if (inputLengthBits <= 448) //Input length must be 448 mod 512.
			inputLengthBits += (448 - inputLengthBitsMod512);
		else
			inputLengthBits += (512 - inputLengthBitsMod512) + 448;

		Uint32 md5InputLength = truncate_cast<Uint32>(inputLengthBits / 8);
		Uint32 md5InputLengthDiff = md5InputLength - length32;

		unsigned char* md5Input = (Byte*)(malloc(sizeof(unsigned char) * (md5InputLength + 8)));
		ExsCopyMemory(md5Input, data, length32);
		md5Input[length32] = 0x80;
		ExsZeroMemory(md5Input + length32 + 1, md5InputLengthDiff);

		//Append additional 64 bits: length mod 2^64.
		ByteConv<Uint32, ByteOrder::Big_Endian>::ToBytes(length32 * 8, md5Input + md5InputLength);
		ByteConv<Uint32, ByteOrder::Big_Endian>::ToBytes(length32 >> 29, md5Input + md5InputLength + 4);
		// Exs::BytesUtils<ByteOrder::Big_Endian>::Decompose(length32 * 8, md5Input + md5InputLength);
		// Exs::BytesUtils<ByteOrder::Big_Endian>::Decompose(length32 >> 29, md5Input + md5InputLength + 4);

		const Uint32 md5ChunkSize = 512/8;

		for (Uint32 chunkOffset = 0; chunkOffset < md5InputLength; chunkOffset += md5ChunkSize)
		{
			Uint32 md5Words[16];
			for (Uint32 wordNum = 0; wordNum < 16; ++wordNum)
			{
				const Byte* md5InputPtr = md5Input + chunkOffset;
				ByteConv<Uint32, ByteOrder::Big_Endian>::ToValue(md5InputPtr + wordNum * 4, &(md5Words[wordNum]));
				// Exs::BytesUtils<ByteOrder::Big_Endian>::Compose(md5InputPtr + wordNum * 4, md5Words + wordNum);
			}

			Uint32 a = hash1;
			Uint32 b = hash2;
			Uint32 c = hash3;
			Uint32 d = hash4;

			EXS_MD5_XF(a, b, c, d, md5Words[0 ], EXS_MD5_SHIFT_R1_1, 0xd76aa478);
			EXS_MD5_XF(d, a, b, c, md5Words[1 ], EXS_MD5_SHIFT_R1_2, 0xe8c7b756);
			EXS_MD5_XF(c, d, a, b, md5Words[2 ], EXS_MD5_SHIFT_R1_3, 0x242070db);
			EXS_MD5_XF(b, c, d, a, md5Words[3 ], EXS_MD5_SHIFT_R1_4, 0xc1bdceee);
			EXS_MD5_XF(a, b, c, d, md5Words[4 ], EXS_MD5_SHIFT_R1_1, 0xf57c0faf);
			EXS_MD5_XF(d, a, b, c, md5Words[5 ], EXS_MD5_SHIFT_R1_2, 0x4787c62a);
			EXS_MD5_XF(c, d, a, b, md5Words[6 ], EXS_MD5_SHIFT_R1_3, 0xa8304613);
			EXS_MD5_XF(b, c, d, a, md5Words[7 ], EXS_MD5_SHIFT_R1_4, 0xfd469501);
			EXS_MD5_XF(a, b, c, d, md5Words[8 ], EXS_MD5_SHIFT_R1_1, 0x698098d8);
			EXS_MD5_XF(d, a, b, c, md5Words[9 ], EXS_MD5_SHIFT_R1_2, 0x8b44f7af);
			EXS_MD5_XF(c, d, a, b, md5Words[10], EXS_MD5_SHIFT_R1_3, 0xffff5bb1);
			EXS_MD5_XF(b, c, d, a, md5Words[11], EXS_MD5_SHIFT_R1_4, 0x895cd7be);
			EXS_MD5_XF(a, b, c, d, md5Words[12], EXS_MD5_SHIFT_R1_1, 0x6b901122);
			EXS_MD5_XF(d, a, b, c, md5Words[13], EXS_MD5_SHIFT_R1_2, 0xfd987193);
			EXS_MD5_XF(c, d, a, b, md5Words[14], EXS_MD5_SHIFT_R1_3, 0xa679438e);
			EXS_MD5_XF(b, c, d, a, md5Words[15], EXS_MD5_SHIFT_R1_4, 0x49b40821);

			EXS_MD5_XG(a, b, c, d, md5Words[1 ], EXS_MD5_SHIFT_R2_1, 0xf61e2562);
			EXS_MD5_XG(d, a, b, c, md5Words[6 ], EXS_MD5_SHIFT_R2_2, 0xc040b340);
			EXS_MD5_XG(c, d, a, b, md5Words[11], EXS_MD5_SHIFT_R2_3, 0x265e5a51);
			EXS_MD5_XG(b, c, d, a, md5Words[0 ], EXS_MD5_SHIFT_R2_4, 0xe9b6c7aa);
			EXS_MD5_XG(a, b, c, d, md5Words[5 ], EXS_MD5_SHIFT_R2_1, 0xd62f105d);
			EXS_MD5_XG(d, a, b, c, md5Words[10], EXS_MD5_SHIFT_R2_2, 0x02441453);
			EXS_MD5_XG(c, d, a, b, md5Words[15], EXS_MD5_SHIFT_R2_3, 0xd8a1e681);
			EXS_MD5_XG(b, c, d, a, md5Words[4 ], EXS_MD5_SHIFT_R2_4, 0xe7d3fbc8);
			EXS_MD5_XG(a, b, c, d, md5Words[9 ], EXS_MD5_SHIFT_R2_1, 0x21e1cde6);
			EXS_MD5_XG(d, a, b, c, md5Words[14], EXS_MD5_SHIFT_R2_2, 0xc33707d6);
			EXS_MD5_XG(c, d, a, b, md5Words[3 ], EXS_MD5_SHIFT_R2_3, 0xf4d50d87);
			EXS_MD5_XG(b, c, d, a, md5Words[8 ], EXS_MD5_SHIFT_R2_4, 0x455a14ed);
			EXS_MD5_XG(a, b, c, d, md5Words[13], EXS_MD5_SHIFT_R2_1, 0xa9e3e905);
			EXS_MD5_XG(d, a, b, c, md5Words[2 ], EXS_MD5_SHIFT_R2_2, 0xfcefa3f8);
			EXS_MD5_XG(c, d, a, b, md5Words[7 ], EXS_MD5_SHIFT_R2_3, 0x676f02d9);
			EXS_MD5_XG(b, c, d, a, md5Words[12], EXS_MD5_SHIFT_R2_4, 0x8d2a4c8a);

			EXS_MD5_XH(a, b, c, d, md5Words[5 ], EXS_MD5_SHIFT_R3_1, 0xfffa3942);
			EXS_MD5_XH(d, a, b, c, md5Words[8 ], EXS_MD5_SHIFT_R3_2, 0x8771f681);
			EXS_MD5_XH(c, d, a, b, md5Words[11], EXS_MD5_SHIFT_R3_3, 0x6d9d6122);
			EXS_MD5_XH(b, c, d, a, md5Words[14], EXS_MD5_SHIFT_R3_4, 0xfde5380c);
			EXS_MD5_XH(a, b, c, d, md5Words[1 ], EXS_MD5_SHIFT_R3_1, 0xa4beea44);
			EXS_MD5_XH(d, a, b, c, md5Words[4 ], EXS_MD5_SHIFT_R3_2, 0x4bdecfa9);
			EXS_MD5_XH(c, d, a, b, md5Words[7 ], EXS_MD5_SHIFT_R3_3, 0xf6bb4b60);
			EXS_MD5_XH(b, c, d, a, md5Words[10], EXS_MD5_SHIFT_R3_4, 0xbebfbc70);
			EXS_MD5_XH(a, b, c, d, md5Words[13], EXS_MD5_SHIFT_R3_1, 0x289b7ec6);
			EXS_MD5_XH(d, a, b, c, md5Words[0 ], EXS_MD5_SHIFT_R3_2, 0xeaa127fa);
			EXS_MD5_XH(c, d, a, b, md5Words[3 ], EXS_MD5_SHIFT_R3_3, 0xd4ef3085);
			EXS_MD5_XH(b, c, d, a, md5Words[6 ], EXS_MD5_SHIFT_R3_4, 0x04881d05);
			EXS_MD5_XH(a, b, c, d, md5Words[9 ], EXS_MD5_SHIFT_R3_1, 0xd9d4d039);
			EXS_MD5_XH(d, a, b, c, md5Words[12], EXS_MD5_SHIFT_R3_2, 0xe6db99e5);
			EXS_MD5_XH(c, d, a, b, md5Words[15], EXS_MD5_SHIFT_R3_3, 0x1fa27cf8);
			EXS_MD5_XH(b, c, d, a, md5Words[2 ], EXS_MD5_SHIFT_R3_4, 0xc4ac5665);

			EXS_MD5_XI(a, b, c, d, md5Words[0 ], EXS_MD5_SHIFT_R4_1, 0xf4292244);
			EXS_MD5_XI(d, a, b, c, md5Words[7 ], EXS_MD5_SHIFT_R4_2, 0x432aff97);
			EXS_MD5_XI(c, d, a, b, md5Words[14], EXS_MD5_SHIFT_R4_3, 0xab9423a7);
			EXS_MD5_XI(b, c, d, a, md5Words[5 ], EXS_MD5_SHIFT_R4_4, 0xfc93a039);
			EXS_MD5_XI(a, b, c, d, md5Words[12], EXS_MD5_SHIFT_R4_1, 0x655b59c3);
			EXS_MD5_XI(d, a, b, c, md5Words[3 ], EXS_MD5_SHIFT_R4_2, 0x8f0ccc92);
			EXS_MD5_XI(c, d, a, b, md5Words[10], EXS_MD5_SHIFT_R4_3, 0xffeff47d);
			EXS_MD5_XI(b, c, d, a, md5Words[1 ], EXS_MD5_SHIFT_R4_4, 0x85845dd1);
			EXS_MD5_XI(a, b, c, d, md5Words[8 ], EXS_MD5_SHIFT_R4_1, 0x6fa87e4f);
			EXS_MD5_XI(d, a, b, c, md5Words[15], EXS_MD5_SHIFT_R4_2, 0xfe2ce6e0);
			EXS_MD5_XI(c, d, a, b, md5Words[6 ], EXS_MD5_SHIFT_R4_3, 0xa3014314);
			EXS_MD5_XI(b, c, d, a, md5Words[13], EXS_MD5_SHIFT_R4_4, 0x4e0811a1);
			EXS_MD5_XI(a, b, c, d, md5Words[4 ], EXS_MD5_SHIFT_R4_1, 0xf7537e82);
			EXS_MD5_XI(d, a, b, c, md5Words[11], EXS_MD5_SHIFT_R4_2, 0xbd3af235);
			EXS_MD5_XI(c, d, a, b, md5Words[2 ], EXS_MD5_SHIFT_R4_3, 0x2ad7d2bb);
			EXS_MD5_XI(b, c, d, a, md5Words[9 ], EXS_MD5_SHIFT_R4_4, 0xeb86d391);

			hash1 += a;
			hash2 += b;
			hash3 += c;
			hash4 += d;
		}

		free(md5Input);
	}


	void HashCodeTraits<HashAlgorithm::MD5>::Compute(const void* data, Size_t length, Uint32* output)
	{
		Uint32 defaultInitHash[] =
		{
			EXS_HASH_INIT_DEFAULT_MD50,
			EXS_HASH_INIT_DEFAULT_MD51,
			EXS_HASH_INIT_DEFAULT_MD52,
			EXS_HASH_INIT_DEFAULT_MD53
		};

		Update(defaultInitHash, data, length, output);
	}


}
