
#include "_Precompiled.h"
#include <iostream>


namespace Exs
{


    std::ostream& operator<<(std::ostream& stream, const SourceLocationInfo& srcInfo)
    {
        stream << srcInfo.function << "(" << srcInfo.fileName << ", line " << srcInfo.lineNumber << ")";
        return stream;
    }


}
