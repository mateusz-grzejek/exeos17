
#include "_Precompiled.h"
#include <ExsCore/Threading/Message.h>
#include <ExsCore/Threading/Thread.h>
#include <ExsCore/Threading/ThreadLocalStorage.h>
#include <ExsCore/Threading/Transceiver.h>
#include <ExsCore/Threading/ThreadInternal.h>


namespace Exs
{


	ThreadLocalStorage* CurrentThread::GetLocalStorage()
	{
		auto tlsInfo = Internal::GetCurrentThreadLocalStorageUnchecked();
		ThreadLocalStorage* threadLocalStorage = tlsInfo.first;

		if (ThreadLocalStorage::Validate(threadLocalStorage) == TLSValidationResult::No_Error)
		{
			ExsTraceError(TRC_Core_Threading, "TLS validation has failed!");
			return nullptr;
		}
		
		return threadLocalStorage;
	}


	Result CurrentThread::ValidateState()
	{
		ThreadLocalStorage* threadLocalStorage = GetLocalStorage();

		if (threadLocalStorage == nullptr)
			return ExsResultEx(RSC_Err_Invalid_State, "TLS has not been initialized.");

		return ExsResult(RSC_Success);
	}


	ThreadHandle CurrentThread::GetHandle()
	{
		ThreadLocalStorage* threadLocalStorage = GetLocalStorage();

		if (threadLocalStorage == nullptr)
			ExsExceptionThrowEx(EXC_Invalid_Operation, "Invalid current thread state!");

		return threadLocalStorage->threadHandle;
	}


	ThreadUID CurrentThread::GetUID()
	{
		ThreadLocalStorage* threadLocalStorage = GetLocalStorage();

		if (threadLocalStorage == nullptr)
			ExsExceptionThrowEx(EXC_Invalid_Operation, "Invalid current thread state!");

		return threadLocalStorage->threadUID;
	}


	Thread* CurrentThread::GetThread()
	{
		ThreadLocalStorage* threadLocalStorage = GetLocalStorage();

		if (threadLocalStorage == nullptr)
			ExsExceptionThrowEx(EXC_Invalid_Operation, "Invalid current thread state!");

		return threadLocalStorage->thread;
	}


	void CurrentThread::SleepFor(const Milliseconds& duration)
	{
		ThreadLocalStorage* threadLocalStorage = GetLocalStorage();

		if (threadLocalStorage == nullptr)
			ExsExceptionThrowEx(EXC_Invalid_Operation, "Invalid current thread state!");

		threadLocalStorage->thread->SleepFor(duration);
	}


	void CurrentThread::Exit(Enum exitCode)
	{
		ExsExceptionThrow(EXC_Thread_Exit, exitCode);
	}


	void CurrentThread::Terminate(Enum terminateCode)
	{
		ExsExceptionThrow(EXC_Thread_Terminate, terminateCode);
	}




	CurrentThreadObject::CurrentThreadObject()
	: _tls(nullptr)
	, _handle(nullptr)
	, _uid(0)
	, _thread(nullptr)
	, _transceiver(nullptr)
	{
		this->_Initialize();
	}


	CurrentThreadObject::~CurrentThreadObject()
	{
		this->_Release();
	}


	void CurrentThreadObject::SleepFor(const Milliseconds& duration)
	{
		this->_thread->SleepFor(duration);
	}


	void CurrentThreadObject::_Initialize()
	{
		if (ThreadLocalStorage* threadLocalStorage = CurrentThread::GetLocalStorage())
		{
			this->_tls = threadLocalStorage;
			this->_handle = threadLocalStorage->threadHandle;
			this->_uid = threadLocalStorage->threadUID;
			this->_thread = threadLocalStorage->thread;
			this->_transceiver = threadLocalStorage->thread->GetTransceiver();
		}
	}


	void CurrentThreadObject::_Release()
	{
		this->_tls = nullptr;
		this->_handle = nullptr;
		this->_uid = 0;
		this->_thread = nullptr;
		this->_transceiver = nullptr;
	}


}
