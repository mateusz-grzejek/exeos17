
#pragma once

#ifndef __Exs_Core_Chrono_H__
#define __Exs_Core_Chrono_H__


namespace Exs
{

	
	///<summary>
	/// Represents duration period type, i.e. unit, in which duration is measured.
	///</summary>
	enum class DurationPeriod : Enum
	{
		Nanosecond,
		Microsecond,
		Millisecond,
		Second,
	};


	namespace Internal
	{
		
		template <bool Is_high_resolution_clock_steady>
		struct MonotonicClock;

		template <>
		struct MonotonicClock<true>
		{
			typedef std::chrono::high_resolution_clock Type;
		};

		template <>
		struct MonotonicClock<false>
		{
			typedef std::chrono::steady_clock Type;
		};

		typedef MonotonicClock<std::chrono::high_resolution_clock::is_steady>::Type MonotonicClockType;

	}


	template <DurationPeriod Duration_period>
	struct DurationTraits;

	template <>
	struct DurationTraits<DurationPeriod::Nanosecond>
	{
		typedef std::chrono::nanoseconds::period PeriodType;
	};

	template <>
	struct DurationTraits<DurationPeriod::Microsecond>
	{
		typedef std::chrono::microseconds::period PeriodType;
	};

	template <>
	struct DurationTraits<DurationPeriod::Millisecond>
	{
		typedef std::chrono::milliseconds::period PeriodType;
	};

	template <>
	struct DurationTraits<DurationPeriod::Second>
	{
		typedef std::chrono::seconds::period PeriodType;
	};
	

	///
	typedef Internal::MonotonicClockType::time_point TimePoint;

	///
	typedef Internal::MonotonicClockType::rep Duration_value_t;


	enum : Duration_value_t
	{
		// Represents infinite timeout.
		Timeout_Infinite = stdx::limits<Duration_value_t>::max_value
	};

	
	///<summary>
	/// Represents monotonic clock. Monotonic clock is internally implemented as either <c>std::high_resolution_clock</c>
	/// or <c>std::steady_clock</c>. <c>high_resolution_clock</c> is used if its <c>::is_steady</c> property evaluates
	/// to true. <c>steady_clock</c> is used otherwise.
	///</summary>
	class MonotonicClock
	{
	public:
		static TimePoint Now()
		{
			return Internal::MonotonicClockType::now();
		}
	};


	///<summary>
	///</summary>
	// template <DurationPeriod Duration_period>
	// class Duration
	// {
	// public:
	// 	typedef Duration<Duration_period> MyType;
	// 
	// 	typedef typename DurationTraits<Duration_period>::PeriodType PeriodType;
	// 	typedef std::chrono::duration<Duration_value_t, PeriodType> InternalType;
	// 
	// private:
	// 	InternalType  _duration;
	// 
	// public:
	// 	Duration()
	// 	{ }
	// 
	// 	Duration(Duration_value_t value)
	// 	: _duration(value)
	// 	{ }
	// 
	// 	template <DurationPeriod Other_period>
	// 	Duration(const Duration<Other_period>& other)
	// 	: _duration(std::chrono::duration_cast<InternalType>(other.GetDuration()))
	// 	{ }
	// 
	// 	template <typename Value_type, class Period>
	// 	Duration(const std::chrono::duration<Value_type, Period>& duration)
	// 	: _duration(std::chrono::duration_cast<InternalType>(duration))
	// 	{ }
	// 
	// 	operator InternalType&()
	// 	{
	// 		return this->_duration;
	// 	}
	// 
	// 	operator const InternalType&() const
	// 	{
	// 		return this->_duration;
	// 	}
	// 
	// 	MyType operator+() const
	// 	{
	// 		return *this;
	// 	}
	// 
	// 	MyType operator-() const
	// 	{
	// 		return MyType(-this->_duration);
	// 	}
	// 
	// 	MyType& operator++()
	// 	{
	// 		++this->_duration;
	// 		return *this;
	// 	}
	// 
	// 	MyType operator++(int)
	// 	{
	// 		return MyType(this->_duration++);
	// 	}
	// 
	// 	MyType& operator--()
	// 	{
	// 		--this->_duration;
	// 		return *this;
	// 	}
	// 
	// 	MyType operator--(int)
	// 	{
	// 		return MyType(this->_duration--);
	// 	}
	// 
	// 	MyType& operator+=(const Duration& duration)
	// 	{
	// 		this->_duration += duration._duration;
	// 		return *this;
	// 	}
	// 
	// 	MyType& operator+=(Duration_value_t value)
	// 	{
	// 		this->_duration += InternalType(value);
	// 		return *this;
	// 	}
	// 
	// 	MyType& operator-=(const Duration& duration)
	// 	{
	// 		this->_duration -= duration._duration;
	// 		return *this;
	// 	}
	// 
	// 	MyType& operator-=(Duration_value_t value)
	// 	{
	// 		this->_duration -= InternalType(value);
	// 		return *this;
	// 	}
	// 
	// 	MyType& operator*=(Duration_value_t value)
	// 	{
	// 		this->_duration *= value;
	// 		return *this;
	// 	}
	// 
	// 	MyType& operator/=(Duration_value_t value)
	// 	{
	// 		this->_duration /= value;
	// 		return *this;
	// 	}
	// 
	// 	MyType& operator%=(Duration_value_t value)
	// 	{
	// 		this->_duration /= value;
	// 		return *this;
	// 	}
	// 
	// 	Duration_value_t GetCount() const
	// 	{
	// 		return this->_duration.count();
	// 	}
	// 
	// 	const InternalType& GetDuration() const
	// 	{
	// 		return this->_duration;
	// 	}
	// };
	// 
	// 
	// template <DurationPeriod Duration_period>
	// inline Duration<Duration_period> operator+(const Duration<Duration_period>& lhs, const Duration<Duration_period>& rhs)
	// {
	// 	return Duration<Duration_period>(lhs.GetCount() + rhs.GetCount());
	// }
	// 
	// template <DurationPeriod Duration_period>
	// inline Duration<Duration_period> operator+(const Duration<Duration_period>& lhs, Duration_value_t rhs)
	// {
	// 	return Duration<Duration_period>(lhs.GetCount() + rhs);
	// }
	// 
	// template <DurationPeriod Duration_period>
	// inline Duration<Duration_period> operator-(const Duration<Duration_period>& lhs, const Duration<Duration_period>& rhs)
	// {
	// 	return Duration<Duration_period>(lhs.GetCount() - rhs.GetCount());
	// }
	// 
	// template <DurationPeriod Duration_period>
	// inline Duration<Duration_period> operator-(const Duration<Duration_period>& lhs, Duration_value_t rhs)
	// {
	// 	return Duration<Duration_period>(lhs.GetCount() - rhs);
	// }
	// 
	// template <DurationPeriod Duration_period>
	// inline Duration<Duration_period> operator*(const Duration<Duration_period>& lhs, Duration_value_t rhs)
	// {
	// 	return Duration<Duration_period>(lhs.GetCount() * rhs);
	// }
	// 
	// template <DurationPeriod Duration_period>
	// inline Duration<Duration_period> operator*(Duration_value_t lhs, const Duration<Duration_period>& rhs)
	// {
	// 	return Duration<Duration_period>(rhs.GetCount() * lhs);
	// }
	// 
	// template <DurationPeriod Duration_period>
	// inline Duration<Duration_period> operator/(const Duration<Duration_period>& lhs, Duration_value_t rhs)
	// {
	// 	return Duration<Duration_period>(lhs.GetCount() / rhs);
	// }



	template <class Period>
	using std_duration = std::chrono::duration<Duration_value_t, Period>;


	template <DurationPeriod Duration_period>
	using Duration = std::chrono::duration<Duration_value_t, typename DurationTraits<Duration_period>::PeriodType>;


	///<summary> Typedef for duration expressed in nanoseconds. </summary>
	typedef Duration<DurationPeriod::Nanosecond> Nanoseconds;
	
	///<summary> Typedef for duration expressed in microseconds. </summary>
	typedef Duration<DurationPeriod::Microsecond> Microseconds;
	
	///<summary> Typedef for duration expressed in milliseconds. </summary>
	typedef Duration<DurationPeriod::Millisecond> Milliseconds;
	
	///<summary> Typedef for duration expressed in seconds. </summary>
	typedef Duration<DurationPeriod::Second> Seconds;


	template <DurationPeriod Out, class Period>
	inline Duration<Out> DurationCast(const std_duration<Period>& duration)
	{
		typedef std::chrono::duration<Duration_value_t, typename DurationTraits<Out>::PeriodType> OutDurationType;
		return std::chrono::duration_cast<OutDurationType>(duration);
	}


	class TimeStamp
	{
	private:
		Uint64  _value;

	public:
		TimeStamp()
		: _value(0)
		{ }

		TimeStamp(Uint64 value)
		: _value(value)
		{ }

		Uint64 GetValue() const
		{
			return this->_value;
		}

		template <DurationPeriod Period>
		static Duration_value_t Convert(Uint64 value)
		{
			typedef typename DurationTraits<Period>::PeriodType RatioType;
			return _conv(value, RatioType::num, RatioType::den);
		}

	private:
		EXS_LIBAPI_CORE static Duration_value_t _conv(Uint64 value, Int rn, Int rd);
	};


	inline Uint64 operator-(const TimeStamp& lhs, const TimeStamp& rhs)
	{
		return rhs.GetValue() - lhs.GetValue();
	}


	///<summary>
	/// Computes difference between two timestamps as Duration<Perdiod> object.
	///</summary>
	template <DurationPeriod Period>
	inline Duration<Period> TimeStampDiff(const TimeStamp& first, const TimeStamp& second)
	{
		return Duration<Period>(TimeStamp::Convert<Period>(second - first));
	}

	
	///<summary>
	/// Represents performance counter - platform-specific "clock", that uses the highest resolution available on each
	/// platform. PerfCounter uses TimeStamp, which holds implementation-specific value used to represent point in time.
	///</summary>
	class EXS_LIBCLASS_CORE PerfCounter
	{
	public:
		///<summary>
		///</summary>
		static TimeStamp GetCurrentTimeStamp();
	};


}


#endif /* __Exs_Core_Chrono_H__ */
