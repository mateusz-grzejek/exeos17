
#pragma once

#ifndef __Exs_Core_TagObjects_H__
#define __Exs_Core_TagObjects_H__


namespace Exs
{

	
	///<summary>  </summary>
	struct AccessNoneTag { };
	
	///<summary>  </summary>
	struct AccessReadOnlyTag { };
	
	///<summary>  </summary>
	struct AccessWriteOnlyTag { };
	
	///<summary>  </summary>
	struct ConstQualifiedCallTag { };

	///<summary>  </summary>
	struct UninitializedTag { };


	namespace Tag
	{

		///<summary>  </summary>
		EXS_LIBOBJECT_CORE extern const AccessNoneTag accessNone;

		///<summary>  </summary>
		EXS_LIBOBJECT_CORE extern const AccessReadOnlyTag accessReadOnly;

		///<summary>  </summary>
		EXS_LIBOBJECT_CORE extern const AccessWriteOnlyTag accessWriteOnly;

		///<summary>  </summary>
		EXS_LIBOBJECT_CORE extern const ConstQualifiedCallTag constQualifiedCall;

		///<summary>  </summary>
		EXS_LIBOBJECT_CORE extern const UninitializedTag uninitialized;

	}


}


#endif /* __Exs_Core_TagObjects_H__ */
