
#pragma once

#ifndef __Exs_Core_CommonDefs_H__
#define __Exs_Core_CommonDefs_H__


#define ExsSleep(milliseconds) \
	SLEEP(milliseconds)

#define ExsCaseReturnConstantString(constant) \
	case constant: return #constant

#define ExsCommonGetAlignedValue(value, alignment) \
	((value + alignment) & (~(alignment - 1)))


namespace Exs
{


	#define ExsDeclareRefPtrClass(type) class type; typedef std::shared_ptr<type> type##RefPtr;
	#define ExsDeclareRefPtrStruct(type) struct type; typedef std::shared_ptr<type> type##RefPtr;


	typedef Uint32 U32ID;


	///<summary>
	///</summary>
	enum class ByteOrder : Enum
	{
		Big_Endian = EXS_ENDIANNESS_BE,
		Little_Endian = EXS_ENDIANNESS_LE,
		Arch_Default = EXS_ENDIANNESS,
		Arch_Swap = EXS_ENDIANNESS_SWAP,
		Unknown = 0
	};


	///<summary>
	///</summary>
	enum class CharacterEncoding : Enum
	{
		ASCII = EXS_ENCODING_ASCII,
		UTF16 = EXS_ENCODING_UTF16,
		UTF32 = EXS_ENCODING_UTF32,
		UTF8 = EXS_ENCODING_UTF8,
		Unicode = EXS_UNICODE_ENCODING,
		Wchar = EXS_WCHAR_STR_ENCODING,
		Unknown = 0
	};


	///<summary>
	///</summary>
	enum class DataUnit : Uint32
	{
		Byte = 1,
		Kilobyte = 1024,
		Megabyte = 1024 * static_cast<Uint32>(Kilobyte),
		Gigabyte = 1024 * static_cast<Uint32>(Megabyte)
	};


	///<summary>
	///</summary>
	enum class HashAlgorithm : Enum
	{
		Adler32 = 0xAC7100,
		CRC32,
		DJB2,
		FNV1A,
		MD5,
		SDBM,
		Unknown = 0
	};


	///<summary>
	///</summary>
	enum class NumericBase : Enum
	{
		Decimal,
		Hexadecimal,
		Octal
	};


	///<summary>
	///</summary>
	enum AccessModeFlags : Enum
	{
		AccessMode_None = 0,
		AccessMode_Read = 0x0001,
		AccessMode_Write = 0x0002,
		AccessMode_Full = AccessMode_Read | AccessMode_Write
	};


	enum : Uint32
	{
		//
		Invalid_U32ID = stdx::limits<Uint32>::max_value,

		// Represents unspecified 32bit ID. Certain functions allow this value to be passed if automatically generated IDs
		// should be used. Some objects may also have this ID set (which indicates, that ID is not/should not be used).
		Auto_U32ID = stdx::limits<Uint32>::max_value - 1
	};


	enum : Size_t
	{
		// Represents the largest value, that can be stored in native size type.
		Max_Size = stdx::limits<Size_t>::max_value,

		// Represents invalid length of sequence, collection, string or other measurable object. It usually means, that
		// such objects contains invalid/corrupted/forbidden value/state and thus, its length cannot be properly determined.
		//
		Invalid_Length = stdx::limits<Size_t>::max_value,

		//
		Invalid_Offset = stdx::limits<Uint>::max_value,

		// Represents invalid position in sequence, collection, string, etc. It is widely used in
		// various searching functions which return this value if requested element cannot be found.
		Invalid_Position = stdx::limits<Size_t>::max_value
	};


	///<summary>
	///</summary>
	template <DataUnit Input_unit, DataUnit Output_unit = DataUnit::Byte>
	struct UnitConverter
	{
		typedef stdx::static_ratio<Uint32, static_cast<Uint32>(Input_unit), static_cast<Uint32>(Output_unit)> RatioType;

		template <typename Result_t, typename Value_t>
		static constexpr Result_t Get(Value_t value)
		{
			return static_cast<Result_t>((static_cast<Result_t>(value) * RatioType::num) / RatioType::den);
		};
	};


	template <typename Result, typename Value>
	inline Result TruncateCast(const Value& value)
	{
		return static_cast<Result>(value);
	}

	#define pointer_cast static_cast
	#define truncate_cast TruncateCast


	///<summary>
	///</summary>
	template <typename Array_element_t, typename Index_t, Size_t Size>
	inline Array_element_t GetConstantArrayElement(const Array_element_t (&constantArray)[Size], Index_t index)
	{
		Size_t elementIndex = static_cast<Size_t>(index);
		ExsDebugAssert( elementIndex < Size );
		return constantArray[elementIndex];
	}


	///<summary>
	///</summary>
	template <class T>
	Int ByteCompare(const T& first, const T& second)
	{
		return memcmp(&first, &second, sizeof(T));
	}


	ExsForceInline Uint16 ByteSwap(Uint16 value)
	{
		return EXS_BYTESWAP16(value);
	}

	ExsForceInline Uint32 ByteSwap(Uint32 value)
	{
		return EXS_BYTESWAP32(value);
	}

	ExsForceInline Uint64 ByteSwap(Uint64 value)
	{
		return EXS_BYTESWAP64(value);
	}


	template <ByteOrder>
	struct ByteConvBase;


	template <>
	struct ByteConvBase<ByteOrder::Arch_Default>
	{
		ExsForceInline static Uint16 GetNative16(const Uint16* value)
		{
			return *value;
		}

		ExsForceInline static Uint32 GetNative32(const Uint32* value)
		{
			return *value;
		}

		ExsForceInline static Uint64 GetNative64(const Uint64* value)
		{
			return *value;
		}

		ExsForceInline static Uint16 GetNative16(const Byte* bytes)
		{
			return *(reinterpret_cast<const Uint16*>(bytes));
		}

		ExsForceInline static Uint32 GetNative32(const Byte* bytes)
		{
			return *(reinterpret_cast<const Uint16*>(bytes));
		}

		ExsForceInline static Uint64 GetNative64(const Byte* bytes)
		{
			return *(reinterpret_cast<const Uint64*>(bytes));
		}
	};


	template <>
	struct ByteConvBase<ByteOrder::Arch_Swap>
	{
		ExsForceInline static Uint16 GetNative16(const Uint16* value)
		{
			return ByteSwap(*value);
		}

		ExsForceInline static Uint32 GetNative32(const Uint32* value)
		{
			return ByteSwap(*value);
		}

		ExsForceInline static Uint64 GetNative64(const Uint64* value)
		{
			return ByteSwap(*value);
		}

		ExsForceInline static Uint16 GetNative16(const Byte* bytes)
		{
			return ByteSwap(*(reinterpret_cast<const Uint16*>(bytes)));
		}

		ExsForceInline static Uint32 GetNative32(const Byte* bytes)
		{
			return ByteSwap(*(reinterpret_cast<const Uint32*>(bytes)));
		}

		ExsForceInline static Uint64 GetNative64(const Byte* bytes)
		{
			return ByteSwap(*(reinterpret_cast<const Uint64*>(bytes)));
		}
	};


	template <typename Type, ByteOrder Byte_order>
	struct ByteConv;


	template <ByteOrder Byte_order>
	struct ByteConv<Uint16, Byte_order> : public ByteConvBase<Byte_order>
	{
		ExsForceInline static Uint16 ToBytes(Uint16 value, Byte* output)
		{
			return (*(reinterpret_cast<Uint16*>(output)) = ByteConvBase<Byte_order>::GetNative16(&value));
		}

		ExsForceInline static Uint16 ToValue(const Byte* bytes)
		{
			return ByteConvBase<Byte_order>::GetNative16(reinterpret_cast<const Uint16*>(bytes));
		}

		ExsForceInline static Uint16 ToValue(const Byte* bytes, Uint16* output)
		{
			return (*output = ByteConvBase<Byte_order>::GetNative16(reinterpret_cast<const Uint16*>(bytes)));
		}
	};


	template <ByteOrder Byte_order>
	struct ByteConv<Uint32, Byte_order> : public ByteConvBase<Byte_order>
	{
		ExsForceInline static Uint32 ToBytes(Uint32 value, Byte* output)
		{
			return (*(reinterpret_cast<Uint32*>(output)) = ByteConvBase<Byte_order>::GetNative32(&value));
		}

		ExsForceInline static Uint32 ToValue(const Byte* bytes)
		{
			return ByteConvBase<Byte_order>::GetNative32(reinterpret_cast<const Uint32*>(bytes));
		}

		ExsForceInline static Uint32 ToValue(const Byte* bytes, Uint32* output)
		{
			return (*output = ByteConvBase<Byte_order>::GetNative32(reinterpret_cast<const Uint32*>(bytes)));
		}
	};


	template <ByteOrder Byte_order>
	struct ByteConv<Uint64, Byte_order> : public ByteConvBase<Byte_order>
	{
		ExsForceInline static Uint64 ToBytes(Uint64 value, Byte* output)
		{
			return (*(reinterpret_cast<Uint64*>(output)) = ByteConvBase<Byte_order>::GetNative64(&value));
		}

		ExsForceInline static Uint64 ToValue(const Byte* bytes)
		{
			return ByteConvBase<Byte_order>::GetNative64(reinterpret_cast<const Uint64*>(bytes));
		}

		ExsForceInline static Uint64 ToValue(const Byte* bytes, Uint64* output)
		{
			return (*output = ByteConvBase<Byte_order>::GetNative64(reinterpret_cast<const Uint64*>(bytes)));
		}
	};


}


#endif /* __Exs_Core_CommonDefs_H__ */
