
#pragma once

#ifndef __Exs_Core_HashCode_H__
#define __Exs_Core_HashCode_H__


namespace Exs
{


	///<summary>
	///</summary>
	struct HashInput
	{
		const void* data;
		Size_t length;
	};


	///<summary>
	/// Returns HashInput for specified object. This function is u
	///</summary>
	template <class T>
	inline HashInput GetHashInput(const T& ref)
	{
		return HashInput { &ref, sizeof(T) };
	}

	inline HashInput GetHashInput(const char* str)
	{
		return HashInput { str, strlen(str) };
	}

	inline HashInput GetHashInput(const wchar_t* str)
	{
		return HashInput { str, wcslen(str) };
	}

	inline HashInput GetHashInput(const std::string& str)
	{
		return HashInput { str.c_str(), str.length() };
	}

	inline HashInput GetHashInput(const std::wstring& str)
	{
		return HashInput { str.c_str(), str.length() };
	}


	template <class T>
	struct DefaultHashAlgorithm
	{
		static const HashAlgorithm value = HashAlgorithm::SDBM;
	};

	template <>
	struct DefaultHashAlgorithm<const char*>
	{
		static const HashAlgorithm value = HashAlgorithm::DJB2;
	};

	template <>
	struct DefaultHashAlgorithm<const wchar_t*>
	{
		static const HashAlgorithm value = HashAlgorithm::DJB2;
	};

	template <>
	struct DefaultHashAlgorithm<std::string>
	{
		static const HashAlgorithm value = HashAlgorithm::DJB2;
	};

	template <>
	struct DefaultHashAlgorithm<std::wstring>
	{
		static const HashAlgorithm value = HashAlgorithm::DJB2;
	};


	template <Size_t Hash_size>
	struct HashTypeTraits;

	template <>
	struct HashTypeTraits<32>
	{
		typedef Uint32 ValueType;

		static const Size_t byteSize = sizeof(Uint32);
		static const Size_t size = byteSize * 8;
	};

	template <>
	struct HashTypeTraits<64>
	{
		typedef Uint64 ValueType;

		static const Size_t byteSize = sizeof(Uint64);
		static const Size_t size = byteSize * 8;
	};

	template <>
	struct HashTypeTraits<128>
	{
		typedef Uint32 ValueType[4];

		static const Size_t byteSize = sizeof(Uint32) * 4;
		static const Size_t size = byteSize * 8;
	};


	template <HashAlgorithm Hash_algo>
	struct HashCodeTraits;

	template <>
	struct HashCodeTraits<HashAlgorithm::Adler32> : public HashTypeTraits<32>
	{
		EXS_LIBAPI_CORE static Uint32 Update(Uint32 hash, const void* input, Size_t length);
		EXS_LIBAPI_CORE static Uint32 Compute(const void* input, Size_t length);
	};

	template <>
	struct HashCodeTraits<HashAlgorithm::CRC32> : public HashTypeTraits<32>
	{
		EXS_LIBAPI_CORE static Uint32 Update(Uint32 hash, const void* input, Size_t length);
		EXS_LIBAPI_CORE static Uint32 Compute(const void* input, Size_t length);
	};

	template <>
	struct HashCodeTraits<HashAlgorithm::DJB2> : public HashTypeTraits<32>
	{
		EXS_LIBAPI_CORE static Uint32 Update(Uint32 hash, const void* input, Size_t length);
		EXS_LIBAPI_CORE static Uint32 Compute(const void* input, Size_t length);
	};

	template <>
	struct HashCodeTraits<HashAlgorithm::MD5> : public HashTypeTraits<128>
	{
		EXS_LIBAPI_CORE static void Update(const Uint32* hash, const void* input, Size_t length, Uint32* output);
		EXS_LIBAPI_CORE static void Compute(const void* input, Size_t length, Uint32* output);
	};

	template <>
	struct HashCodeTraits<HashAlgorithm::FNV1A> : public HashTypeTraits<64>
	{
		EXS_LIBAPI_CORE static Uint64 Update(Uint64 hash, const void* input, Size_t length);
		EXS_LIBAPI_CORE static Uint64 Compute(const void* input, Size_t length);
	};

	template <>
	struct HashCodeTraits<HashAlgorithm::SDBM> : public HashTypeTraits<32>
	{
		EXS_LIBAPI_CORE static Uint32 Update(Uint32 hash, const void* input, Size_t length);
		EXS_LIBAPI_CORE static Uint32 Compute(const void* input, Size_t length);
	};


	template <HashAlgorithm Hash_algo>
	struct HashCode
	{
	public:
		typedef HashCode<Hash_algo> MyType;
		typedef HashCodeTraits<Hash_algo> TraitsType;

		typedef typename TraitsType::ValueType ValueType;

		static const Size_t byteSize = TraitsType::byteSize;
		static const Size_t size = TraitsType::size;

	public:
		ValueType value;

	public:
		HashCode()
		: value(0)
		{ }

		HashCode(ValueType value)
		: value(value)
		{ }

		void Swap(MyType& other)
		{
			std::swap(this->value, other.value);
		}

		bool IsEqualTo(const MyType& other) const
		{
			return this->value = other.value;
		}

		Int32 Compare(const MyType& other) const
		{
			return (this->value == other.value) ? 0 : ((this->value > other.value) ? 1 : -1);
		}
	};


	template <>
	struct HashCode<HashAlgorithm::MD5>
	{
	public:
		typedef HashCode<HashAlgorithm::MD5> MyType;
		typedef HashCodeTraits<HashAlgorithm::MD5> TraitsType;

		typedef TraitsType::ValueType ValueType;

		static const Size_t byteSize = TraitsType::byteSize;
		static const Size_t size = TraitsType::size;

	public:
		union
		{
			struct
			{
				Uint32 u1;
				Uint32 u2;
				Uint32 u3;
				Uint32 u4;
			};

			ValueType data;
		};

	public:
		HashCode()
		: u1(0)
		, u2(0)
		, u3(0)
		, u4(0)
		{ }

		HashCode(Uint32 u1, Uint32 u2, Uint32 u3, Uint32 u4)
		: u1(u1)
		, u2(u2)
		, u3(u3)
		, u4(u4)
		{ }

		void Swap(MyType& other)
		{
			std::swap(this->data, other.data);
		}

		bool IsEqualTo(const MyType& other) const
		{
			return (this->u1 == other.u1) && (this->u2 == other.u2) && (this->u3 == other.u3) && (this->u4 == other.u4);
		}

		Int32 Compare(const MyType& other) const
		{
			return memcmp(this->data, other.data, byteSize);
		}
	};


	template <HashAlgorithm Hash_algo>
	inline bool operator==(const HashCode<Hash_algo>& lhs, const HashCode<Hash_algo>& rhs)
	{
		return lhs.IsEqualTo(rhs);
	}

	template <HashAlgorithm Hash_algo>
	inline bool operator!=(const HashCode<Hash_algo>& lhs, const HashCode<Hash_algo>& rhs)
	{
		return !lhs.IsEqualTo(rhs);
	}

	template <HashAlgorithm Hash_algo>
	inline bool operator<(const HashCode<Hash_algo>& lhs, const HashCode<Hash_algo>& rhs)
	{
		return lhs.Compare(rhs) < 0;
	}

	template <HashAlgorithm Hash_algo>
	inline bool operator<=(const HashCode<Hash_algo>& lhs, const HashCode<Hash_algo>& rhs)
	{
		return lhs.Compare(rhs) <= 0;
	}

	template <HashAlgorithm Hash_algo>
	inline bool operator>(const HashCode<Hash_algo>& lhs, const HashCode<Hash_algo>& rhs)
	{
		return lhs.Compare(rhs) > 0;
	}

	template <HashAlgorithm Hash_algo>
	inline bool operator>=(const HashCode<Hash_algo>& lhs, const HashCode<Hash_algo>& rhs)
	{
		return lhs.Compare(rhs) >= 0;
	}


	template <HashAlgorithm Hash_algo>
	inline void swap(HashCode<Hash_algo>& left, HashCode<Hash_algo>& right)
	{
		left.Swap(right);
	}


	typedef HashCode<HashAlgorithm::Adler32> Adler32;
	typedef HashCode<HashAlgorithm::CRC32> CRC32;
	typedef HashCode<HashAlgorithm::DJB2> DJB2;
	typedef HashCode<HashAlgorithm::FNV1A> FNV1A;
	typedef HashCode<HashAlgorithm::MD5> MD5;
	typedef HashCode<HashAlgorithm::SDBM> SDBM;


	///<summary>
	///</summary>
	template <class T, class HashCodeType>
	struct HashCodeGen
	{
		HashCodeType operator()(const T& value) const
		{
			auto hashInput = GetHashInput(value);
			return HashCodeType(HashCodeType::TraitsType::Compute(hashInput.data, hashInput.length));
		}

		HashCodeType operator()(const HashCodeType& hash, const T& value) const
		{
			auto hashInput = GetHashInput(value);
			return HashCodeType(HashCodeType::TraitsType::Update(hash.value, hashInput.data, hashInput.length));
		}
	};

	///<summary>
	///</summary>
	template <class T>
	struct HashCodeGen<T, MD5>
	{
		MD5 operator()(const T& value) const
		{
			MD5 resultHash;
			auto hashInput = GetHashInput(value);
			MD5::TraitsType::Compute(hashInput.data, hashInput.length, resultHash.data);
			return resultHash;
		}

		MD5 operator()(const MD5& hash, const T& value) const
		{
			MD5 resultHash;
			auto hashInput = GetHashInput(value);
			MD5::TraitsType::Update(hash.data, hashInput.data, hashInput.length, resultHash.data);
			return resultHash;
		}
	};


	///<summary>
	///</summary>
	template <class HashCodeType, class T>
	inline HashCodeType GetHashCode(const T& value)
	{
		return HashCodeGen<T, HashCodeType>()(value);
	}


	///<summary>
	///</summary>
	template <class HashCodeType, class T>
	inline HashCodeType UpdateHashCode(const HashCodeType& hashCode, const T& value)
	{
		return HashCodeGen<T, HashCodeType>()(hashCode, value);
	}

	
	template <class T>
	using DefaultHashCode = HashCode<DefaultHashAlgorithm<T>::value>;


	template <class T>
	using DefaultHashCodeGen = HashCodeGen< T, DefaultHashCode<T> >;


	///<summary>
	///</summary>
	template <class T>
	inline DefaultHashCode<T> GetDefaultHashCode(const T& value)
	{
		return GetHashCode< DefaultHashCode<T> >(value);
	}



}


#endif /* __Exs_Core_HashCode_H__ */
