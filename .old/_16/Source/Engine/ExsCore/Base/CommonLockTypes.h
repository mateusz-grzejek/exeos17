
#pragma once

#ifndef __Exs_Core_CommonLockTypes_H__
#define __Exs_Core_CommonLockTypes_H__


namespace Exs
{


	/// Default lock, which is an std::mutex.
	typedef std::mutex DefaultLock;


	///<summary>
	///</summary>
	class EmptyLock
	{
		EXS_DECLARE_NONCOPYABLE(EmptyLock);

	public:
		EmptyLock()
		{ }

		void lock()
		{ }

		bool try_lock()
		{
			return true;
		}

		void unlock()
		{ }
	};


	///<summary>
	///</summary>
	class SpinLock
	{
		EXS_DECLARE_NONCOPYABLE(SpinLock);

	protected:
		std::atomic_flag  _flag;

	public:
		SpinLock()
		{ }

		void lock()
		{
			for (Uint32 yn = 0; !this->_flag.test_and_set(std::memory_order_acq_rel); ++yn)
			{
				YieldExecution(yn);
			}
		}

		bool try_lock()
		{
			return this->_flag.test_and_set(std::memory_order_acq_rel);
		}

		void unlock()
		{
			this->_flag.clear(std::memory_order_release);
		}
	};


	///<summary>
	/// Basic locking interface provider. Works with various types of mutexes,
	/// providing unified interface for gaining exclusive access to shared resources.
	///</summary>
	template <class Mutex_type>
	class MutexAdapter
	{
		EXS_DECLARE_NONCOPYABLE(MutexAdapter);

	public:
		typedef Mutex_type MutexType;
		typedef MutexAdapter<Mutex_type> MyType;

	protected:
		MutexType  _mutex;

	public:
		MutexAdapter()
		{ }

		~MutexAdapter()
		{ }

		void lock()
		{
			this->_mutex.lock();
		}

		bool try_lock()
		{
			return this->_mutex.try_lock();
		}

		void unlock()
		{
			this->_mutex.unlock();
		}
	};


	//
	typedef MutexAdapter<DefaultLock> DefaultMutex;

	//
	typedef MutexAdapter<EmptyLock> EmptyMutex;

	//
  #if ( EXS_CONFIG_BASE_USE_SPINLOCK_AS_LIGHT_MUTEX )
	typedef MutexAdapter<SpinLock> LightMutex;
  #else
	typedef MutexAdapter<DefaultLock> LightMutex;
  #endif


	///<summary>
	/// Specifies how lock can be accessed and retrieved. It influences directly what kind of public accessors
	/// are inherited from <c>Lockablle</c> base class.
	///</summary>
	enum class LockAccess : Enum
	{
		// Lock is always retrieved as non-const reference, which allows the object to be locked in any possible
		// state. Note, that this implies lock member to be declared as <c>mutable</c>.
		Relaxed,

		// The type of retrieved lock (in terms of its const-ness) matches the objects's type.
		// This makes impossible to lock an object, that was declared/retrieved as const-qualified.
		Strict
	};


	///<summary>
	/// Helper base class, that can be easily derived to inherit lock object of desired type. Accessibility
	/// can be controlled using <c>Lock_access</c> parameter.
	///</summary>
	template <class Lock_t, LockAccess Lock_access = LockAccess::Strict>
	class Lockable;


	template <class Lock_t>
	class Lockable<Lock_t, LockAccess::Strict>
	{
	public:
		typedef Lock_t LockType;
		typedef Lockable<Lock_t, LockAccess::Strict> MyType;

	protected:
		Lock_t _lock;

	public:
		Lockable()
		{ }

		Lock_t& GetLock()
		{
			return this->_lock;
		}

		const Lock_t& GetLock() const
		{
			return this->_lock;
		}
	};


	template <class Lock_t>
	class Lockable<Lock_t, LockAccess::Relaxed>
	{
	public:
		typedef Lock_t LockType;
		typedef Lockable<Lock_t, LockAccess::Relaxed> MyType;

	protected:
		mutable Lock_t _lock;

	public:
		Lockable()
		{ }

		Lock_t& GetLock() const
		{
			return this->_lock;
		}
	};


}


#define ExsEnterCriticalSection(lock) \
	{ std::unique_lock<typename std::decay<decltype(lock)>::type> csLockGuard { lock }

#define ExsEnterNamedCriticalSection(lockGuardName, lock) \
	{ std::unique_lock<typename std::decay<decltype(lock)>::type> lockGuardName { lock }

#define ExsLeaveCriticalSection() }


#endif /* __Exs_Core_CommonLockTypes_H__ */
