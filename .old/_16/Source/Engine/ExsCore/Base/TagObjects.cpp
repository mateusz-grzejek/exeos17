
#include "_Precompiled.h"
#include <ExsCore/Prerequisites.h>


namespace Exs
{


	namespace Tag
	{
		
		const AccessNoneTag accessNone = { };

		const AccessReadOnlyTag accessReadOnly = { };

		const AccessWriteOnlyTag accessWriteOnly = { };

		const ConstQualifiedCallTag constQualifiedCall = { };

		const UninitializedTag uninitialized = { };

	}


}
