
#pragma once

#ifndef __Exs_Core_BaseConfig_H__
#define __Exs_Core_BaseConfig_H__

#include <ExsConfig/Base.h>
#include <ExsConfig/Detection.h>
#include <ExsConfig/Version.h>
#include <ExsConfig/Include.h>
#include <ExsConfig/Common.h>


#if defined( max )
#  undef max
#endif

#if defined( min )
#  undef min
#endif


#define friendapi private


#define EXS_CONCAT(x,y)   x##y
#define EXS_MKSTR(x)      #x
#define EXS_CONCAT2(x,y)  EXS_CONCAT(x,y)
#define EXS_MKSTR2(x)     EXS_MKSTR(x)
#define EXS_WSTR(text)    EXS_CONCAT(L,text)
#define EXS_UNUSED(ref)   (void)(ref)


#define EXS_DECLARE_DEFAULT_COPY_CA(Type, access) \
	access: \
		Type(const Type&) = default; \
		Type& operator=(const Type&) = default;

#define EXS_DECLARE_DEFAULT_MOVE_CA(Type, access) \
	access: \
		Type(Type&&) = default; \
		Type& operator=(Type&&) = default;


#if !( EXS_STATE_PRINT_FORMAT_SPECIFIERS_DEFINED )
#
#  define EXS_STATE_PRINT_FORMAT_SPECIFIERS_DEFINED 1
#
#  define EXS_PFI32      "d"
#  define EXS_PFI32_HEX  "X"
#  define EXS_PFI32_OCT  "o"
#  define EXS_PFI64      "ll"
#  define EXS_PFI64_HEX  "llX"
#  define EXS_PFI64_OCT  "llo"
#  define EXS_PFU32      "u"
#  define EXS_PFU32_HEX  "X"
#  define EXS_PFU32_OCT  "o"
#  define EXS_PFU64      "llu"
#  define EXS_PFU64_HEX  "llX"
#  define EXS_PFU64_OCT  "llo"
#
#endif


#if ( EXS_TARGET_64 )
#
#  define EXS_PFI      EXS_PFI64
#  define EXS_PFI_HEX  EXS_PFI64_HEX
#  define EXS_PFI_OCT  EXS_PFI64_OCT
#  define EXS_PFU      EXS_PFU64
#  define EXS_PFU_HEX  EXS_PFU64_HEX
#  define EXS_PFU_OCT  EXS_PFU64_OCT
#
#else
#
#  define EXS_PFI      EXS_PFI32
#  define EXS_PFI_HEX  EXS_PFI32_HEX
#  define EXS_PFI_OCT  EXS_PFI32_OCT
#  define EXS_PFU      EXS_PFU32
#  define EXS_PFU_HEX  EXS_PFU32_HEX
#  define EXS_PFU_OCT  EXS_PFU32_OCT
#
#endif


#define EXS_NCID_RESERVED_MASK       0xFF000000
#define EXS_NCID_GET_RESERVED(ncid)  ((Enum)(ncid) & EXS_NCID_RESERVED_MASK)


typedef uint8_t   Byte;
typedef int8_t    Int8;
typedef int16_t   Int16;
typedef int32_t   Int32;
typedef int64_t   Int64;
typedef uint8_t   Uint8;
typedef uint16_t  Uint16;
typedef uint32_t  Uint32;
typedef uint64_t  Uint64;


#if ( EXS_TARGET_64 )
typedef Int64   Int;
typedef Uint64  Uint;
#else
typedef w64 Int32   Int;
typedef w64 Uint32  Uint;
#endif


#if ( EXS_CONFIG_BASE_USE_DOUBLE_PRECISION )
typedef double  Real;
#else
typedef float  Real;
#endif


//
typedef Uint Size_t;

//
typedef Int Ptrdiff_t;

//
typedef Uint32 Enum;

//
typedef Uint32 CodePoint;


#define EXS_MAKEU16B(b1, b2) \
	((((Uint16)b1 & 0xFF) << 8) | ((Uint16)b2 & 0xFF))

#define EXS_MAKEU32B(b1, b2, b3, b4) \
	((((Uint32)b1 & 0xFF) << 24) | (((Uint32)b2 & 0xFF) << 16) | (((Uint32)b3 & 0xFF) << 8) | ((Uint32)b4 & 0xFF))

#define EXS_MAKEU32S(s1, s2) \
	((((Uint32)s1 & 0xFFFF) << 16) | ((Uint32)s2 & 0xFFFF))


#define EXS_U32_HIBYTE1(u32) (Uint8)(((Uint32)u32 >> 24) & 0xFF)
#define EXS_U32_HIBYTE2(u32) (Uint8)(((Uint32)u32 >> 16) & 0xFF)
#define EXS_U32_HIBYTE3(u32) (Uint8)(((Uint32)u32 >> 8) & 0xFF)
#define EXS_U32_HIBYTE4(u32) (Uint8)((Uint32)u32 & 0xFF)

#define EXS_U32_HIWORD1(u32) (Uint16)(((Uint32)u32 >> 16) & 0xFFFF)
#define EXS_U32_HIWORD2(u32) (Uint16)((Uint32)u32 & 0xFFFF)

#define EXS_U16_HIBYTE1(u16) (Uint8)(((Uint16)u16 >> 8) & 0xFF)
#define EXS_U16_HIBYTE2(u16) (Uint8)((Uint16)u16 & 0xFF)


#if ( EXS_BUILD_SHARED_MODULES || EXS_USE_SHARED_MODULES )
#  define EXS_MODULE_EXPORT EXS_ATTR_DLL_EXPORT
#  define EXS_MODULE_IMPORT EXS_ATTR_DLL_IMPORT
#else
#  define EXS_MODULE_EXPORT
#  define EXS_MODULE_IMPORT
#endif


#if ( EXS_BUILD_MODULE_CORE )
#  define EXS_LIBAPI_CORE       EXS_MODULE_EXPORT
#  define EXS_LIBCLASS_CORE     EXS_MODULE_EXPORT
#  define EXS_LIBOBJECT_CORE    EXS_MODULE_EXPORT
#else
#  define EXS_LIBAPI_CORE       EXS_MODULE_IMPORT
#  define EXS_LIBCLASS_CORE     EXS_MODULE_IMPORT
#  define EXS_LIBOBJECT_CORE    EXS_MODULE_IMPORT
#endif


#include <ExsConfig/Extensions.h>
#include <ExsConfig/Specific.h>


#endif /* __Exs_Core_BaseConfig_H__ */
