
#pragma once

#ifndef __Exs_Core_SharedData_H__
#define __Exs_Core_SharedData_H__

#include "Prerequisites.h"


namespace Exs
{


	typedef LightMutex SharedDataLock;


	///<summary>
	///</summary>
	struct SharedData
	{
		/// Lock used to acquire unique access to the data. Every sub-type of shared data is assumed
		/// to be possibly shared in every meaning of this word, including access from multiple threads.
		SharedDataLock accessLock;
	};


	///<summary>
	///</summary>
	template <class Data_t>
	class SharedDataRefHandle
	{
	public:
		typedef Data_t DataType;
		typedef SharedDataRefHandle<Data_t> MyType;

	private:
		std::shared_ptr<Data_t>  _data;

	public:
		SharedDataRefHandle(SharedDataRefHandle&&) = default;
		SharedDataRefHandle& operator=(SharedDataRefHandle&&) = default;

		SharedDataRefHandle(const SharedDataRefHandle&) = default;
		SharedDataRefHandle& operator=(const SharedDataRefHandle&) = default;

		SharedDataRefHandle()
		{ }

		SharedDataRefHandle(std::nullptr_t)
		{ }

		explicit SharedDataRefHandle(const std::shared_ptr<Data_t>& data)
		: _data(data)
		{ }

		template <class Dt>
		SharedDataRefHandle(const SharedDataRefHandle<Dt>& handle)
		: _data(handle._data)
		{ }

		~SharedDataRefHandle()
		{ }

		SharedDataRefHandle& operator=(std::nullptr_t)
		{
			this->_data = nullptr;
			return *this;
		}

		template <class Dt>
		SharedDataRefHandle& operator=(const SharedDataRefHandle<Dt>& rhs)
		{
			if (this != &rhs)
				MyType(rhs).Swap(*this);

			return *this;
		}

		operator bool() const
		{
			return this->_data ? true : false;
		}

		Data_t* operator->()
		{
			return this->_data.get();
		}

		const Data_t* operator->() const
		{
			return this->_data.get();
		}

		Data_t& operator*()
		{
			return *(this->_data);
		}

		const Data_t& operator*() const
		{
			return *(this->_data);
		}

		Uint GetShareCount() const
		{
			return this->_data.use_count();
		}

		bool IsShared() const
		{
			return !this->_data.unique();
		}
	};


#define ExsDeclareSharedDataRefHandle(dataType) \
	struct dataType; typedef SharedDataRefHandle<dataType> dataType##RefHandle;


}


#endif /* __Exs_Core_SharedData_H__ */
