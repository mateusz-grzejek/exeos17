
#pragma once

#ifndef __Exs_Core_Exception_H__
#define __Exs_Core_Exception_H__

#include "Prerequisites.h"


#if !defined( EXS_EXCEPTION_CONFIG_BREAK_ON_THROW )
#  if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
#    define EXS_EXCEPTION_CONFIG_BREAK_ON_THROW 1
#  else
#    define EXS_EXCEPTION_CONFIG_BREAK_ON_THROW 0
#  endif
#endif


#define EXS_EXCEPTION_CODE_NCID_RESERVED  0xEF000000
#define EXS_EXCEPTION_CODE_TYPE_MASK      0x00FF0000
#define EXS_EXCEPTION_CODE_IID_MASK       0x0000FFFF


#define ExsEnumDeclareExceptionCode(type, iid) \
	(EXS_EXCEPTION_CODE_NCID_RESERVED | (((uint32_t)type & 0xFF) << 16) | (uint32_t)iid)

#define ExsEnumGetExceptionType(exceptionCode) \
	static_cast<ExceptionType>((exceptionCode & EXS_EXCEPTION_CODE_TYPE_MASK) >> 16)

#define ExsEnumValidateExceptionCode(exceptionCode) \
	EXS_NCID_GET_RESERVED(exceptionCode) == EXS_EXCEPTION_CODE_NCID_RESERVED;


namespace Exs
{

	
	///<summary>
	/// Specifies type of exception. Required for EXC code composition. They are directly reflected by actual
	/// types defined below. Every custom exception type must derive from one of these.
	///</summary>
	enum class ExceptionType : Uint8
	{
		// 
		Application = 0xE1,
		Debug = 0xE2,
		Interrupt = 0xE3,
		Logic = 0xE4,
		Runtime = 0xE5,
		System = 0xE6,
	
		Unknown			= 0,
	};


	enum : Enum
	{
		/////////////////////////// ExceptionType::Interrupt ///////////////////////////

		// Reuqest for thread interruption (exit command).
		EXC_Thread_Exit = ExsEnumDeclareExceptionCode(ExceptionType::Interrupt, 0x0001),

		// Reuqest for thread interruption (termination command).
		EXC_Thread_Terminate,
		
		/////////////////////////// ExceptionType::Logic ///////////////////////////

		// General access violation.
		EXC_Access_Violation = ExsEnumDeclareExceptionCode(ExceptionType::Logic, 0x0001),

		// Buffer overflow (amount of data greater than available capacity).
		EXC_Buffer_Overflow,

		// Invalid operation in terms of the current execution context.
		EXC_Invalid_Operation,

		// Invalid operation in terms of the current execution context.
		EXC_Invalid_Thread_Type,

		// Specified value is out of allowed range.
		EXC_Out_Of_Range,

		// Requested sequence of operations would most likely cause a deadlock.
		EXC_Possible_Deadlock,

		// Current thread has no permission to execute current code.
		EXC_Unauthorized_Thread_Access,
		
		/////////////////////////// ExceptionType::Runtime ///////////////////////////

		// General arithmetic error violation.
		EXC_Arithmetic_Error = ExsEnumDeclareExceptionCode(ExceptionType::Runtime, 0x0001),

		// Specified/assembled code point is not supported by current encoder/decoder.
		EXC_Code_Point_Out_Of_Encoding_Range,

		// dynamic_cast<> has failed.
		EXC_Dynamic_Cast_Failure,

		//
		EXC_Internal_Error,

		// Specified argument has an invalid value in this context.
		EXC_Invalid_Argument,
		
		// Specified/decoded code point is not a valid UC code point.
		EXC_Invalid_Code_Point,

		// Specified key does not exist in collection.
		EXC_Invalid_Collection_Key,

		// Byte squence is not a valid UTF sequence.
		EXC_Invalid_Encoded_Byte_Sequence,

		// State of current object is not valid. Current action will be aborted.
		EXC_Invalid_State,

		// Requested operation requires access rights which have not been granted.
		EXC_Missing_Access_Rights,

		// Non-zero size value was specified for data pointer which is null.
		EXC_Non_Zero_Null_Pointer_Size,

		// Specified pointer is a nullptr which is not allowed in this context.
		EXC_Null_Pointer,

		// Invalid state object has been encountered during thread registration.
		EXC_Thread_Registration_State_Error,

		// Specified WeakPtr is pointing to an object which has been already released.
		EXC_Weak_Ptr_Expired
	};


	struct ExceptionInfo
	{
		Enum                  code;
		const char*           name;
		const char*           description;
		SourceLocationInfo    srcInfo;
	};

	
	///<summary>
	/// Common base type for all exception types. It inherits from <c>std::exception</c> in order to allow
	/// proper handling in external code which may not be aware of this class' existence. It should never be
	/// used directly. Instead, use one of provided subtypes.
	///</summary>
	class EXS_LIBCLASS_CORE Exception : public std::exception
	{
	protected:
		ExceptionType         _type;
		const char*           _baseTypeName;
		Enum                  _code;
		std::string           _name;
		std::string           _description;
		SourceLocationInfo    _srcLocationInfo;

	public:
		Exception(ExceptionType type, const char* baseTypeName, const ExceptionInfo& info) noexcept;
		virtual ~Exception();

		virtual const char* what() const gnoexcept override;

		Enum GetCode() const;
		ExceptionType GetType() const;

		const SourceLocationInfo& GetSourceLocationInfo() const;
		const char* GetBaseTypeName() const;

		const std::string& GetName() const;
		const std::string& GetDescription() const;

		std::string ToString() const;
	};


	inline const char* Exception::what() const gnoexcept
	{
		return this->_name.data();
	}

	inline Enum Exception::GetCode() const
	{
		return this->_code;
	}

	inline ExceptionType Exception::GetType() const
	{
		return this->_type;
	}

	inline const SourceLocationInfo& Exception::GetSourceLocationInfo() const
	{
		return this->_srcLocationInfo;
	}

	inline const char* Exception::GetBaseTypeName() const
	{
		return this->_baseTypeName;
	}

	inline const std::string& Exception::GetName() const
	{
		return this->_name;
	}

	inline const std::string& Exception::GetDescription() const
	{
		return this->_description;
	}



	
	///<summary>
	/// Represents exception defined by the application. It is provided for client code and never used internally.
	///</summary>
	class ApplicationException : public Exception
	{
	public:
		ApplicationException(const ExceptionInfo& info) noexcept
		: Exception(ExceptionType::Application, "Application Exception", info)
		{ }
	};

	
	///<summary>
	/// Represents exception thrown only in debug builds. Used for interrupting not-yet-implemented sections
	/// of code (if they are critical and cannot be skipped safely) or to signal assertion failures during
	/// integrated tests.
	///</summary>
	class DebugException : public Exception
	{
	public:
		DebugException(const ExceptionInfo& info) noexcept
		: Exception(ExceptionType::Debug, "Debug Exception", info)
		{ }
	};

	
	///<summary>
	/// Specialized type of exception, used for interrupting complex code execution. For example, it is used
	/// internally to interrupt execution of thread (this type of exception is always handled properly by thread
	/// control procedure and is never considered an error).
	///</summary>
	class InterruptException : public Exception
	{
	public:
		InterruptException(const ExceptionInfo& info) noexcept
		: Exception(ExceptionType::Interrupt, "Interrupt Exception", info)
		{ }
	};

	
	///<summary>
	/// Used to signal common logic errors. This kind of errors are usually caused by wrong design and/or execution
	/// logic. In most cases, they can be simply fixed by repairing the code.
	///</summary>
	class LogicException : public Exception
	{
	public:
		LogicException(const ExceptionInfo& info) noexcept
		: Exception(ExceptionType::Logic, "Logic Exception", info)
		{ }
	};

	
	///<summary>
	/// Represents runtime exceptions, which may arise in many situations, like invalid state of an object,
	/// missing resources or wrong input data. It may also be caused by bad code, that causes data races.
	///</summary>
	class RuntimeException : public Exception
	{
	public:
		RuntimeException(const ExceptionInfo& info) noexcept
		: Exception(ExceptionType::Runtime, "Runtime Exception", info)
		{ }
	};

	
	///<summary>
	/// Used to report errors caused by native system API (e.g. Win32 or X11). This errors are usually caused
	/// by wrong usage of system calls, but in some situations they origin may lie elsewhere.
	///</summary>
	class SystemException : public Exception
	{
	public:
		SystemException(const ExceptionInfo& info) noexcept
		: Exception(ExceptionType::System, "System Exception", info)
		{ }
	};

	
	///<summary>
	/// Represents unknown type of exception.
	///</summary>
	class UnknownException : public Exception
	{
	public:
		UnknownException(const ExceptionInfo& info) noexcept
		: Exception(ExceptionType::Unknown, "Unknown", info)
		{ }
	};
	



	template <ExceptionType>
	struct ExceptionTypeByTag
	{
		typedef UnknownException Type;
	};

	template <>
	struct ExceptionTypeByTag<ExceptionType::Application>
	{
		typedef ApplicationException Type;
	};

	template <>
	struct ExceptionTypeByTag<ExceptionType::Debug>
	{
		typedef ApplicationException Type;
	};

	template <>
	struct ExceptionTypeByTag<ExceptionType::Interrupt>
	{
		typedef ApplicationException Type;
	};

	template <>
	struct ExceptionTypeByTag<ExceptionType::Logic>
	{
		typedef ApplicationException Type;
	};

	template <>
	struct ExceptionTypeByTag<ExceptionType::Runtime>
	{
		typedef ApplicationException Type;
	};

	template <>
	struct ExceptionTypeByTag<ExceptionType::System>
	{
		typedef ApplicationException Type;
	};


	template <Enum Exception_code>
	struct ExceptionTypeByCode
	{
		typedef typename ExceptionTypeByTag<ExsEnumGetExceptionType(Exception_code)>::Type Type;
	};


	#define EXS_EXCEPTION_SET_CODE_TYPE(code, type) \
		template<> struct ExceptionTypeByCode<code> { typedef type Type; };

	
	EXS_LIBAPI_CORE void DefaultExceptionHandler(const Exception& exception);
	EXS_LIBAPI_CORE void DefaultExceptionHandler(const std::exception& exception);


	namespace Internal
	{

		template <Enum Exception_code, class... Args>
		EXS_ATTR_NO_RETURN inline void Throw(const char* name, const char* description, const SourceLocationInfo& srcInfo, Args&&... args)
		{
			typedef typename ExceptionTypeByCode<Exception_code>::Type ExceptionType;
			ExceptionInfo exceptionInfo { Exception_code, name, description, srcInfo };
			ExsDebugInterrupt();
			throw ExceptionType(exceptionInfo, std::forward<Args>(args)...);
		}

	}

	
#define ExsExceptionThrow(exceptionCode, ...) \
	Internal::Throw<exceptionCode>(EXS_MKSTR(exceptionCode), nullptr, EXS_CURRENT_SOURCE_INFO, ##__VA_ARGS__)

#define ExsExceptionThrowEx(exceptionCode, description, ...) \
	Internal::Throw<exceptionCode>(EXS_MKSTR(exceptionCode), description, EXS_CURRENT_SOURCE_INFO, ##__VA_ARGS__)


#if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
#  define ExsDebugExceptionThrow(exceptionCode, ...)                ExsExceptionThrow(exceptionCode, ##__VA_ARGS__)
#  define ExsDebugExceptionThrowEx(exceptionCode, description, ...) ExsExceptionThrowEx(exceptionCode, description, ##__VA_ARGS__)
#else
#  define ExsDebugExceptionThrow(exceptionCode, ...)
#  define ExsDebugExceptionThrowEx(exceptionCode, description, ...)
#endif


}


#endif /* __Exs_Core_Exception_H__ */
