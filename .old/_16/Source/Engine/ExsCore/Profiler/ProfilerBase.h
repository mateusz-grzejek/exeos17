
#pragma once

#ifndef __Exs_Core_ProfilerBase_H__
#define __Exs_Core_ProfilerBase_H__

#include "../Prerequisites.h"


namespace Exs
{


	typedef HashCode<HashAlgorithm::FNV1A> ProfilingSampleID;
	typedef HashCode<HashAlgorithm::CRC32> ProfilingSampleLabelID;


}


#endif /* __Exs_Core_ProfilerBase_H__ */
