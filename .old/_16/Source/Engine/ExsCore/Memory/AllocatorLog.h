
#pragma once

#ifndef __Exs_Core_AllocatorLog_H__
#define __Exs_Core_AllocatorLog_H__

#include "AllocatorBase.h"
#include "MemoryAllocationsStatistics.h"
#include "Exs/Core/stdx/assoc_array.h"


namespace Exs
{


	class EXS_LIBCLASS_CORE AllocatorLog : public Lockable<LightMutex, LockAccess::Relaxed>
	{
		friend class Allocator;

	public:
		struct MemoryStateFlags
		{
			enum : Enum
			{
				Active = 0x0001,
				Released = 0x0002
			};
		};

		struct AllocatorInfo
		{
			Allocator*						allocator;
			AllocatorID						id;
			String							name;
			MemoryAllocationsStatistics		memoryAllocationsStatistics;
		};

		struct MemoryEntry
		{
			AllocatorID		allocatorID;
			Size_t			entryIndex;
			Size_t			allocEventIndex;
			Size_t			deallocEventIndex;
			Uint			memAddress;
			Size_t			memSize;
			Mask<Enum>		state;
		};

		struct MemoryAllocEvent
		{
			Size_t				memoryEntryIndex;
			SourceLocationInfo	srcLocationInfo;
		};

		struct MemoryDeallocEvent
		{
			Size_t	memoryEntryIndex;
		};

		// Holds list of active memory units (those, that were allocated and are still in use) stored as linear
		// indexes inside MemoryEntries array. They are sorted using address of allocated memory (stored as Uint).
		typedef AssocArray<Uint, Size_t> ActiveMemoryList;
		
		typedef Array<MemoryEntry> MemoryEntries;
		typedef Array<MemoryAllocEvent> MemoryAllocEvents;
		typedef Array<MemoryDeallocEvent> MemoryDeallocEvents;

		typedef AssocArray<AllocatorID, AllocatorInfo> RegisteredAllocators;

	private:
		ActiveMemoryList               _activeMemoryRefList;
		MemoryEntries                  _memoryEntries;
		MemoryAllocEvents              _memoryAllocEvents;
		MemoryDeallocEvents            _memoryDeallocEvents;
		RegisteredAllocators           _registeredAllocators;
		MemoryAllocationsStatistics    _allocationsStatistics;

	public:
		AllocatorLog();
		~AllocatorLog();

		Result RegisterAllocation(Allocator* allocator, void* memPtr, Size_t memSize, const SourceLocationInfo& srcInfo);
		Result RegisterDeallocation(Allocator* allocator, void* memPtr);

		const MemoryAllocationsStatistics& GetAllocationsStatistics() const;
		
		const ActiveMemoryList& GetActiveMemoryList() const;
		const MemoryEntries& GetMemoryEntries() const;
		const MemoryAllocEvents& GetMemoryAllocEvents() const;
		const MemoryDeallocEvents& GetMemoryDeallocEvents() const;

	private:
		Result AddAllocator(Allocator* allocator, const String& name);
	};


	inline const MemoryAllocationsStatistics& AllocatorLog::GetAllocationsStatistics() const
	{
		return this->_allocationsStatistics;
	}

	inline const AllocatorLog::ActiveMemoryList& AllocatorLog::GetActiveMemoryList() const
	{
		return this->_activeMemoryRefList;
	}

	inline const AllocatorLog::MemoryEntries& AllocatorLog::GetMemoryEntries() const
	{
		return this->_memoryEntries;
	}

	inline const AllocatorLog::MemoryAllocEvents& AllocatorLog::GetMemoryAllocEvents() const
	{
		return this->_memoryAllocEvents;
	}

	inline const AllocatorLog::MemoryDeallocEvents& AllocatorLog::GetMemoryDeallocEvents() const
	{
		return this->_memoryDeallocEvents;
	}


}


#endif /* __Exs_Core_AllocatorLog_H__ */
