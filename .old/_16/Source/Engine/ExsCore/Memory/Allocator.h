
#pragma once

#ifndef __Exs_Core_Allocator_H__
#define __Exs_Core_Allocator_H__

#include "AllocatorBase.h"


namespace Exs
{


	class AllocatorLog;
	class MemoryAllocationsStatistics;


	class EXS_LIBCLASS_CORE Allocator
	{
	protected:
		AllocatorID                     _id;
		Uint16                          _baseAlignment;
		AllocatorLog*                   _log;
		MemoryAllocationsStatistics*    _allocationsStatistics;

	public:
		Allocator(AllocatorID id, Uint16 baseAlignment);

		void EnableAllocationsLogging(AllocatorLog* log, const String& name);
		void EnableMemoryStatistics(MemoryAllocationsStatistics* statistics);

		AllocatorID GetID() const;

	protected:
		virtual void OnMemoryAlloc(void* memPtr, Size_t memSize);
		virtual void OnMemoryDealloc(void* memPtr, Size_t memSize);

		void RegisterAllocation(void* memPtr, Size_t memSize, const SourceLocationInfo& srcInfo);
		void RegisterDeallocation(void* memPtr, Size_t memSize);
	};
	

	inline void Allocator::EnableMemoryStatistics(MemoryAllocationsStatistics* statistics)
	{
		this->_allocationsStatistics = statistics;
	}
	
	inline AllocatorID Allocator::GetID() const
	{
		return this->_id;
	}


}


#endif /* __Exs_Core_Allocator_H__ */
