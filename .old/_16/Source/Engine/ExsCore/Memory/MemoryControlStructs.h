
#pragma once

#ifndef __Exs_Core_MemoryControlStructs_H__
#define __Exs_Core_MemoryControlStructs_H__

#include "MemoryBase.h"


namespace Exs
{


	template <class Header_type, Size_t User_memory_size>
	struct MemoryUnit
	{
		typedef Header_type HeaderType;
		
		static const Size_t alignment = 0;
		static const Size_t userMemorySize = User_memory_size;
		static const Size_t headerSize = sizeof(Header_type);

		Header_type  header;
		Byte         memory[userMemorySize];
	};


	template <class Header_type, Size_t User_memory_size, Uint16 Alignment = Memory_Base_Alignment>
	struct AlignedMemoryUnit
	{
		typedef Header_type HeaderType;
		
		static const Size_t alignment = Alignment;
		static const Size_t userMemorySize = User_memory_size;
		static const Size_t allocSize = ExsCommonGetAlignedValue(userMemorySize, alignment);
		static const Size_t headerSize = ExsCommonGetAlignedValue(sizeof(Header_type), alignment);

		union
		{
			Header_type  header;
			Byte         headerMemory[headerSize];
		};
		
		Byte  memory[userMemorySize];
	};


}


#endif /* __Exs_Core_MemoryControlStructs_H__ */
