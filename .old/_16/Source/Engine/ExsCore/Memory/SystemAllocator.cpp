
#include "_Precompiled.h"
#include <ExsCore/Memory/SystemAllocator.h>


namespace Exs
{


	struct SystemMemoryHeader : public MemoryHeaderBase
	{
		MemoryAlignmentInfo alignmentInfo;
		stdx::mask<Enum> flags;
		Size_t memorySize;
	};


	enum : Size_t
	{
		System_Memory_Header_Size = sizeof(SystemMemoryHeader)
	};


	void* SystemAllocator::AllocAligned(Size_t memSize, Uint16 alignment)
	{
		void* memory = std::malloc(System_Memory_Header_Size + memSize + alignment);
		Uint userAddress = reinterpret_cast<Uint>(memory) + System_Memory_Header_Size;
		
		Uint alignedAddress = 0;
		Uint16 alignOffset = 0;

		AlignMemoryAddress(userAddress, alignment, &alignedAddress, &alignOffset);
		SystemMemoryHeader* header = reinterpret_cast<SystemMemoryHeader*>(alignedAddress - System_Memory_Header_Size);

		header->flags = MRF_Aligned;
		header->memorySize = memSize;
		header->alignmentInfo.alignment = alignment;
		header->alignmentInfo.offset = alignOffset;

		return header + 1; //Right after the header starts our memory
	}


	void* SystemAllocator::Alloc(Size_t memSize)
	{
		size_t sz = System_Memory_Header_Size + memSize;
		void* memory = std::malloc(sz);
		Uint address = reinterpret_cast<Uint>(memory);

		Uint userSpaceAddress = address + System_Memory_Header_Size;
		SystemMemoryHeader* header = reinterpret_cast<SystemMemoryHeader*>(address);

		header->flags = 0;
		header->memorySize = memSize;

		return reinterpret_cast<void*>(userSpaceAddress);
	}


	void SystemAllocator::Free(void* memPtr)
	{
		if (memPtr != nullptr)
		{
			Uint baseAddress = reinterpret_cast<Uint>(memPtr) - System_Memory_Header_Size;
			SystemMemoryHeader* header = reinterpret_cast<SystemMemoryHeader*>(baseAddress);

			if (header->flags.is_set(MRF_Aligned))
				baseAddress -= header->alignmentInfo.offset;

			std::free(reinterpret_cast<void*>(baseAddress));
		}
	}


	void* SystemAllocator::Realloc(void* memPtr, Size_t newSize)
	{
		Size_t oldSize = 0;
		void* oldMemory = nullptr;
		void* newMemory = nullptr;

		Uint16 alignment = 0;

		if (memPtr != nullptr)
		{
			Uint baseAddress = reinterpret_cast<Uint>(memPtr) - System_Memory_Header_Size;
			SystemMemoryHeader* header = reinterpret_cast<SystemMemoryHeader*>(baseAddress);

			if (header->flags.is_set(MRF_Aligned))
			{
				alignment = header->alignmentInfo.alignment;
				baseAddress -= header->alignmentInfo.offset;
			}

			oldSize = header->memorySize;
			oldMemory = reinterpret_cast<void*>(baseAddress);
		}

		if (newSize > 0)
		{
			if (alignment > 0)
				newMemory = AllocAligned(newSize, alignment);
			else
				newMemory = Alloc(newSize);
		}

		if (newMemory && memPtr)
			ExsMoveMemory(newMemory, memPtr, stdx::get_min_of(oldSize, newSize));

		if (oldMemory)
			std::free(oldMemory);

		return newMemory;
	}


	void* SystemAllocator::AllocAligned(Size_t memSize, Uint16 alignment, const SourceLocationInfo& srcInfo)
	{
		EXS_UNUSED( srcInfo );
		return AllocAligned(memSize, alignment);
	}


	void* SystemAllocator::Alloc(Size_t memSize, const SourceLocationInfo& srcInfo)
	{
		EXS_UNUSED( srcInfo );
		return Alloc(memSize);
	}


	void SystemAllocator::Free(void* memPtr, const SourceLocationInfo& srcInfo)
	{
		EXS_UNUSED( srcInfo );
		Free(memPtr);
	}


	void* SystemAllocator::Realloc(void* memPtr, Size_t newSize, const SourceLocationInfo& srcInfo)
	{
		EXS_UNUSED( srcInfo );
		return Realloc(memPtr, newSize);
	}


}
