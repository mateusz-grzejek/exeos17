
#include "_Precompiled.h"

#if 0

#include <ExsCore/Memory/Allocator.h>
#include <ExsCore/Memory/AllocatorLog.h>


namespace Exs
{


	Allocator::Allocator(AllocatorID id, Uint16 baseAlignment)
	: _id((id != Invalid_Allocator_ID) ? id : reinterpret_cast<Uint>(this))
	, _baseAlignment(baseAlignment)
	, _log(nullptr)
	, _allocationsStatistics(nullptr)
	{ }


	void Allocator::EnableAllocationsLogging(AllocatorLog* log, const String& name)
	{
		if (this->_log != nullptr)
		{
			ExsDebugInterrupt();
			return;
		}

		this->_log = log;
		this->_log->AddAllocator(this, name);
	}


	void Allocator::OnMemoryAlloc(void* memPtr, Size_t memSize)
	{ }


	void Allocator::OnMemoryDealloc(void* memPtr, Size_t memSize)
	{ }

	
	void Allocator::RegisterAllocation(void* memPtr, Size_t memSize, const SourceLocationInfo& srcInfo)
	{
		if (this->_log != nullptr)
		{
			this->_log->RegisterAllocation(this, memPtr, memSize, srcInfo);
		}
		else if (this->_allocationsStatistics != nullptr)
		{
			this->_allocationsStatistics->RegisterAllocation(memSize);
		}

		this->OnMemoryAlloc(memPtr, memSize);
	}


	void Allocator::RegisterDeallocation(void* memPtr, Size_t memSize)
	{
		this->OnMemoryAlloc(memPtr, memSize);
		
		if (this->_log)
		{
			this->_log->RegisterDeallocation(this, memPtr);
		}
		else if (this->_allocationsStatistics != nullptr)
		{
			this->_allocationsStatistics->RegisterDeallocation(memSize);
		}
	}


}

#endif
