
#include "_Precompiled.h"
#include <ExsCore/Memory/InternalAllocator.h>
#include <ExsCore/Memory/SystemAllocator.h>


namespace Exs
{


	void* InternalAllocator::AllocAligned(Size_t memSize, Uint16 alignment)
	{
		return SystemAllocator::AllocAligned(memSize, alignment);
	}


	void* InternalAllocator::Alloc(Size_t memSize)
	{
		return SystemAllocator::Alloc(memSize);
	}


	void InternalAllocator::Free(void* memPtr)
	{
		SystemAllocator::Free(memPtr);
	}


	void* InternalAllocator::Realloc(void* memPtr, Size_t newSize)
	{
		return SystemAllocator::Realloc(memPtr, newSize);
	}


	void* InternalAllocator::AllocAligned(Size_t memSize, Uint16 alignment, const SourceLocationInfo& srcInfo)
	{
		EXS_UNUSED( srcInfo );
		return AllocAligned(memSize, alignment);
	}


	void* InternalAllocator::Alloc(Size_t memSize, const SourceLocationInfo& srcInfo)
	{
		EXS_UNUSED( srcInfo );
		return Alloc(memSize);
	}


	void InternalAllocator::Free(void* memPtr, const SourceLocationInfo& srcInfo)
	{
		EXS_UNUSED( srcInfo );
		Free(memPtr);
	}


	void* InternalAllocator::Realloc(void* memPtr, Size_t newSize, const SourceLocationInfo& srcInfo)
	{
		EXS_UNUSED( srcInfo );
		return Realloc(memPtr, newSize);
	}


}
