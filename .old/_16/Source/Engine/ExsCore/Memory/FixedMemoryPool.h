
#pragma once

#ifndef __Exs_Core_FixedMemoryPool_H__
#define __Exs_Core_FixedMemoryPool_H__

#include "AllocatorBase.h"
#include "MemoryControlStructs.h"


namespace Exs
{


	enum : Size_t
	{
		Memory_Pool_Capacity_Default = 0
	};


	template <Size_t Alloc_size>
	struct FixedPoolDefaultCapacity
	{
		static const Size_t defaultSizeBytes = UnitConverter<DataUnit::Megabyte, DataUnit::Byte, Size_t>::Convert(1);
		static const Size_t defaultSizeUnits = Alloc_size * (1 << 12);

		static const Size_t Value = defaultSizeBytes <= defaultSizeUnits ? defaultSizeBytes : Default_size_units;
	};


	template <Size_t Alloc_size, Size_t Total_capacity>
	struct FixedPoolCapacity
	{
		static const Size_t Value =
			(Total_capacity == Memory_Pool_Capacity_Default) ? FixedPoolDefaultCapacity<Alloc_size>::value : Total_capacity;
	};


	template <	Size_t Alloc_size,
				Size_t Alignment = Memory_Base_Alignment,
				Size_t Total_capacity = Memory_Pool_Capacity_Default,
				AllocationSync Alloc_sync = AllocationSync::Default>
	class FixedMemoryPool
	{
	public:
		typedef FixedMemoryPool<Alloc_size, Alignment, Total_capacity, Alloc_sync> MyType;
		typedef typename AllocationLock<Alloc_sync>::Type LockType;

		struct MemoryHeader : public MemoryHeaderBase
		{
			Uint16	allocState;
			Uint16	internalValue;
		};

		typedef AlignedMemoryUnit<MemoryHeader, Alloc_size, Alignment> MemoryUnit;
		
		static const Size_t alignment = MemoryUnit::Alignment;
		static const Size_t allocSize = MemoryUnit::Alloc_size;
		static const Size_t headerSize = MemoryUnit::Header_size;

		static const Size_t totalCapacity = FixedPoolCapacity<allocSize, Total_capacity>::value;
		static const Size_t blockLength = totalCapacity / allocSize;
		
		// We do not want to spare more space than necessery - use the smallest type, that can hold all possible
		// index values (we know in advance how many blocks will be stored inside this pool).
		typedef typename GetTypeRequiredForValue<blockLength>::Type IndexType;
		
		static const Size_t unitSize = sizeof(MemoryUnit);
		static const Size_t indexTypeSize = sizeof(IndexType);
		
		static const Size_t memoryDataSize = unitSize * blockLength;
		static const Size_t memoryIndexSize = indexTypeSize * blockLength;

	private:
		Byte*                _memory;
		MemoryUnit*          _memoryUnits;
		IndexType*           _memoryIndex;
		Uint16               _memoryUnitsAlignmentOffset;
		Uint16               _memoryIndexAlignmentOffset;
		std::atomic<IndexType>    _allocRefIndex;
		LockType             _lock;

	public:
		FixedMemoryPool()
		: _memory(nullptr)
		, _memoryUnits(nullptr)
		, _memoryIndex(nullptr)
		, _memoryUnitsAlignmentOffset(0)
		, _memoryIndexAlignmentOffset(0)
		, _allocRefIndex(0)
		{ }

		Result Initialize()
		{
			ExsDebugAssert( this->IsInitialized() );

			void* memoryPtr = SystemMalloc(memoryDataSize + Memory_Base_Alignment);
			void* indexPtr = SystemMalloc(memoryIndexSize + Memory_Base_Alignment);
			
			Uint memoryAddress = reinterpret_cast<Uint>(memoryPtr);
			Uint index_address = reinterpret_cast<Uint>(indexPtr);
			
			Uint alignedMemory = 0;
			Uint alignedIndex = 0;
			Uint16 memoryUnitsAlignmentOffset = 0;
			Uint16 memoryIndexAlignmentOffset = 0;
			
			AlignMemoryAddress(memoryAddress, Memory_Base_Alignment, &alignedMemory, &memoryUnitsAlignmentOffset);
			AlignMemoryAddress(index_address, Memory_Base_Alignment, &alignedIndex, &memoryIndexAlignmentOffset);
			
			this->_memory = reinterpret_cast<Byte*>(alignedMemory);
			this->_memoryUnits = reinterpret_cast<MemoryUnit*>(alignedMemory);
			this->_memoryIndex = reinterpret_cast<IndexType*>(alignedIndex);
			
			this->_memoryUnitsAlignmentOffset = memoryUnitsAlignmentOffset;
			this->_memoryIndexAlignmentOffset = memoryIndexAlignmentOffset;
			
			ExsZeroMemory(this->_memory, memoryIndexSize);

			for (IndexType n = 0; n < Block_length; n += 4)
			{
				this->_memoryIndex[n] = n;
				this->_memoryIndex[n+1] = n + 1;
				this->_memoryIndex[n+2] = n + 2;
				this->_memoryIndex[n+3] = n + 3;
			}

			return ExsResult(RSC_Success);
		}

		void Release()
		{
			if (!this->IsInitialized())
				return;
			
			Uint memoryUnitsAddress = reinterpret_cast<Uint>(this->_memory);
			Uint memoryIndexAddress = reinterpret_cast<Uint>(this->_memoryIndex);

			if (this->_memoryUnitsAlignmentOffset > 0)
				memoryUnitsAddress -= this->_memoryUnitsAlignmentOffset;

			if (this->_memoryIndexAlignmentOffset > 0)
				memoryIndexAddress -= this->_memoryIndexAlignmentOffset;
			
			SystemFree(reinterpret_cast<void*>(memoryUnitsAddress));
			SystemFree(reinterpret_cast<void*>(memoryIndexAddress));
			
			this->_memory = nullptr;
			this->_memoryUnits = nullptr;
			this->_memoryIndex = nullptr;
			
			this->_memoryUnitsAlignmentOffset = 0;
			this->_memoryIndexAlignmentOffset = 0;
		}

		MemoryUnit* Allocate()
		{
			ExsDebugAssert( this->IsInitialized() );
			
			IndexType memoryUnitIndex = stdx::limits<IndexType>::max_value;

			ExsEnterCriticalSection(this->_lock);
			{
				IndexType refIndex = this->_allocRefIndex.load(std::memory_order_relaxed);

				if (refIndex == Block_length)
					return nullptr;

				memoryUnitIndex = this->_memoryIndex[refIndex];
				this->_allocRefIndex.fetch_add(1, std::memory_order_relaxed);
			}
			ExsLeaveCriticalSection();

			MemoryUnit* memory_unit = &(this->_memoryUnits[memoryUnitIndex]);

			if (memory_unit->header.allocState != 0)
			{
				ExsDebugInterrupt();
				return nullptr;
			}

			memory_unit->header.allocState = 1;

			return memory_unit;
		}

		void Deallocate(MemoryUnit* memUnit)
		{
			ExsDebugAssert( this->IsInitialized() );
			
			Ptrdiff_t memoryDiff = memUnit - this->_memoryUnits;
			IndexType memoryIndex = static_cast<IndexType>(memoryDiff);

			if ((memoryDiff < 0) || (memoryDiff >= blockLength))
			{
				ExsDebugInterrupt();
				return;
			}

			if (memUnit->header.allocState == 0)
			{
				ExsDebugInterrupt();
				return;
			}

			memUnit->header.allocState = 0;

			ExsEnterCriticalSection(this->_lock);
			{
				IndexType refIndex = this->_allocRefIndex.load(std::memory_order_relaxed);

				if (refIndex == 0)
				{
					ExsDebugInterrupt();
					return;
				}

				this->_memoryIndex[refIndex - 1] = memoryIndex;
				this->_allocRefIndex.fetch_sub(1, std::memory_order_relaxed);
			}
			ExsLeaveCriticalSection();
		}

		bool IsInitialized() const
		{
			return this->_memory != nullptr;
		}
	};


}


#endif /* __Exs_Core_FixedMemoryPool_H__ */
