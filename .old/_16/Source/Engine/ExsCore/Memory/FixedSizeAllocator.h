
#pragma once

#ifndef __Exs_Core_FixedSizeAllocator_H__
#define __Exs_Core_FixedSizeAllocator_H__

#include "Allocator.h"
#include "FixedMemoryPool.h"


namespace Exs
{


	namespace Config
	{

		enum : Size_t
		{
			MMR_Fixed_Allocator_Default_Pools_Limit = 16
		};

	}


	template <	Size_t Alloc_unit_size,
				Size_t Single_pool_size,
				Size_t Max_pools_num = Config::MMR_Fixed_Allocator_Default_Pools_Limit,
				AllocationSync Alloc_sync = AllocationSync::Default>
	class FixedSizeAllocator : public Allocator
	{
	public:
		typedef FixedSizeAllocator<Alloc_unit_size, Single_pool_size, Max_pools_num, Alloc_sync> MyType;
		typedef FixedMemoryPool<Alloc_unit_size, Memory_Base_Alignment, Single_pool_size, Alloc_sync> PoolType;
		
		typedef typename PoolType::MemoryUnit MemoryUnit;
		typedef typename PoolType::MemoryHeader MemoryHeader;
		typedef typename AllocationLock<Alloc_sync>::Type LockType;

		typedef decltype(MemoryHeader::internal_value) PoolIndexType;

		typedef Array<PoolType*> MemoryPools;
		
		static const Size_t allocSize = PoolType::Alloc_size;
		static const Size_t headerSize = PoolType::Header_size;
		static const Size_t unitSize = PoolType::Unit_size;
		
		static const Size_t poolSize = Single_pool_size;

		static const PoolIndexType poolsLimit = static_cast<PoolIndexType>(Max_pools_num);

	private:
		MemoryPools    _memoryPools;
		LockType       _lock;

	public:
		FixedSizeAllocator()
		: Allocator(Invalid_ID, Memory_Base_Alignment)
		{
			this->Release();
		}

		void Release()
		{
			this->_ReleasePools();
		}

		void* Allocate(Size_t memSize)
		{
			if (memSize != Alloc_unit_size)
			{
				ExsDebugInterrupt();
				return nullptr;
			}

			PoolIndexType poolIndex = 0;
			MemoryUnit* memoryUnit = nullptr;

			while (true)
			{
				PoolType* memoryPool = this->_GetNextPool(poolIndex);
				memoryUnit = memoryPool->Allocate();

				if (memoryUnit != nullptr)
					break;

				++poolIndex;

				if (poolIndex == Pools_limit)
					return nullptr;
			}

			void* userMemoryPtr = memoryUnit->memory;
			memoryUnit->header.internal_value = poolIndex;

			this->RegisterAllocation(userMemoryPtr, Alloc_unit_size, SourceLocationInfo());

			return userMemoryPtr;
		}

		void Deallocate(void* memPtr)
		{
			Uint memoryAddress = reinterpret_cast<Uint>(memPtr);
			MemoryUnit* memoryUnit = reinterpret_cast<MemoryUnit*>(memoryAddress - headerSize);

			PoolIndexType poolIndex = memoryUnit->header.internal_value;
			PoolType* memoryPool = this->_RetrievePool(poolIndex);

			if (memoryPool == nullptr)
			{
				ExsDebugInterrupt();
				return;
			}
			
			void* userMemoryPtr = memoryUnit->memory;
			memoryUnit->header.internal_value = poolIndex;

			this->RegisterDeallocation(userMemoryPtr, Alloc_unit_size);
			memoryPool->Deallocate(memoryUnit);
		}

	private:
		PoolType* _RetrievePool(PoolIndexType poolIndex)
		{
			PoolType* memoryPool = nullptr;

			ExsEnterCriticalSection(this->_lock);
			{
				Size_t poolsNum = this->_memoryPools.Size();

				if (poolIndex < poolsNum)
					memoryPool = this->_memoryPools[poolIndex];
			}
			ExsLeaveCriticalSection();

			return memoryPool;
		}

		PoolType* _GetNextPool(PoolIndexType currentIndex)
		{
			PoolType* memoryPool = nullptr;

			ExsEnterCriticalSection(this->_lock);
			{
				Size_t poolsNum = this->_memoryPools.Size();

				if ((currentIndex < poolsNum) || this->_AddNewPool())
					memoryPool = this->_memoryPools[currentIndex];
			}
			ExsLeaveCriticalSection();

			return memoryPool;
		}

		bool _AddNewPool()
		{
			Size_t currentPoolsNum = this->_memoryPools.Size();
			ExsDebugAssert( currentPoolsNum < Pools_limit );

			UniquePtr<PoolType> memoryPoolRefPtr { new PoolType() };

			Result initResult = memoryPoolRefPtr->Initialize();

			if (initResult != RSC_Success)
				return false;

			this->_memoryPools.PushBack(memoryPoolRefPtr.GetPtr());
			memoryPoolRefPtr.Clear();

			return true;
		}

		void _ReleasePools()
		{
			//printf("--> Releasing pools (%llu).\n", this->_memoryPools.Size());
			for (auto& memoryPool : this->_memoryPools)
			{
				//printf("\t Pool 0x%llX released.\n", (Uint)memoryPool);
				memoryPool->Release();
				delete memoryPool;
				memoryPool = nullptr;
			}

			this->_memoryPools.Clear();
		}
	};


}


#endif /* __Exs_Core_FixedSizeAllocator_H__ */
