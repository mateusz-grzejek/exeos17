
#include "_Precompiled.h"
#include <ExsCore/Memory/InternalAllocator.h>
#include <ExsCore/Memory/SystemAllocator.h>


namespace Exs
{


	namespace Internal
	{


		void* internalAlignedMalloc(Size_t memSize, Uint16 alignment)
		{
			return InternalAllocator::AllocAligned(memSize, alignment);
		}

		void* internalMalloc(Size_t memSize)
		{
			return InternalAllocator::Alloc(memSize);
		}

		void* internalRealloc(void* memPtr, Size_t newSize)
		{
			return InternalAllocator::Realloc(memPtr, newSize);
		}

		void internalFree(void* memPtr)
		{
			InternalAllocator::Free(memPtr);
		}

		void* internalAlignedMalloc(Size_t memSize, Uint16 alignment, const SourceLocationInfo& srcInfo)
		{
			return InternalAllocator::AllocAligned(memSize, alignment, srcInfo);
		}

		void* internalMalloc(Size_t memSize, const SourceLocationInfo& srcInfo)
		{
			return InternalAllocator::Alloc(memSize, srcInfo);
		}

		void* internalRealloc(void* memPtr, Size_t newSize, const SourceLocationInfo& srcInfo)
		{
			return InternalAllocator::Realloc(memPtr, newSize, srcInfo);
		}

		void internalFree(void* memPtr, const SourceLocationInfo& srcInfo)
		{
			InternalAllocator::Free(memPtr, srcInfo);
		}

		
		void* systemAlignedMalloc(Size_t memSize, Uint16 alignment)
		{
			return SystemAllocator::AllocAligned(memSize, alignment);
		}

		void* systemMalloc(Size_t memSize)
		{
			return SystemAllocator::Alloc(memSize);
		}

		void* systemRealloc(void* memPtr, Size_t newSize)
		{
			return SystemAllocator::Realloc(memPtr, newSize);
		}

		void systemFree(void* memPtr)
		{
			SystemAllocator::Free(memPtr);
		}

		void* systemAlignedMalloc(Size_t memSize, Uint16 alignment, const SourceLocationInfo& srcInfo)
		{
			return SystemAllocator::AllocAligned(memSize, alignment, srcInfo);
		}
	
		void* systemMalloc(Size_t memSize, const SourceLocationInfo& srcInfo)
		{
			return SystemAllocator::Alloc(memSize, srcInfo);
		}

		void* systemRealloc(void* memPtr, Size_t newSize, const SourceLocationInfo& srcInfo)
		{
			return SystemAllocator::Realloc(memPtr, newSize, srcInfo);
		}

		void systemFree(void* memPtr, const SourceLocationInfo& srcInfo)
		{
			SystemAllocator::Free(memPtr, srcInfo);
		}


	}


}
