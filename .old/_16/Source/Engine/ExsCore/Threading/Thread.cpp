
#include "_Precompiled.h"
#include <ExsCore/CoreEngineState.h>
#include <ExsCore/Threading/Thread.h>
#include <ExsCore/Threading/SystemThread.h>
#include <ExsCore/Threading/ThreadAccessGuards.h>
#include <ExsCore/Threading/ThreadLocalStorage.h>
#include <ExsCore/Threading/ThreadSystemController.h>
#include <ExsCore/Threading/Transceiver.h>
#include <ExsCore/Threading/ThreadInternal.h>
#include <stdx/string_utils.h>


namespace Exs
{


	Thread::Thread(CoreEngineStateRefHandle coreEngineState, ThreadBaseInfo* threadBaseInfo, const std::string& name)
	: _baseInfo(threadBaseInfo)
	, _coreEngineState(coreEngineState)
	, _threadSystemController(coreEngineState->threadSystemController)
	, _uid(threadBaseInfo->threadUID)
	, _handle(threadBaseInfo->threadHandle)
	, _name(name)
	, _systemThread(nullptr)
	, _localStorage(nullptr)
	, _internalState(State_Default)
	, _executionState(ThreadExecutionState::Unknown)
	, _waitState(nullptr)
	{ }


	Thread::~Thread()
	{ }


	ThreadExecutionState Thread::WaitForCompletion()
	{
		ThreadExecutionState execState = this->GetExecutionState();
		if (ExsEnumGetThreadExecutionStateIsActive(execState) && !ExsEnumGetThreadExecutionStateIsFinished(execState))
		{
			auto& executionStateLock = this->_executionStateSync.GetLock();
			ExsEnterCriticalSection(executionStateLock);
			{
				execState = this->GetExecutionState();
				if (!ExsEnumGetThreadExecutionStateIsFinished(execState))
				{
					this->_executionStateSync.Wait([&]() -> bool {
						execState = this->GetExecutionState();
						return ExsEnumGetThreadExecutionStateIsFinished(execState);
					});
				}
			}
			ExsLeaveCriticalSection();
		}

		return execState;
	}


	Transceiver* Thread::GetTransceiver()
	{
		return nullptr;
	}


	const char* Thread::GetTextID() const
	{
		return this->_name.c_str();
	}


	void Thread::SetRunPermission()
	{
		if (this->CheckRunPermissionFlag())
		{
			ExsDebugInterrupt();
			return;
		}

		auto& executionStateLock = this->_executionStateSync.GetLock();
		ExsEnterCriticalSection(executionStateLock);
		{
			ExsTraceInfo(TRC_Core_Threading, "Setting run permission for 0x%p...", this);
			this->_internalState.set(State_Run_Permission);
			this->_executionStateSync.NotifyOne();
		}
		ExsLeaveCriticalSection();
	}


	void Thread::SuspendThreadExecution()
	{
		this->_internalState.set(State_Suspend_Request, std::memory_order_release);
	}
	

	void Thread::ResumeThreadExecution()
	{
		if (this->CheckSuspendRequestFlag())
		{
			auto& executionStateLock = this->_executionStateSync.GetLock();
			ExsEnterCriticalSection(executionStateLock);
			{
				if (this->CheckSuspendRequestFlag())
				{
					this->_internalState.unset(State_Suspend_Request, std::memory_order_release);
					this->_executionStateSync.NotifyOne();
				}
			}
			ExsLeaveCriticalSection();
		}
	}


	void Thread::WaitForReadyState(const ThreadLaunchInfo& threadLaunchInfo)
	{
		if (threadLaunchInfo.readyStateSyncObject == nullptr)
		{
			ExsTraceWarning(TRC_Core_Threading, "");
			return;
		}

		ExsTraceInfo(TRC_Core_Threading, "Waiting for ready state of thread 0x%X.", reinterpret_cast<Uint>(this));

		if (!this->IsReady())
		{
			auto& readyStateLock = threadLaunchInfo.readyStateSyncObject->GetLock();
			ExsEnterCriticalSection(readyStateLock);
			{
				if (!this->IsReady())
				{
					threadLaunchInfo.readyStateSyncObject->Wait([this]() -> bool {
						return this->IsReady();
					});
				}
			}
			ExsLeaveCriticalSection();
		}

		ExsTraceInfo(TRC_Core_Threading, "Done. Ready state -> [%s].", this->IsReady() ? "true" : "false");
	}


	bool Thread::Exit()
	{
		// Fetch UID of the current thread.
		ThreadUID currentThreadUID = CurrentThread::GetUID();

		if (currentThreadUID == this->_uid)
		{
			if (!this->CheckRunPermissionFlag())
			{
				// Execution context without RPF means internal implementation (or overridable OnXXX functions).
				// These cannot use Exit(), as this is the top-level functionality, that assumes, that thread
				// is already fully initialized and in one of its execution phases.
				ExsExceptionThrowEx(EXC_Invalid_Operation, "Do not use Exit() internally!");
			}
		}

		if (this->_internalState.test_and_set(State_Exit))
		{
			if (currentThreadUID != this->_uid)
			{
				// Abort current wait - thread may be waiting with a high timeout set.
				// TODO: Perhaps some priority of Exit()? Delay? Abort policy?
				this->AbortCurrentWait();

				if (!this->CheckRunPermissionFlag())
				{
					ExsTraceInfo(TRC_Core_Threading, "Thread 0x%p was in pre-TSA phase. Setting RPF to force progress...", this);
					this->SetRunPermission();
				}
			}

			return true;
		}

		return false;
	}


	void Thread::AbortCurrentWait()
	{
		if (ThreadWaitState* myWaitState = this->GetWaitState())
		{
			myWaitState->waitObject->Interrupt(this);
		}
	}


	ThreadBaseInfo* Thread::Register(ThreadSystemController* threadSystemController, ThreadType threadType)
	{
		ThreadRegInfo threadRegInfo { };
		threadRegInfo.threadType = threadType;

		ThreadBaseInfo* threadBaseInfo = threadSystemController->RegisterThread(threadRegInfo);

		return threadBaseInfo;
	}


	Result Thread::Start(Thread* thread, const ThreadLaunchInfo& launchInfo)
	{
		ExsDebugAssert( thread->_systemThread == nullptr );

		SystemThreadInterface systemThreadInterface { };
		systemThreadInterface.onBegin = std::bind(_OnSystemThreadBeginProc, thread);
		systemThreadInterface.onEnd = std::bind(_OnSystemThreadEndProc, thread);
		systemThreadInterface.onExecute = std::bind(_ExecutionControlProc, thread, launchInfo.readyStateSyncObject);
		systemThreadInterface.onRelease = std::bind(_ReleaseProc, thread);

		// Create system thread, which wraps os-specific thread creation/management routines. Threads are launched as
		// detached threads, because from our perspective, they are equivalently functional, and since we perform our own
		// synchronization method, we don't need joining.

		thread->_systemThread = new SystemThread(std::move(systemThreadInterface));
		thread->_systemThread->RunDetached();

		// Perform synchronization based on the state of initialization process - if requested, current thread is blocked
		// until this thread object finishes its initialization phase. Since this phase also includes registration in
		// parent's list, this synchronization is mandatory for every instance of LocalThread class. Otherwise, parent
		// thread may finish before its child will be able to register itself.

		if ((launchInfo.readyStateSyncObject != nullptr) && launchInfo.readyStateWaitImmediately)
			thread->WaitForReadyState(launchInfo);

		if (launchInfo.setRunPermission)
			thread->SetRunPermission();

		return ExsResultEx(RSC_Success, "Thread has been started successfuly.");
	}


	bool Thread::Init()
	{
		auto tlsInfo = Internal::GetCurrentThreadLocalStorageUnchecked();

		// Unchecked version of TLS getter also returns fetch counter. At this point, TLS should be fetched for the very
		// first time. Check this - if fetch counter is greater than 1, there is a logic error somewhere in code (the TLS
		// cannot be used before initialization which is performed here!).

		if (tlsInfo.second > 1)
			ExsExceptionThrowEx(EXC_Invalid_State, "Thread initialization error - TLS has been already queried before!");

		ThreadLocalStorage* tls = tlsInfo.first;
		ExsZeroMemory(tls, sizeof(ThreadLocalStorage));

		tls->controlBlock.validationKey = Config::TLS_Internal_Validation_Key;
		tls->thread = this;
		tls->threadHandle = this->_handle;
		tls->threadUID = this->_baseInfo->threadUID;
		tls->extDataReleaseList = new TLSExtDataReleaseList();

		this->_localStorage = tls;
		this->OnInit();

		return true;
	}


	void Thread::Release()
	{
		this->OnRelease();

		auto& executionStateLock = this->_executionStateSync.GetLock();
		ExsEnterCriticalSection(executionStateLock);
		{
			ExsDebugAssert( ExsEnumGetThreadExecutionStateIsFinished(this->GetExecutionState()) );
			this->_executionStateSync.Broadcast();
		}
		ExsLeaveCriticalSection();
		
		ThreadLocalStorage* tls = nullptr;
		std::swap(this->_localStorage, tls);
		
		auto current = tls->extDataReleaseList->rbegin();
		auto end = tls->extDataReleaseList->rend();

		for ( ; current != end; ++current)
		{
			(*current)();
		}

		delete tls->extDataReleaseList;
		ExsZeroMemory(tls, sizeof(ThreadLocalStorage));
	}


	void Thread::OnInit()
	{ }


	void Thread::OnRelease()
	{ }


	void Thread::OnSystemThreadBegin()
	{ }


	void Thread::OnSystemThreadEnd()
	{ }

	
	bool Thread::CheckExecutionStateCommands()
	{
		ExsDeclareRestrictedThreadAccess( this->_handle );

		if (this->_internalState.is_set(State_Exit))
		{
			ExsTraceInfo(TRC_Core_Threading, "Exit notification received. Internal state has been set.");
			return false;
		}

		if (this->CheckSuspendRequestFlag())
		{
			auto& executionStateLock = this->_executionStateSync.GetLock();
			ExsEnterCriticalSection(executionStateLock);
			{
				if (this->CheckSuspendRequestFlag())
				{
					this->_executionStateSync.WaitFor(
						Milliseconds(333),
						[this]() -> bool {
							return !this->CheckSuspendRequestFlag();
					});
				}
			}
			ExsLeaveCriticalSection();
		}
		
		return true;
	}


	void Thread::SetReadyState(SyncObject* readyStateSyncObject)
	{
		ExsDeclareRestrictedThreadAccess( this->_handle );

		// If sync object is specified, it means, that parent thread wants to be notified when this thread finishes its
		// execution phase (which already happended: see implementation of ThreadControlProc()). If this is the case,
		// set ready state and signal provided sync object. Otherwise just set the state.

		if (readyStateSyncObject == nullptr)
		{
			this->_internalState.set(State_Ready);
		}
		else
		{
			auto& readyStateLock = readyStateSyncObject->GetLock();
			ExsEnterCriticalSection(readyStateLock);
			{
				this->_internalState.set(State_Ready);
				readyStateSyncObject->NotifyOne();
			}
			ExsLeaveCriticalSection();
		}

		ExsTraceInfo(TRC_Core_Threading, "Ready state has been set.");
	}


	void Thread::SleepFor(const Milliseconds& duration)
	{
		ExsDeclareRestrictedThreadAccess( this->GetUID() );

		// Temporary local object used to perform the sleep operation.
		InternalSyncObject waitObj { this };

		auto& waitObjLock = waitObj.GetLock();
		ExsEnterCriticalSection(waitObj.GetLock());
		{
			waitObj.WaitFor(duration);
		}
		ExsLeaveCriticalSection();
	}
	
	
	void Thread::WaitForRunPermission()
	{
		if (!this->CheckRunPermissionFlag())
		{
			auto& executionStateLock = this->_executionStateSync.GetLock();
			ExsEnterCriticalSection(executionStateLock);
			{
				if (!this->CheckRunPermissionFlag())
				{
					ExsTraceInfo(TRC_Core_Threading, "Waiting for run permission...");

					this->_executionStateSync.Wait([this]() -> bool {
						return this->CheckRunPermissionFlag();
					});

					ExsTraceInfo(TRC_Core_Threading, "Run permission received.");
				}
			}
			ExsLeaveCriticalSection();
		}
	}


	void Thread::_OnSystemThreadBeginProc(Thread* thread)
	{
		thread->OnSystemThreadBegin();
	}


	void Thread::_OnSystemThreadEndProc(Thread* thread)
	{
		thread->OnSystemThreadEnd();
	}


	void Thread::_ReleaseProc(Thread* thread)
	{
		auto threadSystemController = thread->_threadSystemController;
		threadSystemController->UnregisterThread(thread->_baseInfo);

		delete thread;
	}


	void Thread::_ExecutionControlProc(Thread* thread, SyncObject* readyStateSyncObj)
	{
		thread->Init();

		thread->SetExecutionState(ThreadExecutionState::Active);
		thread->SetReadyState(readyStateSyncObj);

		try
		{
			thread->WaitForRunPermission();
			thread->SetExecutionState(ThreadExecutionState::Running);
			
			if (thread->CheckExecutionStateCommands())
			{
				ExsTraceInfo(TRC_Core_Threading, "Thread 0x%p is entering entry proc...", thread);
				thread->Entry();
			}

			thread->SetExecutionState(ThreadExecutionState::Finished);
		}
		catch(const ThreadInterruptException& exception)
		{
			Enum exceptionCode = exception.GetCode();
			Enum interruptCode = exception.GetInterruptCode();

			if (exceptionCode == EXC_Thread_Exit)
			{
				ExsTraceInfo(TRC_Core_Threading, "Thread has been interrupted (SIG_EXIT). Interrupt code: 0x%X.", interruptCode);
				thread->SetExecutionState(ThreadExecutionState::Interrupted_Exit);
			}
			else if (exceptionCode == EXC_Thread_Terminate)
			{
				ExsTraceInfo(TRC_Core_Threading, "Thread has been interrupted (SIG_TERM). Interrupt code: 0x%X.", interruptCode);
				thread->SetExecutionState(ThreadExecutionState::Interrupted_Terminate);
			}
			else
			{
				ExsTraceInfo(TRC_Core_Threading, "Thread has been interrupted (?). Interrupt code: 0x%X.", interruptCode);
				thread->SetExecutionState(ThreadExecutionState::Unknown);

				throw exception;
			}
		}
		catch(const Exception& exception)
		{
			DefaultExceptionHandler(exception);
		}
		catch(const std::exception& exception)
		{
			DefaultExceptionHandler(exception);
		}
		
		thread->Release();
	}


}
