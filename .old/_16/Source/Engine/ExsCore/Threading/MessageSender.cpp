
#include "_Precompiled.h"
#include <ExsCore/Threading/MessageSender.h>
#include <ExsCore/Threading/Message.h>
#include <ExsCore/Threading/MessageDispatcher.h>
#include <ExsCore/Threading/ThreadAccessGuards.h>
#include <ExsCore/Threading/Transceiver.h>
#include <ExsCore/Threading/ActiveThread.h>


namespace Exs
{


	MessageSender::MessageSender(ActiveThread* parentThread, Transceiver* transceiver, MessageDispatcher* dispatcher)
	: _parentThread(parentThread)
	, _parentThreadHandle(transceiver->GetParentThreadHandle())
	, _syncObj(parentThread)
	, _dispatcher(dispatcher)
	, _currentMessageUID(MessageUID_Invalid)
	{ }


	MessageSender::~MessageSender()
	{ }


	Result MessageSender::SendMessage(ThreadUID recipientUID, const CommonMessageHandle& message)
	{
		// Message sender object can be used only by thread which owns it.
		ExsDeclareRestrictedThreadAccess( this->GetParentThreadHandle() );

		if (!this->_dispatcher)
			ExsExceptionThrowEx(EXC_Invalid_Operation, "MessageDispatcher has not been initialized!");

		// Set ID of currently processed message.
		this->SetCurrentMessageUID(message->GetUID());

		// Try to dispatch message to its recipient.
		Result result = this->_dispatcher->DispatchMessage(recipientUID, message);

		// Clear ID of currently processed message.
		this->SetCurrentMessageUID(MessageUID_Invalid);

		return result;
	}


	Result MessageSender::SendMessage(ThreadUID recipientUID, const CommonMessageHandle& message, const MessageResponseRequest& responseRequest)
	{
		// Message sender object can be used only by thread which owns it.
		ExsDeclareRestrictedThreadAccess( this->GetParentThreadHandle() );

		if (!this->_dispatcher)
			ExsExceptionThrowEx(EXC_Invalid_Operation, "MessageDispatcher has not been initialized!");

		// Set ID of currently processed message.
		this->SetCurrentMessageUID(message->GetUID());

		// Try to dispatch message to its recipient.
		Result result = this->_dispatcher->DispatchMessage(recipientUID, message);

		if (result != RSC_Msg_Message_Discarded)
		{
			if (message->HasResponseRequestSet())
			{
				// Check if recipient of this message is registered and active. By default, we propagate only response
				// requests attached to messages, whose recipients can be immediately notified to avoid long waits.
				if ((result != RSC_Msg_Receiver_Inactive) && (result != RSC_Msg_Message_Enqueued))
				{
					message->CreateResponseState(&(this->_syncObj), responseRequest.waitTimeout);
				}
				// Optionally, user can request, that wait for response should be performed even if recipient is not
				// currently active (that includes recipients which are not registered at all).
				else if (responseRequest.waitForMissingRecipient)
				{
					ExsTraceWarning(TRC_Core_Messaging,
						"Creating response state for message 0x" EXS_PFU32_HEX " sent to inactive recipient..", message->GetUID());

					message->CreateResponseState(&(this->_syncObj), responseRequest.waitTimeout);
				}
				// Otherwise, clear response request to cancel it.
				else
				{
					message->ClearResponseRequest();
				}
			}
		}

		// Clear ID of currently processed message.
		this->SetCurrentMessageUID(MessageUID_Invalid);

		return result;
	}


}
