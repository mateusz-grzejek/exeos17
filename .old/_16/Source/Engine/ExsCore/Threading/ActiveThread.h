
#pragma once

#ifndef __Exs_Core_ActiveThread_H__
#define __Exs_Core_ActiveThread_H__

#include "ActiveThreadState.h"
#include "Message.h"
#include "Thread.h"


namespace Exs
{


	class BackgroundThread;
	class LocalThread;


	///<summary>
	///</summary>
	class EXS_LIBCLASS_CORE ActiveThread : public Thread
	{
		EXS_DECLARE_NONCOPYABLE(ActiveThread);

		friend class ExternalThreadWrapper;
		friend class LocalThread;

	protected:
		typedef ActiveThreadState::ChildThreadRef ChildThreadRef;

	protected:
		ThreadUID                             _parentUID;
		MessageDispatcherRefPtr               _messageDispatcher;
		std::unique_ptr<ActiveThreadState>    _activeState;
		std::unique_ptr<Transceiver>          _transceiver;

	protected:
		///<summary>
		/// Default constructor used for thread objects initialization.
		///</summary>
		///<param name="coreEngineState"> Core engine state object. </param>
		///<param name="parentThread"> UID of parent thread. </param>
		///<param name="threadBaseInfo">  </param>
		///<param name="name">  </param>
		ActiveThread(CoreEngineStateRefHandle coreEngineState, ThreadUID parentUID, ThreadBaseInfo* threadBaseInfo, const std::string& name);

	public:
		virtual ~ActiveThread();

		///<summary>
		///</summary>
		virtual Transceiver* GetTransceiver() override final;

		///<summary>
		///</summary>
		///<param name="message">  </param>
		Result SendMessage(ThreadUID recipientUID, const CommonMessageHandle& message);

		///<summary>
		///</summary>
		///<param name="message">  </param>
		Result SendMessage(ThreadUID recipientUID, const CommonMessageHandle& message, const MessageResponseRequest& responseRequest);

		///<summary>
		///</summary>
		ThreadUID GetParentUID() const;

		///<summary>
		/// Returns number of child threads, which were created by this thread object. Note, that this number
		/// refers only to *active* threads (which means threads, that have valid <c>ActiveThreadState</c>
		/// object allocated and registered in ATR.
		///</summary>
		Size_t GetChildThreadsNum() const;

		///<summary>
		/// Returns true, if thread has any child threads (<c>GetChildThreadsNum() > 0</c>) or false otherwise.
		///</summary>
		bool HasChildThreads() const;

	friendapi:
		//
		ChildThreadRef AddChild(LocalThread* thread);

		//
		void RemoveChild(ChildThreadRef threadRef, LocalThread* thread);

	protected:
		//
		CommonMessageHandle PeekNextMessage();

		//
		CommonMessageHandle WaitForNextMessage();

		//
		CommonMessageHandle WaitForNextMessage(const Microseconds& timeout);

	private:
		//
		virtual void Entry() override = 0;

		//
		virtual void OnInit() override;

		//
		virtual void OnRelease() override;
	};


	inline ThreadUID ActiveThread::GetParentUID() const
	{
		return this->_parentUID;
	}

	inline Size_t ActiveThread::GetChildThreadsNum() const
	{
		return this->_activeState->GetChildThreadsNum();
	}

	inline bool ActiveThread::HasChildThreads() const
	{
		return this->_activeState->GetChildThreadsNum() > 0;
	}


	#define EXS_THR_CTOR_STDARGS_DECL_ACTIVE_THREAD \
		CoreEngineStateRefHandle coreEngineState, ThreadUID parentUID, ThreadBaseInfo* threadBaseInfo

	#define EXS_THR_CTOR_STDARGS_DEF_ACTIVE_THREAD \
		coreEngineState, parentUID, threadBaseInfo


	///<summary>
	///</summary>
	class EXS_LIBCLASS_CORE BackgroundThread : public ActiveThread
	{
		EXS_DECLARE_NONCOPYABLE(BackgroundThread);
		
	protected:
		///<param name="coreEngineState"> Core engine state object. </param>
		///<param name="parentThread"> UID of parent thread. </param>
		///<param name="threadBaseInfo">  </param>
		///<param name="name">  </param>
		BackgroundThread(CoreEngineStateRefHandle coreEngineState, ThreadUID parentUID, ThreadBaseInfo* threadBaseInfo, const std::string& name);

	public:
		virtual ~BackgroundThread();
	};


	#define EXS_THR_CTOR_STDARGS_DECL_BACKGROUND_THREAD \
		CoreEngineStateRefHandle coreEngineState, ThreadUID parentUID, ThreadBaseInfo* threadBaseInfo

	#define EXS_THR_CTOR_STDARGS_DEF_BACKGROUND_THREAD \
		coreEngineState, parentUID, threadBaseInfo


	///<summary>
	///</summary>
	class EXS_LIBCLASS_CORE LocalThread : public ActiveThread
	{
		EXS_DECLARE_NONCOPYABLE(LocalThread);

	public:
		typedef ActiveThreadState::ChildThreadRef ChildThreadRef;

	private:
		ActiveThread*     _parentThread;
		ChildThreadRef    _selfParentRef;
		
	protected:
		///<param name="coreEngineState"> Core engine state object. </param>
		///<param name="parentThread"> Pointer to the parent thread object. </param>
		///<param name="threadBaseInfo">  </param>
		///<param name="name">  </param>
		LocalThread(CoreEngineStateRefHandle coreEngineState, ActiveThread* parentThread, ThreadBaseInfo* threadBaseInfo, const std::string& name);
		
	public:
		virtual ~LocalThread();

		///<summary>
		///</summary>
		Thread* GetParentThread() const;

	private:
		//
		virtual void Entry() override = 0;

		//
		virtual void OnSystemThreadBegin() override;

		//
		virtual void OnSystemThreadEnd() override;
	};


	inline Thread* LocalThread::GetParentThread() const
	{
		return this->_parentThread;
	}


	#define EXS_THR_CTOR_STDARGS_DECL_LOCAL_THREAD \
		CoreEngineStateRefHandle coreEngineState, ActiveThread* parentThread, ThreadBaseInfo* threadBaseInfo

	#define EXS_THR_CTOR_STDARGS_DEF_LOCAL_THREAD \
		coreEngineState, parentThread, threadBaseInfo


}


#endif /* __Exs_Core_ActiveThread_H__ */
