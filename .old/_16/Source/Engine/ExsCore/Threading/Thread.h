
#pragma once

#ifndef __Exs_Core_Thread_H__
#define __Exs_Core_Thread_H__

#include "ActiveThreadState.h"
#include "SyncObjects.h"
#include "ThreadCommon.h"
#include "../Exception.h"
#include "../SharedData.h"


namespace Exs
{


	struct ThreadLaunchInfo;

	class SyncObject;
	class Transceiver;
	
	ExsDeclareRefPtrClass(MessageDispatcher);
	ExsDeclareRefPtrClass(ThreadSystemController);
	ExsDeclareSharedDataRefHandle(CoreEngineState);


	///<summary>
	///</summary>
	class ThreadInterruptException : public InterruptException
	{
	private:
		Enum    _interruptCode;

	public:
		ThreadInterruptException(const ExceptionInfo& info, Enum interruptCode) noexcept
		: InterruptException(info)
		, _interruptCode(interruptCode)
		{ }

		Enum GetInterruptCode() const
		{
			return this->_interruptCode;
		}
	};

	
	EXS_EXCEPTION_SET_CODE_TYPE(EXC_Thread_Exit, ThreadInterruptException);
	EXS_EXCEPTION_SET_CODE_TYPE(EXC_Thread_Terminate, ThreadInterruptException);


	///<summary>
	///</summary>
	class EXS_LIBCLASS_CORE Thread
	{
		EXS_DECLARE_NONCOPYABLE(Thread);
		
		friend class CurrentThread;
		friend class CurrentThreadObject;
		friend class SyncObject;
		friend class ThreadLauncher;

	public:
		enum StateFlags : Enum
		{
			// Thread has 'ready state' - it finished the initialization phase and is ready to begin execution.
			State_Ready = 0x0001,

			// Thread has received permission to run.
			State_Run_Permission = 0x0002,

			// Thread has been requested (either internally or by an external source) to end its execution.
			State_Exit = 0x0004,

			//
			State_Suspend_Request = 0x0800,

			//
			State_Default = 0
		};

		typedef LightMutex WaitStateLock;
		
	protected:
		CoreEngineStateRefHandle             _coreEngineState; // Handle to global state.
		ThreadBaseInfo*                      _baseInfo; // Base info allocated for this thread object.
		ThreadSystemControllerRefPtr         _threadSystemController; // Thread system controller object.
		ThreadUID                            _uid; // UID of this thread.
		ThreadHandle                         _handle; // Handle to this thread.
		std::string                          _name; // Name of this thread.
		SystemThread*                        _systemThread; // SystemThread object used to execute this thread.
		ThreadLocalStorage*                  _localStorage; // Pointer to the local storage (TLS) of this thread.
		stdx::atomic_mask<Enum>              _internalState;
		std::atomic<ThreadExecutionState>    _executionState;
		SharedSyncObject                     _executionStateSync;
		std::atomic<ThreadWaitState*>        _waitState;
		WaitStateLock                        _waitStateLock;

	protected:
		///<summary>
		/// Default constructor used for thread objects initialization.
		///</summary>
		///
		///<param name="controller"> Instance of thread controller, that manages this thread's state. </param>
		///<param name="name"> String, that somehow identifies this thread object. </param>
		Thread(CoreEngineStateRefHandle coreEngineState, ThreadBaseInfo* threadBaseInfo, const std::string& name);

	public:
		virtual ~Thread();

		///<summary>
		///</summary>
		virtual ThreadExecutionState WaitForCompletion();
		
		///<summary>
		///</summary>
		virtual Transceiver* GetTransceiver();

		///<summary>
		///</summary>
		virtual const char* GetTextID() const;

		///<summary>
		///</summary>
		void SetStateFlags(Enum stateFlags, bool set = true);

		///<summary>
		///</summary>
		bool TrySetStateFlags(Enum stateFlags, bool set = true);
		
		///<summary>
		///</summary>
		void SetRunPermission();
		
		///<summary>
		///</summary>
		void SuspendThreadExecution();
		
		///<summary>
		///</summary>
		void ResumeThreadExecution();

		///<summary>
		/// Performs state synchronization between the current thread and the thread represented by this object.
		/// Synchronization is done in terms of ready state - the current thread is blocked until thread
		/// represented by this object is ready. The main purpose of this function is to allow threads to wait
		/// for their child threads (so parent can be sure that the newly created thread is ready for execution).
		/// The object used for synchronization must be exactly the same object which was passed to <c>Start()</c>.
		/// If the thread is ready at the time of the call, the function has no effect.
		///</summary>
		///
		///<param name="threadLaunchInfo"> ThreadLaunchInfo object used to launch this thread. </param>
		///
		///<remarks>
		/// This function must be called for every instance of LocalThread, as these threads must always be
		/// properly synchronized with their parents. If <c>threadLaunchInfo</c> is not the same object
		/// which was passed to <c>Start()</c>, behaviour is undefined. If the thread has not been created
		/// with a valid sync object, behaviour is undefined.
		///</remarks>
		void WaitForReadyState(const ThreadLaunchInfo& threadLaunchInfo);
		
		///<summary>
		///</summary>
		bool Exit();
		
		///<summary>
		///</summary>
		void AbortCurrentWait();

		///<summary>
		///</summary>
		bool CheckStateFlags(Enum stateFlags) const;
		
		///<summary>
		///</summary>
		ThreadHandle GetHandle() const;
		
		///<summary>
		///</summary>
		ThreadExecutionState GetExecutionState() const;
		
		///<summary>
		///</summary>
		ThreadUID GetUID() const;
		
		///<summary>
		///</summary>
		const std::string& GetName() const;
		
		///<summary>
		///</summary>
		bool IsActive() const;
		
		///<summary>
		///</summary>
		bool IsReady() const;
		
		///<summary>
		///</summary>
		bool IsRunning() const;
		
		///<summary>
		///</summary>
		static ThreadBaseInfo* Register(ThreadSystemController* threadSystemController, ThreadType threadType);
		
		///<summary>
		/// Starts thread by creating a new system-level thread, that executes specified logic. It also passes
		/// a sync object, that is used to synchronize the current (i.e. parent) thread with the internal state
		/// of this thread object. This sync object is signaled only when started thread finishes its initialization
		///  and isready to begin execution of its entry proc (we say, that such thread acquires "ready state").
		/// This function also automatically waits for this state, depending on the value of second parameter.
		/// Returns RSC_Success on success or appropriate error code otherwise.
		///</summary>
		///
		///<param name="launchInfo"> Info structure containing desciption and data used for thread launching. </param>
		///
		///<remarks>
		/// Synchronization is done as if <c>WaitForReadyState</c> was called. If <c>wait</c> is false,
		/// you should wait manually by calling this function.
		///</remarks>
		///
		///<seealso> <c>Thread::WaitForReadyState()</c> </seealso>
		static Result Start(Thread* thread, const ThreadLaunchInfo& launchInfo);

	protected:
		///<summary>
		/// Blocks current thread of execution for a specified amount of time. Actual duration may differ from
		/// the specified value, due to available clock resolution. This method serves for internal purposes
		/// and may be used only by the thread represented by this object.
		///</summary>
		///
		///<param name="duration"> Duration, for which execution should be suspended. </param>
		void SleepFor(const Milliseconds& duration);
		
		///<summary>
		/// Sets the current execution state of this thread.
		///</summary>
		///
		///<param name="state"> State, in which the thread represented by this object currently is. </param>
		ThreadExecutionState SetExecutionState(ThreadExecutionState state);

		///<summary>
		/// Checks internal state flags, that can be set by external sources and returns boolean value,
		/// which indicates, whether execution should be continued.
		///</summary>
		bool CheckExecutionStateCommands();
		
		///<summary>
		///<summary>
		bool CheckRunPermissionFlag();
		
		///<summary>
		///<summary>
		bool CheckSuspendRequestFlag();
		
		bool Init();
		void Release();

		virtual void OnInit();
		virtual void OnRelease();
		virtual void OnSystemThreadBegin();
		virtual void OnSystemThreadEnd();

	private:
		virtual void Entry() = 0;
		
		void WaitForRunPermission();
		
		ThreadWaitState* GetWaitState();
		WaitStateLock& GetWaitStateLock();
		
		void SetReadyState(SyncObject* readyStateSyncObject);
		void SetWaitState(ThreadWaitState* waitState);

		static void _OnSystemThreadBeginProc(Thread* thread);
		static void _OnSystemThreadEndProc(Thread* thread);
		static void _ReleaseProc(Thread* thread);
		static void _ExecutionControlProc(Thread* thread, SyncObject* readyStateSyncObj);
	};


	inline void Thread::SetStateFlags(Enum stateFlags, bool set)
	{
		set ? this->_internalState.set(stateFlags) : this->_internalState.unset(stateFlags);
	}

	inline bool Thread::TrySetStateFlags(Enum stateFlags, bool set)
	{
		return set ? this->_internalState.test_and_set(stateFlags) : this->_internalState.test_and_unset(stateFlags);
	}

	inline bool Thread::CheckStateFlags(Enum stateFlags) const
	{
		return this->_internalState.is_set(stateFlags);
	}

	inline ThreadHandle Thread::GetHandle() const
	{
		return this->_handle;
	}

	inline ThreadExecutionState Thread::GetExecutionState() const
	{
		return this->_executionState.load(std::memory_order_acquire);
	}

	inline ThreadUID Thread::GetUID() const
	{
		return this->_uid;
	}

	inline const std::string& Thread::GetName() const
	{
		return this->_name;
	}

	inline bool Thread::IsActive() const
	{
		ThreadExecutionState currentState = this->GetExecutionState();
		return (currentState >= ThreadExecutionState::Active) && (currentState < ThreadExecutionState::Finished);
	}

	inline bool Thread::IsReady() const
	{
		return this->_internalState.is_set(State_Ready, std::memory_order_acquire);
	}

	inline bool Thread::IsRunning() const
	{
		return this->GetExecutionState() == ThreadExecutionState::Running;
	}

	inline ThreadExecutionState Thread::SetExecutionState(ThreadExecutionState state)
	{
		return this->_executionState.exchange(state, std::memory_order_release);
	}

	inline bool Thread::CheckRunPermissionFlag()
	{
		return this->_internalState.is_set(State_Run_Permission, std::memory_order_acquire);
	}

	inline bool Thread::CheckSuspendRequestFlag()
	{
		return this->_internalState.is_set(State_Suspend_Request, std::memory_order_acquire);
	}

	inline void Thread::SetWaitState(ThreadWaitState* waitState)
	{
		this->_waitState.store(waitState, std::memory_order_release);
	}

	inline ThreadWaitState* Thread::GetWaitState()
	{
		return this->_waitState.load(std::memory_order_acquire);
	}

	inline Thread::WaitStateLock& Thread::GetWaitStateLock()
	{
		return this->_waitStateLock;
	}


}


#endif /* __Exs_Core_Thread_H__ */
