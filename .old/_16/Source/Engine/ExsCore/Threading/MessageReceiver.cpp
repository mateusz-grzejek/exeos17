
#include "_Precompiled.h"
#include <ExsCore/Threading/MessageReceiver.h>
#include <ExsCore/Threading/ActiveThread.h>
#include <ExsCore/Threading/ThreadAccessGuards.h>
#include <ExsCore/Threading/Transceiver.h>


namespace Exs
{


	MessageInbox::MessageInbox(ThreadHandle parentThreadHandle, Size_t commonMessageIgnoreCountLimit)
	: _parentThreadHandle(parentThreadHandle)
	, _commonMessageIgnoreCountLimit(commonMessageIgnoreCountLimit)
	, _currentCommonMessageIgnoreCount(0)
	{ }


	MessageInbox::~MessageInbox()
	{ }


	void MessageInbox::PutMessage(const CommonMessageHandle& message)
	{
		if (message->IsHighPriorityMessage())
			this->_highPriorityMessages.push_back(message);
		else
			this->_commonMessages.push_back(message);

	}


	CommonMessageHandle MessageInbox::TakeNextMessage()
	{
		CommonMessageHandle messageHandle = nullptr;

		if (auto* sourceInbox = this->_ChooseQueue())
		{
			messageHandle = sourceInbox->front();
			sourceInbox->pop_front();
		}

		return messageHandle;
	}


	void MessageInbox::Clear()
	{
		this->_commonMessages.clear();
		this->_highPriorityMessages.clear();
	}


	MessageInbox::MessageQueue* MessageInbox::_ChooseQueue()
	{
		MessageQueue* sourceInbox = nullptr;

		// By default, select queue with high priority messages (if not empty).
		if (!this->_highPriorityMessages.empty())
			sourceInbox = &(this->_highPriorityMessages);

		if (sourceInbox == nullptr)
		{
			// If priority queue is empty, check second queue and select it, if any messages are enqueued there.
			if (!this->_commonMessages.empty())
			{
				sourceInbox = &(this->_commonMessages);
				this->_currentCommonMessageIgnoreCount = 0;
			}
		}
		else if (!this->_commonMessages.empty())
		{
			// If priority queue was selected, but normal queue also has messages enqueued, check ignore counter.
			if (this->_currentCommonMessageIgnoreCount < this->_commonMessageIgnoreCountLimit)
			{
				// Ok, value of ignore counter is still acceptable - we can ignore low priority messages.
				// Increment ignore counter and leave priority queue selected.
				++this->_currentCommonMessageIgnoreCount;
			}
			else
			{
				// Nope, too many ignores already, we must skip high priority message this time, so messages
				// with lower priorities can be also properly received.
				sourceInbox = &(this->_commonMessages);
				this->_currentCommonMessageIgnoreCount = 0;
			}
		}

		return sourceInbox;
	}




	MessageReceiver::MessageReceiver(ActiveThread* parentThread, Transceiver* transceiver)
	: _parentThread(parentThread)
	, _parentThreadHandle(transceiver->GetParentThreadHandle())
	, _syncObj(parentThread)
	, _inbox(_parentThreadHandle)
	, _messagesNum(0)
	{ }


	MessageReceiver::~MessageReceiver()
	{ }


	void MessageReceiver::PostMessage(CommonMessageHandle message)
	{
		ExsEnterCriticalSection(this->_syncObj.GetLock());
		{
			this->_Append(message);
			this->_syncObj.NotifyOne();
		}
		ExsLeaveCriticalSection();
	}


	CommonMessageHandle MessageReceiver::PeekMessage()
	{
		ExsDeclareRestrictedThreadAccess( this->GetParentThreadHandle() );
		CommonMessageHandle messageHandle = nullptr;

		if (!this->IsEmpty())
		{
			ExsEnterCriticalSection(this->_syncObj.GetLock());
			{
				if (!this->IsEmpty())
					messageHandle = this->_TakeNext();
			}
			ExsLeaveCriticalSection();
		}

		return messageHandle;
	}


	CommonMessageHandle MessageReceiver::WaitForMessage(const Microseconds& waitTimeout)
	{
		ExsDeclareRestrictedThreadAccess( this->GetParentThreadHandle() );
		CommonMessageHandle messageHandle = nullptr;

		ExsEnterCriticalSection(this->_syncObj.GetLock());
		{
			if (this->IsEmpty())
			{
				WaitResult waitResult = this->_syncObj.WaitFor(waitTimeout, [this]() -> bool { return !this->IsEmpty(); });

				if ((waitResult == WaitResult::Signaled) && !this->IsEmpty())
					messageHandle = this->_TakeNext();
			}
		}
		ExsLeaveCriticalSection();

		return messageHandle;
	}

	
	void MessageReceiver::_Append(const CommonMessageHandle& message)
	{
		this->_inbox.PutMessage(message);
		this->_messagesNum.fetch_add(1, std::memory_order_relaxed);
	}


	CommonMessageHandle MessageReceiver::_TakeNext()
	{
		this->_messagesNum.fetch_sub(1, std::memory_order_relaxed);
		CommonMessageHandle message = this->_inbox.TakeNextMessage();

		return message;
	}


}
