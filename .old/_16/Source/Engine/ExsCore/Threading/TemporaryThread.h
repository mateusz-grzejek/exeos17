
#pragma once

#ifndef __Exs_Core_TemporaryThread_H__
#define __Exs_Core_TemporaryThread_H__

#include "Thread.h"


namespace Exs
{


	class TemporaryThread : public Thread
	{
		EXS_DECLARE_NONCOPYABLE(TemporaryThread);

	protected:
		///<summary>
		/// Default constructor used for thread objects initialization.
		///</summary>
		///
		///<param name="controller"> Instance of thread controller, that manages this thread's state. </param>
		///<param name="name"> String, that somehow identifies this thread object. </param>
		TemporaryThread(CoreEngineStateRefHandle coreEngineState, ThreadBaseInfo* threadBaseInfo, const std::string& name)
		: Thread(coreEngineState, threadBaseInfo, name)
		{ }

		///<summary>
		///</summary>
		virtual ~TemporaryThread()
		{ }

		///<summary>
		///</summary>
		virtual void Entry() override = 0;
	};


}


#endif /* __Exs_Core_TemporaryThread_H__ */
