
#include "_Precompiled.h"
#include <ExsCore/Exception.h>
#include <ExsCore/Threading/ThreadAccessGuards.h>
#include <ExsCore/Threading/ThreadLocalStorage.h>
#include <ExsCore/Threading/ThreadInternal.h>


namespace Exs
{


	ThreadAccessGuard::ThreadAccessGuard(const char* funcName)
	: _funcName(funcName)
	{ }


	void ThreadAccessGuard::Enter()
	{
		auto threadHandle = CurrentThread::GetHandle();

		if (!this->IsThreadAllowed(threadHandle))
			this->RaiseError();
	}


	bool ThreadAccessGuard::TryEnter()
	{
		const ThreadHandle& threadHandle = CurrentThread::GetHandle();
		return this->IsThreadAllowed(threadHandle);
	}


	void ThreadAccessGuard::RaiseError()
	{
		ExsExceptionThrowEx(EXC_Unauthorized_Thread_Access, this->_funcName);
	}

	


	SingleThreadAccessGuard::SingleThreadAccessGuard(const char* funcName, ThreadUID threadUID)
	: ThreadAccessGuard(funcName)
	, _allowedThread(threadUID)
	{ }


	SingleThreadAccessGuard::SingleThreadAccessGuard(const char* funcName, ThreadHandle threadHandle)
	: ThreadAccessGuard(funcName)
	, _allowedThread(ThreadUtils::GetUIDFromHandle(threadHandle))
	{ }


	bool SingleThreadAccessGuard::IsThreadAllowed(ThreadUID threadUID) const
	{
		return threadUID == this->_allowedThread;
	}


	bool SingleThreadAccessGuard::IsThreadAllowed(ThreadHandle threadHandle) const
	{
		ThreadUID threadUID = ThreadUtils::GetUIDFromHandle(threadHandle);
		return threadUID == this->_allowedThread;
	}




	MultiThreadAccessGuard::MultiThreadAccessGuard(const char* funcName, std::initializer_list<ThreadUID>&& threadUIDs)
	: ThreadAccessGuard(funcName)
	{
		const ThreadUID* uidArray = threadUIDs.begin();
		size_t threadUIDNum = stdx::get_min_of(threadUIDs.size(), maxAllowedThreadsNum);

		for (size_t n = 0; n < threadUIDNum; ++n)
		{
			this->_allowedThreads[n] = uidArray[n];
		}
	}


	MultiThreadAccessGuard::MultiThreadAccessGuard(const char* funcName, std::initializer_list<ThreadHandle>&& threadHandles)
	: ThreadAccessGuard(funcName)
	{
		const ThreadHandle* handleArray = threadHandles.begin();
		size_t threadHandleNum = stdx::get_min_of(threadHandles.size(), maxAllowedThreadsNum);

		for (size_t n = 0; n < threadHandleNum; ++n)
		{
			this->_allowedThreads[n] = ThreadUtils::GetUIDFromHandle(handleArray[n]);
		}
	}


	bool MultiThreadAccessGuard::IsThreadAllowed(ThreadUID threadUID) const
	{
		for (auto& currentThreadUID : this->_allowedThreads)
		{
			if (currentThreadUID == threadUID)
				return true;
		}

		return false;
	}


	bool MultiThreadAccessGuard::IsThreadAllowed(ThreadHandle threadHandle) const
	{
		Enum threadUID = ThreadUtils::GetUIDFromHandle(threadHandle);
		return this->IsThreadAllowed(threadUID);
	}


}
