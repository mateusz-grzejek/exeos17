
#pragma once

#ifndef __Exs_Core_ConcurrentDataPool_H__
#define __Exs_Core_ConcurrentDataPool_H__

#include "../Exception.h"


namespace Exs
{


	template <class T, Size_t Index_size>
	class ConcurrentDataPool
	{
		EXS_DECLARE_NONCOPYABLE(ConcurrentDataPool);

	private:
		struct Entry
		{
		private:
			std::atomic<Uint32>  _allocState;

		public:
			T  data;

		public:
			Entry()
			: _allocState(0)
			{ }

			bool Allocate()
			{
				Uint32 desiredAllocState = 0;

				bool allocated = this->_allocState.compare_exchange_strong(desiredAllocState, 1,
					std::memory_order_acq_rel, std::memory_order_relaxed);

				return allocated;
			}

			void Release()
			{
				this->_allocState.store(std::memory_order_release);
			}

			bool IsAllocated() const
			{
				return this->_allocState.load(std::memory_order_acquire) != 0;
			}
		};

		typedef std::array<Entry, Index_size> Entries;

	private:
		std::atomic<Size_t>  _allocCounter;
		Entries              _entries;

	public:
		ConcurrentDataPool()
		: _allocCounter(0)
		{ }

		~ConcurrentDataPool()
		{ }

		T& operator[](Size_t index)
		{
			Entry& entry = this->_entries[index];
			ExsDebugAssert( entry.IsAllocated() );

			return entry.data;
		}

		const T& operator[](Size_t index) const
		{
			const Entry& entry = this->_entries[index];
			ExsDebugAssert( entry.IsAllocated() );

			return entry.data;
		}

		const Entry* GetEntry(Size_t index) const
		{
			const Entry& entry = this->_entries[index];
			return &(entry);
		}

		Size_t Alloc()
		{
			Size_t allocatedDataNum = this->_allocCounter.load(std::memory_order_relaxed);

			while (true)
			{
				if (allocatedDataNum == Index_size)
					return Invalid_Position;

				bool entryAcquired = this->_allocCounter.compare_exchange_strong(
					allocatedDataNum, allocatedDataNum + 1, std::memory_order_acq_rel, std::memory_order_relaxed);

				if (entryAcquired)
					break;
			}

			for (Size_t entryIndex = 0 ; entryIndex < Index_size; ++entryIndex)
			{
				Entry& entry = this->_entries[entryIndex];

				if (entry.Allocate())
					return entryIndex;
			}

			ExsExceptionThrowEx(EXC_Internal_Error, "ConcurrentDataPool::Alloc() has failed with critical desync error.");

			// Some compilers cannot resolve above macro properly and complain about missing return statement, altough this
			// line will never be executed.

			// return Invalid_Position;
		}

		void Release(Size_t index)
		{
			Entry& entry = this->_entries[index];
			ExsDebugAssert( entry.IsAllocated() );

			entry.Release();
			this->_allocCounter.fetch_sub(1, std::memory_order_release);
		}

		bool CheckAllocStatus(Size_t index) const
		{
			const Entry& entry = this->_entries[index];
			return entry.IsAllocated();
		}
	};


}


#endif /* __Exs_Core_ConcurrentDataPool_H__ */
