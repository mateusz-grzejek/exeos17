
#pragma once

#ifndef __Exs_Core_MessageResponse_H__
#define __Exs_Core_MessageResponse_H__

#include "LockedResourcePtr.h"
#include "Message.h"


namespace Exs
{


	///<summary>
	///</summary>
	template <class Content_type>
	struct MessageResponse
	{
	public:
		Message*        const respondedMessage;
		MessageUID      const respondedMessageUID;
		Content_type    content;

	public:
		MessageResponse(Message* message, MessageUID messageUID)
		: respondedMessage(message)
		, respondedMessageUID(messageUID)
		{ }
	};


	///<summary>
	/// Represents immediate response request state attached to a message object. Immediate response is a kind of response,
	/// that is passed directly back to sender thread using active instance of this class. 'Active' request is a request,
	/// that has been explicitly set and has not been cancelled by the time message is received by recipient.
	///</summary>
	class MessageResponseState
	{
		friend class Message;

	public:
		typedef InternalSyncObject::LockType LockType;
		typedef LockedResourcePtr<Message, LockType> LockedMessagePtr;

	private:
		enum StateFlags : Enum
		{
			//
			State_Cancelled = 0x8000,

			//
			State_Response_Ready = 0x0001
		};
	private:
		Message*                 _message;
		InternalSyncObject*      _syncObj;
		Milliseconds             _waitTimeout;
		stdx::atomic_mask<Enum>  _state;

	public:
		MessageResponseState(Message* message, InternalSyncObject* syncObject, const Milliseconds& waitTimeout)
		: _message(message)
		, _syncObj(syncObject)
		, _waitTimeout(waitTimeout)
		, _state(0)
		{ }

		LockedMessagePtr Lock()
		{
			// Check if response request has not been cancelled by the sender (current thread might have been busy
			// and this request has already expired).
			if (!this->IsResponseReady() && !this->IsCancelled())
			{
				auto& responseStateLock = this->_syncObj->GetLock();
				ExsEnterNamedCriticalSection(responseLockGuard, responseStateLock);
				{
					// We are inside the critical section defined by the lock of our sync object. This is the same lock
					// acquired by the sender during request cancelling, so we can safely test cancel flag and return.

					if (!this->IsCancelled())
					{
						// Request is still active and sender is waiting for the response - return locked response ptr.
						return LockedMessagePtr(this->_message, std::move(responseLockGuard));
					}
				}
				ExsLeaveCriticalSection();
			}

			return LockedMessagePtr();
		}

		void Notify(LockedMessagePtr& response)
		{
			ExsDebugAssert( response.GetResourcePtr() == this->_message );
			this->_state.set(State_Response_Ready);
			this->_syncObj->NotifyOne();
		}

		const Milliseconds& GetWaitTimeout() const
		{
			return this->_waitTimeout;
		}

		bool IsCancelled() const
		{
			return this->_state.is_set(State_Cancelled, std::memory_order_acquire);
		}

		bool IsResponseReady() const
		{
			return this->_state.is_set(State_Response_Ready, std::memory_order_acquire);
		}

	private:
		void Cancel()
		{
			this->_state.set(State_Cancelled, std::memory_order_release);
			this->_syncObj = nullptr;
		}

		InternalSyncObject* GetSyncObject()
		{
			ExsDebugAssert( this->_syncObj != nullptr );
			return this->_syncObj;
		}
	};


	template <class Content_type, class Response_content_type>
	class MessageWithContent<Content_type, Response_content_type, true, true> : public Message
	{
	public:
		typedef Content_type ContentType;
		typedef MessageResponse<Response_content_type> ResponseType;

	private:
		Content_type  _content;
		ResponseType  _response;

	public:
		template <class... ContentArgs>
		MessageWithContent(MessageCode code, MessageUID uid, stdx::mask<Enum> attribFlags, ContentArgs&&... contentArgs)
		: Message(code, uid, attribFlags)
		, _content(std::forward<ContentArgs>(contentArgs)...)
		, _response(this, uid)
		{ }

		virtual ~MessageWithContent()
		{ }

		const Content_type& GetContent() const
		{
			return this->_content;
		}

		const ResponseType& GetResponse() const
		{
			return this->_response;
		}

	private:
		virtual bool CreateResponseState(InternalSyncObject* syncObject, const Milliseconds& waitTimeout) override
		{
			this->_responseState.Reset(new MessageResponseState(this, syncObject, waitTimeout));
			return true;
		}
	};


	template <class Content_type, class Response_content_type>
	class MessageWithContent<Content_type, Response_content_type, false, true> : public Message
	{
	public:
		typedef void ContentType;
		typedef MessageResponse<Response_content_type> ResponseType;

	private:
		ResponseType  _response;

	public:
		MessageWithContent(MessageCode code, MessageUID uid, stdx::mask<Enum> attribFlags)
		: Message(code, uid, attribFlags)
		, _response(this, uid)
		{ }

		virtual ~MessageWithContent()
		{ }

		const ResponseType& GetResponse() const
		{
			return this->_response;
		}

	private:
		virtual bool CreateResponseState(InternalSyncObject* syncObject, const Milliseconds& waitTimeout) override
		{
			this->_responseState.Reset(new MessageResponseState(this, syncObject, waitTimeout));
			return true;
		}
	};


}


#endif /* __Exs_Core_MessageResponse_H__ */
