
#pragma once

#ifndef __Exs_Core_ExternalThreadWrapper_H__
#define __Exs_Core_ExternalThreadWrapper_H__

#include "ActiveThread.h"
#include "../CoreEngineState.h"


namespace Exs
{

	
	class ExternalThreadWrapper
	{
		EXS_DECLARE_NONCOPYABLE(ExternalThreadWrapper);

	private:
		std::unique_ptr<ActiveThread>  _threadObject;

	public:
		ExternalThreadWrapper(ActiveThread* threadObject)
		: _threadObject(threadObject)
		{
			if (this->_threadObject)
			{
				this->_threadObject->Init();
				this->_threadObject->SetExecutionState(ThreadExecutionState::Active);
			}
		}
		
		~ExternalThreadWrapper()
		{
			if (this->_threadObject)
			{
				this->_threadObject->SetExecutionState(ThreadExecutionState::Finished);
				this->_threadObject->Release();
			}
		}

		template <class Thread_t>
		Thread_t* As() const
		{
			return dbgsafe_ptr_cast<Thread_t*>(this->_threadObject.get());
		}

		ActiveThread* Get() const
		{
			return this->_threadObject.get();
		}
	};


	using ExternalThreadHandle = std::shared_ptr<ExternalThreadWrapper>;


	template <class Thread_t, class... Args>
	inline ExternalThreadHandle CreateExternalThreadObject(CoreEngineStateRefHandle coreEngineState, Args&&... args)
	{
		auto threadSystemController = coreEngineState->threadSystemController;
		ExsDebugAssert( threadSystemController  );
		
		auto* threadBaseInfo = Thread::Register(threadSystemController.get(), ThreadType::Active);
		ExsDebugAssert( threadBaseInfo != nullptr );

		// Is there any way to get UID of parent thread during external thread wrapper cration?
		ThreadUID parentThreadUID = 0;

		auto threadObject = std::make_unique<Thread_t>(coreEngineState, parentThreadUID, threadBaseInfo, std::forward<Args>(args)...);
		auto externalThreadObject = std::make_shared<ExternalThreadWrapper>(threadObject.release());

		return externalThreadObject;

		/* auto threadSystemController = coreEngineState->threadSystemController;
		ExsDebugAssert( threadSystemController  );
		
		ThreadBaseInfo* threadBaseInfo = Thread::Register(threadSystemController.get(), ThreadType::Active);
		ExsDebugAssert( threadBaseInfo != nullptr );

		auto externalThreadDeleter = std::bind([](Thread_t* threadPtr) -> void {
				Internal::ExternalThreadInitializer::Release(threadPtr);
				threadPtr->~Thread_t();
			},
			std::placeholders::_1);

		auto externalThreadObject = std::allocate_shared<Thread_t>(
			std::move(externalThreadDeleter), threadBaseInfo, sharedGlobalState, std::forward<Args>(args)...);

		Internal::ExternalThreadInitializer::Initialize(externalThreadObject.GetPtr());

		return externalThreadObject;*/
	}



}


#endif /* __Exs_Core_ExternalThreadWrapper_H__ */
