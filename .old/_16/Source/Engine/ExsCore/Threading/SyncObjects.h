
#pragma once

#ifndef __Exs_Core_SyncObjects_H__
#define __Exs_Core_SyncObjects_H__

#include "ConditionVariable.h"
#include "ConcurrentDataPool.h"


namespace Exs
{


	class SyncObject;


	// Use RSC_ codes, so these values may be easily returned by functions returning Result.
	enum class WaitResult : Enum
	{

		//
		Abort = RSC_Wait_Res_Abort,

		//
		Interrupt = RSC_Wait_Res_Interrupt,

		//
		Init_Error = RSC_Err_Failed,

		//
		Signaled = RSC_Success,

		//
		Timeout = RSC_Wait_Res_Timeout
	};


	namespace Config
	{

		enum : Duration_value_t
		{
			SNC_Default_Wait_Timeout_Milliseconds = 16
		};

	}


	inline Duration<DurationPeriod::Millisecond> GetDefaultThreadWaitTimeout()
	{
		return Duration<DurationPeriod::Millisecond>(Config::SNC_Default_Wait_Timeout_Milliseconds);
	}


	///<summary>
	/// Represents "state" of waiting process. When thread enters wait state (it calls one of Wait() methods from
	/// SyncObject class), sync object creates object of this type, fills it with required data and sets as current
	/// wait state for the thread object. This state object is used internally to control current thread state.
	///</summary>
	class ThreadWaitState
	{
	public:
		typedef LightMutex ThreadWaitStateLock;

	public:
		SyncObject*             waitObject; // Sync object used to synchroize wait process.
		Thread*                 threadObject; // Thread owning this state object.
		ThreadWaitStateLock*    threadWaitStateLock; // Internal lock of thread, used to gain exclusive access to its wait state.
		ThreadExecutionState    savedThreadExecutionState; // Execution state that thread object has, when it called Wait(). 
		std::atomic<bool>       interruptFlag; // Set, when waiting is interrupted with no respect to state of resource thread is waiting for.

	public:
		ThreadWaitState(SyncObject* waitObj)
		: waitObject(waitObj)
		, threadObject(nullptr)
		, threadWaitStateLock(nullptr)
		, savedThreadExecutionState(ThreadExecutionState::Unknown)
		, interruptFlag(false)
		{ }

		void SetInterruptFlag(bool enable)
		{
			this->interruptFlag.store(enable, std::memory_order_release);
		}

		bool HasInterruptFlag() const
		{
			return this->interruptFlag.load(std::memory_order_acquire);
		}
	};


	class SyncObject
	{
		EXS_DECLARE_NONCOPYABLE(SyncObject);

	public:
		typedef LightMutex LockType;

	protected:
		struct WaitContextInfo
		{
		public:
			Thread*           threadObject;
			ThreadWaitState*  threadWaitState;
			Uint              xdata[2];

		public:
			WaitContextInfo(ThreadWaitState* waitState)
			: threadObject(nullptr)
			, threadWaitState(waitState)
			{ }
		};

	protected:
		LockType                              _lock;
		ConditionVariableAdapter<LockType>    _syncCv;
		std::atomic<bool>                     _abortFlag;

	public:
		SyncObject()
		: _syncCv(_lock)
		, _abortFlag(false)
		{ }

		///<summary>
		///</summary>
		WaitResult Wait()
		{
			ThreadWaitState threadWaitState { this };
			WaitContextInfo waitContextInfo { &threadWaitState };

			if (!this->OnWaitBegin(&waitContextInfo))
				return WaitResult::Init_Error;

			this->_syncCv.Wait();

			this->OnWaitEnd(&waitContextInfo);

			if (threadWaitState.HasInterruptFlag())
				return WaitResult::Interrupt;

			if (this->HasAbortFlag())
				return WaitResult::Abort;

			return WaitResult::Signaled;		
		}

		///<summary>
		///</summary>
		template <class Predicate>
		WaitResult Wait(Predicate predicate)
		{
			ThreadWaitState threadWaitState { this };
			WaitContextInfo waitContextInfo { &threadWaitState };

			if (!this->OnWaitBegin(&waitContextInfo))
				return WaitResult::Init_Error;

			this->_syncCv.Wait([&]() -> bool {
				return predicate() || threadWaitState.HasInterruptFlag() || this->HasAbortFlag();
			});

			this->OnWaitEnd(&waitContextInfo);

			if (threadWaitState.HasInterruptFlag())
				return WaitResult::Interrupt;

			if (this->HasAbortFlag())
				return WaitResult::Abort;

			return WaitResult::Signaled;
		}

		///<summary>
		///</summary>
		template <class Period>
		WaitResult WaitFor(const std_duration<Period>& duration)
		{
			Duration_value_t durationCount = duration.count();

			if (durationCount == 0)
				return WaitResult::Timeout;

			if (durationCount == Timeout_Infinite)
				return this->Wait();

			ThreadWaitState threadWaitState { this };
			WaitContextInfo waitContextInfo { &threadWaitState };

			if (!this->OnWaitBegin(&waitContextInfo))
				return WaitResult::Init_Error;

			auto cvStatus = this->_syncCv.WaitFor(duration);
			bool signaled = (cvStatus != ConditionVariableStatus::Timeout);

			this->OnWaitEnd(&waitContextInfo);

			if (!signaled)
				return WaitResult::Timeout;

			if (signaled && threadWaitState.HasInterruptFlag())
				return WaitResult::Interrupt;

			if (signaled && this->HasAbortFlag())
				return WaitResult::Abort;

			return WaitResult::Signaled;
		}

		///<summary>
		///</summary>
		template <class Period, class Predicate>
		WaitResult WaitFor(const std_duration<Period>& duration, Predicate predicate)
		{
			Duration_value_t durationCount = duration.count();

			if (durationCount == 0)
				return WaitResult::Timeout;
			
			if (durationCount == Timeout_Infinite)
				return this->Wait(predicate);
			
			ThreadWaitState threadWaitState { this };
			WaitContextInfo waitContextInfo { &threadWaitState };

			if (!this->OnWaitBegin(&waitContextInfo))
				return WaitResult::Init_Error;

			bool signaled = this->_syncCv.WaitFor(
				duration,
				[&]() -> bool {
					return predicate() || threadWaitState.HasInterruptFlag() || this->HasAbortFlag();
			});
			
			this->OnWaitEnd(&waitContextInfo);

			if (!signaled)
				return WaitResult::Timeout;

			if (signaled && threadWaitState.HasInterruptFlag())
				return WaitResult::Interrupt;

			if (signaled && this->HasAbortFlag())
				return WaitResult::Abort;
			
			return WaitResult::Signaled;
		}

		///<summary>
		/// Returns reference to the internal lock used to control wait process.
		///</summary>
		LockType& GetLock()
		{
			return this->_lock;
		}

		///<summary>
		/// Returns const reference to the internal lock used to control wait process.
		///</summary>
		const LockType& GetLock() const
		{
			return this->_lock;
		}

		///<summary>
		/// Returns true if abort flag has been set or false otherwise.
		///</summary>
		bool HasAbortFlag() const
		{
			return this->_abortFlag.load(std::memory_order_acquire);
		}

		///<summary>
		/// Aborts all active wait operations executed using this sync object. All threads waiting for this SO
		/// are signalled. All Wait() functions caused by this call to exit will return WaitResult::Abort.
		///</summary>
		virtual void Abort() = 0;

		///<summary>
		/// Signals one waiting thread, causing it to finish waiting process and resume its execution.
		/// NOTE: This method should not be used to signal threads in error conditions - use Abort() or Interrupt() for that.
		///</summary>
		virtual void NotifyOne() = 0;

		///<summary>
		/// Signals all waiting threads, causing them to finish waiting process and resume their execution.
		/// NOTE: This method should not be used to signal threads in error conditions - use Abort() or Interrupt() for that.
		///</summary>
		virtual void Broadcast() = 0;

		///<summary>
		/// Interrupts wait process of the specified thread. If thread is not waiting for this sync object (or not at all),
		/// this request is silently ignored. Wait() function caused by this call to exit will return WaitResult::Interrupt.
		///</summary>
		EXS_LIBAPI_CORE virtual void Interrupt(Thread* thread);

	protected:
		// Sets abort flag to active.
		void SetAbortFlag(bool active)
		{
			this->_abortFlag.store(active, std::memory_order_release);
		}

		// Marks thread as waiting and sets waitState as its current wait state.
		void MarkThread(Thread* thread, ThreadWaitState* waitState);

		// Unmarks thread and clears its current wait state to NULL.
		void ResetThread(Thread* thread, ThreadWaitState* waitState);

		// Returns true if thread is in wait state using specified state object.
		static bool IsThreadWaiting(Thread* thread, ThreadWaitState* waitState);

		// Returns wait state of specified thread object (used by sub-classes).
		// Defined to make only one friendship: Thread-SyncObject.
		static ThreadWaitState* GetThreadWaitState(Thread* thread);

	private:
		// Performs initial setup of waiting process and saves information about execution context. Returns true
		// if waiting process can be continued or false otherwise - in such case WaitResult::Init_Error is returned.
		virtual bool OnWaitBegin(WaitContextInfo* waitInfo) = 0;

		// Clears thread state and reverts changes done by OnWaitBegin(). Clears execution context info.
		virtual void OnWaitEnd(WaitContextInfo* waitInfo) = 0;
	};

	
	class InternalSyncObject : public SyncObject
	{
		EXS_DECLARE_NONCOPYABLE(InternalSyncObject);

	private:
		Thread*  _owningThread;

	public:
		InternalSyncObject(Thread* owningThread)
		: SyncObject()
		, _owningThread(owningThread)
		{ }

		Thread* GetOwningThread() const
		{
			return this->_owningThread;
		}
	
		// @override SyncObject::Abort
		EXS_LIBAPI_CORE virtual void Abort() override;

		// @override SyncObject::NotifyOne
		EXS_LIBAPI_CORE virtual void NotifyOne() override;

		// @override SyncObject::Broadcast
		EXS_LIBAPI_CORE virtual void Broadcast() override;

	private:
		// @override SyncObject::OnWaitBegin
		EXS_LIBAPI_CORE virtual bool OnWaitBegin(WaitContextInfo* waitInfo) override;

		// @override SyncObject::OnWaitEnd
		EXS_LIBAPI_CORE virtual void OnWaitEnd(WaitContextInfo* waitInfo) override;
	};

	
	class UniqueSyncObject : public SyncObject
	{
		EXS_DECLARE_NONCOPYABLE(UniqueSyncObject);

	private:
		std::atomic<Thread*> _waitingThread;
		
	public:
		UniqueSyncObject()
		: SyncObject()
		, _waitingThread(nullptr)
		{ }

		Thread* GetWaitingThread() const
		{
			return this->_waitingThread.load(std::memory_order_relaxed);
		}
	
		// @override SyncObject::Abort
		EXS_LIBAPI_CORE virtual void Abort() override;

		// @override SyncObject::NotifyOne
		EXS_LIBAPI_CORE virtual void NotifyOne() override;

		// @override SyncObject::Broadcast
		EXS_LIBAPI_CORE virtual void Broadcast() override;

	private:
		void SetCurrentWaitingThread(Thread* thread)
		{
			this->_waitingThread.store(thread, std::memory_order_relaxed);
		}

		// @override SyncObject::OnWaitBegin
		EXS_LIBAPI_CORE virtual bool OnWaitBegin(WaitContextInfo* waitInfo) override;

		// @override SyncObject::OnWaitEnd
		EXS_LIBAPI_CORE virtual void OnWaitEnd(WaitContextInfo* waitInfo) override;
	};

	
	class SharedSyncObject : public SyncObject
	{
		EXS_DECLARE_NONCOPYABLE(SharedSyncObject);

	public:
		enum : Size_t
		{
			Max_Waiting_Threads_Num = Config::THR_Max_Total_Threads_Num
		};

		typedef ConcurrentDataPool<Thread*, Max_Waiting_Threads_Num> WaitingThreads;

	private:
		WaitingThreads  _waitingThreads;
		
	public:
		SharedSyncObject()
		: SyncObject()
		{ }

		// @override SyncObject::Abort
		EXS_LIBAPI_CORE virtual void Abort() override;

		// @override SyncObject::NotifyOne
		EXS_LIBAPI_CORE virtual void NotifyOne() override;

		// @override SyncObject::Broadcast
		EXS_LIBAPI_CORE virtual void Broadcast() override;
		
	private:
		// @override SyncObject::OnWaitBegin
		EXS_LIBAPI_CORE virtual bool OnWaitBegin(WaitContextInfo* waitInfo) override;

		// @override SyncObject::OnWaitEnd
		EXS_LIBAPI_CORE virtual void OnWaitEnd(WaitContextInfo* waitInfo) override;
	};


}


#endif /* __Exs_Core_SyncObjects_H__ */
