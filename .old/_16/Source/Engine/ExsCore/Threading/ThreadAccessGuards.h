
#pragma once

#ifndef __Exs_Core_ThreadAccessGuards_H__
#define __Exs_Core_ThreadAccessGuards_H__

#include "../Prerequisites.h"


namespace Exs
{


	class EXS_LIBCLASS_CORE ThreadAccessGuard
	{
		EXS_DECLARE_NONCOPYABLE(ThreadAccessGuard);

	protected:
		const char*  _funcName;

	public:
		ThreadAccessGuard(const char* funcName);
		
		virtual bool IsThreadAllowed(ThreadUID threadUID) const = 0;
		virtual bool IsThreadAllowed(ThreadHandle threadHandle) const = 0;

		void Enter();
		bool TryEnter();

	protected:
		void RaiseError();
	};


	class EXS_LIBCLASS_CORE SingleThreadAccessGuard : public ThreadAccessGuard
	{
		EXS_DECLARE_NONCOPYABLE(SingleThreadAccessGuard);

	protected:
		Enum    _allowedThread;

	public:
		SingleThreadAccessGuard(const char* funcName, ThreadUID threadUID);
		SingleThreadAccessGuard(const char* funcName, ThreadHandle threadHandle);
		
		virtual bool IsThreadAllowed(ThreadUID threadUID) const override;
		virtual bool IsThreadAllowed(ThreadHandle threadHandle) const override;
	};


	class EXS_LIBCLASS_CORE MultiThreadAccessGuard : public ThreadAccessGuard
	{
		EXS_DECLARE_NONCOPYABLE(MultiThreadAccessGuard);

	public:
		static const Size_t maxAllowedThreadsNum = 4;

	protected:
		std::array<Enum, maxAllowedThreadsNum>  _allowedThreads;

	public:
		MultiThreadAccessGuard(const char* funcName, std::initializer_list<ThreadUID>&& threadIDs);
		MultiThreadAccessGuard(const char* funcName, std::initializer_list<ThreadHandle>&& threadHandles);
		
		virtual bool IsThreadAllowed(ThreadUID threadUID) const override;
		virtual bool IsThreadAllowed(ThreadHandle threadHandle) const override;
	};




	namespace Internal
	{
	#if ( EXS_CONFIG_BASE_ENABLE_THREAD_ACCESS_RESTRICTION )

		inline void CheckThreadAccess(const char* funcName, ThreadUID threadUID)
		{
			SingleThreadAccessGuard accessGuard(funcName, threadUID);
			accessGuard.Enter();
		}

		inline void CheckThreadAccess(const char* funcName, std::initializer_list<ThreadUID>&& threadIDs)
		{
			MultiThreadAccessGuard accessGuard(funcName, std::forward< std::initializer_list<ThreadUID> >(threadIDs));
			accessGuard.Enter();
		}

		inline void CheckThreadAccess(const char* funcName, ThreadHandle threadHandle)
		{
			SingleThreadAccessGuard accessGuard(funcName, threadHandle);
			accessGuard.Enter();
		}

		inline void CheckThreadAccess(const char* funcName, std::initializer_list<ThreadHandle>&& threadHandles)
		{
			MultiThreadAccessGuard accessGuard(funcName, std::forward< std::initializer_list<ThreadHandle> >(threadHandles));
			accessGuard.Enter();
		}

	#endif
	}


}


#if ( EXS_CONFIG_BASE_ENABLE_THREAD_ACCESS_RESTRICTION )
#  define ExsDeclareRestrictedThreadAccess(allowedThreadsIDs) Internal::CheckThreadAccess(EXS_FUNC, allowedThreadsIDs)
#else
#  define ExsDeclareRestrictedThreadAccess(allowedThreadsIDs)
#endif


#endif /* __Exs_Core_ThreadAccessGuards_H__ */
