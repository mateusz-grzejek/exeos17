
#include "_Precompiled.h"
#include <ExsCore/Threading/Transceiver.h>
#include <ExsCore/Threading/ActiveThread.h>
#include <ExsCore/Threading/MessageDispatcher.h>
#include <ExsCore/Threading/ThreadAccessGuards.h>


namespace Exs
{


	Transceiver::Transceiver(ActiveThread* parentThread, MessageDispatcher* dispatcher)
	: _parentThread(parentThread)
	, _parentThreadHandle(parentThread->GetHandle())
	, _parentThreadUID(parentThread->GetUID())
	, _receiver(parentThread, this)
	, _sender(parentThread, this, dispatcher)
	, _isActive(false)
	{ }


	Transceiver::~Transceiver()
	{ }


	bool Transceiver::Initialize(const TransceiverInitData& initData, bool activate)
	{
		ExsDeclareRestrictedThreadAccess( this->GetParentThreadHandle() );

		// Save state provided by the dispatcher in initData.
		this->_state = initData.statePtr;

		if (activate)
		{
			this->FetchEnqueuedMessages();
			this->_isActive.store(true, std::memory_order_release);
		}

		return true;
	}


	void Transceiver::Release()
	{
		ExsDeclareRestrictedThreadAccess( this->GetParentThreadHandle() );
		
		this->_isActive.store(false, std::memory_order_release);
		this->_state = nullptr;
	}


	bool Transceiver::Activate()
	{
		if (!this->_state)
			ExsExceptionThrowEx(EXC_Invalid_Operation, "Attempt to activate an unregistered transceiver!");

		ExsEnterCriticalSection(this->_state->GetLock());
		{
			if (!this->IsActive())
			{
				this->FetchEnqueuedMessages();
				this->_isActive.store(true, std::memory_order_release);
			}
		}
		ExsLeaveCriticalSection();

		return true;
	}


	void Transceiver::Deactivate()
	{
		if (!this->_state)
			ExsExceptionThrowEx(EXC_Invalid_Operation, "Attempt to deactivate an unregistered transceiver!");

		ExsEnterCriticalSection(this->_state->GetLock());
		{
			if (this->IsActive())
				this->_isActive.store(false, std::memory_order_release);
		}
		ExsLeaveCriticalSection();
	}


	void Transceiver::FetchEnqueuedMessages()
	{
		// Retrieve the list of enqueued messages. This method is called with locked
		// state, so we may safely iterate over all messages and remove them from queue.
		auto& enqueuedMessageList = this->_state->GetEnqueuedMessages();

		for (auto& enqueuedMessage : enqueuedMessageList)
		{
			// Append message to receiver's inbox (special method for Transceiver).
			this->_receiver.AppendEnqueued(enqueuedMessage);
		}

		// Remove fetched messages from the queue.
		enqueuedMessageList.clear();
	}


}
