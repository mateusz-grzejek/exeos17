
#pragma once

#ifndef __Exs_Core_SystemThread_H__
#define __Exs_Core_SystemThread_H__

#include "../Prerequisites.h"
#include <thread>


namespace Exs
{


	struct SystemThreadInterface
	{
		std::function<void()>  onBegin;
		std::function<void()>  onEnd;
		std::function<void()>  onExecute;
		std::function<void()>  onRelease;
	};


	///<summary>
	/// Represents thread of execution at system level. Every instance of this class yields separate and independent thread,
	/// that executes using provided procedure as an entry point.
	///</summary>
	class EXS_LIBCLASS_CORE SystemThread
	{
		EXS_DECLARE_NONCOPYABLE(SystemThread);

	public:
		typedef std::function<void()> InternalProc;
		typedef std::thread::native_handle_type NativeHandle;

	protected:
		SystemThreadInterface           _interface;
		std::unique_ptr<std::thread>    _stdThreadObject;
		std::thread::id                 _stdThreadObjectID;

	public:
		SystemThread(SystemThreadInterface&& thrInterface)
		: _interface(std::forward<SystemThreadInterface>(thrInterface))
		{ }

		~SystemThread()
		{ }

		void RunAsChild()
		{
			if (this->_stdThreadObject)
			{
				ExsDebugInterrupt();
				return;
			}

			this->_stdThreadObject.reset(new std::thread(ThreadCaller, this));
			this->_stdThreadObjectID = this->_stdThreadObject->get_id();
		}

		void RunDetached()
		{
			if (this->_stdThreadObject)
			{
				ExsDebugInterrupt();
				return;
			}

			this->_stdThreadObject.reset(new std::thread(ThreadCaller, this));
			this->_stdThreadObjectID = this->_stdThreadObject->get_id();
			this->_stdThreadObject->detach();
		}

		void Join()
		{
			this->_stdThreadObject->join();
		}

		NativeHandle GetNativeHandle()
		{
			return this->_stdThreadObject->native_handle();
		}

		const std::thread::id& GetStdThreadID() const
		{
			return this->_stdThreadObjectID;
		}

		bool IsJoinable() const
		{
			return this->_stdThreadObject && this->_stdThreadObject->joinable();
		}

	protected:
		static void ThreadCaller(SystemThread* systemThread);
	};


}


#endif /* __Exs_Core_SystemThread_H__ */
