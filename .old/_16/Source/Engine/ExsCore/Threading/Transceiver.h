
#pragma once

#ifndef __Exs_Core_Transceiver_H__
#define __Exs_Core_Transceiver_H__

#include "Message.h"
#include "MessageReceiver.h"
#include "MessageSender.h"


namespace Exs
{


	struct TransceiverInitData;

	class ActiveThread;
	class TransceiverState;


	class EXS_LIBCLASS_CORE Transceiver final
	{
		EXS_DECLARE_NONCOPYABLE(Transceiver);
		
		friend class ActiveThread;
		friend class MessageDispatcher;
		friend class MessageReceiver;
		friend class MessageSender;

	private:
		ActiveThread*        _parentThread;
		ThreadHandle         _parentThreadHandle;
		ThreadUID            _parentThreadUID;
		MessageReceiver      _receiver;
		MessageSender        _sender;
		TransceiverState*    _state;
		std::atomic<bool>         _isActive;

	public:
		Transceiver(ActiveThread* parentThread, MessageDispatcher* dispatcher);
		~Transceiver();

		Result SendMessage(ThreadUID recipientUID, const CommonMessageHandle& message);
		Result SendMessage(ThreadUID recipientUID, const CommonMessageHandle& message, const MessageResponseRequest& responseRequest);

		ThreadHandle GetParentThreadHandle() const;
		ThreadUID GetParentThreadUID() const;

		bool IsActive() const;
		
	private:
		bool PostMessage(MessageHandle<Message> message);

		MessageHandle<Message> PeekMessage();
		MessageHandle<Message> WaitForMessage(const Microseconds& waitTimeout);

		bool Initialize(const TransceiverInitData& initData, bool activate);
		void Release();

		bool Activate();
		void Deactivate();

		void FetchEnqueuedMessages();
	};


	inline Result Transceiver::SendMessage(ThreadUID recipientUID, const CommonMessageHandle& message)
	{
		return this->_sender.SendMessage(recipientUID, message);
	}

	inline Result Transceiver::SendMessage(ThreadUID recipientUID, const CommonMessageHandle& message, const MessageResponseRequest& responseRequest)
	{
		return this->_sender.SendMessage(recipientUID, message, responseRequest);
	}

	inline ThreadHandle Transceiver::GetParentThreadHandle() const
	{
		return this->_parentThreadHandle;
	}

	inline ThreadUID Transceiver::GetParentThreadUID() const
	{
		return this->_parentThreadUID;
	}

	inline bool Transceiver::IsActive() const
	{
		return this->_isActive.load(std::memory_order_acquire);
	}
	
	inline bool Transceiver::PostMessage(MessageHandle<Message> message)
	{
		if (this->IsActive())
		{
			this->_receiver.PostMessage(message);
			return true;
		}

		return false;
	}

	inline MessageHandle<Message> Transceiver::PeekMessage()
	{
		return this->_receiver.PeekMessage();
	}

	inline MessageHandle<Message> Transceiver::WaitForMessage(const Microseconds& waitTimeout)
	{
		return this->_receiver.WaitForMessage(waitTimeout);
	}


}


#endif /* __Exs_Core_Transceiver_H__ */
