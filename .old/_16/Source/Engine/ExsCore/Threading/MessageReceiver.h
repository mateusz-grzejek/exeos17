
#pragma once

#ifndef __Exs_Core_MessageReceiver_H__
#define __Exs_Core_MessageReceiver_H__

#include "Message.h"
#include "SyncObjects.h"


namespace Exs
{


	class ActiveThread;


	///<summary>
	/// Generic message inbox used by receivers to store incoming messages. It contains two separated
	/// lists - one for normal messages and one for messages with high priority flag set.
	///</summary>
	class MessageInbox
	{
		EXS_DECLARE_NONCOPYABLE(MessageInbox);

	public:
		enum : Size_t
		{
			// Max number of times normal messages can be ignored in favor of high priority ones.
			Max_Common_Message_Ignore_Count = 16,

			// Default number of times normal messages can be ignored in favor of high priority ones.
			Default_Common_Message_Ignore_Count = 4
		};

		// Generic message queue - ref counted pointers to a base type of Message.
		typedef std::deque<CommonMessageHandle> MessageQueue;

	private:
		ThreadHandle    _parentThreadHandle;
		MessageQueue    _commonMessages;
		MessageQueue    _highPriorityMessages;
		Size_t          _commonMessageIgnoreCountLimit;
		Size_t          _currentCommonMessageIgnoreCount;

	public:
		MessageInbox(ThreadHandle parentThreadHandle, Size_t commonMessageIgnoreCountLimit = Default_Common_Message_Ignore_Count);
		~MessageInbox();
		
		///<summary>
		/// Puts message to appropriate queue within the inbox.
		///</summary>
		///<param name="message"> Message object. </param>
		void PutMessage(const CommonMessageHandle& message);
		
		///<summary>
		/// Retrieves next message using internal policy and returns it. Message is removed from its queue.
		///</summary>
		CommonMessageHandle TakeNextMessage();
		
		///<summary>
		/// Retrieves next message using internal policy and returns it. Message is removed from its queue.
		///</summary>
		void Clear();
		
		///<summary>
		/// Returns number of all messages stored in the inbox.
		///</summary>
		Size_t GetMessagesNum() const;
		
		///<summary>
		/// Returns true if there are no messages in the inbox or false otherwise.
		///</summary>
		bool IsEmpty() const;

	private:
		// Selects queue to fetch next message from.
		MessageQueue* _ChooseQueue();
	};


	inline Size_t MessageInbox::GetMessagesNum() const
	{
		return this->_commonMessages.size() + this->_highPriorityMessages.size();
	}

	inline bool MessageInbox::IsEmpty() const
	{
		return this->_commonMessages.empty() && this->_highPriorityMessages.empty();
	}


	///<summary>
	///</summary>
	class EXS_LIBCLASS_CORE MessageReceiver
	{
		EXS_DECLARE_NONCOPYABLE(MessageReceiver);

		friend class Transceiver;

	private:
		ActiveThread*          _parentThread;
		ThreadHandle           _parentThreadHandle;
		InternalSyncObject     _syncObj;
		MessageInbox           _inbox;
		std::atomic<Size_t>    _messagesNum;

	public:
		MessageReceiver(ActiveThread* parentThread, Transceiver* transceiver);
		~MessageReceiver();

		///<summary>
		///</summary>
		void PostMessage(CommonMessageHandle message);

		///<summary>
		///</summary>
		CommonMessageHandle PeekMessage();

		///<summary>
		///</summary>
		CommonMessageHandle WaitForMessage(const Microseconds& waitTimeout);
		
		///<summary>
		///</summary>
		ThreadHandle GetParentThreadHandle() const;
		
		///<summary>
		///</summary>
		Size_t GetEnqueuedMessagesNum() const;
		
		///<summary>
		///</summary>
		bool IsEmpty() const;

	friendapi:
		// Appends message to the inbox without performing any synchronization or validation.
		// Used by Transceiver to append enequeued messages during activation process.
		void AppendEnqueued(const CommonMessageHandle& message);

	private:
		// Appends message to the inbox.
		void _Append(const CommonMessageHandle& message);

		// Retrieves next message and removes it from the inbox.
		CommonMessageHandle _TakeNext();
	};
	

	inline ThreadHandle MessageReceiver::GetParentThreadHandle() const
	{
		return this->_parentThreadHandle;
	}

	inline Size_t MessageReceiver::GetEnqueuedMessagesNum() const
	{
		return this->_messagesNum.load(std::memory_order_relaxed);
	}

	inline bool MessageReceiver::IsEmpty() const
	{
		return this->GetEnqueuedMessagesNum() == 0;
	}

	inline void MessageReceiver::AppendEnqueued(const CommonMessageHandle& message)
	{
		this->_Append(message);
	}


}


#endif /* __Exs_Core_MessageReceiver_H__ */
