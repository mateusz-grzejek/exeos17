
#pragma once

#ifndef __Exs_Core_MessageDispatcher_H__
#define __Exs_Core_MessageDispatcher_H__

#include "Message.h"
#include "ThreadInternal.h"


namespace Exs
{

	
	class MessageDispatcher;
	class MessageSender;
	class TransceiverState;

	
	///<summary>
	/// Helper struct, defined in order to allow transceiver's registration process to be extendable.
	/// Initialized, valid instance of this type is created during registration and passed back to transceiver
	/// after its state is locked by registering dispatcher.
	///</summary>
	struct TransceiverInitData
	{
		TransceiverState* statePtr;
	};

	
	///<summary>
	/// Represents state of transceiver. It wraps all necessary components required for proper, synchronized
	/// state change and registration/unregistration of transceivers.
	///</summary>
	class TransceiverState : public Lockable<LightMutex, LockAccess::Strict>
	{
	public:
		typedef std::deque<CommonMessageHandle> EnqueuedMessages;

	private:
		stdx::sync_ref_counter       _refCounter;
		std::atomic<Transceiver*>    _transceiver;
		EnqueuedMessages             _enqueuedMessages;

	public:
		TransceiverState()
		: _refCounter(0)
		, _transceiver(nullptr)
		{ }

		bool AcquireUseRef()
		{
			Uint refsNum = this->_refCounter.increment_cnz();
			return refsNum > 0;
		}

		void SetOwnerRef()
		{
			Uint refsNum = this->_refCounter.increment();
			ExsDebugAssert( refsNum == 1 );
		}

		void ReleaseRef()
		{
			this->_refCounter.decrement();
		}

		void SetTransceiver(Transceiver* transceiver)
		{
			this->_transceiver.store(transceiver, std::memory_order_relaxed);
		}

		void ClearContent()
		{
			this->_enqueuedMessages.clear();
		}

		EnqueuedMessages& GetEnqueuedMessages()
		{
			return this->_enqueuedMessages;
		}

		Transceiver* GetTransceiver() const
		{
			return this->_transceiver.load(std::memory_order_relaxed);
		}

		stdx::ref_counter_value_t GetUseRef() const
		{
			return this->_refCounter.get_value();
		}
	};

	
	///<summary>
	///</summary>
	class TransceiverUseRef
	{
	private:
		TransceiverState*  _state;

	public:
		TransceiverUseRef(TransceiverState& state)
		: _state(state.AcquireUseRef() ? &state : nullptr)
		{ }

		~TransceiverUseRef()
		{
			if (this->_state != nullptr)
			{
				this->_state->ReleaseRef();
				this->_state = nullptr;
			}
		}

		operator bool() const
		{
			return this->_state != nullptr;
		}
	};


	///<summary>
	///</summary>
	class EXS_LIBCLASS_CORE MessageDispatcher
	{
		EXS_DECLARE_NONCOPYABLE(MessageDispatcher);
		
		friend class ActiveThread;
		friend class MessageSender;

	private:
		typedef std::array<TransceiverState, Config::THR_Max_Active_Threads_Num> TransceiverStateArray;

	private:
		TransceiverStateArray    _transceiverStateArray;
		std::atomic<Size_t>      _registeredTransceiversNum;

	public:
		MessageDispatcher();
		~MessageDispatcher();

		///<summary>
		/// Registers transceiver in the index, optionaly activating it.
		///</summary>
		Result RegisterTransceiver(Transceiver* transceiver, bool activate);
		
		///<summary>
		/// Unregisters transceiver. Active transceivers are automatically deactivated during unregistration.
		///</summary>
		Result UnregisterTransceiver(Transceiver* transceiver);
		
		///<summary>
		/// Dispatches message to specified recipient or enqueues it, if possible.
		///</summary>
		Result DispatchMessage(ThreadUID recipientUID, CommonMessageHandle message);
		
		///<summary>
		/// Returns number of currently registered transceivers.
		///</summary>
		Size_t GetRegisteredTransceiversNum() const;

		///<summary>
		/// Returns true if there is no registered transceivers or false otherwise.
		///</summary>
		bool IsEmpty() const;

	private:
		// Gains unique access to the state and tries to post message to the transceiver.
		// If that fails, it enqueues the message in the suspend queue (if the message has required flags set).
		Result _EnqueueMessage(CommonMessageHandle message, TransceiverState& transceiverState);

		// Retrieves state of transceiver with specified UID.
		TransceiverState& _GetTransceiverState(ThreadUID threadUID);
	};


	inline Size_t MessageDispatcher::GetRegisteredTransceiversNum() const
	{
		return this->_registeredTransceiversNum.load(std::memory_order_relaxed);
	}

	inline bool MessageDispatcher::IsEmpty() const
	{
		return this->GetRegisteredTransceiversNum() == 0;
	}

	inline TransceiverState& MessageDispatcher::_GetTransceiverState(ThreadUID threadUID)
	{
		ThreadIndex threadIndex = ThreadUtils::GetIndexFromUID(threadUID);
		return this->_transceiverStateArray[threadIndex];
	}


}


#endif /* __Exs_Core_MessageDispatcher_H__ */
