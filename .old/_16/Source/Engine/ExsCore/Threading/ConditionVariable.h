
#pragma once

#ifndef __Exs_Core_ConditionVariable_H__
#define __Exs_Core_ConditionVariable_H__

#include "../Prerequisites.h"


namespace Exs
{


	enum class ConditionVariableStatus : Enum
	{
		No_Timeout = static_cast<Enum>(std::cv_status::no_timeout),

		Timeout = static_cast<Enum>(std::cv_status::timeout)

	};


	template <class Lock_t>
	class ConditionVariableAdapter
	{
		EXS_DECLARE_NONCOPYABLE(ConditionVariableAdapter);

	public:
		typedef Lock_t LockType;
		typedef std::unique_lock<Lock_t> LockInterface;
		typedef ConditionVariableAdapter<Lock_t> MyType;

	private:
		LockInterface                _lock;
		std::condition_variable_any  _cv;

	public:
		explicit ConditionVariableAdapter(Lock_t& lock)
		: _lock(lock)
		{ }

		void Broadcast()
		{
			this->_cv.notify_all();
		}

		void NotifyOne()
		{
			this->_cv.notify_one();
		}

		void Wait()
		{
			this->_cv.wait(this->_lock);
		}

		template <class Predicate>
		void Wait(Predicate predicate)
		{
			this->_cv.wait(this->_lock, predicate);
		}

		template <class Period>
		ConditionVariableStatus WaitFor(const std_duration<Period>& duration)
		{
			auto result = this->_cv.wait_for (this->_lock, duration);

		#if ( EXS_COMPILER & EXS_COMPILER_MSVC ) && ( EXS_COMPILER < EXS_COMPILER_MSVC_2013 )
			return result ? ConditionVariableStatus::No_Timeout : ConditionVariableStatus::Timeout;
		#else
			return static_cast<ConditionVariableStatus>(result);
		#endif
		}

		template <class Period, class Predicate>
		bool WaitFor(const std_duration<Period>& duration, Predicate predicate)
		{
			return this->_cv.wait_for (this->_lock, duration, predicate);
		}

		ConditionVariableStatus WaitUntil(const TimePoint& time)
		{
			auto result = this->_cv.wait_until(this->_lock, time);
			
		#if ( EXS_COMPILER & EXS_COMPILER_MSVC ) && ( EXS_COMPILER < EXS_COMPILER_MSVC_2013 )
			return result ? ConditionVariableStatus::No_Timeout : ConditionVariableStatus::Timeout;
		#else
			return static_cast<ConditionVariableStatus>(result);
		#endif
		}

		template <class Predicate>
		bool WaitUntil(const TimePoint& time, Predicate predicate)
		{
			return this->_cv.wait_until(this->_lock, time, predicate);
		}
	};


}


#endif /* __Exs_Core_ConditionVariable_H__ */
