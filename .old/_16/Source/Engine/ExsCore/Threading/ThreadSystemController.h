
#pragma once

#ifndef __Exs_Core_ThreadSystemController_H__
#define __Exs_Core_ThreadSystemController_H__

#include "ConcurrentDataPool.h"
#include "ThreadCommon.h"
#include <stdx/assoc_array.h>


namespace Exs
{

	
	class ActiveThread;
	class TemporaryThread;
	class ThreadSystemController;


	///<summary>
	///</summary>
	class RegisteredThreads : public Lockable<LightMutex, LockAccess::Relaxed>
	{
	public:
		typedef stdx::assoc_array<ThreadUID, ThreadBaseInfo*> ThreadArray;
	
	private:
		ThreadArray    _threads;
	
	public:
		RegisteredThreads()
		{ }

		~RegisteredThreads()
		{ }
		
		///<summary>
		///</summary>
		void Add(ThreadBaseInfo* threadBaseInfo)
		{
			this->_threads.insert(threadBaseInfo->threadUID, threadBaseInfo);
		}
		
		///<summary>
		///</summary>
		void Remove(ThreadUID threadUID)
		{
			auto threadInfoRef = this->_threads.find(threadUID);
			ExsDebugAssert( threadInfoRef != this->_threads.end() );
			this->_threads.erase(threadInfoRef);
		}
		
		///<summary>
		///</summary>
		const ThreadArray& GetThreads() const
		{
			return this->_threads;
		}
	};
	
	
	///<summary>
	///</summary>
	class EXS_LIBCLASS_CORE ThreadSystemRegIndex
	{
	public:
		static const Size_t activeThreadsLimit = Config::THR_Max_Active_Threads_Num;
		static const Size_t temporaryThreadsLimit = Config::THR_Max_Temporary_Threads_Num;
		
		typedef ConcurrentDataPool<ThreadBaseInfo, activeThreadsLimit> ActiveThreadInfoHolder;
		typedef ConcurrentDataPool<ThreadBaseInfo, temporaryThreadsLimit> TemporaryThreadInfoHolder;

	private:
		ActiveThreadInfoHolder       _activeThreads;
		TemporaryThreadInfoHolder    _temporaryThreads;
		std::atomic<Size_t>          _activeThreadsNum;
		std::atomic<Size_t>          _temporaryThreadsNum;
		std::atomic<Size_t>          _totalRegisteredThreadsNum;
		std::atomic<Size_t>          _regCounter;
		std::atomic<Size_t>          _unregCounter;

	public:
		ThreadSystemRegIndex();
		~ThreadSystemRegIndex();
		
		///<summary>
		///</summary>
		ThreadBaseInfo* Register(const ThreadRegInfo& regInfo);

		///<summary>
		///</summary>
		bool Unregister(ThreadBaseInfo* baseInfo);
		
		///<summary>
		///</summary>
		bool CheckRegistrationState(ThreadUID threadUID) const;

		///<summary>
		///</summary>
		Size_t GetRegisteredThreadsNum() const;

		///<summary>
		///</summary>
		bool IsEmpty() const;

	private:
		ThreadBaseInfo* _RegisterActive();
		ThreadBaseInfo* _RegisterTemporary();

		bool _UnregisterActive(ThreadBaseInfo* baseInfo);
		bool _UnregisterTemporary(ThreadBaseInfo* baseInfo);
	};


	inline Size_t ThreadSystemRegIndex::GetRegisteredThreadsNum() const
	{
		return this->_totalRegisteredThreadsNum.load(std::memory_order_relaxed);
	}

	inline bool ThreadSystemRegIndex::IsEmpty() const
	{
		return this->GetRegisteredThreadsNum() == 0;
	}

	
	///<summary>
	///</summary>
	class EXS_LIBCLASS_CORE ThreadSystemController
	{
		EXS_DECLARE_NONCOPYABLE(ThreadSystemController);

		friend class Thread;

	private:
		RegisteredThreads       _registeredThreads;
		std::atomic<Size_t>     _registeredThreadsNum;
		ThreadSystemRegIndex    _threadsRegIndex;

	public:
		ThreadSystemController();
		virtual ~ThreadSystemController();
		
		///<summary>
		///</summary>
		const RegisteredThreads& GetRegisteredThreads() const;
		
		///<summary>
		///</summary>
		Size_t GetRegisteredThreadsNum() const;
		
		///<summary>
		///</summary>
		bool IsThreadRegistered(ThreadUID threadUID);
		
		///<summary>
		///</summary>
		bool IsThreadRegistered(ThreadHandle threadHandle);

	private:
		///<summary>
		///</summary>
		ThreadBaseInfo* RegisterThread(const ThreadRegInfo& regInfo);
		
		///<summary>
		///</summary>
		void UnregisterThread(ThreadBaseInfo* threadBaseInfo);
	};


	inline const RegisteredThreads& ThreadSystemController::GetRegisteredThreads() const
	{
		return this->_registeredThreads;
	}

	inline Size_t ThreadSystemController::GetRegisteredThreadsNum() const
	{
		return this->_registeredThreadsNum.load(std::memory_order_relaxed);
	}


}


#endif /* __Exs_Core_ThreadSystemController_H__ */
