
#pragma once

#ifndef __Exs_Core_MessageBase_H__
#define __Exs_Core_MessageBase_H__

#include "../Prerequisites.h"


#define EXS_MESSAGE_CODE_NCID_RESERVED    0x47000000
#define EXS_MESSAGE_CODE_CLASS_MASK       0x00FFFF00
#define EXS_MESSAGE_CODE_IID_MASK         0x000000FF


#define ExsEnumDeclareMessageCode(category, iid) \
	(EXS_MESSAGE_CODE_NCID_RESERVED | (((MessageCode)category & 0xFFFF) << 16) | (MessageCode)(iid & 0xFF))

#define ExsEnumGetMessageClass(messageCode) \
	(MessageClass)(((MessageCode)messageCode & EXS_MESSAGE_CODE_CLASS_MASK) >> 16)

#define ExsEnumValidateMessageCode(messageCode) \
	EXS_NCID_GET_RESERVED(messageCode) == EXS_MESSAGE_CODE_NCID_RESERVED


namespace Exs
{

	
	class Message;
	class Transceiver;

	///
	typedef Uint32 MessageCode;

	///
	typedef Uint16 MessageClass;

	///
	typedef Uint32 MessageUID;

	///
	template <class Message_type>
	using MessageHandle = stdx::intrusive_ptr<Message_type>;

	///
	typedef MessageHandle<Message> CommonMessageHandle;


	enum : MessageUID
	{
		MessageUID_Base = 0x00100000,

		MessageUID_Invalid = stdx::limits<MessageUID>::max_value
	};


	enum : MessageClass
	{
		MessageClass_Internal = 0xCC01,

		MessageClass_Undefined = 0xCC00
	};


}


#endif /* __Exs_Core_MessageBase_H__ */
