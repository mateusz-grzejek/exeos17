
#include "_Precompiled.h"
#include <ExsCore/Threading/ThreadSystemController.h>
#include <ExsCore/Threading/ActiveThread.h>
#include <ExsCore/Threading/ThreadInternal.h>


namespace Exs
{


	ThreadSystemRegIndex::ThreadSystemRegIndex()
	: _activeThreadsNum(0)
	, _temporaryThreadsNum(0)
	, _totalRegisteredThreadsNum(0)
	, _regCounter(0)
	, _unregCounter(0)
	{ }

	
	ThreadSystemRegIndex::~ThreadSystemRegIndex()
	{ }


	ThreadBaseInfo* ThreadSystemRegIndex::Register(const ThreadRegInfo& regInfo)
	{
		ThreadBaseInfo* baseInfoPtr = nullptr;

		if (regInfo.threadType == ThreadType::Active)
		{
			baseInfoPtr = this->_RegisterActive();

			if (baseInfoPtr != nullptr)
				ExsTraceInfo(TRC_Core_Threading, "Active thread 0x%" EXS_PFU32_HEX " has been registered at %u.", baseInfoPtr->threadUID, baseInfoPtr->regIndex);
			else
				ExsTraceInfo(TRC_Core_Threading, "Active thread reg error.");
		}
		else if (regInfo.threadType == ThreadType::Temporary)
		{
			baseInfoPtr = this->_RegisterTemporary();

			if (baseInfoPtr != nullptr)
				ExsTraceInfo(TRC_Core_Threading, "Temporary thread 0x%" EXS_PFU32_HEX " has been registered at %u.", baseInfoPtr->threadUID, baseInfoPtr->regIndex);
			else
				ExsTraceInfo(TRC_Core_Threading, "Temporary thread reg error.");
		}

		if (baseInfoPtr != nullptr)
		{
			this->_totalRegisteredThreadsNum.fetch_add(1, std::memory_order_release);
			this->_regCounter.fetch_add(1, std::memory_order_release);

			baseInfoPtr->threadHandle = baseInfoPtr;
			baseInfoPtr->threadType = regInfo.threadType;
			baseInfoPtr->threadUID = ExsEnumDeclareThreadUID(regInfo.threadType, baseInfoPtr->regIndex);
		}

		return baseInfoPtr;
	}


	bool ThreadSystemRegIndex::Unregister(ThreadBaseInfo* baseInfo)
	{
		bool threadUnregistered = false;

		if (baseInfo->threadType == ThreadType::Active)
		{
			threadUnregistered = this->_UnregisterActive(baseInfo);
		}
		else if (baseInfo->threadType == ThreadType::Temporary)
		{
			threadUnregistered = this->_UnregisterTemporary(baseInfo);
		}

		if (threadUnregistered)
		{
			this->_totalRegisteredThreadsNum.fetch_sub(1, std::memory_order_release);
			this->_unregCounter.fetch_add(1, std::memory_order_release);
		}

		return threadUnregistered;
	}


	bool ThreadSystemRegIndex::CheckRegistrationState(ThreadUID threadUID) const
	{
		ThreadIndex threadIndex = ThreadUtils::GetIndexFromUID(threadUID);
		ThreadType threadType = ThreadUtils::GetTypeFromUID(threadUID);

		bool registrationState = false;

		if (threadType == ThreadType::Active)
		{
			auto* entry = this->_activeThreads.GetEntry(threadIndex);
			registrationState = entry->IsAllocated() && (entry->data.threadUID == threadUID);
		}
		else if (threadType == ThreadType::Temporary)
		{
			auto* entry = this->_activeThreads.GetEntry(threadIndex);
			registrationState = entry->IsAllocated() && (entry->data.threadUID == threadUID);
		}
		else
		{
			ExsDebugInterrupt();
			registrationState = false;
		}

		return registrationState;
	}
	

	ThreadBaseInfo* ThreadSystemRegIndex::_RegisterActive()
	{
		Size_t regIndex = this->_activeThreads.Alloc();

		if (regIndex == Invalid_Position)
		{
			ExsTraceWarning(TRC_Core_Threading, "ThreadSystemRegIndex: reg(Active) -> failure, limit has been exceeded.");
			return nullptr;
		}

		auto& threadBaseInfo = this->_activeThreads[regIndex];
		threadBaseInfo.regIndex = truncate_cast<ThreadIndex>(regIndex);
		this->_activeThreadsNum.fetch_add(1, std::memory_order_release);

		return &threadBaseInfo;
	}
	

	ThreadBaseInfo* ThreadSystemRegIndex::_RegisterTemporary()
	{
		Size_t regIndex = this->_temporaryThreads.Alloc();

		if (regIndex == Invalid_Position)
		{
			ExsTraceWarning(TRC_Core_Threading, "ThreadSystemRegIndex: reg(Temporary) -> failure, limit has been exceeded.");
			return nullptr;
		}
		
		auto& threadBaseInfo = this->_temporaryThreads[regIndex];
		threadBaseInfo.regIndex = truncate_cast<ThreadIndex>(regIndex);
		this->_temporaryThreadsNum.fetch_add(1, std::memory_order_release);

		return &threadBaseInfo;
	}


	bool ThreadSystemRegIndex::_UnregisterActive(ThreadBaseInfo* baseInfo)
	{
		ExsDebugAssert( baseInfo->threadType == ThreadType::Active );
		auto& threadBaseInfoAtIndex = this->_activeThreads[baseInfo->regIndex];

		if (baseInfo != &threadBaseInfoAtIndex)
		{
			return false;
		}
		
		Uint32 regIndex = baseInfo->regIndex;

		baseInfo->regIndex = truncate_cast<ThreadIndex>(Invalid_Position);
		baseInfo->threadHandle = nullptr;
		baseInfo->threadType = ThreadType::Unknown;
		
		this->_activeThreadsNum.fetch_sub(1, std::memory_order_release);
		this->_activeThreads.Release(regIndex);

		return true;
	}


	bool ThreadSystemRegIndex::_UnregisterTemporary(ThreadBaseInfo* baseInfo)
	{
		ExsDebugAssert( baseInfo->threadType == ThreadType::Temporary );
		auto& threadBaseInfoAtIndex = this->_temporaryThreads[baseInfo->regIndex];

		if (baseInfo != &threadBaseInfoAtIndex)
		{
			return false;
		}
		
		Uint32 regIndex = baseInfo->regIndex;
		
		baseInfo->regIndex = truncate_cast<ThreadIndex>(Invalid_Position);
		baseInfo->threadHandle = nullptr;
		baseInfo->threadType = ThreadType::Unknown;
		
		this->_temporaryThreadsNum.fetch_sub(1, std::memory_order_release);
		this->_temporaryThreads.Release(regIndex);

		return true;
	}




	ThreadSystemController::ThreadSystemController()
	{ }


	ThreadSystemController::~ThreadSystemController()
	{ }


	// ActiveThreadStateLockedPtr ThreadSystemController::GetActiveThreadState(ThreadUID threadUID)
	// {
	// 	ExsEnterCriticalSection(this->_activeThreads.GetLock());
	// 	{
	// 		// Since access is locked, we can now safely query for state of thread with specified UID.
	// 
	// 		ActiveThreadState* threadState = this->_activeThreads.Find(threadUID);
	// 
	// 		if (threadState)
	// 		{
	// 			// We have our state, but we need it to be valid as long as client is using it!
	// 
	// 			// Active thread state may be removed from registry only if both locks are acquired (internal
	// 			// lock and threadState's lock). But after leaving this function internal lock will be released
	// 			// (we cannot hold it beyond the scope of this class!). This may result in returning pointer
	// 			// to state, that few moments later can be invalidated. So we go the tricky way: we have found
	// 			// requested thread state and internal lock is acquired. Now:
	// 
	// 			auto& stateLock = threadState->GetLock();
	// 
	// 			// Acquire second lock. It will always be acquired successfuly in terms of actions performed
	// 			// by this class - they are acquired always in the same order, so if internal lock was free,
	// 			// this one must also be available (i.e. no deadlock possibility).
	// 
	// 			stateLock.Lock();
	// 
	// 			// Now, simply return required state and adopt acquired lock. It will stay locked and even if
	// 			// thread wanted to remove it from list of active threads, he would have to wait for it to
	// 			// become available (because such process requires both locks - see UnregisterActiveThread()
	// 			// for details). Lock is released automatically, when returned guard is destroyed.
	// 
	// 			return ActiveThreadStateLockedPtr(threadState, stateLock, Tags::lockPolicyAdopt);
	// 		}
	// 	}
	// 	ExsLeaveCriticalSection();
	// 
	// 	return nullptr;
	// }


	bool ThreadSystemController::IsThreadRegistered(ThreadUID threadUID)
	{
		bool threadRegState = this->_threadsRegIndex.CheckRegistrationState(threadUID);
		return threadRegState;
	}


	bool ThreadSystemController::IsThreadRegistered(ThreadHandle threadHandle)
	{
		ThreadUID threadUID = ThreadUtils::GetUIDFromHandle(threadHandle);
		return this->IsThreadRegistered(threadUID);
	}


	ThreadBaseInfo* ThreadSystemController::RegisterThread(const ThreadRegInfo& regInfo)
	{
		ThreadBaseInfo* threadInfo = this->_threadsRegIndex.Register(regInfo);

		if (threadInfo != nullptr)
		{
			ExsEnterCriticalSection(this->_registeredThreads.GetLock());
			{
				this->_registeredThreads.Add(threadInfo);
				this->_registeredThreadsNum.fetch_add(1, std::memory_order_release);
			}
			ExsLeaveCriticalSection();
		}

		return threadInfo;
	}


	void ThreadSystemController::UnregisterThread(ThreadBaseInfo* threadInfo)
	{
		ThreadUID threadUID = threadInfo->threadUID;

		ExsEnterCriticalSection(this->_registeredThreads.GetLock());
		{
			this->_registeredThreads.Remove(threadUID);
			this->_registeredThreadsNum.fetch_add(1, std::memory_order_release);
		}
		ExsLeaveCriticalSection();

		this->_threadsRegIndex.Unregister(threadInfo);
	}


	// void ThreadSystemController::AddActiveThread(ActiveThread* thread, ActiveThreadState* acState)
	// {
	// 	ExsDebugAssert( (thread != nullptr) && (acState != nullptr) );
	// 	ExsDebugAssert( acState->GetUID() == thread->GetUID() );
	// 
	// 	ExsEnterCriticalSection(this->_activeThreads.GetLock());
	// 	{
	// 		this->_activeThreads.Add(acState);
	// 	}
	// 	ExsLeaveCriticalSection();
	// }
	// 
	// 
	// void ThreadSystemController::RemoveActiveThread(ActiveThread* thread, ActiveThreadState* acState)
	// {
	// 	ExsDebugAssert( (thread != nullptr) && (acState != nullptr) );
	// 	ExsDebugAssert( acState->GetUID() == thread->GetUID() );
	// 
	// 	ExsEnterCriticalSection(this->_activeThreads.GetLock());
	// 	{
	// 		// Additional internal lock on thread state is required for correct (and safe) retrieving
	// 		// of state objects. See GetActiveThreadState() for details.
	// 
	// 		ExsEnterNamedCriticalSection(thread_state_lock, acState->GetLock());
	// 		{
	// 			this->_activeThreads.Remove(acState);
	// 		}
	// 		ExsLeaveCriticalSection();
	// 	}
	// 	ExsLeaveCriticalSection();
	// }


}
