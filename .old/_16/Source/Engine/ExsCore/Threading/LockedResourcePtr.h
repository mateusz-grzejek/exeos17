
#pragma once

#ifndef __Exs_Core_LockedResourcePtr_H__
#define __Exs_Core_LockedResourcePtr_H__

#include "../Prerequisites.h"


namespace Exs
{

	
	///<summary>
	/// Represents pointer to a resource, that has some kind of unique access semantics (like common shared
	/// resource, used by multiple threads), containing raw pointer to such resource and an associated lock,
	/// that is used to control access to this resource. This class is used when certain operations cannot be
	/// performed in a scope of a single function (e.g. lock is acquired inside a function and should stay
	/// acquired after that function returns). It is an essential utility class used to ensure cross-function
	/// exception-safety.
	///</summary>
	template <class T, class Lock_t>
	class LockedResourcePtr
	{
	public:
		typedef Lock_t LockType;
		typedef LockedResourcePtr<T, Lock_t> MyType;

	private:
		std::unique_lock<Lock_t>  _lockGuard;
		T*                        _resourcePtr;

	public:
		LockedResourcePtr()
		: _resourcePtr(nullptr)
		{ }
		
		LockedResourcePtr(LockedResourcePtr&& source)
		: _resourcePtr(nullptr)
		{
			this->Swap(source);
		}
		
		LockedResourcePtr(std::nullptr_t)
		: _resourcePtr(nullptr)
		{ }
		
		LockedResourcePtr(T* resource, std::unique_lock<Lock_t>&& lockGuard)
		: _lockGuard(std::move(lockGuard))
		, _resourcePtr(resource)
		{ }

		LockedResourcePtr(T* resource, Lock_t& lock)
		: _lockGuard(lock)
		, _resourcePtr(resource)
		{ }
		
		LockedResourcePtr(T* resource, Lock_t& lock, const std::adopt_lock_t&)
		: _lockGuard(lock, std::adopt_lock)
		, _resourcePtr(resource)
		{ }
		
		LockedResourcePtr(T* resource, Lock_t& lock, const std::try_to_lock_t&)
		: _lockGuard(lock, std::try_to_lock)
		, _resourcePtr(_lockGuard.owns_lock() ? resource : nullptr)
		{ }

		~LockedResourcePtr()
		{
			this->Release();
		}

		MyType& operator=(LockedResourcePtr&& rhs)
		{
			if (this != &rhs)
				MyType(std::move(rhs)).Swap(*this);

			return *this;
		}

		explicit operator bool() const
		{
			return this->_resourcePtr != nullptr;
		}

		explicit operator T*() const
		{
			return this->_resourcePtr;
		}

		const T& operator*() const
		{
			ExsDebugAssert( this->_resourcePtr != nullptr );
			return *(this->_resourcePtr);
		}

		const T* operator->() const
		{
			ExsDebugAssert( this->_resourcePtr != nullptr );
			return this->_resourcePtr;
		}

		T* GetResourcePtr()
		{
			return this->_resourcePtr;
		}

		void Clear()
		{
			this->_resourcePtr = nullptr;
		}

		Lock_t* Unlock()
		{
			if (this->_lockGuard.owns_lock())
			{
				this->_lockGuard.unlock();
				this->_resourcePtr = nullptr;
			}
		}

		Lock_t* Release()
		{
			Lock_t* lock = nullptr;

			if (this->_lockGuard.owns_lock())
			{
				this->_lockGuard.unlock();
				this->_resourcePtr = nullptr;
				lock = this->_lockGuard.release();
			}

			return lock;
		}

		void Swap(LockedResourcePtr& other)
		{
			std::swap(this->_lockGuard, other._lockGuard);
			std::swap(this->_resourcePtr, other._resourcePtr);
		}
	};


}


#endif /* __Exs_Core_LockedResourcePtr_H__ */
