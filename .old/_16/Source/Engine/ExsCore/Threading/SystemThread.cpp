
#include "_Precompiled.h"
#include <ExsCore/Exception.h>
#include <ExsCore/Threading/SystemThread.h>


namespace Exs
{


	void SystemThread::ThreadCaller(SystemThread* systemThread)
	{
		auto& threadInterface = systemThread->_interface;
		ExsDebugAssert( threadInterface.onExecute );

		try
		{
			if (threadInterface.onBegin)
				threadInterface.onBegin();

			threadInterface.onExecute();
			
			if (threadInterface.onEnd)
				threadInterface.onEnd();
		}
		catch(const Exception& exception)
		{
			DefaultExceptionHandler(exception);
		}
		catch(const std::exception& exception)
		{
			DefaultExceptionHandler(exception);
		}
		catch(...)
		{
			ExsDebugInterrupt();
		}

		if (threadInterface.onRelease)
			threadInterface.onRelease();
	}


}
