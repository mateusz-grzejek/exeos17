
#pragma once

#ifndef __Exs_Core_ThreadLauncher_H__
#define __Exs_Core_ThreadLauncher_H__

#include "ActiveThread.h"
#include "../CoreEngineState.h"


namespace Exs
{


	class ActiveThread;
	class BackgroundThread;
	class LocalThread;
	class TemporaryThread;


	class ThreadLauncher
	{
	private:
		struct BackgroundThreadLauncher;
		struct LocalThreadLauncher;
		struct TemporaryThreadLauncher;
		struct UnknownThreadLauncher;

	public:
		template <class Thread_type, class... Args>
		static std::pair<Thread_type*, Result> Launch(CoreEngineStateRefHandle coreEngineState, const ThreadLaunchInfo& launchInfo, Args&&... args)
		{
			typedef typename stdx::conditional_type<
				std::is_base_of<BackgroundThread, Thread_type>::value,
				BackgroundThreadLauncher,
				typename stdx::conditional_type<
					std::is_base_of<LocalThread, Thread_type>::value,
					LocalThreadLauncher,
					typename stdx::conditional_type<
						std::is_base_of<TemporaryThread, Thread_type>::value,
						TemporaryThreadLauncher,
						UnknownThreadLauncher>::type>::type>::type LauncherType;

			return LauncherType::template Launch<Thread_type>(coreEngineState, launchInfo, std::forward<Args>(args)...);
		}

	private:
		template <class Launcher_t>
		struct BaseLaucher
		{
			static ActiveThread* ValidateCurrentThread()
			{
				Thread* currentThread = CurrentThread::GetThread();
				return dynamic_cast<ActiveThread*>(currentThread);
			}

			template <class Thread_type, class... Args>
			static std::pair<Thread_type*, Result> Launch(CoreEngineStateRefHandle coreEngineState, const ThreadLaunchInfo& launchInfo, Args&&... args)
			{
				if (!coreEngineState->threadSystemController)
					ExsExceptionThrowEx(EXC_Invalid_Operation, "Threading is disabled.");

				// Validate whether current thread can create a new thread. Only ActiveThread-based threads can do this.
				ActiveThread* currentThread = ValidateCurrentThread();

				if (currentThread == nullptr)
					return std::pair<Thread_type*, Result>(nullptr, ExsResult(RSC_Thread_Subthreads_Not_Allowed));

				// Register thread in the index. If it succeeds, we will get ThreadBaseInfo object containing base info for the thread.
				ThreadBaseInfo* threadBaseInfo = Thread::Register(coreEngineState->threadSystemController.get(), Launcher_t::threadType);

				if (threadBaseInfo == nullptr)
					return std::pair<Thread_type*, Result>(nullptr, ExsResult(RSC_Err_Failed));

				// Create actual thread object (launcher/thread-specific method).
				Thread_type* newThreadPtr = Launcher_t::template CreateThread<Thread_type>(currentThread, threadBaseInfo, coreEngineState, std::forward<Args>(args)...);
				std::unique_ptr<Thread_type> newThread { newThreadPtr };

				Result threadStartRes = Launcher_t::StartThread(currentThread, newThread.GetPtr(), launchInfo);

				if (threadStartRes != RSC_Success)
					return std::pair<Thread_type*, Result> { nullptr, threadStartRes };

				return std::pair<Thread_type*, Result> { newThread.Clear(), threadStartRes };
			}
		};

		struct BackgroundThreadLauncher : public BaseLaucher<BackgroundThreadLauncher>
		{
			static const ThreadType threadType = ThreadType::Active;

			template <class Thread_type, class... Args>
			static Thread_type* CreateThread(ActiveThread* currentThread, ThreadBaseInfo* threadBaseInfo, CoreEngineStateRefHandle coreEngineState, Args&&... args)
			{
				BackgroundThread* newThread = new Thread_type(currentThread->GetUID(), threadBaseInfo, coreEngineState, std::forward<Args>(args)...);
				return static_cast<Thread_type*>(newThread);
			}

			template <class Thread_type>
			static Result StartThread(ActiveThread* currentThread, Thread_type* thread, const ThreadLaunchInfo& launchInfo)
			{
				Thread::Start(thread, launchInfo);
				return ExsResult(RSC_Success);
			}
		};

		struct LocalThreadLauncher : public BaseLaucher<LocalThreadLauncher>
		{
			static const ThreadType threadType = ThreadType::Active;

			template <class Thread_type, class... Args>
			static Thread_type* CreateThread(ActiveThread* currentThread, ThreadBaseInfo* threadBaseInfo, CoreEngineStateRefHandle coreEngineState, Args&&... args)
			{
				LocalThread* newThread = new Thread_type(currentThread, threadBaseInfo, coreEngineState, std::forward<Args>(args)...);
				return static_cast<Thread_type*>(newThread);
			}

			template <class Thread_type>
			static Result StartThread(ActiveThread* currentThread, Thread_type* thread, const ThreadLaunchInfo& launchInfo)
			{
				if (launchInfo.readyStateSyncObject == nullptr)
				{
					InternalSyncObject readyStateSyncObject { currentThread };
					ThreadLaunchInfo localLaunchInfoObj { };
					localLaunchInfoObj.readyStateSyncObject = &readyStateSyncObject;
					localLaunchInfoObj.readyStateWaitImmediately = true;
					localLaunchInfoObj.setRunPermission = launchInfo.setRunPermission;

					return StartThread(currentThread, thread, localLaunchInfoObj);
				}
				else
				{
					Result startResult = Thread::Start(thread, launchInfo);

					if ((startResult == RSC_Success) && !launchInfo.readyStateWaitImmediately)
						thread->WaitForReadyState(launchInfo);

					return startResult;
				}
			}
		};

		struct TemporaryThreadLauncher : public BaseLaucher<TemporaryThreadLauncher>
		{
			static const ThreadType threadType = ThreadType::Temporary;

			template <class Thread_type, class... Args>
			static Thread_type* CreateThread(ActiveThread* currentThread, ThreadBaseInfo* threadBaseInfo, CoreEngineStateRefHandle coreEngineState, Args&&... args)
			{
				TemporaryThread* newThread = new Thread_type(threadBaseInfo, coreEngineState, std::forward<Args>(args)...);
				return static_cast<Thread_type*>(newThread);
			}

			template <class Thread_type>
			static Result StartThread(ActiveThread* currentThread, Thread_type* thread, const ThreadLaunchInfo& launchInfo)
			{
				Thread::Start(thread, launchInfo);
				return ExsResult(RSC_Success);
			}
		};

		struct UnknownThreadLauncher : public BaseLaucher<UnknownThreadLauncher>
		{
			static const ThreadType threadType = ThreadType::Unknown;

			template <class Thread_type, class... Args>
			static Thread_type* CreateThread(ActiveThread* currentThread, ThreadBaseInfo* threadBaseInfo, CoreEngineStateRefHandle coreEngineState, Args&&... args)
			{
				return nullptr;
			}

			template <class Thread_type>
			static Result StartThread(ActiveThread* currentThread, Thread_type* thread, const ThreadLaunchInfo& launchInfo)
			{
				return ExsResult(RSC_Err_Failed);
			}
		};
	};


}


#endif /* __Exs_Core_ThreadLauncher_H__ */
