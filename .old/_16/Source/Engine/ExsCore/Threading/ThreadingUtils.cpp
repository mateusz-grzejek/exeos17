
#include "_Precompiled.h"
#include <ExsCore/Threading/ThreadInternal.h>
#include <ExsCore/Threading/ThreadLocalStorage.h>


namespace Exs
{


	namespace Internal
	{
	
		std::pair<ThreadLocalStorage*, Uint> GetCurrentThreadLocalStorageUnchecked()
		{
			static EXS_ATTR_THREAD_LOCAL ThreadLocalStorage threadLocalStorage { 0 };
			++threadLocalStorage.controlBlock.uncheckedFetchCount;
			return std::pair<ThreadLocalStorage*, Uint>(&threadLocalStorage, threadLocalStorage.controlBlock.uncheckedFetchCount);
		}
	
	}


}
