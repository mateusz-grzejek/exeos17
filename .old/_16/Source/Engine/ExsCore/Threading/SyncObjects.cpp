
#include "_Precompiled.h"
#include <ExsCore/Threading/SyncObjects.h>
#include <ExsCore/Threading/Thread.h>
#include <ExsCore/Threading/ThreadAccessGuards.h>


namespace Exs
{


	void SyncObject::Interrupt(Thread* thread)
	{
		if (thread->GetWaitState() != nullptr)
		{
			ExsEnterCriticalSection(thread->GetWaitStateLock());
			{
				if (ThreadWaitState* waitState = thread->GetWaitState())
				{
					if (!waitState->HasInterruptFlag())
					{
						if (this->IsThreadWaiting(thread, waitState))
						{
							waitState->SetInterruptFlag(true);

							// Notify all threads! We don't know which thread would be signalled on NotifyOne(), so we must signal them all.
							// Thread, whose state was just marked will exit wait loop. others will sleep again.
							this->_syncCv.Broadcast();
						}
					}
				}
			}
			ExsLeaveCriticalSection();
		}
	}


	void SyncObject::MarkThread(Thread* thread, ThreadWaitState* waitState)
	{
		auto& threadWaitStateLock = thread->GetWaitStateLock();

		ExsEnterCriticalSection(thread->GetWaitStateLock());
		{
			waitState->savedThreadExecutionState = thread->SetExecutionState(ThreadExecutionState::Idle_Wait);
			waitState->threadObject = thread;
			waitState->threadWaitStateLock = &threadWaitStateLock;
			thread->SetWaitState(waitState);
		}
		ExsLeaveCriticalSection();
	}


	void SyncObject::ResetThread(Thread* thread, ThreadWaitState* waitState)
	{
		ExsEnterCriticalSection(thread->GetWaitStateLock());
		{
			thread->SetWaitState(nullptr);
			waitState->threadObject = nullptr;
			waitState->threadWaitStateLock = nullptr;
			thread->SetExecutionState(waitState->savedThreadExecutionState);
		}
		ExsLeaveCriticalSection();
	}


	bool SyncObject::IsThreadWaiting(Thread* thread, ThreadWaitState* waitState)
	{
		if (waitState->threadObject == thread)
		{
			ExsRuntimeAssert( thread->GetWaitState() == waitState );
			return true;
		}

		return false;
	}


	ThreadWaitState* SyncObject::GetThreadWaitState(Thread* thread)
	{
		return thread->GetWaitState();
	}

	


	void InternalSyncObject::Abort()
	{
		this->SetAbortFlag(true);
		this->_syncCv.NotifyOne();
	}
	
	
	void InternalSyncObject::NotifyOne()
	{
		this->_syncCv.NotifyOne();
	}


	void InternalSyncObject::Broadcast()
	{
		this->_syncCv.Broadcast();
	}


	bool InternalSyncObject::OnWaitBegin(WaitContextInfo* waitInfo)
	{
		ExsDeclareRestrictedThreadAccess( this->_owningThread->GetUID() );

		waitInfo->threadObject = this->_owningThread;
		this->SetAbortFlag(false);
		this->MarkThread(this->_owningThread, waitInfo->threadWaitState);

		return true;
	}


	void InternalSyncObject::OnWaitEnd(WaitContextInfo* waitInfo)
	{
		ExsDeclareRestrictedThreadAccess( this->_owningThread->GetUID() );

		this->ResetThread(this->_owningThread, waitInfo->threadWaitState);
		waitInfo->threadObject = nullptr;
	}

	


	void UniqueSyncObject::Abort()
	{
		this->SetAbortFlag(true);
		this->_syncCv.NotifyOne();
	}
	
	
	void UniqueSyncObject::NotifyOne()
	{
		this->_syncCv.NotifyOne();
	}


	void UniqueSyncObject::Broadcast()
	{
		this->_syncCv.Broadcast();
	}


	bool UniqueSyncObject::OnWaitBegin(WaitContextInfo* waitInfo)
	{
		if (this->GetWaitingThread() != nullptr)
			ExsExceptionThrowEx(EXC_Invalid_Operation, "Two or more threads tried to wait on UniqueSyncObject.");

		Thread* currentThread = CurrentThread::GetThread();
		waitInfo->threadObject = currentThread;

		this->SetAbortFlag(false);
		this->SetCurrentWaitingThread(currentThread);
		this->MarkThread(currentThread, waitInfo->threadWaitState);

		return true;
	}


	void UniqueSyncObject::OnWaitEnd(WaitContextInfo* waitInfo)
	{
		this->ResetThread(waitInfo->threadObject, waitInfo->threadWaitState);
		this->SetCurrentWaitingThread(nullptr);

		waitInfo->threadObject = nullptr;
	}




	void SharedSyncObject::Abort()
	{
		this->SetAbortFlag(true);
		this->_syncCv.Broadcast();
	}


	void SharedSyncObject::NotifyOne()
	{
		this->_syncCv.NotifyOne();
	}
	
	
	void SharedSyncObject::Broadcast()
	{
		this->_syncCv.Broadcast();
	}


	bool SharedSyncObject::OnWaitBegin(WaitContextInfo* waitInfo)
	{
		Thread* currentThread = CurrentThread::GetThread();
		Size_t threadIndex = this->_waitingThreads.Alloc();

		if (threadIndex == Invalid_Position)
			ExsExceptionThrowEx(EXC_Invalid_Operation, "Cannot synchronize another thread, limit exceeded.");

		waitInfo->threadObject = currentThread;
		waitInfo->xdata[0] = threadIndex;

		this->SetAbortFlag(false);
		this->MarkThread(currentThread, waitInfo->threadWaitState);

		return true;
	}


	void SharedSyncObject::OnWaitEnd(WaitContextInfo* waitInfo)
	{
		Size_t threadIndex = waitInfo->xdata[0];

		this->ResetThread(waitInfo->threadObject, waitInfo->threadWaitState);
		this->_waitingThreads.Release(threadIndex);

		waitInfo->threadObject = nullptr;
		waitInfo->xdata[0] = 0;
	}


}
