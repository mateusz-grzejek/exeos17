
#pragma once

#ifndef __Exs_Core_Message_H__
#define __Exs_Core_Message_H__

#include "MessageBase.h"
#include "SyncObjects.h"


namespace Exs
{

	
	class InternalSyncObject;
	class MessageDispatcher;
	class MessageInbox;
	class MessageResponseState;
	class MessageSender;


	///<summary>
	///</summary>
	enum MessageAttribFlags : Enum
	{
		//
		MessageAttrib_High_Priority = 0x8000,

		//
		MessageAttrib_No_Suspend = 0x0010,

		//
		MessageAttrib_Set_Response_Request = 0x0001,

		//
		MessageAttrib_Wait_For_Missing_Recipient = 0x0002,

		//
		MessageAttrib_Default = 0
	};


	template <MessageCode>
	struct MessageCodeType
	{
		typedef Message Type;
	};


	inline MessageUID GenerateMessageUniqueID()
	{
		static std::atomic<Uint32> messageUIDConter { MessageUID_Base + 1 };
		Uint32 messageIndex = messageUIDConter.fetch_add(1, std::memory_order_relaxed);
		ExsDebugAssert( messageIndex < stdx::limits<Uint32>::max_value );
		return static_cast<MessageUID>(messageIndex);
	}


	template <MessageCode Code, class... Args>
	inline MessageHandle<typename MessageCodeType<Code>::Type> CreateMessage(stdx::mask<MessageAttribFlags> attribFlags, Args&&... args)
	{
		typedef typename MessageCodeType<Code>::Type MessageType;
		return MessageHandle<MessageType>(new MessageType(Code, GenerateMessageUniqueID(), attribFlags, std::forward<Args>(args)...));
	}


	struct MessageResponseRequest
	{
		Milliseconds waitTimeout;
		bool waitForMissingRecipient;
	};


	///<summary>
	///</summary>
	class EXS_LIBCLASS_CORE Message : public stdx::ref_counted_base<stdx::atomic_ref_counter>
	{
		EXS_DECLARE_NONCOPYABLE(Message);

		friend class MessageDispatcher;
		friend class MessageSender;

	protected:
		MessageCode                              _code; // Content of the message.
		MessageUID                               _uid; // Message ID, generated automatically. It is guaranteed to be unique.
		stdx::mask<MessageAttribFlags>           _attribFlags; // Current state of the message.
		std::unique_ptr<MessageResponseState>    _responseState; // Response state object, used to synchronize responding process.
		
	public:
		Message(MessageCode code, MessageUID uid, stdx::mask<MessageAttribFlags> attribFlags);

		virtual ~Message();

		template <class Message_type>
		Message_type* GetAs();

		template <class Message_type>
		const Message_type* GetAs() const;

		bool WaitForResponse();

		MessageCode GetCode() const;
		MessageClass GetClass() const;
		MessageUID GetUID() const;

		bool HasResponseRequestSet() const;
		bool IsHighPriorityMessage() const;
		bool IsSuspendableMessage() const ;

	private:
		///
		void ClearResponseRequest();

		///
		virtual bool CreateResponseState(InternalSyncObject* syncObject, const Milliseconds& waitTimeout);
	};


	inline MessageCode Message::GetCode() const
	{
		return this->_code;
	}

	inline MessageClass Message::GetClass() const
	{
		return ExsEnumGetMessageClass(this->_code);
	}

	inline MessageUID Message::GetUID() const
	{
		return this->_uid;
	}

	inline bool Message::HasResponseRequestSet() const
	{
		return this->_attribFlags.is_set(MessageAttrib_Set_Response_Request);
	}

	inline bool Message::IsHighPriorityMessage() const
	{
		return this->_attribFlags.is_set(MessageAttrib_High_Priority);
	}

	inline bool Message::IsSuspendableMessage() const
	{
		return !this->_attribFlags.is_set(MessageAttrib_No_Suspend);
	}

	inline void Message::ClearResponseRequest()
	{
		this->_attribFlags.unset(MessageAttrib_Set_Response_Request);
	}


	///<summary>
	///</summary>
	template < class Content_type,
	           class Response_content_type,
	           bool Has_content = !std::is_void<Content_type>::value,
	           bool Has_response = !std::is_void<Response_content_type>::value>
	class MessageWithContent;


	template <class Content_type, class Response_content_type>
	class MessageWithContent<Content_type, Response_content_type, true, false> : public Message
	{
	public:
		typedef Content_type ContentType;

	private:
		Content_type  _content;

	public:
		template <class... ContentArgs>
		MessageWithContent(MessageCode code, MessageUID uid, stdx::mask<Enum> attribFlags, ContentArgs&&... contentArgs)
		: Message(code, uid, attribFlags)
		, _content(std::forward<ContentArgs>(contentArgs)...)
		{ }

		virtual ~MessageWithContent()
		{ }

		const Content_type& GetContent() const
		{
			return this->_content;
		}
	};


#define ExsRegisterMessageTypeWithContent(typeName, contentType) \
	typedef MessageWithContent<contentType, void> typeName;

#define ExsRegisterMessageTypeWithContentAndResponse(typeName, contentType, responseType) \
	typedef MessageWithContent<contentType, responseType> typeName;

#define ExsRegisterMessageTypeWithResponse(typeName, responseType) \
	typedef MessageWithContent<void, responseType> typeName;

#define ExsRegisterMessageCode(code, type) \
	template <> struct MessageCodeType<code> { typedef type Type; };


};


#endif /* __Exs_Core_Message_H__ */
