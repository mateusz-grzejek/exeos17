
#include "_Precompiled.h"
#include <ExsCore/Threading/ThreadInternal.h>
#include <ExsCore/Threading/ThreadLocalStorage.h>


namespace Exs
{


	TLSValidationResult ThreadLocalStorage::Validate(ThreadLocalStorage* tlsPtr)
	{
		if (tlsPtr->controlBlock.validationKey != Config::TLS_Internal_Validation_Key)
			return TLSValidationResult::Invalid_Key;

		if (tlsPtr->thread == nullptr)
			return TLSValidationResult::Invalid_Thread_Object;
		
		return TLSValidationResult::No_Error;
	}


	TLSValidationResult ThreadLocalStorage::ValidateCurrent()
	{
		ThreadLocalStorage* tlsPtr = CurrentThread::GetLocalStorage();
		TLSValidationResult validationResult = Validate(tlsPtr);

		return validationResult;
	}


}
