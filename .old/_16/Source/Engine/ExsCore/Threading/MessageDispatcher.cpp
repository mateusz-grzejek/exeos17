
#include "_Precompiled.h"
#include <ExsCore/Threading/MessageDispatcher.h>
#include <ExsCore/Threading/ThreadAccessGuards.h>
#include <ExsCore/Threading/Transceiver.h>
#include <ExsCore/Threading/ThreadInternal.h>


namespace Exs
{


	MessageDispatcher::MessageDispatcher()
	: _registeredTransceiversNum(0)
	{ }


	MessageDispatcher::~MessageDispatcher()
	{ }


	Result MessageDispatcher::RegisterTransceiver(Transceiver* transceiver, bool activate)
	{
		ExsDebugAssert( transceiver != nullptr );
		ExsDeclareRestrictedThreadAccess( transceiver->GetParentThreadHandle() );

		Enum threadUID = transceiver->GetParentThreadUID();
		TransceiverState& transceiverState = this->_GetTransceiverState(threadUID);

		// Lock transceiver state to prevent any modifications during registration process.
		ExsEnterCriticalSection(transceiverState.GetLock());
		{
			Transceiver* currentTransceiver = transceiverState.GetTransceiver();

			// Only one thread with given UID may be active at the same time. If tranceiver object is not empty,
			// there is another thread with the same UID, that has been already registered or *something*
			// went wrong during unregistration and state was not properly cleared.

			if (currentTransceiver != nullptr)
				return ExsResultEx(RSC_Err_Registration_State_Mismatch, "Acquired state has non-NULL transceiver set.");

			// Fill data required by transceiver for its intiialization.
			TransceiverInitData initData { &transceiverState };

			// Perform transceiver initialization. If initialization fails, abort registration process and leave
			// state unmodified. All content passed inside initData should remain unchanged in this case.
			// NOTE: Initialization of transceiver is done with its state locked!
			if (!transceiver->Initialize(initData, activate))
				return ExsResult(RSC_Err_Transceiver_Activation_Failed);

			transceiverState.SetOwnerRef();
			transceiverState.SetTransceiver(transceiver);

			this->_registeredTransceiversNum.fetch_add(1, std::memory_order_relaxed);
		}
		ExsLeaveCriticalSection();

		return ExsResult(RSC_Success);
	}


	Result MessageDispatcher::UnregisterTransceiver(Transceiver* transceiver)
	{
		ExsDebugAssert( transceiver != nullptr );
		ExsDeclareRestrictedThreadAccess( transceiver->GetParentThreadHandle() );

		Enum threadUID = transceiver->GetParentThreadUID();
		TransceiverState& transceiverState = this->_GetTransceiverState(threadUID);
		
		// Lock transceiver state to prevent any modifications during unregistration process.
		ExsEnterCriticalSection(transceiverState.GetLock());
		{
			Transceiver* currentTransceiver = transceiverState.GetTransceiver();

			// Check if transceiver being currently stored is the same object as the specified one. If they are different,
			// something is veeery wrong. Either state has been modified externally or unregistration has been already done.

			if (currentTransceiver == nullptr)
				return ExsResultEx(RSC_Err_Registration_State_Mismatch, "Acquired state has a NULL transceiver set.");
			else if (currentTransceiver != transceiver)
				return ExsResultEx(RSC_Err_Registration_State_Mismatch, "Specified transceiver and acquired one do not match.");

			this->_registeredTransceiversNum.fetch_sub(1, std::memory_order_relaxed);

			// Release "owner reference" - all remaining references belong to active dispatch executions.
			transceiverState.ReleaseRef();

			// Because of performance, we do not lock state when dispatching a message. Instead, every dispatch
			// increments active refs counter of a state using Cnz variant. If successful - transceiver is fetched
			// and used. Since we release our (owner) reference (see above), we must now wait for all active dispatch
			// executions to complete.

			for (Uint32 yn = 0; transceiverState.GetUseRef() > 0; ++yn)
			{
				YieldExecution(yn);
			}

			// Use ref must be always zero at this point.
			ExsDebugAssert( transceiverState.GetUseRef() == 0 );

			// Clear transceiver object.
			transceiverState.SetTransceiver(nullptr);

			// Clear messages, that may be still in the queue.
			transceiverState.ClearContent();

			// Release transceiver. It will be also automatically deactivated if it is still active at this point.
			transceiver->Release();

		}
		ExsLeaveCriticalSection();

		return ExsResult(RSC_Success);
	}


	Result MessageDispatcher::DispatchMessage(ThreadUID recipientUID, CommonMessageHandle message)
	{
		// Retrieve TransceiverState object of the recipient.
		TransceiverState& transceiverState = this->_GetTransceiverState(recipientUID);

		if (transceiverState.GetUseRef() > 0)
		{
			// Acquire use ref. It will be acquired only if transceiver is registered.
			TransceiverUseRef transceiverUseRef { transceiverState };

			if (transceiverUseRef)
			{
				Transceiver* transceiver = transceiverState.GetTransceiver();
				ExsDebugAssert( transceiver != nullptr );

				if (transceiver->PostMessage(message))
					return ExsResult(RSC_Success);
			}
		}
		
		// NOTE: Use ref is released!
		return this->_EnqueueMessage(message, transceiverState);
	}


	Result MessageDispatcher::_EnqueueMessage(CommonMessageHandle message, TransceiverState& transceiverState)
	{
		ExsEnterCriticalSection(transceiverState.GetLock());
		{
			if (transceiverState.GetUseRef() > 0)
			{
				if (Transceiver* transceiver = transceiverState.GetTransceiver())
				{
					if (transceiver->PostMessage(message))
						return ExsResult(RSC_Success);
				}
			}

			if (message->IsSuspendableMessage())
			{
				// Message is not suspendable - it was supposed to be delivered *only* directly to the recipient. Since it is
				// either deactivated or not present at all, message must be discarded.
				return ExsResult(RSC_Msg_Message_Discarded);
			}
			else
			{
				// Enqueue message, so it can be fetched by recipient after it activates itself again.
				auto& enqueuedMessages = transceiverState.GetEnqueuedMessages();
				enqueuedMessages.push_back(message);
			}
		}
		ExsLeaveCriticalSection();

		return ExsResult(RSC_Msg_Message_Enqueued);
	}


}
