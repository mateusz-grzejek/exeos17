
#include "_Precompiled.h"
#include <ExsCore/Singleton.h>
#include <ExsCore/Threading/Message.h>
#include <ExsCore/Threading/MessageResponse.h>
#include <ExsCore/Threading/MessageSender.h>
#include <ExsCore/Threading/ActiveThread.h>


namespace Exs
{


#if ( EXS_CONFIG_BASE_ENABLE_DEBUG )


	class MessageInstancesChecker : public Singleton<MessageInstancesChecker, SingletonType::Synchronized>
	{
	protected:
		std::atomic<Size_t>    _instancesCounter;

	public:
		MessageInstancesChecker()
		: _instancesCounter(0)
		{ }

		~MessageInstancesChecker()
		{
			ExsDebugAssert( this->GetCounter() == 0 );
		}

		static void Increment()
		{
			MessageInstancesChecker* checker = GetSingletonPtr();
			checker->_instancesCounter.fetch_add(1, std::memory_order_seq_cst);
		}

		static void Decrement()
		{
			MessageInstancesChecker* checker = GetSingletonPtr();
			checker->_instancesCounter.fetch_sub(1, std::memory_order_seq_cst);
		}

		static Size_t GetCounter()
		{
			MessageInstancesChecker* checker = GetSingletonPtr();
			return checker->_instancesCounter.load(std::memory_order_seq_cst);
		}
	};


#endif


	Message::Message(MessageCode code, MessageUID uid, stdx::mask<MessageAttribFlags> attribFlags)
	: _code(code)
	, _uid(uid)
	, _attribFlags(attribFlags)
	{
	#if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
		MessageInstancesChecker::Increment();
	#endif
	}


	Message::~Message()
	{
	#if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
		MessageInstancesChecker::Decrement();
	#endif
	}


	bool Message::WaitForResponse()
	{
		// Check response state. It is created by the Sender during intial part of the sending process.
		if (MessageResponseState* responseState = this->_responseState.get())
		{
			if (!responseState->IsResponseReady())
			{
				// Retrieve sync object. It will always be a valid object, since response state is initialized with a lock
				// which belongs to the current thread's sender object and is cleared only by this method (when timeout
				// occures and request is cancelled).

				InternalSyncObject* syncObj = responseState->GetSyncObject();
				auto& responseSyncLock = syncObj->GetLock();

				// Lock response state.
				ExsEnterCriticalSection(responseSyncLock);
				{
					if (!responseState->IsResponseReady())
					{
						syncObj->WaitFor(
							responseState->GetWaitTimeout(),
							[&responseState]() -> bool {
								return responseState->IsResponseReady();
						});
					}

					if (responseState->IsResponseReady())
						return true;

					// If response is not ready, either timeout has occured or wait has been aborted.
					// Cancel the request. This will also clear syncObject used by responseState.
					responseState->Cancel();
				}
				ExsLeaveCriticalSection();
			}
		}

		return false;
	}


	bool Message::CreateResponseState(InternalSyncObject* syncObject, const Milliseconds& waitTimeout)
	{
		return false;
	}


}
