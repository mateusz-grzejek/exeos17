
#pragma once

#ifndef __Exs_Core_MessageSender_H__
#define __Exs_Core_MessageSender_H__

#include "MessageBase.h"
#include "SyncObjects.h"


namespace Exs
{


	struct MessageResponseRequest;
	
	class ActiveThread;
	class MessageDispatcher;


	class EXS_LIBCLASS_CORE MessageSender
	{
		EXS_DECLARE_NONCOPYABLE(MessageSender);
		
	protected:
		ActiveThread*              _parentThread;
		ThreadHandle               _parentThreadHandle;
		InternalSyncObject         _syncObj;
		MessageDispatcher*         _dispatcher;
		std::atomic<MessageUID>    _currentMessageUID;

	public:
		MessageSender(ActiveThread* parentThread, Transceiver* transceiver, MessageDispatcher* dispatcher);
		virtual ~MessageSender();
		
		///<summary>
		///</summary>
		Result SendMessage(ThreadUID recipientUID, const CommonMessageHandle& message);
		
		///<summary>
		///</summary>
		Result SendMessage(ThreadUID recipientUID, const CommonMessageHandle& message, const MessageResponseRequest& responseRequest);
		
		///<summary>
		///</summary>
		ThreadHandle GetParentThreadHandle() const;
		
		///<summary>
		///</summary>
		MessageUID GetCurrentMessageUID() const;

	private:
		//
		void SetCurrentMessageUID(MessageUID messageID);
	};


	inline void MessageSender::SetCurrentMessageUID(MessageUID messageID)
	{
		this->_currentMessageUID.store(messageID, std::memory_order_relaxed);
	}

	inline ThreadHandle MessageSender::GetParentThreadHandle() const
	{
		return this->_parentThreadHandle;
	}

	inline MessageUID MessageSender::GetCurrentMessageUID() const
	{
		return this->_currentMessageUID.load(std::memory_order_relaxed);
	}


}


#endif /* __Exs_Core_MessageSender_H__ */
