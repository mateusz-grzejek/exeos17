
#include "_Precompiled.h"
#include <ExsCore/CoreEngineState.h>
#include <ExsCore/Threading/ActiveThread.h>
#include <ExsCore/Threading/MessageDispatcher.h>
#include <ExsCore/Threading/ThreadLocalStorage.h>
#include <ExsCore/Threading/ThreadSystemController.h>
#include <ExsCore/Threading/Transceiver.h>
#include <ExsCore/Threading/ThreadInternal.h>


namespace Exs
{


	ActiveThread::ActiveThread(	CoreEngineStateRefHandle coreEngineState,
	                            ThreadUID parentUID,
	                            ThreadBaseInfo* threadBaseInfo,
	                            const std::string& name)
	: Thread(coreEngineState, threadBaseInfo, name)
	, _parentUID(parentUID)
	, _messageDispatcher(coreEngineState->messageDispatcher)
	{ }


	ActiveThread::~ActiveThread()
	{ }


	Transceiver* ActiveThread::GetTransceiver()
	{
		return this->_transceiver.get();
	}


	Result ActiveThread::SendMessage(ThreadUID recipientUID, const CommonMessageHandle& message)
	{
		return this->_transceiver->SendMessage(recipientUID, message);
	}


	Result ActiveThread::SendMessage(ThreadUID recipientUID, const CommonMessageHandle& message, const MessageResponseRequest& responseRequest)
	{
		return this->_transceiver->SendMessage(recipientUID, message, responseRequest);
	}


	void ActiveThread::OnInit()
	{
		Thread::OnInit();

		this->_activeState = std::make_unique<ActiveThreadState>(this, this->_baseInfo);
		
		if (this->_messageDispatcher)
		{
			this->_transceiver = std::make_unique<Transceiver>(this, this->_messageDispatcher.get());
			this->_messageDispatcher->RegisterTransceiver(this->_transceiver.get(), true);
		}
	}


	void ActiveThread::OnRelease()
	{
		ExsTraceInfo(TRC_Core_Threading, "Sending SIG_EXIT to child threads...");

		if (this->_activeState->GetChildThreadsNum() > 0)
		{
			// Acuire lock of active state. Active state holds list of child threads and is locked each time
			// a child is added/removed. This allows us to safely iterate over all currently working child
			// threads. Note, that some of them may already be at release phase, but calling Exit() on such
			// thread object is guaranteed to have no side effects.

			auto& activeStateLock = this->_activeState->GetLock();
			ExsEnterCriticalSection(activeStateLock);
			{
				auto& childThreads = this->_activeState->GetChildThreads();
				for (auto& childThread : childThreads)
				{
					childThread->Exit();
					ExsTraceInfo(TRC_Core_Threading, "%s has been notified.", childThread->GetTextID());
				}
			}
			ExsLeaveCriticalSection();
		}

		ExsTraceInfo(TRC_Core_Threading, "Waiting for child threads to complete...");

		// Wait until all child threads finish they execution. Possible improvement: is spinning in a loop really a good
		// idea? Waiting on CV may be a more efficient solution, but on the other hand, threads interruption should be
		// performed as fast as possible, so this loop should quit almost immediately.

		while (this->_activeState->GetChildThreadsNum() > 0)
		{
			this->SleepFor(GetDefaultThreadWaitTimeout());
		}

		ExsTraceInfo(TRC_Core_Threading, "All child threads have completed.");

		if (this->_messageDispatcher)
		{
			ExsDebugAssert( this->_transceiver );
			this->_messageDispatcher->UnregisterTransceiver(this->_transceiver.get());
			this->_transceiver = nullptr;
		}

		Thread::OnRelease();
	}


	ActiveThread::ChildThreadRef ActiveThread::AddChild(LocalThread* thread)
	{
		ExsEnterCriticalSection(this->_activeState->GetLock());
		{
			ChildThreadRef childRef = this->_activeState->AddChild(thread);
			return childRef;
		}
		ExsLeaveCriticalSection();
	}


	void ActiveThread::RemoveChild(ChildThreadRef threadRef, LocalThread* thread)
	{
		ExsEnterCriticalSection(this->_activeState->GetLock());
		{
			this->_activeState->RemoveChild(threadRef, thread);
		}
		ExsLeaveCriticalSection();
	}


	CommonMessageHandle ActiveThread::PeekNextMessage()
	{
		if (!this->_messageDispatcher)
		{
			ExsExceptionThrowEx(EXC_Invalid_Operation,
				"Message dispatcher has not been received. Thread cannot communicate via MessageSystem.");
		}

		return this->_transceiver->PeekMessage();
	}


	CommonMessageHandle ActiveThread::WaitForNextMessage()
	{
		if (!this->_messageDispatcher)
		{
			ExsExceptionThrowEx(EXC_Invalid_Operation,
				"Message dispatcher has not been received. Thread cannot communicate via MessageSystem.");
		}

		return this->_transceiver->WaitForMessage(DurationCast<DurationPeriod::Microsecond>(Milliseconds(Config::THR_Default_Message_Wait_Timeout)));
	}


	CommonMessageHandle ActiveThread::WaitForNextMessage(const Microseconds& timeout)
	{
		if (!this->_messageDispatcher)
		{
			ExsExceptionThrowEx(EXC_Invalid_Operation,
				"Message dispatcher has not been received. Thread cannot communicate via MessageSystem.");
		}

		return this->_transceiver->WaitForMessage(timeout);
	}

	


	BackgroundThread::BackgroundThread(	CoreEngineStateRefHandle coreEngineState,
	                                    ThreadUID parentUID,
	                                    ThreadBaseInfo* threadBaseInfo,
	                                    const std::string& name)
	: ActiveThread(coreEngineState, parentUID, threadBaseInfo, name)
	{ }


	BackgroundThread::~BackgroundThread()
	{ }

	


	LocalThread::LocalThread( CoreEngineStateRefHandle coreEngineState,
	                          ActiveThread* parentThread,
	                          ThreadBaseInfo* threadBaseInfo,
	                          const std::string& name)
	: ActiveThread(coreEngineState, parentThread->GetUID(), threadBaseInfo, name)
	, _parentThread(parentThread)
	{ }


	LocalThread::~LocalThread()
	{ }


	void LocalThread::OnSystemThreadBegin()
	{
		if (this->_parentThread)
		{
			// Add self to the parent list. Note, that this method effectively acquires ATS lock.
			this->_selfParentRef = this->_parentThread->AddChild(this);
		}
	}


	void LocalThread::OnSystemThreadEnd()
	{
		if (this->_parentThread)
		{
			// Remove self from the parent list. Note, that this method effectively acquires ATS lock.
			this->_parentThread->RemoveChild(this->_selfParentRef, this);
		}
	}


}
