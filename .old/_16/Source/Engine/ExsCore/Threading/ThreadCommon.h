
#pragma once

#ifndef __Exs_Core_ThreadBase_H__
#define __Exs_Core_ThreadBase_H__

#include "../Prerequisites.h"


namespace Exs
{


	class SyncObject;
	class ThreadSystemController;


	struct ThreadRegInfo
	{
		///
		ThreadType threadType = ThreadType::Unknown;
	};

	
	///<summary>
	/// Contains data used to control the process of launching new threads.
	///</summary>
	struct ThreadLaunchInfo
	{
		// Sync object used for ready state synchronization.
		SyncObject* readyStateSyncObject = nullptr;

		// Indicates whether synchronization should be performed immediately after thread is created.
		bool readyStateWaitImmediately = false;

		//
		bool setRunPermission = true;
	};


}


#endif /* __Exs_Core_ThreadBase_H__ */
