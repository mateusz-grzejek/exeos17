
#pragma once

#ifndef __Exs_Core_ThreadLocalStorage_H__
#define __Exs_Core_ThreadLocalStorage_H__

#include "../Prerequisites.h"


namespace Exs
{


	class ActiveThread;
	class ProfilerModule;


	enum class TLSValidationResult : Enum
	{
		No_Error,
		Invalid_Key,
		Invalid_Thread_Object
	};


	enum : Size_t
	{
		TLS_UDI_Profiler_Module
	};


	typedef std::list< std::function<void()> > TLSExtDataReleaseList;

	
	struct ThreadLocalStorage
	{
	public:
		struct ControlBlock
		{
			Uint32 validationKey;

			Uint uncheckedFetchCount;
		};

		ControlBlock controlBlock;

		Thread* thread;

		ThreadHandle threadHandle;

		ThreadUID threadUID;

		ProfilerModule* profilerModule;

		void* singletons[Config::TLS_Ext_Data_Array_Size];

		void* extData[Config::TLS_Singleton_Array_Size];

		TLSExtDataReleaseList* extDataReleaseList;

	public:
		EXS_LIBAPI_CORE static TLSValidationResult Validate(ThreadLocalStorage* tlsPtr);
		EXS_LIBAPI_CORE static TLSValidationResult ValidateCurrent();
	};


	namespace TLS
	{

		inline void* GetSingleton(Enum singletonID)
		{
			ExsDebugAssert( singletonID < Config::TLS_Singleton_Array_Size );
			ThreadLocalStorage* tls = CurrentThread::GetLocalStorage();
			return tls->singletons[singletonID];
		}

		template <class Type>
		inline Type* GetSingletonAs(Enum singletonID)
		{
			ExsDebugAssert( singletonID < Config::TLS_Singleton_Array_Size );
			ThreadLocalStorage* tls = CurrentThread::GetLocalStorage();
			return pointer_cast<Type*>(tls->singletons[singletonID]);
		}

		inline void* GetExtData(Enum extDataID)
		{
			ExsDebugAssert( extDataID < Config::TLS_Ext_Data_Array_Size );
			ThreadLocalStorage* tls = CurrentThread::GetLocalStorage();
			return tls->extData[extDataID];
		}

		template <class Type>
		inline Type* GetExtDataAs(Enum extDataID)
		{
			ExsDebugAssert( extDataID < Config::TLS_Ext_Data_Array_Size );
			ThreadLocalStorage* tls = CurrentThread::GetLocalStorage();
			return pointer_cast<Type*>(tls->extData[extDataID]);
		}

		inline bool IsExtDataSet(Enum extDataID)
		{
			return GetExtData(extDataID) != nullptr;
		}

		inline void SetExtData(Enum extDataID, void* dataPtr, std::function<void(void*)> releaseProc = nullptr)
		{
			ExsDebugAssert( extDataID < Config::TLS_Ext_Data_Array_Size );

			ThreadLocalStorage* tls = CurrentThread::GetLocalStorage();
			tls->extData[extDataID] = dataPtr;

			if (releaseProc)
				tls->extDataReleaseList->push_back(std::bind(releaseProc, dataPtr));
		}

	};


}


#endif /* __Exs_Core_ThreadLocalStorage_H__ */
