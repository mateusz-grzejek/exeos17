
#pragma once

#ifndef __Exs_Core_ActiveThreadInfo_H__
#define __Exs_Core_ActiveThreadInfo_H__

#include "ThreadCommon.h"


namespace Exs
{


	class ActiveThread;
	class LocalThread;
	class ThreadSystemController;

	
	///<summary>
	///</summary>
	class ActiveThreadState : public Lockable<LightMutex, LockAccess::Strict>
	{
	public:
		typedef std::list<LocalThread*> ChildThreads;
		typedef ChildThreads::iterator ChildThreadRef;

	private:
		ActiveThread*        _thread;
		ThreadUID            _uid;
		ThreadHandle         _handle;
		ChildThreads         _childThreads;
		std::atomic<Size_t>  _childThreadsNum;

	public:
		ActiveThreadState(ActiveThread* thread, ThreadBaseInfo* baseInfo)
		: _thread(thread)
		, _uid(baseInfo->threadUID)
		, _handle(baseInfo->threadHandle)
		{ }

		~ActiveThreadState()
		{ }

		ChildThreadRef AddChild(LocalThread* thread)
		{
			ChildThreadRef childRef = this->_childThreads.insert(this->_childThreads.end(), thread);
			this->_childThreadsNum.fetch_add(1, std::memory_order_release);

			return childRef;
		}

		void RemoveChild(ChildThreadRef threadRef, LocalThread* thread)
		{
			ExsDebugAssert(*threadRef == thread);
			this->_childThreadsNum.fetch_sub(1, std::memory_order_release);
			this->_childThreads.erase(threadRef);
		}

		const ChildThreads& GetChildThreads() const
		{
			return this->_childThreads;
		}

		Size_t GetChildThreadsNum() const
		{
			return this->_childThreadsNum.load(std::memory_order_acquire);
		}

		ThreadHandle GetHandle() const
		{
			return this->_handle;
		}

		ActiveThread* GetThread() const
		{
			return this->_thread;
		}

		ThreadUID GetUID() const
		{
			return this->_uid;
		}
	};


}


#endif /* __Exs_Core_ActiveThreadInfo_H__ */
