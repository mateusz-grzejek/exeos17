
#pragma once

#ifndef __Exs_Core_ThreadInternal_H__
#define __Exs_Core_ThreadInternal_H__

#include "../Prerequisites.h"


#define EXS_THREAD_UID_NCID_RESERVED    0x7C000000
#define EXS_THREAD_UID_TYPE_MASK        0x00FF0000
#define EXS_THREAD_UID_INDEX_MASK       0x0000FFFF


#define ExsEnumDeclareThreadUID(threadType, index) \
	(EXS_THREAD_UID_NCID_RESERVED | ((static_cast<Enum>(threadType) & 0xFF) << 16) | (static_cast<Enum>(index) & 0xFFFF))

#define ExsEnumGetThreadIndex(threadUID) \
	static_cast<ThreadIndex>(threadUID & EXS_THREAD_UID_INDEX_MASK)

#define ExsEnumGetThreadType(threadUID) \
	static_cast<ThreadType>((threadUID & EXS_THREAD_UID_TYPE_MASK) >> 16)

#define ExsEnumValidateThreadUID(threadUID) \
	EXS_NCID_GET_RESERVED(threadUID) == EXS_THREAD_UID_NCID_RESERVED


namespace Exs
{


	struct ThreadLocalStorage;


	namespace Config
	{

		enum : Uint32
		{
			TLS_Internal_Validation_Key = EXS_THREAD_UID_NCID_RESERVED | 0x00370188
		};

	}

	
	namespace ThreadUtils
	{

		inline ThreadIndex GetIndexFromUID(Enum threadUID)
		{
			ExsDebugAssert( ExsEnumValidateThreadUID(threadUID) );
			ThreadIndex threadIndex = ExsEnumGetThreadIndex(threadUID);
			ExsDebugAssert( threadIndex < Config::THR_Max_Total_Threads_Num );
			return threadIndex;
		}

		inline ThreadType GetTypeFromUID(Enum threadUID)
		{
			ExsDebugAssert( ExsEnumValidateThreadUID(threadUID) );
			ThreadType threadType = ExsEnumGetThreadType(threadUID);
			ExsDebugAssert( (threadType == ThreadType::Active) || (threadType == ThreadType::Temporary) );
			return threadType;
		}

		inline Enum GetUIDFromHandle(ThreadHandle threadHandle)
		{
			ThreadBaseInfo* threadBaseInfo = pointer_cast<ThreadBaseInfo*>(threadHandle);
			return threadBaseInfo->threadUID;
		}

		inline ThreadIndex GetIndexFromHandle(ThreadHandle threadHandle)
		{
			Enum threadUID = GetUIDFromHandle(threadHandle);
			ThreadIndex threadIndex = GetIndexFromUID(threadUID);
			return threadIndex;
		}

	};


	namespace Internal
	{

		std::pair<ThreadLocalStorage*, Uint> GetCurrentThreadLocalStorageUnchecked();

	}


}


#endif /* __Exs_Core_ThreadInternal_H__ */
