
#include "_Precompiled.h"
#include <ExsCore/CoreEngineState.h>
#include <ExsCore/TraceOutput.h>
#include <ExsCore/Plugins/PluginSystem.h>
#include <ExsCore/Threading/MessageDispatcher.h>
#include <ExsCore/Threading/ThreadSystemController.h>


namespace Exs
{


	CoreEngineStateRefHandle CreateCoreEngineState(stdx::mask<CoreEngineFeatureFlags> featureMask)
	{
		auto state = std::make_shared<CoreEngineState>();

		ExsEnterCriticalSection(state->accessLock);
		{
			if (featureMask.is_set(CoreEngineFeature_Threads))
			{
				// ThreadSystemController, responsible for general management of threading system.
				state->threadSystemController = std::make_shared<ThreadSystemController>();

				// Messages are available only with enabled threading.
				if (featureMask.is_set(CoreEngineFeature_Messages))
				{
					// MessageDispatcher, used to control inter-thread communication.
					state->messageDispatcher = std::make_shared<MessageDispatcher>();
				}
			}

			if (featureMask.is_set(CoreEngineFeature_Threads))
			{
				// PluginSystem, used to load, unload and manage plugins.
				state->pluginSystem = std::make_shared<PluginSystem>();
			}

			// Save also pointer to shared TraceOutputController object.
			state->traceOutputController = GetTraceOutputController();
		}
		ExsLeaveCriticalSection();

		return CoreEngineStateRefHandle(state);
	}


}
