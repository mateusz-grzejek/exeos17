
#pragma once

#ifndef __Exs_Core_PlatformUtils_H__
#define __Exs_Core_PlatformUtils_H__

#include "Prerequisites.h"


namespace Exs
{


	class PlatformUtils
	{
	#if ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_WIN32 )
	public:
		EXS_LIBAPI_CORE static bool Win32CheckLastError();
		EXS_LIBAPI_CORE static bool Win32CheckResult(HRESULT result);
	#endif
	};


}


#endif /* __Exs_Core_PlatformUtils_H__ */
