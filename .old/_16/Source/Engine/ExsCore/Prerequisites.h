
#pragma once

#ifndef __Exs_Core_Prerequsites_H__
#define __Exs_Core_Prerequsites_H__

#include "Base/BaseConfig.h"
#include "Base/DebugDefs.h"

#include "Base/CommonDefs.h"
#include "Base/CommonTypes.h"
#include "Base/Chrono.h"
#include "Base/HashCode.h"
#include "Base/Result.h"
#include "Base/ResultCodes.inl"
#include "Base/TagObjects.h"

#include "Base/DebugOutput.h"
#include "Base/TraceCommon.h"
#include "Base/ThreadBaseDefs.h"
#include "Base/CommonLockTypes.h"

#endif /* __Exs_Core_Prerequsites_H__ */
