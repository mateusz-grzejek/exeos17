
#pragma once

#ifndef __Exs_Core_CoreEngineState_H__
#define __Exs_Core_CoreEngineState_H__

#include "SharedData.h"


namespace Exs
{


	class TraceOutputController;

	ExsDeclareRefPtrClass(MessageDispatcher);
	ExsDeclareRefPtrClass(PluginSystem);
	ExsDeclareRefPtrClass(ThreadSystemController);

	ExsDeclareSharedDataRefHandle(CoreEngineState);


	///<summary>
	///</summary>
	enum CoreEngineFeatureFlags : Enum
	{
		//
		CoreEngineFeature_None = 0,

		//
		CoreEngineFeature_Messages = 0x0002,

		//
		CoreEngineFeature_Plugins = 0x0008,

		//
		CoreEngineFeature_Threads = 0x0001,

		//
		CoreEngineFeature_All = 0xFFFF
	};


	///<summary>
	/// Core state data used by the engine. It contains instances of objects which provide functionality
	/// of the most important modules of the engine. Instance of this structure is created and initialized
	/// at the very start of the application and persists as long as there are active references to it.
	/// [NOTE: If detached (background) threads are used, completion of main thread will not cause this data
	/// to be released - it will be done by the last thread of the application.]
	///</summary>
	struct CoreEngineState : public SharedData
	{
		// Message dispatcher objects, responsible for inter-thread communication.
		MessageDispatcherRefPtr messageDispatcher;

		// Plugin system, used to load and manage plugins.
		PluginSystemRefPtr pluginSystem;

		// Core component of threading system. Responsible for thread management.
		ThreadSystemControllerRefPtr threadSystemController;

		//
		TraceOutputController* traceOutputController;
	};


	EXS_LIBAPI_CORE CoreEngineStateRefHandle CreateCoreEngineState(stdx::mask<CoreEngineFeatureFlags> featureMask = CoreEngineFeature_All);


}


#endif /* __Exs_Core_CoreEngineState_H__ */
