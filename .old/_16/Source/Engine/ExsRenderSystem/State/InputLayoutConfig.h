
#pragma once

#ifndef __Exs_RenderSystem_InputLayoutConfig_H__
#define __Exs_RenderSystem_InputLayoutConfig_H__

#include "CommonIADefs.h"


namespace Exs
{


	ExsDeclareRefPtrClass(IndexBuffer);


	///<summary>
	/// Represents primitive topology configuration which describes how primitves are assembled during IA stage.
	///</summary>
	struct IAPrimitiveTopologyDescription
	{
		// Specifies primitve topology, which represents a basic type of primitives.
		PrimitiveTopology primitiveTopology;

		// If primitiveTopology is set to Tesselation_Patch, this value specifies how many control points contains each
		// single patch. For any other topology this value is ignored.
		Uint32 tesselationPatchControlPointsNum;
	};


	///<summary>
	///</summary>
	struct InputLayoutConfiguration
	{
		struct VertexAttributeDesc
		{
			// State of this attribute (active/inactive).
			IAVertexAttribState state = IAVertexAttribState::Inactive;

			// @IAVertexAttribDescription::name
			std::string name;

			// @IAVertexAttribDescription::format
			VertexAttribFormat format;

			// @IAVertexAttribDescription::streamIndex
			IAVertexStreamIndex streamIndex;

			// @IAVertexAttribDescription::relativeOffset
			Uint relativeOffset;
		};

		struct VertexStreamDesc
		{
			// State of this stream (active/inactive).
			IAVertexStreamState state = IAVertexStreamState::Inactive;

			// @IAVertexStreamDescription::rate
			IAVertexStreamRate rate;

			// @IAVertexStreamDescription::stride
			Uint stride;
		};

		// Array for vertex attributes configuration.
		typedef VertexSpecArray<IAVertexAttribLocation, VertexAttributeDesc, Config::RS_IA_Max_Vertex_Input_Attribs_Num> VertexAttribArray;

		// Array for vertex streams configuration.
		typedef VertexSpecArray<IAVertexStreamIndex, VertexStreamDesc, Config::RS_IA_Max_Vertex_Input_Streams_Num> VertexStreamArray;

		VertexAttribArray attribArray;

		VertexStreamArray streamArray;

		IAPrimitiveTopologyDescription primitiveTopologyDescription;
	};


}


#endif /* __Exs_RenderSystem_InputLayoutConfig_H__ */
