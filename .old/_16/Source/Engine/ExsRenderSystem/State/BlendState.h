
#pragma once

#ifndef __Exs_RenderSystem_BlendState_H__
#define __Exs_RenderSystem_BlendState_H__

#include "BlendConfig.h"


namespace Exs
{

	
	///<summary>
	///</summary>
	class BlendStateDescriptor : public RSStateDescriptor
	{
		EXS_DECLARE_NONCOPYABLE(BlendStateDescriptor);

	protected:
		BlendConfiguration    _configuration;

	public:
		///<param name="renderSystem"> Pointer to the RenderSystem instance. </param>
		///<param name="baseObjectID"> Unique ID assigned to this descriptor. </param>
		///<param name="descriptorPID">  </param>
		///<param name="configuration">  </param>
		BlendStateDescriptor( RenderSystem* renderSystem,
		                      RSBaseObjectID baseObjectID,
		                      RSStateDescriptorPID descriptorPID,
		                      const BlendConfiguration& configuration)
		: RSStateDescriptor(renderSystem, baseObjectID, 0, RSStateDescriptorType::Blend, descriptorPID)
		, _configuration(configuration)
		{ }

		const BlendConfiguration& GetConfiguration() const
		{
			return this->_configuration;
		}

		static bool ValidateConfiguration(const BlendConfiguration& configuration)
		{
			return true;
		}
	};


}


#endif /* __Exs_RenderSystem_BlendState_H__ */
