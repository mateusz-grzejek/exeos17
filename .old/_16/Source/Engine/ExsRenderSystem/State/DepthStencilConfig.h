
#pragma once

#ifndef __Exs_RenderSystem_DepthStencilConfig_H__
#define __Exs_RenderSystem_DepthStencilConfig_H__

#include "CommonStateDefs.h"


namespace Exs
{


	///<summary>
	/// Specifies whether depth data is written to the depth-stencil buffer.
	///</summary>
	enum class DepthWriteMask : Enum
	{
		// Enables writing depth data to the depth-stencil buffer.
		All = 1,

		// Disables writing depth data to the depth-stencil buffer.
		Zero = 0
	};


	///<summary>
	/// Identifies operations that can be performed on stencil data during stencil part of the depth-stencil test.
	///</summary>
	enum class StencilOp : Enum
	{
		// Stencil data is left unchanged.
		Keep,

		// Stencil data is replaced with reference value.
		Replace,

		// Stencil data is inverted.
		Invert,

		// Stencil data is set to 0.
		Zero,

		// Stencil data is incremented by 1 and clamped.
		Incr_Clamp,

		// Stencil data is incremented by 1 and wrapped around.
		Incr_Wrap,

		// Stencil data is decremented by 1 and clamped.
		Decr_Clamp,

		// Stencil data is decremented by 1 and wrapped around.
		Decr_Wrap
	};


	///<summary>
	///</summary>
	struct DepthStencilConfiguration
	{
		struct DepthSettings
		{
			ComparisonFunction comparisonFunc;

			DepthWriteMask writeMask;
		};

		struct StencilSettings
		{
			struct Face
			{
				ComparisonFunction comparisonFunc;

				StencilOp opStencilFail;

				StencilOp opStencilPassDepthFail;

				StencilOp opStencilPassDepthPass;
			};

			Face backFace;

			Face frontFace;

			Uint8 refValue;

			Uint8 writeMask;
		};

		ActiveState depthTestState;

		DepthSettings depthSettings;

		ActiveState stencilTestState;

		StencilSettings stencilSettings;
	};


}


#endif /* __Exs_RenderSystem_DepthStencilConfig_H__ */
