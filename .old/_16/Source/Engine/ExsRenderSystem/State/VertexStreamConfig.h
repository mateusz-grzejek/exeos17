
#pragma once

#ifndef __Exs_RenderSystem_VertexStreamConfig_H__
#define __Exs_RenderSystem_VertexStreamConfig_H__

#include "CommonIADefs.h"


namespace Exs
{


	ExsDeclareRSClassObjHandle(VertexStreamStateDescriptor);


	///<summary>
	///</summary>
	struct VertexStreamConfiguration
	{
		struct VertexBufferBinding
		{
			// @IAVertexBufferBinding::vertexBuffer
			VertexBufferHandle buffer = nullptr;

			// @IAVertexBufferBinding::vertexDataBaseOffset
			Uint dataBaseOffset = Invalid_Offset;

			// @IAVertexBufferBinding::vertexDataStride
			Uint dataStride = Invalid_Length;
		};

		struct IndexBufferBinding
		{
			//
			IndexBufferHandle buffer;

			//
			Uint dataBaseOffset;
		};

		typedef VertexSpecArray<IAVertexStreamIndex, VertexBufferBinding, Config::RS_IA_Max_Vertex_Input_Streams_Num> VertexBufferBindingArray;

		//
		IndexBufferBinding indexBufferBinding;

		//
		VertexBufferBindingArray vertexBufferBindingArray;
	};


}


#endif /* __Exs_RenderSystem_VertexStreamConfig_H__ */
