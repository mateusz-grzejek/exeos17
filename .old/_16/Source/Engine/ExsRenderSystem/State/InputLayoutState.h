
#pragma once

#ifndef __Exs_RenderSystem_InputLayoutState_H__
#define __Exs_RenderSystem_InputLayoutState_H__

#include "InputLayoutConfig.h"


namespace Exs
{


	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM InputLayoutStateDescriptor : public RSStateDescriptor
	{
		EXS_DECLARE_NONCOPYABLE(InputLayoutStateDescriptor);

	protected:
		InputLayoutConfiguration    _configuration;

	public:
		///<param name="renderSystem"> Pointer to the RenderSystem instance. </param>
		///<param name="baseObjectID"> Unique ID assigned to this descriptor. </param>
		///<param name="descriptorPID">  </param>
		///<param name="configuration">  </param>
		InputLayoutStateDescriptor( RenderSystem* renderSystem,
		                            RSBaseObjectID baseObjectID,
		                            RSStateDescriptorPID descriptorPID,
		                            const InputLayoutConfiguration& configuration)
		: RSStateDescriptor(renderSystem, baseObjectID, 0, RSStateDescriptorType::Input_Layout, descriptorPID)
		, _configuration(configuration)
		{ }

		const InputLayoutConfiguration& GetConfiguration() const
		{
			return this->_configuration;
		}

		static bool ValidateConfiguration(const InputLayoutConfiguration& configuration)
		{
			return true;
		}
	};


}


#endif /* __Exs_RenderSystem_InputLayoutState_H__ */
