
#pragma once

#ifndef __Exs_RenderSystem_GraphicsShaderConfig_H__
#define __Exs_RenderSystem_GraphicsShaderConfig_H__

#include "CommonStateDefs.h"


namespace Exs
{


	ExsDeclareRSClassObjHandle(Shader);


	struct GraphicsShaderDataBinding
	{
		const char* baseName;

		Uint32 bindLocation;
	};

	
	// Represents initializer for an empty GraphicsShaderDataBinding object. Empty description object can be used
	// as a guard object to signal end of the array without need to explicitly specify its size.
	#define GraphicsShaderDataBindingEmpty { nullptr, 0 }


	// Checks if specified GraphicsShaderDataBinding object is empty.
	#define GraphicsShaderDataBindingIsEmpty(binding) ((binding).baseName == 0)


	///<summary>
	/// Configuration of graphics shader stages used by the rendering pipeline. It is used to create and initialize
	/// state descriptor which represents this state.
	///</summary>
	struct GraphicsShaderConfiguration
	{
		//
		ShaderHandle vertexShader;

		//
		ShaderHandle tesselationControlShader;

		//
		ShaderHandle tesselationEvaluationShader;

		//
		ShaderHandle geometryShader;

		//
		ShaderHandle pixelShader;

		//
		stdx::mask<Enum> activeStageMask = ShaderStage_Vertex | ShaderStage_Pixel;

		//
		const GraphicsShaderDataBinding* customAttribBindingArray = nullptr;

		//
		const GraphicsShaderDataBinding* customSamplerBindingArray = nullptr;

		//
		const GraphicsShaderDataBinding* customUniformBlockBindingArray = nullptr;
	};


}


#endif /* __Exs_RenderSystem_GraphicsShaderConfig_H__ */
