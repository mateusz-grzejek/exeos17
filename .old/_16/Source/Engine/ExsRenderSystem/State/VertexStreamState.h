
#pragma once

#ifndef __Exs_RenderSystem_VertexStreamState_H__
#define __Exs_RenderSystem_VertexStreamState_H__

#include "VertexStreamConfig.h"


namespace Exs
{


	///<summary>
	///</summary>
	class VertexStreamStateDescriptor : public RSStateDescriptor
	{
		EXS_DECLARE_NONCOPYABLE(VertexStreamStateDescriptor);

	protected:
		VertexStreamConfiguration    _configuration;

	public:
		///<param name="renderSystem"> Pointer to the RenderSystem instance. </param>
		///<param name="baseObjectID"> Unique ID assigned to this descriptor. </param>
		///<param name="descriptorPID">  </param>
		///<param name="configuration">  </param>
		VertexStreamStateDescriptor( RenderSystem* renderSystem,
		                             RSBaseObjectID baseObjectID,
		                             RSStateDescriptorPID descriptorPID,
		                             const VertexStreamConfiguration& configuration)
		: RSStateDescriptor(renderSystem, baseObjectID, 0, RSStateDescriptorType::Vertex_Stream, descriptorPID)
		, _configuration(configuration)
		{ }

		const VertexStreamConfiguration& GetConfiguration() const
		{
			return this->_configuration;
		}

		static bool ValidateConfiguration(const VertexStreamConfiguration& configuration)
		{
			return true;
		}
	};


};


#endif /* __Exs_RenderSystem_VertexBufferState_H__ */
