
#pragma once

#ifndef __Exs_RenderSystem_RSContextStateController_H__
#define __Exs_RenderSystem_RSContextStateController_H__

#include "Prerequisites.h"


namespace Exs
{


	class RSThreadState;

	class BlendStateDescriptor;
	class DepthStencilStateDescriptor;
	class GraphicsShaderStateDescriptor;
	class InputLayoutStateDescriptor;
	class RasterizerStateDescriptor;
	class VertexStreamStateDescriptor;
	
	ExsDeclareRSClassObjHandle(Sampler);
	ExsDeclareRSClassObjHandle(ShaderResource);


	enum RSStateUpdateFlags : Enum
	{
		RSStateUpdate_Descriptor_Blend,
		RSStateUpdate_Descriptor_Depth_Stencil,
		RSStateUpdate_Descriptor_Graphics_Shader,
		RSStateUpdate_Descriptor_Input_Layout,
		RSStateUpdate_Descriptor_Rasterizer,
		RSStateUpdate_Descriptor_Vertex_Stream
	};


	class EXS_LIBCLASS_RENDERSYSTEM RSContextStateController
	{
		friend class RSContextCommandQueue;

	protected:
		struct DynamicState
		{
			Color clearColor;
			DepthRange depthRange;
			RectU32 scissorRect;
			Viewport viewport;
		};

		struct StateDescriptors
		{
			BlendStateDescriptor* blend;
			DepthStencilStateDescriptor* depthStencil;
			GraphicsShaderStateDescriptor* graphicsShader;
			InputLayoutStateDescriptor* inputLayout;
			RasterizerStateDescriptor* rasterizer;
			VertexStreamStateDescriptor* vertexStream;
		};

	protected:
		RenderSystem*                      _renderSystem;
		RSThreadState*                     _rsThreadState;
		GraphicsPipelineStateObjectHandle  _graphicsPipelineStateObject;
		VertexArrayStateObjectHandle       _vertexArrayStateObject;
		DynamicState                       _dynamicState;
		StateDescriptors                   _stateDescriptors;
		stdx::mask<RSStateUpdateFlags>     _updateMask;

	public:
		RSContextStateController(RenderSystem* renderSystem, RSThreadState* rsThreadState);
		virtual ~RSContextStateController();

		virtual bool SetClearColor(const Color& color) = 0;
		virtual bool SetDepthRange(const DepthRange& depthRange) = 0;
		virtual bool SetScissorRect(const RectU32& scissorRect) = 0;
		virtual bool SetViewport(const Viewport& viewport) = 0;

		virtual void Reset();

		bool SetGraphicsPipelineState(const GraphicsPipelineStateObjectHandle& graphicsPipelineStateObject);
		bool SetVertexArrayState(const VertexArrayStateObjectHandle& vertexArrayStateObject);
		
		const GraphicsPipelineStateObjectHandle& GetGraphicsPipelineStateObject() const;
		const VertexArrayStateObjectHandle& GetVertexArrayStateObject() const;

		const Color& GetClearColor() const;
		const DepthRange& GetDepthRange() const;
		const RectU32& GetScissorRect() const;
		const Viewport& GetViewport() const;

	protected:
		virtual bool ApplyInternalRenderState();

	private:
		virtual void UpdateBlendState(BlendStateDescriptor* blendStateDescriptor) = 0;
		virtual void UpdateDepthStencilState(DepthStencilStateDescriptor* depthStencilStateDescriptor) = 0;
		virtual void UpdateRasterizerState(RasterizerStateDescriptor* rasterizerStateDescriptor) = 0;
	};


	inline const GraphicsPipelineStateObjectHandle& RSContextStateController::GetGraphicsPipelineStateObject() const
	{
		return this->_graphicsPipelineStateObject;
	}

	inline const VertexArrayStateObjectHandle& RSContextStateController::GetVertexArrayStateObject() const
	{
		return this->_vertexArrayStateObject;
	}

	inline const Color& RSContextStateController::GetClearColor() const
	{
		return this->_dynamicState.clearColor;
	}

	inline const DepthRange& RSContextStateController::GetDepthRange() const
	{
		return this->_dynamicState.depthRange;
	}

	inline const RectU32& RSContextStateController::GetScissorRect() const
	{
		return this->_dynamicState.scissorRect;
	}

	inline const Viewport& RSContextStateController::GetViewport() const
	{
		return this->_dynamicState.viewport;
	}


};


#endif /* __Exs_RenderSystem_RSContextStateController_H__ */
