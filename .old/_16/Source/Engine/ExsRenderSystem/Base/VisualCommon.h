
#pragma once

#ifndef __Exs_RenderSystem_VisualCommon_H__
#define __Exs_RenderSystem_VisualCommon_H__


namespace Exs
{

	
	///<summary>
	///</summary>
	enum class VisualColorFormat : Enum
	{
		//
		R8G8B8A8 = EXS_MAKEU32B(8, 8, 8, 8)
	};


	struct VisualColorDesc
	{
		//
		Uint8 redBits;

		//
		Uint8 greenBits;

		//
		Uint8 blueBits;

		//
		Uint8 alphaBits;
	};

	
	///<summary>
	///</summary>
	enum class VisualDepthStencilFormat : Enum
	{
		//
		None = EXS_MAKEU16B(0, 0),

		//
		D16 = EXS_MAKEU16B(16, 0),

		//
		D24S8 = EXS_MAKEU16B(24, 8),

		//
		D32 = EXS_MAKEU16B(32, 0)
	};


	struct VisualDepthStencilDesc
	{
		//
		Uint8 depthBufferSize;

		//
		Uint8 stencilBufferSize;
	};

	
	///<summary>
	///</summary>
	enum class VisualMSAAConfig : Enum
	{
		//
		Disabled = EXS_MAKEU16B(0, 0),

		//
		x1 = EXS_MAKEU16B(1, 1),

		//
		x2 = EXS_MAKEU16B(1, 2),

		//
		x4 = EXS_MAKEU16B(1, 4),

		//
		x8 = EXS_MAKEU16B(1, 8),

		//
		Best = x8
	};


	struct VisualMSAADesc
	{
		//
		Uint32 count;

		//
		Uint32 quality;
	};


	///<summary>
	///</summary>
	struct VisualConfig
	{
		//
		VisualColorFormat colorFormat;

		//
		VisualDepthStencilFormat depthStencilFormat;

		//
		VisualMSAAConfig msaaConfig;
	};


	inline VisualColorDesc RSGetVisualColorDesc(VisualColorFormat colorFormat)
	{
		VisualColorDesc colorDesc;
		colorDesc.redBits = EXS_U32_HIBYTE1(colorFormat);
		colorDesc.greenBits = EXS_U32_HIBYTE2(colorFormat);
		colorDesc.blueBits = EXS_U32_HIBYTE3(colorFormat);
		colorDesc.alphaBits = EXS_U32_HIBYTE4(colorFormat);
		return colorDesc;
	}


	inline VisualDepthStencilDesc RSGetVisualDepthStencilDesc(VisualDepthStencilFormat depthStencilFormat)
	{
		VisualDepthStencilDesc depthStencilDesc;
		depthStencilDesc.depthBufferSize = EXS_U16_HIBYTE1(depthStencilFormat);
		depthStencilDesc.stencilBufferSize = EXS_U16_HIBYTE2(depthStencilFormat);
		return depthStencilDesc;
	}


	inline VisualMSAADesc RSGetVisualMSAADesc(VisualMSAAConfig msaaConfig)
	{
		VisualMSAADesc msaaDesc;
		msaaDesc.count = EXS_U16_HIBYTE1(msaaConfig);
		msaaDesc.quality = EXS_U16_HIBYTE2(msaaConfig);
		return msaaDesc;
	}


	inline constexpr Uint32 RSGetVisualMSAAConfigSamplesNum(VisualMSAAConfig msaaConfig)
	{
		return EXS_U16_HIBYTE2(msaaConfig);
	}


	inline constexpr VisualMSAAConfig RSGetVisualMSAAConfigForSampleNum(Uint32 samplesNum)
	{
		return (samplesNum != 0) ? static_cast<VisualMSAAConfig>(EXS_MAKEU16B(1, samplesNum)) : VisualMSAAConfig::Disabled;
	}


}


#endif /* __Exs_RenderSystem_VisualCommon_H__ */

