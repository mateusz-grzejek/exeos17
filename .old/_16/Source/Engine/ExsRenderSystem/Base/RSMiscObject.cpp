
#include "_Precompiled.h"
#include <ExsRenderSystem/RenderSystem.h>


namespace Exs
{


	RSMiscObject::RSMiscObject( RenderSystem* renderSystem,
	                            RSBaseObjectID baseObjectID,
	                            Enum initialState,
	                            RSMiscObjectType miscObjectType)
	: RSBaseObject(renderSystem, RSBaseObjectType::Misc_Object, baseObjectID, initialState)
	, _miscObjectType(miscObjectType)
	{ }


	RSMiscObject::~RSMiscObject()
	{
		this->_renderSystem->OnMiscObjectDestroy(this->GetBaseObjectID(), this);
	}


}
