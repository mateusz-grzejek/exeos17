
#include "_Precompiled.h"
#include <ExsRenderSystem/RenderSystem.h>


namespace Exs
{


	RSStateObject::RSStateObject( RenderSystem* renderSystem,
	                              RSBaseObjectID baseObjectID,
	                              Enum initialState,
	                              RSStateObjectType stateObjectType)
	: RSBaseObject(renderSystem, RSBaseObjectType::State_Object, baseObjectID, initialState)
	, _stateObjectType(stateObjectType)
	{ }


	RSStateObject::~RSStateObject()
	{
		this->_renderSystem->OnStateObjectDestroy(this->GetBaseObjectID(), this);
	}


}
