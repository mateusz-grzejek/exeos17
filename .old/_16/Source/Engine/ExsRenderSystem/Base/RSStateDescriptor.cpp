
#include "_Precompiled.h"
#include <ExsRenderSystem/RenderSystem.h>


namespace Exs
{


	RSStateDescriptor::RSStateDescriptor( RenderSystem* renderSystem,
	                                      RSBaseObjectID baseObjectID,
	                                      Enum initialState,
	                                      RSStateDescriptorType stateDescriptorType,
	                                      RSStateDescriptorPID descriptorPID)
	: RSBaseObject(renderSystem, RSBaseObjectType::State_Descriptor, baseObjectID, initialState)
	, _stateDescriptorType(stateDescriptorType)
	, _descriptorPID(descriptorPID)
	{ }


	RSStateDescriptor::~RSStateDescriptor()
	{
		this->_renderSystem->OnStateDescriptorDestroy(this->GetBaseObjectID(), this);
	}


}
