
#include "_Precompiled.h"
#include <ExsRenderSystem/RenderSystem.h>


namespace Exs
{


	RSResource::RSResource( RenderSystem* renderSystem,
	                        RSBaseObjectID baseObjectID,
	                        Enum initialState,
	                        RSResourceType resourceType,
	                        Enum readyStateConditionMask)
	: RSBaseObject(renderSystem, RSBaseObjectType::Resource, baseObjectID, initialState)
	, _resourceType(resourceType)
	, _readyStateConditionMask(readyStateConditionMask)
	{ }


	RSResource::~RSResource()
	{
		this->_renderSystem->OnResourceDestroy(this->GetBaseObjectID(), this);
	}


	void RSResource::BindStorage(const RSInternalHandle<RSResourceStorage>& storage)
	{
		ExsDebugAssert( !this->_storage && storage );
		this->OnBindStorage(storage.get());
		this->_storage = storage;
	}


	void RSResource::OnBindStorage(RSResourceStorage* storage)
	{ }


}
