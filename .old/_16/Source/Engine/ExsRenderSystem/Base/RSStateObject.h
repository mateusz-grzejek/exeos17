
#pragma once

#ifndef __Exs_RenderSystem_RSStateObject_H__
#define __Exs_RenderSystem_RSStateObject_H__


namespace Exs
{


	///<summary>
	/// Common class for state objects. State object is an object, that describes a set of related
	/// properties and settings which should bee seen as a whole.  Every state object type contains
	/// one or more descriptors (RSStateDescriptor), each one of them containing configuration of a
	/// specific feature of the rendering pipeline. Two standard state objects are provided: state
	/// object for graphics pipeline configuration (GraphicsPipelineStateObject) and another one
	/// for vertex array configuration (VertexArrayStateObject).
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM RSStateObject : public RSBaseObject
	{
		EXS_DECLARE_NONCOPYABLE(RSStateObject);

	protected:
		RSStateObjectType   _stateObjectType;

	public:
		///<param name="renderSystem"> Pointer to the RenderSystem instance. </param>
		///<param name="baseObjectID"> Unique ID assigned to this state object. </param>
		///<param name="initialState"> Initial state that is set for this state object. </param>
		///<param name="stateObjectType">  </param>
		RSStateObject( RenderSystem* renderSystem,
		               RSBaseObjectID baseObjectID,
		               Enum initialState,
		               RSStateObjectType stateObjectType);

		virtual ~RSStateObject();
		
		///<summary>
		///</summary>
		RSStateObjectType GetStateObjectType() const;
	};
	

	inline RSStateObjectType RSStateObject::GetStateObjectType() const
	{
		return this->_stateObjectType;
	}


}


#endif /* __Exs_RenderSystem_RSStateObject_H__ */
