
#pragma once

#ifndef __Exs_RenderSystem_BaseConfig_H__
#define __Exs_RenderSystem_BaseConfig_H__

#define EXS_RS_ENABLE_VK12 1

#include <ExsCore/Exception.h>

#include <ExsMath/Color.h>
#include <ExsMath/Vector2.h>
#include <ExsMath/Vector4.h>
#include <ExsMath/Matrix4.h>
#include <ExsMath/VectorUtils.h>
#include <ExsMath/MatrixUtils.h>
#include <ExsMath/Rect.h>


#if ( EXS_BUILD_MODULE_RENDERSYSTEM )
#  define EXS_LIBAPI_RENDERSYSTEM       EXS_MODULE_EXPORT
#  define EXS_LIBCLASS_RENDERSYSTEM     EXS_MODULE_EXPORT
#  define EXS_LIBOBJECT_RENDERSYSTEM    extern EXS_MODULE_EXPORT
#else
#  define EXS_LIBAPI_RENDERSYSTEM       EXS_MODULE_IMPORT
#  define EXS_LIBCLASS_RENDERSYSTEM     EXS_MODULE_IMPORT
#  define EXS_LIBOBJECT_RENDERSYSTEM    extern EXS_MODULE_IMPORT
#endif

#endif /* __Exs_RenderSystem_BaseConfig_H__ */
