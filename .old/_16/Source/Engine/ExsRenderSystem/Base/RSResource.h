
#pragma once

#ifndef __Exs_RenderSystem_RSResource_H__
#define __Exs_RenderSystem_RSResource_H__


namespace Exs
{


	///<summary>
	/// Base class for all resource objects created and managed by the render system.
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM RSResource : public RSBaseObject
	{
		EXS_DECLARE_NONCOPYABLE(RSResource);

	protected:
		RSResourceType                       _resourceType; // Type of the resource.
		RSMappedMemoryInfo                   _mappedMemoryInfo;
		RSInternalHandle<RSResourceStorage>  _storage;
		const Enum                           _readyStateConditionMask;

	public:
		///<param name="renderSystem"> Pointer to the RenderSystem instance. </param>
		///<param name="baseObjectID"> Unique ID assigned to this resource. </param>
		///<param name="initialState"> Initial state that is set for this resource. </param>
		///<param name="resourceType">  </param>
		///<param name="readyStateConditionMask">  </param>
		RSResource( RenderSystem* renderSystem,
		            RSBaseObjectID baseObjectID,
		            Enum initialState,
		            RSResourceType resourceType,
		            Enum readyStateConditionMask);

		virtual ~RSResource();

		///<summary>
		///</summary>
		void BindStorage(const RSInternalHandle<RSResourceStorage>& storage);
		
		///<summary>
		///</summary>
		RSResourceStorage* GetStorage();

		///<summary>
		///</summary>
		const RSResourceStorage* GetStorage() const;
		
		///<summary>
		///</summary>
		const RSMappedMemoryInfo& GetMappedMemoryInfo() const;
		
		///<summary>
		///</summary>
		RSResourceType GetResourceType() const;
		
		///<summary>
		///</summary>
		bool IsReady() const;

	protected:
		virtual void OnBindStorage(RSResourceStorage* storage);
	};
	

	inline RSResourceStorage* RSResource::GetStorage()
	{
		return this->_storage.get();
	}

	inline const RSResourceStorage* RSResource::GetStorage() const
	{
		return this->_storage.get();
	}

	inline const RSMappedMemoryInfo& RSResource::GetMappedMemoryInfo() const
	{
		return this->_mappedMemoryInfo;
	}

	inline RSResourceType RSResource::GetResourceType() const
	{
		return this->_resourceType;
	}

	inline bool RSResource::IsReady() const
	{
		return this->_state.is_set(this->_readyStateConditionMask);
	}


}


#endif /* __Exs_RenderSystem_RSResource_H__ */
                       