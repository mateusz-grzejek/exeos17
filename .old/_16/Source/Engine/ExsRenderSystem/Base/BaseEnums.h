
#pragma once

#ifndef __Exs_RenderSystem_BaseEnums_H__
#define __Exs_RenderSystem_BaseEnums_H__


namespace Exs
{


	enum class RSBaseObjectType : Enum
	{
		Misc_Object = 0x4601,

		Resource,

		Resource_View,

		State_Descriptor,

		State_Object
	};


	///<summary>
	///</summary>
	enum class RSMiscObjectType : Enum
	{
		Render_Target = 0xE101,

		Sampler,

		Shader
	};


	///<summary>
	///</summary>
	enum class RSResourceType : Enum
	{
		Buffer = 0xE201,

		Texture
	};


	///<summary>
	///</summary>
	enum class RSResourceViewType : Enum
	{
		Depth_Stencil = 0xE301,

		Render_Target,

		Shader_Resource
	};


	///<summary>
	///</summary>
	enum class RSStateDescriptorType : Enum
	{
		//
		Blend = 0xE401,

		//
		Depth_Stencil,

		//
		Graphics_Shader,

		//
		Input_Layout,

		//
		Rasterizer,

		//
		Vertex_Stream
	};


	///<summary>
	///</summary>
	enum class RSStateObjectType : Enum
	{
		//
		Graphics_Pipeline = 0xE401,

		//
		Vertex_Array
	};


	///<summary>
	///</summary>
	enum class ActiveState : Enum
	{
		//
		Disabled,

		//
		Enabled
	};


	enum class ShaderResourceViewResType : Enum
	{
		Buffer,

		Texture_2D,

		Texture_2D_Array,

		Texture_2D_Multisample,

		Texture_2D_Multisample_Array,

		Texture_3D,

		Texture_Cube_Map
	};


	///<summary>
	///</summary>
	enum RSResourceUsageFlags : Enum
	{
		//
		RSResourceUsage_Unspecified = 0,

		//
		RSResourceUsage_Dynamic = 0x0001,

		//
		RSResourceUsage_Immutable = 0x0002,

		//
		RSResourceUsage_Static = 0x0004,

		//
		RSResourceUsage_Temporary = 0x0008,

		//
		RSResourceUsage_Readback = 0x0010,

		//
		RSResourceUsage_Transfer = 0x0020
	};


	///<summary>
	///</summary>
	enum RSMemoryMapFlags : Enum
	{
		//
		RSMemoryMap_Access_Read = static_cast<Enum>(AccessMode_Read),

		//
		RSMemoryMap_Access_Write = static_cast<Enum>(AccessMode_Write),

		//
		RSMemoryMap_Invalidate_All = 0x0100,

		//
		RSMemoryMap_Invalidate_Range = 0x0200,

		//
		RSMemoryMap_No_Overwrite = 0x1000
	};


	///<summary>
	/// Specifies level and type of access to a resource when it is mapped to client memory.
	///</summary>
	enum class RSResourceMapMode : Enum
	{
		// Resource is mapped with read-only permission.
		Read = RSMemoryMap_Access_Read,

		// Resource is mapped with both read and write permissions.
		Read_Write = RSMemoryMap_Access_Read | RSMemoryMap_Access_Write,

		// Resource is mapped with write-only permission.
		Write = RSMemoryMap_Access_Write,

		// Resource is mapped with write-only permission. After resource is mapped, all its data is discarded and becomes
		// undefined. This access can only be used in case of resources with dynamic usage.
		Write_Invalidate_All = RSMemoryMap_Access_Write | RSMemoryMap_Invalidate_All,

		// Resource is mapped with write-only permission. After resource is mapped, data within mapped range is discarded
		// and becomes undefined. This access can only be used in case of resources with dynamic usage.
		Write_Invalidate_Range = RSMemoryMap_Access_Write | RSMemoryMap_Invalidate_Range,

		// Resource is mapped with write-only permission. However, application must ensure, that sections of the resource,
		// that could be potentially in use, will not be affected. Write operations should be performed only on uninitialized
		// sections of the resource. Due to less restrictions, this access mode may be more efficient than Write_Only.
		Write_No_Overwrite = RSMemoryMap_Access_Write | RSMemoryMap_No_Overwrite
	};


	///<summary>
	/// Comparison functions, used in various operations in rendering pipeline(like depth/stencil tests or alpha testing).
	///</summary>
	enum class ComparisonFunction : Enum
	{
		Always,

		Equal,

		Greater,

		Greater_Equal,

		Less,

		Less_Equal,

		Never,

		Not_Equal
	};


	///<summary>
	/// Identifies a way, in which vertices are interpreted and assembled by the input assembler (IA) stage.
	///</summary>
	enum class PrimitiveTopology : Enum
	{
		Point_List,

		Line_List,

		Line_List_Adjacency,

		Line_Strip,

		Line_Strip_Adjacency,

		Triangle_List,

		Triangle_List_Adjacency,

		Triangle_Strip,

		Triangle_Strip_Adjacency,

		Tesselation_Patch
	};


	///<summary>
	///</summary>
	enum ShaderStageFlags : Enum
	{
		ShaderStage_Vertex = 0x0001,

		ShaderStage_Tesselation_Control = 0x0002,

		ShaderStage_Tesselation_Evaluation = 0x0004,

		ShaderStage_Geometry = 0x0008,

		ShaderStage_Pixel = 0x0010,

		ShaderStage_All_Graphics = 0x00FF,

		ShaderStage_Compute = 0x0100,

		ShaderStage_None = 0
	};


}


#endif /* __Exs_RenderSystem_BaseEnums_H__ */
