
#pragma once

#ifndef __Exs_RenderSystem_RSResourceStorage_H__
#define __Exs_RenderSystem_RSResourceStorage_H__


namespace Exs
{

	
	///<summary>
	///</summary>
	struct RSResourceStorageInitDesc
	{
		RSResourceMemoryInfo memoryInfo;

		stdx::mask<RSResourceUsageFlags> usageFlags;
	};

	
	///<summary>
	///</summary>
	class RSResourceStorage : public stdx::ref_counted_base<stdx::atomic_ref_counter>
	{
		EXS_DECLARE_NONCOPYABLE(RSResourceStorage);

	protected:
		RSResourceMemoryInfo  _memoryInfo;
		RSMappedMemoryInfo    _mappedMemoryInfo;

	public:
		///<param name="memoryInfo">  </param>
		RSResourceStorage(const RSResourceMemoryInfo& memoryInfo)
		: _memoryInfo(memoryInfo)
		{ }

		virtual ~RSResourceStorage()
		{ }

		///<summary>
		///</summary>
		virtual bool MapMemory(RSResourceMapMode mapMode, RSMappedMemoryInfo* mappedMemoryInfo) = 0;

		///<summary>
		///</summary>
		virtual bool MapMemory(const RSMemoryRange& range, RSResourceMapMode mapMode, RSMappedMemoryInfo* mappedMemoryInfo) = 0;

		///<summary>
		///</summary>
		virtual void UnmapMemory() = 0;
		
		///<summary>
		///</summary>
		template <class T>
		T* As()
		{
			T* otherPtr = dbgsafe_ptr_cast<T*>(this);
			return otherPtr;
		}
		
		///<summary>
		///</summary>
		template <class T>
		const T* As() const
		{
			T* otherPtr = dbgsafe_ptr_cast<T*>(this);
			return otherPtr;
		}
		
		///<summary>
		///</summary>
		bool CheckMemoryAccess(stdx::mask<AccessModeFlags> accessModeFlags) const
		{
			stdx::mask<AccessModeFlags> internalAccess = this->_memoryInfo.access;
			return internalAccess.is_set(accessModeFlags);
		}
		
		///<summary>
		///</summary>
		bool CheckMemoryAccess(RSMemoryAccess access) const
		{
			AccessModeFlags accessFlags = static_cast<AccessModeFlags>((Enum)access & (AccessMode_Read | AccessMode_Write));
			return this->CheckMemoryAccess(accessFlags);
		}

		///<summary>
		/// Returns range of mapped memory retrieved from previous call to <c>Map()</c> or <c>MapRange()</c>. If buffer
		/// was not mapped, range with <c>offset</c> and <c>length</c> both set to 0 is returned.
		///</summary>
		const RSMappedMemoryInfo& GetMappedMemoryInfo() const
		{
			return this->_mappedMemoryInfo;
		}

		///<summary>
		/// Returns true, if memory is currently mapped.
		///</summary>
		bool IsMapped() const
		{
			return this->_mappedMemoryInfo.ptr != nullptr;
		}
	};

	
	template <class Storage, class... Args>
	inline RSInternalHandle<Storage> CreateRSResourceStorage(Args&&... args)
	{
		return stdx::make_instrusive<Storage>(std::forward<Args>(args)...);
	}


}


#endif /* _Exs_RenderSystem_RSResourceStorage_H__ */
