
#include "_Precompiled.h"
#include <ExsRenderSystem/RSContextCommandQueue.h>
#include <ExsRenderSystem/RenderSystem.h>
#include <ExsRenderSystem/RSContextStateController.h>
#include <ExsRenderSystem/RSThreadState.h>


namespace Exs
{


	RSContextCommandQueue::RSContextCommandQueue(RenderSystem* renderSystem, RSThreadState* rsThreadState, RSContextStateController* rsContextStateController)
	: _renderSystem(renderSystem)
	, _rsThreadState(rsThreadState)
	, _rsContextStateController(rsContextStateController)
	{ }


	RSContextCommandQueue::~RSContextCommandQueue()
	{ }


	void RSContextCommandQueue::Reset()
	{
	}


	bool RSContextCommandQueue::SetGraphicsPipelineState(const GraphicsPipelineStateObjectHandle& graphicsPipelineStateObject)
	{
		return this->_rsContextStateController->SetGraphicsPipelineState(graphicsPipelineStateObject);
	}


	bool RSContextCommandQueue::SetVertexArrayState(const VertexArrayStateObjectHandle& vertexArrayStateObject)
	{
		return this->_rsContextStateController->SetVertexArrayState(vertexArrayStateObject);
	}


	bool RSContextCommandQueue::SetClearColor(const Color* color)
	{
		if (color == nullptr)
		{
			const Color defaultColor { 255, 255, 255, 255 };
			return this->SetClearColor(&defaultColor);
		}
		else
		{
			return this->_rsContextStateController->SetClearColor(*color);
		}
	}


	bool RSContextCommandQueue::SetDepthRange(const DepthRange* depthRange)
	{
		if (depthRange == nullptr)
		{
			const DepthRange defaultDepthRange(0.0f, 1.0f);
			return this->SetDepthRange(&defaultDepthRange);
		}
		else
		{
			return this->_rsContextStateController->SetDepthRange(*depthRange);
		}
	}


	bool RSContextCommandQueue::SetScissorRect(const RectU32* scissorRect)
	{
		if (scissorRect == nullptr)
		{
			const auto& viewport = this->_rsContextStateController->GetViewport();
			const RectU32 defaultScissorRect(viewport.origin, viewport.size);
			return this->SetScissorRect(&defaultScissorRect);
		}
		else
		{
			return this->_rsContextStateController->SetScissorRect(*scissorRect);
		}
	}


	bool RSContextCommandQueue::SetViewport(const Viewport* viewport)
	{
		if (viewport == nullptr)
		{
			Viewport defaultViewport { };
			return this->SetViewport(&defaultViewport);
		}
		else
		{
			return this->_rsContextStateController->SetViewport(*viewport);
		}
	}


}
