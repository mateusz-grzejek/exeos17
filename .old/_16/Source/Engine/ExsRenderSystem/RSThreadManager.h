
#pragma once

#ifndef __Exs_RenderSystem_RSThreadManager_H__
#define __Exs_RenderSystem_RSThreadManager_H__

#include "RSThreadState.h"


namespace Exs
{
	
	class RSContextCommandQueue;
	class RSContextStateController;


	class EXS_LIBCLASS_RENDERSYSTEM RSThreadManager
	{
		friend class RSThreadState;

	protected:
		struct RSThreadEntry
		{
			enum AcquireStateFlags : Enum
			{
				AcquireState_Active = 0x0001
			};

			//
			stdx::atomic_mask<AcquireStateFlags> acquireState;

			//
			std::unique_ptr<RSThreadState> threadState;

			//
			RSThreadIndex index = RS_Thr_Invalid_Thread_Index;

			//
			RSThread* threadObject = nullptr;
		};

		typedef std::array<RSThreadEntry, Config::RS_THR_Worker_Threads_Num_Limit + 1> RSThreadArray;

	protected:
		RenderSystem*          _renderSystem; // Instance of RenderSystem.
		RSThreadArray          _rsThreadArray; // Array of thread entries.
		RSThreadEntry*         _rsMainThreadEntry; // Data of main rendering thread.
		std::atomic<Uint32>    _activeThreadsNum;
		Uint32                 _workerThreadsNumLimit;

	public:
		RSThreadManager(RenderSystem* renderSystem);
		virtual ~RSThreadManager();

		RSThreadState* AcquireThreadState(RSThreadType threadType, RSThread* threadObject);

		void ReleaseThreadState(RSThreadState* threadState);

		void SetWorkerThreadsLimit(Uint32 limit);
		
		Uint32 GetActiveThreadsNum() const;
		Uint32 GetWorkerThreadsLimit() const;

		static RSThread* QueryCurrentThreadObject();

	friendapi:
		//
		static bool BindThreadState(RSThreadState* threadState);

		//
		static void UnbindThreadState(RSThreadState* threadState);

	private:
		RSThreadEntry* TryAcquireMainThreadState();
		RSThreadEntry* TryAcquireWorkerThreadState();
	};


	inline void RSThreadManager::SetWorkerThreadsLimit(Uint32 limit)
	{
		ExsDebugAssert( this->GetActiveThreadsNum() <= 1 );
		ExsDebugAssert( limit <= Config::RS_THR_Worker_Threads_Num_Limit );
		this->_workerThreadsNumLimit = limit;
	}

	inline Uint32 RSThreadManager::GetActiveThreadsNum() const
	{
		return this->_activeThreadsNum.load(std::memory_order_relaxed);
	}

	inline Uint32 RSThreadManager::GetWorkerThreadsLimit() const
	{
		return this->_workerThreadsNumLimit;
	}


}


#endif /* __Exs_RenderSystem_RSThreadManager_H__ */
