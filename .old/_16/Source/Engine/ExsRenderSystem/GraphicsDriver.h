
#pragma once

#ifndef __Exs_RenderSystem_GraphicsDriver_H__
#define __Exs_RenderSystem_GraphicsDriver_H__

#include "GraphicsDriverCapabilities.h"
#include "VideoMode.h"


namespace Exs
{


	class RSThreadState;
	class SystemWindow;

	ExsDeclareRefPtrClass(RenderSystem);


	enum : Enum
	{
		RSC_GraphicsDriver_Invalid_Flags,
		RSC_GraphicsDriver_Requested_Features_Not_Supported
	};


	///<summary>
	///</summary>
	enum GraphicsDriverInitFlags : Enum
	{
		//
		GraphicsDriverInit_Default = 0,

		//
		GraphicsDriverInit_Enable_Debug_Layer = 0x0100,

		//
		GraphicsDriverInit_Enable_Multithreading = 0x0200,

		//
		GraphicsDriverInit_Force_Compatibility_Restriction = 0x0400,

		//
		GraphicsDriverInit_Use_Reference_Driver = 0x1000
	};


	///<summary>
	///</summary>
	enum class VerticalSyncMode : Enum
	{
		Disabled = 0,

		Enabled,

		Adaptive,

		Auto,

		Default = Disabled
	};


	///<summary>
	///</summary>
	struct GraphicsDriverConfigInfo
	{
		//
		const GraphicsDriverCapabilities* capabilities;

		//
		SystemWindow* targetWindow;
	};


	///<summary>
	///</summary>
	struct GraphicsDriverInitDesc
	{
		//
		GraphicsDriverFeatureLevel featureLevel;
		
		//
		VisualConfig visualConfig;

		// Initialization flags (see GraphicsDriverInitFlags enumeration).
		stdx::mask<GraphicsDriverInitFlags> flags;

		// Additional, GraphicsDriver-specific args.
		std::string specificArgs;
	};


	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM GraphicsDriver
	{
	protected:
		std::unique_ptr<GraphicsDriverConfigInfo>    _configInfo; // Config info, used to pre-initialize GraphicsDriver.
		SystemWindow*                          _targetWindow; // System window GraphicsDriver is initialized for.
		GraphicsDriverCapabilities                   _capabilities; // Capapbilities of the GraphicsDriver.
		RenderSystemRefPtr                     _renderSystem; // Instance of RenderSystem.
		RSThreadState*                         _mainRSThreadState; // RSThreadState for main thread.

	public:
		GraphicsDriver(const GraphicsDriverCapabilitiesDesc& capabilitiesDesc);
		virtual ~GraphicsDriver();

		///<summary>
		///</summary>
		///<param name="systemWindow">  </param>
		virtual GraphicsDriverConfigInfo* Configure(SystemWindow& targetWindow) = 0;

		///<summary>
		///</summary>
		///<param name="initDesc">  </param>
		virtual Result Initialize(const GraphicsDriverInitDesc& initDesc) = 0;
		
		///<summary>
		///</summary>
		virtual std::vector<VisualMSAAConfig> QuerySupportedMSAAConfigurations(const VisualConfig& visualConfig) = 0;
		
		///<summary>
		///</summary>
		virtual std::vector<VideoModeConfig> QuerySupportedVideoModes() = 0;
		
		///<summary>
		///</summary>
		virtual Result SetVideoMode(const VideoModeConfig& vmConfig, stdx::mask<VideoModeFlags> flags) = 0;

		///<summary>
		///</summary>
		virtual void Release() = 0;
		
		///<summary>
		///</summary>
		RSThreadState* GetMainRSThreadState() const;

		///<summary>
		///</summary>
		RenderSystem* GetRenderSystem() const;

	protected:
		//
		bool AcquireMainThreadState();
		
		//
		void ReleaseMainThreadState();
		
		//
		void ReleaseActiveObjects();
	};


	inline RSThreadState* GraphicsDriver::GetMainRSThreadState() const
	{
		return this->_mainRSThreadState;
	}

	inline RenderSystem* GraphicsDriver::GetRenderSystem() const
	{
		return this->_renderSystem.get();
	}


}


#endif /* __Exs_RenderSystem_GraphicsDriver_H__ */
