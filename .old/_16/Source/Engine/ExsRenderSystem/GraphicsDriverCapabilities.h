
#pragma once

#ifndef __Exs_RenderSystem_GraphicsDriverCapabilities_H__
#define __Exs_RenderSystem_GraphicsDriverCapabilities_H__

#include "Prerequisites.h"


namespace Exs
{


	enum : Uint32
	{
		//
		GraphicsDriver_Metric_Value_Unspecified = stdx::limits<Uint32>::max_value
	};


	///<summary>
	///</summary>
	enum class GraphicsDriverFeatureLevel : Enum
	{
		//
		FL4D10G33SM4 = 0xF4A03340,

		//
		FL5D11G42SM5 = 0xF5B04050,

		//
		FL6D12G45SM5 = 0xF6C04550,

		//
		Unspecified = 0xFFFFFF
	};


	enum class ShaderModelVersion : Enum
	{
		SM_3_0 = 0x1030,
		SM_4_0 = 0x1040,
		SM_4_1 = 0x1041,
		SM_5_0 = 0x1050,
		SM_5_1 = 0x1051,
		Unknown = 0
	};


	///<summary>
	///</summary>
	enum class GraphicsDriverCapFeatureID : Enum
	{
		//
		Multithreaded_Command_Execution = 0,

		//
		Multithreaded_Resource_Ceation,

		//
		Shader_Explicit_Attrib_Binding,

		//
		Shader_Geometry_Stage,

		//
		Shader_Tesselation_Stage,

		//
		Texture_Anisotropy_Filter,

		//
		Viewport_Array,

		//
		Unknown
	};


	///<summary>
	///</summary>
	enum class GraphicsDriverCapMetricID : Enum
	{
		//
		Texture_Max_Anisotropy_Level = 0,

		//
		Viewport_Array_Size,

		//
		Unknown
	};


	enum : Uint32
	{
		GraphicsDriver_Cap_Features_Num = static_cast<Uint32>(GraphicsDriverCapFeatureID::Unknown),
		GraphicsDriver_Cap_Metrics_Num = static_cast<Uint32>(GraphicsDriverCapMetricID::Unknown)
	};


	///<summary>
	///</summary>
	struct GraphicsDriverCapabilitiesDesc
	{
		struct FeaturesDesc
		{
			bool multithreadedCommandExecution;
			bool multithreadedResourceCeation;
			bool shaderExplicitAttribBinding;
			bool shaderGeaometryStage;
			bool shaderTesselationStage;
			bool textureAnisotropyFilter;
			bool viewportArray;
		};

		struct MetricsDesc
		{
			Uint32 textureMaxAnisotropyLevel;
			Uint32 viewportArraySize;
		};

		FeaturesDesc featuresDesc;

		MetricsDesc metricsDesc;

		GraphicsDriverFeatureLevel featureLevel;

		ShaderModelVersion  shaderModelVersion;
	};


	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM GraphicsDriverCapabilities
	{
	private:
		struct InternalDesc
		{
			typedef GraphicsDriverCapabilitiesDesc::FeaturesDesc FeaturesDesc;
			typedef GraphicsDriverCapabilitiesDesc::MetricsDesc MetricsDesc;

			union Features
			{
				FeaturesDesc desc;
				bool descArray[GraphicsDriver_Cap_Features_Num];
			};

			union Metrics
			{
				MetricsDesc desc;
				Uint32 descArray[GraphicsDriver_Cap_Metrics_Num];
			};

			Features features;
			Metrics metrics;
			GraphicsDriverFeatureLevel featureLevel;
			ShaderModelVersion  shaderModelVersion;
		};

	private:
		InternalDesc                _internalDesc;
		GraphicsDriverCapabilitiesDesc    _sourceDesc;

	public:
		GraphicsDriverCapabilities(const GraphicsDriverCapabilitiesDesc& capabilitiesDesc);
		~GraphicsDriverCapabilities();

		///<summary>
		///</summary>
		void SetDesc(const GraphicsDriverCapabilitiesDesc& capabilitiesDesc);

		///<summary>
		/// Checks if GraphicsDriver capabilities defined by this object are compatible with specified GraphicsDriverCapabilitiesDesc.
		/// It returns true if contained capabilities are at least as high as specified ones or false otherwise.
		///</summary>
		///<param name=""capabilitiesDesc> GraphicsDriverCapabilitiesDesc object with capabilities to check against. </param>
		bool CheckSupport(const GraphicsDriverCapabilitiesDesc& capabilitiesDesc) const;

		///<summary>
		///</summary>
		const GraphicsDriverCapabilitiesDesc& GetDesc() const;

		///<summary>
		///</summary>
		GraphicsDriverFeatureLevel GetFeatureLevel() const;

		///<summary>
		///</summary>
		ShaderModelVersion GetShaderModelVersion() const;

		///<summary>
		///</summary>
		bool GetFeatureSupportState(GraphicsDriverCapFeatureID featureID) const;

		///<summary>
		///</summary>
		Uint32 GetMetricValue(GraphicsDriverCapMetricID metricID) const;

		///<summary>
		/// Queries and returns reference GraphicsDriver capabilities for specified feature level. Reference capabilities specify guaranteed
		/// level of support for GraphicsDrivers implementing certain feature level.
		///</summary>
		static bool QueryRefCapabilitiesDesc(GraphicsDriverFeatureLevel GraphicsDriverFeatureLevel, GraphicsDriverCapabilitiesDesc* capabilities);
	};


	inline const GraphicsDriverCapabilitiesDesc& GraphicsDriverCapabilities::GetDesc() const
	{
		return this->_sourceDesc;
	}

	inline GraphicsDriverFeatureLevel GraphicsDriverCapabilities::GetFeatureLevel() const
	{
		return this->_internalDesc.featureLevel;
	}

	inline ShaderModelVersion GraphicsDriverCapabilities::GetShaderModelVersion() const
	{
		return this->_internalDesc.shaderModelVersion;
	}

	inline bool GraphicsDriverCapabilities::GetFeatureSupportState(GraphicsDriverCapFeatureID featureID) const
	{
		return GetConstantArrayElement(this->_internalDesc.features.descArray, featureID);
	}

	inline Uint32 GraphicsDriverCapabilities::GetMetricValue(GraphicsDriverCapMetricID metricID) const
	{
		return GetConstantArrayElement(this->_internalDesc.metrics.descArray, metricID);
	}


}


#endif /* __Exs_RenderSystem_GraphicsDriverCapabilities_H__ */
