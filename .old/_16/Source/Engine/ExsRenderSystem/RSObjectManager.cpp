
#include "_Precompiled.h"
#include <ExsRenderSystem/RSObjectManager.h>
#include <ExsRenderSystem/RSObjectFactory.h>


namespace Exs
{


	RSObjectManager::RSObjectManager(RenderSystem* renderSystem, RSObjectFactory* rsObjectFactory)
	: _renderSystem(renderSystem)
	, _rsObjectFactory(rsObjectFactory)
	, _registeredObjectsNum(0)
	, _regCount(0)
	{ }


	RSObjectManager::~RSObjectManager()
	{
		// If list of registered objects is not empty, it means, that some RS objects still
		// exist somewhere in the memory. However, destruction of the manager indicates, that
		// RenderSystem is also being destroyed. This is performed usually when renderer DLL
		// is being unloaded, which cause segfault when those objects will be released.
		// It is the responsibility of the application to ensure correct lifetime management.

		ExsDebugAssert( this->_registeredObjects.empty() );
	}


	RSBaseObject* RSObjectManager::GetObjectByID(RSBaseObjectID baseObjectID) const
	{
		auto registeredObjectInfoIter = this->_registeredObjects.find(baseObjectID);
		if (registeredObjectInfoIter == this->_registeredObjects.end())
			return nullptr;

		ExsDebugAssert( registeredObjectInfoIter->value.objectPtr );
		return registeredObjectInfoIter->value.objectPtr;
	}


	void RSObjectManager::RegisterObject(RSBaseObjectType baseObjectType, const RSBaseObjectHandle& object)
	{
		RSBaseObjectID baseObjectID = object->GetBaseObjectID();
		RSBaseObjectType specifiedObjectBaseType = object->GetBaseObjectType();
		
		if (specifiedObjectBaseType != baseObjectType)
		{
			ExsDebugInterrupt();
			return;
		}

		ExsEnterCriticalSection(this->_accessLock);
		{
			ObjectInfo objectInfo { };
			objectInfo.baseObjectType = baseObjectType;
			objectInfo.objectPtr = object.get();
			objectInfo.regCount = this->_regCount.fetch_add(1, std::memory_order_relaxed);

			ExsDebugCode(
				auto existingObjectInfoIter = this->_registeredObjects.find(baseObjectID);
				ExsDebugAssert( existingObjectInfoIter == this->_registeredObjects.end() );
			);

			this->_registeredObjects.insert(baseObjectID, objectInfo);
			this->_registeredObjectsNum.fetch_add(1, std::memory_order_acq_rel);
		}
		ExsLeaveCriticalSection();
	}


	void RSObjectManager::UnregisterObject(RSBaseObjectType baseObjectType, RSBaseObjectID baseObjectID, RSBaseObject* object)
	{
		RSBaseObjectType specifiedObjectBaseType = object->GetBaseObjectType();
		
		if (specifiedObjectBaseType != baseObjectType)
		{
			ExsDebugInterrupt();
			return;
		}

		ExsEnterCriticalSection(this->_accessLock);
		{
			auto registeredObjectInfoIter = this->_registeredObjects.find(baseObjectID);
			ExsDebugAssert( registeredObjectInfoIter != this->_registeredObjects.end() );

			// Do not remove: used to explicitly control the moment of destruction.
			// registeredObjectInfoIter->value.objectPtr = nullptr;

			this->_registeredObjects.erase(registeredObjectInfoIter);
			this->_registeredObjectsNum.fetch_sub(1, std::memory_order_acq_rel);
		}
		ExsLeaveCriticalSection();
	}


	void RSObjectManager::Cleanup()
	{
		if (!this->_registeredObjects.empty())
		{
			ExsEnterCriticalSection(this->_accessLock);
			{
				auto rbegin = this->_registeredObjects.rbegin();
				auto rend = this->_registeredObjects.rend();

				for (auto current = rbegin; current != rend; ++current)
				{
					auto& objectInfo = current->value;
					objectInfo.objectPtr = nullptr;
				}
			}
			ExsLeaveCriticalSection();
		}
	}


	void RSObjectManager::ReleaseObjects()
	{
		// Normally, all RS objects are destroyed by client code when they are no longer needed. This method
		// is used in very special cases, when objects cannot be released in normal way (for example: it is
		// always called when RenderSystem is being released).

		if (!this->_registeredObjects.empty())
		{
			ExsEnterCriticalSection(this->_accessLock);
			{
				// RS objects may depend on each other - for example, GraphicsShaderStateDescriptor holds
				// active references  to all shader objects it contains. To allow proper handling of such
				// scenarios and allow detection of possible memory leaks, we release objects in strict order:
				// StateObjects -> StateDescriptors -> Resources, with each group sorted from the most recent.
				
				auto rbegin = this->_registeredObjects.rbegin();
				auto rend = this->_registeredObjects.rend();
				
				for (auto current = rbegin; current != rend; ++current)
				{
					auto& objectInfo = current->value;
					objectInfo.objectPtr = nullptr;
				}
			}
			ExsLeaveCriticalSection();
		}
	}


}
