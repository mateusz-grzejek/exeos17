
#pragma once

#ifndef __Exs_RenderSystem_RSThreadState_H__
#define __Exs_RenderSystem_RSThreadState_H__

#include "Prerequisites.h"


namespace Exs
{


	struct RSThreadCoreData;

	class RSContextCommandQueue;
	class RSContextStateController;
	class RSThreadManager;


	enum : Enum
	{
		RSThreadState_Reserved = 0x010000
	};


	enum : Uint32
	{
		TLS_Index_RS_Thread_State = 3
	};


	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM RSThreadState
	{
		friend class RenderSystem;
		friend class RSThreadManager;
		friend class RSContextCommandQueue;
		friend class RSContextStateController;

	protected:
		RenderSystem*     _renderSystem;
		RSThreadManager*  _rsThreadManager;
		RSThreadType _rsThreadType;
		RSThreadIndex _rsThreadIndex;
		RSThread*         _rsThreadObject;
		std::unique_ptr<RSContextCommandQueue>  _rsContextCommandQueue;
		std::unique_ptr<RSContextStateController>  _rsContextStateController;

	public:
		RSThreadState(RenderSystem* renderSystem, RSThreadManager* rsThreadManager, RSThreadType rsThreadType, RSThreadIndex rsThreadIndex);

		virtual ~RSThreadState();

		//
		bool Attach();

		//
		void Detach();

		//
		void Reset();

		//
		template <class Command_queue_t, class... Args>
		Command_queue_t* CreateCommandQueue(Args&&... args);

		//
		template <class State_controller_t, class... Args>
		State_controller_t* CreateStateController(Args&&... args);

		RenderSystem* GetRenderSystem() const;
		RSThreadType GetRSThreadType() const;
		RSThreadIndex GetRSThreadIndex() const;
		RSThread* GetThreadObject() const;

		RSContextCommandQueue* GetContextCommandQueue() const;
		RSContextStateController* GetContextStateController() const;

	friendapi:
		//
		void SetThreadObject(RSThread* threadObject);

	private:
		//
		virtual void OnAttach();
		
		//
		virtual void OnDetach();
	};
	

	template <class Command_queue_t, class... Args>
	inline Command_queue_t* RSThreadState::CreateCommandQueue(Args&&... args)
	{
		ExsDebugAssert( !this->_rsContextCommandQueue );
		this->_rsContextCommandQueue = std::make_unique<Command_queue_t>(std::forward<Args>(args)...);
		return static_cast<Command_queue_t*>(this->_rsContextCommandQueue.get());
	}
	
	template <class State_controller_t, class... Args>
	inline State_controller_t* RSThreadState::CreateStateController(Args&&... args)
	{
		ExsDebugAssert( !this->_rsContextStateController );
		this->_rsContextStateController = std::make_unique<State_controller_t>(std::forward<Args>(args)...);
		return static_cast<State_controller_t*>(this->_rsContextStateController.get());
	}
	
	inline RenderSystem* RSThreadState::GetRenderSystem() const
	{
		return this->_renderSystem;
	}

	inline RSThreadType RSThreadState::GetRSThreadType() const
	{
		return this->_rsThreadType;
	}

	inline RSThreadIndex RSThreadState::GetRSThreadIndex() const
	{
		return this->_rsThreadIndex;
	}

	inline RSThread* RSThreadState::GetThreadObject() const
	{
		return this->_rsThreadObject;
	}

	inline RSContextCommandQueue* RSThreadState::GetContextCommandQueue() const
	{
		return this->_rsContextCommandQueue.get();
	}

	inline RSContextStateController* RSThreadState::GetContextStateController() const
	{
		return this->_rsContextStateController.get();
	}


}


#endif /* __Exs_RenderSystem_RSThreadState_H__ */
