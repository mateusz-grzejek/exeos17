
#pragma once

#ifndef __Exs_RenderSystem_GeometryBuffer_H__
#define __Exs_RenderSystem_GeometryBuffer_H__

#include "GPUBuffer.h"


namespace Exs
{


	enum class GeometryBufferType : Enum
	{
		Index_Buffer = static_cast<Enum>(GPUBufferType::Index_Buffer),

		Vertex_Buffer = static_cast<Enum>(GPUBufferType::Vertex_Buffer)
	};

	
	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM GeometryBuffer : public GPUBuffer
	{
		EXS_DECLARE_NONCOPYABLE(GeometryBuffer);

	public:
		GeometryBuffer(RenderSystem* renderSystem, RSBaseObjectID baseObjectID, GeometryBufferType bufferType);
		virtual ~GeometryBuffer();
		
		///<summary>
		///</summary>
		bool BeginDynamicUpdate(GPUBufferUpdateMode updateMode);
		
		///<summary>
		///</summary>
		bool BeginDynamicUpdate(const RSMemoryRange& range, GPUBufferUpdateMode updateMode);
		
		///<summary>
		///</summary>
		void EndDynamicUpdate();
		
		///<summary>
		///</summary>
		void Update(const RSMemoryRange& range, const void* data, Size_t dataSize, bool autoFlush);
		
		///<summary>
		///</summary>
		void CopySubdata(const RSMemoryRange& range, const void* data, Size_t dataSize);
	};


}


#endif /* __Exs_RenderSystem_GeometryBuffer_H__ */
