
#pragma once

#ifndef __Exs_Engine_TextureBase_H__
#define __Exs_Engine_TextureBase_H__

#include <ExsRenderSystem/Prerequisites.h>


namespace Exs
{


	class Texture;
	class TextureStorage;


	///<summary>
	///</summary>
	enum class TextureType : Enum
	{
		Tex_2D,

		Tex_2D_Array,

		Tex_2D_Multisample,

		Tex_3D,

		Tex_Cube_Map
	};


	enum class CubeMapFace : Enum
	{
		Positive_X,
		Negative_X,
		Positive_Y,
		Negative_Y,
		Positive_Z,
		Negative_Z
	};


	struct TextureDimensions
	{
	public:
		Vector2U32 size;
		Uint32 depth;
		Uint32 arraySize;

	public:
		TextureDimensions()
		: depth(0)
		, arraySize(0)
		{ }

		TextureDimensions(Uint32 width, Uint32 height = 1, Uint32 depth = 1, Uint32 arraySize = 1)
		: size(width, height)
		, depth(depth)
		, arraySize(arraySize)
		{ }
	};

	
	///<summary>
	///</summary>
	struct TextureRange
	{
		//
		Vector3U32 offset;

		//
		Vector3U32 size;
	};

	
	///<summary>
	///</summary>
	struct TextureData2DDesc
	{
		// Pointer to the data.
		const void* dataPtr;

		// Size of the data, in bytes.
		Size_t dataSize;

		//
		Uint32 rowPitch;
	};

	
	///<summary>
	///</summary>
	struct TextureData2DArrayDesc
	{
		const TextureData2DDesc* arrayData;

		const Uint32* arrayIndex;

		Uint32 arraySize;
	};

	
	///<summary>
	///</summary>
	struct TextureData3DDesc
	{
		// Pointer to the data.
		const void* dataPtr;

		// Size of the data, in bytes.
		Uint32 dataSize;

		//
		Uint32 rowPitch;

		//
		Uint32 slicePitch;
	};

	
	///<summary>
	///</summary>
	struct TextureDataCubeMapDesc
	{
		TextureData2DDesc faceData[6];
	};

	
	///<summary>
	///</summary>
	struct TextureCreateInfo
	{
		Uint32 width;

		Uint32 height;

		GraphicDataFormat internalFormat;
		
		RSMemoryAccess memoryAccess;
		
		stdx::mask<RSResourceUsageFlags> usageFlags;

		Uint32 mipmapLevelsNum = 32;
	};


	struct Texture2DCreateInfo : public TextureCreateInfo
	{
		TextureData2DDesc initDataDesc;
	};


	struct Texture2DArrayCreateInfo : public TextureCreateInfo
	{
		Uint32 arraySize;

		TextureData2DArrayDesc initDataDesc;
	};


	struct Texture2DMultisampleCreateInfo : public TextureCreateInfo
	{
	};


	struct Texture3DCreateInfo : public TextureCreateInfo
	{
		Uint32 depth;

		TextureData3DDesc initDataDesc;
	};


	struct TextureCubeMapCreateInfo : public TextureCreateInfo
	{
		TextureDataCubeMapDesc initDataDesc;
	};


}


#endif /* __Exs_Engine_TextureBase_H__ */
