
#pragma once

#ifndef __Exs_RenderSystem_GPUBuffer_H__
#define __Exs_RenderSystem_GPUBuffer_H__

#include "GPUBufferBase.h"


namespace Exs
{


	class GPUBufferStorage;

	
	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM GPUBuffer : public RSResource
	{
		EXS_DECLARE_NONCOPYABLE(GPUBuffer);

	protected:
		GPUBufferType      _bufferType;
		Size_t             _size;
		GPUBufferStorage*  _bufferStorage;

	public:
		GPUBuffer(RenderSystem* renderSystem, RSBaseObjectID baseObjectID, GPUBufferType bufferType);
		virtual ~GPUBuffer();

		template <RSMemoryAccess Access>
		RSMappedMemoryPtr<Byte, Access> GetMappedMemoryPtr() const;

		///<summary>
		///</summary>
		GPUBufferType GetBufferType() const;

		///<summary>
		///</summary>
		Size_t GetSize() const;

	protected:
		virtual void OnBindStorage(RSResourceStorage* storage);
	};
	

	template <RSMemoryAccess Access>
	inline RSMappedMemoryPtr<Byte, Access> GPUBuffer::GetMappedMemoryPtr() const
	{
		RSMemoryAccess memoryAccess = static_cast<RSMemoryAccess>(this->_mappedMemoryInfo.access & AccessMode_Full);
		return RSMappedMemoryPtrGet<Access>::Get(memoryAccess, this->_mappedMemoryInfo.ptr);
	}

	inline GPUBufferType GPUBuffer::GetBufferType() const
	{
		return this->_bufferType;
	}

	inline Size_t GPUBuffer::GetSize() const
	{
		return this->_size;
	}


}


#endif /* __Exs_RenderSystem_GPUBuffer_H__ */
