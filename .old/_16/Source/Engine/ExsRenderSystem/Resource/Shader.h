
#pragma once

#ifndef __Exs_RenderSystem_Shader_H__
#define __Exs_RenderSystem_Shader_H__

#include "ShaderBase.h"
#include <stdx/data_array.h>


namespace Exs
{


	ExsDeclareRefPtrClass(Shader);

	//
	typedef stdx::dynamic_data_array<Byte> ShaderBytecode;


	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM Shader : public RSMiscObject
	{
		EXS_DECLARE_NONCOPYABLE(Shader);

	protected:
		ShaderType        _type;
		ShaderBytecode    _bytecode;
		
	public:
		Shader(RenderSystem* renderSystem, RSBaseObjectID baseObjectID, ShaderType shaderType, ShaderBytecode&& bytecode);
		virtual ~Shader();

		const ShaderBytecode& GetBytecode() const;

		ShaderType GetShaderType() const;
	};


	inline const ShaderBytecode& Shader::GetBytecode() const
	{
		return this->_bytecode;
	}

	inline ShaderType Shader::GetShaderType() const
	{
		return this->_type;
	}


}


#endif /* __Exs_RenderSystem_Shader_H__ */
