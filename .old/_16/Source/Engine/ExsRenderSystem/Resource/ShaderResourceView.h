
#pragma once

#ifndef __Exs_RenderSystem_ShaderResourceView_H__
#define __Exs_RenderSystem_ShaderResourceView_H__

#include "../Prerequisites.h"


namespace Exs
{


	class ResourceBuffer;
	class Texture2D;
	class Texture2DArray;
	class Texture2DMultisample;
	class Texture3D;
	class TextureCubeMap;

	ExsDeclareRSClassObjHandle(ShaderResourceView);


	struct ShaderResourceBinding
	{
		//
		Uint32 resourceIndex;

		//
		ShaderResourceViewHandle resourceView;

		//
		stdx::mask<ShaderStageFlags> stageAccess;
	};


	class ShaderResourceView : public RSResourceView
	{
		EXS_DECLARE_NONCOPYABLE(ShaderResourceView);

	private:
		ShaderResourceViewResType  _resourceType;
		RSResourceHandle           _resource;

	public:
		ShaderResourceView(RenderSystem* renderSystem, RSBaseObjectID baseObjectID, ShaderResourceViewResType resourceType, const RSResourceHandle& resource)
		: RSResourceView(renderSystem, baseObjectID, 0, RSResourceViewType::Shader_Resource)
		, _resourceType(resourceType)
		, _resource(resource)
		{ }
		
		RSResource* GetResource() const
		{
			return this->_resource.get();
		}

		ShaderResourceViewResType GetResourceType() const
		{
			return this->_resourceType;
		}
	};


}


#endif /* __Exs_RenderSystem_ShaderResourceView_H__ */
