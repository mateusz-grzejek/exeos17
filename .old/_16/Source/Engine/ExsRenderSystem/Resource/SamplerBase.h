
#pragma once

#ifndef __Exs_RenderSystem_SamplerBase_H__
#define __Exs_RenderSystem_SamplerBase_H__

#include "../Prerequisites.h"


namespace Exs
{


	ExsDeclareRSClassObjHandle(Sampler);


	///<summary>
	///</summary>
	enum class TextureAddressMode : Enum
	{
		Clamp_To_Border,

		Clamp_To_Edge,

		Mirror_Once,

		Mirror_Repeat,

		Repeat,
	};


	enum class TextureFilter : Enum
	{
		Linear,

		Point,

		None
	};

	
	///<summary>
	///</summary>
	struct SamplerBinding
	{
		//
		Uint32 samplerIndex;

		//
		Uint32 textureIndex;

		//
		SamplerHandle sampler;

		//
		stdx::mask<ShaderStageFlags> stageAccess;
	};


	///<summary>
	///</summary>
	struct SamplerParameters
	{
		struct AddressMode
		{
			TextureAddressMode x;
			TextureAddressMode y;
			TextureAddressMode z;
		};

		struct Filter
		{
			TextureFilter magFilter;
			TextureFilter minFilter;
			TextureFilter mipLevelSamplingFilter;
		};

		AddressMode addressMode;

		Filter filter;

		Uint32 anisotropyLevel;

		float lodBias;

		std::pair<Real, Real> lodRange;

		Color borderColor;

		bool refComparisonEnabled;

		ComparisonFunction refComparisonFunction;
	};


}


#endif /* __Exs_RenderSystem_SamplerBase_H__ */

