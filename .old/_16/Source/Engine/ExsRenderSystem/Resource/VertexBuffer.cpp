
#include "_Precompiled.h"
#include <ExsRenderSystem/Resource/VertexBuffer.h>
#include <ExsRenderSystem/Resource/GPUBufferStorage.h>


namespace Exs
{


	VertexBuffer::VertexBuffer(RenderSystem* renderSystem, RSBaseObjectID baseObjectID)
	: GeometryBuffer(renderSystem, baseObjectID, GeometryBufferType::Vertex_Buffer)
	{ }


	VertexBuffer::~VertexBuffer()
	{ }


}
