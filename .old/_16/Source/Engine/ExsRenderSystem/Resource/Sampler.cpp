
#include "_Precompiled.h"
#include <ExsRenderSystem/Resource/Sampler.h>


namespace Exs
{


	Sampler::Sampler(RenderSystem* renderSystem, RSBaseObjectID baseObjectID, const SamplerParameters& parameters)
	: RSMiscObject(renderSystem, baseObjectID, 0, RSMiscObjectType::Sampler)
	, _parameters(parameters)
	{ }


	Sampler::~Sampler()
	{ }


}
