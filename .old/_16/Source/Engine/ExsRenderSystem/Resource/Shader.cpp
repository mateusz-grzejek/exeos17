
#include "_Precompiled.h"
#include <ExsRenderSystem/Resource/Shader.h>


namespace Exs
{


	Shader::Shader(RenderSystem* renderSystem, RSBaseObjectID baseObjectID, ShaderType shaderType, ShaderBytecode&& bytecode)
	: RSMiscObject(renderSystem, baseObjectID, 0, RSMiscObjectType::Shader)
	, _type(shaderType)
	, _bytecode(std::move(bytecode))
	{ }


	Shader::~Shader()
	{ }


}
