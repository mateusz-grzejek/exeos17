
#include "_Precompiled.h"
#include <ExsRenderSystem/RSStateDescriptorCache.h>
#include <ExsRenderSystem/State/BlendState.h>
#include <ExsRenderSystem/State/DepthStencilState.h>
#include <ExsRenderSystem/State/GraphicsShaderState.h>
#include <ExsRenderSystem/State/RasterizerState.h>
#include <ExsRenderSystem/State/InputLayoutState.h>
#include <ExsRenderSystem/State/VertexStreamState.h>

/*
namespace Exs
{


	RSStateDescriptorCache::RSStateDescriptorCache(RenderSystem* renderSystem)
	: _renderSystem(renderSystem)
	{ }


	RSStateDescriptorCache::~RSStateDescriptorCache()
	{ }


	void RSStateDescriptorCache::RegisterDescriptor(const RSStateDescriptorHandle& descriptor)
	{
	}


	void RSStateDescriptorCache::Reset()


	RSStateDescriptorInfo<BlendStateDescriptorHandle> RSStateDescriptorCache::GetBlendStateDescriptor(const BlendConfiguration& configuration) const
	{
		Uint32 configHash = GetHashCode<SDBM>(configuration).value;
		RSStateDescriptorPID descriptorPID = ((static_cast<Uint64>(RSStateDescriptorType::Blend) << 32) | configHash);
		BlendStateDescriptorHandle descriptorHandle = nullptr;

		auto descriptorRef = this->_cachedDescriptors.find(descriptorPID);
		if (descriptorRef != this->_cachedDescriptors.end())
		{
			RSStateDescriptorType descriptorType = descriptorRef->value->GetDescriptorType();
			if (descriptorType == RSStateDescriptorType::Blend)
			{
				descriptorHandle = stdx::dbgsafe_shared_ptr_cast<BlendStateDescriptor>(descriptorRef->value);
			}
		}

		return RSStateDescriptorInfo<BlendStateDescriptorHandle> { descriptorHandle, descriptorPID };
	}


	RSStateDescriptorInfo<DepthStencilStateDescriptorHandle> RSStateDescriptorCache::GetDepthStencilStateDescriptor(const DepthStencilConfiguration& configuration) const
	{
		Uint32 configHash = GetHashCode<SDBM>(configuration).value;
		RSStateDescriptorPID descriptorPID = ((static_cast<Uint64>(RSStateDescriptorType::Depth_Stencil) << 32) | configHash);
		DepthStencilStateDescriptorHandle descriptorHandle = nullptr;

		auto descriptorRef = this->_cachedDescriptors.find(descriptorPID);
		if (descriptorRef != this->_cachedDescriptors.end())
		{
			RSStateDescriptorType descriptorType = descriptorRef->value->GetDescriptorType();
			if (descriptorType == RSStateDescriptorType::Depth_Stencil)
			{
				descriptorHandle = stdx::dbgsafe_shared_ptr_cast<DepthStencilStateDescriptor>(descriptorRef->value);
			}
		}

		return RSStateDescriptorInfo<DepthStencilStateDescriptorHandle> { descriptorHandle, descriptorPID };
	}


	RSStateDescriptorInfo<GraphicsShaderStateDescriptorHandle> RSStateDescriptorCache::GetGraphicsShaderStateDescriptor(const GraphicsShaderConfiguration& configuration) const
	{
		Uint32 configHash = GetHashCode<SDBM>(configuration).value;
		RSStateDescriptorPID descriptorPID = ((static_cast<Uint64>(RSStateDescriptorType::Graphics_Shader) << 32) | configHash);
		GraphicsShaderStateDescriptorHandle descriptorHandle = nullptr;

		auto descriptorRef = this->_cachedDescriptors.find(descriptorPID);
		if (descriptorRef != this->_cachedDescriptors.end())
		{
			RSStateDescriptorType descriptorType = descriptorRef->value->GetDescriptorType();
			if (descriptorType == RSStateDescriptorType::Graphics_Shader)
			{
				descriptorHandle = stdx::dbgsafe_shared_ptr_cast<GraphicsShaderStateDescriptor>(descriptorRef->value);
			}
		}

		return RSStateDescriptorInfo<GraphicsShaderStateDescriptorHandle> { descriptorHandle, descriptorPID };
	}


	RSStateDescriptorInfo<InputLayoutStateDescriptorHandle> RSStateDescriptorCache::GetInputLayoutStateDescriptor(const InputLayoutConfiguration& configuration) const
	{
		Uint32 configHash = GetHashCode<SDBM>(configuration).value;
		RSStateDescriptorPID descriptorPID = ((static_cast<Uint64>(RSStateDescriptorType::Input_Layout) << 32) | configHash);
		InputLayoutStateDescriptorHandle descriptorHandle = nullptr;

		auto descriptorRef = this->_cachedDescriptors.find(descriptorPID);
		if (descriptorRef != this->_cachedDescriptors.end())
		{
			RSStateDescriptorType descriptorType = descriptorRef->value->GetDescriptorType();
			if (descriptorType == RSStateDescriptorType::Input_Layout)
			{
				descriptorHandle = stdx::dbgsafe_shared_ptr_cast<InputLayoutStateDescriptor>(descriptorRef->value);
			}
		}

		return RSStateDescriptorInfo<InputLayoutStateDescriptorHandle> { descriptorHandle, descriptorPID };
	}


	RSStateDescriptorInfo<RasterizerStateDescriptorHandle> RSStateDescriptorCache::GetRasterizerStateDescriptor(const RasterizerConfiguration& configuration) const
	{
		Uint32 configHash = GetHashCode<SDBM>(configuration).value;
		RSStateDescriptorPID descriptorPID = ((static_cast<Uint64>(RSStateDescriptorType::Rasterizer) << 32) | configHash);
		RasterizerStateDescriptorHandle descriptorHandle = nullptr;

		auto descriptorRef = this->_cachedDescriptors.find(descriptorPID);
		if (descriptorRef != this->_cachedDescriptors.end())
		{
			RSStateDescriptorType descriptorType = descriptorRef->value->GetDescriptorType();
			if (descriptorType == RSStateDescriptorType::Rasterizer)
			{
				descriptorHandle = stdx::dbgsafe_shared_ptr_cast<RasterizerStateDescriptor>(descriptorRef->value);
			}
		}

		return RSStateDescriptorInfo<RasterizerStateDescriptorHandle> { descriptorHandle, descriptorPID };
	}


	RSStateDescriptorInfo<VertexStreamStateDescriptorHandle> RSStateDescriptorCache::GetVertexStreamStateDescriptor(const VertexStreamConfiguration& configuration) const
	{
		Uint32 configHash = GetHashCode<SDBM>(configuration).value;
		RSStateDescriptorPID descriptorPID = ((static_cast<Uint64>(RSStateDescriptorType::Vertex_Stream) << 32) | configHash);
		VertexStreamStateDescriptorHandle descriptorHandle = nullptr;

		auto descriptorRef = this->_cachedDescriptors.find(descriptorPID);
		if (descriptorRef != this->_cachedDescriptors.end())
		{
			RSStateDescriptorType descriptorType = descriptorRef->value->GetDescriptorType();
			if (descriptorType == RSStateDescriptorType::Vertex_Stream)
			{
				descriptorHandle = stdx::dbgsafe_shared_ptr_cast<VertexStreamStateDescriptor>(descriptorRef->value);
			}
		}

		return RSStateDescriptorInfo<VertexStreamStateDescriptorHandle> { descriptorHandle, descriptorPID };
	}


}
*/