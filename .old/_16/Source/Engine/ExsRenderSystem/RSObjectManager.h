
#pragma once

#ifndef __Exs_RenderSystem_RSObjectManager_H__
#define __Exs_RenderSystem_RSObjectManager_H__

#include "RSObjectFactory.h"
#include <stdx/assoc_array.h>


namespace Exs
{


	class RSObjectFactory;
	class RSObjectManager;


	template <RSBaseObjectType>
	struct RSObjectClassTraits;
	
	template <>
	struct RSObjectClassTraits<RSBaseObjectType::Misc_Object>
	{
		typedef RSMiscObject Type;
	};
	
	template <>
	struct RSObjectClassTraits<RSBaseObjectType::Resource>
	{
		typedef RSResource Type;
	};

	template <>
	struct RSObjectClassTraits<RSBaseObjectType::Resource_View>
	{
		typedef RSResourceView Type;
	};

	template <>
	struct RSObjectClassTraits<RSBaseObjectType::State_Descriptor>
	{
		typedef RSStateDescriptor Type;
	};

	template <>
	struct RSObjectClassTraits<RSBaseObjectType::State_Object>
	{
		typedef RSStateObject Type;
	};


	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM RSObjectManager
	{
		EXS_DECLARE_NONCOPYABLE(RSObjectManager);

	private:
		struct ObjectInfo
		{
			// Base type of the object.
			RSBaseObjectType baseObjectType;

			// Active ref pointer to the object.
			RSBaseObject* objectPtr;

			// Reg counter (unique, represents total registration order).
			Uint64 regCount;
		};

		typedef stdx::assoc_array<RSBaseObjectID, ObjectInfo> RegisteredObjectList;

	private:
		RenderSystem*         _renderSystem; // Render system instance.
		RSObjectFactory*      _rsObjectFactory; // Factory used for objects creation.
		RegisteredObjectList  _registeredObjects; // List of all registered (active) objects.
		std::atomic<Uint32>   _registeredObjectsNum; // Number of all registered objects.
		std::atomic<Uint64>   _regCount;
		LightMutex            _accessLock;

	public:
		RSObjectManager(RenderSystem* renderSystem, RSObjectFactory* rsObjectFactory);
		~RSObjectManager();

		///<summary>
		///</summary>
		template <class Misc_object_t, class Render_system_t, class... Args>
		RSHandle<Misc_object_t> NewMiscObject(Render_system_t* renderSystem, Args&&... args);

		///<summary>
		///</summary>
		template <class Resource_t, class Render_system_t, class... Args>
		RSHandle<Resource_t> NewResource(Render_system_t* renderSystem, Args&&... args);

		///<summary>
		///</summary>
		template <class Resource_view_t, class Render_system_t, class... Args>
		RSHandle<Resource_view_t> NewResourceView(Render_system_t* renderSystem, Args&&... args);

		///<summary>
		///</summary>
		template <class State_descriptor_t, class Render_system_t, class... Args>
		RSHandle<State_descriptor_t> NewStateDescriptor(Render_system_t* renderSystem, Args&&... args);

		///<summary>
		///</summary>
		template <class State_object_t, class Render_system_t, class... Args>
		RSHandle<State_object_t> NewStateObject(Render_system_t* renderSystem, Args&&... args);

		///<summary>
		///</summary>
		void OnMiscObjectDestroy(RSBaseObjectID miscObjectID, RSMiscObject* miscObject);

		///<summary>
		///</summary>
		void OnResourceDestroy(RSBaseObjectID resourceID, RSResource* resource);

		///<summary>
		///</summary>
		void OnResourceViewDestroy(RSBaseObjectID resourceViewID, RSResourceView* resourceView);

		///<summary>
		///</summary>
		void OnStateDescriptorDestroy(RSBaseObjectID stateDescriptorID, RSStateDescriptor* stateDescriptor);

		///<summary>
		///</summary>
		void OnStateObjectDestroy(RSBaseObjectID stateObjectID, RSStateObject* stateObject);
		
		///<summary>
		///</summary>
		void Cleanup();
		
		///<summary>
		///</summary>
		void ReleaseObjects();

		///<summary>
		///</summary>
		Size_t GetRegisteredObjectsNum() const;

		///<summary>
		///</summary>
		RSBaseObject* GetObjectByID(RSBaseObjectID baseObjectID) const;

		///<summary>
		///</summary>
		template <RSBaseObjectType Object_t>
		typename RSObjectClassTraits<Object_t>::Type* GetObjectByIDType(RSBaseObjectID baseObjectID) const;

	private:
		//
		void RegisterObject(RSBaseObjectType baseObjectType, const RSBaseObjectHandle& object);

		//
		void UnregisterObject(RSBaseObjectType baseObjectType, RSBaseObjectID baseObjectID, RSBaseObject* object);
	};
	
	
	template <class Misc_object_t, class Render_system_t, class... Args>
	inline RSHandle<Misc_object_t> RSObjectManager::NewMiscObject(Render_system_t* renderSystem, Args&&... args)
	{
		auto miscObjectPtr = this->_rsObjectFactory->CreateMiscObject<Misc_object_t>(renderSystem, std::forward<Args>(args)...);
		this->RegisterObject(RSBaseObjectType::Misc_Object, miscObjectPtr);
		return miscObjectPtr;
	}

	template <class Resource_t, class Render_system_t, class... Args>
	inline RSHandle<Resource_t> RSObjectManager::NewResource(Render_system_t* renderSystem, Args&&... args)
	{
		auto resourcePtr = this->_rsObjectFactory->CreateResource<Resource_t>(renderSystem, std::forward<Args>(args)...);
		this->RegisterObject(RSBaseObjectType::Resource, resourcePtr);
		return resourcePtr;
	}

	template <class Resource_view_t, class Render_system_t, class... Args>
	inline RSHandle<Resource_view_t> RSObjectManager::NewResourceView(Render_system_t* renderSystem, Args&&... args)
	{
		auto resourceViewPtr = this->_rsObjectFactory->CreateResourceView<Resource_view_t>(renderSystem, std::forward<Args>(args)...);
		this->RegisterObject(RSBaseObjectType::Resource_View, resourceViewPtr);
		return resourceViewPtr;
	}

	template <class State_descriptor_t, class Render_system_t, class... Args>
	inline RSHandle<State_descriptor_t> RSObjectManager::NewStateDescriptor(Render_system_t* renderSystem, Args&&... args)
	{
		auto stateDescriptorPtr = this->_rsObjectFactory->CreateStateDescriptor<State_descriptor_t>(renderSystem, std::forward<Args>(args)...);
		this->RegisterObject(RSBaseObjectType::State_Descriptor, stateDescriptorPtr);
		return stateDescriptorPtr;
	}

	template <class State_object_t, class Render_system_t, class... Args>
	inline RSHandle<State_object_t> RSObjectManager::NewStateObject(Render_system_t* renderSystem, Args&&... args)
	{
		auto stateObjectPtr = this->_rsObjectFactory->CreateStateObject<State_object_t>(renderSystem, std::forward<Args>(args)...);
		this->RegisterObject(RSBaseObjectType::State_Object, stateObjectPtr);
		return stateObjectPtr;
	}

	inline void RSObjectManager::OnMiscObjectDestroy(RSBaseObjectID miscObjectID, RSMiscObject* miscObject)
	{
		ExsDebugAssert( (miscObject != nullptr) && (miscObject->GetBaseObjectID() == miscObjectID) );
		this->UnregisterObject(RSBaseObjectType::Misc_Object, miscObjectID, miscObject);
	}

	inline void RSObjectManager::OnResourceDestroy(RSBaseObjectID resourceID, RSResource* resource)
	{
		ExsDebugAssert( (resource != nullptr) && (resource->GetBaseObjectID() == resourceID) );
		this->UnregisterObject(RSBaseObjectType::Resource, resourceID, resource);
	}

	inline void RSObjectManager::OnResourceViewDestroy(RSBaseObjectID resourceViewID, RSResourceView* resourceView)
	{
		ExsDebugAssert( (resourceView != nullptr) && (resourceView->GetBaseObjectID() == resourceViewID) );
		this->UnregisterObject(RSBaseObjectType::Resource_View, resourceViewID, resourceView);
	}

	inline void RSObjectManager::OnStateDescriptorDestroy(RSBaseObjectID stateDescriptorID, RSStateDescriptor* stateDescriptor)
	{
		ExsDebugAssert( (stateDescriptor != nullptr) && (stateDescriptor->GetBaseObjectID() == stateDescriptorID) );
		this->UnregisterObject(RSBaseObjectType::State_Descriptor, stateDescriptorID, stateDescriptor);
	}

	inline void RSObjectManager::OnStateObjectDestroy(RSBaseObjectID stateObjectID, RSStateObject* stateObject)
	{
		ExsDebugAssert( (stateObject != nullptr) && (stateObject->GetBaseObjectID() == stateObjectID) );
		this->UnregisterObject(RSBaseObjectType::State_Object, stateObjectID, stateObject);
	}

	inline Size_t RSObjectManager::GetRegisteredObjectsNum() const
	{
		return this->_registeredObjectsNum.load(std::memory_order_acquire);
	}

	template <RSBaseObjectType Object_t>
	inline typename RSObjectClassTraits<Object_t>::Type* RSObjectManager::GetObjectByIDType(RSBaseObjectID baseObjectID) const
	{
		if (RSBaseObject* registeredObjectPtr = this->GetObjectByID(baseObjectID))
		{
			ExsDebugAssert( registeredObjectPtr->GetBaseObjectType() == Object_t );
			return dbgsafe_ptr_cast<typename RSObjectClassTraits<Object_t>::Type*>(registeredObjectPtr);
		}

		return nullptr;
	}


}


#endif /* __Exs_RenderSystem_RSObjectManager_H__ */
