
#pragma once

#ifndef __Exs_RenderSystem_DeviceMemory_H__
#define __Exs_RenderSystem_DeviceMemory_H__

#include "RSMemoryCommon.h"


namespace Exs
{


	class DeviceMemoryPool;


	///<summary>
	///</summary>
	enum class DeviceMemoryType : Enum
	{
		//
		System_Memory,

		//
		Video_Memory
	};

	
	struct DeviceMemoryBlock
	{
		//
		DeviceMemoryPool* pool;

		//
		Uint64 offset;

		//
		Uint64 length;
	};


	///<summary>
	///</summary>
	struct DeviceMemoryPoolProperties
	{
		//
		RS_memory_size_t baseAlignment;

		//
		DeviceMemoryType memoryType;
	};

	
	///<summary>
	/// D3D12: D3D12_MEMORY_POOL (L0/L1)
	/// Vulkan: VkMemoryType (VkMemoryHeap)
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM DeviceMemoryPool
	{
	public:
		typedef std::list<DeviceMemoryBlock> MemoryBlockList;
		typedef MemoryBlockList::iterator MemoryBlockRef;

	protected:
		RSMemorySystem*             _memorySystem;
		Uint64                      _size;
		DeviceMemoryPoolProperties  _properties;
		MemoryBlockList             _allocatedBlocks;
		std::atomic<Uint64>         _allocatedSize;

	public:
		///<summary>
		///</summary>
		DeviceMemoryPool(RSMemorySystem* memorySystem, Uint64 size, const DeviceMemoryPoolProperties& properties)
		: _memorySystem(memorySystem)
		, _size(size)
		, _properties(properties)
		, _allocatedSize(0)
		{ }

		virtual ~DeviceMemoryPool()
		{ }

		///<summary>
		///</summary>
        virtual Result Reset() = 0;
		
		///<summary>
		///</summary>
		DeviceMemoryBlock* RegisterAllocation(Uint64 offset, Uint64 size)
		{
			auto blockRef = this->_allocatedBlocks.insert(this->_allocatedBlocks.end(), DeviceMemoryBlock());
			blockRef->pool = this;
			blockRef->offset = offset;
			blockRef->length = size;

			Uint64 currSize = this->_allocatedSize.load(std::memory_order_acquire);
			this->_allocatedSize.store(currSize + size, std::memory_order_release);

			return &(*blockRef);
		}

		///<summary>
		///</summary>
		bool IsMemoryAvailable(Uint64 memSize) const
		{
			Uint64 usedSize = this->_allocatedSize.load(std::memory_order_relaxed);
			return this->_size - usedSize >= memSize;
		}

		///<summary>
		///</summary>
		const DeviceMemoryPoolProperties& GetProperties() const
		{
			return this->_properties;
		}

		///<summary>
		///</summary>
		Uint64 GetSize() const
		{
			return this->_size;
		}

		///<summary>
		///</summary>
		Uint64 GetAllocatedSize() const
		{
			return this->_allocatedSize.load(std::memory_order_relaxed);
		}
	};


}


#endif /* __Exs_RenderSystem_DeviceMemory_H__ */
