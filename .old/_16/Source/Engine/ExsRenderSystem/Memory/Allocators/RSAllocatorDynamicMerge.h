
#pragma once

#ifndef __Exs_RenderSystem_RSAllocatorBlockMerge_H__
#define __Exs_RenderSystem_RSAllocatorBlockMerge_H__

#include <ExsRenderSystem/Memory/RSAllocator.h>
#include <stdx/sorted_array.h>


namespace Exs
{


	struct RSAllocatorDynamicMergeConfig
	{
		RS_memory_size_t maxAllocOversize = 4096;

		RS_memory_size_t minBlockSize = 4096;
	};


	class RSAllocatorDynamicMerge : public RSAllocator
	{
	private:
		enum class MemoryBlockType : Enum
		{
			Active,
			Free
		};

		struct MemoryBlock
		{
			MemoryBlockType blockType;
			RS_memory_size_t offset;
			RS_memory_size_t size;
			MemoryReference memRef; // Set after block is allocated, otherwise undefined.
		};

		struct FreeBlockRef
		{
			RS_memory_size_t offset;
			RS_memory_size_t size;
			MemoryBlock* block;
		};

		struct MemoryBlockCmp
		{
			bool operator()(const MemoryBlock& left, const MemoryBlock& right) const
			{
				return left.offset < right.offset;
			}

			bool operator()(const MemoryBlock& left, const RS_memory_size_t& right) const
			{
				return left.offset < right;
			}
		};

		struct FreeBlockRefCmp
		{
			bool operator()(const FreeBlockRef& left, const FreeBlockRef& right) const
			{
				return (left.size < right.size) || ((left.size == right.size) && (left.offset < right.offset));
			}

			bool operator()(const FreeBlockRef& left, const RS_memory_size_t& right) const
			{
				return left.size < right;
			}
		};

		typedef stdx::sorted_array<MemoryBlock, MemoryBlockCmp> MemoryBlockArray;
		typedef stdx::sorted_array<FreeBlockRef, FreeBlockRefCmp> FreeBlockArrayView;
		typedef MemoryBlockArray::iterator MemoryBlockIter;

	private:
		RSAllocatorDynamicMergeConfig    _config;
		RS_memory_size_t                 _allocOffset;
		MemoryBlockArray                 _memoryBlocks;
		FreeBlockArrayView                _freeBlockRefs;

	public:
		RSAllocatorDynamicMerge(RSMemoryHeap* heap, RS_memory_size_t memorySize)
		: RSAllocator(heap, memorySize)
		{ }

		RSAllocatorDynamicMerge(RSMemoryHeap* heap, RS_memory_size_t memorySize, const RSAllocatorDynamicMergeConfig& allocatorConfig)
		: RSAllocator(heap, memorySize)
		, _config(allocatorConfig)
		{ }

		virtual ~RSAllocatorDynamicMerge()
		{ }

		RSMemoryRefData* Alloc(RS_memory_size_t size)
		{
			RSMemoryRefData* memoryPtr = this->_allocFreeMemoryBlock(size);

			if (memoryPtr == nullptr)
			{
				RS_memory_size_t availableMemory = this->_memorySize - this->_allocOffset;

				if (availableMemory >= size)
				{
					MemoryBlock allocBlock { };
					allocBlock.offset = this->_allocOffset;
					allocBlock.size = size;

					memoryPtr = this->_registerBlockAllocation(&allocBlock);

					this->_memoryBlocks.insert(allocBlock);
					this->_allocOffset += size;
				}
			}

			return memoryPtr;
		}

		void Free(RSMemoryRefData* memoryPtr)
		{
			ExsDebugAssert( memoryPtr != nullptr );
			RS_memory_size_t blockOffset = static_cast<RS_memory_size_t>(memoryPtr->memoryOffset);

			auto blockRef = this->_memoryBlocks.lower_bound(blockOffset);
			ExsDebugAssert( blockRef != this->_memoryBlocks.end() );
			ExsDebugAssert( blockRef->offset == blockOffset );
			ExsDebugAssert( blockRef->blockType == MemoryBlockType::Active );

			auto memoryRefDataIter = blockRef->memRef;
			ExsDebugAssert( &(*memoryRefDataIter) == memoryPtr );

			blockRef->blockType = MemoryBlockType::Free;
			blockRef = this->_mergeBlockWithAdjacent(blockRef);

			FreeBlockRef freeBlockRef { };
			freeBlockRef.offset = blockRef->offset;
			freeBlockRef.size = blockRef->size;
			freeBlockRef.block = &(*blockRef);

			this->_memoryAllocations.erase(memoryRefDataIter);
		}

		void Reset(stdx::mask<Enum> flags)
		{
			// if (flags.is_set(RSAllocatorReset_Invalidate_Resource_Memory))
			// {
			// 	for (auto& memoryRefData : this->_memoryAllocations)
			// 	{
			// 		if (memoryRefData.resource != nullptr)
			// 			0;//memoryRefData.resource->InvalidateMemory(RSResourceMemoryInvalidateMode::Default);
			// 	}
			// }

			this->_memoryAllocations.clear();
			this->_memoryBlocks.clear();
			this->_freeBlockRefs.clear();
			this->_allocOffset = 0;
		}

	private:
		RSMemoryRefData* _allocFreeMemoryBlock(RS_memory_size_t size)
		{
			RSMemoryRefData* memoryPtr = nullptr;

			if (!this->_freeBlockRefs.empty())
			{
				// Find the smallest block >= required size.
				auto freeMemoryBlockRef = this->_freeBlockRefs.lower_bound(size);

				if (freeMemoryBlockRef != this->_freeBlockRefs.end())
				{
					// Compute how much memory would be wasted by allocating this block...
					RS_memory_size_t allocOversize = freeMemoryBlockRef->size - size;

					// ... and validate it against specified value (by default: 4 KB).
					if (allocOversize <= this->_config.maxAllocOversize)
					{
						// If waste of memory is acceptable, allocate memory referenced by this block.
						memoryPtr = this->_registerBlockAllocation(freeMemoryBlockRef->block);
						this->_freeBlockRefs.erase(freeMemoryBlockRef);
					}
					// Otherwise, if size of remaining block is acceptable, split this block into two parts.
					else if (allocOversize >= this->_config.minBlockSize)
					{
						MemoryBlock* memoryBlock = freeMemoryBlockRef->block;
						auto blockRef = this->_memoryBlocks.lower_bound(memoryBlock->offset);
						ExsDebugAssert( blockRef != this->_memoryBlocks.end() );
						ExsDebugAssert( blockRef->offset == memoryBlock->offset );

						MemoryBlock allocBlock { };
						allocBlock.offset = blockRef->offset;
						allocBlock.size = size;

						MemoryBlock secondBlock { };
						secondBlock.blockType = MemoryBlockType::Free;
						secondBlock.offset = allocBlock.offset + allocBlock.size;
						secondBlock.size = blockRef->size - size;

						memoryPtr = this->_registerBlockAllocation(&allocBlock);

						this->_memoryBlocks.erase(blockRef);
						this->_memoryBlocks.insert(allocBlock);
						this->_memoryBlocks.insert(secondBlock);
					}
				}
			}

			return memoryPtr;
		}

		MemoryBlockIter _mergeBlockWithAdjacent(MemoryBlockIter blockIter)
		{
			ExsDebugAssert( blockIter != this->_memoryBlocks.end() );

			if (blockIter > this->_memoryBlocks.begin())
			{
				// Reference (iterator) to the previous block.
				auto prevBlockIter = blockIter - 1;

				// Check if it is a free block.
				if (prevBlockIter->blockType == MemoryBlockType::Free)
				{
					// Extend previous block with memory of block pointed to by 'blockIter'. Remove this block.

					prevBlockIter->size += blockIter->size;
					this->_memoryBlocks.erase(blockIter);
					this->_removeFreeBlockReference(blockIter);

					// Update original iterator, so merging process can continue. Note, that next block can be still
					// safely merged, since blocks are stored in continuous memory.
					blockIter = prevBlockIter;
				}
			}

			if (this->_memoryBlocks.end() - blockIter > 1)
			{
				auto nextBlockIter = blockIter + 1;

				if (nextBlockIter->blockType == MemoryBlockType::Free)
				{
					blockIter->size += nextBlockIter->size;
					this->_memoryBlocks.erase(nextBlockIter);
					this->_removeFreeBlockReference(nextBlockIter);
				}
			}

			return blockIter;
		}

		RSMemoryRefData* _registerBlockAllocation(MemoryBlock* memoryBlock)
		{
			RSMemoryRefData memoryRefData { };
			memoryRefData.memoryOffset = memoryBlock->offset;
			memoryRefData.memorySize = memoryBlock->size;
			memoryRefData.heap = this->_heap;

			memoryBlock->blockType = MemoryBlockType::Active;
			memoryBlock->memRef = this->_memoryAllocations.insert(this->_memoryAllocations.end(), memoryRefData);

			RSMemoryRefData* memoryPtr = &(*memoryBlock->memRef);

			return memoryPtr;
		}

		void _removeFreeBlockReference(MemoryBlockIter blockIter)
		{
			// Free block references are sorted by their size. Blocks with the same size are sorted by offset in ascending order.
			auto freeBlockRefIter = this->_freeBlockRefs.lower_bound(blockIter->size);

			// There may be more than one block with this size. Because of that, we iterate through them to find proper one.
			// NOTE: if linear search becomes a performance issue, use BS (blocks with same size are also sorted by offset).
			while (freeBlockRefIter->offset != blockIter->offset)
			{
				++freeBlockRefIter;
			}

			ExsDebugAssert( freeBlockRefIter->block == &(*blockIter) );
			this->_freeBlockRefs.erase(freeBlockRefIter);
		}
	};


}


#endif /* __Exs_RenderSystem_RSAllocatorBlockMerge_H__ */
