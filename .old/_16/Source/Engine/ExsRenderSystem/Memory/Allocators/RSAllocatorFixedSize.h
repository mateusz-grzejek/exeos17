
#pragma once

#ifndef __Exs_RenderSystem_RSAllocatorFixedSize_H__
#define __Exs_RenderSystem_RSAllocatorFixedSize_H__

#include <ExsRenderSystem/Memory/RSAllocator.h>
#include <stdx/sorted_array.h>


namespace Exs
{


	struct RSAllocatorFixedSizeConfig
	{
		RS_memory_size_t allocationSize = stdx::limits<RS_memory_size_t>::max_value;
	};


	class RSAllocatorFixedSize : public RSAllocator
	{
	private:
		struct ActiveMemoryBlock
		{
			MemoryReference memRef;
			RS_memory_size_t offset;
		};
		
		struct FreeMemoryBlock
		{
			RS_memory_size_t offset;
		};

		struct ActiveMemoryBlockCmp
		{
			bool operator()(const ActiveMemoryBlock& left, const ActiveMemoryBlock& right) const
			{
				return left.offset < right.offset;
			}
			
			bool operator()(const ActiveMemoryBlock& left, const RS_memory_size_t& right) const
			{
				return left.offset < right;
			}
		};
		
		typedef stdx::sorted_array<ActiveMemoryBlock, ActiveMemoryBlockCmp> ActiveMemoryBlockArray;
		typedef std::stack<FreeMemoryBlock> FreeMemoryBlockStack;

	private:
		RSAllocatorFixedSizeConfig    _config;
		RS_memory_size_t              _allocOffset;
		ActiveMemoryBlockArray        _activeMemoryBlocks;
		FreeMemoryBlockStack          _freeMemoryBlocks;

	public:
		RSAllocatorFixedSize(RSMemoryPool* pool, RS_memory_size_t memorySize, const RSAllocatorFixedSizeConfig& allocatorConfig)
		: RSAllocator(pool, memorySize)
		{ }

		virtual ~RSAllocatorFixedSize()
		{ }
		
		RSMemoryRefData* Alloc(RS_memory_size_t size)
		{
			ExsDebugAssert( size <= this->_config.allocationSize );

			RSMemoryRefData* memoryPtr = this->_allocFreeMemoryBlock();

			if (memoryPtr == nullptr)
			{
				RS_memory_size_t availableMemory = this->_memorySize - this->_allocOffset;

				if (availableMemory >= size)
				{
					memoryPtr = this->_registerAllocation(this->_allocOffset);
					this->_allocOffset += size;
				}
			}

			return memoryPtr;
		}

		Result Free(RSMemoryRefData* memoryPtr)
		{
			ExsDebugAssert( memoryPtr != nullptr );

			RS_memory_size_t blockOffset = memoryPtr->memoryOffset;

			auto blockRef = this->_activeMemoryBlocks.find(blockOffset);
			ExsDebugAssert( blockRef != this->_activeMemoryBlocks.end() );

			auto memoryRefDataIter = blockRef->memRef;
			ExsDebugAssert( &(*memoryRefDataIter) == memoryPtr );

			// If this block is the last currently allocated block, move alloc offset instead of adding it to the list.
			if (blockOffset + memoryPtr->memorySize == this->_allocOffset)
			{
				this->_allocOffset -= memoryPtr->memorySize;
				ExsDebugAssert( this->_allocOffset == blockOffset );
			}
			else
			{
				FreeMemoryBlock freeMemoryBlock { };
				freeMemoryBlock.offset = blockRef->offset;
				this->_freeMemoryBlocks.push(freeMemoryBlock);
			}

			this->_activeMemoryBlocks.erase(blockRef);
			this->_memoryAllocations.erase(memoryRefDataIter);

			return ExsResult(RSC_Success);
		}
		
		void Reset(stdx::mask<Enum> flags)
		{
			if (flags.is_set(RSAllocatorReset_Invalidate_Resource_Memory))
			{
				for (auto& memoryRefData : this->_memoryAllocations)
				{
					if (memoryRefData.resource != nullptr)
						0;//memoryRefData.resource->InvalidateMemory(RSResourceMemoryInvalidateMode::Default);
				}
			}

			while (!this->_freeMemoryBlocks.empty())
			{
				this->_freeMemoryBlocks.pop();
			}

			this->_memoryAllocations.clear();
			this->_activeMemoryBlocks.clear();
			this->_allocOffset = 0;
		}

	private:
		RSMemoryRefData* _allocFreeMemoryBlock()
		{
			RSMemoryRefData* memoryPtr = nullptr;

			if (!this->_freeMemoryBlocks.empty())
			{
				auto freeMemoryBlock = this->_freeMemoryBlocks.top();
				memoryPtr = this->_registerAllocation(freeMemoryBlock.offset);
				this->_freeMemoryBlocks.pop();
			}

			return memoryPtr;
		}

		RSMemoryRefData* _registerAllocation(RS_memory_size_t offset)
		{
			RSMemoryRefData memoryRefData { };
			memoryRefData.memoryOffset = offset;
			memoryRefData.memorySize = this->_config.allocationSize;
			memoryRefData.pool = this->_pool;
			
			ActiveMemoryBlock activeMemoryBlock { };
			activeMemoryBlock.memRef = this->_memoryAllocations.insert(this->_memoryAllocations.end(), memoryRefData);
			activeMemoryBlock.offset = offset;
			
			RSMemoryRefData* memoryPtr = &(*activeMemoryBlock.memRef);
			this->_activeMemoryBlocks.insert(activeMemoryBlock);

			return memoryPtr;
		}
	};


}


#endif /* __Exs_RenderSystem_RSAllocatorFixedSize_H__ */
