
#pragma once

#ifndef __Exs_RenderSystem_RSAllocatorStaticBlock_H__
#define __Exs_RenderSystem_RSAllocatorStaticBlock_H__

#include "../RSAllocator.h"
#include <ExsCore/SortedArray.h>


namespace Exs
{


	struct RSAllocatorStaticBlockConfig
	{
		RS_memory_size_t maxMarginSize = 4096;
	};


	class RSAllocatorStaticBlock : public RSAllocator
	{
	private:
		struct ActiveMemoryBlock
		{
			MemoryReference memRef;
			RS_memory_size_t offset;
			RS_memory_size_t size;
		};

		struct FreeMemoryBlock
		{
			RS_memory_size_t offset;
			RS_memory_size_t size;
		};

		struct ActiveMemoryBlockCmp
		{
			bool operator()(const ActiveMemoryBlock& left, const ActiveMemoryBlock& right) const
			{
				return left.offset < right.offset;
			}
			
			bool operator()(const ActiveMemoryBlock& left, const RS_memory_size_t& right) const
			{
				return left.offset < right;
			}
		};
		
		struct FreeMemoryBlockCmp
		{
			bool operator()(const FreeMemoryBlock& left, const FreeMemoryBlock& right) const
			{
				return left.size < right.size;
			}
			
			bool operator()(const FreeMemoryBlock& left, const RS_memory_size_t& right) const
			{
				return left.size < right;
			}
		};
		
		typedef SortedArray<ActiveMemoryBlock, ActiveMemoryBlockCmp> ActiveMemoryBlockArray;
		typedef SortedArray<FreeMemoryBlock, FreeMemoryBlockCmp> FreeMemoryBlockArray;

	private:
		RSAllocatorStaticBlockConfig    _config;
		RS_memory_size_t                _allocOffset;
		ActiveMemoryBlockArray          _activeMemoryBlocks;
		FreeMemoryBlockArray            _freeMemoryBlocks;

	public:
		RSAllocatorStaticBlock(RSMemoryPool* pool, RS_memory_size_t memorySize, const RSAllocatorStaticBlockConfig& allocatorConfig)
		: RSAllocator(pool, memorySize)
		{ }

		virtual ~RSAllocatorStaticBlock()
		{ }
		
		RSMemoryRefData* Alloc(RS_memory_size_t size)
		{
			RSMemoryRefData* memoryPtr = this->_allocFreeMemoryBlock(size);

			if (memoryPtr == nullptr)
			{
				RS_memory_size_t availableMemory = this->_memorySize - this->_allocOffset;

				if (availableMemory >= size)
				{
					memoryPtr = this->_registerAllocation(this->_allocOffset, size);
					this->_allocOffset += size;
				}
			}

			return memoryPtr;
		}

		Result Free(RSMemoryRefData* memoryPtr)
		{
			ExsDebugAssert( memoryPtr != nullptr );

			RS_memory_size_t blockOffset = memoryPtr->memoryOffset;
			
			auto blockRef = this->_activeMemoryBlocks.Find(blockOffset);
			ExsDebugAssert( blockRef != this->_activeMemoryBlocks.End() );

			auto memoryRefDataIter = blockRef->memRef;
			ExsDebugAssert( &(*memoryRefDataIter) == memoryPtr );

			// If this block is the last currently allocated block, move alloc offset instead of adding it to the list.
			if (blockOffset + memoryPtr->memorySize == this->_allocOffset)
			{
				this->_allocOffset -= memoryPtr->memorySize;
				ExsDebugAssert( this->_allocOffset == blockOffset );
			}
			else
			{
				FreeMemoryBlock freeMemoryBlock { };
				freeMemoryBlock.offset = blockRef->offset;
				freeMemoryBlock.size = blockRef->size;
				this->_freeMemoryBlocks.Insert(freeMemoryBlock);
			}

			this->_activeMemoryBlocks.Erase(blockRef);
			this->_memoryAllocations.Erase(memoryRefDataIter);

			return ExsResult(RSC_Success);
		}
		
		void Reset(Mask<Enum> flags)
		{
			if (flags.IsSet(RSAllocatorReset_Invalidate_Resource_Memory))
			{
				for (auto& memoryRefData : this->_memoryAllocations)
				{
					if (memoryRefData.resource != nullptr)
						memoryRefData.resource->InvalidateMemory(RSResourceMemoryInvalidateMode::Default);
				}
			}

			this->_memoryAllocations.Clear();
			this->_activeMemoryBlocks.Clear();
			this->_freeMemoryBlocks.Clear();
			this->_allocOffset = 0;
		}

	private:
		RSMemoryRefData* _allocFreeMemoryBlock(RS_memory_size_t size)
		{
			RSMemoryRefData* memoryPtr = nullptr;

			if (!this->_freeMemoryBlocks.IsEmpty())
			{
				// Find the smallest blocks which is >= required size.
				auto freeMemoryBlockRef = this->_freeMemoryBlocks.LowerBound(size);

				if (freeMemoryBlockRef != this->_freeMemoryBlocks.End())
				{
					// Compute how much memory would be wasted by allocating this block...
					RS_memory_size_t maxMarginSize = freeMemoryBlockRef->size - size;
					
					// ... and validate it against specified value (by default: 4 KB).
					if (maxMarginSize <= this->_config.maxMarginSize)
					{
						memoryPtr = this->_registerAllocation(freeMemoryBlockRef->offset, freeMemoryBlockRef->size);
						this->_freeMemoryBlocks.Erase(freeMemoryBlockRef);
					}
				}
			}

			return memoryPtr;
		}

		RSMemoryRefData* _registerAllocation(RS_memory_size_t offset, RS_memory_size_t size)
		{
			RSMemoryRefData memoryRefData { };
			memoryRefData.memoryOffset = offset;
			memoryRefData.memorySize = size;
			memoryRefData.pool = this->_pool;
			
			ActiveMemoryBlock activeMemoryBlock { };
			activeMemoryBlock.memRef = this->_memoryAllocations.PushBack(memoryRefData);
			activeMemoryBlock.offset = offset;
			activeMemoryBlock.size = size;
			
			RSMemoryRefData* memoryPtr = &(*activeMemoryBlock.memRef);
			this->_activeMemoryBlocks.Insert(activeMemoryBlock);

			return memoryPtr;
		}
	};


}


#endif /* __Exs_RenderSystem_RSAllocatorStaticBlock_H__ */
