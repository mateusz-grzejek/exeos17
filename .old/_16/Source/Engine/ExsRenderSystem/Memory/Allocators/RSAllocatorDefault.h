
#pragma once

#ifndef __Exs_RenderSystem_RSAllocatorDefault_H__
#define __Exs_RenderSystem_RSAllocatorDefault_H__

#include "RSAllocatorDynamicMerge.h"


namespace Exs
{


	typedef RSAllocatorDynamicMerge RSAllocatorDefault;


}


#endif /* __Exs_RenderSystem_RSAllocatorDefault_H__ */

