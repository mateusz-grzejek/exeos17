
#pragma once

#ifndef __Exs_RenderSystem_RSAllocator_H__
#define __Exs_RenderSystem_RSAllocator_H__

#include "RSMemoryCommon.h"


namespace Exs
{


	///<summary>
	///</summary>
	enum RSAllocatorResetFlags : Enum
	{
	};


	///<summary>
	/// Represents base class for RS allocators, responsible for managing memory allocated from the device. This class
	/// provides only common data and interface related to properties of memory and allocation data.
	///</summary>
	class RSAllocator
	{
	protected:
		typedef std::list<RSMemoryRefData> MemoryAllocationList;
		typedef MemoryAllocationList::iterator MemoryReference;

	protected:
		RSMemoryHeap*           _heap;
		RS_memory_size_t        _memorySize;
		MemoryAllocationList    _memoryAllocations;

	public:
		RSAllocator(RSMemoryHeap* heap, RS_memory_size_t memorySize)
		: _heap(heap)
		, _memorySize(memorySize)
		{ }

		virtual ~RSAllocator()
		{ }

		const MemoryAllocationList& GetMemoryAllocations() const
		{
			return this->_memoryAllocations;
		}

		RS_memory_size_t GetMemorySize() const
		{
			return this->_memorySize;
		}
	};


	///<summary>
	/// Defines interface used by RSMemoryPools to manage their memory. This set of functors
	/// is built from allocator object and passed to the pool during its construction.
	///</summary>
	struct RSAllocatorInterface
	{
		// Allocation function. Receives memory size, returns ref data representing allocated memory.
		typedef std::function<RSMemoryRefData*(RS_memory_size_t)> AllocCallback;

		// Deallocation function. Receives ref data representing allocated memory, returns nothing.
		typedef std::function<void(RSMemoryRefData*)> FreeCallback;

		// Reset function. Receives flags describing how reset process should be performed, returns result.
		typedef std::function<void(stdx::mask<RSAllocatorResetFlags>)> ResetCallback;

		// Memory allocation callback.
		AllocCallback allocCallback;

		// Memory deallocation callback.
		FreeCallback freeCallback;

		// Reset callback.
		ResetCallback resetCallback;
	};


	///<summary>
	///</summary>
	class RSAllocatorWrapper
	{
		EXS_DECLARE_NONCOPYABLE(RSAllocatorWrapper);

	private:
		std::unique_ptr<RSAllocator>  _allocator;
		RSAllocatorInterface          _allocatorInterface;

	public:
		RSAllocatorWrapper(RSAllocatorWrapper&&) = default;
		RSAllocatorWrapper& operator=(RSAllocatorWrapper&&) = default;

		///<summary>
		///</summary>
		RSAllocatorWrapper()
		{ }

		///<summary>
		///</summary>
		RSAllocatorWrapper(std::unique_ptr<RSAllocator>&& allocator, RSAllocatorInterface&& allocatorInterface)
		: _allocator(std::move(allocator))
		, _allocatorInterface(std::forward<RSAllocatorInterface>(allocatorInterface))
		{ }

		///<summary>
		/// Forwards allocation request to the underlying allocator object.
		///</summary>
		RSMemoryRefData* Alloc(RS_memory_size_t size)
		{
			return this->_allocatorInterface.allocCallback(size);
		}

		///<summary>
		/// Forwards free request to the underlying allocator object.
		///</summary>
		void Free(RSMemoryRefData* memoryRefData)
		{
			this->_allocatorInterface.freeCallback(memoryRefData);
		}

		///<summary>
		/// Forwards reset request to the underlying allocator object.
		///</summary>
		void Reset(stdx::mask<RSAllocatorResetFlags> flags)
		{
			this->_allocatorInterface.resetCallback(flags);
		}
	};


	///<summary>
	///</summary>
	template <class Allocator_t, class... Args>
	inline RSAllocatorWrapper CreateRSAllocatorWrapper(RSMemoryHeap* heap, RS_memory_size_t memorySize, Args&&... args)
	{
		auto allocator = std::make_unique<Allocator_t>(heap, memorySize, std::forward<Args>(args)...);

		RSAllocatorInterface allocatorInterface;
		allocatorInterface.allocCallback = std::bind(&Allocator_t::Alloc, allocator.get(), std::placeholders::_1);
		allocatorInterface.freeCallback = std::bind(&Allocator_t::Free, allocator.get(), std::placeholders::_1);
		allocatorInterface.resetCallback = std::bind(&Allocator_t::Reset, allocator.get(), std::placeholders::_1);

		return RSAllocatorWrapper(std::move(allocator), std::move(allocatorInterface));
	}


}


#endif /* __Exs_RenderSystem_RSAllocator_H__ */
