
#pragma once

#ifndef __Exs_RenderSystemBase_H__
#define __Exs_RenderSystemBase_H__

#include "Base/BaseConfig.h"
#include "Base/BaseEnums.h"
#include "Base/CommonDefs.h"
#include "Base/CommonTypes.h"
#include "Base/VisualCommon.h"
#include "Base/StateDefs.h"
#include "Base/MemoryBase.h"

#include "Base/RSBaseObjectInterface.h"
#include "Base/RSResourceStorage.h"
#include "Base/RSBaseObject.h"
#include "Base/RSMiscObject.h"
#include "Base/RSResource.h"
#include "Base/RSResourceView.h"
#include "Base/RSStateDescriptor.h"
#include "Base/RSStateObject.h"


namespace Exs
{


	ExsDeclareRefPtrClass(GraphicsDriver);
	ExsDeclareRefPtrClass(RenderSystem);


}


#endif /* __Exs_RenderSystemBase_H__ */
