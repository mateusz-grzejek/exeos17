
#pragma once

#ifndef __Exs_RenderSystem_RSStateDescriptorCache_H__
#define __Exs_RenderSystem_RSStateDescriptorCache_H__

#include "State/CommonStateDefs.h"
#include <stdx/assoc_array.h>


namespace Exs
{


	ExsDeclareRSClassObjHandle(RSStateDescriptor);


	template <RSStateDescriptorType Descriptor_type>
	struct RSStateDescriptorInfo
	{
		//Handle to the descriptor object.
		typename RSStateDescriptorTypeProperties<Descriptor_type>::Handle handle;

		// PID 
		RSStateDescriptorPID pid;
	};


	///<summary>
	///</summary>
	class RSStateDescriptorCache
	{
	public:
		typedef stdx::assoc_array<RSStateDescriptorPID, RSStateDescriptorHandle> CachedDescriptors;

	protected:
		RenderSystem*        _renderSystem;
		CachedDescriptors    _cachedDescriptors;

	public:
		RSStateDescriptorCache(RenderSystem* renderSystem)
		: _renderSystem(renderSystem)
		{ }

		~RSStateDescriptorCache()
		{ }

		void RegisterDescriptor(const RSStateDescriptorHandle& descriptor)
		{
			ExsDebugCode(
				auto descriptorRef = this->_cachedDescriptors.find(descriptor->GetDescriptorPID());
				ExsDebugAssert( descriptorRef == this->_cachedDescriptors.end() ););

			RSStateDescriptorPID descriptorPID = descriptor->GetDescriptorPID();
			this->_cachedDescriptors.insert(descriptorPID, descriptor);
		}

		void Reset()
		{
			this->_cachedDescriptors.clear();
		}

		template <RSStateDescriptorType Descriptor_type_tag, class Config>
		RSStateDescriptorInfo<Descriptor_type_tag> GetDescriptor(const Config& configuration)
		{
			typedef typename RSStateDescriptorTypeProperties<Descriptor_type_tag>::Type DescriptorType;

			// Descriptor PID consists of two 32bit parts - first is an SDBM hash computed from specified
			// configuration object, second is descriptor type tag (RSStateDescriptorType enumerator).
			Uint32 configHash = GetHashCode<SDBM>(configuration).value;
			RSStateDescriptorPID descriptorPID = ((static_cast<Uint64>(Descriptor_type_tag) << 32) | configHash);

			// PID is always returned inside info object to allow creation of new descriptors by the RenderSystem.
			RSStateDescriptorInfo<Descriptor_type_tag> descriptorInfo { nullptr, descriptorPID };

			auto descriptorRef = this->_cachedDescriptors.find(descriptorPID);
			if (descriptorRef != this->_cachedDescriptors.end())
			{
				// Descriptor type should match specified type tag. If they do not match, it means, that
				// RenderSystem has created descriptor using PID queried for different descriptor type.
				RSStateDescriptorType descriptorType = descriptorRef->value->GetStateDescriptorType();
				ExsDebugAssert( descriptorType == Descriptor_type_tag );

				// Cast handle to the appropriate descriptor type.
				descriptorInfo.handle = stdx::dbgsafe_shared_ptr_cast<DescriptorType>(descriptorRef->value);
			}

			return descriptorInfo;
		}
	};
	

}


#endif /* __Exs_RenderSystem_RSStateDescriptorCache_H__ */
