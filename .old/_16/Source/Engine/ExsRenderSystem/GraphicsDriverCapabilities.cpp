
#include "_Precompiled.h"
#include <ExsRenderSystem/GraphicsDriverCapabilities.h>


namespace Exs
{


	GraphicsDriverCapabilities::GraphicsDriverCapabilities(const GraphicsDriverCapabilitiesDesc& capabilitiesDesc)
	{
		this->SetDesc(capabilitiesDesc);
	}


	GraphicsDriverCapabilities::~GraphicsDriverCapabilities()
	{ }


	void GraphicsDriverCapabilities::SetDesc(const GraphicsDriverCapabilitiesDesc& capabilitiesDesc)
	{
		this->_internalDesc.featureLevel = capabilitiesDesc.featureLevel;
		this->_internalDesc.shaderModelVersion = capabilitiesDesc.shaderModelVersion;
		this->_internalDesc.features.desc = capabilitiesDesc.featuresDesc;
		this->_internalDesc.metrics.desc = capabilitiesDesc.metricsDesc;
		this->_sourceDesc = capabilitiesDesc;
	}


	bool GraphicsDriverCapabilities::CheckSupport(const GraphicsDriverCapabilitiesDesc& capabilitiesDesc) const
	{
		InternalDesc::Features features { };
		features.desc = capabilitiesDesc.featuresDesc;

		InternalDesc::Metrics metrics { };
		metrics.desc = capabilitiesDesc.metricsDesc;

		for (Size_t featureIndex = 0; featureIndex < GraphicsDriver_Cap_Features_Num; ++featureIndex)
		{
			if (features.descArray[featureIndex] && !this->_internalDesc.features.descArray[featureIndex])
				return false;
		}

		for (Size_t metricIndex = 0; metricIndex < GraphicsDriver_Cap_Metrics_Num; ++metricIndex)
		{
			if (metrics.descArray[metricIndex] && !this->_internalDesc.metrics.descArray[metricIndex])
				return false;
		}

		return true;
	}


	bool GraphicsDriverCapabilities::QueryRefCapabilitiesDesc(GraphicsDriverFeatureLevel GraphicsDriverFeatureLevel, GraphicsDriverCapabilitiesDesc* capabilities)
	{
		static const GraphicsDriverCapabilitiesDesc refCapabilities { };

		if (capabilities != nullptr)
			*capabilities = refCapabilities;

		return true;
	};


}
