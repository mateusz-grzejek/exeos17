
#include "_Precompiled.h"
#include <ExsRenderSystem/RenderSystem.h>
#include <ExsRenderSystem/RSThreadManager.h>
#include <ExsRenderSystem/RSThread.h>
#include <ExsCore/Threading/ThreadLocalStorage.h>


namespace Exs
{


	RSThreadManager::RSThreadManager(RenderSystem* renderSystem)
	: _renderSystem(renderSystem)
	, _rsMainThreadEntry(&(_rsThreadArray[RS_Thr_Main_Thread_Index]))
	{ }


	RSThreadManager::~RSThreadManager()
	{ }


	RSThreadState* RSThreadManager::AcquireThreadState(RSThreadType threadType, RSThread* threadObject)
	{
		if (threadType != RSThreadType::Main)
		{
			// Main RS thread must be always created first. If worker thread is being
			// initialized, check if main thread entry has already beeen acquired.
			if (!this->_rsMainThreadEntry->acquireState.is_set(RSThreadEntry::AcquireState_Active))
			{
				ExsTraceError(TRC_RenderSystem, "");
				return nullptr;
			}
		}

		// If NULL threadObject is specified, fetch current thread. If NULL is returned, current thread is not a valid RS thread.
		if (threadObject == nullptr)
		{
			threadObject = QueryCurrentThreadObject();
			if (threadObject == nullptr)
				return nullptr;
		}

		RSThreadEntry* threadEntry = nullptr;

		if (threadType == RSThreadType::Main)
			threadEntry = this->TryAcquireMainThreadState();
		else if (threadType == RSThreadType::Worker)
			threadEntry = this->TryAcquireWorkerThreadState();

		if (threadEntry == nullptr)
			return nullptr;

		// Check if state is acquired for the first time.
		if (!threadEntry->threadState)
		{
			threadEntry->threadState = this->_renderSystem->InitializeRSThreadState(threadEntry->index, threadType);
			ExsDebugAssert( threadEntry->threadState );
		}

		threadEntry->threadState->SetThreadObject(threadObject);
		threadEntry->threadObject = threadObject;

		return threadEntry->threadState.get();
	}


	void RSThreadManager::ReleaseThreadState(RSThreadState* threadState)
	{
		ExsDebugAssert( threadState != nullptr );

		// Releasing main RSThread must be extra-validated.
		if (threadState->GetRSThreadType() == RSThreadType::Main)
		{
			// Main RSThread must be the last thread released.
			if (this->GetActiveThreadsNum() > 1)
				ExsExceptionThrowEx(EXC_Invalid_Operation, "");
		}
		
		RSThreadIndex rsThreadIndex = threadState->GetRSThreadIndex();
		ActiveThread* threadObject = threadState->GetThreadObject();

		if (threadObject == nullptr)
			ExsExceptionThrowEx(EXC_Invalid_State, "");

		RSThreadEntry& threadEntry = this->_rsThreadArray[rsThreadIndex];

		if (!threadEntry.acquireState.is_set(RSThreadEntry::AcquireState_Active))
			ExsExceptionThrowEx(EXC_Invalid_Operation, "");

		if (threadEntry.threadState.get() != threadState)
			ExsExceptionThrowEx(EXC_Invalid_State, "");

		if (threadEntry.threadObject != threadObject)
			ExsExceptionThrowEx(EXC_Invalid_State, "");
		
		threadEntry.threadState->SetThreadObject(nullptr);
		threadEntry.threadObject = nullptr;

		// After clearing is done, unset flag, so state can be required by another thread.
		threadEntry.acquireState.unset(RSThreadEntry::AcquireState_Active);
	}


	RSThread* RSThreadManager::QueryCurrentThreadObject()
	{
		Thread* currentThread = CurrentThread::GetThread();
		return dynamic_cast<RSThread*>(currentThread);
	}


	bool RSThreadManager::BindThreadState(RSThreadState* threadState)
	{
		ActiveThread* stateOwner = threadState->GetThreadObject();
		ActiveThread* currentThread = QueryCurrentThreadObject();

		if (currentThread == nullptr)
			return false;

		if (currentThread != stateOwner)
			return false;

		ThreadLocalStorage* threadLocalStorage = CurrentThread::GetLocalStorage();
		threadLocalStorage->extData[TLS_Index_RS_Thread_State] = threadState;

		return true;
	}


	void RSThreadManager::UnbindThreadState(RSThreadState* threadState)
	{
		ActiveThread* stateOwner = threadState->GetThreadObject();
		ActiveThread* currentThread = QueryCurrentThreadObject();

		if (currentThread == nullptr)
			ExsExceptionThrowEx(EXC_Invalid_Operation, "");

		if (currentThread != stateOwner)
			ExsExceptionThrowEx(EXC_Invalid_Operation, "");

		ThreadLocalStorage* threadLocalStorage = CurrentThread::GetLocalStorage();
		threadLocalStorage->extData[TLS_Index_RS_Thread_State] = nullptr;
	}


	RSThreadManager::RSThreadEntry* RSThreadManager::TryAcquireMainThreadState()
	{
		RSThreadIndex mainThreadIndex = RS_Thr_Main_Thread_Index;
		RSThreadEntry& threadEntry = this->_rsThreadArray[mainThreadIndex];

		if (threadEntry.acquireState.test_and_set(RSThreadEntry::AcquireState_Active))
		{
			if (threadEntry.index == RS_Thr_Invalid_Thread_Index)
				threadEntry.index = mainThreadIndex;

			return &threadEntry;
		}

		return nullptr;
	}


	RSThreadManager::RSThreadEntry* RSThreadManager::TryAcquireWorkerThreadState()
	{
		if (this->_workerThreadsNumLimit == 0)
			return nullptr;

		if (this->GetActiveThreadsNum() > this->_workerThreadsNumLimit)
			return nullptr;

		RSThreadEntry* workerThreadEntry = nullptr;
		RSThreadIndex workerThreadIndex = 0;

		for (Uint32 index = 0; index < this->_workerThreadsNumLimit; ++index)
		{
			workerThreadIndex = RS_Thr_Worker_Thread_Index_Base + index;
			RSThreadEntry& threadEntry = this->_rsThreadArray[workerThreadIndex];

			if (threadEntry.acquireState.test_and_set(RSThreadEntry::AcquireState_Active))
			{
				if (threadEntry.index == RS_Thr_Invalid_Thread_Index)
					threadEntry.index = workerThreadIndex;

				return &threadEntry;
			}
		}

		return nullptr;
	}



}
