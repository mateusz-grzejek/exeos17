
#pragma once

#ifndef __Exs_Engine_RSSharedState_H__
#define __Exs_Engine_RSSharedState_H__

#include "../Prerequisites.h"


namespace Exs
{

	
	class RSResourceLibrary;
	class RSStateObjectLibrary;

	
	///<summary>
	/// Stores 
	///</summary>
	class EXS_LIBCLASS_ENGINE RSSharedState
	{
	public:
		// Pointer to the RenderSystem instance.
		RenderSystem* const renderSystem;

		// Resource library containing all resources (shaders and textures) used by the engine.
		std::unique_ptr<RSResourceLibrary> const resourceLibrary;
		
		// Resource library containing all state objects (GPSO and VASO) used by the engine.
		std::unique_ptr<RSStateObjectLibrary> const stateObjectLibrary;

	public:
		RSSharedState(RenderSystem* renderSystem);

		~RSSharedState();
	};


}


#endif /* __Exs_Engine_RSSharedState_H__ */
