
#pragma once

#ifndef __Exs_Engine_RSResourceLibrary_H__
#define __Exs_Engine_RSResourceLibrary_H__

#include "../Prerequisites.h"


namespace Exs
{

	
	struct ShaderCreateInfo;
	struct Texture2DCreateInfo;
	struct Texture2DArrayCreateInfo;
	struct Texture3DCreateInfo;

	ExsDeclareRSClassObjHandle(Shader);
	ExsDeclareRSClassObjHandle(Texture);
	ExsDeclareRSClassObjHandle(Texture2D);
	ExsDeclareRSClassObjHandle(Texture2DArray);
	ExsDeclareRSClassObjHandle(Texture3D);

	
	class EXS_LIBCLASS_ENGINE RSResourceLibrary
	{
	private:
		typedef std::map<std::string, ShaderHandle> ShaderMap;
		typedef std::map<std::string, TextureHandle> TextureMap;

	private:
		RenderSystem*  _renderSystem;
		ShaderMap      _shaders;
		TextureMap     _textures;

	public:
		RSResourceLibrary(RenderSystem* renderSystem);
	};


};


#endif /* __Exs_Engine_RSResourceLibrary_H__ */
