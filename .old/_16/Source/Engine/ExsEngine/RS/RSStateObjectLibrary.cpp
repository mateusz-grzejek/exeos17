
#include "_Precompiled.h"
#include <ExsEngine/RS/RSStateObjectLibrary.h>
#include <ExsRenderSystem/RenderSystem.h>


namespace Exs
{


	RSStateObjectLibrary::RSStateObjectLibrary(RenderSystem* renderSystem)
	: _renderSystem(renderSystem)
	{ }

	
	bool RSStateObjectLibrary::CreateGraphicsPipelineStateObject(const std::string& name, const GraphicsPipelineStateObjectCreateInfo& createInfo)
	{
		auto stateObjectRef = this->_graphicsPipelineStateObjects.find(name);
		if (stateObjectRef == this->_graphicsPipelineStateObjects.end())
			return false;
		
		auto stateObject = this->_renderSystem->CreateGraphicsPipelineStateObject(createInfo);
		if (!stateObject)
			return false;

		this->_graphicsPipelineStateObjects.insert(std::pair<std::string, GraphicsPipelineStateObjectHandle>(name, stateObject));
		return true;
	}

	
	bool RSStateObjectLibrary::CreateVertexArrayStateObject(const std::string& name, const VertexArrayStateObjectCreateInfo& createInfo)
	{
		auto stateObjectRef = this->_vertexArrayStateObjects.find(name);
		if (stateObjectRef == this->_vertexArrayStateObjects.end())
			return false;
		
		auto stateObject = this->_renderSystem->CreateVertexArrayStateObject(createInfo);
		if (!stateObject)
			return false;

		this->_vertexArrayStateObjects.insert(std::pair<std::string, VertexArrayStateObjectHandle>(name, stateObject));
		return true;
	}


}
