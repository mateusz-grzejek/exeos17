
#pragma once

#ifndef __Exs_Engine_RenderingContext_H__
#define __Exs_Engine_RenderingContext_H__

#include "RSSharedState.h"
#include <ExsRenderSystem/RSContextCommandQueue.h>
#include <ExsRenderSystem/RSContextStateController.h>


namespace Exs
{

	
	///<summary>
	/// Contains all state required and used by renderers to execute rendering commands.
	///</summary>
	class EXS_LIBCLASS_ENGINE RenderingContext
	{
	public:
		// Pointer to the RenderSystem instance
		RenderSystem* const renderSystem;

		// Pointer to the shared RS state.
		RSSharedState* const rsSharedState;

		// Command queue of the thread which created this context.
		RSContextCommandQueue* const rsCommandQueue;
		
		// State controller of the thread which created this context.
		RSContextStateController* const rsStateController;

	public:
		RenderingContext(RSSharedState* rsSharedState, RSContextCommandQueue* commandQueue);
	};


}


#endif /* __Exs_Engine_RenderingContext_H__ */
