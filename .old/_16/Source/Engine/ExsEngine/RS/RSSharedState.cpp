
#include "_Precompiled.h"
#include <ExsEngine/RS/RSSharedState.h>
#include <ExsEngine/RS/RSResourceLibrary.h>
#include <ExsEngine/RS/RSStateObjectLibrary.h>


namespace Exs
{


	RSSharedState::RSSharedState(RenderSystem* renderSystem)
	: renderSystem(renderSystem)
	, resourceLibrary(new RSResourceLibrary(renderSystem))
	, stateObjectLibrary(new RSStateObjectLibrary(renderSystem))
	{ }


	RSSharedState::~RSSharedState()
	{ }


}
