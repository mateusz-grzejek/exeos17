
#pragma once

#ifndef __Exs_Engine_Base2DRectGeometry_H__
#define __Exs_Engine_Base2DRectGeometry_H__

#include "GeometryData.h"
#include "GeometryManager.h"


namespace Exs
{


	///<summary>
	/// Stores basic info about geometry data of objects, that are built of rectangles.
	///</summary>
	struct Base2DRectGeometryDataStorageInfo : public GeometryDataStorageBaseInfo
	{
		// Memory allocated for indices.
		GeometryMemory indicesMemory;

		// Memory allocated for vertices.
		GeometryMemory verticesMemory;

		// Offset from the beginning of the vertex buffer used by this geometry (in number of vertices).
		// Note, that this value is also the first index value used for indexed geometry.
		Uint32 verticesElementOffset;

		// Capacity (in number of elements) of storage allocated for indices.
		Uint32 indicesElementCapacity;

		// Capacity (in number of rects) of storage allocated for indices.
		Uint32 indicesRectCapacity;

		// Capacity (in number of elements) of storage allocated for vertices.
		Uint32 verticesElementCapacity;

		// Capacity (in number of rects) of storage allocated for vertices.
		Uint32 verticesRectCapacity;
	};

	
	///<summary>
	/// Contains initialization data used by Base2DRectGeometryManager to initialize its storage.
	/// This desc structure should contain either size of a buffers to create for the manager or
	/// handle/range pair indicating a subrange of an existing buffer. While index buffer is optional
	/// and both 'indicesStorageCapacity' and 'indexBuffer' may be empty, vertex buffer must be always
	/// specified. Otherwise such attempted initialization will fail.
	///</summary>
	struct Base2DRectGeometryManagerInitDesc
	{
		// Type of indices which will be stored in the index buffer.
		IndexBufferDataType indicesDataType = IndexBufferDataType::Uint32;
		
		// Size of index buffer to create. Used if 'indexBuffer' in an empty handle.
		Size_t indicesStorageCapacity;

		// Handle to index buffer to be used for indices storage allocation.
		IndexBufferHandle indexBuffer;
		
		// Range of indexBuffer to use. This range must be used exclusively by a single geometry.
		RSMemoryRange indexBufferRange;

		// Size of a single vertex which will be stored in the vertex buffer. Must not be zero.
		Size_t verticesDataSize = 0;

		// Size of vertex buffer to create. Used if 'vertexBuffer' in an empty handle.
		Size_t verticesStorageCapacity;
		
		// Handle to vertex buffer to be used for vertices storage allocation.
		VertexBufferHandle vertexBuffer;
		
		// Range of vertexBuffer to use. This range must be used exclusively by a single geometry.
		RSMemoryRange vertexBufferRange;
	};

	
	///<summary>
	///</summary>
	struct Base2DRectGeometry : public Geometry<Base2DRectGeometryDataStorageInfo>
	{
	};

	
	///<summary>
	/// Implements manager which manages geometry of primitives built of rectangles (either indexed or non-indexed).
	/// It is initialized with vertex buffer and optional index buffer which serve as a storage for rectangles geometry.
	/// Single manager of this type can allocate both indexed and non-indexed geometry.
	///</summary>
	class EXS_LIBCLASS_ENGINE Base2DRectGeometryManager : public GeometryManager
	{
	protected:
		GeometryBufferState  _indexBufferState;
		GeometryStorage*     _indexBufferStorage;
		GeometryBufferState  _vertexBufferState;
		GeometryStorage*     _vertexBufferStorage;

	public:
		Base2DRectGeometryManager(RenderSystem* renderSystem);
		virtual ~Base2DRectGeometryManager();
		
		///<summary>
		///</summary>
		bool Initialize(const Base2DRectGeometryManagerInitDesc& initDesc);
		
		///<summary>
		///</summary>
		template <class Index_t>
		Index_t* GetIndicesUpdatePtr(const Base2DRectGeometry& geometry, Size_t rectOffset = 0, Size_t rectsNum = Invalid_Length);
		
		///<summary>
		///</summary>
		template <class Vertex_t>
		Vertex_t* GetVerticesUpdatePtr(const Base2DRectGeometry& geometry, Size_t rectOffset = 0, Size_t rectsNum = Invalid_Length);
		
		///<summary>
		///</summary>
		bool CheckAvailableSpace(Size_t geometrySize, GeometryDataType geometryType) const;

	protected:
		//
		bool ReserveIndexedGeometryStorage(Size_t geometrySize, Base2DRectGeometry* geometry);
		
		//
		bool ReserveNonIndexedGeometryStorage(Size_t geometrySize, Base2DRectGeometry* geometry);

	private:
		// Allocates space for 'geometrySize' indexed rectangles.
		bool _ReserveIndexed(Uint32 geometrySize, Base2DRectGeometryDataStorageInfo* geometryDataInfo);
		
		// Allocates space for 'geometrySize' non-indexed rectangles.
		bool _ReserveNonIndexed(Uint32 geometrySize, Base2DRectGeometryDataStorageInfo* geometryDataInfo);
		
		//
		void* _RetrieveIndicesUpdatePtr(const Base2DRectGeometry& geometry, Size_t rectOffset, Size_t rectsNum);
		
		//
		void* _RetrieveVerticesUpdatePtr(const Base2DRectGeometry& geometry, Size_t rectOffset, Size_t rectsNum);
	};
	

	template <class Index_t>
	inline Index_t* Base2DRectGeometryManager::GetIndicesUpdatePtr(const Base2DRectGeometry& geometry, Size_t rectOffset, Size_t indicesNum)
	{
		if (sizeof(Index_t) != this->_indexBufferState.elementSize)
		{
			ExsTraceError(TRC_Engine_Common, "Mismatch in indices data size.");
			return nullptr;
		}

		if (geometry.type != GeometryDataType::Indexed)
		{
			ExsTraceError(TRC_Engine_Common, "Requested indices update ptr for non-indexed geometry!");
			return nullptr;
		}

		void* baseUpdatePtr = this->_RetrieveIndicesUpdatePtr(geometry, rectOffset, indicesNum);
		return reinterpret_cast<Index_t*>(baseUpdatePtr);
	}

	template <class Vertex_t>
	inline Vertex_t* Base2DRectGeometryManager::GetVerticesUpdatePtr(const Base2DRectGeometry& geometry, Size_t rectOffset, Size_t verticesNum)
	{
		if (sizeof(Vertex_t) != this->_vertexBufferState.elementSize)
		{
			ExsTraceError(TRC_Engine_Common, "Mismatch in vertices data size.");
			return nullptr;
		}

		void* baseUpdatePtr = this->_RetrieveVerticesUpdatePtr(geometry, rectOffset, indicesNum);
		return reinterpret_cast<Vertex_t*>(baseUpdatePtr);
	}


}


#endif /* __Exs_Engine_Base2DRectGeometry_H__ */
