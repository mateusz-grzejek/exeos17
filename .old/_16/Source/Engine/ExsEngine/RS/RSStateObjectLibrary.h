
#pragma once

#ifndef __Exs_Engine_RSStateObjectLibrary_H__
#define __Exs_Engine_RSStateObjectLibrary_H__

#include "../Prerequisites.h"


namespace Exs
{

	
	struct GraphicsPipelineStateObjectCreateInfo;
	struct VertexArrayStateObjectCreateInfo;

	ExsDeclareRSClassObjHandle(GraphicsPipelineStateObject);
	ExsDeclareRSClassObjHandle(VertexArrayStateObject);

	
	class EXS_LIBCLASS_ENGINE RSStateObjectLibrary
	{
	private:
		typedef std::map<std::string, GraphicsPipelineStateObjectHandle> GraphicsPipelineStateObjectMap;
		typedef std::map<std::string, VertexArrayStateObjectHandle> VertexArrayStateObjectMap;

	private:
		RenderSystem*                     _renderSystem;
		GraphicsPipelineStateObjectMap    _graphicsPipelineStateObjects;
		VertexArrayStateObjectMap         _vertexArrayStateObjects;

	public:
		RSStateObjectLibrary(RenderSystem* renderSystem);

		void Release();
		
		bool CreateGraphicsPipelineStateObject(const std::string& name, const GraphicsPipelineStateObjectCreateInfo& createInfo);

		bool CreateVertexArrayStateObject(const std::string& name, const VertexArrayStateObjectCreateInfo& createInfo);

		GraphicsPipelineStateObjectHandle GetGraphicsPipelineStateObject(const std::string& name) const;

		VertexArrayStateObjectHandle GetVertexArrayStateObject(const std::string& name) const;
	};


	inline GraphicsPipelineStateObjectHandle RSStateObjectLibrary::GetGraphicsPipelineStateObject(const std::string& name) const
	{
		auto stateObjectRef = this->_graphicsPipelineStateObjects.find(name);
		return (stateObjectRef != this->_graphicsPipelineStateObjects.end()) ? stateObjectRef->second : nullptr;
	}
	
	inline VertexArrayStateObjectHandle RSStateObjectLibrary::GetVertexArrayStateObject(const std::string& name) const
	{
		auto stateObjectRef = this->_vertexArrayStateObjects.find(name);
		return (stateObjectRef != this->_vertexArrayStateObjects.end()) ? stateObjectRef->second : nullptr;
	}


};


#endif /* __Exs_Engine_RSStateObjectLibrary_H__ */
