
#pragma once

#ifndef __Exs_Engine_GeometryBase_H__
#define __Exs_Engine_GeometryBase_H__

#include "../Prerequisites.h"
#include <ExsRenderSystem/Resource/GPUBufferBase.h>
#include <ExsRenderSystem/State/CommonIADefs.h>


namespace Exs
{


	class GeometryManager;
	class GeometryStorage;
	
	ExsDeclareRSClassObjHandle(GeometryBuffer);


	enum class GeometryUpdateMode : Enum
	{
		None = 0,
		Append,
		Modify,
		Unspecified
	};


	struct GeometryMemory
	{
		// Range within the source buffer.
		RSMemoryRange bufferRange;
	};


}


#endif /* __Exs_Engine_GeometryBase_H__ */
