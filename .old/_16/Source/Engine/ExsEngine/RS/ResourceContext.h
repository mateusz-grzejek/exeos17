
#pragma once

#ifndef __Exs_Engine_ResourceContext_H__
#define __Exs_Engine_ResourceContext_H__

#include "RSSharedState.h"


namespace Exs
{

	
	///<summary>
	///</summary>
	class EXS_LIBCLASS_ENGINE ResourceContext
	{
	public:
		// Pointer to the RenderSystem instance
		RenderSystem* const renderSystem;

		// Pointer to the shared RS state.
		RSSharedState* const rsSharedState;

	public:
		ResourceContext(RSSharedState* rsSharedState);
	};


}


#endif /* __Exs_Engine_ResourceContext_H__ */
