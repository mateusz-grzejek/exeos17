
#pragma once

#ifndef __Exs_Engine_BaseConfig_H__
#define __Exs_Engine_BaseConfig_H__

#include <ExsRenderSystem/Prerequisites.h>

#include <map>
#include <tuple>


#if ( EXS_BUILD_MODULE_ENGINE )
#  define EXS_LIBAPI_ENGINE       EXS_MODULE_EXPORT
#  define EXS_LIBCLASS_ENGINE     EXS_MODULE_EXPORT
#  define EXS_LIBOBJECT_ENGINE    extern EXS_MODULE_EXPORT
#else
#  define EXS_LIBAPI_ENGINE       EXS_MODULE_IMPORT
#  define EXS_LIBCLASS_ENGINE     EXS_MODULE_IMPORT
#  define EXS_LIBOBJECT_ENGINE    extern EXS_MODULE_IMPORT
#endif


#endif /* __Exs_Engine_BaseConfig_H__ */
