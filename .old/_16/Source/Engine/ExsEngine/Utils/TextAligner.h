
#pragma once

#ifndef __Exs_Engine_TextAligner_H__
#define __Exs_Engine_TextAligner_H__

#include <ExsResManager/Resources/FontGlyph.h>


namespace Exs
{


	struct AlignedChar
	{
		Uint32 bitmapLayer;
		CodePoint codePoint;
		const FontGlyph* glyph;
		Vector2U64 pos;
		Vector2U64 size;

	};


	struct AlignedText
	{
		std::vector<AlignedChar> data;

		Size_t length;

		Vector2F wsize;
	};


	enum class TextAlignerModeFlags : Enum
	{
		TextAlignerMode_Align_Left,
		TextAlignerMode_Align_Right,
		TextAlignerMode_Align_Center,
		TextAlignerMode_Align_Justify,
		TextAlignerMode_Break_Words,
		TextAlignerMode_Multiline
	};


	struct TextAlignerConfig
	{
		Vector2U32 extraSpace;

		stdx::mask<TextAlignerModeFlags> modeFlags;
	};


	class TextAligner
	{
	private:
		struct CharData
		{
			const FontGlyph* glyph;
			RectU32 rect;
		};

		typedef std::vector<CharData> CharArray;

		struct AlignedWord
		{
			CharArray charArray;

			RectU32 boundingRect;
		};

		class TextLine
		{
		private:
			Vector2U32  _size;
			Uint32      _currentXbase;
		};

		typedef std::vector<TextLine> TextLineArray;

	private:
		Vector2U32  _baseOffset;
		Vector2U32  _destBoxSize;
		Vector4U32  _destPadding;
		Uint32      _lineHeight;

	public:

	private:
		static AlignedWord ParseWord(const char*);
	};


	EXS_LIBAPI_ENGINE AlignedText AlignText(const std::string& text, Font& font, const Vector2U32& baseOffset);


}


#endif /* __Exs_Engine_TextAligner_H__ */
