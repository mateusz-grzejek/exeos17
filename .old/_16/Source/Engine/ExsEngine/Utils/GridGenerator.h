
#pragma once

#ifndef __Exs_Engine_GridGenerator_H__
#define __Exs_Engine_GridGenerator_H__

#include "../Prerequisites.h"
#include <stdx/data_array.h>


namespace Exs
{


	enum class GridVerticesOrder
	{
		Clockwise,
		Counter_Clockwise
	};


	struct GridGeometry
	{
		stdx::dynamic_data_array<Vector3F> vertices;

		stdx::dynamic_data_array<Uint32> indices;

		Size_t verticesNum;

		Size_t indicesNum;

		GridVerticesOrder verticesOrder;
	};


	class EXS_LIBCLASS_ENGINE GridGenerator
	{
	public:
		static bool Generate( const Vector3F& origin,
		                      const Vector2U32& size,
		                      const Vector2F& advance,
		                      GridGeometry* geometry,
		                      GridVerticesOrder verticesOrder = GridVerticesOrder::Counter_Clockwise);

		static bool GenerateIndexed( const Vector3F& origin,
		                             const Vector2U32& size,
		                             const Vector2F& advance,
		                             GridGeometry* geometry,
		                             GridVerticesOrder verticesOrder = GridVerticesOrder::Counter_Clockwise);
		
	private:
		//
		static void _GenerateVerticesIndexed(const Vector3F& origin, const Vector2U32& size, const Vector2F& advance, GridGeometry* geometry);

		//
		static void _GenerateVerticesNoIndexClockwise(const Vector3F& origin, const Vector2U32& size, const Vector2F& advance, GridGeometry* geometry);

		//
		static void _GenerateVerticesNoIndexCounterClockwise(const Vector3F& origin, const Vector2U32& size, const Vector2F& advance, GridGeometry* geometry);

		//
		static void _GenerateIndicesClockwise(const Vector2U32& size, GridGeometry* geometry);

		//
		static void _GenerateIndicesCounterClockwise(const Vector2U32& size, GridGeometry* geometry);
	};


}


#endif /* __Exs_Engine_GridGenerator_H__ */
