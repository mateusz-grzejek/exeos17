
#include "_Precompiled.h"
#include <ExsEngine/Utils/GridGenerator.h>


namespace Exs
{


	bool GridGenerator::Generate( const Vector3F& origin,
	                              const Vector2U32& size,
	                              const Vector2F& advance,
	                              GridGeometry* geometry,
	                              GridVerticesOrder verticesOrder)
	{
		ExsDebugAssert( geometry != nullptr );

		if ((size.x == 0) || (size.y == 0))
			return false;

		geometry->indicesNum = 0;
		geometry->verticesNum = 0;

		if (verticesOrder == GridVerticesOrder::Clockwise)
		{
			_GenerateVerticesNoIndexClockwise(origin, size, advance, geometry);
			geometry->verticesOrder = GridVerticesOrder::Clockwise;
		}
		else
		{
			_GenerateVerticesNoIndexCounterClockwise(origin, size, advance, geometry);
			geometry->verticesOrder = GridVerticesOrder::Counter_Clockwise;
		}

		return true;
	}


	bool GridGenerator::GenerateIndexed( const Vector3F& origin,
	                                     const Vector2U32& size,
	                                     const Vector2F& advance,
	                                     GridGeometry* geometry,
	                                     GridVerticesOrder verticesOrder)
	{
		ExsDebugAssert( geometry != nullptr );

		if ((size.x == 0) || (size.y == 0))
			return false;

		geometry->indicesNum = 0;
		geometry->verticesNum = 0;

		if (verticesOrder == GridVerticesOrder::Clockwise)
		{
			_GenerateVerticesIndexed(origin, size, advance, geometry);
			_GenerateIndicesClockwise(size, geometry);
			geometry->verticesOrder = GridVerticesOrder::Clockwise;
		}
		else
		{
			_GenerateVerticesIndexed(origin, size, advance, geometry);
			_GenerateIndicesCounterClockwise(size, geometry);
			geometry->verticesOrder = GridVerticesOrder::Counter_Clockwise;
		}

		return true;
	}


	void GridGenerator::_GenerateVerticesIndexed(const Vector3F& origin, const Vector2U32& size, const Vector2F& advance, GridGeometry* geometry)
	{
		Size_t verticesNum = (size.x + 1) * (size.y + 1);
		auto& vertices = geometry->vertices;

		if (vertices.size() < verticesNum)
			vertices.resize(verticesNum);

		Vector3F* verticesData = vertices.data_ptr();

		for (Uint32 dz = 0; dz <= size.y; ++dz)
		{
			Vector3F current { origin.x, origin.y, origin.z + (dz * advance.y) };

			for (Uint32 dx = 0; dx <= size.x; ++dx)
			{
				verticesData->x = current.x + (dx * advance.x);
				verticesData->y = current.y;
				verticesData->z = current.z;

				++verticesData;
			};
		}

		geometry->verticesNum = verticesNum;
	}


	void GridGenerator::_GenerateVerticesNoIndexClockwise(const Vector3F& origin, const Vector2U32& size, const Vector2F& advance, GridGeometry* geometry)
	{
		Size_t verticesNum = size.x * size.y * 6;
		auto& vertices = geometry->vertices;
		
		if (vertices.size() < verticesNum)
			vertices.resize(verticesNum);

		Vector3F* verticesData = vertices.data_ptr();

		for (Uint32 dz = 0; dz < size.y; ++dz)
		{
			Vector3F current { origin.x, origin.y, origin.z + (dz * advance.y) };

			for (Uint32 dx = 0; dx < size.x; ++dx)
			{
				Vector3F v1(current.x + (dx * advance.x), current.y, current.z);
				Vector3F v2(current.x + ((dx + 1) * advance.x), current.y, current.z);
				Vector3F v3(current.x + (dx * advance.x), current.y, current.z + advance.y);
				Vector3F v4(current.x + ((dx + 1) * advance.x), current.y, current.z + advance.y);

				verticesData[0] = v1;
				verticesData[1] = v3;
				verticesData[2] = v4;

				verticesData[3] = v1;
				verticesData[4] = v3;
				verticesData[5] = v2;

				verticesData += 6;
			};
		}

		geometry->verticesNum = verticesNum;
	}


	void GridGenerator::_GenerateVerticesNoIndexCounterClockwise(const Vector3F& origin, const Vector2U32& size, const Vector2F& advance, GridGeometry* geometry)
	{
		Size_t verticesNum = size.x * size.y * 6;
		auto& vertices = geometry->vertices;

		if (vertices.size() < verticesNum)
			vertices.resize(verticesNum);

		Vector3F* verticesData = vertices.data_ptr();

		for (Uint32 dz = 0; dz < size.y; ++dz)
		{
			Vector3F current { origin.x, origin.y, origin.z + (dz * advance.y) };

			for (Uint32 dx = 0; dx < size.x; ++dx)
			{
				Vector3F v1(current.x + (dx * advance.x), current.y, current.z);
				Vector3F v2(current.x + ((dx + 1) * advance.x), current.y, current.z);
				Vector3F v3(current.x + (dx * advance.x), current.y, current.z + advance.y);
				Vector3F v4(current.x + ((dx + 1) * advance.x), current.y, current.z + advance.y);

				verticesData[0] = v1;
				verticesData[1] = v2;
				verticesData[2] = v4;

				verticesData[3] = v1;
				verticesData[4] = v4;
				verticesData[5] = v3;

				verticesData += 6;
			};
		}

		geometry->verticesNum = verticesNum;
	}


	void GridGenerator::_GenerateIndicesClockwise(const Vector2U32& size, GridGeometry* geometry)
	{
		Size_t indicesNum = size.x * size.y * 6;
		auto& indices = geometry->indices;
		
		if (indices.size() < indicesNum)
			indices.resize(indicesNum);

		Uint32* indicesData = indices.data_ptr();
		Uint32 indicesXdAmount = size.x + 1;

		for (Uint32 dz = 0; dz < size.y; ++dz)
		{
			for (Uint32 dx = 0; dx < size.x; ++dx)
			{
				indicesData[0] = (dz * indicesXdAmount) + dx;
				indicesData[1] = ((dz + 1) * indicesXdAmount) + dx;
				indicesData[2] = ((dz + 1) * indicesXdAmount) + dx + 1;

				indicesData[3] = (dz * indicesXdAmount) + dx;
				indicesData[4] = ((dz + 1) * indicesXdAmount) + dx + 1;
				indicesData[5] = (dz * indicesXdAmount) + dx + 1;

				indicesData += 6;
			};
		}

		geometry->indicesNum = indicesNum;
	}


	void GridGenerator::_GenerateIndicesCounterClockwise(const Vector2U32& size, GridGeometry* geometry)
	{
		Size_t indicesNum = size.x * size.y * 6;
		auto& indices = geometry->indices;

		if (indices.size() < indicesNum)
			indices.resize(indicesNum);

		Uint32* indicesData = indices.data_ptr();
		Uint32 indicesXdAmount = size.x + 1;

		for (Uint32 dz = 0; dz < size.y; ++dz)
		{
			for (Uint32 dx = 0; dx < size.x; ++dx)
			{
				indicesData[0] = (dz * indicesXdAmount) + dx;
				indicesData[1] = (dz * indicesXdAmount) + dx + 1;
				indicesData[2] = ((dz + 1) * indicesXdAmount) + dx + 1;

				indicesData[3] = (dz * indicesXdAmount) + dx;
				indicesData[4] = ((dz + 1) * indicesXdAmount) + dx + 1;
				indicesData[5] = ((dz + 1) * indicesXdAmount) + dx;

				indicesData += 6;
			};
		}

		geometry->indicesNum = indicesNum;
	}


}
