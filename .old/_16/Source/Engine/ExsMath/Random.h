
#pragma once

#ifndef __Exs_Math_Random_H__
#define __Exs_Math_Random_H__

#include "MathBase.h"
#include <random>


namespace Exs
{


	template <typename Value_t>
	struct RandomGeneratorTraits;

	template <>
	struct RandomGeneratorTraits<char>
	{
		typedef std::uniform_int_distribution<Int16> Distribution;
		typedef std::ranlux24_base Generator;
	};

	template <>
	struct RandomGeneratorTraits<Int8>
	{
		typedef std::uniform_int_distribution<Int16> Distribution;
		typedef std::ranlux24_base Generator;
	};

	template <>
	struct RandomGeneratorTraits<Uint8>
	{
		typedef std::uniform_int_distribution<Uint16> Distribution;
		typedef std::ranlux24_base Generator;
	};

	template <>
	struct RandomGeneratorTraits<Int16>
	{
		typedef std::uniform_int_distribution<Int16> Distribution;
		typedef std::ranlux24_base Generator;
	};

	template <>
	struct RandomGeneratorTraits<Uint16>
	{
		typedef std::uniform_int_distribution<Uint16> Distribution;
		typedef std::ranlux24_base Generator;
	};

	template <>
	struct RandomGeneratorTraits<Int32>
	{
		typedef std::uniform_int_distribution<Int32> Distribution;
		typedef std::ranlux24_base Generator;
	};

	template <>
	struct RandomGeneratorTraits<Uint32>
	{
		typedef std::uniform_int_distribution<Uint32> Distribution;
		typedef std::ranlux24_base Generator;
	};

	template <>
	struct RandomGeneratorTraits<Int64>
	{
		typedef std::uniform_int_distribution<Int64> Distribution;
		typedef std::ranlux48_base Generator;
	};

	template <>
	struct RandomGeneratorTraits<Uint64>
	{
		typedef std::uniform_int_distribution<Uint64> Distribution;
		typedef std::ranlux48_base Generator;
	};

	template <>
	struct RandomGeneratorTraits<float>
	{
		typedef std::uniform_real_distribution<float> Distribution;
		typedef std::ranlux24_base Generator;
	};

	template <>
	struct RandomGeneratorTraits<double>
	{
		typedef std::uniform_real_distribution<double> Distribution;
		typedef std::ranlux48_base Generator;
	};

	template <>
	struct RandomGeneratorTraits<long double>
	{
		typedef std::uniform_real_distribution<long double> Distribution;
		typedef std::ranlux48_base Generator;
	};


	template <typename Value_t>
	struct DefaultRandomGenerator
	{
	public:
		typedef RandomGeneratorTraits<Value_t> Traits;

		typedef typename Traits::Distribution Distribution;
		typedef typename Traits::Generator Generator;

	private:
		std::random_device    _rnDevice;
		Distribution          _distribution;
		Generator             _generator;

	public:
		DefaultRandomGenerator()
		: _distribution(stdx::limits<Value_t>::min_value, stdx::limits<Value_t>::max_value)
		{
			this->_generator.seed();
		}

		DefaultRandomGenerator(Value_t minVal, Value_t maxVal)
		: _distribution(minVal, maxVal)
		{
			this->_generator.seed();
		}
		
		Value_t operator()()
		{
			return static_cast<Value_t>(this->_distribution(this->_generator));
		}

		void Discard(Size_t count)
		{
			this->_generator.discard(truncate_cast<Uint32>(count));
		}

		void Seed(Uint32 seedVal)
		{
			this->_generator.seed(seedVal);
		}
	};


	template < typename Value_t, class Generator = DefaultRandomGenerator<Value_t> >
	class Random
	{
		EXS_DECLARE_NONCOPYABLE(Random);

	protected:
		Generator    _generator;

	public:
		Random()
		{ }

		Random(const Generator& generator)
		: _generator(generator)
		{ }
		
		Random(Value_t minVal, Value_t maxVal)
		: _generator(minVal, maxVal)
		{ }
		
		Value_t operator()()
		{
			return this->_generator();
		}

		void Discard(Size_t count)
		{
			this->_generator.Discard(count);
		}

		void Seed(Uint32 seedVal)
		{
			this->_generator.Seed(seedVal);
		}
	};


	template <typename Value_t>
	struct STDCRandomGenerator
	{
	protected:
		Value_t  _min;
		Value_t  _max;
		Value_t  _rangeSize;

	public:
		STDCRandomGenerator()
		: _min(stdx::limits<Value_t>::min_value)
		, _max(stdx::limits<Value_t>::max_value)
		, _rangeSize(_max - _min)
		{ }

		STDCRandomGenerator(Value_t minVal, Value_t maxVal)
		: _min(minVal)
		, _max(maxVal)
		, _rangeSize(_max - _min)
		{ }

		Value_t operator()()
		{
			int randomNumber = std::rand();
			return static_cast<Value_t>(randomNumber % this->_rangeSize) + this->_min;
		}

		static void Discard(Size_t count)
		{
			for (Size_t n = 0; n < count; ++n)
			{
				std::rand();
			}
		}

		static void Seed(Uint32 seedVal)
		{
			std::srand(seedVal);
		}
	};


	template <>
	struct STDCRandomGenerator<Int64>;

	template <>
	struct STDCRandomGenerator<Uint64>;

	template <>
	struct STDCRandomGenerator<float>;

	template <>
	struct STDCRandomGenerator<double>;

	template <>
	struct STDCRandomGenerator<long double>;


}


#endif /* __Exs_Math_Random_H__ */
