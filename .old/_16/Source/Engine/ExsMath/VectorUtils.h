
#pragma once

#ifndef __Exs_Math_VectorUtils_H__
#define __Exs_Math_VectorUtils_H__

#include "Vector2.h"
#include "Vector3.h"
#include "Vector4.h"


namespace Exs
{


	template <Precision P = Precision::Default>
	struct Vector
	{
	public:
		typedef typename ScalarPrecisionType<P>::Type ScalarType;

		template <typename T>
		static Vector2<ScalarType> Inverse(const Vector2<T>& vec2)
		{
			return Vector2<ScalarType>(
				static_cast<ScalarType>(1) / static_cast<ScalarType>(vec2.x),
				static_cast<ScalarType>(1) / static_cast<ScalarType>(vec2.y));
		}

		template <typename T>
		static Vector3<ScalarType> Inverse(const Vector3<T>& vec3)
		{
			return Vector3<ScalarType>(
				static_cast<ScalarType>(1) / static_cast<ScalarType>(vec3.x),
				static_cast<ScalarType>(1) / static_cast<ScalarType>(vec3.y),
				static_cast<ScalarType>(1) / static_cast<ScalarType>(vec3.z));
		}

		template <typename T>
		static Vector4<ScalarType> Inverse(const Vector4<T>& vec4)
		{
			return Vector4<ScalarType>(
				static_cast<ScalarType>(1) / static_cast<ScalarType>(vec4.x),
				static_cast<ScalarType>(1) / static_cast<ScalarType>(vec4.y),
				static_cast<ScalarType>(1) / static_cast<ScalarType>(vec4.z),
				static_cast<ScalarType>(1) / static_cast<ScalarType>(vec4.w));
		}

		template <typename T>
		static Vector2<ScalarType> Sqrt(const Vector2<T>& vec2)
		{
			return Vector2<ScalarType>(
				sqrt(static_cast<ScalarType>(vec2.x)),
				sqrt(static_cast<ScalarType>(vec2.y)));
		}

		template <typename T>
		static Vector3<ScalarType> Sqrt(const Vector3<T>& vec3)
		{
			return Vector3<ScalarType>(
				sqrt(static_cast<ScalarType>(vec3.x)),
				sqrt(static_cast<ScalarType>(vec3.y)),
				sqrt(static_cast<ScalarType>(vec3.z)));
		}

		template <typename T>
		static Vector4<ScalarType> Sqrt(const Vector4<T>& vec4)
		{
			return Vector4<ScalarType>(
				sqrt(static_cast<ScalarType>(vec4.x)),
				sqrt(static_cast<ScalarType>(vec4.y)),
				sqrt(static_cast<ScalarType>(vec4.z)),
				sqrt(static_cast<ScalarType>(vec4.w)));
		}

		template <typename T>
		static Vector2<ScalarType> InvSqrt(const Vector2<T>& vec2)
		{
			return Vector2<ScalarType>(
				static_cast<ScalarType>(1) / sqrt(static_cast<ScalarType>(vec2.x)),
				static_cast<ScalarType>(1) / sqrt(static_cast<ScalarType>(vec2.y)));
		}

		template <typename T>
		static Vector3<ScalarType> InvSqrt(const Vector3<T>& vec3)
		{
			return Vector3<ScalarType>(
				static_cast<ScalarType>(1) / sqrt(static_cast<ScalarType>(vec3.x)),
				static_cast<ScalarType>(1) / sqrt(static_cast<ScalarType>(vec3.y)),
				static_cast<ScalarType>(1) / sqrt(static_cast<ScalarType>(vec3.z)));
		}

		template <typename T>
		static Vector4<ScalarType> InvSqrt(const Vector4<T>& vec4)
		{
			return Vector4<ScalarType>(
				static_cast<ScalarType>(1) / sqrt(static_cast<ScalarType>(vec4.x)),
				static_cast<ScalarType>(1) / sqrt(static_cast<ScalarType>(vec4.y)),
				static_cast<ScalarType>(1) / sqrt(static_cast<ScalarType>(vec4.z)),
				static_cast<ScalarType>(1) / sqrt(static_cast<ScalarType>(vec4.w)));
		}

		template <typename T>
		static Vector2<ScalarType> InvSqrtApprox(const Vector2<T>& vec2)
		{
			return InvSqrt(vec2);
		}

		template <typename T>
		static Vector3<ScalarType> InvSqrtApprox(const Vector3<T>& vec3)
		{
			return InvSqrt(vec3);
		}

		template <typename T>
		static Vector4<ScalarType> InvSqrtApprox(const Vector4<T>& vec4)
		{
			return InvSqrt(vec4);
		}

		template <typename T>
		static T Dot(const Vector2<T>& v1, const Vector2<T>& v2)
		{
			const Vector2<T> tmp(v1 * v2);
			return tmp.x + tmp.y;
		}

		template <typename T>
		static T Dot(const Vector3<T>& v1, const Vector3<T>& v2)
		{
			const Vector3<T> tmp(v1 * v2);
			return tmp.x + tmp.y + tmp.z;
		}

		template <typename T>
		static T Dot(const Vector4<T>& v1, const Vector4<T>& v2)
		{
			const Vector4<T> tmp(v1 * v2);
			return tmp.x + tmp.y + tmp.z + tmp.w;
		}

		template <typename T>
		static Vector2<ScalarType> Normalize(const Vector2<T>& vec2)
		{
			const ScalarType dot_inv_sqrt =
				Exs::InvSqrt(static_cast<ScalarType>(Dot(vec2, vec2)));

			return Vector2<ScalarType>(
				static_cast<ScalarType>(vec2.x) * dot_inv_sqrt,
				static_cast<ScalarType>(vec2.y) * dot_inv_sqrt);

		}

		template <typename T>
		static Vector3<ScalarType> Normalize(const Vector3<T>& vec3)
		{
			const ScalarType dot_inv_sqrt =
				Exs::InvSqrt(static_cast<ScalarType>(Dot(vec3, vec3)));

			return Vector3<ScalarType>(
				static_cast<ScalarType>(vec3.x) * dot_inv_sqrt,
				static_cast<ScalarType>(vec3.y) * dot_inv_sqrt,
				static_cast<ScalarType>(vec3.z) * dot_inv_sqrt);
		}

		template <typename T>
		static Vector4<ScalarType> Normalize(const Vector4<T>& vec4)
		{
			const ScalarType dot_inv_sqrt =
				Exs::InvSqrt(static_cast<ScalarType>(Dot(vec4, vec4)));

			return Vector4<ScalarType>(
				static_cast<ScalarType>(vec4.x) * dot_inv_sqrt,
				static_cast<ScalarType>(vec4.y) * dot_inv_sqrt,
				static_cast<ScalarType>(vec4.z) * dot_inv_sqrt,
				static_cast<ScalarType>(vec4.w) * dot_inv_sqrt);
		}

		template <typename T>
		static Vector2<ScalarType> NormalizeApprox(const Vector2<T>& vec2)
		{
			return Normalize(vec2);

		}

		template <typename T>
		static Vector3<ScalarType> NormalizeApprox(const Vector3<T>& vec3)
		{
			return Normalize(vec3);
		}

		template <typename T>
		static Vector4<ScalarType> NormalizeApprox(const Vector4<T>& vec4)
		{
			return Normalize(vec4);
		}

		template <typename T>
		static ScalarType Length(const Vector2<T>& vec2)
		{
			return Exs::Sqrt(static_cast<ScalarType>(Dot(vec2, vec2)));
		}

		template <typename T>
		static ScalarType Length(const Vector3<T>& vec3)
		{
			return Exs::Sqrt(static_cast<ScalarType>(Dot(vec3, vec3)));
		}

		template <typename T>
		static ScalarType Length(const Vector4<T>& vec4)
		{
			return Exs::Sqrt(static_cast<ScalarType>(Dot(vec4, vec4)));
		}

		template <typename T>
		static ScalarType Distance(const Vector2<T>& v1, const Vector2<T>& v2)
		{
			return Length(v2 - v1);
		}

		template <typename T>
		static ScalarType Distance(const Vector3<T>& v1, const Vector3<T>& v2)
		{
			return Length(v2 - v1);
		}

		template <typename T>
		static ScalarType Distance(const Vector4<T>& v1, const Vector4<T>& v2)
		{
			return Length(v2 - v1);
		}

		template <typename T>
		static Vector3<ScalarType> Cross(const Vector3<T>& v1, const Vector3<T>& v2)
		{
			return Vector3<ScalarType>(
				static_cast<ScalarType>(v1.y) * v2.z - static_cast<ScalarType>(v1.z) * v2.y,
				static_cast<ScalarType>(v1.z) * v2.x - static_cast<ScalarType>(v1.x) * v2.z,
				static_cast<ScalarType>(v1.x) * v2.y - static_cast<ScalarType>(v1.y) * v2.x);
		}
	};


}


#if ( EXS_MATH_USE_SIMD_INTRINSICS )
#  include "VectorUtilsSIMD.inl"
#endif


#endif /* __Exs_Math_VectorUtils_H__ */
