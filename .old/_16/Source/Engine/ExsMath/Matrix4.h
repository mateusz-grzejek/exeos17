
#pragma once

#ifndef __Exs_Math_Matrix4_H__
#define __Exs_Math_Matrix4_H__

#include "MatrixBase.h"
#include "Matrix3.h"
#include "Vector4.h"


namespace Exs
{


	template <typename T>
	struct Matrix4x4 : public MatrixBase<T, 4, 4>
	{
	protected:
		Vector4<T> _c0, _c1, _c2, _c3;

	public:
		Matrix4x4()
		: _c0(1, 0, 0, 0)
		, _c1(0, 1, 0, 0)
		, _c2(0, 0, 1, 0)
		, _c3(0, 0, 0, 1)
		{ }

		Matrix4x4(const Matrix4x4<T>& origin)
		: _c0(origin._c0)
		, _c1(origin._c1)
		, _c2(origin._c2)
		, _c3(origin._c3)
		{ }

		template <typename U>
		Matrix4x4(const Matrix4x4<U>& mat4)
		: _c0(mat4[0])
		, _c1(mat4[1])
		, _c2(mat4[2])
		, _c3(mat4[3])
		{ }

		Matrix4x4(const Vector4<T>& c0, const Vector4<T>& c1, const Vector4<T>& c2, const Vector4<T>& c3)
		: _c0(c0)
		, _c1(c1)
		, _c2(c2)
		, _c3(c3)
		{ }

		template <typename C0, typename C1, typename C2, typename C3>
		Matrix4x4(const Vector4<C0>& c0, const Vector4<C1>& c1, const Vector4<C2>& c2, const Vector4<C3>& c3)
		: _c0(c0)
		, _c1(c1)
		, _c2(c2)
		, _c3(c3)
		{ }

		Matrix4x4(	T x0, T y0, T z0, T w0,
					T x1, T y1, T z1, T w1,
					T x2, T y2, T z2, T w2,
					T x3, T y3, T z3, T w3)
		: _c0(x0, y0, z0, w0)
		, _c1(x1, y1, z1, w1)
		, _c2(x2, y2, z2, w2)
		, _c3(x3, y3, z3, w3)
		{ }

		template <	typename Tx0, typename Ty0, typename Tz0, typename Tw0,
					typename Tx1, typename Ty1, typename Tz1, typename Tw1,
					typename Tx2, typename Ty2, typename Tz2, typename Tw2,
					typename Tx3, typename Ty3, typename Tz3, typename Tw3>
		Matrix4x4(	Tx0 x0, Ty0 y0, Tz0 z0, Tw0 w0,
					Tx1 x1, Ty1 y1, Tz1 z1, Tw1 w1,
					Tx2 x2, Ty2 y2, Tz2 z2, Tw2 w2,
					Tx3 x3, Ty3 y3, Tz3 z3, Tw3 w3)
		: _c0(static_cast<T>(x0), y0, z0, w0)
		, _c1(static_cast<T>(x1), y1, z1, w1)
		, _c2(static_cast<T>(x2), y2, z2, w2)
		, _c3(static_cast<T>(x3), y3, z3, w3)
		{ }

		template <typename U>
		Matrix4x4(U scalar)
		: _c0(static_cast<T>(scalar), 0, 0, 0)
		, _c1(0, static_cast<T>(scalar), 0, 0)
		, _c2(0, 0, static_cast<T>(scalar), 0)
		, _c3(0, 0, 0, static_cast<T>(scalar))
		{ }

		Matrix4x4(const T* col0, const T* col1, const T* col2, const T* col3)
		: _c0(col0)
		, _c1(col1)
		, _c2(col2)
		, _c3(col3)
		{ }

		explicit Matrix4x4(const UninitializedTag&)
		{ }

		Matrix4x4<T>& operator=(const Matrix4x4<T>& rhs)
		{
			this->_c0 = rhs._c0;
			this->_c1 = rhs._c1;
			this->_c2 = rhs._c2;
			this->_c3 = rhs._c3;

			return *this;
		}

		template <typename U>
		Matrix4x4<T>& operator=(const Matrix4x4<U>& rhs)
		{
			this->_c0 = rhs[0];
			this->_c1 = rhs[1];
			this->_c2 = rhs[2];
			this->_c3 = rhs[3];

			return *this;
		}

		operator Matrix3x3<T>() const
		{
			return Matrix3x3<T>(
				this->_c0.Data(),
				this->_c1.Data(),
				this->_c2.Data());
		}

		T* Data()
		{
			return this->_c0.Data();
		}

		const T* Data() const
		{
			return this->_c0.Data();
		}

		Vector4<T>& operator[](Size_t index)
		{
			ExsDebugAssert( index < 4 );
			return (&(this->_c0))[index];
		}

		const Vector4<T>& operator[](Size_t index) const
		{
			ExsDebugAssert( index < 4 );
			return (&(this->_c0))[index];
		}

		template <typename U>
		Matrix4x4<T>& operator+=(const Matrix4x4<U>& rhs)
		{
			this->_c0 += rhs[0];
			this->_c1 += rhs[1];
			this->_c2 += rhs[2];
			this->_c3 += rhs[3];

			return *this;
		}

		template <typename U>
		Matrix4x4<T>& operator+=(U rhs)
		{
			this->_c0 += rhs;
			this->_c1 += rhs;
			this->_c2 += rhs;
			this->_c3 += rhs;

			return *this;
		}

		template <typename U>
		Matrix4x4<T>& operator-=(const Matrix4x4<U>& rhs)
		{
			this->_c0 -= rhs[0];
			this->_c1 -= rhs[1];
			this->_c2 -= rhs[2];
			this->_c3 -= rhs[3];

			return *this;
		}

		template <typename U>
		Matrix4x4<T>& operator-=(U rhs)
		{
			this->_c0 -= rhs;
			this->_c1 -= rhs;
			this->_c2 -= rhs;
			this->_c3 -= rhs;

			return *this;
		}

		template <typename U>
		Matrix4x4<T>& operator*=(const Matrix4x4<U>& rhs)
		{
			const Vector4<T>& mc0 = this->_c0;
			const Vector4<T>& mc1 = this->_c1;
			const Vector4<T>& mc2 = this->_c2;
			const Vector4<T>& mc3 = this->_c3;

			const Vector4<T>& rc0 = rhs[0];
			const Vector4<T>& rc1 = rhs[1];
			const Vector4<T>& rc2 = rhs[2];
			const Vector4<T>& rc3 = rhs[3];

			this->_c0 = (mc0 * rc0.x + mc1 * rc0.y) + (mc2 * rc0.z + mc3 * rc0.w);
			this->_c1 = (mc0 * rc1.x + mc1 * rc1.y) + (mc2 * rc1.z + mc3 * rc1.w);
			this->_c2 = (mc0 * rc2.x + mc1 * rc2.y) + (mc2 * rc2.z + mc3 * rc2.w);
			this->_c3 = (mc0 * rc3.x + mc1 * rc3.y) + (mc2 * rc3.z + mc3 * rc3.w);

			return *this;
		}

		template <typename U>
		Matrix4x4<T>& operator*=(U rhs)
		{
			this->_c0 *= rhs;
			this->_c1 *= rhs;
			this->_c2 *= rhs;
			this->_c3 *= rhs;

			return *this;
		}

		template <typename U>
		Matrix4x4<T>& operator/=(U rhs)
		{
			this->_c0 /= rhs;
			this->_c1 /= rhs;
			this->_c2 /= rhs;
			this->_c3 /= rhs;

			return *this;
		}
	};

	
	template <typename T>
	inline Matrix4x4<T> operator+(const Matrix4x4<T>& lhs, const Matrix4x4<T>& rhs)
	{
		return Matrix4x4<T>(
			lhs[0] + rhs[0],
			lhs[1] + rhs[1],
			lhs[2] + rhs[2],
			lhs[3] + rhs[3]);
	}

	template <typename T>
	inline Matrix4x4<T> operator+(const Matrix4x4<T>& lhs, T rhs)
	{
		return Matrix4x4<T>(
			lhs[0] + rhs,
			lhs[1] + rhs,
			lhs[2] + rhs,
			lhs[3] + rhs);
	}

	template <typename T>
	inline Matrix4x4<T> operator+(T lhs, const Matrix4x4<T>& rhs)
	{
		return Matrix4x4<T>(
			lhs + rhs[0],
			lhs + rhs[1],
			lhs + rhs[2],
			lhs + rhs[3]);
	}

	template <typename T>
	inline Matrix4x4<T> operator-(const Matrix4x4<T>& lhs, const Matrix4x4<T>& rhs)
	{
		return Matrix4x4<T>(
			lhs[0] - rhs[0],
			lhs[1] - rhs[1],
			lhs[2] - rhs[2],
			lhs[3] - rhs[3]);
	}

	template <typename T>
	inline Matrix4x4<T> operator-(const Matrix4x4<T>& lhs, T rhs)
	{
		return Matrix4x4<T>(
			lhs[0] - rhs,
			lhs[1] - rhs,
			lhs[2] - rhs,
			lhs[3] - rhs);
	}

	template <typename T>
	inline Matrix4x4<T> operator-(T lhs, const Matrix4x4<T>& rhs)
	{
		return Matrix4x4<T>(
			lhs - rhs[0],
			lhs - rhs[1],
			lhs - rhs[2],
			lhs - rhs[3]);
	}

	template <typename T>
	inline Matrix4x4<T> operator*(const Matrix4x4<T>& lhs, const Matrix4x4<T>& rhs)
	{
		const Vector4<T>& lc0 = lhs[0];
		const Vector4<T>& lc1 = lhs[1];
		const Vector4<T>& lc2 = lhs[2];
		const Vector4<T>& lc3 = lhs[3];

		const Vector4<T>& rc0 = rhs[0];
		const Vector4<T>& rc1 = rhs[1];
		const Vector4<T>& rc2 = rhs[2];
		const Vector4<T>& rc3 = rhs[3];

		return Matrix4x4<T>(
			(lc0 * rc0.x + lc1 * rc0.y) + (lc2 * rc0.z + lc3 * rc0.w),
			(lc0 * rc1.x + lc1 * rc1.y) + (lc2 * rc1.z + lc3 * rc1.w),
			(lc0 * rc2.x + lc1 * rc2.y) + (lc2 * rc2.z + lc3 * rc2.w),
			(lc0 * rc3.x + lc1 * rc3.y) + (lc2 * rc3.z + lc3 * rc3.w));
	}

	template <typename T>
	inline Vector4<T> operator*(const Matrix4x4<T>& lhs, const Vector4<T>& rhs)
	{
		const Vector4<T>& c0 = lhs[0];
		const Vector4<T>& c1 = lhs[1];
		const Vector4<T>& c2 = lhs[2];
		const Vector4<T>& c3 = lhs[3];

		return Vector4<T>(
			Vector<>::Dot(Vector4<T>(c0[0], c1[0], c2[0], c3[0]), rhs),
			Vector<>::Dot(Vector4<T>(c0[1], c1[1], c2[1], c3[1]), rhs),
			Vector<>::Dot(Vector4<T>(c0[2], c1[2], c2[2], c3[2]), rhs),
			Vector<>::Dot(Vector4<T>(c0[3], c1[3], c2[3], c3[3]), rhs));
	}

	template <typename T>
	inline Vector4<T> operator*(const Vector4<T>& lhs, const Matrix4x4<T>& rhs)
	{
		return Vector4<T>(
			Vector<>::Dot(lhs, rhs[0]),
			Vector<>::Dot(lhs, rhs[1]),
			Vector<>::Dot(lhs, rhs[2]),
			Vector<>::Dot(lhs, rhs[3]));
	}

	template <typename T>
	inline Matrix4x4<T> operator*(const Matrix4x4<T>& lhs, T rhs)
	{
		return Matrix4x4<T>(
			lhs[0] * rhs,
			lhs[1] * rhs,
			lhs[2] * rhs,
			lhs[3] * rhs);
	}

	template <typename T>
	inline Matrix4x4<T> operator*(T lhs, const Matrix4x4<T>& rhs)
	{
		return Matrix4x4<T>(
			lhs * rhs[0],
			lhs * rhs[1],
			lhs * rhs[2],
			lhs * rhs[3]);
	}

	template <typename T>
	inline Matrix4x4<T> operator/(const Matrix4x4<T>& lhs, T rhs)
	{
		return Matrix4x4<T>(
			lhs[0] / rhs,
			lhs[1] / rhs,
			lhs[2] / rhs,
			lhs[3] / rhs);
	}

	template <typename T>
	inline Matrix4x4<T> operator/(T lhs, const Matrix4x4<T>& rhs)
	{
		return Matrix4x4<T>(
			lhs / rhs[0],
			lhs / rhs[1],
			lhs / rhs[2],
			lhs / rhs[3]);
	}

	template <typename T, typename U>
	inline Matrix4x4<T> operator-(const Matrix4x4<T>& rhs)
	{
		return Matrix4x4<T>(-rhs[0], -rhs[1], -rhs[2], -rhs[3]);
	}

	
	template <typename T>
	inline bool operator==(const Matrix4x4<T>& lhs, const Matrix4x4<T>& rhs)
	{
		return (lhs[0] == rhs[0]) && (lhs[1] == rhs[1]) && (lhs[2] == rhs[2]) && (lhs[3] == rhs[3]);
	}

	template <typename T>
	inline bool operator!=(const Matrix4x4<T>& lhs, const Matrix4x4<T>& rhs)
	{
		return (lhs[0] != rhs[0]) || (lhs[1] != rhs[1]) || (lhs[2] != rhs[2]) || (lhs[3] != rhs[3]);
	}


	typedef Matrix4x4<float>     Matrix4F;
	typedef Matrix4x4<double>    Matrix4D;
	typedef Matrix4x4<Int32>     Matrix4I32;
	typedef Matrix4x4<Uint32>    Matrix4U32;
	typedef Matrix4x4<Int64>     Matrix4I64;
	typedef Matrix4x4<Uint64>    Matrix4U64;


}


#if ( EXS_MATH_USE_SIMD_INTRINSICS )
#  include "Matrix4SIMD.inl"
#endif


#endif /* __Exs_Math_Matrix4_H__ */
