
#pragma once

#ifndef __Exs_Math_Quaternion_H__
#define __Exs_Math_Quaternion_H__

#include "Vector3.h"
#include "Vector4.h"
#include "Matrix4.h"


namespace Exs
{


	template <typename T>
	struct Quaternion
	{
	};


	typedef Quaternion<ScalarPrecisionType<Precision::Default>::Type> Quat;
	
	typedef Quaternion<float>	QuatF;
	typedef Quaternion<double>	QuatD;
	typedef Quaternion<Int32>	QuatI32;
	typedef Quaternion<Int64>	QuatI64;


}


#endif /* __Exs_Math_Quaternion_H__ */
