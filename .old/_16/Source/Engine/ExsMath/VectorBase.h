
#pragma once

#ifndef __Exs_Math_VectorBase_H__
#define __Exs_Math_VectorBase_H__

#include "MathBase.h"


#if ( EXS_CONFIG_BASE_ENABLE_EXTENDED_INSTRUCTION_SET && EXS_FEATURE_ANONYMOUS_UNIONS )
#
#  if ( EXS_SUPPORTED_EIS & EXS_EIS_SSE ) // SSE -> float[4] (__m128)
#    define EXS_MATH_USE_VX128F 1
#  else
#    define EXS_MATH_USE_VX128F 0
#  endif
#
#  if ( EXS_SUPPORTED_EIS & EXS_EIS_SSE2 ) // SSE2 -> int32[4] (__m128i)
#    define EXS_MATH_USE_VX128I 1
#  else
#    define EXS_MATH_USE_VX128I 0
#  endif
#
#  if ( EXS_SUPPORTED_EIS & EXS_EIS_AVX ) // AVX -> double[4] (__m256d)
#    define EXS_MATH_USE_VX256D 1
#  else
#    define EXS_MATH_USE_VX256D 0
#  endif
#
#  if ( EXS_SUPPORTED_EIS & EXS_EIS_AVX2 ) // AVX2 -> int64[4] (__m256i)
#    define EXS_MATH_USE_VX256I 1
#  else
#    define EXS_MATH_USE_VX256I 0
#  endif
#
#endif


#if ( EXS_MATH_USE_VX128F || EXS_MATH_USE_VX128I || EXS_MATH_USE_VX256D || EXS_MATH_USE_VX256I )
#  define EXS_MATH_USE_SIMD_INTRINSICS 1
#else
#  define EXS_MATH_USE_SIMD_INTRINSICS 0
#endif


namespace Exs
{
	

	namespace Internal
	{

		template <typename Type, Size_t Size>
		struct VectorSIMD
		{
			typedef Type DataType[Size];
		};

		
	#if ( EXS_MATH_USE_VX128F )

		template <>
		struct VectorSIMD<float, 3>
		{
		public:
			typedef __m128 DataType;
		};

		template <>
		struct VectorSIMD<float, 4>
		{
		public:
			typedef __m128 DataType;
		};

	#endif


	#if ( EXS_MATH_USE_VX128I )

		template <>
		struct VectorSIMD<Int32, 3>
		{
		public:
			typedef __m128i DataType;
		};

		template <>
		struct VectorSIMD<Int32, 4>
		{
		public:
			typedef __m128i DataType;
		};

	#endif


	#if ( EXS_MATH_USE_VX256D )

		template <>
		struct VectorSIMD<double, 3>
		{
		public:
			typedef __m256d DataType;
		};

		template <>
		struct VectorSIMD<double, 4>
		{
		public:
			typedef __m256d DataType;
		};

	#endif
		

	#if ( EXS_MATH_USE_VX256I )

		template <>
		struct VectorSIMD<Int64, 3>
		{
		public:
			typedef __m256i DataType;
		};

		template <>
		struct VectorSIMD<Int64, 4>
		{
		public:
			typedef __m256i DataType;
		};

	#endif

	}


	template <typename T, Size_t N>
	struct VectorBase
	{
	public:
		static const Size_t length = N;
		static const Size_t sizeInBytes = sizeof(T) * length;

		struct Uninitialized
		{
		};
	};


}


#endif /* __Exs_Math_VectorBase_H__ */
