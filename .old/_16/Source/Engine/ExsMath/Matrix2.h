
#pragma once

#ifndef __Exs_Math_Matrix2_H__
#define __Exs_Math_Matrix2_H__

#include "MatrixBase.h"
#include "Vector2.h"


namespace Exs
{


	template <typename T>
	struct Matrix2x2 : public MatrixBase<T, 2, 2>
	{
	protected:
		Vector2<T> _c0, _c1;

	public:
		Matrix2x2()
		: _c0(1, 0)
		, _c1(0, 1)
		{ }

		Matrix2x2(const Matrix2x2<T>& origin)
		: _c0(origin._c0)
		, _c1(origin._c1)
		{ }

		template <typename U>
		Matrix2x2(const Matrix2x2<U>& mat2)
		: _c0(mat2[0])
		, _c1(mat2[1])
		{ }

		Matrix2x2(const Vector2<T>& c0, const Vector2<T>& c1)
		: _c0(c0)
		, _c1(c1)
		{ }

		template <typename C0, typename C1>
		Matrix2x2(const Vector2<C0>& c0, const Vector2<C1>& c1)
		: _c0(c0)
		, _c1(c1)
		{ }

		Matrix2x2(	T x0, T y0,
					T x1, T y1)
		: _c0(x0, y0)
		, _c1(x1, y1)
		{ }

		template <	typename Tx0, typename Ty0,
					typename Tx1, typename Ty1>
		Matrix2x2(	Tx0 x0, Ty0 y0,
					Tx1 x1, Ty1 y1)
		: _c0(static_cast<T>(x0), y0)
		, _c1(static_cast<T>(x1), y1)
		{ }

		template <typename U>
		Matrix2x2(U scalar)
		: _c0(static_cast<T>(scalar), 0)
		, _c1(0, static_cast<T>(scalar))
		{ }

		Matrix2x2(const T* col0, const T* col1)
		: _c0(col0)
		, _c1(col1)
		{ }

		explicit Matrix2x2(const UninitializedTag&)
		{ }

		Matrix2x2<T>& operator=(const Matrix2x2<T>& rhs)
		{
			this->_c0 = rhs._c0;
			this->_c1 = rhs._c1;
	
			return *this;
		}

		template <typename U>
		Matrix2x2<T>& operator=(const Matrix2x2<U>& rhs)
		{
			this->_c0 = rhs[0];
			this->_c1 = rhs[1];
	
			return *this;
		}

		T* Data()
		{
			return this->_c0.Data();
		}

		const T* Data() const
		{
			return this->_c0.Data();
		}

		Vector2<T>& operator[](Size_t index)
		{
			ExsDebugAssert( index < 2 );
			return (&(this->_c0))[index];
		}

		const Vector2<T>& operator[](Size_t index) const
		{
			ExsDebugAssert( index < 2 );
			return (&(this->_c0))[index];
		}

		template <typename U>
		Matrix2x2<T>& operator+=(const Matrix2x2<U>& rhs)
		{
			this->_c0 += rhs[0];
			this->_c1 += rhs[1];
	
			return *this;
		}

		template <typename U>
		Matrix2x2<T>& operator+=(U rhs)
		{
			this->_c0 += rhs;
			this->_c1 += rhs;
	
			return *this;
		}

		template <typename U>
		Matrix2x2<T>& operator-=(const Matrix2x2<U>& rhs)
		{
			this->_c0 -= rhs[0];
			this->_c1 -= rhs[1];
	
			return *this;
		}

		template <typename U>
		Matrix2x2<T>& operator-=(U rhs)
		{
			this->_c0 -= rhs;
			this->_c1 -= rhs;
	
			return *this;
		}

		template <typename U>
		Matrix2x2<T>& operator*=(const Matrix2x2<U>& rhs)
		{
			const Vector2<T>& mc0 = this->_c0;
			const Vector2<T>& mc1 = this->_c1;
			
			const Vector2<T>& rc0 = rhs[0];
			const Vector2<T>& rc1 = rhs[1];

			this->_c0 = mc0 * rc0.x + mc1 * rc0.y;
			this->_c1 = mc0 * rc1.x + mc1 * rc1.y;

			return *this;
		}

		template <typename U>
		Matrix2x2<T>& operator*=(U rhs)
		{
			this->_c0 *= rhs;
			this->_c1 *= rhs;
	
			return *this;
		}

		template <typename U>
		Matrix2x2<T>& operator/=(U rhs)
		{
			this->_c0 /= rhs;
			this->_c1 /= rhs;
	
			return *this;
		}
	};

	
	template <typename T>
	inline Matrix2x2<T> operator+(const Matrix2x2<T>& lhs, const Matrix2x2<T>& rhs)
	{
		return Matrix2x2<T>(
			lhs[0] + rhs[0],
			lhs[1] + rhs[1]);
	}

	template <typename T>
	inline Matrix2x2<T> operator+(const Matrix2x2<T>& lhs, T rhs)
	{
		return Matrix2x2<T>(
			lhs[0] + rhs,
			lhs[1] + rhs);
	}

	template <typename T>
	inline Matrix2x2<T> operator+(T lhs, const Matrix2x2<T>& rhs)
	{
		return Matrix2x2<T>(
			lhs + rhs[0],
			lhs + rhs[1]);
	}

	template <typename T>
	inline Matrix2x2<T> operator-(const Matrix2x2<T>& lhs, const Matrix2x2<T>& rhs)
	{
		return Matrix2x2<T>(
			lhs[0] - rhs[0],
			lhs[1] - rhs[1]);
	}

	template <typename T>
	inline Matrix2x2<T> operator-(const Matrix2x2<T>& lhs, T rhs)
	{
		return Matrix2x2<T>(
			lhs[0] - rhs,
			lhs[1] - rhs);
	}

	template <typename T>
	inline Matrix2x2<T> operator-(T lhs, const Matrix2x2<T>& rhs)
	{
		return Matrix2x2<T>(
			lhs - rhs[0],
			lhs - rhs[1]);
	}

	template <typename T>
	inline Matrix2x2<T> operator*(const Matrix2x2<T>& lhs, const Matrix2x2<T>& rhs)
	{
		const Vector2<T>& lc0 = lhs[0];
		const Vector2<T>& lc1 = lhs[1];

		const Vector2<T>& rc0 = rhs[0];
		const Vector2<T>& rc1 = rhs[1];

		return Matrix4x4<T>(
			lc0 * rc0.x + lc1 * rc0.y,
			lc0 * rc1.x + lc1 * rc1.y);
	}

	template <typename T>
	inline Vector2<T> operator*(const Matrix2x2<T>& lhs, const Vector2<T>& rhs)
	{
		const Vector2<T>& c0 = lhs[0];
		const Vector2<T>& c1 = lhs[1];

		return Vector2<T>(
			Vector<>::Dot(Vector2<T>(c0[0], c1[0]), rhs),
			Vector<>::Dot(Vector2<T>(c0[1], c1[1]), rhs));
	}

	template <typename T>
	inline Vector2<T> operator*(const Vector2<T>& lhs, const Matrix2x2<T>& rhs)
	{
		return Vector2<T>(
			Vector<>::Dot(lhs, rhs[0]),
			Vector<>::Dot(lhs, rhs[1]));
	}

	template <typename T>
	inline Matrix2x2<T> operator*(const Matrix2x2<T>& lhs, T rhs)
	{
		return Matrix2x2<T>(
			lhs[0] * rhs,
			lhs[1] * rhs);
	}

	template <typename T>
	inline Matrix2x2<T> operator*(T lhs, const Matrix2x2<T>& rhs)
	{
		return Matrix2x2<T>(
			lhs * rhs[0],
			lhs * rhs[1]);
	}
	
	template <typename T>
	inline Matrix2x2<T> operator/(const Matrix2x2<T>& lhs, T rhs)
	{
		return Matrix4x4<T>(
			lhs[0] / rhs,
			lhs[1] / rhs);
	}

	template <typename T>
	inline Matrix2x2<T> operator/(T lhs, const Matrix2x2<T>& rhs)
	{
		return Matrix2x2<T>(
			lhs / rhs[0],
			lhs / rhs[1]);
	}

	template <typename T, typename U>
	inline Matrix2x2<T> operator-(const Matrix2x2<T>& rhs)
	{
		return Matrix2x2<T>(-rhs[0], -rhs[1]);
	}

	
	template <typename T>
	inline bool operator==(const Matrix2x2<T>& lhs, const Matrix2x2<T>& rhs)
	{
		return (lhs[0] == rhs[0]) && (lhs[1] == rhs[1]);
	}

	template <typename T>
	inline bool operator!=(const Matrix2x2<T>& lhs, const Matrix2x2<T>& rhs)
	{
		return (lhs[0] != rhs[0]) || (lhs[1] != rhs[1]);
	}

	
	typedef Matrix2x2<float>     Matrix2F;
	typedef Matrix2x2<double>    Matrix2D;
	typedef Matrix2x2<Int32>     Matrix2I32;
	typedef Matrix2x2<Uint32>    Matrix2U32;
	typedef Matrix2x2<Int64>     Matrix2I64;
	typedef Matrix2x2<Uint64>    Matrix2U64;


}


#endif /* __Exs_Math_Matrix2_H__ */
