
namespace Exs
{


#if ( EXS_MATH_USE_VX128F )


	template <>
	ExsForceInline Vector4<float>::Vector4()
	: _vxd(_mm_setzero_ps())
	{ }

	template <>
	template <>
	ExsForceInline Vector4<float>::Vector4(float x, float y, float z, float w)
	: _vxd(_mm_set_ps(w, z, y, x))
	{ }

	template <>
	template <>
	ExsForceInline Vector4<float>::Vector4(float scalar)
	: _vxd(_mm_set1_ps(scalar))
	{ }

	template <>
	template <>
	ExsForceInline Vector4<float>::Vector4(const float* data)
	: _vxd(_mm_loadu_ps(data))
	{ }

	template <>
	template <>
	ExsForceInline Vector4<float>& Vector4<float>::operator=(float rhs)
	{
		this->_vxd = _mm_set1_ps(rhs);
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<float>& Vector4<float>::operator+=(const Vector4<float>& rhs)
	{
		this->_vxd = _mm_add_ps(this->_vxd, rhs._vxd);
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<float>& Vector4<float>::operator+=(float rhs)
	{
		this->_vxd = _mm_add_ps(this->_vxd, _mm_set1_ps(rhs));
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<float>& Vector4<float>::operator-=(const Vector4<float>& rhs)
	{
		this->_vxd = _mm_sub_ps(this->_vxd, rhs._vxd);
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<float>& Vector4<float>::operator-=(float rhs)
	{
		this->_vxd = _mm_sub_ps(this->_vxd, _mm_set1_ps(rhs));
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<float>& Vector4<float>::operator*=(const Vector4<float>& rhs)
	{
		this->_vxd = _mm_mul_ps(this->_vxd, rhs._vxd);
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<float>& Vector4<float>::operator*=(float rhs)
	{
		this->_vxd = _mm_mul_ps(this->_vxd, _mm_set1_ps(rhs));
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<float>& Vector4<float>::operator/=(const Vector4<float>& rhs)
	{
		this->_vxd = _mm_div_ps(this->_vxd, rhs._vxd);
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<float>& Vector4<float>::operator/=(float rhs)
	{
		this->_vxd = _mm_div_ps(this->_vxd, _mm_set1_ps(rhs));
		return *this;
	}

	
	template <>
	ExsForceInline Vector4<float> operator+(const Vector4<float>& lhs, const Vector4<float>& rhs)
	{
		const __m128 result = _mm_add_ps(lhs._vxd, rhs._vxd);
		return Vector4<float>(reinterpret_cast<const float*>(&result));
	}

	template <>
	ExsForceInline Vector4<float> operator+(const Vector4<float>& lhs, float rhs)
	{
		const __m128 result = _mm_add_ps(lhs._vxd, _mm_set1_ps(rhs));
		return Vector4<float>(reinterpret_cast<const float*>(&result));
	}

	template <>
	ExsForceInline Vector4<float> operator+(float lhs, const Vector4<float>& rhs)
	{
		const __m128 result = _mm_add_ps(_mm_set1_ps(lhs), rhs._vxd);
		return Vector4<float>(reinterpret_cast<const float*>(&result));
	}

	template <>
	ExsForceInline Vector4<float> operator-(const Vector4<float>& lhs, const Vector4<float>& rhs)
	{
		const __m128 result = _mm_sub_ps(lhs._vxd, rhs._vxd);
		return Vector4<float>(reinterpret_cast<const float*>(&result));
	}

	template <>
	ExsForceInline Vector4<float> operator-(const Vector4<float>& lhs, float rhs)
	{
		const __m128 result = _mm_sub_ps(lhs._vxd, _mm_set1_ps(rhs));
		return Vector4<float>(reinterpret_cast<const float*>(&result));
	}

	template <>
	ExsForceInline Vector4<float> operator-(float lhs, const Vector4<float>& rhs)
	{
		const __m128 result = _mm_sub_ps(_mm_set1_ps(lhs), rhs._vxd);
		return Vector4<float>(reinterpret_cast<const float*>(&result));
	}

	template <>
	ExsForceInline Vector4<float> operator*(const Vector4<float>& lhs, const Vector4<float>& rhs)
	{
		const __m128 result = _mm_mul_ps(lhs._vxd, rhs._vxd);
		return Vector4<float>(reinterpret_cast<const float*>(&result));
	}

	template <>
	ExsForceInline Vector4<float> operator*(const Vector4<float>& lhs, float rhs)
	{
		const __m128 result = _mm_mul_ps(lhs._vxd, _mm_set1_ps(rhs));
		return Vector4<float>(reinterpret_cast<const float*>(&result));
	}

	template <>
	ExsForceInline Vector4<float> operator*(float lhs, const Vector4<float>& rhs)
	{
		const __m128 result = _mm_mul_ps(_mm_set1_ps(lhs), rhs._vxd);
		return Vector4<float>(reinterpret_cast<const float*>(&result));
	}

	template <>
	ExsForceInline Vector4<float> operator/(const Vector4<float>& lhs, const Vector4<float>& rhs)
	{
		const __m128 result = _mm_div_ps(lhs._vxd, rhs._vxd);
		return Vector4<float>(reinterpret_cast<const float*>(&result));
	}

	template <>
	ExsForceInline Vector4<float> operator/(const Vector4<float>& lhs, float rhs)
	{
		const __m128 result = _mm_div_ps(lhs._vxd, _mm_set1_ps(rhs));
		return Vector4<float>(reinterpret_cast<const float*>(&result));
	}

	template <>
	ExsForceInline Vector4<float> operator/(float lhs, const Vector4<float>& rhs)
	{
		const __m128 result = _mm_div_ps(_mm_set1_ps(lhs), rhs._vxd);
		return Vector4<float>(reinterpret_cast<const float*>(&result));
	}

#endif


#if ( EXS_MATH_USE_VX128I )

	template <>
	ExsForceInline Vector4<Int32>::Vector4()
	: _vxd(_mm_setzero_si128())
	{ }

	template <>
	template <>
	ExsForceInline Vector4<Int32>::Vector4(Int32 x, Int32 y, Int32 z, Int32 w)
	: _vxd(_mm_set_epi32(w, z, y, x))
	{ }

	template <>
	template <>
	ExsForceInline Vector4<Int32>::Vector4(Int32 scalar)
	: _vxd(_mm_set1_epi32(scalar))
	{ }

	template <>
	template <>
	ExsForceInline Vector4<Int32>::Vector4(const Int32* data)
	: _vxd(_mm_loadu_si128(reinterpret_cast<const __m128i*>(data)))
	{ }

	template <>
	template <>
	ExsForceInline Vector4<Int32>& Vector4<Int32>::operator=(Int32 rhs)
	{
		this->_vxd = _mm_set1_epi32(rhs);
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<Int32>& Vector4<Int32>::operator+=(const Vector4<Int32>& rhs)
	{
		this->_vxd = _mm_add_epi32(this->_vxd, rhs._vxd);
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<Int32>& Vector4<Int32>::operator+=(Int32 rhs)
	{
		this->_vxd = _mm_add_epi32(this->_vxd, _mm_set1_epi32(rhs));
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<Int32>& Vector4<Int32>::operator-=(const Vector4<Int32>& rhs)
	{
		this->_vxd = _mm_sub_epi32(this->_vxd, rhs._vxd);
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<Int32>& Vector4<Int32>::operator-=(Int32 rhs)
	{
		this->_vxd = _mm_sub_epi32(this->_vxd, _mm_set1_epi32(rhs));
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<Int32>& Vector4<Int32>::operator*=(const Vector4<Int32>& rhs)
	{
		this->_vxd = _mm_mul_epi32(this->_vxd, rhs._vxd);
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<Int32>& Vector4<Int32>::operator*=(Int32 rhs)
	{
		this->_vxd = _mm_mul_epi32(this->_vxd, _mm_set1_epi32(rhs));
		return *this;
	}

	
	template <>
	ExsForceInline Vector4<Int32> operator+(const Vector4<Int32>& lhs, const Vector4<Int32>& rhs)
	{
		const __m128i result = _mm_add_epi32(lhs._vxd, rhs._vxd);
		return Vector4<Int32>(reinterpret_cast<const Int32*>(&result));
	}

	template <>
	ExsForceInline Vector4<Int32> operator+(const Vector4<Int32>& lhs, Int32 rhs)
	{
		const __m128i result = _mm_add_epi32(lhs._vxd, _mm_set1_epi32(rhs));
		return Vector4<Int32>(reinterpret_cast<const Int32*>(&result));
	}

	template <>
	ExsForceInline Vector4<Int32> operator+(Int32 lhs, const Vector4<Int32>& rhs)
	{
		const __m128i result = _mm_add_epi32(_mm_set1_epi32(lhs), rhs._vxd);
		return Vector4<Int32>(reinterpret_cast<const Int32*>(&result));
	}

	template <>
	ExsForceInline Vector4<Int32> operator-(const Vector4<Int32>& lhs, const Vector4<Int32>& rhs)
	{
		const __m128i result = _mm_sub_epi32(lhs._vxd, rhs._vxd);
		return Vector4<Int32>(reinterpret_cast<const Int32*>(&result));
	}

	template <>
	ExsForceInline Vector4<Int32> operator-(const Vector4<Int32>& lhs, Int32 rhs)
	{
		const __m128i result = _mm_sub_epi32(lhs._vxd, _mm_set1_epi32(rhs));
		return Vector4<Int32>(reinterpret_cast<const Int32*>(&result));
	}

	template <>
	ExsForceInline Vector4<Int32> operator-(Int32 lhs, const Vector4<Int32>& rhs)
	{
		const __m128i result = _mm_sub_epi32(_mm_set1_epi32(lhs), rhs._vxd);
		return Vector4<Int32>(reinterpret_cast<const Int32*>(&result));
	}
	
	template <>
	ExsForceInline Vector4<Int32> operator*(const Vector4<Int32>& lhs, const Vector4<Int32>& rhs)
	{
		const __m128i result = _mm_mul_epi32(lhs._vxd, rhs._vxd);
		return Vector4<Int32>(reinterpret_cast<const Int32*>(&result));
	}

	template <>
	ExsForceInline Vector4<Int32> operator*(const Vector4<Int32>& lhs, Int32 rhs)
	{
		const __m128i result = _mm_mul_epi32(lhs._vxd, _mm_set1_epi32(rhs));
		return Vector4<Int32>(reinterpret_cast<const Int32*>(&result));
	}

	template <>
	ExsForceInline Vector4<Int32> operator*(Int32 lhs, const Vector4<Int32>& rhs)
	{
		const __m128i result = _mm_mul_epi32(_mm_set1_epi32(lhs), rhs._vxd);
		return Vector4<Int32>(reinterpret_cast<const Int32*>(&result));
	}

#endif


#if ( EXS_MATH_USE_VX256D )

	template <>
	ExsForceInline Vector4<double>::Vector4()
	: _vxd(_mm256_setzero_pd())
	{ }

	template <>
	template <>
	ExsForceInline Vector4<double>::Vector4(double x, double y, double z, double w)
	: _vxd(_mm256_set_pd(w, z, y, x))
	{ }

	template <>
	template <>
	ExsForceInline Vector4<double>::Vector4(double scalar)
	: _vxd(_mm256_set1_pd(scalar))
	{ }

	template <>
	template <>
	ExsForceInline Vector4<double>::Vector4(const double* data)
	: _vxd(_mm256_loadu_pd(data))
	{ }

	template <>
	template <>
	ExsForceInline Vector4<double>& Vector4<double>::operator=(double rhs)
	{
		this->_vxd = _mm256_set1_pd(rhs);
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<double>& Vector4<double>::operator+=(const Vector4<double>& rhs)
	{
		this->_vxd = _mm256_add_pd(this->_vxd, rhs._vxd);
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<double>& Vector4<double>::operator+=(double rhs)
	{
		this->_vxd = _mm256_add_pd(this->_vxd, _mm256_set1_pd(rhs));
		return *this;
	}
	
	template <>
	template <>
	ExsForceInline Vector4<double>& Vector4<double>::operator-=(const Vector4<double>& rhs)
	{
		this->_vxd = _mm256_sub_pd(this->_vxd, rhs._vxd);
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<double>& Vector4<double>::operator-=(double rhs)
	{
		this->_vxd = _mm256_sub_pd(this->_vxd, _mm256_set1_pd(rhs));
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<double>& Vector4<double>::operator*=(const Vector4<double>& rhs)
	{
		this->_vxd = _mm256_mul_pd(this->_vxd, rhs._vxd);
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<double>& Vector4<double>::operator*=(double rhs)
	{
		this->_vxd = _mm256_mul_pd(this->_vxd, _mm256_set1_pd(rhs));
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<double>& Vector4<double>::operator/=(const Vector4<double>& rhs)
	{
		this->_vxd = _mm256_div_pd(this->_vxd, rhs._vxd);
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<double>& Vector4<double>::operator/=(double rhs)
	{
		this->_vxd = _mm256_div_pd(this->_vxd, _mm256_set1_pd(rhs));
		return *this;
	}

	
	template <>
	ExsForceInline Vector4<double> operator+(const Vector4<double>& lhs, const Vector4<double>& rhs)
	{
		const __m256d result = _mm256_add_pd(lhs._vxd, rhs._vxd);
		return Vector4<double>(reinterpret_cast<const double*>(&result));
	}

	template <>
	ExsForceInline Vector4<double> operator+(const Vector4<double>& lhs, double rhs)
	{
		const __m256d result = _mm256_add_pd(lhs._vxd, _mm256_set1_pd(rhs));
		return Vector4<double>(reinterpret_cast<const double*>(&result));
	}

	template <>
	ExsForceInline Vector4<double> operator+(double lhs, const Vector4<double>& rhs)
	{
		const __m256d result = _mm256_add_pd(_mm256_set1_pd(lhs), rhs._vxd);
		return Vector4<double>(reinterpret_cast<const double*>(&result));
	}

	template <>
	ExsForceInline Vector4<double> operator-(const Vector4<double>& lhs, const Vector4<double>& rhs)
	{
		const __m256d result = _mm256_sub_pd(lhs._vxd, rhs._vxd);
		return Vector4<double>(reinterpret_cast<const double*>(&result));
	}

	template <>
	ExsForceInline Vector4<double> operator-(const Vector4<double>& lhs, double rhs)
	{
		const __m256d result = _mm256_sub_pd(lhs._vxd, _mm256_set1_pd(rhs));
		return Vector4<double>(reinterpret_cast<const double*>(&result));
	}

	template <>
	ExsForceInline Vector4<double> operator-(double lhs, const Vector4<double>& rhs)
	{
		const __m256d result = _mm256_sub_pd(_mm256_set1_pd(lhs), rhs._vxd);
		return Vector4<double>(reinterpret_cast<const double*>(&result));
	}

	template <>
	ExsForceInline Vector4<double> operator*(const Vector4<double>& lhs, const Vector4<double>& rhs)
	{
		const __m256d result = _mm256_mul_pd(lhs._vxd, rhs._vxd);
		return Vector4<double>(reinterpret_cast<const double*>(&result));
	}

	template <>
	ExsForceInline Vector4<double> operator*(const Vector4<double>& lhs, double rhs)
	{
		const __m256d result = _mm256_mul_pd(lhs._vxd, _mm256_set1_pd(rhs));
		return Vector4<double>(reinterpret_cast<const double*>(&result));
	}

	template <>
	ExsForceInline Vector4<double> operator*(double lhs, const Vector4<double>& rhs)
	{
		const __m256d result = _mm256_mul_pd(_mm256_set1_pd(lhs), rhs._vxd);
		return Vector4<double>(reinterpret_cast<const double*>(&result));
	}

	template <>
	ExsForceInline Vector4<double> operator/(const Vector4<double>& lhs, const Vector4<double>& rhs)
	{
		const __m256d result = _mm256_div_pd(lhs._vxd, rhs._vxd);
		return Vector4<double>(reinterpret_cast<const double*>(&result));
	}

	template <>
	ExsForceInline Vector4<double> operator/(const Vector4<double>& lhs, double rhs)
	{
		const __m256d result = _mm256_div_pd(lhs._vxd, _mm256_set1_pd(rhs));
		return Vector4<double>(reinterpret_cast<const double*>(&result));
	}

	template <>
	ExsForceInline Vector4<double> operator/(double lhs, const Vector4<double>& rhs)
	{
		const __m256d result = _mm256_div_pd(_mm256_set1_pd(lhs), rhs._vxd);
		return Vector4<double>(reinterpret_cast<const double*>(&result));
	}

#endif


#if ( EXS_MATH_USE_VX256I )

	template <>
	ExsForceInline Vector4<Int64>::Vector4()
	: _vxd(_mm256_setzero_si256())
	{ }

	template <>
	template <>
	ExsForceInline Vector4<Int64>::Vector4(Int64 x, Int64 y, Int64 z, Int64 w)
	: _vxd(_mm256_set_epi64x(w, z, y, x))
	{ }

	template <>
	template <>
	ExsForceInline Vector4<Int64>::Vector4(Int64 scalar)
	: _vxd(_mm256_set1_epi64x(scalar))
	{ }

	template <>
	template <>
	ExsForceInline Vector4<Int64>::Vector4(const Int64* data)
	: _vxd(_mm256_loadu_si256(reinterpret_cast<const __m256i*>(data)))
	{ }

	template <>
	template <>
	ExsForceInline Vector4<Int64>& Vector4<Int64>::operator=(Int64 rhs)
	{
		this->_vxd = _mm256_set1_epi64x(rhs);
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<Int64>& Vector4<Int64>::operator+=(const Vector4<Int64>& rhs)
	{
		this->_vxd = _mm256_add_epi64(this->_vxd, rhs._vxd);
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<Int64>& Vector4<Int64>::operator+=(Int64 rhs)
	{
		this->_vxd = _mm256_add_epi64(this->_vxd, _mm256_set1_epi64x(rhs));
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<Int64>& Vector4<Int64>::operator-=(const Vector4<Int64>& rhs)
	{
		this->_vxd = _mm256_sub_epi64(this->_vxd, rhs._vxd);
		return *this;
	}

	template <>
	template <>
	ExsForceInline Vector4<Int64>& Vector4<Int64>::operator-=(Int64 rhs)
	{
		this->_vxd = _mm256_sub_epi64(this->_vxd, _mm256_set1_epi64x(rhs));
		return *this;
	}

	template <>
	ExsForceInline Vector4<Int64> operator+(const Vector4<Int64>& lhs, const Vector4<Int64>& rhs)
	{
		const __m256i result = _mm256_add_epi64(lhs._vxd, rhs._vxd);
		return Vector4<Int64>(reinterpret_cast<const Int64*>(&result));
	}

	template <>
	ExsForceInline Vector4<Int64> operator+(const Vector4<Int64>& lhs, Int64 rhs)
	{
		const __m256i result = _mm256_add_epi64(lhs._vxd, _mm256_set1_epi64x(rhs));
		return Vector4<Int64>(reinterpret_cast<const Int64*>(&result));
	}

	template <>
	ExsForceInline Vector4<Int64> operator+(Int64 lhs, const Vector4<Int64>& rhs)
	{
		const __m256i result = _mm256_add_epi64(_mm256_set1_epi64x(lhs), rhs._vxd);
		return Vector4<Int64>(reinterpret_cast<const Int64*>(&result));
	}

	template <>
	ExsForceInline Vector4<Int64> operator-(const Vector4<Int64>& lhs, const Vector4<Int64>& rhs)
	{
		const __m256i result = _mm256_sub_epi64(lhs._vxd, rhs._vxd);
		return Vector4<Int64>(reinterpret_cast<const Int64*>(&result));
	}

	template <>
	ExsForceInline Vector4<Int64> operator-(const Vector4<Int64>& lhs, Int64 rhs)
	{
		const __m256i result = _mm256_sub_epi64(lhs._vxd, _mm256_set1_epi64x(rhs));
		return Vector4<Int64>(reinterpret_cast<const Int64*>(&result));
	}

	template <>
	ExsForceInline Vector4<Int64> operator-(Int64 lhs, const Vector4<Int64>& rhs)
	{
		const __m256i result = _mm256_sub_epi64(_mm256_set1_epi64x(lhs), rhs._vxd);
		return Vector4<Int64>(reinterpret_cast<const Int64*>(&result));
	}

#endif


}
