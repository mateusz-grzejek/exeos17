
#pragma once

#ifndef __Exs_Math_MatrixUtils_H__
#define __Exs_Math_MatrixUtils_H__

#include "Matrix2.h"
#include "Matrix3.h"
#include "Matrix4.h"
#include "VectorUtils.h"


namespace Exs
{

	
	template <Precision P = Precision::Default>
	struct Matrix
	{
	public:
		typedef typename ScalarPrecisionType<P>::Type ScalarType;


		template <typename T>
		static T Determinant(const Matrix2x2<T>& mat2)
		{
			return mat2[0][0] * mat2[1][1] - mat2[0][1] * mat2[1][0];
		}


		template <typename T>
		static T Determinant(const Matrix3x3<T>& mat3)
		{
			const T rv1 = mat3[0][0] * mat3[1][1] * mat3[2][2];
			const T rv2 = mat3[0][1] * mat3[1][2] * mat3[2][0];
			const T rv3 = mat3[0][2] * mat3[1][0] * mat3[2][1];
			
			const T lv1 = mat3[0][2] * mat3[1][1] * mat3[2][0];
			const T lv2 = mat3[0][0] * mat3[1][2] * mat3[2][1];
			const T lv3 = mat3[0][1] * mat3[1][0] * mat3[2][2];

			return (rv1 + rv2 + rv3) - (lv1 + lv2 + lv3);
		}


		template <typename T>
		static T Determinant(const Matrix4x4<T>& mat4)
		{
			const T rv1 = mat4[0][0] * mat4[1][1] * mat4[2][2] * mat4[3][3];
			const T rv2 = mat4[0][1] * mat4[1][2] * mat4[2][3] * mat4[3][0];
			const T rv3 = mat4[0][2] * mat4[1][3] * mat4[2][0] * mat4[3][1];
			const T rv4 = mat4[0][3] * mat4[1][0] * mat4[2][1] * mat4[3][2];
			
			const T lv1 = mat4[0][3] * mat4[1][2] * mat4[2][1] * mat4[3][0];
			const T lv2 = mat4[0][0] * mat4[1][3] * mat4[2][2] * mat4[3][1];
			const T lv3 = mat4[0][1] * mat4[1][0] * mat4[2][3] * mat4[3][2];
			const T lv4 = mat4[0][2] * mat4[1][1] * mat4[2][0] * mat4[3][3];

			return ((rv1 + rv2) + (rv3 + rv4)) - ((lv1 + lv2) + (lv3 + lv4));
		}


		template <typename T>
		static Matrix2x2<T> Transpose(const Matrix2x2<T>& mat2)
		{
			return Matrix2x2<T>(
				mat2[0][0], mat2[1][0],
				mat2[0][1], mat2[1][1]);
		}


		template <typename T>
		static Matrix3x3<T> Transpose(const Matrix3x3<T>& mat3)
		{
			return Matrix3x3<T>(
				mat3[0][0], mat3[1][0], mat3[2][0],
				mat3[0][1], mat3[1][1], mat3[2][1],
				mat3[0][2], mat3[1][2], mat3[2][2]);
		}


		template <typename T>
		static Matrix4x4<T> Transpose(const Matrix4x4<T>& mat4)
		{
			return Matrix4x4<T>(
				mat4[0][0], mat4[1][0], mat4[2][0], mat4[3][0],
				mat4[0][1], mat4[1][1], mat4[2][1], mat4[3][1],
				mat4[0][2], mat4[1][2], mat4[2][2], mat4[3][2],
				mat4[0][3], mat4[1][3], mat4[2][3], mat4[3][3]);
		}


		template <typename T>
		static Matrix2x2<T> Inverse(const Matrix2x2<T>& mat2)
		{
		}


		template <typename T>
		static Matrix3x3<T> Inverse(const Matrix3x3<T>& mat3)
		{
		}


		template <typename T>
		static Matrix4x4<T> Inverse(const Matrix4x4<T>& mat4)
		{
			const T coef_00 = mat4[2][2] * mat4[3][3] - mat4[3][2] * mat4[2][3];
			const T coef_02 = mat4[1][2] * mat4[3][3] - mat4[3][2] * mat4[1][3];
			const T coef_03 = mat4[1][2] * mat4[2][3] - mat4[2][2] * mat4[1][3];
			const T coef_04 = mat4[2][1] * mat4[3][3] - mat4[3][1] * mat4[2][3];
			const T coef_06 = mat4[1][1] * mat4[3][3] - mat4[3][1] * mat4[1][3];
			const T coef_07 = mat4[1][1] * mat4[2][3] - mat4[2][1] * mat4[1][3];
			const T coef_08 = mat4[2][1] * mat4[3][2] - mat4[3][1] * mat4[2][2];
			const T coef_10 = mat4[1][1] * mat4[3][2] - mat4[3][1] * mat4[1][2];
			const T coef_11 = mat4[1][1] * mat4[2][2] - mat4[2][1] * mat4[1][2];
			const T coef_12 = mat4[2][0] * mat4[3][3] - mat4[3][0] * mat4[2][3];
			const T coef_14 = mat4[1][0] * mat4[3][3] - mat4[3][0] * mat4[1][3];
			const T coef_15 = mat4[1][0] * mat4[2][3] - mat4[2][0] * mat4[1][3];
			const T coef_16 = mat4[2][0] * mat4[3][2] - mat4[3][0] * mat4[2][2];
			const T coef_18 = mat4[1][0] * mat4[3][2] - mat4[3][0] * mat4[1][2];
			const T coef_19 = mat4[1][0] * mat4[2][2] - mat4[2][0] * mat4[1][2];
			const T coef_20 = mat4[2][0] * mat4[3][1] - mat4[3][0] * mat4[2][1];
			const T coef_22 = mat4[1][0] * mat4[3][1] - mat4[3][0] * mat4[1][1];
			const T coef_23 = mat4[1][0] * mat4[2][1] - mat4[2][0] * mat4[1][1];

			Vector4<T> fc_0(coef_00, coef_00, coef_02, coef_03);
			Vector4<T> fc_1(coef_04, coef_04, coef_06, coef_07);
			Vector4<T> fc_2(coef_08, coef_08, coef_10, coef_11);
			Vector4<T> fc_3(coef_12, coef_12, coef_14, coef_15);
			Vector4<T> fc_4(coef_16, coef_16, coef_18, coef_19);
			Vector4<T> fc_5(coef_20, coef_20, coef_22, coef_23);

			Vector4<T> vec_0(mat4[1][0], mat4[0][0], mat4[0][0], mat4[0][0]);
			Vector4<T> vec_1(mat4[1][1], mat4[0][1], mat4[0][1], mat4[0][1]);
			Vector4<T> vec_2(mat4[1][2], mat4[0][2], mat4[0][2], mat4[0][2]);
			Vector4<T> vec_3(mat4[1][3], mat4[0][3], mat4[0][3], mat4[0][3]);

			Vector4<T> inv_0(vec_1 * fc_0 - vec_2 * fc_1 + vec_3 * fc_2);
			Vector4<T> inv_1(vec_0 * fc_0 - vec_2 * fc_3 + vec_3 * fc_4);
			Vector4<T> inv_2(vec_0 * fc_1 - vec_1 * fc_3 + vec_3 * fc_5);
			Vector4<T> inv_3(vec_0 * fc_2 - vec_1 * fc_4 + vec_2 * fc_5);

			Vector4<T> sgn_0(static_cast<T>(1), -1, 1, -1);
			Vector4<T> sgn_1(static_cast<T>(-1), 1, -1, 1);
			
			Matrix4x4<T> inv(inv_0 * sgn_0, inv_1 * sgn_1, inv_2 * sgn_0, inv_3 * sgn_1);
			Vector4<T> dotSrc(mat4[0] * Vector4<T>(inv[0][0], inv[1][0], inv[2][0], inv[3][0]));

			const T dot = (dotSrc.x + dotSrc.y) + (dotSrc.z + dotSrc.w);
			const T one_over_det = static_cast<T>(1) / dot;

			return inv * one_over_det;
		}


		static Matrix4x4<ScalarType> RotationX(ScalarType angle)
		{
			ScalarType angle_sin_value = sin(angle);
			ScalarType angle_cos_value = cos(angle);

			return Matrix4x4<ScalarType>(
				1, 0, 0, 0,
				0, angle_cos_value, -angle_sin_value, 0,
				0, angle_sin_value, angle_cos_value, 0,
				0, 0, 0, 1);
		}


		static Matrix4x4<ScalarType> RotationY(ScalarType angle)
		{
			ScalarType angle_sin_value = sin(angle);
			ScalarType angle_cos_value = cos(angle);

			return Matrix4x4<ScalarType>(
				angle_cos_value, 0, angle_sin_value, 0,
				0, 1, 0, 0,
				-angle_sin_value, 0, angle_cos_value, 0,
				0, 0, 0, 1);
		}


		static Matrix4x4<ScalarType> RotationZ(ScalarType angle)
		{
			ScalarType angle_sin_value = sin(angle);
			ScalarType angle_cos_value = cos(angle);

			return Matrix4x4<ScalarType>(
				angle_cos_value, -angle_sin_value, 0, 0,
				angle_sin_value, angle_cos_value, 0, 0,
				0, 0, 1, 0,
				0, 0, 0, 1);
		}


		template <typename T>
		static Matrix4x4<ScalarType> Rotation(T axisX, T axisY, T axisZ, ScalarType angle)
		{
			const Vector4<ScalarType> axis(Vector<P>::Normalize(Vector3<ScalarType>(axisX, axisY, axisZ)), 1);

			const ScalarType angle_cos = cos(angle);
			const ScalarType angle_sin = sin(angle);

			const ScalarType one_minus_cos = static_cast<ScalarType>(1) - angle_cos;
			
			const Vector4<ScalarType> axmc(axis * one_minus_cos);
			const Vector4<ScalarType> axms(axis * angle_sin);
			
			const Vector4<ScalarType> col_0(axmc.x * axis + Vector4<ScalarType>(angle_cos, axms.z, -axms.y, 0));
			const Vector4<ScalarType> col_1(axmc.y * axis + Vector4<ScalarType>(-axms.z, angle_cos, axms.x, 0));
			const Vector4<ScalarType> col_2(axmc.z * axis + Vector4<ScalarType>(axms.y, -axms.x, angle_cos, 0));

			return Matrix4x4<ScalarType>(
				col_0,
				col_1,
				col_2,
				Vector4<ScalarType>(0, 0, 0, 1));
		}


		template <typename T>
		static Matrix4x4<ScalarType> Rotation(const Vector3<T>& axis, ScalarType angle)
		{
			return Rotation(axis.x, axis.y, axis.z, angle);
		}

		
		template <typename T>
		static Matrix4x4<ScalarType> Scaling(T scaleX, T scaleY, T scaleZ)
		{
			return Matrix4x4<ScalarType>(
				scaleX, 0, 0, 0,
				0, scaleY, 0, 0,
				0, 0, scaleZ, 0,
				0, 0, 0, static_cast<ScalarType>(1));
		}

		
		template <typename T>
		static Matrix4x4<ScalarType> Scaling(const Vector3<T>& scaleVec)
		{
			return Scaling(scaleVec.x, scaleVec.y, scaleVec.z);
		}

		
		template <typename T>
		static Matrix4x4<ScalarType> Translation(T translateX, T translateY, T translateZ)
		{
			return Matrix4x4<ScalarType>(
				1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 1, 0,
				translateX, translateY, translateZ, 1);
		}

		
		template <typename T>
		static Matrix4x4<ScalarType> Translation(const Vector3<T>& translateVec)
		{
			return Translation(translateVec.x, translateVec.y, translateVec.z);
		}


		static Matrix4x4<ScalarType> RotateX(const Matrix4x4<ScalarType>& mat, ScalarType angle)
		{
			return mat * RotationX(angle);
		}


		static Matrix4x4<ScalarType> RotateY(const Matrix4x4<ScalarType>& mat, ScalarType angle)
		{
			return mat * RotationY(angle);
		}


		static Matrix4x4<ScalarType> RotateZ(const Matrix4x4<ScalarType>& mat, ScalarType angle)
		{
			return mat * RotationZ(angle);
		}


		template <typename T>
		static Matrix4x4<ScalarType> Rotate(const Matrix4x4<ScalarType>& mat, T axisX, T axisY, T axisZ, ScalarType angle)
		{
			return Rotation(axisX, axisY, axisZ, angle) * mat;
		}


		template <typename T>
		static Matrix4x4<ScalarType> Rotate(const Matrix4x4<ScalarType>& mat, const Vector3<T>& axis, ScalarType angle)
		{
			return Rotation(axis.x, axis.y, axis.z, angle) * mat;
		}

		
		template <typename T>
		static Matrix4x4<ScalarType> Scale(const Matrix4x4<ScalarType>& mat, T scaleX, T scaleY, T scaleZ)
		{
			return mat * Scaling(scaleX, scaleY, scaleZ);
		}

		
		template <typename T>
		static Matrix4x4<ScalarType> Scale(const Matrix4x4<ScalarType>& mat, const Vector3<T>& scaleVec)
		{
			return  mat * Scaling(scaleVec.x, scaleVec.y, scaleVec.z);
		}

		
		template <typename T>
		static Matrix4x4<ScalarType> Translate(const Matrix4x4<ScalarType>& mat, T translateX, T translateY, T translateZ)
		{
			return mat * Translation(translateX, translateY, translateZ);
		}

		
		template <typename T>
		static Matrix4x4<ScalarType> Translate(const Matrix4x4<ScalarType>& mat, const Vector3<T>& translateVec)
		{
			return mat * Translation(translateVec.x, translateVec.y, translateVec.z);
		}

		
		template <typename T>
		static Matrix4x4<ScalarType> Ortho(T left, T right, T bottom, T top)
		{
			const ScalarType m00 = static_cast<ScalarType>(2) / (right - left);
			const ScalarType m11 = static_cast<ScalarType>(2) / (top - bottom);
			const ScalarType m22 = -static_cast<ScalarType>(1);
			const ScalarType m30 = -((left + right) / (right - left));
			const ScalarType m31 = -((top + bottom) / (top - bottom));

			return Matrix4x4<ScalarType>(
				m00, 0, 0, 0,
				0, m11, 0, 0,
				0, 0, m22, 0,
				m30, m31, 0, 1);
		}

		
		template <typename T>
		static Matrix4x4<ScalarType> Ortho(T left, T right, T bottom, T top, T nearZ, T farZ)
		{
			const ScalarType m00 = static_cast<ScalarType>(2) / (right - left);
			const ScalarType m11 = static_cast<ScalarType>(2) / (top - bottom);
			const ScalarType m22 = -(static_cast<ScalarType>(2) / (farZ - nearZ));
			const ScalarType m30 = -((right + left) / (right - left));
			const ScalarType m31 = -((top + bottom) / (top - bottom));
			const ScalarType m32 = -((farZ + nearZ) / (farZ - nearZ));

			return Matrix4x4<ScalarType>(
				m00, 0, 0, 0,
				0, m11, 0, 0,
				0, 0, m22, 0,
				m30, m31, m32, 1);
		}

		
		template <typename T>
		static Matrix4x4<ScalarType> Frustum(T left, T right, T bottom, T top, T nearZ, T farZ)
		{
			const ScalarType m00 = (static_cast<T>(2) * nearZ) / (right - left);
			const ScalarType m11 = (static_cast<T>(2) * nearZ) / (top - bottom);
			const ScalarType m20 = (right + left) / (right - left);
			const ScalarType m21 = (top + bottom) / (top - bottom);
			const ScalarType m22 = -((farZ + nearZ) / (farZ - nearZ));
			const ScalarType m23 = -static_cast<T>(1);
			const ScalarType m32 = -((static_cast<T>(2) * nearZ * farZ) / (farZ - nearZ));

			return Matrix4x4<ScalarType>(
				m00, 0, 0, 0,
				0, m11, 0, 0,
				m20, m21, m22, m23,
				0, 0, m32, 1);
		}

		
		template <typename T>
		static Matrix4x4<ScalarType> Perspective(T fovy, T aspectRatio, T nearZ, T farZ)
		{
			const ScalarType tan_fovy_2 = tan(fovy / static_cast<T>(2));
			const ScalarType z_range = farZ - nearZ;
			
			const ScalarType m00 = static_cast<T>(1) / (aspectRatio * tan_fovy_2);
			const ScalarType m11 = static_cast<T>(1) / tan_fovy_2;
			const ScalarType m22 = -((farZ + nearZ) / (farZ - nearZ));
			const ScalarType m23 = -static_cast<T>(1);
			const ScalarType m32 = -((static_cast<T>(2) * farZ * nearZ) / (farZ - nearZ));

			return Matrix4x4<ScalarType>(
				m00, 0, 0, 0,
				0, m11, 0, 0,
				0, 0, m22, m23,
				0, 0, m32, 1);
		}

		
		template <typename T>
		static Matrix4x4<ScalarType> LookAt(const Vector3<T>& eyePosition,
											const Vector3<T>& focusPoint,
											const Vector3<T>& upDirection)
		{
			const Vector3<T> view_dir = Vector<P>::Normalize(focusPoint - eyePosition);
			const Vector3<T> snorm = Vector<P>::Normalize(Vector<P>::Cross(view_dir, upDirection));
			const Vector3<T> ucvec = Vector<P>::Cross(snorm, view_dir);
			
			const ScalarType sn_eye_dot_neg = -Vector<P>::Dot(snorm, eyePosition);
			const ScalarType uc_eye_dot_neg = -Vector<P>::Dot(snorm, eyePosition);
			const ScalarType vd_eye_dot = Vector<P>::Dot(view_dir, eyePosition);

			return Matrix4x4<ScalarType>(
				snorm.x, ucvec.x, -view_dir.x, static_cast<ScalarType>(0),
				snorm.y, ucvec.y, -view_dir.y, static_cast<ScalarType>(0),
				snorm.z, ucvec.z, -view_dir.z, static_cast<ScalarType>(0),
				sn_eye_dot_neg, uc_eye_dot_neg, vd_eye_dot, static_cast<ScalarType>(1));
		}
	};


}


#if ( EXS_MATH_USE_SIMD_INTRINSICS )
#  include "MatrixUtilsSIMD.inl"
#endif


#endif /* __Exs_Math_MatrixUtils_H__ */
