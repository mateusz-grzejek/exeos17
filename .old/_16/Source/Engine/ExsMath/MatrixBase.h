
#pragma once

#ifndef __Exs_Math_MatrixBase_H__
#define __Exs_Math_MatrixBase_H__

#include "VectorBase.h"


namespace Exs
{


	template <typename T, Size_t M, Size_t N>
	struct MatrixBase
	{
	public:
		static const Size_t rowsNum = M;
		static const Size_t columnsNum = N;
		static const Size_t length = M * N;
		static const Size_t sizeInBytes = sizeof(T) * length;

		struct Uninitialized
		{
		};
	};


}


#endif /* __Exs_Math_MatrixBase_H__ */
