
#pragma once

#ifndef __Exs_Math_Vector4_H__
#define __Exs_Math_Vector4_H__

#include "Vector2.h"
#include "Vector3.h"


namespace Exs
{


	template <typename T>
	struct Vector4 : public VectorBase<T, 4>
	{
	public:
	#if ( EXS_MATH_USE_SIMD_INTRINSICS )
		union
		{
			struct
			{
				T x, y, z, w;
			};

			typename Internal::VectorSIMD<T, 4>::DataType _vxd;
		};
	#else
		T x, y, z, w;
	#endif

	public:
		Vector4()
		: x(static_cast<T>(0))
		, y(static_cast<T>(0))
		, z(static_cast<T>(0))
		, w(static_cast<T>(0))
		{ }

		Vector4(const Vector4<T>& origin)
		: x(origin.x)
		, y(origin.y)
		, z(origin.z)
		, w(origin.w)
		{ }

		template <typename U>
		Vector4(const Vector4<U>& vec4)
		: x(static_cast<T>(vec4.x))
		, y(static_cast<T>(vec4.y))
		, z(static_cast<T>(vec4.z))
		, w(static_cast<T>(vec4.w))
		{ }

		template <typename U, typename Tw>
		Vector4(const Vector3<U>& xyz, Tw w)
		: x(static_cast<T>(xyz.x))
		, y(static_cast<T>(xyz.y))
		, z(static_cast<T>(xyz.z))
		, w(static_cast<T>(w))
		{ }

		template <typename Tx, typename U>
		Vector4(Tx x, const Vector3<U>& yzw)
		: x(static_cast<T>(x))
		, y(static_cast<T>(yzw.x))
		, z(static_cast<T>(yzw.y))
		, w(static_cast<T>(yzw.z))
		{ }

		template <typename U, typename Tz, typename Tw>
		Vector4(const Vector2<U>& xy, Tz z, Tw w)
		: x(static_cast<T>(xy.x))
		, y(static_cast<T>(xy.y))
		, z(static_cast<T>(z))
		, w(static_cast<T>(w))
		{ }

		template <typename Tx, typename U, typename Tw>
		Vector4(Tx x, const Vector2<U>& yz, Tw w)
		: x(static_cast<T>(x))
		, y(static_cast<T>(yz.x))
		, z(static_cast<T>(yz.y))
		, w(static_cast<T>(w))
		{ }

		template <typename Tx, typename Ty, typename U>
		Vector4(Tx x, Ty y, const Vector2<U>& zw)
		: x(static_cast<T>(x))
		, y(static_cast<T>(y))
		, z(static_cast<T>(zw.x))
		, w(static_cast<T>(zw.y))
		{ }

		template <typename U, typename V>
		Vector4(const Vector2<U>& xy, const Vector2<V>& zw)
		: x(static_cast<T>(xy.x))
		, y(static_cast<T>(xy.y))
		, z(static_cast<T>(zw.x))
		, w(static_cast<T>(zw.y))
		{ }

		template <typename Tx, typename Ty, typename Tz, typename Tw>
		Vector4(Tx x, Ty y, Tz z, Tw w)
		: x(static_cast<T>(x))
		, y(static_cast<T>(y))
		, z(static_cast<T>(z))
		, w(static_cast<T>(w))
		{ }

		template <typename U>
		explicit Vector4(U scalar)
		: x(scalar)
		, y(scalar)
		, z(scalar)
		, w(scalar)
		{ }

		explicit Vector4(const T* data)
		: x(data[0])
		, y(data[1])
		, z(data[2])
		, w(data[3])
		{ }

		explicit Vector4(const UninitializedTag&)
		{ }

		Vector4<T>& operator=(const Vector4<T>& rhs)
		{
			this->x = rhs.x;
			this->y = rhs.y;
			this->z = rhs.z;
			this->w = rhs.w;

			return *this;
		}

		template <typename U>
		Vector4<T>& operator=(const Vector4<U>& rhs)
		{
			this->x = static_cast<T>(rhs.x);
			this->y = static_cast<T>(rhs.y);
			this->z = static_cast<T>(rhs.z);
			this->w = static_cast<T>(rhs.w);

			return *this;
		}

		template <typename U>
		Vector4<T>& operator=(U rhs)
		{
			this->x = static_cast<T>(rhs);
			this->y = static_cast<T>(rhs);
			this->z = static_cast<T>(rhs);
			this->w = static_cast<T>(rhs);

			return *this;
		}

		operator Vector3<T>() const
		{
			return Vector3<T>(this->x, this->y, this->z);
		}

		T* Data()
		{
			return &(this->x);
		}

		const T* Data() const
		{
			return &(this->x);
		}

		T& operator[](Size_t index)
		{
			ExsDebugAssert( index < 4 );
			return (&(this->x))[index];
		}

		const T& operator[](Size_t index) const
		{
			ExsDebugAssert( index < 4 );
			return (&(this->x))[index];
		}

		Vector4<T>& operator++()
		{
			++this->x;
			++this->y;
			++this->z;
			++this->w;

			return *this;
		}

		Vector4<T> operator++(int)
		{
			Vector4<T> result(*this);
			++(*this);

			return result;
		}

		Vector4<T>& operator--()
		{
			--this->x;
			--this->y;
			--this->z;
			--this->w;

			return *this;
		}

		Vector4<T> operator--(int)
		{
			Vector4<T> result(*this);
			--(*this);

			return result;
		}

		template <typename U>
		Vector4<T>& operator+=(const Vector4<U>& rhs)
		{
			this->x += static_cast<T>(rhs.x);
			this->y += static_cast<T>(rhs.y);
			this->z += static_cast<T>(rhs.z);
			this->w += static_cast<T>(rhs.w);

			return *this;
		}

		template <typename U>
		Vector4<T>& operator+=(U rhs)
		{
			this->x += static_cast<T>(rhs);
			this->y += static_cast<T>(rhs);
			this->z += static_cast<T>(rhs);
			this->w += static_cast<T>(rhs);

			return *this;
		}

		template <typename U>
		Vector4<T>& operator-=(const Vector4<U>& rhs)
		{
			this->x -= static_cast<T>(rhs.x);
			this->y -= static_cast<T>(rhs.y);
			this->z -= static_cast<T>(rhs.z);
			this->w -= static_cast<T>(rhs.w);

			return *this;
		}

		template <typename U>
		Vector4<T>& operator-=(U rhs)
		{
			this->x -= static_cast<T>(rhs);
			this->y -= static_cast<T>(rhs);
			this->z -= static_cast<T>(rhs);
			this->w -= static_cast<T>(rhs);

			return *this;
		}

		template <typename U>
		Vector4<T>& operator*=(const Vector4<U>& rhs)
		{
			this->x *= static_cast<T>(rhs.x);
			this->y *= static_cast<T>(rhs.y);
			this->z *= static_cast<T>(rhs.z);
			this->w *= static_cast<T>(rhs.w);

			return *this;
		}

		template <typename U>
		Vector4<T>& operator*=(U rhs)
		{
			this->x *= static_cast<T>(rhs);
			this->y *= static_cast<T>(rhs);
			this->z *= static_cast<T>(rhs);
			this->w *= static_cast<T>(rhs);

			return *this;
		}

		template <typename U>
		Vector4<T>& operator/=(const Vector4<U>& rhs)
		{
			this->x /= static_cast<T>(rhs.x);
			this->y /= static_cast<T>(rhs.y);
			this->z /= static_cast<T>(rhs.z);
			this->w /= static_cast<T>(rhs.w);

			return *this;
		}

		template <typename U>
		Vector4<T>& operator/=(U rhs)
		{
			this->x /= static_cast<T>(rhs);
			this->y /= static_cast<T>(rhs);
			this->z /= static_cast<T>(rhs);
			this->w /= static_cast<T>(rhs);

			return *this;
		}
	};

	
	template <typename T>
	inline Vector4<T> operator+(const Vector4<T>& lhs, const Vector4<T>& rhs)
	{
		return Vector4<T>(
			lhs.x + rhs.x,
			lhs.y + rhs.y,
			lhs.z + rhs.z,
			lhs.w + rhs.w);
	}

	template <typename T>
	inline Vector4<T> operator+(const Vector4<T>& lhs, T rhs)
	{
		return Vector4<T>(
			lhs.x + rhs,
			lhs.y + rhs,
			lhs.z + rhs,
			lhs.w + rhs);
	}

	template <typename T>
	inline Vector4<T> operator+(T lhs, const Vector4<T>& rhs)
	{
		return Vector4<T>(
			lhs + rhs.x,
			lhs + rhs.y,
			lhs + rhs.z,
			lhs + rhs.w);
	}

	template <typename T>
	inline Vector4<T> operator-(const Vector4<T>& lhs, const Vector4<T>& rhs)
	{
		return Vector4<T>(
			lhs.x - rhs.x,
			lhs.y - rhs.y,
			lhs.z - rhs.z,
			lhs.w - rhs.w);
	}

	template <typename T>
	inline Vector4<T> operator-(const Vector4<T>& lhs, T rhs)
	{
		return Vector4<T>(
			lhs.x - rhs,
			lhs.y - rhs,
			lhs.z - rhs,
			lhs.w - rhs);
	}

	template <typename T>
	inline Vector4<T> operator-(T lhs, const Vector4<T>& rhs)
	{
		return Vector4<T>(
			lhs - rhs.x,
			lhs - rhs.y,
			lhs - rhs.z,
			lhs - rhs.w);
	}

	template <typename T>
	inline Vector4<T> operator*(const Vector4<T>& lhs, const Vector4<T>& rhs)
	{
		return Vector4<T>(
			lhs.x * rhs.x,
			lhs.y * rhs.y,
			lhs.z * rhs.z,
			lhs.w * rhs.w);
	}

	template <typename T>
	inline Vector4<T> operator*(const Vector4<T>& lhs, T rhs)
	{
		return Vector4<T>(
			lhs.x * rhs,
			lhs.y * rhs,
			lhs.z * rhs,
			lhs.w * rhs);
	}

	template <typename T>
	inline Vector4<T> operator*(T lhs, const Vector4<T>& rhs)
	{
		return Vector4<T>(
			lhs * rhs.x,
			lhs * rhs.y,
			lhs * rhs.z,
			lhs * rhs.w);
	}

	template <typename T>
	inline Vector4<T> operator/(const Vector4<T>& lhs, const Vector4<T>& rhs)
	{
		return Vector4<T>(
			lhs.x / rhs.x,
			lhs.y / rhs.y,
			lhs.z / rhs.z,
			lhs.w / rhs.w);
	}

	template <typename T>
	inline Vector4<T> operator/(const Vector4<T>& lhs, T rhs)
	{
		return Vector4<T>(
			lhs.x / rhs,
			lhs.y / rhs,
			lhs.z / rhs,
			lhs.w / rhs);
	}

	template <typename T>
	inline Vector4<T> operator/(T lhs, const Vector4<T>& rhs)
	{
		return Vector4<T>(
			lhs.x / rhs,
			lhs.y / rhs,
			lhs.z / rhs,
			lhs.w / rhs);
	}

	template <typename T, typename U>
	inline Vector4<T> operator-(const Vector4<T>& rhs)
	{
		return Vector4<T>(-rhs.x, -rhs.y, -rhs.z, -rhs.w);
	}

	
	template <typename T>
	inline bool operator==(const Vector4<T>& lhs, const Vector4<T>& rhs)
	{
		return (lhs.x == rhs.x) && (lhs.y == rhs.y) && (lhs.z == rhs.z) && (lhs.w == rhs.w);
	}

	template <typename T>
	inline bool operator!=(const Vector4<T>& lhs, const Vector4<T>& rhs)
	{
		return (lhs.x != rhs.x) || (lhs.y != rhs.y) || (lhs.z != rhs.z) || (lhs.w != rhs.w);
	}


	typedef Vector4<float>     Vector4F;
	typedef Vector4<double>    Vector4D;
	typedef Vector4<Int32>     Vector4I32;
	typedef Vector4<Uint32>    Vector4U32;
	typedef Vector4<Int64>     Vector4I64;
	typedef Vector4<Uint64>    Vector4U64;


}


#if ( EXS_MATH_USE_SIMD_INTRINSICS )
#  include "Vector4SIMD.inl"
#endif


#endif /* __Exs_Math_Vector4_H__ */
