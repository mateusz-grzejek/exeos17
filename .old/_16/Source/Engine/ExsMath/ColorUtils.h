
#pragma once

#ifndef __Exs_Math_ColorUtils_H__
#define __Exs_Math_ColorUtils_H__

#include "Color.h"
#include "Vector4.h"


namespace Exs
{


	inline Vector3F GetColorVectorRGB(const Color& color)
	{
		float r = static_cast<float>(color.rgba.red) / 255.0f;
		float g = static_cast<float>(color.rgba.green) / 255.0f;
		float b = static_cast<float>(color.rgba.blue) / 255.0f;
		return Vector3F(r, g, b);
	}

	inline Vector4F GetColorVectorRGBA(const Color& color)
	{
		float r = static_cast<float>(color.rgba.red) / 255.0f;
		float g = static_cast<float>(color.rgba.green) / 255.0f;
		float b = static_cast<float>(color.rgba.blue) / 255.0f;
		float a = static_cast<float>(color.rgba.alpha) / 255.0f;
		return Vector4F(r, g, b, a);
	}


}


#endif /* __Exs_Math_ColorUtils_H__ */
