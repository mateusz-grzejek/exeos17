
#pragma once

#ifndef __Exs_UI_UIWidget_H__
#define __Exs_UI_UIWidget_H__

#include "UIComponent.h"


namespace Exs
{


	class EXS_LIBCLASS_UI UIWidget : public UIComponent
	{
	protected:
		Vector2F  _baseSize;

	public:
		template <typename Pv_t, typename Sv_t>
		UIWidget(UILayer* layer, UIWidget* parent, const Vector2<Pv_t>& position, const Vector2<Sv_t>& size, const Color& color = ColorU32::White);

		template <typename Pv_t, typename Sv_t>
		UIWidget(UILayer* layer, UIWidget* parent, UIAnchorPoint anchorPoint, const Vector2<Pv_t>& position, const Vector2<Sv_t>& size, const Color& color, stdx::mask<Enum> initialState);
		
		template <class Widget, class... Args>
		Handle<Widget> AddWidget(Args&&... args);

		template <class Widget, class... Args>
		Handle<Widget> AddOverlay(Args&&... args);

		template <typename Sv_t>
		void UpdateBaseSize(const Vector2<Sv_t>& size)
		{
			this->_baseSize = size;
			this->_state.set(UIWidgetState_Base_Property_Changed);
		}

		void Draw();

	protected:
		virtual bool OnRenderBegin();
		virtual void OnRenderEnd();

	private:
		virtual void Render() = 0;
	};


	template <typename Pv_t, typename Sv_t>
	inline UIWidget::UIWidget(UILayer* layer, UIWidget* parent, const Vector2<Pv_t>& position, const Vector2<Sv_t>& size, const Color& color)
	: UIComponent(layer, parent, position, size, color)
	{ }
	
	template <typename Pv_t, typename Sv_t>
	inline UIWidget::UIWidget(UILayer* layer, UIWidget* parent, UIAnchorPoint anchorPoint, const Vector2<Pv_t>& position, const Vector2<Sv_t>& size, const Color& color, stdx::mask<UIWidgetStateFlags> initialState)
	: UIComponent(layer, parent, anchorPoint, position, size, color, initialState)
	{ }

	template <class Widget, class... Args>
	inline Handle<Widget> UIWidget::AddWidget(Args&&... args)
	{
		return CreateAutoRefObject<Widget>(this->_layer, this, std::forward<Args>(args)...);
	}
	
	template <class Widget, class... Args>
	inline Handle<Widget> UIWidget::AddOverlay(Args&&... args)
	{
		UILayer* nextLayer = this->_layer->NextLayer();
		return CreateAutoRefObject<Widget>(nextLayer, this, std::forward<Args>(args)...);
	}


}


#endif /* __Exs_UI_UIWidget_H__ */
