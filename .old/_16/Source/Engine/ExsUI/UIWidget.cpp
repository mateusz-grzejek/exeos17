
#include "_Precompiled.h"
#include <ExsUI/UIWidget.h>


namespace Exs
{


	void UIWidget::Draw()
	{
		if (!this->OnRenderBegin())
			return;

		this->Render();
		this->OnRenderEnd();
	}


	bool UIWidget::OnRenderBegin()
	{
		return true;
	}


	void UIWidget::OnRenderEnd()
	{
		this->_state.unset(UIWidgetState_Base_Properties_Changed);
	}


}
