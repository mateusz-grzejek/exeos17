
#include "_Precompiled.h"
#include "TextAligner.h"
#include <ExsResManager/Resources/Font.h>


namespace Exs
{


	TextAligner::TextAligner(Font& font, const TextAlignmentSettings& settings)
	: _font(&font)
	, _fontLineHeight(font.GetLineHeight())
	, _cmSpaceMetrics(font.GetGlyphMetrics(' '))
	, _settings(settings)
	, _currentLineIndex(0)
	, _currentLine(nullptr)
	, _charPosXRange(settings.margin.x, settings.margin.x + settings.lineWidth)
	, _currentOffset(0, 0)
	{ }


	void TextAligner::Align(const std::string& text)
	{
		 if (this->_currentLine != nullptr)
		 {
			 this->Reset();
		 }

		 this->Append(text);
	}


	void TextAligner::Append(const std::string& text)
	{
		if (this->_settings.lineWidth == 0)
		{
			// If width of a single line was set to 0, simply do not align
			// anything and silently return. (TODO: some warning here, perhaps?).

			return;
		}
	}


	void TextAligner::Reset()
	{
		this->_currentLine = nullptr;
		this->_currentLineIndex = 0;
		this->_currentOffset.x = 0;
		this->_currentOffset.y = 0;
		this->_alignedText.lines.clear();
	}


	void TextAligner::SetFont(Font& font)
	{
		if (this->_currentLine != nullptr)
		{
			this->Reset();
		}

		this->_font = &font;
		this->_fontLineHeight = font.GetLineHeight();
		this->_cmSpaceMetrics = font.GetGlyphMetrics(' ');
	}


	void TextAligner::UpdateSettings(const TextAlignmentSettings& settings)
	{
		if (this->_currentLine != nullptr)
		{
			this->Reset();
		}

		this->_settings = settings;
		this->_charPosXRange.min = settings.margin.x;
		this->_charPosXRange.max = settings.margin.x + settings.lineWidth;
	}


	void TextAligner::_ReserveLineSpace(Uint64 width)
	{
		if (!this->_CheckLineSpace(width))
		{
			this->_NextLine();
		}
	}


	void TextAligner::_NextLine()
	{
		if (this->_currentLineIndex == this->_alignedText.lines.size())
		{
			// Add new line
			this->_alignedText.lines.emplace_back();
		}
	
		auto* nextLine = &(this->_alignedText.lines[this->_currentLineIndex]);
		nextLine->chars.clear();
		nextLine->width = 0;
		nextLine->extraCharSpace = 0;

		this->_currentLine->width = this->_currentOffset.x;
	
		this->_currentLine = nextLine;
		this->_currentOffset.x = this->_charPosXRange.min;
		this->_currentOffset.y += this->_fontLineHeight;

		ExsDebugAssert( !this->_alignedText.lines.empty() );
		this->_currentLineIndex = this->_alignedText.lines.size() - 1;
	}


	bool TextAligner::_Append(const std::string& text)
	{
		const Size_t textLength = text.length();
		std::vector<TextChar> charVector;

		for (Size_t tpos = 0; tpos < textLength; )
		{
			if (this->_ProcessSpecialChar(text[tpos]))
			{
				tpos += 1;
				continue;
			}

			//
			auto txtinfo = this->_ProcessSubtext(text, tpos, &charVector);

			if (txtinfo.second > 0)
			{
				if (!this->_CheckLineSpace(txtinfo.first))
				{
					this->_NextLine();
					this->_ReparseAsNextLine(charVector);
				}

				for (auto& ch : charVector)
				{
					this->_currentLine->chars.push_back(ch);
				}

				this->_currentLine->width += txtinfo.first;
			}
		}
	}


	void TextAligner::_ProcessSpace(Size_t count)
	{
		if ((count > 0) && (this->_cmSpaceMetrics != nullptr))
		{
			Int32 spaceSize = (this->_cmSpaceMetrics->advance.x - this->_cmSpaceMetrics->bearing.x);
			spaceSize = stdx::get_max_of(spaceSize, 0);

			Uint64 lineFreeSpace = this->_GetLineFreeSpace();
			Uint64 spacesNumFreeSpace = lineFreeSpace / spaceSize;

			count = stdx::get_min_of(spacesNumFreeSpace, truncate_cast<Size_t>(count));
			Uint64 spaceAdvSize = count * spaceSize;

			this->_currentOffset.x += spaceAdvSize;
		}
	}


	bool TextAligner::_ProcessSpecialChar(char ch)
	{
		if (ch == ' ')
		{
			this->_ProcessSpace(1);
			return true;
		}

		if (ch == '\n')
		{
			if (this->_settings.enableMultiline)
			{
				this->_NextLine();
			}
			else
			{
				this->_ProcessSpace(1);
			}

			return true;
		}

		if (ch == '\t')
		{
			this->_ProcessSpace(this->_settings.lineWidth);
			return true;
		}

		if ((ch == '\a') || (ch == '\r'))
		{
			return true;
		}

		return false;
	}


	std::pair<Size_t, Uint64> TextAligner::_ProcessSubtext(const std::string& text, Size_t start, std::vector<TextChar>* charData)
	{
		const Size_t nextSep = text.find_first_of(" \t\n\r", start + 1);
		const Size_t wordEnd = (nextSep != std::string::npos) ? nextSep : text.length();

		std::pair<Size_t, Uint64> result;
		result.first = wordEnd - start;
		result.second = 0;

		charData->clear();

		Int64 xbase = this->_currentOffset.x;
		Int64 ybase = this->_currentOffset.y;
		CodePoint prevChar = 0;

		for (Size_t i = start; i < wordEnd; ++i)
		{
			CodePoint charcp = static_cast<CodePoint>(text[i]);

			if (const auto* gmetrics = this->_font->GetGlyphMetrics(charcp))
			{
				Int32 kerning = this->_font->GetKerning(prevChar, charcp);
				Int64 ybaseDiff = gmetrics->size.y - gmetrics->bearing.y;

				TextChar tchar;
				tchar.codePoint = charcp;
				tchar.rect.position.x = xbase + kerning;
				tchar.rect.position.y = ybase - ybaseDiff;
				tchar.rect.size.x = gmetrics->size.x;
				tchar.rect.size.y = gmetrics->size.y;
				charData->push_back(tchar);

				xbase += (gmetrics->advance.x + kerning - gmetrics->bearing.x);
			}

			prevChar = charcp;
		}

		return result;
	}


	void TextAligner::_ReparseAsNextLine(std::vector<TextChar>& charData)
	{
		auto& firstChar = charData.front();
		auto xdiff = firstChar.rect.position.x - this->_charPosXRange.min;

		for (auto& ch : charData)
		{
			ch.rect.position.x -= xdiff;
			ch.rect.position.y += this->_fontLineHeight;
		}
	}


}
