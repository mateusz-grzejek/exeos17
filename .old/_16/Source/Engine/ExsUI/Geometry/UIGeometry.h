
#pragma once

#ifndef __Exs_UI_UIGeometry_H__
#define __Exs_UI_UIGeometry_H__

#include "UIGeometryBase.h"
#include <ExsEngine/RS/Base2DRectGeometry.h>


namespace Exs
{

	
	struct UITextGeometry;
	struct UIWidgetGeometry;

	class UIText;


	class EXS_LIBCLASS_UI UIGeometryComposer
	{
	public:
		bool UpdateTextGeometry(const UIText& textComponent, UITextGeometry* textGeometry, const std::string& newText);
	};

	
	class EXS_LIBCLASS_UI UIGeometryManager : public Base2DRectGeometryManager
	{
	public:
		UIGeometryManager(RenderSystem* renderSystem);
		virtual ~UIGeometryManager();
		
		///<summary>
		///</summary>
		bool AllocateWidgetGeometry(Size_t geometrySize, UIWidgetGeometry* geometry)
		{
			return this->ReserveIndexedGeometryStorage(geometrySize, geometry);
		}
		
		///<summary>
		///</summary>
		bool AllocateTextGeometry(Size_t textLength, UITextGeometry* geometry)
		{
			return this->ReserveNonIndexedGeometryStorage(textLength, geometry);
		}
		
		///<summary>
		/// Updates text geometry using provided text string and properties.
		///</summary>
		bool UpdateTextGeometry(UIGeometry& textGeometry, const std::string& newText, const TextProperties& textProperties);

	private:
		static void FillTextCharGeometry(const TextCharData& charData, const TextProperties& textProperties, UIGeometryVertex* vertices);
	};

	
	///<summary>
	///</summary>
	EXS_LIBAPI_UI Handle<UIGeometryManager> CreateUIGeometryManager(RenderSystem* renderSystem, const UIGeometryManagerInitDesc& initDesc);


}


#endif /* __Exs_UI_UIGeometry_H__ */
