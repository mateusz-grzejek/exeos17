
#pragma once

#ifndef __Exs_UI_UIWidgetGeometry_H__
#define __Exs_UI_UIWidgetGeometry_H__

#include "UIGeometry.h"


namespace Exs
{


	struct UIWidgetGeometryBatch
	{
		//
		Uint indicesBaseOffset;
	
		//
		Uint32 indicesNum;
	
		//
		Uint32 verticesNum;

		//
		ShaderResourceViewHandle sourceTexture;
	};


	///<summary>
	///</summary>
	struct UIWidgetGeometryVertex : public UIGeometryBaseVertex
	{
		// 3D position: 3 x sizeof(float) = 12 bytes.
		Vector3F position;

		// 2D offset: 2 x sizeof(float) = 8 bytes.
		Vector2F baseOffset;

		// 3D texture coord: 3 x sizeof(float) = 12 bytes.
		Vector3F texCoord;
	
		// Constant color: sizeof(Uint32) = 4 bytes.
		Uint32 fixedColor;
	
		// Padding value: 3 x sizeof(Uint32) = 12 bytes.
		Uint32 _padding[3];

		/// Total control vertex size: 48 bytes.
	};


}


#endif /* __Exs_UI_UIWidgetGeometry_H__ */
