
#pragma once

#ifndef __Exs_UI_CommonDefs_H__
#define __Exs_UI_CommonDefs_H__


namespace Exs
{


	enum : TraceCategoryID
	{
		TRC_UI = 0x770201,
	};

	
	EXS_TRACE_SET_CATEGORY_NAME(TRC_UI, "UI");


}


#endif /* __Exs_UI_CommonDefs_H__ */
