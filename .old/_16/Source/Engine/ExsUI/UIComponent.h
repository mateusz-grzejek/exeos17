
#pragma once

#ifndef __Exs_UI_UIComponent_H__
#define __Exs_UI_UIComponent_H__

#include "UIBase.h"


namespace Exs
{

	
	class UIWidget;
	class UILayer;


	enum : Enum
	{
		UIComponentState_Base_Property_Changed = 0x0001,

		UIComponentState_Dynamic_State_Enabled = 0x1000,

		UIComponentState_Default = UIComponentState_Base_Property_Changed | UIComponentState_Dynamic_State_Enabled,
	};


	class UIComponent
	{
	protected:
		struct DynamicState
		{
			Vector2F offset = Vector2F(0.0f, 0.0f);

			Vector2F scale = Vector2F(1.0f, 1.0f);

			float opacity = 1.0f;

			float rotationAngle = 0.0f;

			Color color = ColorU32::White;
		};

	protected:
		UILayer*          _layer;
		UIComponent*      _parent;
		UIAnchorPoint     _anchorPoint;
		Vector2F          _basePosition;
		Color             _baseColor;
		DynamicState      _dynamicState;
		stdx::mask<Enum>  _state;

	public:
		template <typename Pv_t, typename Sv_t>
		UIComponent(UILayer* layer, UIComponent* parent, const Vector2<Pv_t>& position, const Vector2<Sv_t>& size, const Color& color = ColorU32::White)
		: UIComponent(layer, parent, UIAnchorPoint::Default, position, size, color, UIComponentState_Default)
		{ }

		template <typename Pv_t, typename Sv_t>
		UIComponent(UILayer* layer, UIComponent* parent, UIAnchorPoint anchorPoint, const Vector2<Pv_t>& position, const Vector2<Sv_t>& size, const Color& color, stdx::mask<Enum> initialState)
		: _parent(parent)
		, _layer(layer)
		, _anchorPoint(anchorPoint)
		, _basePosition(position)
		, _baseSize(size)
		, _baseColor(color)
		, _state(initialState | UIComponentState_Base_Property_Changed)
		{ }

		void UpdateAnchorPoint(UIAnchorPoint anchorPoint)
		{
			this->_anchorPoint = anchorPoint;
			this->_state.set(UIComponentState_Base_Property_Changed);
		}

		void UpdateBaseColor(const Color& color)
		{
			this->_baseColor = color;
			this->_state.set(UIComponentState_Base_Property_Changed);
		}

		template <typename Pv_t>
		void UpdateBasePosition(const Vector2<Pv_t>& position)
		{
			this->_basePosition = position;
			this->_state.set(UIComponentState_Base_Property_Changed);
		}

		void EnableDynamicState(bool enable)
		{
			enable ? this->_state.set(UIComponentState_Dynamic_State_Enabled) : this->_state.unset(UIComponentState_Dynamic_State_Enabled);
		}

		void SetColor(const Color& color)
		{
			this->_dynamicState.color = color;
		}

		void SetOpacity(float opacity)
		{
			this->_dynamicState.opacity = Clamp(opacity, 0.0f, 1.0f);
		}

		void SetRotationAngleDeg(float angle)
		{
			this->_dynamicState.rotationAngle = (angle * EXS_MATH_SPC_PI) / 180.0f;
		}

		void SetRotationAngleRad(float angle)
		{
			this->_dynamicState.rotationAngle = angle;
		}
		
		template <typename Ov_t>
		void SetOffset(const Vector2<Ov_t>& offset)
		{
			this->_dynamicState.offset = offset;
		}
		
		template <typename Val_t>
		void SetOffsetX(Val_t xoff)
		{
			this->_dynamicState.offset.x = static_cast<decltype(this->_dynamicProperties.offset.x)>(xoff);
		}
		
		template <typename Val_t>
		void SetOffsetY(Val_t yoff)
		{
			this->_dynamicState.offset.y = static_cast<decltype(this->_dynamicProperties.offset.y)>(yoff);
		}

		void SetScale(const Vector2F& scale)
		{
			this->_dynamicState.scale = scale;
		}

		void SetScaleX(float xscale)
		{
			this->_dynamicState.scale.x = xscale;
		}

		void SetScaleY(float yscale)
		{
			this->_dynamicState.scale.y = yscale;
		}
		
		UIComponent* GetParent() const
		{
			return this->_parent;
		}

		template <class T>
		T* As()
		{
			return dbgsafe_ptr_cast<T*>(this);
		}

		template <class T>
		const T* As() const
		{
			return dbgsafe_ptr_cast<const T*>(this);
		}

		UILayer* GetLayer() const
		{
			return this->_layer;
		}

		const stdx::mask<Enum>& GetState() const
		{
			return this->_state;
		}

		bool IsDynamicStateEnabled() const
		{
			return this->_state.is_set(UIComponentState_Dynamic_State_Enabled);
		}
	};


}


#endif /* __Exs_UI_UIComponent_H__ */
