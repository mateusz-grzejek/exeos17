
#pragma once

#ifndef __Exs_UI_UIRenderer_H__
#define __Exs_UI_UIRenderer_H__

#include "UIBase.h"
#include <ExsEngine/RS/BaseRenderer.h>


namespace Exs
{

	
	ExsDeclareRSClassObjHandle(ConstantBuffer);
	ExsDeclareRSClassObjHandle(Sampler);
	ExsDeclareRSClassObjHandle(Shader);


	class EXS_LIBCLASS_ENGINE UIRenderer : public BaseRenderer
	{
	private:
		//
		static ShaderHandle defaultPixelShader;

		//
		static ShaderHandle defaultVertexShader;

		//
		static SamplerHandle fontTextureSampler;

		//
		static GraphicsPipelineStateObjectHandle defaultGraphicsPipelineState;

	public:
		UIRenderer(RenderingContext* rcontext);

		void Render(const int);
	};


}


#endif /* __Exs_UI_UIRenderer_H__ */
