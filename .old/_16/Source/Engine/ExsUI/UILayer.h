
#pragma once

#ifndef __Exs_UI_UILayer_H__
#define __Exs_UI_UILayer_H__

#include "UIComponent.h"


namespace Exs
{


	class UIGeometryManager;


	class EXS_LIBCLASS_UI UILayer
	{
	private:
		UIGeometryManager*        _geometryManager;
		UILayer*                  _prev;
		std::unique_ptr<UILayer>  _next;
		Int32                     _depth;

	public:
		UILayer(UIGeometryManager* geometryManager, UILayer* prev, Int32 depth);
		~UILayer();
		
		template <class Control, class... Args>
		Handle<Control> AddControl(Args&&... args);

		UILayer* NextLayer();

		UIGeometryManager* GetGeometryManager() const;

		UILayer* GetNext() const;
		UILayer* GetPrev() const;

		Int32 GetDepth() const;
	};
	

	template <class Control, class... Args>
	inline Handle<Control> UILayer::AddControl(Args&&... args)
	{
		return CreateAutoRefObject<Control>(nullptr, this, std::forward<Args>(args)...);
	}

	inline UIGeometryManager* UILayer::GetGeometryManager() const
	{
		return this->_geometryManager;
	}

	inline UILayer* UILayer::GetNext() const
	{
		return this->_next.get();
	}
	
	inline UILayer* UILayer::GetPrev() const
	{
		return this->_prev;
	}

	inline Int32 UILayer::GetDepth() const
	{
		return this->_depth;
	}


}


#endif /* __Exs_UI_UILayer_H__ */
