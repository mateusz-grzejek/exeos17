
#include "_Precompiled.h"
#include <ExsUI/UIGeometry.h>
#include <ExsUI/Controls/Text.h>
#include <ExsResManager/Resources/Font.h>


namespace Exs
{


	TextLabel::TextLabel(UIControl* parent, UILayer* layer, Font& font, Uint32 maxLength, const std::string& text, const Vector2U32& position, const Color& color)
	: UIControl(parent, layer, position, Vector2U32(0, 0), color)
	, _font(&font)
	, _maxLength(maxLength)
	, _text(text)
	{
		if (!this->_text.empty())
			this->_UpdateGeometry();
	}


	bool TextLabel::OnRenderBegin()
	{
		if (!UIControl::OnRenderBegin())
			return false;

		if (this->_state.is_set(UIControlState_Update_Geometry))
		{
			this->_UpdateGeometry();
			this->_state.unset(UIControlState_Update_Geometry);
		}

		return true;
	}


	void TextLabel::OnRenderEnd()
	{ }


	void TextLabel::_UpdateGeometry()
	{
		TextProperties textProperties;
		textProperties.baseOffset.x = static_cast<float>(this->_position.x);
		textProperties.baseOffset.y = static_cast<float>(this->_position.y);
		textProperties.baseOffset.z = static_cast<float>(this->_layer->GetDepth()) * 0.01f;
		textProperties.color = this->_color.value;
		textProperties.font = this->_font;

		auto* geometryManager = this->_layer->GetGeometryManager();
		
		if (!this->_geometry)
		{
			Uint32 geometrySize = stdx::get_max_of(this->_maxLength, static_cast<Uint32>(this->_text.length()));
			this->_geometry = geometryManager->AddGeometry(geometrySize);
		}

		geometryManager->UpdateTextGeometry(*(this->_geometry), this->_text, textProperties);
	}




	std::vector<TextCharData> TextUtils::GenerateTextCharData(const std::string& text, const TextProperties& textProperties)
	{
		std::vector<TextCharData> result;

		Int64 xbase = 0;
		Int64 ybase = 0;
		CodePoint prevChar = 0;

		if (textProperties.font != nullptr)
		{
			for (auto& c : text)
			{
				if (const auto* cglyph = textProperties.font->GetGlyph(c))
				{
					Int32 kerning = textProperties.font->GetKerning(prevChar, cglyph->codePoint);
					Int64 ybaseDiff = cglyph->metrics.size.y - cglyph->metrics.bearing.y;

					TextCharData charData;
					charData.bitmapLayer = cglyph->bitmap.layerIndex;
					charData.rect.position.x = xbase + kerning;
					charData.rect.position.y = ybase - ybaseDiff;
					charData.rect.size.x = cglyph->metrics.size.x;
					charData.rect.size.y = cglyph->metrics.size.y;
					charData.texc.position.x = cglyph->bitmap.offset.x;
					charData.texc.position.y = cglyph->bitmap.offset.y + cglyph->bitmap.size.y;
					charData.texc.size.x = cglyph->bitmap.size.x;
					charData.texc.size.y = -cglyph->bitmap.size.y;
					result.push_back(charData);

					xbase += (cglyph->metrics.advance.x + kerning - cglyph->metrics.bearing.x);
				}
			}

			if (!textProperties.font->IsLayeredFont())
			{
				std::sort(result.begin(), result.end(),
					[](const TextCharData& left, const TextCharData& right) -> bool {
						return left.bitmapLayer < right.bitmapLayer;
				});
			}
		}

		TextCharData charData;
		charData.bitmapLayer = -1;
		result.push_back(charData);

		return result;
	}


}
