
function(ExsAddApp
		name
		outSubdir
		baseDir
		srcDirList
		incDirList
		linkLibList)

	# Add project subdirectory with CMakeLists.txt
	add_subdirectory(${baseDir})

	set(targetName Sample.${name}.${CMINPUT_TARGET_ARCH}.${CMINPUT_TARGET_BUILD})

	# This var will store list of source files for this project.
	set(${name}_SRC_FILES)

	# Iterate over specified source directories and add files located there.
	foreach(srcDir ${srcDirList})
		set(baseSrcDir ${CMAKE_SOURCE_DIR}/${srcDir})
		if(EXISTS ${baseSrcDir})
			file(GLOB_RECURSE SRC_FILES
					"${baseSrcDir}/*.h"
					"${baseSrcDir}/*.inl"
					"${baseSrcDir}/*.c"
					"${baseSrcDir}/*.cpp")
			list(APPEND ${name}_SRC_FILES ${SRC_FILES})
		endif()
	endforeach()

	# Add subproject
	add_executable(${targetName} ${${name}_SRC_FILES})

	# Set output directories.
	set_target_properties(${targetName} PROPERTIES
			OUTPUT_NAME ${name}
			ARCHIVE_OUTPUT_DIRECTORY "${CMCFG_COMMON_DIR_OUTPUT}/${outSubdir}"
			LIBRARY_OUTPUT_DIRECTORY "${CMCFG_COMMON_DIR_OUTPUT}/${outSubdir}"
			RUNTIME_OUTPUT_DIRECTORY "${CMCFG_COMMON_DIR_OUTPUT}/${outSubdir}")

	# Add include directories
	target_include_directories(${targetName} PUBLIC ${incDirList})

	# Add linker input
	target_link_libraries(${targetName} PUBLIC ${linkLibList})

endfunction(ExsAddApp)

function(ExsAddLibrary
	type
	libType
	name
	libName
	outSubdir
	baseDir
	srcDirList
	incDirList
	linkLibList)

	# Add project subdirectory with CMakeLists.txt
	add_subdirectory(${baseDir})

	set(targetName ${type}.${name}.${CMINPUT_TARGET_ARCH}.${CMINPUT_TARGET_BUILD})

	# This var will store list of source files for this project.
	set(${name}_SRC_FILES)

	# Append source files defined by the project.
	list(APPEND ${name}_SRC_FILES ${CMCFG_EXS_${type}_${name}_SRC_FILES})

	# Iterate over specified source directories and add files located there.
	foreach(srcDir ${srcDirList})
		set(baseSrcDir ${CMAKE_SOURCE_DIR}/${srcDir})
		if(EXISTS ${baseSrcDir})
			file(GLOB_RECURSE SRC_FILES
				"${baseSrcDir}/*.h"
				"${baseSrcDir}/*.inl"
				"${baseSrcDir}/*.c"
				"${baseSrcDir}/*.cpp")
			list(APPEND ${name}_SRC_FILES ${SRC_FILES})
		endif()
	endforeach()

	# Add subproject
	add_library(${targetName} ${${name}_SRC_FILES})

	# Set output directories.
	set_target_properties(${targetName} PROPERTIES
		OUTPUT_NAME ${libName}
		ARCHIVE_OUTPUT_DIRECTORY "${CMCFG_COMMON_DIR_OUTPUT}/${outSubdir}"
		LIBRARY_OUTPUT_DIRECTORY "${CMCFG_COMMON_DIR_OUTPUT}/${outSubdir}"
		RUNTIME_OUTPUT_DIRECTORY "${CMCFG_COMMON_DIR_OUTPUT}/${outSubdir}")

	# Add include directories
	target_include_directories(${targetName} PUBLIC ${incDirList})

	# Add linker input
	target_link_libraries(${targetName} PUBLIC ${linkLibList})

endfunction(ExsAddLibrary)

function(ExsAddLibraryDependency name)
    ExsAddLibrary(
		Dependency
		STATIC
		${name}
		${name}
		""
		"Source/Dependencies/${name}"
		""
		"${CMCFG_EXS_DEPENDENCIES_INCLUDE_DIRS}"
		"")
endfunction(ExsAddLibraryDependency)

function(ExsAddLibraryEngineLib name)
    ExsAddLibrary(
    	EngineLib
		STATIC
		${name}
		Exs${name}
		""
		"Source/Engine/Exs/${name}"
		"Source/Engine/Exs/${name}"
		"${CMCFG_EXS_ENGINE_INCLUDE_DIRS}"
		"${CMCFG_EXS_ENGINE_LINKER_INPUT}")
endfunction(ExsAddLibraryEngineLib)

function(ExsAddLibraryEngineModule libType name)
    ExsAddLibrary(
    	EngineModule
		libType
		${name}
		Exs${name}
		""
		"Source/Engine/Exs/${name}"
		"Source/Engine/Exs/${name}"
		"${CMCFG_EXS_ENGINE_INCLUDE_DIRS}"
		"${CMCFG_EXS_ENGINE_LINKER_INPUT}")
endfunction(ExsAddLibraryEngineModule)

function(ExsAddLibrarySystemDrvRenderSystem libType name commonName)
    ExsAddLibrary(
    	RSDriver
		libType
		${name}
		Exs${name}
		""
		"Source/SystemDrv/RenderSystem/ExsDrv/${name}"
		"Source/SystemDrv/RenderSystem/ExsDrv/${name};Source/SystemDrv/RenderSystem/ExsDrv/${commonName}"
		"${CMCFG_EXS_SYSTEMDRV_RENDERSYSTEM_INCLUDE_DIRS}"
		"${CMCFG_EXS_SYSTEMDRV_RENDERSYSTEM_LINKER_INPUT}")
endfunction(ExsAddLibrarySystemDrvRenderSystem)

function(ExsAddAppSample name)
	ExsAddApp(
		${name}
		""
		"Samples/${name}"
		"Samples/${name}"
		"${CMCFG_EXS_SAMPLE_INCLUDE_DIRS}"
		"${CMCFG_EXS_SAMPLE_LINKER_INPUT}")
endfunction(ExsAddAppSample)


#function(ExsAddLibraryGraphicDriver common name)
#	add_subdirectory(Source/Drivers/Graphics/ExsGraphicDriver${name})
#	set(targetName GraphicDriver.${name}.${EXSC_TARGET_ARCH}.${EXSC_TARGET_BUILD})
#	add_library(${targetName} SHARED
#		${ExsFilesGraphicDriver_${name}_Include} ${ExsFilesGraphicDriver_${name}_Source} ${ExsFilesGraphicDriver_${common}_Include} ${ExsFilesGraphicDriver_${common}_Source})
#	set_target_properties(${targetName} PROPERTIES
#		OUTPUT_NAME "ExsGraphicDriver.${name}" ARCHIVE_OUTPUT_DIRECTORY "${ExsCfgBinDirectory}/Drivers" LIBRARY_OUTPUT_DIRECTORY "${ExsCfgBinDirectory}/Drivers" RUNTIME_OUTPUT_DIRECTORY "${ExsCfgBinDirectory}/Drivers")
#	target_compile_definitions(${targetName} PRIVATE ${ExsCfgCompilerDefs} ${ExsCfgCompilerDefsGraphicDriver_${name}})
#	target_compile_options(${targetName} PRIVATE ${ExsCfgCompilerFlags})
#	target_include_directories(${targetName} PRIVATE ${ExsCfgIncludeDirsGraphicDriver_${name}})
#	foreach(includeDir ${ExsCfgIncludeDirs_GraphicDrivers})
#		target_include_directories(${targetName} PRIVATE ${includeDir})
#	endforeach()
#	target_link_libraries(${targetName} ${ExsCfgLinkerInputGraphicDriver_${name}})
#endfunction(ExsAddLibraryGraphicDriver)
#
#function(ExsAddLibrarySample name)
#	add_subdirectory(Samples/${name})
#	set(targetName Sample.${name}.${EXSC_TARGET_ARCH}.${EXSC_TARGET_BUILD})
#	add_library(${targetName}
#		${ExsFilesSample_${name}_Include} ${ExsFilesSample_${name}_Source})
#	set_target_properties(${targetName} PROPERTIES
#		OUTPUT_NAME ${name} RUNTIME_OUTPUT_DIRECTORY ${ExsCfgBinDirectory})
#	target_compile_definitions(${targetName} PRIVATE ${ExsCfgCompilerDefs} ${ExsCfgCompilerDefsSample_${name}})
#	target_compile_options(${targetName} PRIVATE ${ExsCfgCompilerFlags})
#	target_include_directories(${targetName} PRIVATE ${ExsCfgIncludeDirsSample_${name}})
#	foreach(includeDir ${ExsCfgIncludeDirs_Samples})
#		target_include_directories(${targetName} PRIVATE ${includeDir})
#	endforeach()
#endfunction(ExsAddLibrarySample)
