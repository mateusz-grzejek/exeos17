
set(CMCFG_EXTERNAL_INCLUDE_DIRS
	Source/Dependencies)

set(CMCFG_EXS_DEPENDENCIES_INCLUDE_DIRS
	Source/Dependencies)

set(CMCFG_EXS_ENGINE_INCLUDE_DIRS
	Config/Include;
	External/Include;
	Source/Dependencies;
	Source/Engine)

set(CMCFG_EXS_SYSTEMDRV_RENDERSYSTEM_INCLUDE_DIRS
	Config/Include;
	External/Include;
	Source/Dependencies;
	Source/Engine;
	Source/Drivers/RenderSystem)

set(CMCFG_EXS_SYSTEMDRV_SCRIPTSYSTEM_INCLUDE_DIRS
	Config/Include;
	External/Include;
	Source/Dependencies;
	Source/Engine;
	Source/Drivers/ScriptSystem)

set(CMCFG_EXS_SYSTEMDRV_SOUNDSYSTEM_INCLUDE_DIRS
	Config/Include;
	External/Include;
	Source/Dependencies;
	Source/Engine;
	Source/Drivers/SoundSystem)

set(CMCFG_EXS_SAMPLE_INCLUDE_DIRS
	Config/Include;
	External/Include;
	Source/Dependencies;
	Source/Engine)
