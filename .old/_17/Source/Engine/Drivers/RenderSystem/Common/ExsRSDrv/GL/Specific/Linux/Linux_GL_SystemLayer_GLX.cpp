
#include <ExsCommonGL/GL_SystemLayer.h>
#include <ExsCore/System/SystemWindow.h>
#include <stdx/string_utils.h>


namespace Exs
{


	GLXFBConfig GLXSelectFBConfig(SystemEnvData* systemEnvData, const VisualConfig& visualConfig);
	bool GLXEnumerateFBConfigs(SystemEnvData* systemEnvData, const VisualConfig& visualConfig, std::vector<GLXFBConfig>* fbConfigArray);


	void GLSystemContext::Bind()
	{
		ExsDebugAssert( this->_contextData.handle != nullptr );

		Window windowHandle = this->_systemWindow->GetHandle();
		int result = glXMakeContextCurrent(this->_systemEnvData->display, windowHandle, windowHandle, this->_contextData.handle);
		ExsDebugAssert( result == True );
	}

	void GLSystemContext::Unbind()
	{
		ExsDebugAssert( this->_contextData.handle != nullptr );

		int result = glXMakeCurrent(this->_systemEnvData->display, X11_Null_Window, nullptr);
		ExsDebugAssert( result == True );
	}


	void GLSystemContext::Destroy()
	{
		if (this->_contextData.handle == nullptr)
			return;

		GLXContext currentContext = glXGetCurrentContext();
		GLXDrawable currentDrawable = glXGetCurrentDrawable();

		if (this->_contextData.handle == currentContext)
		{
			Window windowHandle = this->_systemWindow->GetHandle();
			ExsDebugAssert( windowHandle == currentDrawable );

			glXMakeCurrent(this->_systemEnvData->display, X11_Null_Window, nullptr);
		}

		glXDestroyContext(this->_systemEnvData->display, this->_contextData.handle);

		this->_contextData.handle = nullptr;
		this->_systemWindow = nullptr;
		this->_systemEnvData = nullptr;
	}


	void GLSystemContext::SwapBuffers()
	{
		ExsDebugAssert( this->_contextData.handle != nullptr );

		Window windowHandle = this->_systemWindow->GetHandle();
		glXSwapBuffers(this->_systemEnvData->display, windowHandle);
	}




	std::unique_ptr<GLSystemContext> GLSystemLayer::CreateLegacyContext(SystemWindow& systemWindow, const VisualConfig& visualConfig)
	{
		auto appGlobalState = systemWindow.GetAppGlobalState();
		auto* systemEnvData = &(appGlobalState->systemEnvData);

		if (glXQueryExtension(systemEnvData->display, nullptr, nullptr) == False)
		{
			ExsTraceError(TRC_GraphicsDriver_GL, "glXQueryExtension: failed!");
			return nullptr;
		}

		GLXFBConfig fbConfig = GLXSelectFBConfig(systemEnvData, visualConfig);
		if (fbConfig == nullptr)
		{
			ExsTraceError(TRC_GraphicsDriver_GL, "SelectFBConfig: failed!");
			return nullptr;
		}

		GLXContext contextHandle = glXCreateNewContext(systemEnvData->display, fbConfig, GLX_RGBA_TYPE, nullptr, True);
		if (!contextHandle)
		{
			ExsTraceWarning(TRC_GraphicsDriver_GL, "Failed to create <legacy> GL context (glXCreateContextAttribsARB() has failed).");
			return nullptr;
		}

		GLSystemContextData contextData;
		contextData.handle = contextHandle;

		auto systemContextObject = std::make_unique<GLSystemContext>(&systemWindow, systemEnvData, contextData);
		return systemContextObject;
	}


	std::unique_ptr<GLSystemContext> GLSystemLayer::CreateCoreContext(SystemWindow& systemWindow, const GLSystemCoreContextInitAttribs& initAttribs)
	{
		auto appGlobalState = systemWindow.GetAppGlobalState();
		auto* systemEnvData = &(appGlobalState->systemEnvData);

		GLXFBConfig fbConfig = GLXSelectFBConfig(systemEnvData, initAttribs.visualConfig);
		if (fbConfig == nullptr)
		{
			ExsTraceError(TRC_GraphicsDriver_GL, "");
			return nullptr;
		}

		auto glXCreateContextAttribsARBPtr = (PFNGLXCREATECONTEXTATTRIBSARBPROC)glXGetProcAddressARB((const GLubyte*)("glXCreateContextAttribsARB"));
		if (!glXCreateContextAttribsARBPtr)
		{
			ExsTraceError(TRC_GraphicsDriver_GL, "");
			return nullptr;
		}

		GLVersionNumber versionNumber = initAttribs.version;
		stdx::mask<int> contextFlags = 0;

		if (initAttribs.flags.is_set(GLSystemContextInit_Enable_Debug))
			contextFlags.set(GLX_CONTEXT_DEBUG_BIT_ARB);

		if (initAttribs.flags.is_set(GLSystemContextInit_Forward_Compatible))
			contextFlags.set(GLX_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB);

		const int contextAttribs[] =
		{
			GLX_CONTEXT_MAJOR_VERSION_ARB, versionNumber.major,
			GLX_CONTEXT_MINOR_VERSION_ARB, versionNumber.minor,
			GLX_CONTEXT_PROFILE_MASK_ARB,  GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
			GLX_CONTEXT_FLAGS_ARB,         contextFlags,
			X11C_None
		};

		GLXContext contextHandle = glXCreateContextAttribsARBPtr(systemEnvData->display, fbConfig, nullptr, True, contextAttribs);
		if (!contextHandle)
		{
			ExsTraceWarning(TRC_GraphicsDriver_GL, "Failed to create <core> GL context (glXCreateContextAttribsARB() has failed).");
			return nullptr;
		}

		GLSystemContextData contextData;
		contextData.handle = contextHandle;

		auto systemContextObject = std::make_unique<GLSystemContext>(&systemWindow, systemEnvData, contextData);
		return systemContextObject;
	}


	std::vector<std::string> GLSystemLayer::QuerySupportedSystemExtensions(GLSystemContext* systemContext, SystemEnvData* systemEnvData)
	{
		const auto& contextData = systemContext->GetContextData();
		std::vector<std::string> extensionArray;

		if (const char* glxExtensionStr = glXQueryExtensionsString(systemEnvData->display, systemEnvData->screen))
		{
			stdx::split_string(glxExtensionStr, ' ',
				[&extensionArray](const char* str, size_t length) -> void {
					extensionArray.push_back(std::string(str, length));
				});
		}

		return extensionArray;
	}


	std::vector<VisualMSAAConfig> GLSystemLayer::QuerySupportedMSAAConfigurations(GLSystemContext* systemContext, SystemEnvData* systemEnvData, const VisualConfig& visualConfig)
	{
		const auto& contextData = systemContext->GetContextData();

		std::vector<GLXFBConfig> fbConfigArray { };
		std::vector<VisualMSAAConfig> msaaConfigArray { };

		if (!GLXEnumerateFBConfigs(systemEnvData, visualConfig, &fbConfigArray))
		{
			ExsTraceError(TRC_GraphicsDriver_GL, "");
			return msaaConfigArray;
		}

		const Uint32 maxMSAALevel = RSGetVisualMSAAConfigSamplesNum(VisualMSAAConfig::Best);
		Uint32 msaaConfigurationsNum = 0;
		bool msaaSupportStateArray[maxMSAALevel + 1] = { false };

		for (auto& fbConfig : fbConfigArray)
		{
			int samplesNumValue = -1;
			glXGetFBConfigAttrib(systemEnvData->display, fbConfig, GLX_SAMPLE_BUFFERS, &samplesNumValue);

			if ((samplesNumValue == 0) || (samplesNumValue > maxMSAALevel))
				continue;

			if (msaaSupportStateArray[samplesNumValue])
				continue;

			msaaSupportStateArray[samplesNumValue] = true;
			msaaConfigurationsNum += 1;
		}

		msaaConfigArray.clear();
		msaaConfigArray.reserve(msaaConfigurationsNum);

		for (Uint32 n = 1; n < maxMSAALevel + 1; ++n)
		{
			if (msaaSupportStateArray[n])
			{
				VisualMSAAConfig msaaConfig = RSGetVisualMSAAConfigForSampleNum(n);
				msaaConfigArray.push_back(msaaConfig);
			}
		}

		if (msaaConfigArray.empty())
		{
			VisualMSAAConfig defaultMSAAConfig = RSGetVisualMSAAConfigForSampleNum(0);
			msaaConfigArray.push_back(defaultMSAAConfig);
		}

		return msaaConfigArray;
	}




	GLXFBConfig GLXSelectFBConfig(SystemEnvData* systemEnvData, const VisualConfig& visualConfig)
	{
		std::vector<GLXFBConfig> fbConfigArray { };

		if (!GLXEnumerateFBConfigs(systemEnvData, visualConfig, &fbConfigArray))
		{
			ExsTraceError(TRC_GraphicsDriver_GL, "");
			return nullptr;
		}

		return fbConfigArray[0];
	}


	bool GLXEnumerateFBConfigs(SystemEnvData* systemEnvData, const VisualConfig& visualConfig, std::vector<GLXFBConfig>* fbConfigArray)
	{
		VisualColorDesc colorDesc = RSGetVisualColorDesc(visualConfig.colorFormat);
		VisualDepthStencilDesc depthStencilDesc = RSGetVisualDepthStencilDesc(visualConfig.depthStencilFormat);
		VisualMSAADesc msaaDesc = RSGetVisualMSAADesc(visualConfig.msaaConfig);

		const int visualAttribs[] =
		{
			GLX_X_RENDERABLE,    True,
			GLX_DOUBLEBUFFER,    True,
		    GLX_DRAWABLE_TYPE,   GLX_WINDOW_BIT,
		    GLX_RENDER_TYPE,     GLX_RGBA_BIT,
			GLX_X_VISUAL_TYPE,   GLX_TRUE_COLOR,
			GLX_RED_SIZE,        colorDesc.redBits,
			GLX_GREEN_SIZE,      colorDesc.greenBits,
			GLX_BLUE_SIZE,       colorDesc.blueBits,
			GLX_ALPHA_SIZE,      colorDesc.alphaBits,
			GLX_DEPTH_SIZE,      depthStencilDesc.depthBufferSize,
			GLX_STENCIL_SIZE,    depthStencilDesc.stencilBufferSize,
		    GLX_SAMPLE_BUFFERS,  static_cast<int>(msaaDesc.count),
		    GLX_SAMPLES,         static_cast<int>(msaaDesc.quality),
		    X11C_None
		};

		int compatibleFBConfigsNum = 0;
		int fbConfigsNum = 0;

		if (GLXFBConfig* fbConfigs = glXChooseFBConfig(systemEnvData->display, systemEnvData->screen, visualAttribs, &fbConfigsNum))
		{
			if (fbConfigsNum == 0)
				return false;

			auto fbConfigsArrayView = BindArrayView(fbConfigs, fbConfigsNum);
			for (auto& fbConfig : fbConfigsArrayView)
			{;
				if (XVisualInfo* visualInfo = glXGetVisualFromFBConfig(systemEnvData->display, fbConfig))
				{
					fbConfigArray->push_back(fbConfig);
					compatibleFBConfigsNum += 1;
					XFree(visualInfo);
				}
			}

			XFree(fbConfigs);
			fbConfigs = nullptr;

			if (compatibleFBConfigsNum > 0)
				return true;
		}

		return false;
	}


}
