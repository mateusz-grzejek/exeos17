
#include <ExsCommonGL/GL_DebugOutput.h>

// #pragma clang diagnostic push
// #pragma ide diagnostic ignored "IncompatibleTypes"


namespace Exs
{


#if ( EXS_GL_FEATURE_DEBUG_OUTPUT )

	static const Size_t eventInfoBufferSize = 1024;

	GLDebugEventClassification GetEventClassification(GLenum category, GLenum type, GLenum severity);

	class GLDebugOutputAMD : public GLDebugOutput
	{
	public:
		GLDebugOutputAMD();
		virtual ~GLDebugOutputAMD();

		virtual void EnableSync(bool enable) override;

	protected:
		virtual void SetCallbackActive(bool active) override;

		void ProcessEvent(GLuint id, GLenum category, GLenum severity, const GLchar* message);

		static void GLAPIENTRY EventCallback(GLuint id, GLenum category, GLenum severity, GLsizei length, const GLchar* message, GLvoid* userParam);
	};

	class GLDebugOutputARB : public GLDebugOutput
	{
	public:
		GLDebugOutputARB();
		virtual ~GLDebugOutputARB();

		virtual void EnableSync(bool enable) override;

	protected:
		virtual void SetCallbackActive(bool active) override;

		void ProcessEvent(GLuint id, GLenum source, GLenum type, GLenum severity, const GLchar* message);

		static void GLAPIENTRY EventCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const GLvoid* userParam);
	};

	class GLDebugOutputKHR : public GLDebugOutput
	{
	public:
		GLDebugOutputKHR();
		virtual ~GLDebugOutputKHR();

		virtual void EnableSync(bool enable) override;

	protected:
		virtual void SetCallbackActive(bool active) override;

		void ProcessEvent(GLuint id, GLenum source, GLenum type, GLenum severity, const GLchar* message);

		static void GLAPIENTRY EventCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const GLvoid* userParam);
	};




	GLDebugOutput::GLDebugOutput(GLDebugOutputVersion version, const char* extensionName)
	: _version(version)
	, _extensionName(extensionName)
	, _eventsCounter(0)
	, _breakOnEvent(true)
	, _eventFilterEnabled(false)
	, _callbackActive(false)
	{ }


	GLDebugOutput::~GLDebugOutput()
	{ }


	void GLDebugOutput::ActivateCallback()
	{
		this->SetCallbackActive(true);
		ExsTraceInfo(TRC_GraphicsDriver_GL, "Debug output callback has been activated.");
	}


	void GLDebugOutput::DeactivateCallback()
	{
		this->SetCallbackActive(false);
		ExsTraceInfo(TRC_GraphicsDriver_GL, "Debug output callback has been deactivated.");
	}


	void GLDebugOutput::SetEventFilter(GLuint eventID, bool ignored)
	{
		if (ignored)
		{
			this->_ignoredEvents[eventID] = eventID;
		}
		else
		{
			auto eventEntryRef = this->_ignoredEvents.find(eventID);

			if (eventEntryRef != this->_ignoredEvents.end())
				this->_ignoredEvents.erase(eventEntryRef);
		}
	}


	bool GLDebugOutput::IsEventIgnored(GLuint eventID) const
	{
		if (!this->_eventFilterEnabled || this->_ignoredEvents.empty())
			return false;

		auto eventEntryRef = this->_ignoredEvents.find(eventID);
		return eventEntryRef != this->_ignoredEvents.end();
	}


	void GLDebugOutput::EnableDebugOutput(bool enable)
	{
		if (enable)
		{
			glEnable(GL_DEBUG_OUTPUT);
			ExsTraceInfo(TRC_GraphicsDriver_GL, "Debug output has been enabled.");
		}
		else
		{
			glDisable(GL_DEBUG_OUTPUT);
			ExsTraceInfo(TRC_GraphicsDriver_GL, "Debug output has been disabled.");
		}
	}


	std::unique_ptr<GLDebugOutput> GLDebugOutput::Initialize(const GLExtensionManager& extensionManager, GLDebugOutputVersion preferred)
	{
		GLDebugOutputVersion selectedDebugOutputVersion = GLDebugOutputVersion::Unknown;

		if (ValidateVersion(preferred) && extensionManager.CheckSupport(static_cast<GLCoreExtensionID>(preferred)))
			selectedDebugOutputVersion = preferred;
		else if (extensionManager.CheckSupport(static_cast<GLCoreExtensionID>(GLDebugOutputVersion::KHR_Core)))
			selectedDebugOutputVersion = GLDebugOutputVersion::KHR_Core;
		else if (extensionManager.CheckSupport(static_cast<GLCoreExtensionID>(GLDebugOutputVersion::ARB_Extension)))
			selectedDebugOutputVersion = GLDebugOutputVersion::ARB_Extension;
		else if (extensionManager.CheckSupport(static_cast<GLCoreExtensionID>(GLDebugOutputVersion::AMD_Extension)))
			selectedDebugOutputVersion = GLDebugOutputVersion::AMD_Extension;

		std::unique_ptr<GLDebugOutput> debugOutputInterface;

		switch(selectedDebugOutputVersion)
		{
		case GLDebugOutputVersion::AMD_Extension:
			debugOutputInterface = std::make_unique<GLDebugOutputAMD>();

		case GLDebugOutputVersion::ARB_Extension:
			debugOutputInterface = std::make_unique<GLDebugOutputARB>();

		case GLDebugOutputVersion::KHR_Core:
			debugOutputInterface = std::make_unique<GLDebugOutputKHR>();
		}

		if (debugOutputInterface)
		{
			const auto& extString = debugOutputInterface->GetExtensionName();
			GLDebugOutputVersion version = debugOutputInterface->GetVersion();

			ExsTraceInfo(TRC_GraphicsDriver_GL,
				"GLDebugOutput object has been initialized (version %u: %s)", version, extString.c_str());
		}

		return debugOutputInterface;
	}


	void GLDebugOutput::DisplayEventInfo(GLDebugEventClassification eventClassification, const char* eventInfo) const
	{
	#if ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_WIN32 )

		auto messageBoxText = std::string(eventInfo) + "\n\n--> Break execution?";
		UINT messageIcon = MB_ICONINFORMATION;
		if (eventClassification == GLDebugEventClassification::Error)
			messageIcon = MB_ICONSTOP;
		if (eventClassification == GLDebugEventClassification::Warning)
			messageIcon = MB_ICONWARNING;

		if (MessageBoxA(nullptr, messageBoxText.c_str(), "OpenGL Debug Event", MB_YESNO | messageIcon) == IDYES)
			ExsRuntimeInterrupt();

	#else

		ExsTraceNotification(TRC_GraphicsDriver_GL, eventInfo);
		ExsDebugAssert( !this->_breakOnEvent );

	#endif
	}




	const char* GetEventCategoryStr(GLenum category)
	{
		switch(category)
		{
		case GL_DEBUG_CATEGORY_API_ERROR_AMD:
			return "API error";
		case GL_DEBUG_CATEGORY_WINDOW_SYSTEM_AMD:
			return "Window system";
		case GL_DEBUG_CATEGORY_DEPRECATION_AMD:
			return "Deprecation";
		case GL_DEBUG_CATEGORY_UNDEFINED_BEHAVIOR_AMD:
			return "Undefined behavior";
		case GL_DEBUG_CATEGORY_PERFORMANCE_AMD:
			return "Performance";
		case GL_DEBUG_CATEGORY_SHADER_COMPILER_AMD:
			return "Shader compiler";
		case GL_DEBUG_CATEGORY_APPLICATION_AMD:
			return "Application";
		case GL_DEBUG_CATEGORY_OTHER_AMD:
			return "Other";
		default:
			return "Unknown";
		}
	}


	const char* GetEventSourceStr(GLenum source)
	{
		switch(source)
		{
		case GL_DEBUG_SOURCE_API:
			return "GL API";
		case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
			return "Window system";
		case GL_DEBUG_SOURCE_SHADER_COMPILER:
			return "Shader compiler";
		case GL_DEBUG_SOURCE_THIRD_PARTY:
			return "Third party";
		case GL_DEBUG_SOURCE_APPLICATION:
			return "Application";
		case GL_DEBUG_SOURCE_OTHER:
			return "Other";
		default:
			return "Unknown";
		}
	}


	const char* GetEventTypeStr(GLenum type)
	{
		switch(type)
		{
		case GL_DEBUG_TYPE_ERROR:
			return "Error";
		case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
			return "Deprecated behavior";
		case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
			return "Undefined behavior";
		case GL_DEBUG_TYPE_PORTABILITY:
			return "Portability";
		case GL_DEBUG_TYPE_PERFORMANCE:
			return "Performance";
		case GL_DEBUG_TYPE_OTHER:
			return "Other";
		default:
			return "Unknown";
		}
	}


	const char* GetEventSeverityStr(GLenum severity)
	{
		switch(severity)
		{
		case GL_DEBUG_SEVERITY_HIGH:
			return "High";
		case GL_DEBUG_SEVERITY_MEDIUM:
			return "Medium";
		case GL_DEBUG_SEVERITY_LOW:
			return "Low";
		case GL_DEBUG_SEVERITY_NOTIFICATION:
			return "Notification";
		default:
			return "Unknown";
		}
	}


	GLDebugEventClassification GetEventClassification(GLenum category, GLenum type, GLenum severity)
	{
		if ((category == GL_DEBUG_CATEGORY_API_ERROR_AMD) || (category == GL_DEBUG_CATEGORY_UNDEFINED_BEHAVIOR_AMD))
			return GLDebugEventClassification::Error;

		if ((category == GL_DEBUG_CATEGORY_PERFORMANCE_AMD) || (category == GL_DEBUG_CATEGORY_DEPRECATION_AMD))
			return GLDebugEventClassification::Warning;

		if ((type == GL_DEBUG_TYPE_ERROR) || (type == GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR))
			return GLDebugEventClassification::Error;

		if ((type == GL_DEBUG_TYPE_PERFORMANCE) || (type == GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR))
			return GLDebugEventClassification::Warning;

		if (severity == GL_DEBUG_SEVERITY_HIGH)
			return GLDebugEventClassification::Error;

		if (severity == GL_DEBUG_SEVERITY_MEDIUM)
			return GLDebugEventClassification::Warning;

		return GLDebugEventClassification::Notification;
	}




	GLDebugOutputAMD::GLDebugOutputAMD()
	: GLDebugOutput(GLDebugOutputVersion::AMD_Extension, "AMD_debug_output")
	{ }


	GLDebugOutputAMD::~GLDebugOutputAMD()
	{ }


	void GLDebugOutputAMD::EnableSync(bool enable)
	{
		EXS_UNUSED( enable );
	}


	void GLDebugOutputAMD::SetCallbackActive(bool active)
	{
		if (active)
		{
			glDebugMessageCallbackAMD(EventCallback, this);
			this->_callbackActive = true;
		}
		else
		{
			glDebugMessageCallbackAMD(nullptr, nullptr);
			this->_callbackActive = false;
		}
	}


	void GLDebugOutputAMD::ProcessEvent(GLuint id, GLenum category, GLenum severity, const GLchar* message)
	{
		++this->_eventsCounter;

		if (this->IsEventIgnored(id))
			return;

		char eventInfoBuffer[eventInfoBufferSize];

		snprintf(	eventInfoBuffer,
					eventInfoBufferSize,
					"\n----------------------------------------------------------------------\n"\
					"[GL Debug Event (via AMD_debug_output)]\n\n"\
					"- ID: %u\n"\
					"- Category: %s\n"\
					"- Severity: %s\n\n"\
					"- Description: %s\n"\
					"----------------------------------------------------------------------\n",
					id,
					GetEventCategoryStr(category),
					GetEventSeverityStr(severity),
					message);

		GLDebugEventClassification eventClassification = GetEventClassification(category, 0, severity);
		this->DisplayEventInfo(eventClassification, eventInfoBuffer);
	}


	void GLAPIENTRY GLDebugOutputAMD::EventCallback(
		GLuint id, GLenum category, GLenum severity, GLsizei length, const GLchar* message, GLvoid* userParam)
	{
		GLDebugOutputAMD* debugOutputInterface = reinterpret_cast<GLDebugOutputAMD*>((void*)userParam);
		debugOutputInterface->ProcessEvent(id, category, severity, message);
	}




	GLDebugOutputARB::GLDebugOutputARB()
	: GLDebugOutput(GLDebugOutputVersion::ARB_Extension, "ARB_debug_output")
	{ }


	GLDebugOutputARB::~GLDebugOutputARB()
	{ }


	void GLDebugOutputARB::EnableSync(bool enable)
	{
		if (enable)
			glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
		else
			glDisable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
	}


	void GLDebugOutputARB::SetCallbackActive(bool active)
	{
		if (active)
		{
			glDebugMessageCallbackARB(EventCallback, this);
			this->_callbackActive = true;
		}
		else
		{
			glDebugMessageCallbackARB(nullptr, nullptr);
			this->_callbackActive = false;
		}
	}


	void GLDebugOutputARB::ProcessEvent(GLuint id, GLenum source, GLenum type, GLenum severity, const GLchar* message)
	{
		++this->_eventsCounter;

		if (this->IsEventIgnored(id))
			return;

		char eventInfoBuffer[eventInfoBufferSize];

		snprintf(	eventInfoBuffer,
					eventInfoBufferSize,
					"\n----------------------------------------------------------------------\n"\
					"[GL Debug Event (via ARB_debug_output)]\n\n"\
					"- ID: %u\n"\
					"- Source: %s\n"\
					"- Type: %s\n"\
					"- Severity: %s\n\n"\
					"- Description: %s\n"\
					"----------------------------------------------------------------------\n",
					id,
					GetEventSourceStr(source),
					GetEventTypeStr(type),
					GetEventSeverityStr(severity),
					message);

		GLDebugEventClassification eventClassification = GetEventClassification(0, type, severity);
		this->DisplayEventInfo(eventClassification, eventInfoBuffer);
	}


	void GLAPIENTRY GLDebugOutputARB::EventCallback(
		GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const GLvoid* userParam)
	{
		GLDebugOutputARB* debugOutputInterface = reinterpret_cast<GLDebugOutputARB*>((void*)userParam);
		debugOutputInterface->ProcessEvent(id, source, type, severity, message);
	}




	GLDebugOutputKHR::GLDebugOutputKHR()
	: GLDebugOutput(GLDebugOutputVersion::KHR_Core, "KHR_debug")
	{ }


	GLDebugOutputKHR::~GLDebugOutputKHR()
	{ }


	void GLDebugOutputKHR::EnableSync(bool enable)
	{
		if (enable)
			glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		else
			glDisable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	}


	void GLDebugOutputKHR::SetCallbackActive(bool active)
	{
		if (active)
		{
			glDebugMessageCallback(EventCallback, this);
			this->_callbackActive = true;
		}
		else
		{
			glDebugMessageCallback(nullptr, nullptr);
			this->_callbackActive = false;
		}
	}


	void GLDebugOutputKHR::ProcessEvent(GLuint id, GLenum source, GLenum type, GLenum severity, const GLchar* message)
	{
		++this->_eventsCounter;

		if (this->IsEventIgnored(id))
			return;

		char eventInfoBuffer[eventInfoBufferSize];

		snprintf(	eventInfoBuffer,
					eventInfoBufferSize,
					"\n----------------------------------------------------------------------\n"\
					"[GL Debug Event (via KHR_debug)]\n\n"\
					"- ID: %u\n"\
					"- Source: %s\n"\
					"- Type: %s\n"\
					"- Severity: %s\n\n"\
					"- Description: %s\n"\
					"----------------------------------------------------------------------\n",
					id,
					GetEventSourceStr(source),
					GetEventTypeStr(type),
					GetEventSeverityStr(severity),
					message);

		GLDebugEventClassification eventClassification = GetEventClassification(0, type, severity);
		this->DisplayEventInfo(eventClassification, eventInfoBuffer);
	}


	void GLAPIENTRY GLDebugOutputKHR::EventCallback(
		GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const GLvoid* userParam)
	{
		GLDebugOutputKHR* debugOutputInterface = reinterpret_cast<GLDebugOutputKHR*>((void*)userParam);
		debugOutputInterface->ProcessEvent(id, source, type, severity, message);
	}

#endif


}


// #pragma clang diagnostic pop
