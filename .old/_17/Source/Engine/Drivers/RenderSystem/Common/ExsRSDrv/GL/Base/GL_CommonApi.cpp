
#include <ExsCommonGL/GL_SystemLayer.h>


namespace Exs
{


	namespace GLUtils
	{


		GLenum ChooseBufferUsageMode(GLenum bindTarget, stdx::mask<RSResourceUsageFlags> usageFlags)
		{
			GLenum usagePolicy = 0;
		
			if ((bindTarget == GL_PIXEL_PACK_BUFFER) || (bindTarget == GL_PIXEL_UNPACK_BUFFER))
			{
				usagePolicy = GL_STREAM_COPY;
			}
			else if ((bindTarget == GL_COPY_WRITE_BUFFER) || (bindTarget == GL_PIXEL_PACK_BUFFER) || (bindTarget == GL_PIXEL_UNPACK_BUFFER))
			{
				if (usageFlags.is_set(RSResourceUsage_Static))
					usagePolicy = GL_STATIC_COPY;
				else if (usageFlags.is_set(RSResourceUsage_Temporary))
					usagePolicy = GL_STREAM_COPY;
				else
					usagePolicy = GL_DYNAMIC_COPY;
			}
			else if ((bindTarget == GL_COPY_READ_BUFFER) || (bindTarget == GL_TRANSFORM_FEEDBACK_BUFFER))
			{
				if (usageFlags.is_set(RSResourceUsage_Static))
					usagePolicy = GL_STATIC_READ;
				else if (usageFlags.is_set(RSResourceUsage_Temporary))
					usagePolicy = GL_STREAM_READ;
				else
					usagePolicy = GL_DYNAMIC_READ;
			}
			else
			{
				if (usageFlags.is_set(RSResourceUsage_Static))
					usagePolicy = GL_STATIC_DRAW;
				else if (usageFlags.is_set(RSResourceUsage_Temporary))
					usagePolicy = GL_STREAM_DRAW;
				else
					usagePolicy = GL_DYNAMIC_DRAW;
			}
		
			return usagePolicy;
		}


	}


}
