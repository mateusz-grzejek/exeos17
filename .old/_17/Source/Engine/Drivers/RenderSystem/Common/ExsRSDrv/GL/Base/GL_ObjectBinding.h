
#ifndef __Exs_GraphicsDriver_CommonGL_ObjectBinding_H__
#define __Exs_GraphicsDriver_CommonGL_ObjectBinding_H__


namespace Exs
{


	enum class GLObjectBindingRestoreMode
	{
		Disabled,
		Dynamic,
		Static
	};


	template <GLObjectType>
	struct GLObjectBindingAPI;


	template <GLObjectType ObjectType, GLObjectBindingRestoreMode>
	class GLObjectBindingGuard;


	template <GLObjectType ObjectType>
	class GLObjectBindingGuard<ObjectType, GLObjectBindingRestoreMode::Disabled>
	{
	private:
		GLenum  _bindTartet;
		GLuint  _restoreHandle;

	public:
		GLObjectBindingGuard(GLenum bindTarget, GLuint restoreHandle)
		{ }

		~GLObjectBindingGuard()
		{ }
	};


	template <GLObjectType ObjectType>
	class GLObjectBindingGuard<ObjectType, GLObjectBindingRestoreMode::Dynamic>
	{
	private:
		GLenum  _bindTartet;
		GLuint  _restoreHandle;
		bool    _restore;

	public:
		GLObjectBindingGuard(GLenum bindTarget, GLuint restoreHandle, bool restore = false)
		: _bindTartet(bindTarget)
		, _restoreHandle(restoreHandle)
		, _restore(restore)
		{
			if (this->_restore && (this->_restoreHandle == GL_Invalid_Handle))
				this->_restoreHandle = GLObjectBindingAPI<ObjectType>::GetCurrent(this->_bindTartet);
		}

		~GLObjectBindingGuard()
		{
			if (this->_restore)
				GLObjectBindingAPI<ObjectType>::Bind(this->_bindTartet, this->_restoreHandle);
		}
	};


	template <GLObjectType ObjectType>
	class GLObjectBindingGuard<ObjectType, GLObjectBindingRestoreMode::Static>
	{
	private:
		GLenum  _bindTartet;
		GLuint  _restoreHandle;

	public:
		GLObjectBindingGuard(GLenum bindTarget, GLuint restoreHandle)
		: _bindTartet(bindTarget)
		, _restoreHandle(restoreHandle)
		{
			if (this->_restoreHandle == GL_Invalid_Handle)
				this->_restoreHandle = GLObjectBindingAPI<ObjectType>::GetCurrent(this->_bindTartet);
		}

		~GLObjectBindingGuard()
		{
			GLObjectBindingAPI<ObjectType>::Bind(this->_bindTartet, this->_restoreHandle);
		}
	};


	#define ExsGLSetBufferBindingClearPoint(target) \
		GLObjectBindingGuard<GLObjectType::Buffer, GLObjectBindingRestoreMode::Static> bindingGuard { target, 0 };

	#define ExsGLSetBufferBindingClearPointConditional(target, restore) \
		GLObjectBindingGuard<GLObjectType::Buffer, GLObjectBindingRestoreMode::Dynamic> bindingGuard { target, 0, restore };

	#define ExsGLSetBufferBindingRestorePoint(target) \
		GLObjectBindingGuard<GLObjectType::Buffer, GLObjectBindingRestoreMode::Static> bindingGuard { target, GL_Invalid_Handle };

	#define ExsGLSetBufferBindingRestorePointConditional(target, restore) \
		GLObjectBindingGuard<GLObjectType::Buffer, GLObjectBindingRestoreMode::Dynamic> bindingGuard { target, GL_Invalid_Handle, restore };


	#define ExsGLSetTextureBindingClearPoint(target) \
		GLObjectBindingGuard<GLObjectType::Buffer, GLObjectBindingRestoreMode::Static> bindingGuard { target, 0 };

	#define ExsGLSetTextureBindingClearPointConditional(target, restore) \
		GLObjectBindingGuard<GLObjectType::Buffer, GLObjectBindingRestoreMode::Dynamic> bindingGuard { target, 0, restore };

	#define ExsGLSetTextureBindingRestorePoint(target) \
		GLObjectBindingGuard<GLObjectType::Buffer, GLObjectBindingRestoreMode::Static> bindingGuard { target, GL_Invalid_Handle };

	#define ExsGLSetTextureBindingRestorePointConditional(target, restore) \
		GLObjectBindingGuard<GLObjectType::Buffer, GLObjectBindingRestoreMode::Dynamic> bindingGuard { target, GL_Invalid_Handle, restore };


	template <>
	struct GLObjectBindingAPI<GLObjectType::Buffer>
	{
		static void Bind(GLenum target, GLuint handle)
		{
			glBindBuffer(target, handle);
			ExsGLErrorCheck();
		}

		static GLuint GetCurrent(GLenum target)
		{
			GLint handle = 0;
			GLenum queryParam = 0;

			switch(target)
			{
			case GL_ARRAY_BUFFER:
				queryParam = GL_ARRAY_BUFFER_BINDING;
				break;

			case GL_ELEMENT_ARRAY_BUFFER:
				queryParam = GL_ELEMENT_ARRAY_BUFFER_BINDING;
				break;

			default:
				ExsDebugInterrupt();
				return GL_Invalid_Handle;
			}

			glGetIntegerv(queryParam, &handle);
			ExsGLErrorCheck();

			return (handle > 0) ? static_cast<GLuint>(handle) : GL_Invalid_Handle;
		}
	};

	template <>
	struct GLObjectBindingAPI<GLObjectType::Texture>
	{
		static void Bind(GLenum target, GLuint handle)
		{
			glBindTexture(target, handle);
			ExsGLErrorCheck();
		}

		static GLuint GetCurrent(GLenum target)
		{
			GLint handle = 0;
			GLenum queryParam = 0;

			switch(target)
			{
			case GL_TEXTURE_2D:
				queryParam = GL_TEXTURE_BINDING_2D;
				break;

			case GL_TEXTURE_3D:
				queryParam = GL_TEXTURE_BINDING_3D;
				break;

			default:
				ExsDebugInterrupt();
				return GL_Invalid_Handle;
			}

			glGetIntegerv(queryParam, &handle);
			ExsGLErrorCheck();

			return (handle > 0) ? static_cast<GLuint>(handle) : GL_Invalid_Handle;
		}
	};


}


#endif /* __Exs_GraphicsDriver_CommonGL_ObjectBinding_H__ */

