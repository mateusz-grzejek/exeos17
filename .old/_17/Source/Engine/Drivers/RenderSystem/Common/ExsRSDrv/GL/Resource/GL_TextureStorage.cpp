
#include <ExsCommonGL/Resource/GL_TextureStorage.h>


namespace Exs
{


	GLTextureStorage::GLTextureStorage( const RSResourceMemoryInfo& memoryInfo,
		const TextureDimensions& dimensions,
		GraphicDataFormat textureFormat,
		GLTextureObject* textureObject)
	: TextureStorage(memoryInfo, dimensions, textureFormat)
	, _glTextureObject(textureObject)
	, _glTextureObjectHandle(_glTextureObject->GetHandle())
	{ }


	GLTextureStorage::~GLTextureStorage()
	{ }


	bool GLTextureStorage::MapMemory(RSResourceMapMode mapMode, RSMappedMemoryInfo* mappedMemoryInfo)
	{
		return false;
	}


	bool GLTextureStorage::MapMemory(const RSMemoryRange& range, RSResourceMapMode mapMode, RSMappedMemoryInfo* mappedMemoryInfo)
	{
		return false;
	}


	void GLTextureStorage::UnmapMemory()
	{
		ExsDebugAssert( !this->IsMapped() );
	}


	Result GLTextureStorage::UpdateTexture2D(const TextureRange& range, const TextureData2DDesc& dataDesc)
	{
		GLTextureDataDesc oglDataDesc;
		oglDataDesc.dataPtr = dataDesc.dataPtr;
		oglDataDesc.dataSize = truncate_cast<GLsizei>(dataDesc.dataSize);
		oglDataDesc.pixelFormat = GLConstantMap::GetTexturePixelFormat(this->_texFormat);
		oglDataDesc.pixelType = GLConstantMap::GetTexturePixelType(this->_texFormat);

		this->_glTextureObject->UpdateSubdata2D(range.offset.x, range.offset.y, range.size.x, range.size.y, oglDataDesc);

		return ExsResult(RSC_Success);
	}


	Result GLTextureStorage::UpdateTexture3D(const TextureRange& range, const TextureData3DDesc& dataDesc)
	{
		GLTextureDataDesc oglDataDesc;
		oglDataDesc.dataPtr = dataDesc.dataPtr;
		oglDataDesc.dataSize = truncate_cast<GLsizei>(dataDesc.dataSize);
		oglDataDesc.pixelFormat = GLConstantMap::GetTexturePixelFormat(this->_texFormat);
		oglDataDesc.pixelType = GLConstantMap::GetTexturePixelType(this->_texFormat);

		this->_glTextureObject->UpdateSubdata3D(range.offset.x, range.offset.y, range.offset.z, range.size.x, range.size.y, range.size.z, oglDataDesc);

		return ExsResult(RSC_Success);
	}

	
	Result GLTextureStorage::UpdateTexture2DArray(const TextureRange& range, const TextureData2DArrayDesc& dataDesc)
	{
		GLTextureDataDesc oglDataDesc;
		oglDataDesc.pixelFormat = GLConstantMap::GetTexturePixelFormat(this->_texFormat);
		oglDataDesc.pixelType = GLConstantMap::GetTexturePixelType(this->_texFormat);

		for (Uint32 i = 0; i < dataDesc.arraySize; ++i)
		{
			GLint arrayIndex = dataDesc.arrayIndex[i];
			oglDataDesc.dataPtr = dataDesc.arrayData[i].dataPtr;
			oglDataDesc.dataSize = truncate_cast<GLsizei>(dataDesc.arrayData[i].dataSize);

			this->_glTextureObject->UpdateSubdata2DArray(arrayIndex, range.offset.x, range.offset.y, range.size.x, range.size.y, oglDataDesc);
		}

		return ExsResult(RSC_Success);
	}


	Result GLTextureStorage::UpdateTexture2DArraySubtexture(Uint32 arrayIndex, const TextureRange& range, const TextureData2DDesc& dataDesc)
	{
		GLTextureDataDesc oglDataDesc;
		oglDataDesc.dataPtr = dataDesc.dataPtr;
		oglDataDesc.dataSize = truncate_cast<GLsizei>(dataDesc.dataSize);
		oglDataDesc.pixelFormat = GLConstantMap::GetTexturePixelFormat(this->_texFormat);
		oglDataDesc.pixelType = GLConstantMap::GetTexturePixelType(this->_texFormat);

		this->_glTextureObject->UpdateSubdata2DArray(arrayIndex, range.offset.x, range.offset.y, range.size.x, range.size.y, oglDataDesc);

		return ExsResult(RSC_Success);
	}


	Result GLTextureStorage::UpdateTexture3DLayer(Uint32 layerIndex, const TextureRange& range, const TextureData2DDesc& dataDesc)
	{
		GLTextureDataDesc oglDataDesc;
		oglDataDesc.dataPtr = dataDesc.dataPtr;
		oglDataDesc.dataSize = truncate_cast<GLsizei>(dataDesc.dataSize);
		oglDataDesc.pixelFormat = GLConstantMap::GetTexturePixelFormat(this->_texFormat);
		oglDataDesc.pixelType = GLConstantMap::GetTexturePixelType(this->_texFormat);

		this->_glTextureObject->UpdateSubdata3D(range.offset.x, range.offset.y, layerIndex, range.size.x, range.size.y, 1, oglDataDesc);

		return ExsResult(RSC_Success);
	}

	
	Result GLTextureStorage::UpdateTextureCubeMapFace(CubeMapFace face, const TextureRange& range, const TextureData2DDesc& dataDesc)
	{
		GLTextureDataDesc oglDataDesc;
		oglDataDesc.dataPtr = dataDesc.dataPtr;
		oglDataDesc.dataSize = truncate_cast<GLsizei>(dataDesc.dataSize);
		oglDataDesc.pixelFormat = GLConstantMap::GetTexturePixelFormat(this->_texFormat);
		oglDataDesc.pixelType = GLConstantMap::GetTexturePixelType(this->_texFormat);

		GLuint faceIndex = static_cast<Uint32>(face) - static_cast<Uint32>(CubeMapFace::Positive_X);
		ExsDebugAssert( faceIndex < 6 );

		this->_glTextureObject->UpdateSubdataCubeMap(faceIndex, range.offset.x, range.offset.y, range.size.x, range.size.y, oglDataDesc);

		return ExsResult(RSC_Success);
	}



	RSInternalHandle<GLTextureStorage> InitGLTextureStorage2D(GLTextureInterface* textureInterface, const TextureStorageInitDesc& initDesc, const TextureData2DDesc& initData)
	{
		stdx::mask<Enum> memoryAccessMask = static_cast<Enum>(initDesc.memoryInfo.access);
		stdx::mask<Enum> usageFlagsMask = static_cast<Enum>(initDesc.usageFlags);

		auto* textureObject = textureInterface->GetGLTextureObject();
		
		GLTextureInitDesc oglInitDesc;
		oglInitDesc.width = initDesc.textureDimensions.size.x;
		oglInitDesc.height = initDesc.textureDimensions.size.y;
		oglInitDesc.depth = 1;
		oglInitDesc.arraySize = 1;
		oglInitDesc.mipmapLevelsNum = initDesc.mipmapLevelsNum;
		oglInitDesc.msaaSamplesNum = initDesc.msaaSamplesNum;
		oglInitDesc.internalFormat = GLConstantMap::GetTextureInternalFormat(initDesc.textureFormat);
		oglInitDesc.pixelFormat = GLConstantMap::GetTexturePixelFormat(initDesc.textureFormat);
		oglInitDesc.pixelType = GLConstantMap::GetTexturePixelType(initDesc.textureFormat);

		// Try to initialize texture with specified descriptor.
		if (!textureObject->InitializeTexture2D(oglInitDesc, initData))
			return nullptr;

		return CreateRSBaseObjectInterface<GLTextureStorage>(initDesc.memoryInfo, initDesc.textureDimensions, initDesc.textureFormat, textureObject);
	}


	RSInternalHandle<GLTextureStorage> InitGLTextureStorage2DArray(GLTextureInterface* textureInterface, const TextureStorageInitDesc& initDesc, const TextureData2DArrayDesc& initData)
	{
		stdx::mask<Enum> memoryAccessMask = static_cast<Enum>(initDesc.memoryInfo.access);
		stdx::mask<Enum> usageFlagsMask = static_cast<Enum>(initDesc.usageFlags);

		auto* textureObject = textureInterface->GetGLTextureObject();
		
		GLTextureInitDesc oglInitDesc;
		oglInitDesc.width = initDesc.textureDimensions.size.x;
		oglInitDesc.height = initDesc.textureDimensions.size.y;
		oglInitDesc.depth = 1;
		oglInitDesc.arraySize = initDesc.textureDimensions.arraySize;
		oglInitDesc.mipmapLevelsNum = initDesc.mipmapLevelsNum;
		oglInitDesc.msaaSamplesNum = 0;
		oglInitDesc.internalFormat = GLConstantMap::GetTextureInternalFormat(initDesc.textureFormat);
		oglInitDesc.pixelFormat = GLConstantMap::GetTexturePixelFormat(initDesc.textureFormat);
		oglInitDesc.pixelType = GLConstantMap::GetTexturePixelType(initDesc.textureFormat);

		// Try to initialize texture with specified descriptor.
		if (!textureObject->InitializeTexture2DArray(oglInitDesc, initData))
			return nullptr;
		
		return CreateRSBaseObjectInterface<GLTextureStorage>(initDesc.memoryInfo, initDesc.textureDimensions, initDesc.textureFormat, textureObject);
	}


	RSInternalHandle<GLTextureStorage> InitGLTextureStorage2DMultisample(GLTextureInterface* textureInterface, const TextureStorageInitDesc& initDesc)
	{
		stdx::mask<Enum> memoryAccessMask = static_cast<Enum>(initDesc.memoryInfo.access);
		stdx::mask<Enum> usageFlagsMask = static_cast<Enum>(initDesc.usageFlags);

		auto* textureObject = textureInterface->GetGLTextureObject();
		
		GLTextureInitDesc oglInitDesc;
		oglInitDesc.width = initDesc.textureDimensions.size.x;
		oglInitDesc.height = initDesc.textureDimensions.size.y;
		oglInitDesc.depth = 1;
		oglInitDesc.arraySize = 1;
		oglInitDesc.mipmapLevelsNum = 1;
		oglInitDesc.msaaSamplesNum = initDesc.msaaSamplesNum;
		oglInitDesc.internalFormat = GLConstantMap::GetTextureInternalFormat(initDesc.textureFormat);
		oglInitDesc.pixelFormat = GLConstantMap::GetTexturePixelFormat(initDesc.textureFormat);
		oglInitDesc.pixelType = GLConstantMap::GetTexturePixelType(initDesc.textureFormat);

		// Try to initialize texture with specified descriptor.
		if (!textureObject->InitializeTexture2DMultisample(oglInitDesc))
			return nullptr;
		
		return CreateRSBaseObjectInterface<GLTextureStorage>(initDesc.memoryInfo, initDesc.textureDimensions, initDesc.textureFormat, textureObject);
	}


	RSInternalHandle<GLTextureStorage> InitGLTextureStorage3D(GLTextureInterface* textureInterface, const TextureStorageInitDesc& initDesc, const TextureData3DDesc& initData)
	{
		stdx::mask<Enum> memoryAccessMask = static_cast<Enum>(initDesc.memoryInfo.access);
		stdx::mask<Enum> usageFlagsMask = static_cast<Enum>(initDesc.usageFlags);

		auto* textureObject = textureInterface->GetGLTextureObject();
		
		GLTextureInitDesc oglInitDesc;
		oglInitDesc.width = initDesc.textureDimensions.size.x;
		oglInitDesc.height = initDesc.textureDimensions.size.y;
		oglInitDesc.depth = initDesc.textureDimensions.depth;
		oglInitDesc.arraySize = initDesc.textureDimensions.arraySize;
		oglInitDesc.mipmapLevelsNum = initDesc.mipmapLevelsNum;
		oglInitDesc.msaaSamplesNum = 0;
		oglInitDesc.internalFormat = GLConstantMap::GetTextureInternalFormat(initDesc.textureFormat);
		oglInitDesc.pixelFormat = GLConstantMap::GetTexturePixelFormat(initDesc.textureFormat);
		oglInitDesc.pixelType = GLConstantMap::GetTexturePixelType(initDesc.textureFormat);

		// Try to initialize texture with specified descriptor.
		if (!textureObject->InitializeTexture3D(oglInitDesc, initData))
			return nullptr;
		
		return CreateRSBaseObjectInterface<GLTextureStorage>(initDesc.memoryInfo, initDesc.textureDimensions, initDesc.textureFormat, textureObject);
	}


	RSInternalHandle<GLTextureStorage> InitGLTextureStorageCubeMap(GLTextureInterface* textureInterface, const TextureStorageInitDesc& initDesc, const TextureDataCubeMapDesc& initData)
	{
		stdx::mask<Enum> memoryAccessMask = static_cast<Enum>(initDesc.memoryInfo.access);
		stdx::mask<Enum> usageFlagsMask = static_cast<Enum>(initDesc.usageFlags);

		auto* textureObject = textureInterface->GetGLTextureObject();
		
		GLTextureInitDesc oglInitDesc;
		oglInitDesc.width = initDesc.textureDimensions.size.x;
		oglInitDesc.height = initDesc.textureDimensions.size.y;
		oglInitDesc.depth = 1;
		oglInitDesc.arraySize = 1;
		oglInitDesc.mipmapLevelsNum = initDesc.mipmapLevelsNum;
		oglInitDesc.msaaSamplesNum = 0;
		oglInitDesc.internalFormat = GLConstantMap::GetTextureInternalFormat(initDesc.textureFormat);
		oglInitDesc.pixelFormat = GLConstantMap::GetTexturePixelFormat(initDesc.textureFormat);
		oglInitDesc.pixelType = GLConstantMap::GetTexturePixelType(initDesc.textureFormat);

		// Try to initialize texture with specified descriptor.
		if (!textureObject->InitializeTextureCubeMap(oglInitDesc, initData))
			return nullptr;
		
		return CreateRSBaseObjectInterface<GLTextureStorage>(initDesc.memoryInfo, initDesc.textureDimensions, initDesc.textureFormat, textureObject);
	}


}
