
#ifndef __Exs_GraphicsDriver_CommonGL_RSContextCommandQueue_H__
#define __Exs_GraphicsDriver_CommonGL_RSContextCommandQueue_H__

#include "GL_Prerequisites.h"
#include <ExsRenderSystem/RSContextCommandQueue.h>


namespace Exs
{


	class GLRSContextStateController;
	class GLRSThreadState;
	class GLSystemContext;


	class GLRSContextCommandQueue : public RSContextCommandQueue
	{
	protected:
		GLSystemContext*  _glSystemContext;

	public:
		GLRSContextCommandQueue(GLRenderSystem* renderSystem, GLRSThreadState* rsThreadState, GLRSContextStateController* rsContextStateController);
		virtual ~GLRSContextCommandQueue();
		
		virtual void SetConstantBuffer(Uint32 cbufferIndex, stdx::mask<ShaderStageFlags> stageAccess, const ConstantBufferHandle& constantBuffer) override final;
		virtual void SetConstantBufferBindingCacheTable(const ConstantBufferBindingCacheTableHandle& cbufferBindingCacheTable) override final;
		
		virtual void SetSampler(Uint32 resourceIndex, stdx::mask<ShaderStageFlags> stageAccess, const SamplerHandle& sampler) override final;
		virtual void SetSamplerBindingCacheTable(const SamplerBindingCacheTableHandle& samplerBindingCacheTable) override final;
		
		virtual void SetShaderResource(Uint32 resourceIndex, stdx::mask<ShaderStageFlags> stageAccess, const ShaderResourceViewHandle& resourceView) override final;
		virtual void SetShaderResourceBindingCacheTable(const ShaderResourceBindingCacheTableHandle& shaderResourceBindingCacheTable) override final;

		virtual void Clear(stdx::mask<RenderTargetBufferFlags> bufferMask) override final;
		virtual void ClearColorBuffer() override final;

		virtual void Present() override final;
	};


}



#endif /* __Exs_GraphicsDriver_CommonGL_RSContextCommandQueue_H__ */
