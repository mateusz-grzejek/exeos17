
#include <ExsCommonGL/State/GL_RasterizerState.h>
#include <ExsCommonGL/GL_RenderSystem.h>


namespace Exs
{


	bool TranslateGLRasterizerConfiguration(const RasterizerConfiguration& rasterizerConfig, GLRasterizerConfiguration* translatedGLConfig)
	{
		ExsDebugAssert( translatedGLConfig != nullptr );
		
		translatedGLConfig->scissorTestState = (rasterizerConfig.scissorTestState == ActiveState::Enabled);

		translatedGLConfig->settings.faceCullMode = GLConstantMap::GetCullMode(rasterizerConfig.settings.faceCullMode);
		translatedGLConfig->settings.frontFaceOrder = GLConstantMap::GetVerticesOrder(rasterizerConfig.settings.frontFaceOrder);

	#if ( EXS_GL_FEATURE_POLYGON_RENDERING_MODE )
		translatedGLConfig->settings.triangleFillMode = GLConstantMap::GetFillMode(rasterizerConfig.settings.triangleFillMode);
	#endif

		return true;
	}


}
