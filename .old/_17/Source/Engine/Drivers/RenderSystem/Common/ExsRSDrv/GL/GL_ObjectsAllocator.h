
#ifndef __Exs_GraphicsDriver_CommonGL_ObjectsAllocator_H__
#define __Exs_GraphicsDriver_CommonGL_ObjectsAllocator_H__

#include "GL_Prerequisites.h"


namespace Exs
{
	

	template <GLObjectType>
	struct GLObjectsAllocatorTraits;


	template <GLObjectType Object_type>
	class GLObjectsAllocatorSubmodule
	{
	public:
		// typedef GLObjectsAllocatorTraits<Object_type> Traits;
		// typedef DynamicDataArray<GLuint> AllocatedObjects;
		// typedef Stack<GLuint> ReleaseQueue;
		
	private:
		// AllocatedObjects    _allocatedObjects;
		// ReleaseQueue        _releaseQueue;
		// Size_t              _releaseQueueLimit;

	public:
		GLObjectsAllocatorSubmodule()
		{ }

		~GLObjectsAllocatorSubmodule()
		{ }

		GLuint Allocate()
		{ }

		void Deallocate(GLuint handle)
		{
		}

		void ReleaseQueuedObject()
		{
		}

		void SetReleaseQueueLimit(Size_t limit)
		{
		}

		void ResetState()
		{
		}
	};


	class GLObjectsAllocator
	{
	public:
		GLObjectsAllocator()
		{ }

		~GLObjectsAllocator()
		{ }

		static GLuint AllocateBuffer()
		{
			GLuint handle = GL_Invalid_Handle;
			glGenBuffers(1, &handle);
			ExsGLErrorCheck();
			return handle;
		}

		static GLuint AllocateFramebuffer()
		{
			GLuint handle = GL_Invalid_Handle;
			glGenFramebuffers(1, &handle);
			ExsGLErrorCheck();
			return handle;
		}
		
	#if ( EXS_GL_FEATURE_PROGRAM_PIPELINE_OBJECT )

		static GLuint AllocatePipelineState()
		{
			GLuint handle = GL_Invalid_Handle;
			glGenProgramPipelines(1, &handle);
			ExsGLErrorCheck();
			return handle;
		}

	#endif

		static GLuint AllocateRenderbuffer()
		{
			GLuint handle = GL_Invalid_Handle;
			glGenRenderbuffers(1, &handle);
			ExsGLErrorCheck();
			return handle;
		}

		static GLuint AllocateSampler()
		{
			GLuint handle = GL_Invalid_Handle;
			glGenSamplers(1, &handle);
			ExsGLErrorCheck();
			return handle;
		}

		static GLuint AllocateShader(GLenum shaderType)
		{
			GLuint handle = glCreateShader(shaderType);
			ExsGLErrorCheck();
			return handle;
		}

		static GLuint AllocateShaderProgram()
		{
			GLuint handle = glCreateProgram();
			ExsGLErrorCheck();
			return handle;
		}

		static GLuint AllocateTexture()
		{
			GLuint handle = GL_Invalid_Handle;
			glGenTextures(1, &handle);
			ExsGLErrorCheck();
			return handle;
		}

		static GLuint AllocateTransformFeedback()
		{
			GLuint handle = GL_Invalid_Handle;
			glGenTransformFeedbacks(1, &handle);
			ExsGLErrorCheck();
			return handle;
		}

		static GLuint AllocateVertexArray()
		{
			GLuint handle = GL_Invalid_Handle;
			glGenVertexArrays(1, &handle);
			ExsGLErrorCheck();
			return handle;
		}

		static void DeallocateBuffer(GLuint handle)
		{
			glDeleteBuffers(1, &handle);
			ExsGLErrorCheck();
		}

		static void DeallocateFramebuffer(GLuint handle)
		{
			glDeleteFramebuffers(1, &handle);
			ExsGLErrorCheck();
		}
		
	#if ( EXS_GL_FEATURE_PROGRAM_PIPELINE_OBJECT )

		static void DeallocatePipelineState(GLuint handle)
		{
			glDeleteProgramPipelines(1, &handle);
			ExsGLErrorCheck();
		}

	#endif

		static void DeallocateRenderbuffer(GLuint handle)
		{
			glDeleteRenderbuffers(1, &handle);
			ExsGLErrorCheck();
		}

		static void DeallocateSampler(GLuint handle)
		{
			glDeleteSamplers(1, &handle);
			ExsGLErrorCheck();
		}

		static void DeallocateShader(GLuint handle)
		{
			glDeleteShader(handle);
			ExsGLErrorCheck();
		}

		static void DeallocateShaderProgram(GLuint handle)
		{
			glDeleteProgram(handle);
			ExsGLErrorCheck();
		}

		static void DeallocateTexture(GLuint handle)
		{
			glDeleteTextures(1, &handle);
			ExsGLErrorCheck();
		}

		static void DeallocateTransformFeedback(GLuint handle)
		{
			glDeleteTransformFeedbacks(1, &handle);
			ExsGLErrorCheck();
		}

		static void DeallocateVertexArray(GLuint handle)
		{
			glDeleteVertexArrays(1, &handle);
			ExsGLErrorCheck();
		}
	};


	template <>
	struct GLObjectsAllocatorTraits<GLObjectType::Buffer>
	{
		static void Allocate(Size_t count, GLuint* buffers)
		{
			glGenBuffers(static_cast<GLsizei>(count), buffers);
			ExsGLErrorCheck();
		}
		
		static void Deallocate(Size_t count, GLuint* buffers)
		{
			glDeleteBuffers(static_cast<GLsizei>(count), buffers);
			ExsGLErrorCheck();
		}
	};

	template <>
	struct GLObjectsAllocatorTraits<GLObjectType::Framebuffer>
	{
		static void Allocate(Size_t count, GLuint* framebuffers)
		{
			glGenFramebuffers(static_cast<GLsizei>(count), framebuffers);
			ExsGLErrorCheck();
		}
		
		static void Deallocate(Size_t count, GLuint* framebuffers)
		{
			glDeleteFramebuffers(static_cast<GLsizei>(count), framebuffers);
			ExsGLErrorCheck();
		}
	};
	
#if ( EXS_GL_FEATURE_PROGRAM_PIPELINE_OBJECT )

	template <>
	struct GLObjectsAllocatorTraits<GLObjectType::Pipeline_State>
	{
		static void Allocate(Size_t count, GLuint* pipelines)
		{
			glGenProgramPipelines(static_cast<GLsizei>(count), pipelines);
			ExsGLErrorCheck();
		}
		
		static void Deallocate(Size_t count, GLuint* pipelines)
		{
			glDeleteProgramPipelines(static_cast<GLsizei>(count), pipelines);
			ExsGLErrorCheck();
		}
	};

#endif

	template <>
	struct GLObjectsAllocatorTraits<GLObjectType::Renderbuffer>
	{
		static void Allocate(Size_t count, GLuint* renderbuffers)
		{
			glGenRenderbuffers(static_cast<GLsizei>(count), renderbuffers);
			ExsGLErrorCheck();
		}
		
		static void Deallocate(Size_t count, GLuint* renderbuffers)
		{
			glDeleteRenderbuffers(static_cast<GLsizei>(count), renderbuffers);
			ExsGLErrorCheck();
		}
	};

	template <>
	struct GLObjectsAllocatorTraits<GLObjectType::Sampler>
	{
		static void Allocate(Size_t count, GLuint* samplers)
		{
			glGenSamplers(static_cast<GLsizei>(count), samplers);
			ExsGLErrorCheck();
		}
		
		static void Deallocate(Size_t count, GLuint* samplers)
		{
			glDeleteSamplers(static_cast<GLsizei>(count), samplers);
			ExsGLErrorCheck();
		}
	};

	template <>
	struct GLObjectsAllocatorTraits<GLObjectType::Texture>
	{
		static void Allocate(Size_t count, GLuint* textures)
		{
			glGenTextures(static_cast<GLsizei>(count), textures);
			ExsGLErrorCheck();
		}
		
		static void Deallocate(Size_t count, GLuint* textures)
		{
			glDeleteTextures(static_cast<GLsizei>(count), textures);
			ExsGLErrorCheck();
		}
	};

	template <>
	struct GLObjectsAllocatorTraits<GLObjectType::Transform_Feedback>
	{
		static void Allocate(Size_t count, GLuint* transformFeedbacks)
		{
			glGenTransformFeedbacks(static_cast<GLsizei>(count), transformFeedbacks);
			ExsGLErrorCheck();
		}
		
		static void Deallocate(Size_t count, GLuint* transformFeedbacks)
		{
			glDeleteTransformFeedbacks(static_cast<GLsizei>(count), transformFeedbacks);
			ExsGLErrorCheck();
		}
	};

	template <>
	struct GLObjectsAllocatorTraits<GLObjectType::Vertex_Array>
	{
		static void Allocate(Size_t count, GLuint* vertexArrays)
		{
			glGenVertexArrays(static_cast<GLsizei>(count), vertexArrays);
			ExsGLErrorCheck();
		}
		
		static void Deallocate(Size_t count, GLuint* vertexArrays)
		{
			glGenVertexArrays(static_cast<GLsizei>(count), vertexArrays);
			ExsGLErrorCheck();
		}
	};


}


#endif /* __Exs_GraphicsDriver_CommonGL_ObjectsAllocator_H__ */
