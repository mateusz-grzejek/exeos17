
#include <ExsCommonGL/Memory/GL_DeviceMemory.h>


namespace Exs
{


	GLDeviceMemoryPool::GLDeviceMemoryPool(RSMemorySystem* memorySystem, Uint64 size, const DeviceMemoryPoolProperties& properties)
	: DeviceMemoryPool(memorySystem, size, properties)
	, _currentAllocOffset(0)
	{ }


	GLDeviceMemoryPool::~GLDeviceMemoryPool()
	{ }


	Result GLDeviceMemoryPool::Reset()
	{
		// Fetch current value of the alloc offset. Do not perform any actions if it is 0.
		// That means, that no memory was allocated from this device pool.
		RS_memory_size_t allocOffset = this->_currentAllocOffset.load(std::memory_order_acquire);

		if (allocOffset > 0)
		{
			ExsEnterCriticalSection(this->_accessLock);
			{
				// this->_memorySystem->InvalidateDeviceHeap(this); ---> ???
				this->_currentAllocOffset.store(0, std::memory_order_release);
			}
			ExsLeaveCriticalSection();
		}

		return ExsResult(RSC_Success);
	}


	bool GLDeviceMemoryPool::AllocateMemory(RS_memory_size_t memSize, DeviceMemoryBlock** deviceMemory)
	{
		ExsDebugAssert( deviceMemory != nullptr );

		//
		RS_memory_size_t allocatedMemoryOffset = RS_Memory_Invalid_Offset;

		// Cannot allocate more memory than available in the pool.
		if (memSize < this->_size)
		{
			// This is the max value of _currentAllocOffset which allow to allocate
			// requested amount of memory. If it is reached, there is not enough memory.
			Uint64 maxOffset = this->_size - memSize;

			for (RS_memory_size_t allocOffset = this->_currentAllocOffset.load(std::memory_order_relaxed); ; )
			{
				if (allocOffset > maxOffset)
					return false;

				if (this->_currentAllocOffset.compare_exchange_strong(allocOffset, allocOffset + memSize, std::memory_order_acq_rel, std::memory_order_relaxed);)
				{
					allocatedMemoryOffset = allocOffset;
					break;
				}
			}
		}

		//
		*deviceMemory = this->RegisterAllocation(allocatedMemoryOffset, memSize);

		return true;
	}


}
