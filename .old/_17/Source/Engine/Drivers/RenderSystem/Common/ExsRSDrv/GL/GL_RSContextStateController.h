
#ifndef __Exs_GraphicsDriver_CommonGL_RSContextStateController_H__
#define __Exs_GraphicsDriver_CommonGL_RSContextStateController_H__

#include "GL_Prerequisites.h"
#include "State/GL_BlendState.h"
#include "State/GL_DepthStencilState.h"
#include "State/GL_RasterizerState.h"
#include <ExsRenderSystem/RSContextStateController.h>


namespace Exs
{

	
	class GLShaderProgramObject;
	class GLVertexArrayObject;
	class GLVertexArrayObjectCache;


	class GLRSContextStateController : public RSContextStateController
	{
		friend class GLRSContextCommandQueue;

	protected:
		struct CurrentConfig
		{
			GLBlendConfiguration blend;
			GLDepthStencilConfiguration depthStencil;
			GLRasterizerConfiguration rasterizer;
		};

	protected:
		CurrentConfig                _glCurrentConfig;
		GLVertexArrayObjectCache*    _glVertexArrayObjectCache;

	public:
		GLRSContextStateController(GLRenderSystem* renderSystem, RSThreadState* rsThreadState, GLVertexArrayObjectCache* vertexArrayObjectCache);
		virtual ~GLRSContextStateController();

		virtual bool SetClearColor(const Color& color) override;
		virtual bool SetDepthRange(const DepthRange& depthRange) override;
		virtual bool SetScissorRect(const RectU32& scissorRect) override;
		virtual bool SetViewport(const Viewport& viewport) override;

	protected:
		static void BindShaderProgramObject(const GLShaderProgramObject* shaderProgramObject);
		static void BindVertexArrayObject(const GLVertexArrayObject* vertexArrayObject);

	private:
		virtual void UpdateBlendState(BlendStateDescriptor* blendStateDescriptor) override;
		virtual void UpdateDepthStencilState(DepthStencilStateDescriptor* depthStencilStateDescriptor) override;
		virtual void UpdateRasterizerState(RasterizerStateDescriptor* rasterizerStateDescriptor) override;
		
		static void UpdateBlendConfig(const GLBlendConfiguration& blendConfig);
		static void UpdateDepthStencilConfig(const GLDepthStencilConfiguration& depthStencilConfig);
		static void UpdateRasterizerConfig(const GLRasterizerConfiguration& rasterizerConfig);

		static void UpdateBlendConfig(const GLBlendConfiguration& blendConfig, const GLBlendConfiguration& currentBlendConfig);
		static void UpdateDepthStencilConfig(const GLDepthStencilConfiguration& depthStencilConfig, const GLDepthStencilConfiguration& currentDepthStencilConfig);
		static void UpdateRasterizerConfig(const GLRasterizerConfiguration& rasterizerConfig, const GLRasterizerConfiguration& currentRasterizerConfig);
	};


};


#endif /* __Exs_GraphicsDriver_CommonGL_RSContextStateController_H__ */
