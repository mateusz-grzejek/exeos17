
#ifndef __Exs_GraphicsDriver_CommonGL_RenderSystem_H__
#define __Exs_GraphicsDriver_CommonGL_RenderSystem_H__

#include "Memory/GL_RSMemorySystem.h"
#include "State/GL_VertexArrayObjectCache.h"
#include <ExsRenderSystem/RenderSystem.h>


namespace Exs
{


	class GLExtensionManager;
	class GLGraphicsDriver;


	class GLRenderSystem : public RenderSystem
	{
		EXS_DECLARE_NONCOPYABLE(GLRenderSystem);

	protected:
		GLSystemContext*            _glSystemContext;
		GLExtensionManager*         _glExtensionManager;
		GLVertexArrayObjectCache    _glVertexArrayObjectCache;
		GLRSMemorySystem            _glMemorySystem;

	public:
		GLRenderSystem(GLGraphicsDriver* graphicDriver, GLSystemContext* systemContext, GLExtensionManager* extensionManager);
		virtual ~GLRenderSystem();
		
		virtual SamplerHandle CreateSampler(const SamplerCreateInfo& createInfo) override final;
		
		virtual ConstantBufferBindingCacheTableHandle CreateConstantBufferBindingCacheTable(const ConstantBufferBinding* bindings, Uint32 bindingsNum) override final;
		virtual SamplerBindingCacheTableHandle CreateSamplerBindingCacheTable(const SamplerBinding* bindings, Uint32 bindingsNum) override final;
		virtual ShaderResourceBindingCacheTableHandle CreateShaderResourceBindingCacheTable(const ShaderResourceBinding* bindings, Uint32 bindingsNum) override final;

		virtual ConstantBufferHandle CreateConstantBuffer(const ConstantBufferCreateInfo& createInfo) override;
		virtual IndexBufferHandle CreateIndexBuffer(const IndexBufferCreateInfo& createInfo) override;
		virtual VertexBufferHandle CreateVertexBuffer(const VertexBufferCreateInfo& createInfo) override;
		
		virtual Texture2DHandle CreateTexture2D(const Texture2DCreateInfo& createInfo) override;
		virtual Texture3DHandle CreateTexture3D(const Texture3DCreateInfo& createInfo) override;
		virtual Texture2DArrayHandle CreateTexture2DArray(const Texture2DArrayCreateInfo& createInfo) override;
		
		virtual ShaderResourceViewHandle CreateTextureShaderResourceView(const Texture2DHandle& texture2D) override;
		virtual ShaderResourceViewHandle CreateTextureShaderResourceView(const Texture2DArrayHandle& texture2DArray) override;

		virtual BlendStateDescriptorHandle CreateBlendStateDescriptor(const BlendConfiguration& configuration) override;
		virtual DepthStencilStateDescriptorHandle CreateDepthStencilStateDescriptor(const DepthStencilConfiguration& configuration) override;
		virtual RasterizerStateDescriptorHandle CreateRasterizerStateDescriptor(const RasterizerConfiguration& configuration) override;
		virtual VertexStreamStateDescriptorHandle CreateVertexStreamStateDescriptor(const VertexStreamConfiguration& configuration) override;

		bool InitializeMemorySystem();

		GLSystemContext* GetGLSystemContext();

		GLExtensionManager* GetGLExtensionManager();

		GLVertexArrayObjectCache* GetGLVertexArrayObjectCache();
	};


	inline GLSystemContext* GLRenderSystem::GetGLSystemContext()
	{
		return this->_glSystemContext;
	}

	inline GLExtensionManager* GLRenderSystem::GetGLExtensionManager()
	{
		return this->_glExtensionManager;
	}

	inline GLVertexArrayObjectCache* GLRenderSystem::GetGLVertexArrayObjectCache()
	{
		return &(this->_glVertexArrayObjectCache);
	}


}


#endif /* __Exs_GraphicsDriver_CommonGL_RenderSystem_H__ */
