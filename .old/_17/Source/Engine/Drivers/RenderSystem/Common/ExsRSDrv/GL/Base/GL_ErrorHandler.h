
#ifndef __Exs_GraphicsDriver_CommonGL_ErrorHandler_H__
#define __Exs_GraphicsDriver_CommonGL_ErrorHandler_H__


#if !defined( EXS_CONFIG_GL_ENABLE_EXPLICIT_ERROR_CHECKS )
#
#  if ( EXS_GL_TARGET_VERSION == EXS_GL_TARGET_VERSION_GL3 )
#    define EXS_CONFIG_GL_ENABLE_EXPLICIT_ERROR_CHECKS 1
#  elif ( EXS_GL_TARGET_VERSION == EXS_GL_TARGET_VERSION_GL4 )
#    define EXS_CONFIG_GL_ENABLE_EXPLICIT_ERROR_CHECKS 0
#  elif ( EXS_GL_TARGET_VERSION == EXS_GL_TARGET_VERSION_GLES2 )
#    define EXS_CONFIG_GL_ENABLE_EXPLICIT_ERROR_CHECKS 1
#  elif ( EXS_GL_TARGET_VERSION == EXS_GL_TARGET_VERSION_GLES3 )
#    define EXS_CONFIG_GL_ENABLE_EXPLICIT_ERROR_CHECKS 1
#  endif
#
#endif


#if !defined( EXS_CONFIG_GL_ENABLE_EXPLICIT_ERROR_CHECKS_NON_DEBUG )
#  define EXS_CONFIG_GL_ENABLE_EXPLICIT_ERROR_CHECKS_NON_DEBUG 0
#endif


namespace Exs
{


	class GLErrorHandler
	{
	public:
		static void ClearErrorStack();
		static void CheckLastError();
		static bool CheckLastError(GLenum errorCode);

	private:
		static const char* TranslateErrorCode(GLenum errorCode);
	};


#if ( EXS_GL_SYSTEM_LAYER_TYPE == EXS_GL_SYSTEM_LAYER_TYPE_EGL )

	class EGLErrorHandler
	{
	public:
		static void CheckLastError();
		static bool CheckLastError(EGLint errorCode);

	private:
		static const char* TranslateErrorCode(EGLint errorCode);
	};

#endif


}


#if ( EXS_CONFIG_GL_ENABLE_EXPLICIT_ERROR_CHECKS && (EXS_CONFIG_BASE_ENABLE_DEBUG || EXS_CONFIG_GL_ENABLE_EXPLICIT_ERROR_CHECKS_NON_DEBUG) )
#
#  if ( EXS_GL_SYSTEM_LAYER_TYPE == EXS_GL_SYSTEM_LAYER_TYPE_EGL )
#    define ExsEGLErrorCheck()  EGLErrorHandler::CheckLastError()
#  endif
#
#  define ExsGLErrorCheck()       GLErrorHandler::CheckLastError()
#  define ExsGLErrorClearState()  GLErrorHandler::ClearErrorStack()
#
#else
#
#  if ( EXS_GL_SYSTEM_LAYER_TYPE == EXS_GL_SYSTEM_LAYER_TYPE_EGL )
#    define ExsEGLErrorCheck()
#  endif
#
#  define ExsGLErrorCheck()
#  define ExsGLErrorClearState()
#
#endif


#endif /* __Exs_GraphicsDriver_CommonGL_ErrorHandler_H__ */
