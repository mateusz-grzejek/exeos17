
#include <ExsCommonGL/State/GL_DepthStencilState.h>
#include <ExsCommonGL/GL_RenderSystem.h>


namespace Exs
{


	bool TranslateGLDepthStencilConfiguration(const DepthStencilConfiguration& depthStencilConfig, GLDepthStencilConfiguration* translatedGLConfig)
	{
		ExsDebugAssert( translatedGLConfig != nullptr );

		auto& depthSettings = depthStencilConfig.depthSettings;
		auto& stencilSettings = depthStencilConfig.stencilSettings;
		
		translatedGLConfig->depthTestState = (depthStencilConfig.depthTestState == ActiveState::Enabled);
		translatedGLConfig->stencilTestState = (depthStencilConfig.stencilTestState == ActiveState::Enabled);

		if (depthStencilConfig.depthTestState == ActiveState::Enabled)
		{
			translatedGLConfig->depthSettings.comparisonFunc = GLConstantMap::GetComparisonFunction(depthSettings.comparisonFunc);
			translatedGLConfig->depthSettings.writeMask = GLConstantMap::GetDepthWriteMask(depthSettings.writeMask);
		}

		if (depthStencilConfig.stencilTestState == ActiveState::Enabled)
		{
			translatedGLConfig->stencilSettings.comparisonFunc.backFace = GLConstantMap::GetComparisonFunction(stencilSettings.backFace.comparisonFunc);
			translatedGLConfig->stencilSettings.comparisonFunc.frontFace = GLConstantMap::GetComparisonFunction(stencilSettings.frontFace.comparisonFunc);
			translatedGLConfig->stencilSettings.comparisonFunc.refMask = stdx::limits<GLuint>::max_value;
			translatedGLConfig->stencilSettings.comparisonFunc.refValue = static_cast<GLint>(stencilSettings.refValue);

			translatedGLConfig->stencilSettings.opBackFace.stencilFail = GLConstantMap::GetStencilOp(stencilSettings.backFace.opStencilFail);
			translatedGLConfig->stencilSettings.opBackFace.stencilPassDepthFail = GLConstantMap::GetStencilOp(stencilSettings.backFace.opStencilPassDepthFail);
			translatedGLConfig->stencilSettings.opBackFace.stencilPassDepthPass = GLConstantMap::GetStencilOp(stencilSettings.backFace.opStencilPassDepthPass);

			translatedGLConfig->stencilSettings.opFrontFace.stencilFail = GLConstantMap::GetStencilOp(stencilSettings.frontFace.opStencilFail);
			translatedGLConfig->stencilSettings.opFrontFace.stencilPassDepthFail = GLConstantMap::GetStencilOp(stencilSettings.frontFace.opStencilPassDepthFail);
			translatedGLConfig->stencilSettings.opFrontFace.stencilPassDepthPass = GLConstantMap::GetStencilOp(stencilSettings.frontFace.opStencilPassDepthPass);

			translatedGLConfig->stencilSettings.writeMask = static_cast<GLint>(depthStencilConfig.stencilSettings.writeMask);
		}

		return true;
	}


}
