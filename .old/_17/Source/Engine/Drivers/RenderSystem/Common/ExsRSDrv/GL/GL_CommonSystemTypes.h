
#ifndef __Exs_GraphicsDriver_CommonGL_CommonSystemTypes_H__
#define __Exs_GraphicsDriver_CommonGL_CommonSystemTypes_H__

#include "GL_Prerequisites.h"
#include <ExsCore/System/CommonSystemTypes.h>


namespace Exs
{

	
#if ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_WIN32 )
	struct Win32GLSystemContextData;
	typedef Win32GLSystemContextData GLSystemContextData;
#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_WP81 )
#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_LINUX )
	struct LinuxGLSystemContextDataGLX;
	typedef LinuxGLSystemContextDataGLX GLSystemContextData;
#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_ANDROID )
#endif


};


#if ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_WIN32 )
#  include "Specific/Win32/Win32_GL_CommonSystemTypes.h"
#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_WP81 )
#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_LINUX )
#  if ( EXS_GL_SYSTEM_LAYER_TYPE == EXS_GL_SYSTEM_LAYER_TYPE_EGL )
#    include "Specific/Linux/Linux_GL_CommonSystemTypes_EGL.h"
#  else
#    include "Specific/Linux/Linux_GL_CommonSystemTypes_GLX.h"
#  endif
#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_ANDROID )
#endif


#endif /* __Exs_GraphicsDriver_CommonGL_CommonSystemTypes_H__ */
