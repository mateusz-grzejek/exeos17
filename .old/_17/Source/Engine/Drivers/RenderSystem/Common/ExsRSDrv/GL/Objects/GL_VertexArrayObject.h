
#ifndef __Exs_GraphicsDriver_CommonGL_VertexArrayObject_H__
#define __Exs_GraphicsDriver_CommonGL_VertexArrayObject_H__

#include "../GL_Prerequisites.h"


namespace Exs
{


	class GLVertexArrayObject : public GLObject
	{
		EXS_DECLARE_NONCOPYABLE(GLVertexArrayObject);

		friend class GLRenderSystem;

	public:
		GLVertexArrayObject();
		virtual ~GLVertexArrayObject();

		virtual bool Release() override;
		virtual bool ValidateHandle() const override;
	};


}


#endif /* __Exs_GraphicsDriver_CommonGL_VertexArrayObject_H__ */
