
#ifndef __Exs_GraphicsDriver_CommonGL_SamplerInterface_H__
#define __Exs_GraphicsDriver_CommonGL_SamplerInterface_H__

#include "../Objects/GL_SamplerObject.h"


namespace Exs
{


	struct SamplerParameters;


	///<summary>
	///</summary>
	class GLSamplerInterface : public RSBaseObjectInterface
	{
	private:
		std::unique_ptr<GLSamplerObject>  _glSamplerObject;
		GLuint                            _glSamplerObjectHandle;

	public:
		GLSamplerInterface(GLSamplerInterface&&) = default;
		GLSamplerInterface(const GLSamplerInterface&) = default;

		GLSamplerInterface& operator=(GLSamplerInterface&&) = default;
		GLSamplerInterface& operator=(const GLSamplerInterface&) = default;

		GLSamplerInterface(std::unique_ptr<GLSamplerObject>&& samplerObject)
		: RSBaseObjectInterface()
		, _glSamplerObject(std::move(samplerObject))
		, _glSamplerObjectHandle(_glSamplerObject->GetHandle())
		{ }

		virtual ~GLSamplerInterface()
		{ }

		GLSamplerObject* GetGLSamplerObject() const
		{
			return this->_glSamplerObject.get();
		}

		GLuint GetGLSamplerObjectHandle() const
		{
			return this->_glSamplerObjectHandle;
		}
	};


	RSInternalHandle<GLSamplerInterface> InitializeGLSamplerInterface(const SamplerParameters& parameters);


}


#endif /* __Exs_GraphicsDriver_CommonGL_SamplerInterface_H__ */
