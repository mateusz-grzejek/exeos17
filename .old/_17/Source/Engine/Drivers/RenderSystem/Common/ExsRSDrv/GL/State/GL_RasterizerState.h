
#ifndef __Exs_GraphicsDriver_CommonGL_RasterizerState_H__
#define __Exs_GraphicsDriver_CommonGL_RasterizerState_H__

#include "../GL_Prerequisites.h"
#include <ExsRenderSystem/State/RasterizerState.h>


namespace Exs
{


	struct GLRasterizerConfiguration
	{
		struct Settings
		{
			GLenum  faceCullMode;
			GLenum  frontFaceOrder;

		#if ( EXS_GL_FEATURE_POLYGON_RENDERING_MODE )
			GLenum  triangleFillMode;
		#endif
		};

		Settings settings;
		bool scissorTestState;
	};


	class GLRasterizerStateDescriptor : public RasterizerStateDescriptor
	{
		EXS_DECLARE_NONCOPYABLE(GLRasterizerStateDescriptor);

	private:
		GLRasterizerConfiguration    _glConfiguration;

	public:
		GLRasterizerStateDescriptor( RenderSystem* renderSystem,
		                             RSBaseObjectID baseObjectID,
		                             RSStateDescriptorPID descriptorPID,
		                             const RasterizerConfiguration& configuration,
		                             const GLRasterizerConfiguration& configurationGL)
		: RasterizerStateDescriptor(renderSystem, baseObjectID, descriptorPID, configuration)
		, _glConfiguration(configurationGL)
		{ }

		const GLRasterizerConfiguration& GetGLConfiguration() const
		{
			return this->_glConfiguration;
		}
	};


	bool TranslateGLRasterizerConfiguration(const RasterizerConfiguration& configuration, GLRasterizerConfiguration* translatedGLConfig);


};


#endif /* __Exs_GraphicsDriver_CommonGL_RasterizerState_H__ */
