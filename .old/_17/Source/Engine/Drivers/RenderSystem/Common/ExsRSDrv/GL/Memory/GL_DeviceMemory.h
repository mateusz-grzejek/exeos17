
#ifndef __Exs_GraphicsDriver_CommonGL_DeviceMemory_H__
#define __Exs_GraphicsDriver_CommonGL_DeviceMemory_H__

#include "../GL_Prerequisites.h"
#include <ExsRenderSystem/Memory/DeviceMemory.h>


namespace Exs
{


	class GLDeviceMemory;


	///<summary>
	///</summary>
	class GLDeviceMemoryPool : public DeviceMemoryPool
	{
	private:
		typedef std::unique_ptr<GLDeviceMemory> GLDeviceMemoryPtr;
		typedef std::vector<GLDeviceMemoryPtr> GLDeviceMemoryArray;

	private:
		std::atomic<RS_memory_size_t>    _currentAllocOffset;
		RSMemoryLockInternal             _accessLock;

	public:
		GLDeviceMemoryPool(RSMemorySystem* memorySystem, Uint64 size, const DeviceMemoryPoolProperties& properties);
		virtual ~GLDeviceMemoryPool();

		// @ovveride DeviceHeap::Reset.
        virtual Result Reset() override final;

		//
        bool AllocateMemory(RS_memory_size_t memSize, DeviceMemoryBlock** deviceMemory);
	};


}


#endif /* __Exs_GraphicsDriver_CommonGL_DeviceMemory_H__ */
