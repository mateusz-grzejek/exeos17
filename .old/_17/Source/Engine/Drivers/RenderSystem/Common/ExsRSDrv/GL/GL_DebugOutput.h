
#ifndef __Exs_GraphicsDriver_CommonGL_DebugOutput_H__
#define __Exs_GraphicsDriver_CommonGL_DebugOutput_H__

#include "GL_Extensions.h"


namespace Exs
{


	class GLExtensionManager;

	ExsDeclareRefPtrClass(GLDebugOutput);


	enum class GLDebugEventClassification : Enum
	{
		Error,
		Notification,
		Warning
	};


	enum class GLDebugOutputVersion : Enum
	{
		AMD_Extension = static_cast<Enum>(GLCoreExtensionID::AMD_Debug_Output),
		ARB_Extension = static_cast<Enum>(GLCoreExtensionID::ARB_Debug_Output),
		KHR_Core = static_cast<Enum>(GLCoreExtensionID::KHR_Debug),
		Unknown = 0
	};


#if ( EXS_GL_FEATURE_DEBUG_OUTPUT )

	class GLDebugOutput
	{
		EXS_DECLARE_NONCOPYABLE(GLDebugOutput);

	public:
		typedef stdx::assoc_array<GLuint, GLuint> IgnoredEventsList;
		typedef IgnoredEventsList::iterator EventIterator;

	protected:
		GLDebugOutputVersion    _version;
		std::string             _extensionName;
		IgnoredEventsList       _ignoredEvents;
		Size_t                  _eventsCounter;
		bool                    _breakOnEvent;
		bool                    _eventFilterEnabled;
		bool                    _callbackActive;

	public:
		GLDebugOutput(GLDebugOutputVersion version, const char* extensionName);
		virtual ~GLDebugOutput();

		virtual void EnableSync(bool enable) = 0;

		void ActivateCallback();
		void DeactivateCallback();

		void EnableBreakOnEvent(bool enable);
		void EnableEventsFiltering(bool enable);

		void SetEventFilter(GLuint eventID, bool ignored);

		const std::string& GetExtensionName() const;

		Size_t GetEventsCounter() const;
		GLDebugOutputVersion GetVersion() const;
		
		bool IsCallbackActive() const;
		bool IsEventIgnored(GLuint eventID) const;

		static void EnableDebugOutput(bool enable);

		static std::unique_ptr<GLDebugOutput> Initialize(const GLExtensionManager& extensionManager, GLDebugOutputVersion preferred);

	protected:
		virtual void SetCallbackActive(bool active) = 0;

		void DisplayEventInfo(GLDebugEventClassification eventClassification, const char* eventInfo) const;

		static bool ValidateVersion(GLDebugOutputVersion version);
	};


	inline void GLDebugOutput::EnableBreakOnEvent(bool enable)
	{
		this->_breakOnEvent = enable;
	}

	inline void GLDebugOutput::EnableEventsFiltering(bool enable)
	{
		this->_eventFilterEnabled = enable;
	}

	inline const std::string& GLDebugOutput::GetExtensionName() const
	{
		return this->_extensionName;
	}

	inline Size_t GLDebugOutput::GetEventsCounter() const
	{
		return this->_eventsCounter;
	}

	inline GLDebugOutputVersion GLDebugOutput::GetVersion() const
	{
		return this->_version;
	}

	inline bool GLDebugOutput::IsCallbackActive() const
	{
		return this->_callbackActive;
	}

	inline bool GLDebugOutput::ValidateVersion(GLDebugOutputVersion version)
	{
		return (version == GLDebugOutputVersion::AMD_Extension) ||
		       (version == GLDebugOutputVersion::ARB_Extension) ||
		       (version == GLDebugOutputVersion::KHR_Core);
	}

#else

	class GLDebugOutput
	{
	public:
		GLDebugOutput()
		{ }

		~GLDebugOutput()
		{ }
	};

#endif


}


#endif /* __Exs_GraphicsDriver_CommonGL_DebugOutput_H__ */
