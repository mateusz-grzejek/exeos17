
#include <ExsCommonGL/Objects/GL_BufferObject.h>
#include <ExsCommonGL/GL_ObjectsAllocator.h>


namespace Exs
{


	GLBufferObject::GLBufferObject(GLenum bindTarget)
	: GLObject(GLObjectType::Buffer)
	, _bindTarget(bindTarget)
	, _bufferSize(0)
	, _mappedMemory(nullptr)
	{
		this->_handle = GLObjectsAllocator::AllocateBuffer();
	}


	GLBufferObject::GLBufferObject(GLBufferObject&& source)
	: GLObject(GLObjectType::Buffer)
	{
		this->Swap(source);
	}


	GLBufferObject::~GLBufferObject()
	{
		if (this->_handle != GL_Invalid_Handle)
		{
			this->Release();
		}
	}


	GLBufferObject& GLBufferObject::operator=(GLBufferObject&& rhs)
	{
		GLBufferObject(std::move(rhs)).Swap(*this);
		return *this;
	}


	bool GLBufferObject::Release()
	{
		if (!GLObject::Release())
			return false;

		GLObjectsAllocator::DeallocateBuffer(this->_handle);
		this->_handle = GL_Invalid_Handle;

		return true;
	}


	bool GLBufferObject::ValidateHandle() const
	{
		GLboolean checkResult = glIsBuffer(this->_handle);
		return checkResult == GL_TRUE;
	}


	bool GLBufferObject::Initialize(const GLBufferInitDesc& initDesc, const GLBufferData& initData)
	{
		ExsDebugAssert( this->CheckHandle() && !this->IsInitialized() );
		ExsDebugAssert( initDesc.bindTarget == this->_bindTarget );
		ExsDebugAssert( initDesc.size > 0 );

		glBindBuffer(initDesc.bindTarget, this->_handle);
		ExsGLErrorCheck();

		if (initData.dataSize > 0)
		{
			ExsDebugAssert( initData.dataPtr != nullptr );
			ExsDebugAssert( initData.dataSize <= initDesc.size );

			GLsizeiptr initDataSize = stdx::get_min_of(initData.dataSize, initDesc.size);

			glBufferData(initDesc.bindTarget, initDataSize, initData.dataPtr, initDesc.usageHint);
			ExsGLErrorCheck();
		}
		else
		{
			glBufferData(initDesc.bindTarget, initDesc.size, nullptr, initDesc.usageHint);
			ExsGLErrorCheck();
		}

		glBindBuffer(initDesc.bindTarget, 0);
		ExsGLErrorCheck();

		this->_bindTarget = initDesc.bindTarget;
		this->_bufferSize = initDesc.size;

		return true;
	}


	void* GLBufferObject::Map(Uint offset, Size_t length, GLenum flags, bool restoreTargetBinding)
	{
		ExsRuntimeAssert( this->_mappedMemory == nullptr );

		ExsDebugAssert( this->IsInitialized() );
		ExsDebugAssert( (offset < this->_bufferSize) && (this->_bufferSize - offset >= length) );

		ExsGLSetBufferBindingRestorePointConditional(this->_bindTarget, restoreTargetBinding);

		glBindBuffer(this->_bindTarget, this->_handle);
		ExsGLErrorCheck();

		void* mappedPtr = glMapBufferRange(this->_bindTarget, offset, length, flags);
		ExsGLErrorCheck();

		this->_mappedMemory = mappedPtr;

		return this->_mappedMemory;
	}


	void GLBufferObject::Unmap(bool restoreTargetBinding)
	{
		ExsRuntimeAssert( this->_mappedMemory != nullptr );

		ExsGLSetBufferBindingRestorePointConditional(this->_bindTarget, restoreTargetBinding);
		
		glBindBuffer(this->_bindTarget, this->_handle);
		ExsGLErrorCheck();

		glUnmapBuffer(this->_bindTarget);
		ExsGLErrorCheck();

		this->_mappedMemory = nullptr;
	}


	void GLBufferObject::CopySubdata(Uint offset, Size_t size, const void* data, bool restoreTargetBinding)
	{
		ExsRuntimeAssert( this->_mappedMemory == nullptr );

		ExsDebugAssert( this->IsInitialized() );
		ExsDebugAssert( (offset < this->_bufferSize) && (this->_bufferSize - offset >= size) );

		ExsGLSetBufferBindingRestorePointConditional(this->_bindTarget, restoreTargetBinding);
		
		glBindBuffer(this->_bindTarget, this->_handle);
		ExsGLErrorCheck();

		glBufferSubData(this->_bindTarget, offset, size, data);
		ExsGLErrorCheck();
	}


}
