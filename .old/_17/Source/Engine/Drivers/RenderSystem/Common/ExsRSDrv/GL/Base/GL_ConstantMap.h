
#ifndef __Exs_GraphicsDriver_CommonGL_ConstantMap_H__
#define __Exs_GraphicsDriver_CommonGL_ConstantMap_H__

#include <ExsRenderSystem/Resource/ShaderBase.h>


namespace Exs
{


	class GLConstantMap
	{
	public:
		template <typename _BlendEquation>
		static GLenum GetBlendEquation(_BlendEquation blendEquation)
		{
			return _TranslateBlendEquation(static_cast<Enum>(blendEquation));
		}

		template <typename _BlendFactor>
		static GLenum GetBlendFactor(_BlendFactor blendFactor)
		{
			return _TranslateBlendFactor(static_cast<Enum>(blendFactor));
		}

		template <typename _GPUBufferType>
		static GLenum GetBufferBindTarget(_GPUBufferType bufferType)
		{
			return _TranslateBufferBindTarget(static_cast<Enum>(bufferType));
		}
		
		template <typename _ComparisonFunction>
		static GLenum GetComparisonFunction(_ComparisonFunction function)
		{
			return _TranslateComparisonFunction(static_cast<Enum>(function));
		}

		template <typename _CullMode>
		static GLenum GetCullMode(_CullMode cullMode)
		{
			return _TranslateCullMode(static_cast<Enum>(cullMode));
		}

		template <typename _DepthWriteMask>
		static GLboolean GetDepthWriteMask(_DepthWriteMask writeMask)
		{
			return _TranslateDepthWriteMask(static_cast<Enum>(writeMask));
		}

		template <typename _FillMode>
		static GLenum GetFillMode(_FillMode fillMode)
		{
			return _TranslateFillMode(static_cast<Enum>(fillMode));
		}

		template <typename _GraphicDataBaseType>
		static GLenum GetGraphicDataBaseType(_GraphicDataBaseType baseType)
		{
			return _TranslateGraphicDataBaseType(static_cast<Enum>(baseType));
		}

		template <typename _PrimitiveTopology>
		static GLenum GetPrimitiveTopology(_PrimitiveTopology primitiveTopology)
		{
			return _TranslatePrimitiveTopology(static_cast<Enum>(primitiveTopology));
		}

		template <typename _ShaderResourceViewResType>
		static GLenum GetShaderResourceBindTarget(_ShaderResourceViewResType resType)
		{
			return _TranslateShaderResourceBindTarget(static_cast<Enum>(resType));
		}

		template <typename _ShaderType>
		static GLenum GetShaderType(_ShaderType shaderType)
		{
			return _TranslateShaderType(static_cast<Enum>(shaderType));
		}

		template <typename _StencilOp>
		static GLenum GetStencilOp(_StencilOp stencilOp)
		{
			return _TranslateStencilOp(static_cast<Enum>(stencilOp));
		}

		template <typename _TextureAddressMode>
		static GLenum GetTextureAddressMode(_TextureAddressMode addressMode)
		{
			return _TranslateTextureAddressMode(static_cast<Enum>(addressMode));
		}

		template <typename _GraphicDataFormat>
		static GLenum GetTextureInternalFormat(_GraphicDataFormat format)
		{
			return _TranslateTextureInternalFormat(static_cast<Enum>(format));
		}

		template <typename _GraphicDataFormat>
		static GLenum GetTexturePixelFormat(_GraphicDataFormat format)
		{
			return _TranslateTexturePixelFormat(static_cast<Enum>(format));
		}

		template <typename _GraphicDataFormat>
		static GLenum GetTexturePixelType(_GraphicDataFormat format)
		{
			return _TranslateTexturePixelType(static_cast<Enum>(format));
		}

		template <typename _VerticesOrder>
		static GLenum GetVerticesOrder(_VerticesOrder order)
		{
			return _TranslateVerticesOrder(static_cast<Enum>(order));
		}

	private:
		static GLenum _TranslateBlendEquation(Enum blendEquation);
		static GLenum _TranslateBlendFactor(Enum blendFactor);
		static GLenum _TranslateBufferBindTarget(Enum bufferType);
		static GLenum _TranslateComparisonFunction(Enum function);
		static GLenum _TranslateCullMode(Enum cullMode);
		static GLboolean _TranslateDepthWriteMask(Enum writeMask);
		static GLenum _TranslateFillMode(Enum fillMode);
		static GLenum _TranslateGraphicDataBaseType(Enum baseType);
		static GLenum _TranslatePrimitiveTopology(Enum primitiveTopology);
		static GLenum _TranslateShaderResourceBindTarget(Enum resType);
		static GLenum _TranslateShaderType(Enum shaderType);
		static GLenum _TranslateStencilOp(Enum stencilOp);
		static GLenum _TranslateTextureAddressMode(Enum addressMode);
		static GLenum _TranslateTextureInternalFormat(Enum format);
		static GLenum _TranslateTexturePixelFormat(Enum format);
		static GLenum _TranslateTexturePixelType(Enum format);
		static GLenum _TranslateVerticesOrder(Enum order);
	};


}


#endif /* __Exs_GraphicsDriver_CommonGL_ConstantMap_H__ */
