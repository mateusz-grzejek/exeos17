
#include <ExsCommonGL/Objects/GL_VertexArrayObject.h>
#include <ExsCommonGL/GL_ObjectsAllocator.h>


namespace Exs
{


	GLVertexArrayObject::GLVertexArrayObject()
	: GLObject(GLObjectType::Vertex_Array)
	{
		this->_handle = GLObjectsAllocator::AllocateVertexArray();
	}


	GLVertexArrayObject::~GLVertexArrayObject()
	{
		if (this->_handle != GL_Invalid_Handle)
		{
			this->Release();
		}
	}


	bool GLVertexArrayObject::Release()
	{
		if (!GLObject::Release())
			return false;

		GLObjectsAllocator::DeallocateVertexArray(this->_handle);
		this->_handle = GL_Invalid_Handle;

		return true;
	}


	bool GLVertexArrayObject::ValidateHandle() const
	{
		GLboolean checkResult = glIsVertexArray(this->_handle);
		return checkResult == GL_TRUE;
	}


}
