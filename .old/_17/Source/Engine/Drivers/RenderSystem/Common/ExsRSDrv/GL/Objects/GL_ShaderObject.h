
#ifndef __Exs_GraphicsDriver_CommonGL_ShaderObject_H__
#define __Exs_GraphicsDriver_CommonGL_ShaderObject_H__

#include "../GL_Prerequisites.h"


namespace Exs
{


	enum class GLShaderParameter : Enum
	{
		//
		Compilation_Result = 0xA741,
		
		//
		Delete_Status = 0xA742
	};


	class GLShaderObject : public GLObject
	{
		EXS_DECLARE_NONCOPYABLE(GLShaderObject);
		
		friend class GLRenderSystem;

	protected:
		GLenum    _shaderType;

	public:
		GLShaderObject(GLenum shaderType);
		virtual ~GLShaderObject();
		
		virtual bool Release() override;
		virtual bool ValidateHandle() const override;
		
		bool CompileSource(const Byte* source, Size_t length);
		
		GLint QueryStateParameter(GLShaderParameter parameter) const;

		GLenum GetType() const;
		std::string GetInfoLog() const;
		std::string GetSource() const;
		Size_t GetInfoLogLength() const;
		Size_t GetSourceLength() const;
		
		bool IsInfoLogEmpty() const;
		bool IsSourceEmpty() const;

		void Swap(GLShaderObject& other);
	};


	inline GLenum GLShaderObject::GetType() const
	{
		return this->_shaderType;
	}

	inline bool GLShaderObject::IsInfoLogEmpty() const
	{
		return this->GetInfoLogLength() == 0;
	}

	inline bool GLShaderObject::IsSourceEmpty() const
	{
		return this->GetSourceLength() == 0;
	}
	
	inline void GLShaderObject::Swap(GLShaderObject& other)
	{
		ExsDebugAssert( this->_shaderType == other._shaderType );
		std::swap(this->_handle, other._handle);
		std::swap(this->_objType, other._objType);
		std::swap(this->_shaderType, other._shaderType);
	}


}


#endif /* __Exs_GraphicsDriver_CommonGL_ShaderObject_H__ */
