
#ifndef __Exs_GraphicsDriver_CommonGL_TextureStorage_H__
#define __Exs_GraphicsDriver_CommonGL_TextureStorage_H__

#include "GL_TextureInterface.h"
#include <ExsRenderSystem/Resource/TextureStorage.h>


namespace Exs
{


	class GLTextureInterface;
	class GLTextureObject;


	///<summary>
	///</summary>
	class GLTextureStorage : public TextureStorage
	{
	protected:
		GLTextureObject*  _glTextureObject;
		GLuint            _glTextureObjectHandle;

	public:
		GLTextureStorage( const RSResourceMemoryInfo& memoryInfo,
			const TextureDimensions& dimensions,
			GraphicDataFormat textureFormat,
			GLTextureObject* textureObject);

		virtual ~GLTextureStorage();

		virtual bool MapMemory(RSResourceMapMode mapMode, RSMappedMemoryInfo* mappedMemoryInfo) override final;

		virtual bool MapMemory(const RSMemoryRange& range, RSResourceMapMode mapMode, RSMappedMemoryInfo* mappedMemoryInfo) override final;
		
		virtual void UnmapMemory() override final;
		
		virtual Result UpdateTexture2D(const TextureRange& range, const TextureData2DDesc& dataDesc) override final;

		virtual Result UpdateTexture3D(const TextureRange& range, const TextureData3DDesc& dataDesc) override final;

		virtual Result UpdateTexture2DArray(const TextureRange& range, const TextureData2DArrayDesc& dataDesc) override final;

		virtual Result UpdateTexture2DArraySubtexture(Uint32 arrayIndex, const TextureRange& range, const TextureData2DDesc& dataDesc) override final;

		virtual Result UpdateTexture3DLayer(Uint32 layerIndex, const TextureRange& range, const TextureData2DDesc& dataDesc) override final;

		virtual Result UpdateTextureCubeMapFace(CubeMapFace face, const TextureRange& range, const TextureData2DDesc& dataDesc) override final;
	};

	
	RSInternalHandle<GLTextureStorage> InitGLTextureStorage2D(GLTextureInterface* textureInterface, const TextureStorageInitDesc& initDesc, const TextureData2DDesc& initData);
	RSInternalHandle<GLTextureStorage> InitGLTextureStorage2DArray(GLTextureInterface* textureInterface, const TextureStorageInitDesc& initDesc, const TextureData2DArrayDesc& initData);
	RSInternalHandle<GLTextureStorage> InitGLTextureStorage2DMultisample(GLTextureInterface* textureInterface, const TextureStorageInitDesc& initDesc);
	RSInternalHandle<GLTextureStorage> InitGLTextureStorage3D(GLTextureInterface* textureInterface, const TextureStorageInitDesc& initDesc, const TextureData3DDesc& initData);
	RSInternalHandle<GLTextureStorage> InitGLTextureStorageCubeMap(GLTextureInterface* textureInterface, const TextureStorageInitDesc& initDesc, const TextureDataCubeMapDesc& initData);


}


#endif /* __Exs_GraphicsDriver_CommonGL_TextureStorage_H__ */
