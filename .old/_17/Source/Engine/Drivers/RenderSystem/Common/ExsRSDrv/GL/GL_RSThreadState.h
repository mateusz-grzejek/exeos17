
#ifndef __Exs_GraphicsDriver_CommonGL_RSThreadState_H__
#define __Exs_GraphicsDriver_CommonGL_RSThreadState_H__

#include "GL_Prerequisites.h"
#include <ExsRenderSystem/RSThreadState.h>


namespace Exs
{


	ExsDeclareRefPtrClass(GLSystemContext);


	///<summary>
	///</summary>
	class GLRSThreadState : public RSThreadState
	{
	protected:
		GLSystemContext*  _glSystemContext;

	public:
		GLRSThreadState( GLRenderSystem* renderSystem,
		                 RSThreadManager* rsThreadManager,
		                 RSThreadType rsThreadType,
		                 RSThreadIndex rsThreadIndex,
		                 GLSystemContext* oglSystemContext);

		virtual ~GLRSThreadState();
		
		// Return OpenGL system context.
		GLSystemContext* GetGLSystemContext() const;

	private:
		// @override RSThreadState::OnAttach
		virtual void OnAttach() override;

		// @override RSThreadState::OnDetach
		virtual void OnDetach() override;
	};


	inline GLSystemContext* GLRSThreadState::GetGLSystemContext() const
	{
		return this->_glSystemContext;
	}


}


#endif /* __Exs_GraphicsDriver_CommonGL_RSThreadState_H__ */
