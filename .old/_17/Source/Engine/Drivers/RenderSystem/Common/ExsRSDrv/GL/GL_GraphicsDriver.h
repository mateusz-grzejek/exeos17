
#ifndef __Exs_GraphicsDriver_CommonGL_GraphicsDriver_H__
#define __Exs_GraphicsDriver_CommonGL_GraphicsDriver_H__

#include "GL_Prerequisites.h"
#include <ExsRenderSystem/GraphicsDriver.h>


namespace Exs
{


	ExsDeclareRefPtrClass(GLDebugOutput);
	ExsDeclareRefPtrClass(GLExtensionManager);
	ExsDeclareRefPtrClass(GLSystemContext);


	struct GLGraphicsDriverConfigInfo : public GraphicsDriverConfigInfo
	{
		// Store init context in configuration object - it is used only for the time
		// of the initialization process, so storing it inside the GraphicsDriver is a waste.
		std::unique_ptr<GLSystemContext> initContext;
	};


	class GLGraphicsDriver : public GraphicsDriver
	{
	protected:
		std::unique_ptr<GLDebugOutput>      _glDebugOutput;
		std::unique_ptr<GLExtensionManager> _glExtensionManager;
		std::unique_ptr<GLSystemContext>    _glMainSystemContext;
		GLSystemInfo                        _glSystemInfo;

	public:
		GLGraphicsDriver(const GraphicsDriverCapabilitiesDesc& capabilitiesDesc);
		virtual ~GLGraphicsDriver();

		/// @override GraphicsDriver::Configure
		virtual GraphicsDriverConfigInfo* Configure(SystemWindow& targetWindow) override;

		/// @override GraphicsDriver::Initialize
		virtual Result Initialize(const GraphicsDriverInitDesc& initDesc) override;
		
		///
		virtual std::vector<VisualMSAAConfig> QuerySupportedMSAAConfigurations(const VisualConfig& visualConfig) override;
		
		///
		virtual std::vector<VideoModeConfig> QuerySupportedVideoModes() override;
		
		///
		virtual Result SetVideoMode(const VideoModeConfig& vmConfig, stdx::mask<VideoModeFlags> flags) override;

		/// @override GraphicsDriver::Release
		virtual void Release() override;

		///
		GLExtensionManager* GetGLExtensionManager();

	private:
		//
		virtual void InitializeRSState() = 0;

		//
		virtual bool CheckExtensionsSupport();

		//
		virtual bool LoadInterface();

		//
		static GLSystemInfo QuerySystemInfo();

		//
		static std::unique_ptr<GLDebugOutput> SetupDebugOutput(GLExtensionManager& extensionManager);
	};


	inline GLExtensionManager* GLGraphicsDriver::GetGLExtensionManager()
	{
		return this->_glExtensionManager.get();
	}


}


#endif /* __Exs_GraphicsDriver_CommonGL_GraphicsDriver_H__ */
