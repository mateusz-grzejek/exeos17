
#ifndef __Exs_GraphicsDriver_CommonGL_GraphicsShaderState_H__
#define __Exs_GraphicsDriver_CommonGL_GraphicsShaderState_H__

#include "../GL_Prerequisites.h"
#include <ExsRenderSystem/State/GraphicsShaderState.h>


namespace Exs
{


	class GLShaderObject;
	class GLShaderProgramObject;


	struct GLGraphicsShaderConfiguration
	{
		const GLShaderObject* vertexShader;
		const GLShaderObject* tesselationControlShader;
		const GLShaderObject* tesselationEvaluationShader;
		const GLShaderObject* geometryShader;
		const GLShaderObject* pixelShader;

		union
		{
			struct
			{
				GLuint vertexShaderHandle;
				GLuint tesselationControlShaderHandle;
				GLuint tesselationEvaluationShaderHandle;
				GLuint geometryShaderHandle;
				GLuint pixelShaderHandle;
			};

			GLuint handleArray[Config::RS_CFG_Shader_Stages_Num_Graphics];
		};
	};


	class GLGraphicsShaderStateDescriptor : public GraphicsShaderStateDescriptor
	{
		EXS_DECLARE_NONCOPYABLE(GLGraphicsShaderStateDescriptor);

	protected:
		GLGraphicsShaderConfiguration    _glConfiguration;

	public:
		GLGraphicsShaderStateDescriptor( RenderSystem* renderSystem,
		                                 RSBaseObjectID baseObjectID,
		                                 RSStateDescriptorPID descriptorPID,
		                                 const GraphicsShaderConfiguration& configuration,
		                                 const GLGraphicsShaderConfiguration& configurationGL)
		: GraphicsShaderStateDescriptor(renderSystem, baseObjectID, descriptorPID, configuration)
		, _glConfiguration(configurationGL)
		{ }

		//const GLGraphicsShaderConfiguration& GetGLConfiguration() const
		//{
		//	return this->_glConfiguration;
		//}
	};


	bool TranslateGLGraphicsShaderConfiguration(const GraphicsShaderConfiguration& configuration, GLGraphicsShaderConfiguration* translatedGLConfig);


};


#endif /* __Exs_GraphicsDriver_CommonGL_GraphicsShaderState_H__ */
