
#include <ExsRenderSystem/Resource/ConstantBuffer.h>
#include <ExsRenderSystem/Resource/Sampler.h>
#include <ExsRenderSystem/Resource/ShaderResourceView.h>
#include <ExsCommonGL/GL_RSContextCommandQueue.h>
#include <ExsCommonGL/GL_RSContextStateController.h>
#include <ExsCommonGL/GL_RSThreadState.h>
#include <ExsCommonGL/GL_RenderSystem.h>
#include <ExsCommonGL/GL_SystemLayer.h>
#include <ExsCommonGL/Resource/GL_GPUBufferInterface.h>
#include <ExsCommonGL/Resource/GL_SamplerInterface.h>
#include <ExsCommonGL/Resource/GL_TextureInterface.h>
#include <ExsCommonGL/State/GL_BindingCacheTable.h>
#include <ExsCommonGL/State/GL_BlendState.h>
#include <ExsCommonGL/State/GL_DepthStencilState.h>
#include <ExsCommonGL/State/GL_RasterizerState.h>


namespace Exs
{


	GLRSContextCommandQueue::GLRSContextCommandQueue(GLRenderSystem* renderSystem, GLRSThreadState* rsThreadState, GLRSContextStateController* rsContextStateController)
	: RSContextCommandQueue(renderSystem, rsThreadState, rsContextStateController)
	, _glSystemContext(rsThreadState->GetGLSystemContext())
	{ }


	GLRSContextCommandQueue::~GLRSContextCommandQueue()
	{ }


	void GLRSContextCommandQueue::SetConstantBuffer(Uint32 cbufferIndex, stdx::mask<ShaderStageFlags> stageAccess, const ConstantBufferHandle& constantBuffer)
	{
		auto* cbufferState = constantBuffer->GetInterface()->As<GLGPUBufferInterface>();
		GLuint cbufferHandle = cbufferState->GetGLBufferObjectHandle();

		glBindBufferBase(GL_UNIFORM_BUFFER, cbufferIndex, cbufferHandle);
		ExsGLErrorCheck();
	}


	void GLRSContextCommandQueue::SetConstantBufferBindingCacheTable(const ConstantBufferBindingCacheTableHandle& cbufferBindingCacheTable)
	{
		const auto* oglCache = cbufferBindingCacheTable->As<GLConstantBufferBindingCacheTable>();
		const auto& oglBindings = oglCache->GetGLBindings();

		for (auto& binding : oglBindings)
		{
			if (binding.bufferHandle == 0)
				continue;

			glBindBufferBase(GL_UNIFORM_BUFFER, binding.bindIndex, binding.bufferHandle);
			ExsGLErrorCheck();
		}
	}


	void GLRSContextCommandQueue::SetSampler(Uint32 resourceIndex, stdx::mask<ShaderStageFlags> stageAccess, const SamplerHandle& sampler)
	{
		auto* samplerState = sampler->GetInterface()->As<GLSamplerInterface>();
		GLuint samplerHandle = samplerState->GetGLSamplerObjectHandle();

		glActiveTexture(GL_TEXTURE0 + resourceIndex);
		ExsGLErrorCheck();

		glBindSampler(resourceIndex, samplerHandle);
		ExsGLErrorCheck();
	}


	void GLRSContextCommandQueue::SetSamplerBindingCacheTable(const SamplerBindingCacheTableHandle& samplerBindingCacheTable)
	{
		const auto* oglCache = samplerBindingCacheTable->As<GLSamplerBindingCacheTable>();
		const auto& oglBindings = oglCache->GetGLBindings();

		for (auto& binding : oglBindings)
		{
			if (binding.samplerHandle == 0)
				continue;

			glActiveTexture(GL_TEXTURE0 + binding.textureUnit);
			ExsGLErrorCheck();
			
			glBindSampler(binding.textureUnit, binding.samplerHandle);
			ExsGLErrorCheck();
		}
	}


	void GLRSContextCommandQueue::SetShaderResource(Uint32 resourceIndex, stdx::mask<ShaderStageFlags> stageAccess, const ShaderResourceViewHandle& resourceView)
	{
		auto srvResType = resourceView->GetResourceType();
		GLenum bindTarget = GLConstantMap::GetShaderResourceBindTarget(srvResType);

		if (srvResType == ShaderResourceViewResType::Buffer)
		{
			return;
		}
		else
		{
			auto* textureState = resourceView->GetResource()->GetInterface()->As<GLTextureInterface>();
			GLuint textureHandle = textureState->GetGLTextureObjectHandle();
			
			glActiveTexture(GL_TEXTURE0 + resourceIndex);
			ExsGLErrorCheck();

			glBindTexture(bindTarget, textureHandle);
			ExsGLErrorCheck();
		}
	}


	void GLRSContextCommandQueue::SetShaderResourceBindingCacheTable(const ShaderResourceBindingCacheTableHandle& shaderResourceBindingCacheTable)
	{
		const auto* oglCache = shaderResourceBindingCacheTable->As<GLShaderResourceBindingCacheTable>();
		const auto& oglBindings = oglCache->GetGLBindings();

		for (auto& binding : oglBindings)
		{
			if (binding.textureHandle == 0)
				continue;

			glActiveTexture(GL_TEXTURE0 + binding.textureUnit);
			ExsGLErrorCheck();
			
			glBindTexture(binding.bindTarget, binding.textureHandle);
			ExsGLErrorCheck();
		}
	}


	void GLRSContextCommandQueue::Clear(stdx::mask<RenderTargetBufferFlags> bufferMask)
	{
		stdx::mask<GLenum> clearMask = 0;

		if (bufferMask.is_set(RenderTargetBuffer_Color))
			clearMask.set(GL_COLOR_BUFFER_BIT);

		if (bufferMask.is_set(RenderTargetBuffer_Depth))
			clearMask.set(GL_DEPTH_BUFFER_BIT);

		if (bufferMask.is_set(RenderTargetBuffer_Stencil))
			clearMask.set(GL_STENCIL_BUFFER_BIT);

		glClear(clearMask);
		ExsGLErrorCheck();
	}


	void GLRSContextCommandQueue::ClearColorBuffer()
	{
		glClear(GL_COLOR_BUFFER_BIT);
		ExsGLErrorCheck();
	}


	void GLRSContextCommandQueue::Present()
	{
		this->_glSystemContext->SwapBuffers();
	}


}
