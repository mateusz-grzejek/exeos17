
#ifndef __Exs_GraphicsDriver_CommonGL_BlendState_H__
#define __Exs_GraphicsDriver_CommonGL_BlendState_H__

#include "../GL_Prerequisites.h"
#include <ExsRenderSystem/State/BlendState.h>


namespace Exs
{


	ExsDeclareRefPtrClass(GLBlendStateDescriptor);


	struct GLBlendConfiguration
	{
		struct Settings
		{
			struct Equation
			{
				GLenum color;
				GLenum alpha;
			};

			struct Factor
			{
				GLenum colorSrc;
				GLenum colorTgt;
				GLenum alphaSrc;
				GLenum alphaTgt;
			};

			struct ConstantColor
			{
				GLclampf r;
				GLclampf g;
				GLclampf b;
				GLclampf a;
			};

			ConstantColor constantColor;
			Equation equation;
			Factor factor;
		};

		Settings settings;
		bool state;
	};


	class GLBlendStateDescriptor : public BlendStateDescriptor
	{
		EXS_DECLARE_NONCOPYABLE(GLBlendStateDescriptor);

	private:
		GLBlendConfiguration    _glConfiguration;

	public:
		GLBlendStateDescriptor( RenderSystem* renderSystem,
		                        RSBaseObjectID baseObjectID,
		                        RSStateDescriptorPID descriptorPID,
		                        const BlendConfiguration& configuration,
		                        const GLBlendConfiguration& configurationGL)
		: BlendStateDescriptor(renderSystem, baseObjectID, descriptorPID, configuration)
		, _glConfiguration(configurationGL)
		{ }

		const GLBlendConfiguration& GetGLConfiguration() const
		{
			return this->_glConfiguration;
		}
	};


	bool TranslateGLBlendConfiguration(const BlendConfiguration& configuration, GLBlendConfiguration* translatedGLConfig);


};


#endif /* __Exs_GraphicsDriver_CommonGL_BlendState_H__ */
