
#include <ExsCommonGL/GL_RenderSystem.h>
#include <ExsCommonGL/Memory/GL_DeviceMemory.h>
#include <ExsCommonGL/Memory/GL_RSMemorySystem.h>


namespace Exs
{


	GLRSMemorySystem::GLRSMemorySystem(GLRenderSystem* renderSystem)
	: RSMemorySystem(renderSystem)
	{ }


	GLRSMemorySystem::~GLRSMemorySystem()
	{ }


	Result GLRSMemorySystem::Initialize()
	{
		this->_EnumerateDeviceHeaps();
		return ExsResult(RSC_Success);
	}


	RSMemoryHandle GLRSMemorySystem::AllocateResourceMemory(const RSResourceMemoryAllocationDesc& allocDesc)
	{
		auto heapRequirements = GetHeapRequirements(allocDesc);

		for (auto& heapInfo : this->_rsMemoryHeaps)
		{
			const auto& heapProperties = heapInfo.heap->GetProperties();
			if (CheckHeapCompatibility(heapProperties, heapRequirements))
			{
				if (RSMemoryHandle memoryHandle = heapInfo.heap->Alloc(truncate_cast<RS_memory_size_t>(allocDesc.size)))
					return memoryHandle;
			}
		}

		return ExsRSMemoryHandleNone;
	}


	void GLRSMemorySystem::FreeMemory(RSMemoryHandle memoryHandle)
	{
	}


	RSMemoryHeap* GLRSMemorySystem::_CreateResourceHeap(const RSResourceMemoryHeapDesc& heapDesc)
	{
		DeviceMemoryBlock* deviceMemory = nullptr;
		DeviceMemoryType preferredMemoryType = DeviceMemoryType::System_Memory;
		
		if (heapDesc.usage == RSMemoryHeapUsage::Default)
			preferredMemoryType = DeviceMemoryType::Video_Memory;

		for (const auto& poolInfo : this->_deviceMemoryPools)
		{
			if (poolInfo.memoryType == preferredMemoryType)
			{
				auto* oglDeviceMemoryPool = dbgsafe_ptr_cast<GLDeviceMemoryPool*>(poolInfo.pool.get());
				if (oglDeviceMemoryPool->AllocateMemory(heapDesc.size, &deviceMemory))
					break;
			}
		}

		for (const auto& poolInfo : this->_deviceMemoryPools)
		{
			auto* oglDeviceMemoryPool = dbgsafe_ptr_cast<GLDeviceMemoryPool*>(poolInfo.pool.get());
			if (oglDeviceMemoryPool->AllocateMemory(heapDesc.size, &deviceMemory))
				break;
		}

		if (deviceMemory == nullptr)
		{
			// ..?
			return nullptr;
		}

		auto heapInfoRef = this->_rsMemoryHeaps.insert(this->_rsMemoryHeaps.end(), RSMemoryHeapInfo());
		Size_t heapInternalIndex = heapInfoRef - this->_rsMemoryHeaps.begin();
		RSMemoryHeapID heapID = ExsEnumDeclareMemoryHeapID(heapInternalIndex);

		RSMemoryHeapProperties heapProperties;
		heapProperties.flags.set(static_cast<Enum>(heapDesc.alignment));
		heapProperties.flags.set(static_cast<Enum>(heapDesc.resourceType));
		heapProperties.flags.set(static_cast<Enum>(heapDesc.usage));
		heapInfoRef->heap = std::make_shared<RSMemoryHeap>(this, deviceMemory, heapID, heapDesc.size, heapProperties);

		return heapInfoRef->heap.get();
	}


	void GLRSMemorySystem::_EnumerateDeviceHeaps()
	{
		Uint64 memoryPoolSize = stdx::limits<Uint64>::max_value;

		// Initialize system memory pool.
		{
			DeviceMemoryPoolProperties systemMemoryPoolProperties;
			systemMemoryPoolProperties.baseAlignment = EXS_MEMORY_BASE_ALIGNMENT;
			systemMemoryPoolProperties.memoryType = DeviceMemoryType::System_Memory;

			DeviceMemoryPoolInfo systemMemoryPoolInfo;
			systemMemoryPoolInfo.pool = std::make_shared<GLDeviceMemoryPool>(this, memoryPoolSize, systemMemoryPoolProperties);
			systemMemoryPoolInfo.memoryType = DeviceMemoryType::System_Memory;

			this->_deviceMemoryPools.push_back(std::move(systemMemoryPoolInfo));
		}

		// Initialize video memory pool.
		{
			DeviceMemoryPoolProperties videoMemoryPoolProperties;
			videoMemoryPoolProperties.baseAlignment = EXS_MEMORY_BASE_ALIGNMENT;
			videoMemoryPoolProperties.memoryType = DeviceMemoryType::Video_Memory;

			DeviceMemoryPoolInfo videoMemoryPoolInfo;
			videoMemoryPoolInfo.pool = std::make_shared<GLDeviceMemoryPool>(this, memoryPoolSize, videoMemoryPoolProperties);
			videoMemoryPoolInfo.memoryType = DeviceMemoryType::Video_Memory;

			this->_deviceMemoryPools.push_back(std::move(videoMemoryPoolInfo));
		}
	}


}
