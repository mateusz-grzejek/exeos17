
#ifndef __Exs_GraphicsDriver_CommonGL_SamplerObject_H__
#define __Exs_GraphicsDriver_CommonGL_SamplerObject_H__

#include "../GL_Prerequisites.h"


namespace Exs
{


	class GLSamplerObject : public GLObject
	{
		EXS_DECLARE_NONCOPYABLE(GLSamplerObject);
		
		friend class GLRenderSystem;

	public:
		GLSamplerObject();
		virtual ~GLSamplerObject();
		
		virtual bool Release() override;
		virtual bool ValidateHandle() const override;
		
		void SetWrapMode(GLenum wrapModeS, GLenum wrapModeT, GLenum wrapModeR);
		void SetFilter(GLenum minFilter, GLenum magFilter);
		void SetLODBias(float value);
		void SetLODRange(float min, float max);
		void SetMaxAnisotropyLevel(float level);
		void SetCompareMode(GLboolean enable, GLenum func);
		void SetBorderColor(const Color& color);

		void Swap(GLSamplerObject& other);
	};
	
	
	inline void GLSamplerObject::Swap(GLSamplerObject& other)
	{
		std::swap(this->_handle, other._handle);
		std::swap(this->_objType, other._objType);
	}


}


#endif
