cmake_minimum_required(VERSION 2.8)
project(GL)

file(GLOB FILES_INC
        "*.h"
        "Base/*.h"
        "Memory/*.h"
        "Objects/*.h"
        "Resource/*.h"
        "Specific/Linux/*.h"
        "State/*.h")

file(GLOB FILES_SRC
        "*.cpp"
        "Base/*.cpp"
        "Memory/*.cpp"
        "Objects/*.cpp"
        "Resource/*.cpp"
        "Specific/Linux/*.cpp"
        "State/*.cpp")

set(ExsFilesGraphicsDriver_${PROJECT_NAME}_Include ${FILES_INC} PARENT_SCOPE)
set(ExsFilesGraphicsDriver_${PROJECT_NAME}_Source  ${FILES_SRC} PARENT_SCOPE)
