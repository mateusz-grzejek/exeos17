
#ifndef __Exs_GraphicsDriver_CommonGL_InputLayoutState_H__
#define __Exs_GraphicsDriver_CommonGL_InputLayoutState_H__

#include "../GL_Prerequisites.h"
#include <ExsRenderSystem/State/InputLayoutState.h>


namespace Exs
{


	class GLExtensionManager;
	class GLShaderProgramObject;
	class GLVertexArrayObject;


	enum : Uint32
	{
		GL_Invalid_Vertex_Attrib_Location = static_cast<Uint32>(IAVertexAttribLocation::Unspecified),

		GL_Invalid_Vertex_Stream_Index = static_cast<Uint32>(IAVertexStreamIndex::Unspecified)
	};


	struct GLInputLayoutConfiguration
	{
		struct GLVertexAttributeDesc
		{
			GLuint active;
			GLint attribLocation;
			GLenum baseType;
			GLint componentsNum;
			GLboolean normalized;
			GLsizei relativeOffset;
			GLuint streamIndex;
		};

		struct GLVertexStreamDesc
		{
			GLuint active;
			GLuint divisor;
			GLsizei stride;
		};

		typedef std::array<GLVertexAttributeDesc, Config::RS_IA_Max_Vertex_Input_Attribs_Num> VertexAttribArray;
		typedef std::array<GLVertexStreamDesc, Config::RS_IA_Max_Vertex_Input_Streams_Num> VertexStreamArray;

		VertexAttribArray attribArray;
		VertexStreamArray streamArray;
		GLenum primitiveTopology;
	};


	class GLInputLayoutStateDescriptor : public InputLayoutStateDescriptor
	{
		EXS_DECLARE_NONCOPYABLE(GLInputLayoutStateDescriptor);

	protected:
		GLInputLayoutConfiguration    _glConfiguration;

	public:
		GLInputLayoutStateDescriptor( RenderSystem* renderSystem,
		                              RSBaseObjectID baseObjectID,
		                              RSStateDescriptorPID descriptorPID,
		                              const InputLayoutConfiguration& configuration,
		                              const GLInputLayoutConfiguration& configurationGL)
		: InputLayoutStateDescriptor(renderSystem, baseObjectID, descriptorPID, configuration)
		, _glConfiguration(configurationGL)
		{ }

		const GLInputLayoutConfiguration& GetGLConfiguration() const
		{
			return this->_glConfiguration;
		}
	};


	bool TranslateGLInputLayoutConfiguration( const InputLayoutConfiguration& configuration,
	                                          const GLShaderProgramObject& vertexShaderProgram,
	                                          const GLExtensionManager& extensionManager,
	                                          GLInputLayoutConfiguration* translatedGLConfig);


};


#endif /* __Exs_GraphicsDriver_CommonGL_InputLayoutState_H__ */
