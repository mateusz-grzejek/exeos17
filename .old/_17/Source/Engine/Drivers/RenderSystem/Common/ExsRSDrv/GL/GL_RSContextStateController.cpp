
#include <ExsCommonGL/GL_RSContextStateController.h>
#include <ExsCommonGL/GL_RenderSystem.h>
#include <ExsCommonGL/Objects/GL_ShaderProgramObject.h>
#include <ExsCommonGL/Objects/GL_VertexArrayObject.h>
#include <ExsCommonGL/State/GL_BlendState.h>
#include <ExsCommonGL/State/GL_DepthStencilState.h>
#include <ExsCommonGL/State/GL_RasterizerState.h>
#include <ExsRenderSystem/State/GraphicsPipelineStateObject.h>
#include <ExsRenderSystem/State/VertexArrayStateObject.h>


namespace Exs
{


	GLRSContextStateController::GLRSContextStateController(GLRenderSystem* renderSystem, RSThreadState* rsThreadState, GLVertexArrayObjectCache* vertexArrayObjectCache)
	: RSContextStateController(renderSystem, rsThreadState)
	, _glVertexArrayObjectCache(vertexArrayObjectCache)
	{ }


	GLRSContextStateController::~GLRSContextStateController()
	{ }


	bool GLRSContextStateController::SetClearColor(const Color& color)
	{
		if (color != this->_dynamicState.clearColor)
		{
			GLclampf r = static_cast<float>(color.rgba.red) / 255.0f;
			GLclampf g = static_cast<float>(color.rgba.green) / 255.0f;
			GLclampf b = static_cast<float>(color.rgba.blue) / 255.0f;
			GLclampf a = static_cast<float>(color.rgba.alpha) / 255.0f;

			glClearColor(r, g, b, a);
			ExsGLErrorCheck();

			this->_dynamicState.clearColor = color;
			return true;
		}

		return false;
	}


	bool GLRSContextStateController::SetDepthRange(const DepthRange& depthRange)
	{
		if (depthRange != this->_dynamicState.depthRange)
		{
			glDepthRangef(depthRange.min, depthRange.max);
			ExsGLErrorCheck();

			this->_dynamicState.depthRange = depthRange;
			return true;
		}

		return false;
	}


	bool GLRSContextStateController::SetScissorRect(const RectU32& scissorRect)
	{
		if (scissorRect != this->_dynamicState.scissorRect)
		{
			glScissor(scissorRect.position.x, scissorRect.position.y, scissorRect.size.x, scissorRect.size.y);
			ExsGLErrorCheck();

			this->_dynamicState.scissorRect = scissorRect;
			return true;
		}

		return false;
	}


	bool GLRSContextStateController::SetViewport(const Viewport& viewport)
	{
		if (viewport != this->_dynamicState.viewport)
		{
			glViewport(viewport.origin.x, viewport.origin.y, viewport.size.x, viewport.size.y);
			ExsGLErrorCheck();

			this->_dynamicState.viewport = viewport;
			return true;
		}

		return false;
	}


	void GLRSContextStateController::BindShaderProgramObject(const GLShaderProgramObject* shaderProgramObject)
	{
		if (shaderProgramObject != nullptr)
		{
			GLuint shaderProgramObjectHandle = shaderProgramObject->GetHandle();
			glUseProgram(shaderProgramObjectHandle);
			ExsGLErrorCheck();
		}
		else
		{
			glUseProgram(0);
			ExsGLErrorCheck();
		}
	}


	void GLRSContextStateController::BindVertexArrayObject(const GLVertexArrayObject* vertexArrayObject)
	{
		if (vertexArrayObject != nullptr)
		{
			GLuint vertexArrayObjectHandle = vertexArrayObject->GetHandle();
			glBindVertexArray(vertexArrayObjectHandle);
			ExsGLErrorCheck();
		}
		else
		{
			glBindVertexArray(0);
			ExsGLErrorCheck();
		}
	}


	void GLRSContextStateController::UpdateBlendState(BlendStateDescriptor* blendStateDescriptor)
	{
		ExsDebugAssert( blendStateDescriptor != nullptr );

		auto* oglBlendStateDescriptor = blendStateDescriptor->As<GLBlendStateDescriptor>();
		const auto& blendConfig = oglBlendStateDescriptor->GetGLConfiguration();

		if (this->_stateDescriptors.blend != nullptr)
		{
			UpdateBlendConfig(blendConfig, this->_glCurrentConfig.blend);
		}
		else
		{
			UpdateBlendConfig(blendConfig);
		}

		this->_glCurrentConfig.blend = blendConfig;
	}


	void GLRSContextStateController::UpdateDepthStencilState(DepthStencilStateDescriptor* depthStencilStateDescriptor)
	{
		ExsDebugAssert( depthStencilStateDescriptor != nullptr );
		
		auto* oglDepthStencilStateDescriptor = depthStencilStateDescriptor->As<GLDepthStencilStateDescriptor>();
		const auto& depthStencilConfig = oglDepthStencilStateDescriptor->GetGLConfiguration();
		
		if (this->_stateDescriptors.depthStencil != nullptr)
		{
			UpdateDepthStencilConfig(depthStencilConfig, this->_glCurrentConfig.depthStencil);
		}
		else
		{
			UpdateDepthStencilConfig(depthStencilConfig);
		}

		this->_glCurrentConfig.depthStencil = depthStencilConfig;
	}


	void GLRSContextStateController::UpdateRasterizerState(RasterizerStateDescriptor* rasterizerStateDescriptor)
	{
		ExsDebugAssert( rasterizerStateDescriptor != nullptr );
		
		auto* oglRasterizerStateDescriptor = rasterizerStateDescriptor->As<GLRasterizerStateDescriptor>();
		const auto& rasterizerConfig = oglRasterizerStateDescriptor->GetGLConfiguration();

		if (this->_stateDescriptors.rasterizer != nullptr)
		{
			UpdateRasterizerConfig(rasterizerConfig, this->_glCurrentConfig.rasterizer);
		}
		else
		{
			UpdateRasterizerConfig(rasterizerConfig);
		}
		
		this->_glCurrentConfig.rasterizer = rasterizerConfig;
	}
	

	void GLRSContextStateController::UpdateBlendConfig(const GLBlendConfiguration& blendConfig)
	{
		if (blendConfig.state)
		{
			glEnable(GL_BLEND);
			ExsGLErrorCheck();
		}
		else
		{
			glDisable(GL_BLEND);
			ExsGLErrorCheck();
		}

		if (blendConfig.state)
		{
			const auto& blendColor = blendConfig.settings.constantColor;
			glBlendColor(blendColor.r, blendColor.g, blendColor.b, blendColor.a);
			ExsGLErrorCheck();

			const auto& blendEquation = blendConfig.settings.equation;
			glBlendEquationSeparate(blendEquation.color, blendEquation.alpha);
			ExsGLErrorCheck();

			const auto& blendFactor = blendConfig.settings.factor;
			glBlendFuncSeparate(blendFactor.colorSrc, blendFactor.colorTgt, blendFactor.alphaSrc, blendFactor.alphaTgt);
			ExsGLErrorCheck();
		}
	}
	

	void GLRSContextStateController::UpdateDepthStencilConfig(const GLDepthStencilConfiguration& depthStencilConfig)
	{
		if (depthStencilConfig.depthTestState)
		{
			glEnable(GL_DEPTH_TEST);
			ExsGLErrorCheck();
		}
		else
		{
			glDisable(GL_DEPTH_TEST);
			ExsGLErrorCheck();
		}

		if (depthStencilConfig.stencilTestState)
		{
			glEnable(GL_STENCIL_TEST);
			ExsGLErrorCheck();
		}
		else
		{
			glDisable(GL_STENCIL_TEST);
			ExsGLErrorCheck();
		}

		if (depthStencilConfig.depthTestState)
		{
			glDepthFunc(depthStencilConfig.depthSettings.comparisonFunc);
			ExsGLErrorCheck();

			glDepthMask(depthStencilConfig.depthSettings.writeMask);
			ExsGLErrorCheck();
		}

		if (depthStencilConfig.stencilTestState)
		{
			const auto& comparisonFunc = depthStencilConfig.stencilSettings.comparisonFunc;
			glStencilFuncSeparate(comparisonFunc.frontFace, comparisonFunc.backFace, comparisonFunc.refValue, comparisonFunc.refMask);
			ExsGLErrorCheck();

			const auto& stencilOpBack = depthStencilConfig.stencilSettings.opBackFace;
			glStencilOpSeparate(GL_BACK, stencilOpBack.stencilFail, stencilOpBack.stencilPassDepthFail, stencilOpBack.stencilPassDepthPass);
			ExsGLErrorCheck();

			const auto& stencilOpFront = depthStencilConfig.stencilSettings.opFrontFace;
			glStencilOpSeparate(GL_BACK, stencilOpFront.stencilFail, stencilOpFront.stencilPassDepthFail, stencilOpFront.stencilPassDepthPass);
			ExsGLErrorCheck();

			glStencilMask(depthStencilConfig.stencilSettings.writeMask);
			ExsGLErrorCheck();
		}
	}
	

	void GLRSContextStateController::UpdateRasterizerConfig(const GLRasterizerConfiguration& rasterizerConfig)
	{
		if (rasterizerConfig.scissorTestState)
		{
			glEnable(GL_SCISSOR_TEST);
			ExsGLErrorCheck();
		}
		else
		{
			glDisable(GL_SCISSOR_TEST);
			ExsGLErrorCheck();
		}

		if (rasterizerConfig.settings.faceCullMode != 0)
		{
			glEnable(GL_CULL_FACE);
			ExsGLErrorCheck();

			glCullFace(rasterizerConfig.settings.faceCullMode);
			ExsGLErrorCheck();
		}
		else
		{
			glDisable(GL_CULL_FACE);
			ExsGLErrorCheck();
		}

		glFrontFace(rasterizerConfig.settings.frontFaceOrder);
		ExsGLErrorCheck();

	#if ( EXS_GL_FEATURE_POLYGON_RENDERING_MODE )

		glPolygonMode(GL_FRONT_AND_BACK, rasterizerConfig.settings.triangleFillMode);
		ExsGLErrorCheck();

	#endif
	}


	void GLRSContextStateController::UpdateBlendConfig(const GLBlendConfiguration& blendConfig, const GLBlendConfiguration& currentBlendConfig)
	{
		if (blendConfig.state != currentBlendConfig.state)
		{
			if (blendConfig.state)
			{
				glEnable(GL_BLEND);
				ExsGLErrorCheck();
			}
			else
			{
				glDisable(GL_BLEND);
				ExsGLErrorCheck();
			}
		}

		if (blendConfig.state)
		{
			if (ByteCompare(blendConfig.settings.constantColor, currentBlendConfig.settings.constantColor) != 0)
			{
				const auto& blendColor = blendConfig.settings.constantColor;
				glBlendColor(blendColor.r, blendColor.g, blendColor.b, blendColor.a);
				ExsGLErrorCheck();
			}

			if (ByteCompare(blendConfig.settings.equation, currentBlendConfig.settings.equation) != 0)
			{
				const auto& blendEquation = blendConfig.settings.equation;
				glBlendEquationSeparate(blendEquation.color, blendEquation.alpha);
				ExsGLErrorCheck();
			}

			if (ByteCompare(blendConfig.settings.factor, currentBlendConfig.settings.factor) != 0)
			{
				const auto& blendFactor = blendConfig.settings.factor;
				glBlendFuncSeparate(blendFactor.colorSrc, blendFactor.colorTgt, blendFactor.alphaSrc, blendFactor.alphaTgt);
				ExsGLErrorCheck();
			}
		}
	}


	void GLRSContextStateController::UpdateDepthStencilConfig(const GLDepthStencilConfiguration& depthStencilConfig, const GLDepthStencilConfiguration& currentDepthStencilConfig)
	{
		if (depthStencilConfig.depthTestState != currentDepthStencilConfig.depthTestState)
		{
			if (depthStencilConfig.depthTestState)
			{
				glEnable(GL_DEPTH_TEST);
				ExsGLErrorCheck();
			}
			else
			{
				glDisable(GL_DEPTH_TEST);
				ExsGLErrorCheck();
			}
		}

		if (depthStencilConfig.stencilTestState != currentDepthStencilConfig.stencilTestState)
		{
			if (depthStencilConfig.stencilTestState)
			{
				glEnable(GL_STENCIL_TEST);
				ExsGLErrorCheck();
			}
			else
			{
				glDisable(GL_STENCIL_TEST);
				ExsGLErrorCheck();
			}
		}

		if (depthStencilConfig.depthTestState)
		{
			const auto& depthSettings = depthStencilConfig.depthSettings;
			const auto& currentDepthSettings = currentDepthStencilConfig.depthSettings;

			if (depthSettings.comparisonFunc != currentDepthSettings.comparisonFunc)
			{
				glDepthFunc(depthSettings.comparisonFunc);
				ExsGLErrorCheck();
			}

			if (depthSettings.writeMask != currentDepthSettings.writeMask)
			{
				glDepthMask((depthSettings.writeMask != 0) ? GL_TRUE : GL_FALSE);
				ExsGLErrorCheck();
			}
		}

		if (depthStencilConfig.stencilTestState)
		{
			const auto& stencilSettings = depthStencilConfig.stencilSettings;
			const auto& currentStencilSettings = currentDepthStencilConfig.stencilSettings;

			if (ByteCompare(stencilSettings.comparisonFunc, currentStencilSettings.comparisonFunc) != 0)
			{
				const auto& comparisonFunc = stencilSettings.comparisonFunc;
				glStencilFuncSeparate(comparisonFunc.frontFace, comparisonFunc.backFace, comparisonFunc.refValue, comparisonFunc.refMask);
				ExsGLErrorCheck();
			}

			if (ByteCompare(stencilSettings.opBackFace, currentStencilSettings.opBackFace) != 0)
			{
				const auto& stencilOp = stencilSettings.opBackFace;
				glStencilOpSeparate(GL_BACK, stencilOp.stencilFail, stencilOp.stencilPassDepthFail, stencilOp.stencilPassDepthPass);
				ExsGLErrorCheck();
			}

			if (ByteCompare(stencilSettings.opFrontFace, currentStencilSettings.opFrontFace) != 0)
			{
				const auto& stencilOp = stencilSettings.opFrontFace;
				glStencilOpSeparate(GL_BACK, stencilOp.stencilFail, stencilOp.stencilPassDepthFail, stencilOp.stencilPassDepthPass);
				ExsGLErrorCheck();
			}

			if (stencilSettings.writeMask != currentStencilSettings.writeMask)
			{
				glStencilMask(stencilSettings.writeMask);
				ExsGLErrorCheck();
			}
		}
	}


	void GLRSContextStateController::UpdateRasterizerConfig(const GLRasterizerConfiguration& rasterizerConfig, const GLRasterizerConfiguration& currentRasterizerConfig)
	{
		if (rasterizerConfig.scissorTestState != currentRasterizerConfig.scissorTestState)
		{
			if (rasterizerConfig.scissorTestState)
			{
				glEnable(GL_SCISSOR_TEST);
				ExsGLErrorCheck();
			}
			else
			{
				glDisable(GL_SCISSOR_TEST);
				ExsGLErrorCheck();
			}
		}

		const auto& rasterizerSettings = rasterizerConfig.settings;
		const auto& currentRasterizerSettings = currentRasterizerConfig.settings;;

		if (rasterizerSettings.faceCullMode != currentRasterizerSettings.faceCullMode)
		{
			if ((rasterizerSettings.faceCullMode != 0) && (currentRasterizerSettings.faceCullMode == 0))
			{
				glEnable(GL_CULL_FACE);
				ExsGLErrorCheck();

				glCullFace(rasterizerSettings.faceCullMode);
				ExsGLErrorCheck();
			}
			else if ((rasterizerSettings.faceCullMode == 0) && (currentRasterizerSettings.faceCullMode != 0))
			{
				glDisable(GL_CULL_FACE);
				ExsGLErrorCheck();
			}
		}

		if (rasterizerSettings.frontFaceOrder != currentRasterizerSettings.frontFaceOrder)
		{
			glFrontFace(rasterizerSettings.frontFaceOrder);
			ExsGLErrorCheck();
		}

	#if ( EXS_GL_FEATURE_POLYGON_RENDERING_MODE )

		if (rasterizerSettings.triangleFillMode != currentRasterizerSettings.triangleFillMode)
		{
			glPolygonMode(GL_FRONT_AND_BACK, rasterizerSettings.triangleFillMode);
			ExsGLErrorCheck();
		}

	#endif
	}


}
