
#include <ExsCommonGL/Objects/GL_ShaderObject.h>
#include <ExsCommonGL/Objects/GL_ShaderProgramObject.h>
#include <ExsCommonGL/GL_ObjectsAllocator.h>
#include <stdx/memory_buffer.h>


namespace Exs
{


	GLShaderProgramObject::GLShaderProgramObject()
	: GLObject(GLObjectType::Shader_Program)
	{
		this->_handle = GLObjectsAllocator::AllocateShaderProgram();
	}


	GLShaderProgramObject::GLShaderProgramObject(GLShaderProgramObject&& source)
	: GLObject(GLObjectType::Shader_Program)
	{
		this->Swap(source);
	}


	GLShaderProgramObject::~GLShaderProgramObject()
	{
		if (this->_handle != GL_Invalid_Handle)
		{
			this->Release();
		}
	}


	bool GLShaderProgramObject::Release()
	{
		if (!GLObject::Release())
			return false;

		GLObjectsAllocator::DeallocateShaderProgram(this->_handle);
		this->_handle = GL_Invalid_Handle;

		return true;
	}


	bool GLShaderProgramObject::ValidateHandle() const
	{
		GLboolean checkResult = glIsProgram(this->_handle);
		return checkResult == GL_TRUE;
	}


	void GLShaderProgramObject::AttachShader(GLuint shaderHandle)
	{
		ExsDebugAssert( this->CheckHandle() );
		glAttachShader(this->_handle, shaderHandle);
		ExsGLErrorCheck();
	}


	void GLShaderProgramObject::AttachShader(const GLShaderObject& shader)
	{
		ExsDebugAssert( this->CheckHandle() );
		glAttachShader(this->_handle, shader.GetHandle());
		ExsGLErrorCheck();
	}


	void GLShaderProgramObject::DetachAllShaders()
	{
		ExsDebugAssert( this->ValidateHandle() );

		GLint attachedShadersNum = 0;
		glGetProgramiv(this->_handle, GL_ATTACHED_SHADERS, &attachedShadersNum);

		GLuint attachedShaders[64];
		GLsizei returnedShadersNum = 0;
		glGetAttachedShaders(this->_handle, 64, &returnedShadersNum, attachedShaders);
		ExsGLErrorCheck();

		ExsDebugAssert( returnedShadersNum == attachedShadersNum );

		Size_t shadersNum = static_cast<Size_t>(returnedShadersNum);
		ArrayView<GLuint> shadersArray { attachedShaders, shadersNum };

		for (auto& shader : shadersArray)
		{
			glDetachShader(this->_handle, shader);
			ExsGLErrorCheck();
		}
	}


	void GLShaderProgramObject::DetachShader(GLuint shaderHandle)
	{
		ExsDebugAssert( this->CheckHandle() );
		glDetachShader(this->_handle, shaderHandle);
		ExsGLErrorCheck();
	}


	void GLShaderProgramObject::DetachShader(const GLShaderObject& shader)
	{
		ExsDebugAssert( this->CheckHandle() );
		glDetachShader(this->_handle, shader.GetHandle());
		ExsGLErrorCheck();
	}

	
#if ( EXS_GL_FEATURE_PROGRAM_PIPELINE_OBJECT )

	void GLShaderProgramObject::SetSeparableStagesSupport(bool enable)
	{
		GLint separableStagesState = enable ? GL_TRUE : GL_FALSE;
		glProgramParameteri(this->_handle, GL_PROGRAM_SEPARABLE, separableStagesState);
		ExsGLErrorCheck();
	}

#endif


	void GLShaderProgramObject::Bind()
	{
		ExsDebugAssert(this->CheckHandle());

		glUseProgram(this->_handle);
		ExsGLErrorCheck();
	}


	bool GLShaderProgramObject::Link()
	{
		ExsDebugAssert( this->CheckHandle() );
		
		glLinkProgram(this->_handle);
		ExsGLErrorCheck();

		GLint linkStatus = 0;
		glGetProgramiv(this->_handle, GL_LINK_STATUS, &linkStatus);
		ExsGLErrorCheck();

		if (linkStatus != GL_TRUE)
		{
			std::string infoLog = this->GetInfoLog();
			ExsTraceWarning(TRC_GraphicsDriver_GL, "Link process of program %u has failed. Error: %s\n.", this->_handle, infoLog.c_str());
			return false;
		}

		return true;
	}


	bool GLShaderProgramObject::Validate()
	{
		ExsDebugAssert( this->CheckHandle() );
		
		glValidateProgram(this->_handle);
		ExsGLErrorCheck();

		GLint validateStatus = 0;
		glGetProgramiv(this->_handle, GL_VALIDATE_STATUS, &validateStatus);
		ExsGLErrorCheck();

		if (validateStatus != GL_TRUE)
		{
			std::string infoLog = this->GetInfoLog();
			ExsTraceWarning(TRC_GraphicsDriver_GL, "Validation process of program %u has failed. Error: %s\n.", this->_handle, infoLog.c_str());
			return false;
		}

		return true;
	}


	void GLShaderProgramObject::SetAttribLocation(const char* attribName, GLuint location)
	{
		ExsDebugAssert( this->CheckHandle() );

		ExsDebugCode(
			GLint linkResult = this->QueryStateParameter(GLShaderProgramParameter::Link_Result);
			ExsDebugAssert( linkResult == GL_FALSE );
		);

		glBindAttribLocation(this->_handle, location, attribName);
		ExsGLErrorCheck();
	}


	void GLShaderProgramObject::SetSamplerSourceTextureUnit(const char* samplerName, GLuint texIndex)
	{
		ExsDebugAssert(this->CheckHandle());

		ExsDebugCode(
			GLint linkResult = this->QueryStateParameter(GLShaderProgramParameter::Link_Result);
			ExsDebugAssert( linkResult == GL_TRUE );
		);

		GLint samplerLocation = glGetUniformLocation(this->_handle, samplerName);
		ExsGLErrorCheck();

		if (samplerLocation < 0)
		{
			//Warn?
			return;
		}

		if (glProgramUniform1i)
		{
			glProgramUniform1i(this->_handle, samplerLocation, texIndex);
			ExsGLErrorCheck();
		}
		else
		{
			ExsDebugCode(
				GLint currentProgram = -1;
				glGetIntegerv(GL_ACTIVE_PROGRAM, &currentProgram);
				ExsGLErrorCheck();
				ExsDebugAssert( this->_handle == currentProgram );
			);

			glUniform1i(samplerLocation, texIndex);
			ExsGLErrorCheck();
		}
	}


	void GLShaderProgramObject::SetUniformBlockBinding(const char* blockName, GLuint binding)
	{
		ExsDebugAssert(this->CheckHandle());

		ExsDebugCode(
			GLint linkResult = this->QueryStateParameter(GLShaderProgramParameter::Link_Result);
			ExsDebugAssert( linkResult == GL_TRUE );
		);

		GLint blockIndex = glGetUniformBlockIndex(this->_handle, blockName);
		ExsGLErrorCheck();
		
		if (blockIndex < 0)
		{
			//Warn?
			return;
		}

		glUniformBlockBinding(this->_handle, blockIndex, binding);
		ExsGLErrorCheck();
	}


	GLint GLShaderProgramObject::QueryStateParameter(GLShaderProgramParameter parameter) const
	{
		ExsDebugAssert( this->CheckHandle() );

		GLint parameterValue = GL_Invalid_Value;

		switch(parameter)
		{
		case GLShaderProgramParameter::Delete_Status:
			glGetProgramiv(this->_handle, GL_DELETE_STATUS, &parameterValue);
			ExsGLErrorCheck();
			break;
			
		case GLShaderProgramParameter::Link_Result:
			glGetProgramiv(this->_handle, GL_LINK_STATUS, &parameterValue);
			ExsGLErrorCheck();
			break;
			
		case GLShaderProgramParameter::Validation_Result:
			glGetProgramiv(this->_handle, GL_VALIDATE_STATUS, &parameterValue);
			ExsGLErrorCheck();
			break;
		}

		return parameterValue;
	}


	GLuint GLShaderProgramObject::QueryVertexAttribLocation(const char* name) const
	{
		ExsDebugAssert( this->CheckHandle() );

		GLint attribLocation = glGetAttribLocation(this->_handle, reinterpret_cast<const GLchar*>(name));

		if (attribLocation == -1)
		{
			ExsTraceNotification(TRC_GraphicsDriver_GL, "");
			return GL_Invalid_Attrib_Location;
		}

		return static_cast<GLuint>(attribLocation);
	}


	std::string GLShaderProgramObject::GetInfoLog() const
	{
		ExsDebugAssert( this->CheckHandle() );

		std::string infoLog { };

		// Note: length returned by the GL includes null terminator!

		GLint infoLogLength = 0;
		glGetProgramiv(this->_handle, GL_INFO_LOG_LENGTH, &infoLogLength);
		ExsGLErrorCheck();

		if (infoLogLength > 0)
		{
			stdx::dynamic_memory_buffer<GLchar> infoLogBuffer(infoLogLength);
			glGetProgramInfoLog(this->_handle, infoLogLength, nullptr, infoLogBuffer.data_ptr());
			ExsGLErrorCheck();

			infoLog.assign(infoLogBuffer.data_ptr(), infoLogLength - 1);
		}

		return infoLog;
	}


	Size_t GLShaderProgramObject::GetAttachedShadersNum() const
	{
		ExsDebugAssert( this->CheckHandle() );

		GLint attachedShadersNum = 0;
		glGetProgramiv(this->_handle, GL_ATTACHED_SHADERS, &attachedShadersNum);
		ExsGLErrorCheck();

		return attachedShadersNum;
	}


	Size_t GLShaderProgramObject::GetInfoLogLength() const
	{
		ExsDebugAssert( this->CheckHandle() );

		GLint infoLogLength = 0;
		glGetProgramiv(this->_handle, GL_INFO_LOG_LENGTH, &infoLogLength);
		ExsGLErrorCheck();

		return infoLogLength;
	}


	void GLShaderProgramObject::ClearBinding()
	{
		glUseProgram(0);
		ExsGLErrorCheck();
	}


}
