
#ifndef __Exs_GraphicsDriver_CommonGL_CommonApi_H__
#define __Exs_GraphicsDriver_CommonGL_CommonApi_H__


namespace Exs
{


	namespace GLUtils
	{

		//
		GLenum ChooseBufferUsageMode(GLenum bindTarget, stdx::mask<RSResourceUsageFlags> usageFlags);

	}


}


#endif /* __Exs_GraphicsDriver_CommonGL_CommonApi_H__ */
