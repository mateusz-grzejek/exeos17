
#include <ExsCommonGL/GL_RenderSystem.h>
#include <ExsCommonGL/GL_GraphicsDriver.h>
#include <ExsCommonGL/GL_Extensions.h>
#include <ExsCommonGL/Memory/GL_RSMemorySystem.h>
#include <ExsCommonGL/Resource/GL_GPUBufferStorage.h>
#include <ExsCommonGL/Resource/GL_SamplerInterface.h>
#include <ExsCommonGL/Resource/GL_TextureStorage.h>
#include <ExsCommonGL/State/GL_BindingCacheTable.h>
#include <ExsCommonGL/State/GL_BlendState.h>
#include <ExsCommonGL/State/GL_DepthStencilState.h>
#include <ExsCommonGL/State/GL_RasterizerState.h>
#include <ExsCommonGL/State/GL_VertexStreamState.h>
#include <ExsCommonGL/State/GL_VertexArrayObjectCache.h>
#include <ExsRenderSystem/RSStateDescriptorCache.h>
#include <ExsRenderSystem/Resource/ConstantBuffer.h>
#include <ExsRenderSystem/Resource/IndexBuffer.h>
#include <ExsRenderSystem/Resource/Sampler.h>
#include <ExsRenderSystem/Resource/ShaderResourceView.h>
#include <ExsRenderSystem/Resource/Texture.h>
#include <ExsRenderSystem/Resource/VertexBuffer.h>


namespace Exs
{
	
	
	GLRenderSystem::GLRenderSystem(GLGraphicsDriver* graphicDriver, GLSystemContext* systemContext, GLExtensionManager* extensionManager)
	: RenderSystem(graphicDriver, &(_glMemorySystem))
	, _glSystemContext(systemContext)
	, _glExtensionManager(extensionManager)
	, _glMemorySystem(this)
	{ }


	GLRenderSystem::~GLRenderSystem()
	{ }


	SamplerHandle GLRenderSystem::CreateSampler(const SamplerCreateInfo& createInfo)
	{
		if (auto samplerInterface = InitializeGLSamplerInterface(createInfo.parameters))
		{
			auto samplerObject = this->NewMiscObject<Sampler>(this, createInfo.parameters);
			samplerObject->BindInterface(samplerInterface);
			return samplerObject;
		}

		return nullptr;
	}


	ConstantBufferBindingCacheTableHandle GLRenderSystem::CreateConstantBufferBindingCacheTable(const ConstantBufferBinding* bindings, Uint32 bindingsNum)
	{
		return CreateGLConstantBufferBindingCacheTable(bindings, bindingsNum);
	}


	SamplerBindingCacheTableHandle GLRenderSystem::CreateSamplerBindingCacheTable(const SamplerBinding* bindings, Uint32 bindingsNum)
	{
		return CreateGLSamplerBindingCacheTable(bindings, bindingsNum);
	}


	ShaderResourceBindingCacheTableHandle GLRenderSystem::CreateShaderResourceBindingCacheTable(const ShaderResourceBinding* bindings, Uint32 bindingsNum)
	{
		return CreateGLShaderResourceBindingCacheTable(bindings, bindingsNum);
	}


	ConstantBufferHandle GLRenderSystem::CreateConstantBuffer(const ConstantBufferCreateInfo& createInfo)
	{
		if (createInfo.sizeInBytes == 0)
			return nullptr;

		RSResourceMemoryAllocationDesc allocationDesc;
		allocationDesc.size = createInfo.sizeInBytes;
		allocationDesc.resourceUsageFlags = createInfo.usageFlags;
		allocationDesc.access = RSMemoryAccess::Write_Only;
		allocationDesc.allocationTarget = RSMemoryAllocationTarget::Buffer;
		allocationDesc.targetInfo.buffer.type = GPUBufferType::Constant_Buffer;

		RSMemoryHandle memoryHandle = this->_glMemorySystem.AllocateResourceMemory(allocationDesc);
		ExsDebugAssert( memoryHandle != ExsRSMemoryHandleNone );

		GPUBufferStorageInitDesc bufferStorageInitDesc { };
		bufferStorageInitDesc.bufferType = GPUBufferType::Constant_Buffer;
		bufferStorageInitDesc.bufferSize = createInfo.sizeInBytes;
		bufferStorageInitDesc.usageFlags = createInfo.usageFlags;
		bufferStorageInitDesc.memoryInfo.access = RSMemoryAccess::Write_Only;
		bufferStorageInitDesc.memoryInfo.handle = memoryHandle;

		if (auto bufferInterface = InitGLGPUBufferInterface(GPUBufferType::Constant_Buffer))
		{
			GPUBufferDataDesc dataDesc;
			dataDesc.dataPtr = nullptr;
			dataDesc.dataSize = 0;

			if (auto bufferStorage = InitGLGPUBufferStorage(bufferInterface.get(), bufferStorageInitDesc, dataDesc))
			{
				auto constantBuffer = this->NewResource<ConstantBuffer>(this);
				constantBuffer->BindInterface(bufferInterface);
				constantBuffer->BindStorage(bufferStorage);
				return constantBuffer;
			}
		}

		return nullptr;
	}


	IndexBufferHandle GLRenderSystem::CreateIndexBuffer(const IndexBufferCreateInfo& createInfo)
	{
		if (createInfo.sizeInBytes == 0)
			return nullptr;

		RSResourceMemoryAllocationDesc allocationDesc;
		allocationDesc.size = createInfo.sizeInBytes;
		allocationDesc.resourceUsageFlags = createInfo.usageFlags;
		allocationDesc.access = createInfo.memoryAccess;
		allocationDesc.allocationTarget = RSMemoryAllocationTarget::Buffer;
		allocationDesc.targetInfo.buffer.type = GPUBufferType::Index_Buffer;

		RSMemoryHandle memoryHandle = this->_glMemorySystem.AllocateResourceMemory(allocationDesc);
		ExsDebugAssert( memoryHandle != ExsRSMemoryHandleNone );

		GPUBufferStorageInitDesc bufferStorageInitDesc { };
		bufferStorageInitDesc.bufferType = GPUBufferType::Index_Buffer;
		bufferStorageInitDesc.bufferSize = createInfo.sizeInBytes;
		bufferStorageInitDesc.usageFlags = createInfo.usageFlags;
		bufferStorageInitDesc.memoryInfo.access = createInfo.memoryAccess;
		bufferStorageInitDesc.memoryInfo.handle = memoryHandle;
		
		if (auto bufferInterface = InitGLGPUBufferInterface(GPUBufferType::Index_Buffer))
		{
			if (auto bufferStorage = InitGLGPUBufferStorage(bufferInterface.get(), bufferStorageInitDesc, createInfo.initDataDesc))
			{
				auto indexBuffer = this->NewResource<IndexBuffer>(this);
				indexBuffer->BindInterface(bufferInterface);
				indexBuffer->BindStorage(bufferStorage);
				return indexBuffer;
			}
		}

		return nullptr;
	}


	VertexBufferHandle GLRenderSystem::CreateVertexBuffer(const VertexBufferCreateInfo& createInfo)
	{
		if (createInfo.sizeInBytes == 0)
			return nullptr;

		RSResourceMemoryAllocationDesc allocationDesc;
		allocationDesc.size = createInfo.sizeInBytes;
		allocationDesc.resourceUsageFlags = createInfo.usageFlags;
		allocationDesc.access = createInfo.memoryAccess;
		allocationDesc.allocationTarget = RSMemoryAllocationTarget::Buffer;
		allocationDesc.targetInfo.buffer.type = GPUBufferType::Vertex_Buffer;

		RSMemoryHandle memoryHandle = this->_glMemorySystem.AllocateResourceMemory(allocationDesc);
		ExsDebugAssert( memoryHandle != ExsRSMemoryHandleNone );

		GPUBufferStorageInitDesc bufferStorageInitDesc;
		bufferStorageInitDesc.bufferType = GPUBufferType::Vertex_Buffer;
		bufferStorageInitDesc.bufferSize = createInfo.sizeInBytes;
		bufferStorageInitDesc.usageFlags = createInfo.usageFlags;
		bufferStorageInitDesc.memoryInfo.access = createInfo.memoryAccess;
		bufferStorageInitDesc.memoryInfo.handle = memoryHandle;
		
		if (auto bufferInterface = InitGLGPUBufferInterface(GPUBufferType::Vertex_Buffer))
		{
			if (auto bufferStorage = InitGLGPUBufferStorage(bufferInterface.get(), bufferStorageInitDesc, createInfo.initDataDesc))
			{
				auto vertexBuffer = this->NewResource<VertexBuffer>(this);
				vertexBuffer->BindInterface(bufferInterface);
				vertexBuffer->BindStorage(bufferStorage);
				return vertexBuffer;
			}
		}

		return nullptr;
	}


	Texture2DHandle GLRenderSystem::CreateTexture2D(const Texture2DCreateInfo& createInfo)
	{
		if ((createInfo.width == 0) || (createInfo.height == 0))
			return nullptr;

		RSResourceMemoryAllocationDesc allocationDesc;
		Uint32 texelSize = GetGraphicDataFormatSize(createInfo.internalFormat);
		allocationDesc.size = texelSize * createInfo.width * createInfo.height;
		allocationDesc.access = createInfo.memoryAccess;
		allocationDesc.resourceUsageFlags = createInfo.usageFlags;
		allocationDesc.allocationTarget = RSMemoryAllocationTarget::Texture;
		allocationDesc.targetInfo.texture.type = TextureType::Tex_2D;
		allocationDesc.targetInfo.texture.isMSAAEnabled = false;
		allocationDesc.targetInfo.texture.isRTDSTexture = false;

		RSMemoryHandle memoryHandle = this->_glMemorySystem.AllocateResourceMemory(allocationDesc);
		ExsDebugAssert( memoryHandle != ExsRSMemoryHandleNone );

		TextureStorageInitDesc storageInitDesc;
		storageInitDesc.textureType = TextureType::Tex_2D;
		storageInitDesc.textureFormat = createInfo.internalFormat;
		storageInitDesc.textureDimensions.size.x = createInfo.width;
		storageInitDesc.textureDimensions.size.y = createInfo.height;
		storageInitDesc.textureDimensions.depth = 1;
		storageInitDesc.textureDimensions.arraySize = 1;
		storageInitDesc.mipmapLevelsNum = createInfo.mipmapLevelsNum;
		storageInitDesc.msaaSamplesNum = 0;
		storageInitDesc.usageFlags = createInfo.usageFlags;
		storageInitDesc.memoryInfo.access = createInfo.memoryAccess;
		storageInitDesc.memoryInfo.handle = memoryHandle;

		if (auto textureInterface = InitGLTextureInterface(TextureType::Tex_2D))
		{
			if (auto textureStorage = InitGLTextureStorage2D(textureInterface.get(), storageInitDesc, createInfo.initDataDesc))
			{
				auto texture = this->NewResource<Texture2D>(this, createInfo.internalFormat);
				texture->BindInterface(textureInterface);
				texture->BindStorage(textureStorage);
				return texture;
			}
		}

		return nullptr;
	}


	Texture3DHandle GLRenderSystem::CreateTexture3D(const Texture3DCreateInfo& createInfo)
	{
		return nullptr;
	}


	Texture2DArrayHandle GLRenderSystem::CreateTexture2DArray(const Texture2DArrayCreateInfo& createInfo)
	{
		if ((createInfo.width == 0) || (createInfo.height == 0) || (createInfo.arraySize == 0))
			return nullptr;

		RSResourceMemoryAllocationDesc allocationDesc;
		Uint32 texelSize = GetGraphicDataFormatSize(createInfo.internalFormat);
		allocationDesc.size = texelSize * createInfo.width * createInfo.height * createInfo.arraySize;
		allocationDesc.access = createInfo.memoryAccess;
		allocationDesc.resourceUsageFlags = createInfo.usageFlags;
		allocationDesc.allocationTarget = RSMemoryAllocationTarget::Texture;
		allocationDesc.targetInfo.texture.type = TextureType::Tex_2D_Array;
		allocationDesc.targetInfo.texture.isMSAAEnabled = false;
		allocationDesc.targetInfo.texture.isRTDSTexture = false;

		RSMemoryHandle memoryHandle = this->_glMemorySystem.AllocateResourceMemory(allocationDesc);
		ExsDebugAssert( memoryHandle != ExsRSMemoryHandleNone );

		TextureStorageInitDesc storageInitDesc;
		storageInitDesc.textureType = TextureType::Tex_2D_Array;
		storageInitDesc.textureFormat = createInfo.internalFormat;
		storageInitDesc.textureDimensions.size.x = createInfo.width;
		storageInitDesc.textureDimensions.size.y = createInfo.height;
		storageInitDesc.textureDimensions.depth = 1;
		storageInitDesc.textureDimensions.arraySize = createInfo.arraySize;
		storageInitDesc.mipmapLevelsNum = createInfo.mipmapLevelsNum;
		storageInitDesc.msaaSamplesNum = 0;
		storageInitDesc.usageFlags = createInfo.usageFlags;
		storageInitDesc.memoryInfo.access = createInfo.memoryAccess;
		storageInitDesc.memoryInfo.handle = memoryHandle;

		if (auto textureInterface = InitGLTextureInterface(TextureType::Tex_2D_Array))
		{
			if (auto textureStorage = InitGLTextureStorage2DArray(textureInterface.get(), storageInitDesc, createInfo.initDataDesc))
			{
				auto texture = this->NewResource<Texture2DArray>(this, createInfo.internalFormat);
				texture->BindInterface(textureInterface);
				texture->BindStorage(textureStorage);
				return texture;
			}
		}

		return nullptr;
	}


	ShaderResourceViewHandle GLRenderSystem::CreateTextureShaderResourceView(const Texture2DHandle& texture2D)
	{
		auto textureView = this->NewResourceView<ShaderResourceView>(this, ShaderResourceViewResType::Texture_2D, texture2D);
		return textureView;
	}


	ShaderResourceViewHandle GLRenderSystem::CreateTextureShaderResourceView(const Texture2DArrayHandle& texture2DArray)
	{
		auto textureView = this->NewResourceView<ShaderResourceView>(this, ShaderResourceViewResType::Texture_2D_Array, texture2DArray);
		return textureView;
	}


	BlendStateDescriptorHandle GLRenderSystem::CreateBlendStateDescriptor(const BlendConfiguration& configuration)
	{
		// auto blendStateInfo = this->_rsStateDescriptorCache.GetBlendStateDescriptor(configuration);
		auto descriptorInfo = this->_rsStateDescriptorCache.GetDescriptor<RSStateDescriptorType::Blend>(configuration);

		if (descriptorInfo.handle)
			return descriptorInfo.handle;

		if (!GLBlendStateDescriptor::ValidateConfiguration(configuration))
			return nullptr;

		GLBlendConfiguration blendConfigGL { };
		if (TranslateGLBlendConfiguration(configuration, &blendConfigGL))
		{
			auto descriptor = this->NewStateDescriptor<GLBlendStateDescriptor>(this, descriptorInfo.pid, configuration, blendConfigGL);
			return descriptor;
		};

		return nullptr;
	}


	DepthStencilStateDescriptorHandle GLRenderSystem::CreateDepthStencilStateDescriptor(const DepthStencilConfiguration& configuration)
	{
		// auto depthStencilStateInfo = this->_rsStateDescriptorCache.GetDepthStencilStateDescriptor(configuration);
		auto descriptorInfo = this->_rsStateDescriptorCache.GetDescriptor<RSStateDescriptorType::Depth_Stencil>(configuration);

		if (descriptorInfo.handle)
			return descriptorInfo.handle;

		if (!GLDepthStencilStateDescriptor::ValidateConfiguration(configuration))
			return nullptr;

		GLDepthStencilConfiguration depthStencilConfigGL { };
		if (TranslateGLDepthStencilConfiguration(configuration, &depthStencilConfigGL))
		{
			auto descriptor = this->NewStateDescriptor<GLDepthStencilStateDescriptor>(this, descriptorInfo.pid, configuration, depthStencilConfigGL);
			return descriptor;
		}

		return nullptr;
	}


	RasterizerStateDescriptorHandle GLRenderSystem::CreateRasterizerStateDescriptor(const RasterizerConfiguration& configuration)
	{
		// auto rasterizerStateInfo = this->_rsStateDescriptorCache.GetRasterizerStateDescriptor(configuration);
		auto descriptorInfo = this->_rsStateDescriptorCache.GetDescriptor<RSStateDescriptorType::Rasterizer>(configuration);

		if (descriptorInfo.handle)
			return descriptorInfo.handle;

		if (!GLRasterizerStateDescriptor::ValidateConfiguration(configuration))
			return nullptr;

		GLRasterizerConfiguration rasterizerConfigGL { };
		if (TranslateGLRasterizerConfiguration(configuration, &rasterizerConfigGL))
		{
			auto descriptor = this->NewStateDescriptor<GLRasterizerStateDescriptor>(this, descriptorInfo.pid, configuration, rasterizerConfigGL);
			return descriptor;
		}

		return nullptr;
	}


	VertexStreamStateDescriptorHandle GLRenderSystem::CreateVertexStreamStateDescriptor(const VertexStreamConfiguration& configuration)
	{
		// auto vertexBufferStateInfo = this->_rsStateDescriptorCache.GetVertexStreamStateDescriptor(configuration);
		auto descriptorInfo = this->_rsStateDescriptorCache.GetDescriptor<RSStateDescriptorType::Vertex_Stream>(configuration);

		if (descriptorInfo.handle)
			return descriptorInfo.handle;

		if (!GLVertexStreamStateDescriptor::ValidateConfiguration(configuration))
			return nullptr;

		GLVertexStreamConfiguration vertexBufferConfigGL { };
		if (TranslateGLVertexStreamConfiguration(configuration, &vertexBufferConfigGL))
		{
			auto descriptor = this->NewStateDescriptor<GLVertexStreamStateDescriptor>(this, descriptorInfo.pid, configuration, vertexBufferConfigGL);
			return descriptor;
		}

		return nullptr;
	}


	bool GLRenderSystem::InitializeMemorySystem()
	{
		Result initResult = this->_glMemorySystem.Initialize();
		return initResult == RSC_Success;
	}
	
	
}
