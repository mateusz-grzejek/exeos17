
#ifndef __Exs_GraphicsDriver_CommonGL_GLGPUBufferInterface_H__
#define __Exs_GraphicsDriver_CommonGL_GLGPUBufferInterface_H__

#include "../Objects/GL_BufferObject.h"
#include <ExsRenderSystem/Resource/GPUBufferBase.h>


namespace Exs
{


	///<summary>
	///</summary>
	class GLGPUBufferInterface : public RSBaseObjectInterface
	{
	private:
		std::unique_ptr<GLBufferObject>  _glBufferObject;
		GLuint                           _glBufferObjectHandle;

	public:
		GLGPUBufferInterface(GLGPUBufferInterface&&) = default;
		GLGPUBufferInterface(const GLGPUBufferInterface&) = default;

		GLGPUBufferInterface& operator=(GLGPUBufferInterface&&) = default;
		GLGPUBufferInterface& operator=(const GLGPUBufferInterface&) = default;

		GLGPUBufferInterface(std::unique_ptr<GLBufferObject>&& bufferObject)
		: RSBaseObjectInterface()
		, _glBufferObject(std::move(bufferObject))
		, _glBufferObjectHandle(_glBufferObject->GetHandle())
		{ }

		virtual ~GLGPUBufferInterface()
		{ }

		GLBufferObject* GetGLBufferObject() const
		{
			return this->_glBufferObject.get();
		}

		GLuint GetGLBufferObjectHandle() const
		{
			return this->_glBufferObjectHandle;
		}

		bool IsInitialized() const
		{
			return this->_glBufferObject ? (this->_glBufferObjectHandle > 0) : false;
		}
	};
	

	RSInternalHandle<GLGPUBufferInterface> InitGLGPUBufferInterface(GPUBufferType bufferType);

}


#endif /* __Exs_GraphicsDriver_CommonGL_GLGPUBufferInterface_H__ */
