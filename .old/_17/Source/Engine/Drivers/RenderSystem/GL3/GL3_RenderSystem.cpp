
#include <ExsRenderSystem/RSStateDescriptorCache.h>
#include <ExsRenderSystem/RSThreadManager.h>
#include <ExsRenderSystem/RSThreadState.h>
#include <ExsRenderSystem/Resource/Shader.h>
#include <ExsCommonGL/GL_SystemLayer.h>
#include <ExsCommonGL/GL_RSThreadState.h>
#include <ExsGraphicsDriverGL3/GL3_RenderSystem.h>
#include <ExsGraphicsDriverGL3/GL3_GraphicsDriver.h>
#include <ExsGraphicsDriverGL3/GL3_RSContextCommandQueue.h>
#include <ExsGraphicsDriverGL3/GL3_RSContextStateController.h>
#include <ExsGraphicsDriverGL3/Resource/GL3_ShaderInterface.h>
#include <ExsGraphicsDriverGL3/State/GL3_GraphicsShaderState.h>
#include <ExsGraphicsDriverGL3/State/GL3_InputLayoutState.h>


namespace Exs
{
	
	
	GL3RenderSystem::GL3RenderSystem(GL3GraphicsDriver* graphicDriver, GLSystemContext* systemContext, GLExtensionManager* extensionManager)
	: GLRenderSystem(graphicDriver, systemContext, extensionManager)
	{ }


	GL3RenderSystem::~GL3RenderSystem()
	{ }


	ShaderHandle GL3RenderSystem::CreateShader(const ShaderCreateInfo& createInfo)
	{
		if (auto shaderInterface = InitGL3ShaderInterface(createInfo))
		{
			ShaderBytecode shaderBytecode { };

			if (createInfo.createFlags.is_set(ShaderCreate_Preserve_Bytecode))
				shaderBytecode.assign(reinterpret_cast<const Byte*>(createInfo.shaderCode), createInfo.shaderCodeSize);

			auto shader = this->NewMiscObject<Shader>(this, createInfo.shaderType, std::move(shaderBytecode));
			shader->BindInterface(shaderInterface);

			return shader;
		}

		return nullptr;
	}


	GraphicsShaderStateDescriptorHandle GL3RenderSystem::CreateGraphicsShaderStateDescriptor(const GraphicsShaderConfiguration& configuration)
	{
		// auto graphicsShaderStateInfo = this->_rsStateDescriptorCache->GetGraphicsShaderStateDescriptor(configuration);
		auto descriptorInfo = this->_rsStateDescriptorCache.GetDescriptor<RSStateDescriptorType::Graphics_Shader>(configuration);

		if (descriptorInfo.handle)
			return descriptorInfo.handle;

		if (!GL3GraphicsShaderStateDescriptor::ValidateConfiguration(configuration))
			return nullptr;

		GLGraphicsShaderConfiguration graphicsShaderConfigGL { };
		if (TranslateGLGraphicsShaderConfiguration(configuration, &graphicsShaderConfigGL))
		{
			GLShaderProgramDataBindingDesc dataBindingDesc;
			dataBindingDesc.attribBindingArray = configuration.customAttribBindingArray;
			dataBindingDesc.samplerBindingArray = configuration.customSamplerBindingArray;
			dataBindingDesc.uniformBlockBindingArray = configuration.customUniformBlockBindingArray;
			dataBindingDesc.attribBindingsNum = 0;
			dataBindingDesc.samplerBindingsNum = 0;
			dataBindingDesc.uniformBlockBindingsNum = 0;

			for (auto* binding = dataBindingDesc.attribBindingArray; !GraphicsShaderDataBindingIsEmpty(*binding); ++binding)
				++dataBindingDesc.attribBindingsNum;

			for (auto* binding = dataBindingDesc.samplerBindingArray; !GraphicsShaderDataBindingIsEmpty(*binding); ++binding)
				++dataBindingDesc.samplerBindingsNum;

			for (auto* binding = dataBindingDesc.uniformBlockBindingArray; !GraphicsShaderDataBindingIsEmpty(*binding); ++binding)
				++dataBindingDesc.uniformBlockBindingsNum;

			auto shaderProgramObject = BuildGL3ShaderProgramObject(graphicsShaderConfigGL, dataBindingDesc);
			if (shaderProgramObject != nullptr)
			{
				auto descriptor = this->NewStateDescriptor<GL3GraphicsShaderStateDescriptor>(this, descriptorInfo.pid, configuration, graphicsShaderConfigGL, std::move(shaderProgramObject));
				return descriptor;
			}
		}

		return nullptr;
	}


	InputLayoutStateDescriptorHandle GL3RenderSystem::CreateInputLayoutStateDescriptor( const InputLayoutConfiguration& configuration,
	                                                                                    GraphicsShaderStateDescriptorHandle vsSignatureDescriptor)
	{
		// auto inputLayoutStateInfo = this->_rsStateDescriptorCache->GetInputLayoutStateDescriptor(configuration);
		auto descriptorInfo = this->_rsStateDescriptorCache.GetDescriptor<RSStateDescriptorType::Input_Layout>(configuration);

		if (descriptorInfo.handle)
			return descriptorInfo.handle;

		if (!GL3InputLayoutStateDescriptor::ValidateConfiguration(configuration))
			return nullptr;

		auto vsSignatureDescriptorGL = stdx::dbgsafe_shared_ptr_cast<GL3GraphicsShaderStateDescriptor>(vsSignatureDescriptor);
		const auto* vertexShaderProgram = vsSignatureDescriptorGL->GetGLShaderProgramObject();

		GLInputLayoutConfiguration inputLayoutConfigGL { };
		if (TranslateGLInputLayoutConfiguration(configuration, *vertexShaderProgram, *(this->_glExtensionManager), &inputLayoutConfigGL))
		{
			auto descriptor = this->NewStateDescriptor<GL3InputLayoutStateDescriptor>(this, descriptorInfo.pid, configuration, inputLayoutConfigGL);
			return descriptor;
		}

		return nullptr;
	}

	std::unique_ptr<RSThreadState> GL3RenderSystem::InitializeRSThreadState(RSThreadIndex threadIndex, RSThreadType threadType)
	{
		if (threadType != RSThreadType::Main)
		{
			// This should never throw!
			ExsExceptionThrowEx(EXC_Invalid_Operation, "OpenGL3 does not support multi-threading!");
		}

		RSThreadManager* rsThreadManager = this->_rsThreadManager.get();
		GLSystemContext* oglSystemContext = this->GetGLSystemContext();

		GLRSThreadState* threadState = new GLRSThreadState(this, rsThreadManager, threadType, threadIndex, oglSystemContext);
		std::unique_ptr<RSThreadState> threadStateGuardPtr(threadState);

		GL3RSContextStateController* contextStateController =
			threadState->CreateStateController<GL3RSContextStateController>(this, threadState, &(this->_glVertexArrayObjectCache));

		GL3RSContextCommandQueue* contextCommandQueue =
			threadState->CreateCommandQueue<GL3RSContextCommandQueue>(this, threadState, contextStateController);
			
		return threadStateGuardPtr;
	}
	
	
}
