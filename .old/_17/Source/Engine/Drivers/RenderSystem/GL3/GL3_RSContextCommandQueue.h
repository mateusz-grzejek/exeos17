
#ifndef __Exs_GraphicsDriver_GL3_RSContextCommandQueue_H__
#define __Exs_GraphicsDriver_GL3_RSContextCommandQueue_H__

#include "GL3_Prerequisites.h"
#include <ExsCommonGL/GL_RSContextCommandQueue.h>


namespace Exs
{


	class GL3RSContextStateController;


	class GL3RSContextCommandQueue : public GLRSContextCommandQueue
	{
	public:
		GL3RSContextCommandQueue(GL3RenderSystem* renderSystem, GLRSThreadState* rsThreadState, GL3RSContextStateController* rsContextStateController);
		virtual ~GL3RSContextCommandQueue();

		virtual void Draw(Uint verticesOffset, Size_t verticesCount) override;
		virtual void DrawIndexed(Uint verticesOffset, Size_t verticesCount, IndexBufferDataType indicesType) override;
	};


}



#endif /* __Exs_GraphicsDriver_CommonGL_RSContextCommandQueue_H__ */
