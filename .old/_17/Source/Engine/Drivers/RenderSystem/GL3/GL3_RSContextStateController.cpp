
#include <ExsRenderSystem/Resource/Sampler.h>
#include <ExsRenderSystem/Resource/Texture.h>
#include <ExsRenderSystem/State/GraphicsPipelineStateObject.h>
#include <ExsRenderSystem/State/VertexArrayStateObject.h>
#include <ExsCommonGL/Objects/GL_SamplerObject.h>
#include <ExsCommonGL/Objects/GL_ShaderProgramObject.h>
#include <ExsCommonGL/Resource/GL_TextureStorage.h>
#include <ExsCommonGL/State/GL_VertexArrayObjectCache.h>
#include <ExsCommonGL/State/GL_VertexStreamState.h>
#include <ExsGraphicsDriverGL3/GL3_RSContextStateController.h>
#include <ExsGraphicsDriverGL3/GL3_RenderSystem.h>
#include <ExsGraphicsDriverGL3/State/GL3_GraphicsShaderState.h>
#include <ExsGraphicsDriverGL3/State/GL3_InputLayoutState.h>


namespace Exs
{


	GL3RSContextStateController::GL3RSContextStateController(GL3RenderSystem* renderSystem, RSThreadState* rsThreadState, GLVertexArrayObjectCache* vertexArrayObjectCache)
	: GLRSContextStateController(renderSystem, rsThreadState, vertexArrayObjectCache)
	, _primitiveTopology(0)
	, _currentShaderProgramObject(nullptr)
	{ }


	GL3RSContextStateController::~GL3RSContextStateController()
	{ }


	bool GL3RSContextStateController::ApplyInternalRenderState()
	{
		if (GLRSContextStateController::ApplyInternalRenderState())
		{
			if (this->_updateMask.is_set(RSStateUpdate_Descriptor_Graphics_Shader))
			{
				auto* oglGraphicsShaderStateDescriptor = this->_stateDescriptors.graphicsShader->As<GL3GraphicsShaderStateDescriptor>();
				this->UpdateGraphicsShaderState(oglGraphicsShaderStateDescriptor);
			}
		
			if (this->_updateMask.is_set(RSStateUpdate_Descriptor_Input_Layout) || this->_updateMask.is_set(RSStateUpdate_Descriptor_Vertex_Stream))
			{
				auto* oglInputLayoutStateDescriptor = this->_stateDescriptors.inputLayout->As<GL3InputLayoutStateDescriptor>();
				auto* oglVertexStreamStateDescriptor = this->_stateDescriptors.vertexStream->As<GLVertexStreamStateDescriptor>();
				this->UpdateVertexInputState(oglInputLayoutStateDescriptor, oglVertexStreamStateDescriptor);
			}

			this->_updateMask = 0;
			return true;
		}

		return false;
	}


	void GL3RSContextStateController::UpdateGraphicsShaderState(GL3GraphicsShaderStateDescriptor* graphicsShaderStateDescriptor)
	{
		const auto* shaderProgramObject = graphicsShaderStateDescriptor->GetGLShaderProgramObject();
		BindShaderProgramObject(shaderProgramObject);

		this->_currentShaderProgramObject = shaderProgramObject;
	}


	void GL3RSContextStateController::UpdateVertexInputState(GL3InputLayoutStateDescriptor* inputLayoutStateDescriptor, GLVertexStreamStateDescriptor* vertexStreamStateDescriptor)
	{
		const auto* vertexArrayObject = this->_glVertexArrayObjectCache->GetVertexArrayObject(inputLayoutStateDescriptor, vertexStreamStateDescriptor);
		BindVertexArrayObject(vertexArrayObject);

		this->_primitiveTopology = inputLayoutStateDescriptor->GetGLConfiguration().primitiveTopology;
	}


}
