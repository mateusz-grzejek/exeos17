
#ifndef __Exs_GraphicsDriver_GL3_InputLayoutState_H__
#define __Exs_GraphicsDriver_GL3_InputLayoutState_H__

#include "../GL3_Prerequisites.h"
#include <ExsCommonGL/State/GL_InputLayoutState.h>


namespace Exs
{


	ExsDeclareRSClassObjHandle(GL3InputLayoutStateDescriptor);


	class GL3InputLayoutStateDescriptor : public GLInputLayoutStateDescriptor
	{
		EXS_DECLARE_NONCOPYABLE(GL3InputLayoutStateDescriptor);

	public:
		//
		GL3InputLayoutStateDescriptor( GL3RenderSystem* renderSystem,
		                               RSBaseObjectID baseObjectID,
		                               RSStateDescriptorPID descriptorPID,
		                               const InputLayoutConfiguration& inputLayoutConfig,
		                               const GLInputLayoutConfiguration& inputLayoutConfigGL);

		//
		virtual ~GL3InputLayoutStateDescriptor();
	};


};


#endif /* __Exs_GraphicsDriver_CommonGL_InputLayoutState_H__ */
