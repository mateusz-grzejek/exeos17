
#include <ExsGraphicsDriverGL3/GL3_RenderSystem.h>
#include <ExsGraphicsDriverGL3/State/GL3_InputLayoutState.h>


namespace Exs
{


	GL3InputLayoutStateDescriptor::GL3InputLayoutStateDescriptor( GL3RenderSystem* renderSystem,
	                                                              RSBaseObjectID baseObjectID,
	                                                              RSStateDescriptorPID descriptorPID,
	                                                              const InputLayoutConfiguration& inputLayoutConfig,
	                                                              const GLInputLayoutConfiguration& inputLayoutConfigGL)
	: GLInputLayoutStateDescriptor(renderSystem, baseObjectID, descriptorPID, inputLayoutConfig, inputLayoutConfigGL)
	{ }


	GL3InputLayoutStateDescriptor::~GL3InputLayoutStateDescriptor()
	{ }


}
