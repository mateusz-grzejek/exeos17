
#ifndef __Exs_GraphicsDriver_GL3_GraphicsDriver_H__
#define __Exs_GraphicsDriver_GL3_GraphicsDriver_H__

#include "GL3_Prerequisites.h"
#include <ExsCommonGL/GL_GraphicsDriver.h>


namespace Exs
{


	class GL3GraphicsDriver : public GLGraphicsDriver
	{
	public:
		GL3GraphicsDriver(const GraphicsDriverCapabilitiesDesc& capabilitiesDesc);
		virtual ~GL3GraphicsDriver();

	private:
		/// @override GLGraphicsDriver::InitializeRSState
		virtual void InitializeRSState() override final;
	};


}


#endif /* __Exs_GraphicsDriver_GL3_GraphicsDriver_H__ */
