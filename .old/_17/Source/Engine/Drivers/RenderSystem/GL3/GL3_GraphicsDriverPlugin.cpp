
#include <ExsGraphicsDriverGL3/GL3_GraphicsDriverPlugin.h>
#include <ExsGraphicsDriverGL3/GL3_GraphicsDriver.h>


namespace Exs
{


	GL3GraphicsDriverPlugin::GL3GraphicsDriverPlugin(GraphicsDriverPluginManager* manager)
	: GraphicsDriverPlugin(manager)
	{ }


	GL3GraphicsDriverPlugin::~GL3GraphicsDriverPlugin()
	{ }


	GraphicsDriverRefPtr GL3GraphicsDriverPlugin::CreateGraphicsDriver()
	{
		if (this->_graphicDriver)
			return this->_graphicDriver;

		GraphicsDriverCapabilitiesDesc capabilitiesDesc { };
		this->_graphicDriver = std::make_shared<GL3GraphicsDriver>(capabilitiesDesc);

		return this->_graphicDriver;
	}


}
