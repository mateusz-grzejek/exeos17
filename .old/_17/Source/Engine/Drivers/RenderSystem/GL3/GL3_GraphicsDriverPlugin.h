
#ifndef __Exs_GraphicsDriver_GL3_GraphicsDriverPlugin_H__
#define __Exs_GraphicsDriver_GL3_GraphicsDriverPlugin_H__

#include "GL3_Prerequisites.h"
#include <ExsRenderSystem/GraphicsDriverPlugin.h>


namespace Exs
{


	class GL3GraphicsDriverPlugin : public GraphicsDriverPlugin
	{
	public:
		GL3GraphicsDriverPlugin(GraphicsDriverPluginManager* manager);
		virtual ~GL3GraphicsDriverPlugin();

		virtual GraphicsDriverRefPtr CreateGraphicsDriver() override final;
	};


}


#endif /* __Exs_GraphicsDriver_GL3_RendererPlugin_H__ */
