
#include <ExsRenderSystem/State/GraphicsPipelineStateObject.h>
#include <ExsRenderSystem/State/VertexArrayStateObject.h>
#include <ExsGraphicsDriverGL3/State/GL3_InputLayoutState.h>
#include <ExsGraphicsDriverGL3/GL3_RSContextCommandQueue.h>
#include <ExsGraphicsDriverGL3/GL3_RenderSystem.h>
#include <ExsGraphicsDriverGL3/GL3_RSContextStateController.h>


namespace Exs
{


	GL3RSContextCommandQueue::GL3RSContextCommandQueue(GL3RenderSystem* renderSystem, GLRSThreadState* rsThreadState, GL3RSContextStateController* rsContextStateController)
	: GLRSContextCommandQueue(renderSystem, rsThreadState, rsContextStateController)
	{ }


	GL3RSContextCommandQueue::~GL3RSContextCommandQueue()
	{ }


	void GL3RSContextCommandQueue::Draw(Uint verticesOffset, Size_t verticesCount)
	{
		auto* stateController = dbgsafe_ptr_cast<GL3RSContextStateController*>(this->_rsContextStateController);
		stateController->ApplyInternalRenderState();

		GLenum primitiveTopology = stateController->GetPrimitiveTopology();

		glDrawArrays(primitiveTopology, truncate_cast<GLint>(verticesOffset), truncate_cast<GLsizei>(verticesCount));
		ExsGLErrorCheck();
	}


	void GL3RSContextCommandQueue::DrawIndexed(Uint verticesOffset, Size_t verticesCount, IndexBufferDataType indicesType)
	{
		auto* stateController = dbgsafe_ptr_cast<GL3RSContextStateController*>(this->_rsContextStateController);
		stateController->ApplyInternalRenderState();

		GLenum primitiveTopology = stateController->GetPrimitiveTopology();
		GLenum indicesDataType = GLConstantMap::GetGraphicDataBaseType(static_cast<GraphicDataBaseType>(indicesType));

		glDrawElements(primitiveTopology, truncate_cast<GLsizei>(verticesCount), indicesDataType, reinterpret_cast<void*>(verticesOffset));
		ExsGLErrorCheck();
	}


}
