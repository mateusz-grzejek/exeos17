
#include <ExsGraphicsDriverGL3/GL3_GraphicsDriver.h>
#include <ExsGraphicsDriverGL3/GL3_RenderSystem.h>


namespace Exs
{



	GL3GraphicsDriver::GL3GraphicsDriver(const GraphicsDriverCapabilitiesDesc& capabilitiesDesc)
	: GLGraphicsDriver(capabilitiesDesc)
	{ }


	GL3GraphicsDriver::~GL3GraphicsDriver()
	{ }


	void GL3GraphicsDriver::InitializeRSState()
	{
		auto gl3RenderSystem = std::make_shared<GL3RenderSystem>(this, this->_glMainSystemContext.get(), this->_glExtensionManager.get());
		gl3RenderSystem->InitializeMemorySystem();

		this->_renderSystem = gl3RenderSystem;
	}


}
