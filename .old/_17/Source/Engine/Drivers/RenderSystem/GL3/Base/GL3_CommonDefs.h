
#ifndef __Exs_GraphicsDriver_GL3_CommonDefs_H__
#define __Exs_GraphicsDriver_GL3_CommonDefs_H__


namespace Exs
{


	class GL3RenderSystem;


	enum : TraceCategoryID
	{
		TRC_GraphicsDriver_GL3 = EXS_GL_TARGET_VERSION_GL3
	};


}


#endif /* __Exs_GraphicsDriver_GL3_CommonDefs_H__ */
