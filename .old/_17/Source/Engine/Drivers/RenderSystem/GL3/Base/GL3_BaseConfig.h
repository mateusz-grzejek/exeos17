
#ifndef __Exs_GraphicsDriver_GL3_BaseConfig_H__
#define __Exs_GraphicsDriver_GL3_BaseConfig_H__

#include <ExsCommonGL/GL_Prerequisites.h>


#define EXS_LIBAPI_RGL3       EXS_ATTR_DLL_EXPORT
#define EXS_LIBCLASS_RGL3     EXS_ATTR_DLL_EXPORT
#define EXS_LIBOBJECT_RGL3    EXS_ATTR_DLL_EXPORT


#endif /* __Exs_GraphicsDriver_GL3_BaseConfig_H__ */
