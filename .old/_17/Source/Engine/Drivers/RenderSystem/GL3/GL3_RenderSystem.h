
#ifndef __Exs_GraphicsDriver_GL3_RenderSystem_H__
#define __Exs_GraphicsDriver_GL3_RenderSystem_H__

#include "GL3_Prerequisites.h"
#include <ExsCommonGL/GL_RenderSystem.h>


namespace Exs
{


	class GL3GraphicsDriver;
	class GL3RSThreadManager;


	class GL3RenderSystem : public GLRenderSystem
	{
		EXS_DECLARE_NONCOPYABLE(GL3RenderSystem);

	public:
		GL3RenderSystem(GL3GraphicsDriver* graphicDriver, GLSystemContext* systemContext, GLExtensionManager* extensionManager);
		virtual ~GL3RenderSystem();

		// @override RenderSystem::CreateShader
		virtual ShaderHandle CreateShader(const ShaderCreateInfo& createInfo) override;

		// @override RenderSystem::CreateGraphicsShaderStateDescriptor
		virtual GraphicsShaderStateDescriptorHandle CreateGraphicsShaderStateDescriptor(const GraphicsShaderConfiguration& configuration) override;

		// @override RenderSystem::CreateInputLayoutStateDescriptor
		virtual InputLayoutStateDescriptorHandle CreateInputLayoutStateDescriptor( const InputLayoutConfiguration& configuration,
		                                                                           GraphicsShaderStateDescriptorHandle vsSignatureDescriptor) override;

	private:
		//
		virtual std::unique_ptr<RSThreadState> InitializeRSThreadState(RSThreadIndex threadIndex, RSThreadType threadType) override;
	};


}


#endif /* __Exs_GraphicsDriver_GL3_RenderSystem_H__ */
