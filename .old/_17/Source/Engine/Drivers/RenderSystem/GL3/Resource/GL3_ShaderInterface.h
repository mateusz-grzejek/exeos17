
#ifndef __Exs_GraphicsDriver_GL3_ShaderInterface_H__
#define __Exs_GraphicsDriver_GL3_ShaderInterface_H__

#include "../GL3_Prerequisites.h"
#include <ExsCommonGL/Resource/GL_ShaderInterface.h>


namespace Exs
{


	struct ShaderCreateInfo;


	class GL3ShaderInterface : public GLShaderInterface
	{
	public:
		GL3ShaderInterface(std::unique_ptr<GLShaderObject>&& shaderObject)
		: GLShaderInterface(std::move(shaderObject))
		{ }

		virtual ~GL3ShaderInterface()
		{ }
	};


	RSInternalHandle<GL3ShaderInterface> InitGL3ShaderInterface(const ShaderCreateInfo& createInfo);


}


#endif /* __Exs_GraphicsDriver_GL3_ShaderInterface_H__ */
