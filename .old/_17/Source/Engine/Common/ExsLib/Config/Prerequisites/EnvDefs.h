
#ifndef __ExsLib_Config_EnvDefs_H__
#define __ExsLib_Config_EnvDefs_H__


#if defined( max )
#  undef max
#endif

#if defined( min )
#  undef min
#endif

//
#define friendapi private

//
#define EXS_CONCAT(x,y) x##y

//
#define EXS_MKSTR(x) #x

//
#define EXS_CONCAT2(x,y) EXS_CONCAT(x,y)

//
#define EXS_MKSTR2(x) EXS_MKSTR(x)

//
#define EXS_WSTR(text) EXS_CONCAT(L,text)

//
#define EXS_UNUSED(ref) (void)(ref)


#define EXS_DECLARE_NONCOPYABLE(Type) \
	public: Type(const Type&) = delete; Type& operator=(const Type&) = delete;


#if !( EXS_STATE_PRINT_FORMAT_SPECIFIERS_DEFINED )
#  define EXS_PFI32      "d"
#  define EXS_PFI32_HEX  "X"
#  define EXS_PFI32_OCT  "o"
#  define EXS_PFI64      "ll"
#  define EXS_PFI64_HEX  "llX"
#  define EXS_PFI64_OCT  "llo"
#  define EXS_PFU32      "u"
#  define EXS_PFU32_HEX  "X"
#  define EXS_PFU32_OCT  "o"
#  define EXS_PFU64      "llu"
#  define EXS_PFU64_HEX  "llX"
#  define EXS_PFU64_OCT  "llo"
#  define EXS_STATE_PRINT_FORMAT_SPECIFIERS_DEFINED 1
#endif


#if ( EXS_TARGET_64 )
#  define EXS_PFI      EXS_PFI64
#  define EXS_PFI_HEX  EXS_PFI64_HEX
#  define EXS_PFI_OCT  EXS_PFI64_OCT
#  define EXS_PFU      EXS_PFU64
#  define EXS_PFU_HEX  EXS_PFU64_HEX
#  define EXS_PFU_OCT  EXS_PFU64_OCT
#else
#  define EXS_PFI      EXS_PFI32
#  define EXS_PFI_HEX  EXS_PFI32_HEX
#  define EXS_PFI_OCT  EXS_PFI32_OCT
#  define EXS_PFU      EXS_PFU32
#  define EXS_PFU_HEX  EXS_PFU32_HEX
#  define EXS_PFU_OCT  EXS_PFU32_OCT
#endif


#define EXS_NCID_RESERVED_MASK       0xFF000000
#define EXS_NCID_GET_RESERVED(ncid)  ((Enum)(ncid) & EXS_NCID_RESERVED_MASK)


#if !defined( EXS_HW_DESTRUCTIVE_CACHE_INTERFERENCE_SIZE )
#  if defined( EXS_CONFIG_HW_DEFAULT_DESTRUCTIVE_CACHE_INTERFERENCE_SIZE )
#    define EXS_HW_DESTRUCTIVE_CACHE_INTERFERENCE_SIZE  EXS_CONFIG_HW_DEFAULT_DESTRUCTIVE_CACHE_INTERFERENCE_SIZE
#  else
#    define EXS_HW_DESTRUCTIVE_CACHE_INTERFERENCE_SIZE  64
#  endif
#endif


#if ( EXS_BUILD_SHARED_MODULES || EXS_USE_SHARED_MODULES )
#  define EXS_MODULE_EXPORT EXS_ATTR_DLL_EXPORT
#  define EXS_MODULE_IMPORT EXS_ATTR_DLL_IMPORT
#else
#  define EXS_MODULE_EXPORT
#  define EXS_MODULE_IMPORT
#endif


#endif /* __ExsLib_Config_EnvDefs_H__ */
