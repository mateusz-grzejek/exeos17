
#ifndef __ExsLib_Config_Specific_H__
#define __ExsLib_Config_Specific_H__

#if ( EXS_COMPILER & EXS_COMPILER_INTEL )
#  include "Specific/Compiler/Intel/Common.h"
#elif ( EXS_COMPILER & EXS_COMPILER_MSVC )
#  include "Specific/Compiler/MSVC/Common.h"
#  include "Specific/Compiler/MSVC/Crtdefs.h"
#  include "Specific/Compiler/MSVC/AVXx32.h"
#  include "Specific/Compiler/MSVC/SSE2x32.h"
#elif ( EXS_COMPILER & EXS_COMPILER_MINGW )
#  include "Specific/Compiler/MinGW/Common.h"
#elif ( EXS_COMPILER & EXS_COMPILER_GCC )
#  include "Specific/Compiler/GCC/Common.h"
#  include "Specific/Compiler/GCC/Crtdefs.h"
#  include "Specific/Compiler/GCC/Cxx1y.h"
#elif ( EXS_COMPILER & EXS_COMPILER_CLANG )
#  include "Specific/Compiler/Clang/Common.h"
#  include "Specific/Compiler/Clang/Crtdefs.h"
#endif

#endif /* __ExsLib_Config_Specific_H__ */
