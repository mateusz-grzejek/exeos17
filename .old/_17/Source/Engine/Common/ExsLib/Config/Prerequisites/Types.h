
#ifndef __ExsLib_Config_Types_H__
#define __ExsLib_Config_Types_H__


typedef uint8_t   Byte;
typedef int8_t    Int8;
typedef int16_t   Int16;
typedef int32_t   Int32;
typedef int64_t   Int64;
typedef uint8_t   Uint8;
typedef uint16_t  Uint16;
typedef uint32_t  Uint32;
typedef uint64_t  Uint64;


#if ( EXS_TARGET_64 )
typedef Int64 Int;
typedef Uint64 Uint;
#else
typedef w64 Int32 Int;
typedef w64 Uint32 Uint;
#endif


#if ( EXS_CONFIG_BASE_USE_DOUBLE_PRECISION )
typedef double Real;
#else
typedef float Real;
#endif


//
typedef Uint Size_t;

//
typedef Uint Ptrval_t;

//
typedef Int Ptrdiff_t;

//
typedef Uint32 Enum;


#define EXS_MAKEU16B(b1, b2) \
	((((Uint16)b1 & 0xFF) << 8) | ((Uint16)b2 & 0xFF))

#define EXS_MAKEU32B(b1, b2, b3, b4) \
	((((Uint32)b1 & 0xFF) << 24) | (((Uint32)b2 & 0xFF) << 16) | (((Uint32)b3 & 0xFF) << 8) | ((Uint32)b4 & 0xFF))

#define EXS_MAKEU32S(s1, s2) \
	((((Uint32)s1 & 0xFFFF) << 16) | ((Uint32)s2 & 0xFFFF))

#define EXS_U16_HIBYTE1(u16) \
	(Uint8)(((Uint16)u16 >> 8) & 0xFF)

#define EXS_U16_HIBYTE2(u16) \
	(Uint8)((Uint16)u16 & 0xFF)

#define EXS_U32_HIBYTE1(u32) \
	(Uint8)(((Uint32)u32 >> 24) & 0xFF)

#define EXS_U32_HIBYTE2(u32) \
	(Uint8)(((Uint32)u32 >> 16) & 0xFF)

#define EXS_U32_HIBYTE3(u32) \
	(Uint8)(((Uint32)u32 >> 8) & 0xFF)

#define EXS_U32_HIBYTE4(u32) \
	(Uint8)((Uint32)u32 & 0xFF)

#define EXS_U32_HIWORD1(u32) \
	(Uint16)(((Uint32)u32 >> 16) & 0xFFFF)

#define EXS_U32_HIWORD2(u32) \
	(Uint16)((Uint32)u32 & 0xFFFF)


#endif /* __ExsLib_Config_Types_H__ */
