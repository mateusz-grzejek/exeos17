
#if ( (EXS_COMPILER < EXS_COMPILER_MSVC_2013) && !EXS_TARGET_64 )

inline __m128i _mm_set_epi64x(__int64 s1, __int64 s0)
{
	__m128i result;
	__int64* r64 = reinterpret_cast<__int64*>(&result);
	
	r64[0] = s0;
	r64[1] = s1;

	return result;
}

inline __m128i _mm_set1_epi64x(Int64 scalar)
{
	__m128i result;
	__int64* r64 = reinterpret_cast<__int64*>(&result);
	
	r64[0] = scalar;
	r64[1] = scalar;

	return result;
}

#endif
