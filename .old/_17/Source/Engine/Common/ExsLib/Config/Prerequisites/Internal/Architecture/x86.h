
#ifndef __Exs_Config_Common_Architecture_X86_H__
#define __Exs_Config_Common_Architecture_X86_H__


#define EXS_TARGET_ARCHITECTURE_STR	"x86-32 / IA32"


#if !defined( EXS_MEMORY_BASE_ALIGNMENT )
#  define EXS_MEMORY_BASE_ALIGNMENT 4
#endif


#if !defined( EXS_HW_DESTRUCTIVE_CACHE_INTERFERENCE_SIZE )
#  define EXS_HW_DESTRUCTIVE_CACHE_INTERFERENCE_SIZE 64
#endif


#endif /* __Exs_Config_Common_Architecture_X86_H__ */
