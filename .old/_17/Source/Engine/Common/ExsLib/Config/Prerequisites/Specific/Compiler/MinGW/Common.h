
#define DEBUG_BREAK()       __asm("int $0x3");
#define DEBUG_OUTPUT(text)  printf("%s\n", text)
#define SLEEP(miliseconds)  Sleep(miliseconds)

#define EXS_BYTESWAP16  __builtin_bswap16
#define EXS_BYTESWAP32  __builtin_bswap32
#define EXS_BYTESWAP64  __builtin_bswap64

#define EXS_ROTL16(x, n) ((x << n) | (x >> (16-n)))
#define EXS_ROTL32(x, n) ((x << n) | (x >> (32-n)))
#define EXS_ROTL64(x, n) ((x << n) | (x >> (64-n)))

#define EXS_ROTR16(x, n) ((x >> n) | (x << (16-n)))
#define EXS_ROTR32(x, n) ((x >> n) | (x << (32-n)))
#define EXS_ROTR64(x, n) ((x >> n) | (x << (64-n)))
