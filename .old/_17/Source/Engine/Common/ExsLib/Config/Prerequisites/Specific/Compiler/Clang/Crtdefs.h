
#define fseek64    fseeko64
#define ftell64    ftello64
#define snwprintf  swprintf


inline float wcstof(const wchar_t* str, wchar_t** endPtr)
{
	double dbl_value = wcstod(str, endPtr);
	return static_cast<float>(dbl_value);
}
