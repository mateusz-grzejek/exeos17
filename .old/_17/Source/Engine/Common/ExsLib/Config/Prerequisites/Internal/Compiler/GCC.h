
#ifndef __Exs_Config_Common_Compiler_GCC_H__
#define __Exs_Config_Common_Compiler_GCC_H__


#define EXS_COMPILER_STR "GNU GCC"

#include <signal.h>
#include <unistd.h>


#define EXS_FILE  __FILE__
#define EXS_FUNC  __FUNCTION__
#define EXS_LINE  __LINE__


#define EXS_ATTR_ALIGN(n)                 alignas(n)
#define EXS_ATTR_DEPRECATED(msg, repl)    __attribute__((deprecated(msg,repl)))
#define EXS_ATTR_NO_RETURN                __attribute__((noreturn))
#define EXS_ATTR_THREAD_LOCAL             __thread


#define EXS_ATTR_DLL_EXPORT __attribute__((dllexport))
#define EXS_ATTR_DLL_IMPORT __attribute__((dllimport))


#if ( EXS_CONFIG_BASE_FORCE_INLINE )
#  define ExsForceInline __attribute__((always_inline))
#else
#  define ExsForceInline inline
#endif


#define gnoexcept noexcept
#define w64

#define __cdecl


#define EXS_DECLARE_NONCOPYABLE(Type) \
	public: Type(const Type&) = delete; Type& operator=(const Type&) = delete;


#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#pragma GCC diagnostic ignored "-Wmissing-braces"
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#pragma GCC diagnostic ignored "-Wunused-value"
#pragma GCC diagnostic ignored "-Wunused-parameter"


#endif /* __Exs_Config_Common_Compiler_GCC_H__ */
