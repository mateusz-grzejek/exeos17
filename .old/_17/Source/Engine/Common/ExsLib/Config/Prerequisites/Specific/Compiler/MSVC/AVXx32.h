
#if ( (EXS_COMPILER < EXS_COMPILER_MSVC_2013) && !EXS_TARGET_64 )

inline __m256i __cdecl _mm256_set_epi64x(__int64 s3, __int64 s2, __int64 s1, __int64 s0)
{
	__m256i result;
	__int64* r64 = reinterpret_cast<__int64*>(&result);
	
	r64[0] = s0;
	r64[1] = s1;
	r64[2] = s2;
	r64[3] = s3;

	return result;
}

inline __m256i __cdecl _mm256_set1_epi64x(__int64 scalar)
{
	__m256i result;
	__int64* r64 = reinterpret_cast<__int64*>(&result);
	
	r64[0] = scalar;
	r64[1] = scalar;
	r64[2] = scalar;
	r64[3] = scalar;

	return result;
}

#endif
