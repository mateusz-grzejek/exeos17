
#define DEBUG_BREAK()       __debugbreak()
#define DEBUG_OUTPUT(text)  utputDebugStringA("%s\n", text)
#define SLEEP(miliseconds)  leep(miliseconds)

#define EXS_BYTESWAP16(n) (((n & 0xFF00) >> 8) | ((n & 0x00FF) << 8))
#define EXS_BYTESWAP32    _bswap
#define EXS_BYTESWAP64    _bswap64

#define EXS_ROTL16(x, n) ((x << n) | (x >> (16-n)))
#define EXS_ROTL32       _rotl
#define EXS_ROTL64       _lrotl

#define EXS_ROTR16(x, n) ((x >> n) | (x << (16-n)))
#define EXS_ROTR32       _rotr
#define EXS_ROTR64       _lrotr




