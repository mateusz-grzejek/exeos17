
#ifndef __Exs_Config_Include_H__
#define __Exs_Config_Include_H__


// C Library
#include <climits>
#include <cstdarg>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <cstring>

// POSIX
#include <sys/types.h>
#include <sys/stat.h>

// C++ Library
#include <functional>
#include <typeinfo>

// Memory
#include <memory>
#include <new>

// STD eXtensions library
#include <stdx/algo.h>
#include <stdx/atomic_mask.h>
#include <stdx/intrusive_ptr.h>
#include <stdx/limits.h>
#include <stdx/mask.h>
#include <stdx/reg_ctrl_counter.h>
#include <stdx/shared_ptr.h>
#include <stdx/spin_lock.h>
#include <stdx/static_algo.h>
#include <stdx/string.h>
#include <stdx/type_traits.h>

// STD containers
#include <array>
#include <deque>
#include <list>
#include <queue>
#include <stack>
#include <vector>


#if defined( __MINGW64__ ) && ( EXS_SUPPORTED_EIS != EXS_EIS_NONE )
#
#  if defined( _WINDOWS_ ) || defined( _INC_WINDOWS )
#    error "Include mismatch! On MinGW-W64, <initrin.h> must be included before <Windows.h>!"
#  endif
#
#  include <intrin.h>
#
#endif


#if ( EXS_SUPPORTED_EIS & EXS_EIS_NEON )
#  include <arm_neon.h>
#elif ( EXS_SUPPORTED_EIS & EXS_EIS_AVX2 )
#  include <immintrin.h>
#elif ( EXS_SUPPORTED_EIS & EXS_EIS_AVX )
#  include <immintrin.h>
#elif ( EXS_SUPPORTED_EIS & EXS_EIS_SSE41 )
#  include <smmintrin.h>
#elif ( EXS_SUPPORTED_EIS & EXS_EIS_SSE3 )
#  include <pmmintrin.h>
#elif ( EXS_SUPPORTED_EIS & EXS_EIS_SSE2 )
#  include <emmintrin.h>
#elif ( EXS_SUPPORTED_EIS & EXS_EIS_SSE )
#  include <xmmintrin.h>
#endif


#endif /* __Exs_Config_Include_H__ */
