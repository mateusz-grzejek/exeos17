
#ifndef __Exs_Config_Common_Architecture_ARM_H__
#define __Exs_Config_Common_Architecture_ARM_H__


#define EXS_TARGET_ARCHITECTURE_STR	"ARM"


#if !defined( EXS_MEMORY_BASE_ALIGNMENT )
#  define EXS_MEMORY_BASE_ALIGNMENT 4
#endif


#endif /* __Exs_Config_Common_Architecture_ARM_H__ */
