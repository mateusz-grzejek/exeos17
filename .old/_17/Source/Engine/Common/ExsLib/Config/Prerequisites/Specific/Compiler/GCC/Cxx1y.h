
namespace std
{

	template <class T, class... Args>
	inline std::unique_ptr<T> make_unique(Args&&... args)
	{
		return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
    }

}
