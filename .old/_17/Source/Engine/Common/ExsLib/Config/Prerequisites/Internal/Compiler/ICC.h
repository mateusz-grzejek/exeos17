
#ifndef __Exs_Config_Common_Compiler_ICC_H__
#define __Exs_Config_Common_Compiler_ICC_H__


#define EXS_COMPILER_STR "Intel C++ Compiler"


#define DLL_EXPORT      __declspec(dllexport)
#define DLL_IMPORT      __declspec(dllimport)
#define THREAD_LOCAL    __declspec(thread)


#if ( EXS_CONFIG_BASE_FORCE_INLINE )
#  define ExsForceInline __forceinline
#else
#  define ExsForceInline __inline
#endif


#define EXS_TODO(description) __pragma(message(description))


#define EXS_DECLARE_NONCOPYABLE(Type) \
	private: Type(const Type&) = delete; Type& operator=(const Type&) = delete;


#endif /* __Exs_Config_Common_Compiler_ICC_H__ */
