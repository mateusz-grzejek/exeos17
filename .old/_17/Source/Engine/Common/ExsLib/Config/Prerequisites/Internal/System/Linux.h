
#ifndef __Exs_Config_Common_System_Linux_H__
#define __Exs_Config_Common_System_Linux_H__


#define EXS_TARGET_SYSTEM_STR "GNU Linux"

#include <dlfcn.h>
#include <inttypes.h>
#include <sys/signal.h>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xlocale.h>
#include <X11/Xthreads.h>

#include <X11/keysym.h>
#include <X11/keysymdef.h>


enum : XID
{
	X11C_Always = Always,
	X11C_None = None,
	X11C_Success = Success
};


#undef major
#undef minor

#undef Always
#undef None
#undef Success


#define EXS_UNICODE_CHARACTER  EXS_UNICODE_CHARACTER_WIDE
#define EXS_UNICODE_ENCODING   EXS_ENCODING_UTF8
#define EXS_WCHAR_STR_ENCODING EXS_ENCODING_UTF32


#define EXS_ENV_DEFAULT_PATH_DELIMITER		'/'
#define EXS_ENV_DEFAULT_PATH_DELIMITER_STR	"/"
#define EXS_ENV_DYNAMIC_LIBRARY_PREFIX		"lib"
#define EXS_ENV_DYNAMIC_LIBRARY_EXTENSION	".so"


#define ExsCopyMemory(destPtr, srcPtr, size)	memcpy((destPtr), (srcPtr), (size))
#define ExsFillMemory(memPtr, size, value)		memset((memPtr), (value), (size))
#define ExsMoveMemory(destPtr, srcPtr, size)	memmove((destPtr), (srcPtr), (size))
#define ExsZeroMemory(memPtr, size)				memset((memPtr), 0, (size))


#endif /* __Exs_Config_Common_System_Linux_H__ */
