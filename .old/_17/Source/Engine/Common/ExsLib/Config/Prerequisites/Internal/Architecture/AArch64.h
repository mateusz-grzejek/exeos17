
#ifndef __Exs_Config_Common_Architecture_AArch64_H__
#define __Exs_Config_Common_Architecture_AArch64_H__


#define EXS_TARGET_ARCHITECTURE_STR	"AArch64"


#if !defined( EXS_MEMORY_BASE_ALIGNMENT )
#  define EXS_MEMORY_BASE_ALIGNMENT 8
#endif


#endif /* __Exs_Config_Common_Architecture_AArch64_H__ */
