
#if !( EXS_CONFIG_BASE_USE_ISOCPP11_PRINT_FORMAT_SPECIFIERS )
#  define EXS_PFI32      "I32d"
#  define EXS_PFI32_HEX  "I32X"
#  define EXS_PFI32_OCT  "I32o"
#  define EXS_PFI64      "I64d"
#  define EXS_PFI64_HEX  "I64X"
#  define EXS_PFI64_OCT  "I64o"
#  define EXS_PFU32      "I32u"
#  define EXS_PFU32_HEX  "I32X"
#  define EXS_PFU32_OCT  "I32o"
#  define EXS_PFU64      "I64u"
#  define EXS_PFU64_HEX  "I64X"
#  define EXS_PFU64_OCT  "I64o"
#  define EXS_STATE_PRINT_FORMAT_SPECIFIERS_DEFINED 1
#endif


#define fileno     _fileno
#define fseek64    _fseeki64
#define fstat64    _fstati64
#define ftell64    _ftelli64
#define snprintf   _snprintf
#define snwprintf  _snwprintf
#define stat64     _stat64


inline FILE* fopen(const wchar_t* filename, const wchar_t* mode)
{
	return _wfopen(filename, mode);
}

inline float strtof(const char* str, char** endPtr)
{
	double dbl_value = strtod(str, endPtr);
	return static_cast<float>(dbl_value);
}

inline float wcstof(const wchar_t* str, wchar_t** endPtr)
{
	double dbl_value = wcstod(str, endPtr);
	return static_cast<float>(dbl_value);
}

inline Int64 strtoll(const char* str, char** endPtr, int radix)
{
	return _strtoi64(str, endPtr, radix);
}

inline Uint64 strtoull(const char* str, char** endPtr, int radix)
{
	return _strtoui64(str, endPtr, radix);
}

inline Int64 wcstoll(const wchar_t* str, wchar_t** endPtr, int radix)
{
	return _wcstoi64(str, endPtr, radix);
}

inline Uint64 wcstoull(const wchar_t* str, wchar_t** endPtr, int radix)
{
	return _wcstoui64(str, endPtr, radix);
}
