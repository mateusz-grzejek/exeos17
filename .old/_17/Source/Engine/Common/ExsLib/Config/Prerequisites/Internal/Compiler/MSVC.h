
#ifndef __Exs_Config_Common_Compiler_MSVC_H__
#define __Exs_Config_Common_Compiler_MSVC_H__


#define EXS_COMPILER_STR "Microsoft Visual C++"

#include <sysinfoapi.h>
#include <debugapi.h>
#include <intrin.h>


#define EXS_FILE  __FILE__
#define EXS_FUNC  __FUNCSIG__
#define EXS_LINE  __LINE__

#define EXS_ATTR_ALIGN(n)               __declspec(align(n))
#define EXS_ATTR_DEPRECATED(msg, repl)  __declspec(deprecated)
#define EXS_ATTR_DLL_EXPORT             __declspec(dllexport)
#define EXS_ATTR_DLL_IMPORT             __declspec(dllimport)
#define EXS_ATTR_NO_RETURN              __declspec(noreturn)

#define EXS_PRAGMA_STRUCT_PACKING_SET(n)   __pragma(pack(push, n))
#define EXS_PRAGMA_STRUCT_PACKING_UNSET()  __pragma(pack(pop))


#if ( EXS_COMPILER < EXS_COMPILER_MSVC_2015 )
#  define EXS_ATTR_THREAD_LOCAL __declspec(thread)
#else
#  if ( EXS_CONFIG_BASE_USE_ISOCPP11_EXTENDED_STORAGE_SPECIFIERS )
#  	define EXS_ATTR_THREAD_LOCAL thread_local
#  else
#  	define EXS_ATTR_THREAD_LOCAL __declspec(thread)
#  endif
#endif


#if ( EXS_CONFIG_BASE_FORCE_INLINE )
#  define ExsForceInline __forceinline
#else
#  define ExsForceInline __inline
#endif


#define gnoexcept
#define w64 __w64


#if ( EXS_COMPILER < EXS_COMPILER_MSVC_2013 )
#  define EXS_DECLARE_NONCOPYABLE(Type) \
     private: Type(const Type&); Type& operator=(const Type&);
#else
#  define EXS_DECLARE_NONCOPYABLE(Type) \
     private: Type(const Type&) = delete; Type& operator=(const Type&) = delete;
#endif


#pragma warning(disable : 4005)	/* 'MACRONAME': macro redefinition */
#pragma warning(disable : 4065)	/* switch statement contains 'default' but no 'case' labels */
#pragma warning(disable : 4100) /* 'VARNAME': unreferenced formal parameter */
#pragma warning(disable : 4127)	/* conditional expression is constant */
#pragma warning(disable : 4189)	/* 'VARNAME': local variable is initialized but not referenced */
#pragma warning(disable : 4201)	/* nonstandard extension used: nameless struct/union */
#pragma warning(disable : 4251)	/* class 'A' needs to have dll-interface to be used by clients of class 'B' */
#pragma warning(disable : 4275)	/* non dll-interface class 'A' used as base for dll-interface class 'B' */
#pragma warning(disable : 4333)	/* '>>': right shift by too large amount, data loss */
#pragma warning(disable : 4312)	/* 'reinterpret_cast': conversion from 'A' to 'B' of greater size */
#pragma warning(disable : 4512)	/* 'CLASSNAME': assignment operator could not be generated */
#pragma warning(disable : 4624)	/* 'CLASSNAME': destructor was implicitly defined as deleted */
#pragma warning(disable : 4702)	/* unreachable code */
#pragma warning(disable : 4752)	/* found Intel(R) Advanced Vector Extensions; consider using /arch:AVX */
#pragma warning(disable : 4996)	/* this function or variable may be unsafe. Consider using 'funcname'_s instead */

#pragma warning(error: 4242) /* conversion from 'X' to 'Y', possible loss of data */
#pragma warning(error: 4267) /* conversion from 'size_t' to 'T', possible loss of data */


#if ( EXS_CONFIG_BASE_ENABLE_INTRINSICS )
#  pragma intrinsic(memcmp, memcpy, memset)
#  pragma intrinsic(strcmp, strcpy, strlen)
#  pragma intrinsic(wcscmp, wcscpy, wcslen)
#endif


#endif /* __Exs_Config_Common_Compiler_MSVC_H__ */
