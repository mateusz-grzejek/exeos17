
#ifndef __Exs_Common_ArrayView_H__
#define __Exs_Common_ArrayView_H__


namespace Exs
{


	///<summary>
	/// Simple wrapper used to wrap and reference continuous arrays of elements of type T, stored
	/// as plain pointer to memory (T*). It enables such arrays to be iterated using range-for loop.
	///</summary>
	template <class T>
	struct ArrayView
	{
	public:
		typedef ArrayView<T> MyType;

	public:
		T*      begin;
		T*      end;
		Size_t  size;

	public:
		ArrayView()
			: begin( nullptr )
			, end( nullptr )
			, size( 0 )
		{ }

		ArrayView( T* data, Size_t size )
			: begin( data )
			, end( data + size )
			, size( size )
		{ }

		template <Size_t N>
		explicit ArrayView( T( &array )[N] )
			: ArrayView( &(array[0]), N )
		{ }

		void Swap( MyType& other )
		{
			std::swap( this->begin, other.begin );
			std::swap( this->end, other.end );
			std::swap( this->size, other.size );
		}
	};


	template <class T>
	inline void swap( ArrayView<T>& left, ArrayView<T>& right )
	{
		left.Swap( right );
	}


	template <class T>
	inline T* begin( const ArrayView<T>& arrayView )
	{
		return arrayView.begin;
	}

	template <class T>
	inline T* end( const ArrayView<T>& arrayView )
	{
		return arrayView.end;
	}


	///<summary>
	/// Creates ArrayView that wraps specified memory.
	///</summary>
	///<param name="data"> Pointer to the beginning of the array. </param>
	///<param name="size"> Number of valid elements in the array. </param>
	template <class T>
	inline ArrayView<T> BindArrayView( T* data, Size_t size )
	{
		return ArrayView<T>( data, size );
	}

	///<summary>
	///</summary>
	///<param name="array">  </param>
	template <class T, Size_t N>
	inline ArrayView<T> BindArrayView( T( &array )[N] )
	{
		return ArrayView<T>( array );
	}


}

#endif // __Exs_Common_ArrayView_H__
