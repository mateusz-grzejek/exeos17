
#include <ExsLib/SharedBase/Prerequisites.h>


namespace Exs
{


	namespace Tag
	{
		
		const AccessNoneTag accessNone = { };

		const AccessReadOnlyTag accessReadOnly = { };

		const AccessWriteOnlyTag accessWriteOnly = { };

		const ConstQualifiedCallTag constQualifiedCall = { };

		const UncheckedTag unchecked = { };

		const UninitializedTag uninitialized = { };

	}


}
