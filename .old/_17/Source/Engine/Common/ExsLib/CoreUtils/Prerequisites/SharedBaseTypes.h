
#ifndef __ExsLib_SharedBase_SharedBaseTypes_H__
#define __ExsLib_SharedBase_SharedBaseTypes_H__


namespace Exs
{


	template <Int... Seq>
	struct IntegerSequence
	{ };

	template <Int N, Int... Seq>
	struct IntegerSequenceGenerator : IntegerSequenceGenerator<N - 1, N - 1, Seq...>
	{ };

	template <Int... Seq>
	struct IntegerSequenceGenerator<0, Seq...>
	{
		typedef IntegerSequence<Seq...> Type;
	};


	///<summary>
	///</summary>
	struct Ratio
	{
	public:
		Int32  numerator;
		Int32  denominator;

	public:
		Ratio()
			: numerator( 1 )
			, denominator( 1 )
		{ }

		Ratio( Int32 num, Int32 den = 1 )
			: numerator( num / stdx::gcd( num, den ) )
			, denominator( den * numerator / num )
		{ }

		Real GetFactor() const
		{
			return static_cast< Real >( this->numerator ) / static_cast< Real >( this->denominator );
		}

		void Swap( Ratio& other )
		{
			std::swap( this->numerator, other.numerator );
			std::swap( this->denominator, other.denominator );
		}
	};


	inline void swap( Ratio& left, Ratio& right )
	{
		left.Swap( right );
	}


	///<summary>
	///</summary>
	struct Version
	{
	public:
		Uint32  major;
		Uint32  minor;

	public:
		Version()
			: major( 0 )
			, minor( 0 )
		{ }

		Version( Uint32 major, Uint32 minor )
			: major( major )
			, minor( minor )
		{ }
	};


	template <typename Value_t>
	struct Range
	{
	public:
		Value_t min;
		Value_t max;

	public:
		Range()
			: min( static_cast< Value_t >( 0 ) )
			, max( static_cast< Value_t >( 0 ) )
		{ }

		Range( Value_t min, Value_t max )
			: min( min )
			, max( max )
		{ }
	};


	struct Position
	{
	public:
		Int32  x;
		Int32  y;

	public:
		Position()
			: x( 0 )
			, y( 0 )
		{ }

		Position( Int32 x, Int32 y )
			: x( x )
			, y( y )
		{ }

		Position( const Int32* xy )
			: x( xy[0] )
			, y( xy[1] )
		{ }

		Position& Move( Int32 dx, Int32 dy )
		{
			this->x += dx;
			this->y += dy;
			return *this;
		}

		bool IsNonZero() const
		{
			return ( this->x != 0 ) && ( this->y != 0 );
		}

		bool IsValid() const
		{
			return ( this->x >= 0 ) && ( this->y >= 0 );
		}

		static Position Move( const Position& inPos, Int32 dx, Int32 dy )
		{
			return Position( inPos ).Move( dx, dy );
		}

		void Swap( Position& other )
		{
			std::swap( this->x, other.x );
			std::swap( this->y, other.y );
		}
	};


	inline bool operator==( const Position& lhs, const Position& rhs )
	{
		return ( lhs.x == rhs.x ) && ( lhs.y == rhs.y );
	}

	inline bool operator!=( const Position& lhs, const Position& rhs )
	{
		return ( lhs.x != rhs.x ) || ( lhs.y != rhs.y );
	}

	inline Position operator-( const Position& lhs, const Position& rhs )
	{
		return Position( lhs.x - rhs.x, lhs.y - rhs.y );
	}


	inline void swap( Position& left, Position& right )
	{
		left.Swap( right );
	}


	struct Size
	{
	public:
		Uint32  width;
		Uint32  height;

	public:
		Size()
			: width( 0 )
			, height( 0 )
		{ }

		Size( Uint32 w, Uint32 h )
			: width( w )
			, height( h )
		{ }

		explicit Size( const Uint32* wh )
			: width( wh[0] )
			, height( wh[1] )
		{ }

		Size& Scale( Real factor )
		{
			this->width = stdx::get_max_of( static_cast< Uint32 >( this->width * factor ), 0U );
			this->height = stdx::get_max_of( static_cast< Uint32 >( this->height * factor ), 0U );
			return *this;
		}

		Size& Scale( Uint32 factor )
		{
			this->width = this->width * factor;
			this->height = this->height * factor;
			return *this;
		}

		Size& Expand( Uint32 w, Uint32 h )
		{
			this->width += w;
			this->height += h;
			return *this;
		}

		Size& Expand( const Size& exp )
		{
			this->Expand( exp.width, exp.height );
			return *this;
		}

		Size& Shrink( Uint32 w, Uint32 h )
		{
			this->width -= stdx::get_min_of( w, this->width );
			this->height -= stdx::get_min_of( h, this->height );
			return *this;
		}

		Size& Shrink( const Size& shr )
		{
			this->Shrink( shr.width, shr.height );
			return *this;
		}

		static Size Scale( const Size& size, Real factor )
		{
			return Size( size ).Scale( factor );
		}

		static Size Scale( const Size& size, Uint32 factor )
		{
			return Size( size ).Scale( factor );
		}

		static Size Expand( const Size& size, Uint32 w, Uint32 h )
		{
			return Size( size ).Expand( w, h );
		}

		static Size Expand( const Size& size, const Size& exp )
		{
			return Size( size ).Expand( exp );
		}

		static Size Shrink( const Size& size, Uint32 w, Uint32 h )
		{
			return Size( size ).Shrink( w, h );
		}

		static Size Shrink( const Size& size, const Size& shr )
		{
			return Size( size ).Shrink( shr );
		}

		void Swap( Size& other )
		{
			std::swap( this->width, other.width );
			std::swap( this->height, other.height );
		}
	};


	inline bool operator==( const Size& lhs, const Size& rhs )
	{
		return ( lhs.width == rhs.width ) && ( lhs.height == rhs.height );
	}

	inline bool operator!=( const Size& lhs, const Size& rhs )
	{
		return ( lhs.width != rhs.width ) || ( lhs.height != rhs.height );
	}


	inline void swap( Size& left, Size& right )
	{
		left.Swap( right );
	}


	struct SourceLocationInfo
	{
	public:
		const char*  fileName;
		const char*  function;
		Int32        lineNumber;

	public:
		SourceLocationInfo()
			: lineNumber( 0 )
		{ }

		SourceLocationInfo( const char* fileName, const char* function, Int32 lineNumber )
			: fileName( fileName )
			, function( function )
			, lineNumber( lineNumber )
		{ }

		void Swap( SourceLocationInfo& other )
		{
			std::swap( this->fileName, other.fileName );
			std::swap( this->function, other.function );
			std::swap( this->lineNumber, other.lineNumber );
		}
	};


	inline void swap( SourceLocationInfo& left, SourceLocationInfo& right )
	{
		left.Swap( right );
	}


#define EXS_CURRENT_SOURCE_INFO SourceLocationInfo(EXS_FILE, EXS_FUNC, EXS_LINE)


}


#endif /* __ExsLib_SharedBase_SharedBaseTypes_H__ */
