
#ifndef __ExsLib_SharedBase_SharedBaseDefs_H__
#define __ExsLib_SharedBase_SharedBaseDefs_H__


#define ExsSleep(milliseconds) \
	SLEEP(milliseconds)

#define ExsCaseReturnConstantString(constant) \
	case constant: return #constant

#define ExsCommonGetAlignedValue(value, alignment) \
	((value + alignment) & (~(alignment - 1)))


namespace Exs
{


	///
	typedef Uint32 U32ID;

	///
	typedef Uint64 U64ID;


	enum : Uint32
	{
		//
		ID32_None = 0,

		//
		ID32_Invalid = stdx::limits<Uint32>::max_value,

		// Represents unspecified 32 bit ID. Certain functions allow this value to be passed if automatically generated IDs
		// should be used. Some objects may also have this ID set (which indicates, that ID is not/should not be used).
		ID32_Auto = ID32_Invalid - 1
	};


	enum : Uint64
	{
		//
		ID64_None = 0,

		//
		ID64_Invalid = stdx::limits<Uint64>::max_value,

		// Represents unspecified 64 bit ID. Certain functions allow this value to be passed if automatically generated IDs
		// should be used. Some objects may also have this ID set (which indicates, that ID is not/should not be used).
		ID64_Auto = ID64_Invalid - 1
	};


	constexpr Size_t Length_Auto = stdx::limits<Size_t>::max_value;


	///
	template <typename T>
	inline T FromString( const char* str )
	{
		auto result = stdx::string_to_value<T>( str, stdx::numeric_base::decimal );
		return result.first;
	}

	///
	template <typename T>
	inline T FromString( const char* str, stdx::numeric_base base )
	{
		auto result = stdx::string_to_value<T>( str, base );
		return result.first;
	}

	///
	template <typename T>
	inline T FromString( const std::string& str )
	{
		auto result = stdx::string_to_value<T>( str, stdx::numeric_base::decimal );
		return result.first;
	}

	///
	template <typename T>
	inline T FromString( const std::string& str, stdx::numeric_base base )
	{
		auto result = stdx::string_to_value<T>( str, base );
		return result.first;
	}

	///
	template <typename T>
	inline std::string ToString( const T& value )
	{
		return stdx::to_string<char>( value );
	}


	///
	template <typename Result, typename Source>
	ExsForceInline Result DbgSafePtrCast(Source ptr)
	{
	#if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
		Result result = dynamic_cast<Result>(ptr);
		ExsDebugAssert( !ptr || (result != nullptr) );
		return result;
	#else
		return static_cast<Result>(ptr);
	#endif
	};


	///
	template <typename Result, typename Value>
	ExsForceInline constexpr Result TruncateCast(const Value& value)
	{
		return static_cast<Result>(value);
	}


	///
	template <typename Value_t>
	ExsForceInline constexpr Value_t GetAlignedValue(Value_t value, Uint32 alignment)
	{
		return ((value + alignment) & (~(alignment - 1)));
	}


	///
	template <typename Array_element_t, Size_t Size, typename Index_t>
	ExsForceInline Array_element_t GetConstantArrayElement(const Array_element_t (&constantArray)[Size], Index_t index)
	{
		Size_t elementIndex = static_cast<Size_t>(index);
		ExsDebugAssert( elementIndex < Size );
		return constantArray[elementIndex];
	}


	///
	#define pointer_cast static_cast

	///
	#define dbgsafe_ptr_cast DbgSafePtrCast

	///
	#define truncate_cast TruncateCast


}


#endif /* __ExsLib_SharedBase_SharedBaseDefs_H__ */
