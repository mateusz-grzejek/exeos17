
#ifndef __Exs_Common_DebugDefs_H__
#define __Exs_Common_DebugDefs_H__


//
#if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
#  define ExsDebugInterrupt() DEBUG_BREAK()
#else
#  define ExsDebugInterrupt()
#endif

//
#if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
#  define ExsDebugAssert(condition) { if (!(condition)) { ExsDebugInterrupt(); } }
#else
#  define ExsDebugAssert(condition)
#endif

//
#if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
#  define ExsDebugInterruptOnce() \
	{ \
		static std::once_flag callFlag; \
		std::call_once(callFlag, []() { ExsDebugInterrupt(); }); \
	}
#else
#d  efine ExsDebugInterruptOnce()
#endif

//
#if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
#  define ExsDebugAssertOnce(condition) \
	{ \
		static std::once_flag callFlag; \
		if (!(condition)) \
			std::call_once(callFlag, []() { ExsDebugInterrupt(); }); \
	}
#else
#  define ExsDebugAssertOnce(condition)
#endif

//
#if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
#  define ExsDebugCode(code) code
#else
#  define ExsDebugCode(code)
#endif

//
#if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
#  define ExsRuntimeInterrupt() DEBUG_BREAK()
#else
#  define ExsRuntimeInterrupt() { if (Internal::RuntimeInterruptImpl(EXS_FILE, EXS_LINE)) { DEBUG_BREAK(); } }
#endif

//
#if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
#define ExsRuntimeAssert(condition) { if (!(condition)) { ExsRuntimeInterrupt(); }}
#else
#define ExsRuntimeAssert(condition) { if (!(condition)) { if (Internal::RuntimeAssertImpl(EXS_FILE, EXS_LINE, #condition)) { ExsRuntimeInterrupt(); } } }
#endif

//
#if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
#define ExsRuntimeInterruptOnce() \
	{ \
		static std::once_flag callFlag; \
		std::call_once(callFlag, []() { ExsRuntimeInterrupt(); }); \
	}
#else
#define ExsRuntimeInterruptOnce() \
	{ \
		static std::once_flag callFlag; \
		std::call_once(callFlag, []() { if (Internal::RuntimeInterruptImpl(EXS_FILE, EXS_LINE)) { ExsRuntimeInterrupt(); } }); \
	}
#endif

//
#if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
#define ExsRuntimeAssertOnce(condition)	\
	{ \
		static std::once_flag callFlag; \
		if (!(condition)) \
			std::call_once(callFlag, []() { ExsRuntimeInterrupt(); }); \
	}
#else
#define ExsRuntimeAssertOnce(condition) \
	{ \
		static std::once_flag callFlag; \
		if (!(condition)) \
			std::call_once(callFlag, []() { if (Internal::RuntimeAssertImpl(EXS_FILE, EXS_LINE, #condition)) { ExsRuntimeInterrupt(); } }); \
	}
#endif


#endif /* __Exs_Common_DebugDefs_H__ */
