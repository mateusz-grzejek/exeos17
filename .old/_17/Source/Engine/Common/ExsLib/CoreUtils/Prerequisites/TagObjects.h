
#ifndef __Exs_Common_TagObjects_H__
#define __Exs_Common_TagObjects_H__


namespace Exs
{

	
	///
	struct AccessNoneTag { };

	///
	struct AccessReadOnlyTag { };

	///
	struct AccessWriteOnlyTag { };

	///
	struct ConstQualifiedCallTag { };

	///
	struct NoLockTag { };

	///
	struct NoWarnTag { };

	///
	struct UncheckedTag { };

	///
	struct UninitializedTag { };


	namespace Tag
	{

		///
		extern const AccessNoneTag accessNone;

		///
		extern const AccessReadOnlyTag accessReadOnly;

		///
		extern const AccessWriteOnlyTag accessWriteOnly;

		///
		extern const ConstQualifiedCallTag constQualifiedCall;

		///
		extern const NoLockTag noLock;

		///
		extern const NoWarnTag noWarn;

		///
		extern const UncheckedTag unchecked;

		///
		extern const UninitializedTag uninitialized;

	}


}


#endif /* __Exs_Common_TagObjects_H__ */
