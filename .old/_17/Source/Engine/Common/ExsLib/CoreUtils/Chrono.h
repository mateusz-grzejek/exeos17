
#ifndef __Exs_Common_Chrono_H__
#define __Exs_Common_Chrono_H__

#include "Prerequisites.h"
#include <chrono>


namespace Exs
{

	
	///<summary>
	/// Represents duration period type, i.e. unit, in which duration is measured.
	///</summary>
	enum class DurationPeriod : Enum
	{
		Nanosecond,
		Microsecond,
		Millisecond,
		Second,
	};


	namespace Internal
	{
		
		template <bool Is_high_resolution_clock_steady>
		struct MonotonicClock;

		template <>
		struct MonotonicClock<true>
		{
			typedef std::chrono::high_resolution_clock Type;
		};

		template <>
		struct MonotonicClock<false>
		{
			typedef std::chrono::steady_clock Type;
		};

		typedef MonotonicClock<std::chrono::high_resolution_clock::is_steady>::Type MonotonicClockType;

	}


	template <DurationPeriod Duration_period>
	struct DurationTraits;

	template <>
	struct DurationTraits<DurationPeriod::Nanosecond>
	{
		typedef std::chrono::nanoseconds::period PeriodType;
	};

	template <>
	struct DurationTraits<DurationPeriod::Microsecond>
	{
		typedef std::chrono::microseconds::period PeriodType;
	};

	template <>
	struct DurationTraits<DurationPeriod::Millisecond>
	{
		typedef std::chrono::milliseconds::period PeriodType;
	};

	template <>
	struct DurationTraits<DurationPeriod::Second>
	{
		typedef std::chrono::seconds::period PeriodType;
	};
	

	///
	typedef Internal::MonotonicClockType::time_point TimePoint;

	///
	typedef Internal::MonotonicClockType::rep Duration_value_t;


	enum : Duration_value_t
	{
		// Represents infinite timeout.
		Timeout_Infinite = stdx::limits<Duration_value_t>::max_value
	};

	
	///<summary>
	/// Represents monotonic clock. Monotonic clock is internally implemented as either <c>std::high_resolution_clock</c>
	/// or <c>std::steady_clock</c>. <c>high_resolution_clock</c> is used if its <c>::is_steady</c> property evaluates
	/// to true. <c>steady_clock</c> is used otherwise.
	///</summary>
	class MonotonicClock
	{
	public:
		static TimePoint Now()
		{
			return Internal::MonotonicClockType::now();
		}
	};


	template <class Period>
	using std_duration = std::chrono::duration<Duration_value_t, Period>;


	template <DurationPeriod Period>
	using Duration = std::chrono::duration<Duration_value_t, typename DurationTraits<Period>::PeriodType>;


	// Typedef for duration expressed in nanoseconds.
	typedef Duration<DurationPeriod::Nanosecond> Nanoseconds;
	
	// Typedef for duration expressed in microseconds.
	typedef Duration<DurationPeriod::Microsecond> Microseconds;
	
	// Typedef for duration expressed in milliseconds.
	typedef Duration<DurationPeriod::Millisecond> Milliseconds;
	
	// Typedef for duration expressed in seconds.
	typedef Duration<DurationPeriod::Second> Seconds;


	template <DurationPeriod Out, class Period>
	inline Duration<Out> DurationCast(const std_duration<Period>& duration)
	{
		typedef std::chrono::duration<Duration_value_t, typename DurationTraits<Out>::PeriodType> OutDurationType;
		return std::chrono::duration_cast<OutDurationType>(duration);
	}
	

	///<summary>
	///</summary>
	class TimeStamp
	{
	private:
		Uint64  _value;

	public:
		TimeStamp()
		: _value(0)
		{ }

		TimeStamp(Uint64 value)
		: _value(value)
		{ }

		Uint64 GetValue() const
		{
			return this->_value;
		}

		template <DurationPeriod Period>
		static Duration_value_t Convert(Uint64 value)
		{
			typedef typename DurationTraits<Period>::PeriodType RatioType;
			return _conv(value, RatioType::num, RatioType::den);
		}

	private:
		static Duration_value_t _conv(Uint64 value, Int rn, Int rd);
	};


	inline Uint64 operator-(const TimeStamp& lhs, const TimeStamp& rhs)
	{
		return rhs.GetValue() - lhs.GetValue();
	}


	///<summary>
	/// Computes difference between two timestamps as Duration<Perdiod> object.
	///</summary>
	template <DurationPeriod Period>
	inline Duration<Period> TimeStampDiff(const TimeStamp& first, const TimeStamp& second)
	{
		return Duration<Period>(TimeStamp::Convert<Period>(second - first));
	}

	
	///<summary>
	/// Represents performance counter - platform-specific "clock", that uses the highest resolution available on each
	/// platform. PerfCounter uses TimeStamp, which holds implementation-specific value used to represent point in time.
	///</summary>
	class PerfCounter
	{
	public:
		///<summary>
		///</summary>
		static TimeStamp GetCurrentTimeStamp();
	};


}


#endif /* __Exs_Common_Chrono_H__ */
