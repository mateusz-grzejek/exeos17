
#include "HashCode.h"
#include "ByteConv.h"
#include <zlib/zlib.h>
#include "SharedMutex.h"

//
#define EXS_HASH_INIT_DEFAULT_ADLER32 1

//
#define EXS_HASH_INIT_DEFAULT_CRC32 0xFFFFFFFF

//
#define EXS_HASH_INIT_DEFAULT_DJB2 0x1505

//
#define EXS_HASH_INIT_DEFAULT_FNV1A 0xCBF29CE484222325

//
#define EXS_HASH_INIT_DEFAULT_SDBM 0


namespace Exs
{


	Uint32 HashCodeTraits<HashAlgorithm::Adler32>::Update(Uint32 hash, const void* data, Size_t length)
	{
		Uint32 result = hash;

		if (length > 0)
		{
			const Byte* inputBytes = reinterpret_cast<const Byte*>(data);
			result = adler32(hash, inputBytes, truncate_cast<uInt>(length));
		}

		return result;
	}


	Uint32 HashCodeTraits<HashAlgorithm::Adler32>::Compute(const void* data, Size_t length)
	{
		return Update(EXS_HASH_INIT_DEFAULT_ADLER32, data, length);
	}


	Uint32 HashCodeTraits<HashAlgorithm::CRC32>::Update(Uint32 hash, const void* data, Size_t length)
	{
		Uint32 result = hash;

		if (length > 0)
		{
			const Byte* inputBytes = reinterpret_cast<const Byte*>(data);

		#if ( EXS_SUPPORTED_EIS & EXS_EIS_SSE42 )
			for ( ; length >= 4; bytes += 4, length -= 4)
			{
				result = _mm_crc32_u32(hash, *(reinterpret_cast<const Uint32*>(bytes)));
			}
		#endif

			result = crc32(hash, inputBytes, truncate_cast<uInt>(length));
		}

		return result;
	}


	Uint32 HashCodeTraits<HashAlgorithm::CRC32>::Compute(const void* data, Size_t length)
	{
		return Update(EXS_HASH_INIT_DEFAULT_CRC32, data, length);
	}


	Uint32 HashCodeTraits<HashAlgorithm::DJB2>::Update(Uint32 hash, const void* data, Size_t length)
	{
		Uint32 result = hash;

		if (length > 0)
		{
			const Byte* inputBytes = reinterpret_cast<const Byte*>(data);

			while (length > 0)
			{
				result = ((result << 5) + result) ^ (*inputBytes);
				++inputBytes;
				--length;
			}
		}

		return result;
	}


	Uint32 HashCodeTraits<HashAlgorithm::DJB2>::Compute(const void* data, Size_t length)
	{
		return Update(EXS_HASH_INIT_DEFAULT_DJB2, data, length);
	}


	Uint64 HashCodeTraits<HashAlgorithm::FNV1A>::Update(Uint64 hash, const void* data, Size_t length)
	{
		Uint64 result = hash;

		if (length > 0)
		{
			const Uint64 fnvPrimeValue = 0x100000001B3;
			const Uint64 u64Mask = std::numeric_limits<Uint64>::max() << 8;
			const Byte* inputBytes = reinterpret_cast<const Byte*>(data);

			while (length > 0)
			{
				result = result * fnvPrimeValue;
				result = (result & u64Mask) | ((result & 0xFF) ^ (*inputBytes));
				++inputBytes;
				--length;
			}
		}

		return result;
	}


	Uint64 HashCodeTraits<HashAlgorithm::FNV1A>::Compute(const void* data, Size_t length)
	{
		return Update(EXS_HASH_INIT_DEFAULT_FNV1A, data, length);
	}


	Uint32 HashCodeTraits<HashAlgorithm::SDBM>::Update(Uint32 hash, const void* data, Size_t length)
	{
		Uint32 result = hash;

		if (length > 0)
		{
			const Byte* inputBytes = reinterpret_cast<const Byte*>(data);

			while (length > 0)
			{
				result = (*inputBytes) + (result << 6) + (result << 16) - result;
				++inputBytes;
				--length;
			}
		}

		return result;
	}


	Uint32 HashCodeTraits<HashAlgorithm::SDBM>::Compute(const void* data, Size_t length)
	{
		return Update(EXS_HASH_INIT_DEFAULT_SDBM, data, length);
	}


}
