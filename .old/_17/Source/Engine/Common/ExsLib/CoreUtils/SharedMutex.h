
#ifndef __Exs_Common_SharedMutex_H__
#define __Exs_Common_SharedMutex_H__

#include "Prerequisites.h"
#include <stdx/shared_mutex.h>


namespace Exs
{


	using SharedMutex = stdx::shared_spin_lock;


  #if ( EXS_CONFIG_BASE_USE_SHARED_SPINLOCK_AS_DEFAULT_SHARED_MUTEX )
	///
	using DefaultSharedMutex = stdx::shared_spin_lock;
  #else
	///
	using DefaultSharedMutex = stdx::shared_spin_lock;
  #endif

  #if ( EXS_CONFIG_BASE_USE_SHARED_SPINLOCK_AS_LIGHT_SHARED_MUTEX )
	///
	using LightSharedMutex = stdx::shared_spin_lock;
  #else
	///
	using LightSharedMutex = stdx::shared_spin_lock;
  #endif


	///<summary>
	///</summary>
	enum class SharedMutexAccessType : Enum
	{
		Read_Only = 1,

		Read_Write,

		Unspecified = 0
	};


	template <SharedMutexAccessType>
	struct SharedMutexLockType;

	template <>
	struct SharedMutexLockType <SharedMutexAccessType::Read_Only>
	{
		using Type = stdx::shared_lock<SharedMutex>;
	};

	template <>
	struct SharedMutexLockType <SharedMutexAccessType::Read_Write>
	{
		using Type = std::unique_lock<SharedMutex>;
	};


}


#define ExsBeginSharedCriticalSectionWithSharedAccess(sharedMutex) \
	do { stdx::shared_lock<std::decay<decltype(sharedMutex)>::type> lockGuard { sharedMutex }

#define ExsBeginSharedCriticalSectionWithUniqueAccess(sharedMutex) \
	do { std::unique_lock<std::decay<decltype(sharedMutex)>::type> lockGuard { sharedMutex }


#endif /* __Exs_Common_SharedMutex_H__ */
