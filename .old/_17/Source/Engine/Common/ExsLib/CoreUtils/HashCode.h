
#ifndef __Exs_Common_HashCode_H__
#define __Exs_Common_HashCode_H__

#include "Prerequisites.h"


namespace Exs
{


	///<summary>
	///</summary>
	enum class HashAlgorithm : Enum
	{
		Adler32 = 0xAC7100,
		CRC32,
		DJB2,
		FNV1A,
		SDBM,
		UI32,
		UI64,
		Unknown = 0
	};


	///<summary>
	/// Packed structure with data used to compute the hash value.
	///</summary>
	struct HashInput
	{
		const void* data;
		Size_t length;
	};


	///<summary>
	///</summary>
	template <class T>
	inline HashInput GetHashInput(const T& ref)
	{
		return HashInput { &ref, sizeof(T) };
	}

	inline HashInput GetHashInput(const char* str)
	{
		return HashInput { str, strlen(str) };
	}

	inline HashInput GetHashInput(const wchar_t* str)
	{
		return HashInput { str, wcslen(str) };
	}

	inline HashInput GetHashInput(const std::string& str)
	{
		return HashInput { str.c_str(), str.length() };
	}

	inline HashInput GetHashInput(const std::wstring& str)
	{
		return HashInput { str.c_str(), str.length() };
	}


	template <class T>
	struct DefaultHashAlgorithm
	{
		static const HashAlgorithm value = HashAlgorithm::SDBM;
	};

	template <>
	struct DefaultHashAlgorithm<const char*>
	{
		static const HashAlgorithm value = HashAlgorithm::DJB2;
	};

	template <>
	struct DefaultHashAlgorithm<const wchar_t*>
	{
		static const HashAlgorithm value = HashAlgorithm::DJB2;
	};

	template <>
	struct DefaultHashAlgorithm<std::string>
	{
		static const HashAlgorithm value = HashAlgorithm::DJB2;
	};

	template <>
	struct DefaultHashAlgorithm<std::wstring>
	{
		static const HashAlgorithm value = HashAlgorithm::DJB2;
	};


	template <Size_t Hash_size>
	struct HashTypeTraits;

	template <>
	struct HashTypeTraits<32>
	{
		typedef Uint32 ValueType;

		static const Size_t byteSize = sizeof(Uint32);
		static const Size_t size = byteSize * 8;
	};

	template <>
	struct HashTypeTraits<64>
	{
		typedef Uint64 ValueType;

		static const Size_t byteSize = sizeof(Uint64);
		static const Size_t size = byteSize * 8;
	};

	template <>
	struct HashTypeTraits<128>
	{
		typedef Uint32 ValueType[4];

		static const Size_t byteSize = sizeof(Uint32) * 4;
		static const Size_t size = byteSize * 8;
	};


	template <HashAlgorithm Hash_algo>
	struct HashCodeTraits;

	template <>
	struct HashCodeTraits<HashAlgorithm::Adler32> : public HashTypeTraits<32>
	{
		static Uint32 Update(Uint32 hash, const void* input, Size_t length);
		static Uint32 Compute(const void* input, Size_t length);
	};

	template <>
	struct HashCodeTraits<HashAlgorithm::CRC32> : public HashTypeTraits<32>
	{
		static Uint32 Update(Uint32 hash, const void* input, Size_t length);
		static Uint32 Compute(const void* input, Size_t length);
	};

	template <>
	struct HashCodeTraits<HashAlgorithm::DJB2> : public HashTypeTraits<32>
	{
		static Uint32 Update(Uint32 hash, const void* input, Size_t length);
		static Uint32 Compute(const void* input, Size_t length);
	};

	template <>
	struct HashCodeTraits<HashAlgorithm::FNV1A> : public HashTypeTraits<64>
	{
		static Uint64 Update(Uint64 hash, const void* input, Size_t length);
		static Uint64 Compute(const void* input, Size_t length);
	};

	template <>
	struct HashCodeTraits<HashAlgorithm::SDBM> : public HashTypeTraits<32>
	{
		static Uint32 Update(Uint32 hash, const void* input, Size_t length);
		static Uint32 Compute(const void* input, Size_t length);
	};


	template <HashAlgorithm Hash_algo>
	struct HashCode
	{
	public:
		typedef HashCode<Hash_algo> MyType;
		typedef HashCodeTraits<Hash_algo> TraitsType;

		typedef typename TraitsType::ValueType ValueType;

		static const Size_t byteSize = TraitsType::byteSize;
		static const Size_t size = TraitsType::size;

	public:
		ValueType value;

	public:
		HashCode()
		: value(0)
		{ }

		HashCode(ValueType value)
		: value(value)
		{ }

		void Swap(MyType& other)
		{
			std::swap(this->value, other.value);
		}

		bool IsEqualTo(const MyType& other) const
		{
			return this->value = other.value;
		}

		Int32 Compare(const MyType& other) const
		{
			return (this->value == other.value) ? 0 : ((this->value > other.value) ? 1 : -1);
		}
	};


	template <HashAlgorithm Hash_algo>
	inline bool operator==(const HashCode<Hash_algo>& lhs, const HashCode<Hash_algo>& rhs)
	{
		return lhs.IsEqualTo(rhs);
	}

	template <HashAlgorithm Hash_algo>
	inline bool operator!=(const HashCode<Hash_algo>& lhs, const HashCode<Hash_algo>& rhs)
	{
		return !lhs.IsEqualTo(rhs);
	}

	template <HashAlgorithm Hash_algo>
	inline bool operator<(const HashCode<Hash_algo>& lhs, const HashCode<Hash_algo>& rhs)
	{
		return lhs.Compare(rhs) < 0;
	}

	template <HashAlgorithm Hash_algo>
	inline bool operator<=(const HashCode<Hash_algo>& lhs, const HashCode<Hash_algo>& rhs)
	{
		return lhs.Compare(rhs) <= 0;
	}

	template <HashAlgorithm Hash_algo>
	inline bool operator>(const HashCode<Hash_algo>& lhs, const HashCode<Hash_algo>& rhs)
	{
		return lhs.Compare(rhs) > 0;
	}

	template <HashAlgorithm Hash_algo>
	inline bool operator>=(const HashCode<Hash_algo>& lhs, const HashCode<Hash_algo>& rhs)
	{
		return lhs.Compare(rhs) >= 0;
	}


	template <HashAlgorithm Hash_algo>
	inline void swap(HashCode<Hash_algo>& left, HashCode<Hash_algo>& right)
	{
		left.Swap(right);
	}


	typedef HashCode<HashAlgorithm::Adler32> Adler32;
	typedef HashCode<HashAlgorithm::CRC32> CRC32;
	typedef HashCode<HashAlgorithm::DJB2> DJB2;
	typedef HashCode<HashAlgorithm::FNV1A> FNV1A;
	typedef HashCode<HashAlgorithm::SDBM> SDBM;


	///<summary>
	///</summary>
	template <class T, class HashCodeType>
	struct HashCodeGen
	{
		HashCodeType operator()(const T& value) const
		{
			auto hashInput = GetHashInput(value);
			return HashCodeType(HashCodeType::TraitsType::Compute(hashInput.data, hashInput.length));
		}

		HashCodeType operator()(const HashCodeType& hash, const T& value) const
		{
			auto hashInput = GetHashInput(value);
			return HashCodeType(HashCodeType::TraitsType::Update(hash.value, hashInput.data, hashInput.length));
		}
	};


	///<summary>
	///</summary>
	template <class HashCodeType, class T>
	inline HashCodeType GetHashCode(const T& value)
	{
		return HashCodeGen<T, HashCodeType>()(value);
	}


	///<summary>
	///</summary>
	template <class HashCodeType, class T>
	inline HashCodeType UpdateHashCode(const HashCodeType& hashCode, const T& value)
	{
		return HashCodeGen<T, HashCodeType>()(hashCode, value);
	}

	
	template <class T>
	using DefaultHashCode = HashCode<DefaultHashAlgorithm<T>::value>;


	template <class T>
	using DefaultHashCodeGen = HashCodeGen< T, DefaultHashCode<T> >;


	///<summary>
	///</summary>
	template <class T>
	inline DefaultHashCode<T> GetDefaultHashCode(const T& value)
	{
		return GetHashCode< DefaultHashCode<T> >(value);
	}



}


#endif /* __Exs_Common_HashCode_H__ */
