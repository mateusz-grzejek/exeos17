
#ifndef __Exs_Common_StaticReflection_H__
#define __Exs_Common_StaticReflection_H__

#include "HashCode.h"


namespace Exs
{


	template <class T>
	inline const char* GetTypeName()
	{
		return typeid(T).name();
	}


	template <class T>
	inline DefaultHashCode<const char*> GetTypeID()
	{
		return GetDefaultHashCode(GetTypeName<T>());
	}


}


#endif /* __Exs_Common_StaticReflection_H__ */
