
#ifndef __Exs_Common_ByteConv_H__
#define __Exs_Common_ByteConv_H__

#include "Prerequisites.h"


namespace Exs
{


	///<summary>
	///</summary>
	enum class ByteOrder : Enum
	{
		// Big-Endian ordering (most significant byte lies first in memory).
		Big_Endian = EXS_ENDIANNESS_BE,

		// Little-Endian ordering (least significant byte lies first in memory).
		Little_Endian = EXS_ENDIANNESS_LE,

		// Default ordering for the current platform.
		Arch_Default = EXS_ENDIANNESS,

		// Swapped default ordering (BE if default ordering is LE and vice versa).
		Arch_Swap = EXS_ENDIANNESS_SWAP,

		// Unknown ordering, not-yet-implemented ordering, etc.
		Unknown = 0
	};


	///<summary>
	///</summary>
	template <class T>
	Int ByteCompare( const T& first, const T& second )
	{
		return memcmp( &first, &second, sizeof( T ) );
	}


	///<summary>
	///</summary>
	template <class Tx, class Ty>
	Int ByteCompare( const Tx& first, const Ty& second )
	{
		
		size_t compareSize = stdx::get_min_of( sizeof( Tx ), sizeof( Ty ) );
		return memcmp( &first, &second, compareSize );
	}


	///<summary>
	///</summary>
	template <class Tx, class Ty>
	Int ByteCompare( const Tx& first, const Ty& second, const NoWarnTag& )
	{
		size_t compareSize = stdx::get_min_of( sizeof( Tx ), sizeof( Ty ) );
		return memcmp( &first, &second, compareSize );
	}


	ExsForceInline Uint16 ByteSwap(Uint16 value)
	{
		return EXS_BYTESWAP16(value);
	}

	ExsForceInline Uint32 ByteSwap(Uint32 value)
	{
		return EXS_BYTESWAP32(value);
	}

	ExsForceInline Uint64 ByteSwap(Uint64 value)
	{
		return EXS_BYTESWAP64(value);
	}


	template <ByteOrder>
	struct ByteConvBase;


	template <>
	struct ByteConvBase<ByteOrder::Arch_Default>
	{
		ExsForceInline static Uint16 GetNative16(const Uint16* value)
		{
			return *value;
		}

		ExsForceInline static Uint32 GetNative32(const Uint32* value)
		{
			return *value;
		}

		ExsForceInline static Uint64 GetNative64(const Uint64* value)
		{
			return *value;
		}

		ExsForceInline static Uint16 GetNative16(const Byte* bytes)
		{
			return *(reinterpret_cast<const Uint16*>(bytes));
		}

		ExsForceInline static Uint32 GetNative32(const Byte* bytes)
		{
			return *(reinterpret_cast<const Uint16*>(bytes));
		}

		ExsForceInline static Uint64 GetNative64(const Byte* bytes)
		{
			return *(reinterpret_cast<const Uint64*>(bytes));
		}
	};


	template <>
	struct ByteConvBase<ByteOrder::Arch_Swap>
	{
		ExsForceInline static Uint16 GetNative16(const Uint16* value)
		{
			return ByteSwap(*value);
		}

		ExsForceInline static Uint32 GetNative32(const Uint32* value)
		{
			return ByteSwap(*value);
		}

		ExsForceInline static Uint64 GetNative64(const Uint64* value)
		{
			return ByteSwap(*value);
		}

		ExsForceInline static Uint16 GetNative16(const Byte* bytes)
		{
			return ByteSwap(*(reinterpret_cast<const Uint16*>(bytes)));
		}

		ExsForceInline static Uint32 GetNative32(const Byte* bytes)
		{
			return ByteSwap(*(reinterpret_cast<const Uint32*>(bytes)));
		}

		ExsForceInline static Uint64 GetNative64(const Byte* bytes)
		{
			return ByteSwap(*(reinterpret_cast<const Uint64*>(bytes)));
		}
	};


	template <typename Type, ByteOrder Byte_order>
	struct ByteConv;


	template <ByteOrder Byte_order>
	struct ByteConv<Uint16, Byte_order> : public ByteConvBase<Byte_order>
	{
		ExsForceInline static Uint16 ToBytes(Uint16 value, Byte* output)
		{
			return (*(reinterpret_cast<Uint16*>(output)) = ByteConvBase<Byte_order>::GetNative16(&value));
		}

		ExsForceInline static Uint16 ToValue(const Byte* bytes)
		{
			return ByteConvBase<Byte_order>::GetNative16(reinterpret_cast<const Uint16*>(bytes));
		}

		ExsForceInline static Uint16 ToValue(const Byte* bytes, Uint16* output)
		{
			return (*output = ByteConvBase<Byte_order>::GetNative16(reinterpret_cast<const Uint16*>(bytes)));
		}
	};


	template <ByteOrder Byte_order>
	struct ByteConv<Uint32, Byte_order> : public ByteConvBase<Byte_order>
	{
		ExsForceInline static Uint32 ToBytes(Uint32 value, Byte* output)
		{
			return (*(reinterpret_cast<Uint32*>(output)) = ByteConvBase<Byte_order>::GetNative32(&value));
		}

		ExsForceInline static Uint32 ToValue(const Byte* bytes)
		{
			return ByteConvBase<Byte_order>::GetNative32(reinterpret_cast<const Uint32*>(bytes));
		}

		ExsForceInline static Uint32 ToValue(const Byte* bytes, Uint32* output)
		{
			return (*output = ByteConvBase<Byte_order>::GetNative32(reinterpret_cast<const Uint32*>(bytes)));
		}
	};


	template <ByteOrder Byte_order>
	struct ByteConv<Uint64, Byte_order> : public ByteConvBase<Byte_order>
	{
		ExsForceInline static Uint64 ToBytes(Uint64 value, Byte* output)
		{
			return (*(reinterpret_cast<Uint64*>(output)) = ByteConvBase<Byte_order>::GetNative64(&value));
		}

		ExsForceInline static Uint64 ToValue(const Byte* bytes)
		{
			return ByteConvBase<Byte_order>::GetNative64(reinterpret_cast<const Uint64*>(bytes));
		}

		ExsForceInline static Uint64 ToValue(const Byte* bytes, Uint64* output)
		{
			return (*output = ByteConvBase<Byte_order>::GetNative64(reinterpret_cast<const Uint64*>(bytes)));
		}
	};


}


#endif /* __Exs_Common_ByteConv_H__ */
