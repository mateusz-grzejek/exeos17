
#ifndef __Exs_Core_Mutex_H__
#define __Exs_Core_Mutex_H__

#include "Prerequisites.h"
#include <mutex>


namespace Exs
{


	///
	using SpinLockMutex = stdx::spin_lock;

	/// System mutex, which is currently (C++ 11-based version) an std::mutex.
	using SystemMutex = std::mutex;

	///
	using SystemMutexLock = std::unique_lock<SystemMutex>;

  #if ( EXS_CONFIG_BASE_USE_SPINLOCK_AS_DEFAULT_MUTEX )
	///
	using DefaultMutex = SpinLockMutex;
  #else
	///
	using DefaultMutex = SystemMutex;
  #endif

  #if ( EXS_CONFIG_BASE_USE_SPINLOCK_AS_LIGHT_MUTEX )
	///
	using LightMutex = SpinLockMutex;
  #else
	///
	using LightMutex = SystemMutex;
  #endif


	///<summary>
	/// Proxy interface which is used by MutexAdapter to support mutexes with non-standard-like
	/// interfaces (lock/try_lock/unlock). Specialization can be easily defined using macro below.
	///</summary>
	template <class Mutex_type>
	struct MutexInterfaceProxy
	{
		//
		static void Lock(Mutex_type& mutex)
		{
			mutex.lock();
		}

		//
		static bool TryLock(Mutex_type& mutex)
		{
			return mutex.try_lock();
		}

		//
		static void Unlock(Mutex_type& mutex)
		{
			mutex.unlock();
		}
	};


	//
	#define ExsDeclareMutexInterfaceProxy(type, lockFun, tryLockFun, unlockFun) \
		template <> \
		struct MutexInterfaceProxy<type> \
		{ \
			static void Lock(type& mutex) \
			{ \
				mutex.lockFun(); \
			} \
			static bool TryLock(type& mutex) \
			{ \
				return mutex.tryLockFun(); \
			} \
			static void Unlock(type& mutex) \
			{ \
				unlockFun.lockFun(); \
			} \
		};


	///<summary>
	/// Common locking interface provider. Wraps actual mutex to provide unified semantics for lock objects.
	/// It is used in all standard (engine internal) sync primitives to allow easy replacement of lock types.
	///</summary>
	template <class Mutex_type>
	class MutexAdapter
	{
		EXS_DECLARE_NONCOPYABLE(MutexAdapter);

	public:
		typedef Mutex_type MutexType;

	protected:
		Mutex_type  _mutex;

	public:
		MutexAdapter() = default;

		template <class... Args>
		MutexAdapter(Args&&... args)
		: _mutex(std::forward<Args>(args)...)
		{ }

		///<summary>
		///</summary>
		operator Mutex_type&()
		{
			return this->_mutex;
		}

		///<summary>
		///</summary>
		void lock()
		{
			MutexInterfaceProxy<Mutex_type>::Lock(this->_mutex);
		}

		///<summary>
		///</summary>
		bool try_lock()
		{
			return MutexInterfaceProxy<Mutex_type>::TryLock(this->_mutex);
		}

		///<summary>
		///</summary>
		void unlock()
		{
			MutexInterfaceProxy<Mutex_type>::Unlock(this->_mutex);
		}
	};


	///<summary>
	/// Specifies how lock can be accessed and retrieved. It influences directly what kind of public accessors
	/// are inherited from <c>Lockable</c> base class.
	///</summary>
	enum class LockAccess : Enum
	{
		// Lock is always retrieved as non-const reference, which allows the object to be locked
		// in any possible state. Note, that this implies lock member to be declared as <c>mutable</c>.
		Relaxed,

		// The type of retrieved lock (in terms of its const-ness) matches the objects's type.
		// This makes impossible to lock an object, that was declared/retrieved as const-qualified.
		Strict
	};


	///<summary>
	/// Helper base class, that can be derived to inherit lock object of desired type. Accessibility
	/// can be controlled using <c>Lock_access</c> parameter.
	///</summary>
	template <class Lock_t, LockAccess Lock_access = LockAccess::Strict>
	class Lockable;


	template <class Lock_t>
	class alignas(EXS_HW_DESTRUCTIVE_CACHE_INTERFERENCE_SIZE) Lockable<Lock_t, LockAccess::Strict>
	{
	public:
		typedef Lock_t LockType;
		typedef Lockable<Lock_t, LockAccess::Strict> MyType;

	protected:
		Lock_t _lock;

	public:
		Lockable()
		{ }

		Lock_t& GetLock()
		{
			return this->_lock;
		}

		const Lock_t& GetLock() const
		{
			return this->_lock;
		}
	};


	template <class Lock_t>
	class alignas(EXS_HW_DESTRUCTIVE_CACHE_INTERFERENCE_SIZE) Lockable<Lock_t, LockAccess::Relaxed>
	{
	public:
		typedef Lock_t LockType;
		typedef Lockable<Lock_t, LockAccess::Relaxed> MyType;

	protected:
		mutable Lock_t _lock;

	public:
		Lockable()
		{ }

		Lock_t& GetLock() const
		{
			return this->_lock;
		}
	};


}


//
#define ExsBeginCriticalSection(mutex) \
	do { std::unique_lock<std::decay<decltype(mutex)>::type> csLockGuard { mutex }

//
#define ExsBeginNamedCriticalSection(lockGuardName, mutex) \
	do { std::unique_lock<std::decay<decltype(mutex)>::type> lockGuardName { mutex }

//
#define ExsEndCriticalSection() \
	} while(false)

// Leaves the scope of the current critical section. May not be used from the inside of loops within the CS.
#define ExsQuitCriticalSection() \
	break


#endif /* __Exs_Core_Mutex_H__ */
