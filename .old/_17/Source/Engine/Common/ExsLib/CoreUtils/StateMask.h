
#ifndef __Exs_Common_StateMask_H__
#define __Exs_Common_StateMask_H__

#include "Prerequisites.h"


namespace Exs
{


	enum class StateMaskType : Enum
	{
		Default,
		Atomic
	};


	template <typename Tp, StateMaskType Type = StateMaskType::Default>
	class StateMask;


	template <typename Tp>
	class StateMask<Tp, StateMaskType::Default>
	{
	public:
		typedef Tp ValueType;
		typedef stdx::mask<Tp> MaskType;
		typedef StateMask<Tp, StateMaskType::Default> MyType;

	private:
		stdx::mask<Tp> _mask;

	public:
		StateMask(const StateMask&) = default;
		StateMask& operator=(const StateMask&) = default;

		StateMask(Tp value = static_cast<Tp>(0))
		: _mask(value)
		{ }

		void Set(Tp flags)
		{
			this->_mask.set(flags);
		}

		void Unset(Tp flags)
		{
			this->_mask.unset(flags);
		}

		void Reset(Tp value = static_cast<Tp>(0))
		{
			this->_mask.store(value);
		}

		void Clear()
		{
			this->_mask.clear();
		}

		bool IsSet(Tp flags) const
		{
			return this->_mask.is_set(flags);
		}
	};


	template <typename Tp>
	class StateMask<Tp, StateMaskType::Atomic>
	{
	public:
		typedef Tp ValueType;
		typedef stdx::mask<Tp> MaskType;
		typedef StateMask<Tp, StateMaskType::Atomic> MyType;

	private:
		stdx::atomic_mask<Tp> _mask;

	public:
		StateMask(const StateMask&) = delete;
		StateMask& operator=(const StateMask&) = delete;

		StateMask(Tp value = static_cast<Tp>(0))
		: _mask(value)
		{ }

		void Set(Tp flags, std::memory_order memOrder = std::memory_order_relaxed)
		{
			this->_mask.set(flags, memOrder);
		}

		void Unset(Tp flags, std::memory_order memOrder = std::memory_order_relaxed)
		{
			this->_mask.unset(flags, memOrder);
		}

		void Reset(Tp value = static_cast<Tp>(0), std::memory_order memOrder = std::memory_order_relaxed)
		{
			this->_mask.store(value, memOrder);
		}

		void Clear(std::memory_order memOrder = std::memory_order_relaxed)
		{
			this->_mask.clear(memOrder);
		}

		bool IsSet(Tp flags, std::memory_order memOrder = std::memory_order_relaxed) const
		{
			return this->_mask.is_set(flags, memOrder);
		}
	};


}


#endif /* __Exs_Common_StateMask_H__ */
