
#ifndef __ExsLib_System_NativeThreadCommon_H__
#define __ExsLib_System_NativeThreadCommon_H__

#include "Prerequisites.h"
#include <ExsLib/CoreUtils/Mutex.h>


namespace Exs
{
	namespace System
	{


		class Thread;


		///
		using ThreadHandle = std::shared_ptr<Thread>;


	}
}


#endif /* __ExsLib_System_NativeThreadCommon_H__ */
