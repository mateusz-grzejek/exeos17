
#include <ExsLib/System/DynamicLib.h>


namespace Exs
{
	namespace System
	{
		

		static NativeDLLHandle SysLoadDLL( const Path_char_t* filename );
		static void* SysGetDLLSymbol( NativeDLLHandle handle, const char* symbolName );
		static void SysUnloadDLL( NativeDLLHandle handle );


		struct DynamicLibUnloader
		{
			void operator()( DynamicLibState* dynamicLibState )
			{
				if ( dynamicLibState->nativeHandle != NativeDLLHandleNULL )
				{
					SysUnloadDLL( dynamicLibState->nativeHandle );
					dynamicLibState->nativeHandle = NativeDLLHandleNULL;
				}
			}
		};


		DynamicLibHandle LoadDynamicLibrary( const Path& path )
		{
			std::shared_ptr<DynamicLibState> handle{ new DynamicLibState(), DynamicLibUnloader() };
			handle->nativeHandle = SysLoadDLL( path.c_str() );

			if ( handle->nativeHandle == NativeDLLHandleNULL )
			{
				handle.reset();
			}

			return handle;
		}


		void* QueryDynamicLibrarySymbol( const DynamicLibHandle& handle, const char* name )
		{

		}


		Result UnloadDynamicLibrary( const DynamicLibHandle& handle )
		{
			if ( handle->nativeHandle != NativeDLLHandleNULL )
			{
				SysUnloadDLL( handle->nativeHandle );
				handle->nativeHandle = NativeDLLHandleNULL;
			}
		}


	}
}
