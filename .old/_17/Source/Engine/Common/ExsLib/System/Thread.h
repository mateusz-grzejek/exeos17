
#ifndef __ExsLib_SysLayer_NativeThread_H__
#define __ExsLib_SysLayer_NativeThread_H__

#include "ThreadCommon.h"
#include <thread>
#include <mutex>
#include <condition_variable>


namespace Exs
{
	namespace System
	{


		struct ThreadState
		{
			std::thread threadObject;
		};


		struct ThreadStateDeleter
		{
			void operator()( ThreadState* threadState );
		};


		ThreadHandle CreateThread( const char* name );


		///
		template <typename F, typename... Args>
		inline Result RunThread( const ThreadHandle& threadHandle, F&& callable, Args&&... args )
		{

		}


	}



}


#endif /* __ExsLib_SysLayer_NativeThread_H__ */
