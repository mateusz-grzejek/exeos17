
#ifndef __ExsLib_Filesystem_DirectoryEntry_H__
#define __ExsLib_Filesystem_DirectoryEntry_H__

#include "FilesystemCommon.h"


namespace Exs
{
	namespace System
	{


		using DirEntryType = stdfs::file_type;


		class DirectoryEntry
		{
		private:
			Path              _path;
			stdfs::file_type  _type;

		public:
			DirectoryEntry();
			DirectoryEntry( const Path& path, DirEntryType type = DirEntryType::unknown );

			template <class Target>
			Target GetAs() const;

			const Path& GetPath() const;
			DirEntryType GetType() const;
		};


	}
}


#endif /* __ExsLib_Filesystem_DirectoryEntry_H__ */
