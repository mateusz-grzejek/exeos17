
#ifndef __ExsLib_SharedBase_File_H__
#define __ExsLib_SharedBase_File_H__

#include "DirectoryEntry.h"


namespace Exs
{
	namespace Filesystem
	{


		class File
		{
		private:
			Path _path;

		public:
			File();
			File( const Path& path );
			File( const DirectoryEntry& dirEntry );
		};


		template <>
		inline File DirectoryEntry::GetAs<File>() const
		{
			return File( *this );
		}


	}
}


#endif /* __ExsLib_SharedBase_File_H__ */
