
#include <ExsLib/SharedBase/Filesystem/DirectoryIterator.h>


namespace Exs
{
	namespace Filesystem
	{


		DirectoryIterator::DirectoryIterator( DirectoryIterator&& source )
			: _handle( source._handle )
			, _findData( source._findData )
			, _currentState( source._currentState )
		{

		}


		DirectoryIterator::DirectoryIterator( const Path& dirPath )
			: _handle( nullptr )
		{

		}


		DirectoryIterator::~DirectoryIterator()
		{

		}


		DirectoryIterator& DirectoryIterator::operator=( DirectoryIterator&& rhs )
		{

		}


		DirectoryIterator::operator bool() const
		{

		}


		DirectoryEntry& DirectoryIterator::operator*() const
		{

		}


		DirectoryEntry* DirectoryIterator::operator->() const
		{

		}


		DirectoryIterator& DirectoryIterator::operator++()
		{

		}


		DirectoryIterator DirectoryIterator::operator++( int )
		{

		}


		void DirectoryIterator::_NextEntry()
		{
		#if (EXS_TARGET_SYSTEM & EXS_TARGET_PLATFORM_FLAG_MICROSOFT)

			if (this->_handle != nullptr)
			{
				auto result = FindNextFileW( this->_handle, &(this->_findData) );
				if (result == TRUE)
				{

				}
				else
				{
					auto lastError = ::GetLastError();
					if (lastError == ERROR_NO_MORE_FILES)
					{
						::FindClose( this->_handle );
					}

					this->_handle = nullptr;
					this->_currentState = CurrentState::End;
				}
			}

		#elif (EXS_TARGET_SYSTEM & EXS_TARGET_PLATFORM_FLAG_POSIX)

		#endif
		}


		void DirectoryIterator::_OpenDir( const Path& dirPath )
		{
		#if (EXS_TARGET_SYSTEM & EXS_TARGET_PLATFORM_FLAG_MICROSOFT)

			if (HANDLE findHandle = ::FindFirstFileW( dirPath.GetPathname().c_str(), &(this->_findData) ))
			{
				this->_handle = findHandle;
				this->_currentState = CurrentState::Entry_Iter;
			}

		#elif (EXS_TARGET_SYSTEM & EXS_TARGET_PLATFORM_FLAG_POSIX)

		#endif
		}


		void DirectoryIterator::_FetchEntry()
		{

		}


	}
}
