
#ifndef __ExsLib_SharedBase_DirectoryIterator_H__
#define __ExsLib_SharedBase_DirectoryIterator_H__

#include "DirectoryEntry.h"
#include "FilesystemSysDefs.h"


namespace Exs
{
	namespace Filesystem
	{


		class Directory;
		class File;


		class DirectoryIterator
		{
		private:
			enum class CurrentState : Enum
			{
				End,
				Entry_Iter,
				Entry_Ref
			};

			FindStateHandle   _handle;
			FindStateData     _findData;
			DirectoryEntry    _dirEntry;
			CurrentState      _currentState;

		public:
			DirectoryIterator( const DirectoryIterator& ) = delete;
			DirectoryIterator& operator=( const DirectoryIterator& ) = delete;

			DirectoryIterator( DirectoryIterator&& source );
			DirectoryIterator( const Path& dirPath );
			~DirectoryIterator();

			DirectoryIterator& operator=( DirectoryIterator&& rhs );

			explicit operator bool() const;

			DirectoryEntry& operator*() const;

			DirectoryEntry* operator->() const;

			DirectoryIterator& operator++();

			DirectoryIterator operator++( int );

		private:
			void _OpenDir( const Path& dirPath );
			void _NextEntry();
			void _FetchEntry();
		};


	}
}


#endif /* __ExsLib_SharedBase_DirectoryIterator_H__ */
