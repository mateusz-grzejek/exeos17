
#ifndef __ExsLib_SharedBase_Directory_H__
#define __ExsLib_SharedBase_Directory_H__

#include "DirectoryEntry.h"


namespace Exs
{
	namespace Filesystem
	{


		class Directory
		{
		public:
			Directory();
			Directory( const Path& path );
			Directory( const DirectoryEntry& dirEntry );
		};


		template <>
		inline Directory DirectoryEntry::GetAs<Directory>() const
		{
			return Directory( *this );
		}


	}
}


#endif /* __ExsLib_SharedBase_Directory_H__ */
