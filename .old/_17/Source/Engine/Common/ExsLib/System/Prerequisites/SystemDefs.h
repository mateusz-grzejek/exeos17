
#ifndef __ExsLib_System_SystemDefs_H__
#define __ExsLib_System_SystemDefs_H__


namespace Exs
{
	namespace System
	{

		
	#if (EXS_TARGET_SYSTEM & EXS_TARGET_PLATFORM_FLAG_MICROSOFT)

		using NativeDLLHandle = HMODULE;
		using NativeThreadHandle = HANDLE;
		using NativeThreadID = DWORD;
		using NativeWindowHandle = HWND;

		constexpr NativeDLLHandle NativeDLLHandleNULL = nullptr;
		constexpr NativeThreadHandle NativeThreadHandleNULL = nullptr;
		constexpr NativeWindowHandle NativeWindowHandleNULL = nullptr;

	#elif (EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_LINUX)

		using NativeDLLHandle = void*;
		using NativeThreadHandle = pthread_t;
		using NativeWindowHandle = Window;

		constexpr NativeDLLHandle NativeDLLHandleNULL = nullptr;
		constexpr NativeThreadHandle NativeThreadHandleNULL = None;
		constexpr NativeWindowHandle NativeWindowHandleNULL = None;

	#elif (EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_ANDROID)

		using NativeDLLHandle = void*;
		using NativeThreadHandle = pthread_t;
		using NativeWindowHandle = ANativeWindow*;

		constexpr NativeDLLHandle NativeDLLHandleNULL = nullptr;
		constexpr NativeThreadHandle NativeThreadHandleNULL = (pthread_t)0;
		constexpr NativeWindowHandle NativeWindowHandleNULL = nullptr;

	#elif (EXS_TARGET_SYSTEM & EXS_TARGET_PLATFORM_FLAG_APPLE)

		using NativeDLLHandle = void*;
		using NativeThreadHandle = ? ? ? ;
		using NativeWindowHandle = ? ? ? ;

		constexpr NativeDLLHandle NativeDLLHandleNULL = nullptr;
		constexpr NativeThreadHandle NativeThreadHandleNULL = None;
		constexpr NativeWindowHandle NativeWindowHandleNULL = None;

	#endif


		struct DynamicLibState;
		struct ThreadState;
		struct WindowState;

		using DynamicLibHandle = std::shared_ptr<DynamicLibState>;
		using ThreadHandle = std::shared_ptr<ThreadState>;
		using WindowHandle = std::shared_ptr<WindowState>;


		NativeThreadID GetNativeThreadIDFromHandle( NativeThreadHandle handle );


	}
}


#endif /* __ExsLib_System_SystemDefs_H__ */
