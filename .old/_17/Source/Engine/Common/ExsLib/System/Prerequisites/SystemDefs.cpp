
#include <ExsLib/System/Prerequisites.h>


namespace Exs
{
	namespace System
	{


		NativeThreadID GetNativeThreadIDFromHandle( NativeThreadHandle handle )
		{
		#if (EXS_TARGET_SYSTEM & EXS_TARGET_PLATFORM_FLAG_MICROSOFT)
			DWORD threadID = ::GetThreadId( handle );
			return threadID;
		#endif
		}


	}
}
