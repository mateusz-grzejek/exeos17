
#ifndef __ExsLib_System_FilesystemCommon_H__
#define __ExsLib_System_FilesystemCommon_H__

#include <filesystem>


namespace stdfs
{

#if ( ( EXS_COMPILER & EXS_COMPILER_MSVC ) && (EXS_COMPILER <= EXS_COMPILER_MSVC_2017) )
	using namespace std::experimental::filesystem;
#else
	using namespace std::filesystem;
#endif

	using path_char_t = path::value_type;

}


namespace Exs
{
	namespace System
	{


		using Path_char_t = stdfs::path_char_t;

		using Path = stdfs::path;


	}
}


#endif /* __ExsLib_System_FilesystemCommon_H__ */
