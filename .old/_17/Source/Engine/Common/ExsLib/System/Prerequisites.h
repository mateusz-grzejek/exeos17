
#ifndef __ExsLib_SysLayer_Prerequisites_H__
#define __ExsLib_SysLayer_Prerequisites_H__

#include <ExsLib/CoreUtils/Prerequisites.h>

#include "Prerequisites/SystemDefs.h"
#include "Prerequisites/FilesystemCommon.h"

#endif /* __ExsLib_SysLayer_Prerequisites_H__ */
