
#ifndef __ExsLib_SysLayer_DynamicLib_H__
#define __ExsLib_SysLayer_DynamicLib_H__

#include "Prerequisites.h"


namespace Exs
{
	namespace System
	{


		struct DynamicLibState
		{
			NativeDLLHandle nativeHandle = NativeDLLHandleNULL;
		};


		DynamicLibHandle LoadDynamicLibrary( const Path& path );

		void* QueryDynamicLibrarySymbol( const DynamicLibHandle& handle );

		Result UnloadDynamicLibrary( const DynamicLibHandle& handle );




	}
}


#endif /* __ExsLib_SysLayer_DynamicLib_H__ */
