
#ifndef __Exs_Math_Rect_H__
#define __Exs_Math_Rect_H__

#include "Vector2.h"


namespace Exs
{


	template <typename Value_t>
	struct RectBase
	{
	public:
		typedef RectBase<Value_t> MyType;
		typedef Vector2<Value_t> VecType;

	public:
		VecType  position;
		VecType  size;

	public:
		RectBase()
		{ }

		RectBase(const VecType& pos, const VecType& size)
		: position(pos)
		, size(size)
		{ }

		template <typename Other_value_type>
		RectBase(const Vector2<Other_value_type>& pos, const Vector2<Other_value_type>& size)
		: position(pos)
		, size(size)
		{ }

		template <typename Tx, typename Ty, typename Tw, typename Th>
		RectBase(const Tx& x, const Ty& y, const Tw& width, const Th& height)
		: position(x, y)
		, size(width, height)
		{ }

		bool operator==(const MyType& rhs) const
		{
			return (this->position == rhs.position) && (this->size == rhs.size);
		}

		bool operator!=(const MyType& rhs) const
		{
			return (this->position != rhs.position) || (this->size != rhs.size);
		}
	};

	
	typedef RectBase<float>     RectF;
	typedef RectBase<double>    RectD;
	typedef RectBase<Int32>     RectI32;
	typedef RectBase<Uint32>    RectU32;
	typedef RectBase<Int64>     RectI64;
	typedef RectBase<Uint64>    RectU64;


}


#endif /* __Exs_Math_Rect_H__ */
