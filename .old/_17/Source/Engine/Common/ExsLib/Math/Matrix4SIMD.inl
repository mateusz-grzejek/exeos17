
namespace Exs
{


#if ( EXS_MATH_USE_VX128F )

	
	template <>
	template <>
	ExsForceInline Matrix4x4<float>& Matrix4x4<float>::operator*=(const Matrix4x4<float>& rhs)
	{
		__m128& mc0 = this->_c0._vxd;
		__m128& mc1 = this->_c1._vxd;
		__m128& mc2 = this->_c2._vxd;
		__m128& mc3 = this->_c3._vxd;

		const __m128& rc0 = rhs[0]._vxd;
		const __m128& rc1 = rhs[1]._vxd;
		const __m128& rc2 = rhs[2]._vxd;
		const __m128& rc3 = rhs[3]._vxd;
		
		const __m128 tmp_0 = _mm_shuffle_ps(mc0, mc1, 0x44);
		const __m128 tmp_1 = _mm_shuffle_ps(mc2, mc3, 0x44);
		const __m128 tmp_2 = _mm_shuffle_ps(mc0, mc1, 0xEE);
		const __m128 tmp_3 = _mm_shuffle_ps(mc2, mc3, 0xEE);
		
		const __m128 mr0 = _mm_shuffle_ps(tmp_0, tmp_1, 0x88);
		const __m128 mr1 = _mm_shuffle_ps(tmp_0, tmp_1, 0xDD);
		const __m128 mr2 = _mm_shuffle_ps(tmp_2, tmp_3, 0x88);
		const __m128 mr3 = _mm_shuffle_ps(tmp_2, tmp_3, 0xDD);
		
		const float mc0_r0 = _mm_cvtss_f32(_mm_dp_ps4_s0(mr0, rc0));
		const float mc0_r1 = _mm_cvtss_f32(_mm_dp_ps4_s0(mr1, rc0));
		const float mc0_r2 = _mm_cvtss_f32(_mm_dp_ps4_s0(mr2, rc0));
		const float mc0_r3 = _mm_cvtss_f32(_mm_dp_ps4_s0(mr3, rc0));

		const float mc1_r0 = _mm_cvtss_f32(_mm_dp_ps4_s0(mr0, rc1));
		const float mc1_r1 = _mm_cvtss_f32(_mm_dp_ps4_s0(mr1, rc1));
		const float mc1_r2 = _mm_cvtss_f32(_mm_dp_ps4_s0(mr2, rc1));
		const float mc1_r3 = _mm_cvtss_f32(_mm_dp_ps4_s0(mr3, rc1));

		const float mc2_r0 = _mm_cvtss_f32(_mm_dp_ps4_s0(mr0, rc2));
		const float mc2_r1 = _mm_cvtss_f32(_mm_dp_ps4_s0(mr1, rc2));
		const float mc2_r2 = _mm_cvtss_f32(_mm_dp_ps4_s0(mr2, rc2));
		const float mc2_r3 = _mm_cvtss_f32(_mm_dp_ps4_s0(mr3, rc2));

		const float mc3_r0 = _mm_cvtss_f32(_mm_dp_ps4_s0(mr0, rc3));
		const float mc3_r1 = _mm_cvtss_f32(_mm_dp_ps4_s0(mr1, rc3));
		const float mc3_r2 = _mm_cvtss_f32(_mm_dp_ps4_s0(mr2, rc3));
		const float mc3_r3 = _mm_cvtss_f32(_mm_dp_ps4_s0(mr3, rc3));
		
		mc0 = _mm_set_ps(mc0_r3, mc0_r2, mc0_r1, mc0_r0);
		mc1 = _mm_set_ps(mc1_r3, mc1_r2, mc1_r1, mc1_r0);
		mc2 = _mm_set_ps(mc2_r3, mc2_r2, mc2_r1, mc2_r0);
		mc3 = _mm_set_ps(mc3_r3, mc3_r2, mc3_r1, mc3_r0);
	
		return *this;
	}


	template <>
	ExsForceInline Matrix4x4<float> operator*(const Matrix4x4<float>& lhs, const Matrix4x4<float>& rhs)
	{
		const __m128& rc0 = rhs[0]._vxd;
		const __m128& rc1 = rhs[1]._vxd;
		const __m128& rc2 = rhs[2]._vxd;
		const __m128& rc3 = rhs[3]._vxd;
		
		const __m128& lc0 = lhs[0]._vxd;
		const __m128& lc1 = lhs[1]._vxd;
		const __m128& lc2 = lhs[2]._vxd;
		const __m128& lc3 = lhs[3]._vxd;

		const __m128 tmp_0 = _mm_shuffle_ps(lc0, lc1, 0x44);
		const __m128 tmp_1 = _mm_shuffle_ps(lc2, lc3, 0x44);
		const __m128 tmp_2 = _mm_shuffle_ps(lc0, lc1, 0xEE);
		const __m128 tmp_3 = _mm_shuffle_ps(lc2, lc3, 0xEE);
		
		const __m128 lr0 = _mm_shuffle_ps(tmp_0, tmp_1, 0x88);
		const __m128 lr1 = _mm_shuffle_ps(tmp_0, tmp_1, 0xDD);
		const __m128 lr2 = _mm_shuffle_ps(tmp_2, tmp_3, 0x88);
		const __m128 lr3 = _mm_shuffle_ps(tmp_2, tmp_3, 0xDD);
		
		const float mc0_r0 = _mm_cvtss_f32(_mm_dp_ps4_s0(lr0, rc0));
		const float mc0_r1 = _mm_cvtss_f32(_mm_dp_ps4_s0(lr1, rc0));
		const float mc0_r2 = _mm_cvtss_f32(_mm_dp_ps4_s0(lr2, rc0));
		const float mc0_r3 = _mm_cvtss_f32(_mm_dp_ps4_s0(lr3, rc0));

		const float mc1_r0 = _mm_cvtss_f32(_mm_dp_ps4_s0(lr0, rc1));
		const float mc1_r1 = _mm_cvtss_f32(_mm_dp_ps4_s0(lr1, rc1));
		const float mc1_r2 = _mm_cvtss_f32(_mm_dp_ps4_s0(lr2, rc1));
		const float mc1_r3 = _mm_cvtss_f32(_mm_dp_ps4_s0(lr3, rc1));

		const float mc2_r0 = _mm_cvtss_f32(_mm_dp_ps4_s0(lr0, rc2));
		const float mc2_r1 = _mm_cvtss_f32(_mm_dp_ps4_s0(lr1, rc2));
		const float mc2_r2 = _mm_cvtss_f32(_mm_dp_ps4_s0(lr2, rc2));
		const float mc2_r3 = _mm_cvtss_f32(_mm_dp_ps4_s0(lr3, rc2));

		const float mc3_r0 = _mm_cvtss_f32(_mm_dp_ps4_s0(lr0, rc3));
		const float mc3_r1 = _mm_cvtss_f32(_mm_dp_ps4_s0(lr1, rc3));
		const float mc3_r2 = _mm_cvtss_f32(_mm_dp_ps4_s0(lr2, rc3));
		const float mc3_r3 = _mm_cvtss_f32(_mm_dp_ps4_s0(lr3, rc3));

		return Matrix4x4<float>(
			mc0_r0, mc0_r1, mc0_r2, mc0_r3,
			mc1_r0, mc1_r1, mc1_r2, mc1_r3,
			mc2_r0, mc2_r1, mc2_r2, mc2_r3,
			mc3_r0, mc3_r1, mc3_r2, mc3_r3);
	}


	template <>
	ExsForceInline Vector4<float> operator*(const Matrix4x4<float>& lhs, const Vector4<float>& rhs)
	{
		const __m128& lc0 = lhs[0]._vxd;
		const __m128& lc1 = lhs[1]._vxd;
		const __m128& lc2 = lhs[2]._vxd;
		const __m128& lc3 = lhs[3]._vxd;

		const __m128 tmp_0 = _mm_shuffle_ps(lc0, lc1, 0x44);
		const __m128 tmp_1 = _mm_shuffle_ps(lc2, lc3, 0x44);
		const __m128 tmp_2 = _mm_shuffle_ps(lc0, lc1, 0xEE);
		const __m128 tmp_3 = _mm_shuffle_ps(lc2, lc3, 0xEE);
		
		const __m128 lr0 = _mm_shuffle_ps(tmp_0, tmp_1, 0x88);
		const __m128 lr1 = _mm_shuffle_ps(tmp_0, tmp_1, 0xDD);
		const __m128 lr2 = _mm_shuffle_ps(tmp_2, tmp_3, 0x88);
		const __m128 lr3 = _mm_shuffle_ps(tmp_2, tmp_3, 0xDD);
		
		const float v0 = _mm_cvtss_f32(_mm_dp_ps4_s0(lr0, rhs._vxd));
		const float v1 = _mm_cvtss_f32(_mm_dp_ps4_s0(lr1, rhs._vxd));
		const float v2 = _mm_cvtss_f32(_mm_dp_ps4_s0(lr2, rhs._vxd));
		const float v3 = _mm_cvtss_f32(_mm_dp_ps4_s0(lr3, rhs._vxd));

		return Vector4<float>(v0, v1, v2, v3);
	}


	template <>
	ExsForceInline Vector4<float> operator*(const Vector4<float>& lhs, const Matrix4x4<float>& rhs)
	{
		const __m128& rc0 = rhs[0]._vxd;
		const __m128& rc1 = rhs[1]._vxd;
		const __m128& rc2 = rhs[2]._vxd;
		const __m128& rc3 = rhs[3]._vxd;
		
		const float v0 = _mm_cvtss_f32(_mm_dp_ps4_s0(lhs._vxd, rc0));
		const float v1 = _mm_cvtss_f32(_mm_dp_ps4_s0(lhs._vxd, rc1));
		const float v2 = _mm_cvtss_f32(_mm_dp_ps4_s0(lhs._vxd, rc2));
		const float v3 = _mm_cvtss_f32(_mm_dp_ps4_s0(lhs._vxd, rc3));

		return Vector4<float>(v0, v1, v2, v3);
	}


	template <>
	ExsForceInline Matrix4x4<float> operator*(const Matrix4x4<float>& lhs, float rhs)
	{
		const __m128& lc0 = lhs[0]._vxd;
		const __m128& lc1 = lhs[1]._vxd;
		const __m128& lc2 = lhs[2]._vxd;
		const __m128& lc3 = lhs[3]._vxd;

		const __m128 mul_sv = _mm_set1_ps(rhs);
		
		const __m128 mc0 = _mm_mul_ps(lc0, mul_sv);
		const __m128 mc1 = _mm_mul_ps(lc1, mul_sv);
		const __m128 mc2 = _mm_mul_ps(lc2, mul_sv);
		const __m128 mc3 = _mm_mul_ps(lc3, mul_sv);

		return Matrix4x4<float>(
			reinterpret_cast<const float*>(&mc0),
			reinterpret_cast<const float*>(&mc1),
			reinterpret_cast<const float*>(&mc2),
			reinterpret_cast<const float*>(&mc3));
	}


	template <>
	ExsForceInline Matrix4x4<float> operator*(float lhs, const Matrix4x4<float>& rhs)
	{
		const __m128& rc0 = rhs[0]._vxd;
		const __m128& rc1 = rhs[1]._vxd;
		const __m128& rc2 = rhs[2]._vxd;
		const __m128& rc3 = rhs[3]._vxd;

		const __m128 mul_sv = _mm_set1_ps(lhs);
		
		const __m128 c0 = _mm_mul_ps(mul_sv, rc0);
		const __m128 c1 = _mm_mul_ps(mul_sv, rc1);
		const __m128 c2 = _mm_mul_ps(mul_sv, rc2);
		const __m128 c3 = _mm_mul_ps(mul_sv, rc3);

		return Matrix4x4<float>(
			reinterpret_cast<const float*>(&c0),
			reinterpret_cast<const float*>(&c1),
			reinterpret_cast<const float*>(&c2),
			reinterpret_cast<const float*>(&c3));
	}


#endif


#if ( EXS_MATH_USE_VX256D )

	
	template <>
	template <>
	ExsForceInline Matrix4x4<double>& Matrix4x4<double>::operator*=(const Matrix4x4<double>& rhs)
	{
		__m256d& mc0 = this->_c0._vxd;
		__m256d& mc1 = this->_c1._vxd;
		__m256d& mc2 = this->_c2._vxd;
		__m256d& mc3 = this->_c3._vxd;

		const __m256d& rc0 = rhs[0]._vxd;
		const __m256d& rc1 = rhs[1]._vxd;
		const __m256d& rc2 = rhs[2]._vxd;
		const __m256d& rc3 = rhs[3]._vxd;
		
		const __m128d mx0y0 = _mm256_extractf128_pd(mc0, 0);
		const __m128d mx1y1 = _mm256_extractf128_pd(mc1, 0);
		const __m128d mx2y2 = _mm256_extractf128_pd(mc2, 0);
		const __m128d mx3y3 = _mm256_extractf128_pd(mc3, 0);
		
		const __m128d mz0w0 = _mm256_extractf128_pd(mc0, 1);
		const __m128d mz1w1 = _mm256_extractf128_pd(mc1, 1);
		const __m128d mz2w2 = _mm256_extractf128_pd(mc2, 1);
		const __m128d mz3w3 = _mm256_extractf128_pd(mc3, 1);
		
		const __m256d mr0 = _mm256_set_m128d(_mm_shuffle_pd(mx2y2, mx3y3, 0), _mm_shuffle_pd(mx0y0, mx1y1, 0));
		const __m256d mr1 = _mm256_set_m128d(_mm_shuffle_pd(mx2y2, mx3y3, 3), _mm_shuffle_pd(mx0y0, mx1y1, 3));
		const __m256d mr2 = _mm256_set_m128d(_mm_shuffle_pd(mz2w2, mz3w3, 0), _mm_shuffle_pd(mz0w0, mz1w1, 0));
		const __m256d mr3 = _mm256_set_m128d(_mm_shuffle_pd(mz2w2, mz3w3, 3), _mm_shuffle_pd(mz0w0, mz1w1, 3));
		
		const double mc0_r0 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(mr0, rc0)));
		const double mc0_r1 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(mr1, rc0)));
		const double mc0_r2 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(mr2, rc0)));
		const double mc0_r3 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(mr3, rc0)));

		const double mc1_r0 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(mr0, rc1)));
		const double mc1_r1 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(mr1, rc1)));
		const double mc1_r2 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(mr2, rc1)));
		const double mc1_r3 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(mr3, rc1)));

		const double mc2_r0 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(mr0, rc2)));
		const double mc2_r1 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(mr1, rc2)));
		const double mc2_r2 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(mr2, rc2)));
		const double mc2_r3 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(mr3, rc2)));

		const double mc3_r0 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(mr0, rc3)));
		const double mc3_r1 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(mr1, rc3)));
		const double mc3_r2 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(mr2, rc3)));
		const double mc3_r3 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(mr3, rc3)));
		
		mc0 = _mm256_set_pd(mc0_r3, mc0_r2, mc0_r1, mc0_r0);
		mc1 = _mm256_set_pd(mc1_r3, mc1_r2, mc1_r1, mc1_r0);
		mc2 = _mm256_set_pd(mc2_r3, mc2_r2, mc2_r1, mc2_r0);
		mc3 = _mm256_set_pd(mc3_r3, mc3_r2, mc3_r1, mc3_r0);
	
		return *this;
	}


	template <>
	ExsForceInline Matrix4x4<double> operator*(const Matrix4x4<double>& lhs, const Matrix4x4<double>& rhs)
	{
		const __m256d& lc0 = lhs[0]._vxd;
		const __m256d& lc1 = lhs[1]._vxd;
		const __m256d& lc2 = lhs[2]._vxd;
		const __m256d& lc3 = lhs[3]._vxd;

		const __m256d& rc0 = rhs[0]._vxd;
		const __m256d& rc1 = rhs[1]._vxd;
		const __m256d& rc2 = rhs[2]._vxd;
		const __m256d& rc3 = rhs[3]._vxd;
		
		const __m128d lx0y0 = _mm256_extractf128_pd(lc0, 0);
		const __m128d lx1y1 = _mm256_extractf128_pd(lc1, 0);
		const __m128d lx2y2 = _mm256_extractf128_pd(lc2, 0);
		const __m128d lx3y3 = _mm256_extractf128_pd(lc3, 0);
		
		const __m128d lz0w0 = _mm256_extractf128_pd(lc0, 1);
		const __m128d lz1w1 = _mm256_extractf128_pd(lc1, 1);
		const __m128d lz2w2 = _mm256_extractf128_pd(lc2, 1);
		const __m128d lz3w3 = _mm256_extractf128_pd(lc3, 1);
		
		const __m256d lr0 = _mm256_set_m128d(_mm_shuffle_pd(lx2y2, lx3y3, 0), _mm_shuffle_pd(lx0y0, lx1y1, 0));
		const __m256d lr1 = _mm256_set_m128d(_mm_shuffle_pd(lx2y2, lx3y3, 3), _mm_shuffle_pd(lx0y0, lx1y1, 3));
		const __m256d lr2 = _mm256_set_m128d(_mm_shuffle_pd(lz2w2, lz3w3, 0), _mm_shuffle_pd(lz0w0, lz1w1, 0));
		const __m256d lr3 = _mm256_set_m128d(_mm_shuffle_pd(lz2w2, lz3w3, 3), _mm_shuffle_pd(lz0w0, lz1w1, 3));
		
		const double mc0_r0 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lr0, rc0)));
		const double mc0_r1 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lr1, rc0)));
		const double mc0_r2 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lr2, rc0)));
		const double mc0_r3 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lr3, rc0)));

		const double mc1_r0 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lr0, rc1)));
		const double mc1_r1 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lr1, rc1)));
		const double mc1_r2 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lr2, rc1)));
		const double mc1_r3 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lr3, rc1)));

		const double mc2_r0 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lr0, rc2)));
		const double mc2_r1 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lr1, rc2)));
		const double mc2_r2 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lr2, rc2)));
		const double mc2_r3 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lr3, rc2)));

		const double mc3_r0 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lr0, rc3)));
		const double mc3_r1 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lr1, rc3)));
		const double mc3_r2 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lr2, rc3)));
		const double mc3_r3 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lr3, rc3)));

		return Matrix4x4<float>(
			mc0_r0, mc0_r1, mc0_r2, mc0_r3,
			mc1_r0, mc1_r1, mc1_r2, mc1_r3,
			mc2_r0, mc2_r1, mc2_r2, mc2_r3,
			mc3_r0, mc3_r1, mc3_r2, mc3_r3);
	}


	template <>
	ExsForceInline Vector4<double> operator*(const Matrix4x4<double>& lhs, const Vector4<double>& rhs)
	{
		const __m256d& lc0 = lhs[0]._vxd;
		const __m256d& lc1 = lhs[1]._vxd;
		const __m256d& lc2 = lhs[2]._vxd;
		const __m256d& lc3 = lhs[3]._vxd;
		
		const __m128d lx0y0 = _mm256_extractf128_pd(lc0, 0);
		const __m128d lx1y1 = _mm256_extractf128_pd(lc1, 0);
		const __m128d lx2y2 = _mm256_extractf128_pd(lc2, 0);
		const __m128d lx3y3 = _mm256_extractf128_pd(lc3, 0);
		
		const __m128d lz0w0 = _mm256_extractf128_pd(lc0, 1);
		const __m128d lz1w1 = _mm256_extractf128_pd(lc1, 1);
		const __m128d lz2w2 = _mm256_extractf128_pd(lc2, 1);
		const __m128d lz3w3 = _mm256_extractf128_pd(lc3, 1);
		
		const __m256d lr0 = _mm256_set_m128d(_mm_shuffle_pd(lx2y2, lx3y3, 0), _mm_shuffle_pd(lx0y0, lx1y1, 0));
		const __m256d lr1 = _mm256_set_m128d(_mm_shuffle_pd(lx2y2, lx3y3, 3), _mm_shuffle_pd(lx0y0, lx1y1, 3));
		const __m256d lr2 = _mm256_set_m128d(_mm_shuffle_pd(lz2w2, lz3w3, 0), _mm_shuffle_pd(lz0w0, lz1w1, 0));
		const __m256d lr3 = _mm256_set_m128d(_mm_shuffle_pd(lz2w2, lz3w3, 3), _mm_shuffle_pd(lz0w0, lz1w1, 3));
		
		const double v0 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lr0, rhs._vxd)));
		const double v1 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lr1, rhs._vxd)));
		const double v2 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lr2, rhs._vxd)));
		const double v3 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lr3, rhs._vxd)));

		return Vector4<double>(v0, v1, v2, v3);
	}


	template <>
	ExsForceInline Vector4<double> operator*(const Vector4<double>& lhs, const Matrix4x4<double>& rhs)
	{
		const __m256d& rc0 = rhs[0]._vxd;
		const __m256d& rc1 = rhs[1]._vxd;
		const __m256d& rc2 = rhs[2]._vxd;
		const __m256d& rc3 = rhs[3]._vxd;
		
		const double v0 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lhs._vxd, rc0)));
		const double v1 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lhs._vxd, rc1)));
		const double v2 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lhs._vxd, rc2)));
		const double v3 = _mm_cvtsd_f64(_mm256_castpd256_pd128(_mm256_dp_pd4_s0(lhs._vxd, rc3)));

		return Vector4<double>(v0, v1, v2, v3);
	}


	template <>
	ExsForceInline Matrix4x4<double> operator*(const Matrix4x4<double>& lhs, double rhs)
	{
		const __m256d& lc0 = lhs[0]._vxd;
		const __m256d& lc1 = lhs[1]._vxd;
		const __m256d& lc2 = lhs[2]._vxd;
		const __m256d& lc3 = lhs[3]._vxd;

		const __m256d mul_sv = _mm256_set1_pd(rhs);
		
		const __m256d c0 = _mm256_mul_pd(lc0, mul_sv);
		const __m256d c1 = _mm256_mul_pd(lc1, mul_sv);
		const __m256d c2 = _mm256_mul_pd(lc2, mul_sv);
		const __m256d c3 = _mm256_mul_pd(lc3, mul_sv);

		return Matrix4x4<double>(
			reinterpret_cast<const double*>(&c0),
			reinterpret_cast<const double*>(&c1),
			reinterpret_cast<const double*>(&c2),
			reinterpret_cast<const double*>(&c3));
	}


	template <>
	ExsForceInline Matrix4x4<double> operator*(double lhs, const Matrix4x4<double>& rhs)
	{
		const __m256d& rc0 = rhs[0]._vxd;
		const __m256d& rc1 = rhs[1]._vxd;
		const __m256d& rc2 = rhs[2]._vxd;
		const __m256d& rc3 = rhs[3]._vxd;

		const __m256d mul_sv = _mm256_set1_pd(lhs);
		
		const __m256d c0 = _mm256_mul_pd(mul_sv, rc0);
		const __m256d c1 = _mm256_mul_pd(mul_sv, rc1);
		const __m256d c2 = _mm256_mul_pd(mul_sv, rc2);
		const __m256d c3 = _mm256_mul_pd(mul_sv, rc3);

		return Matrix4x4<double>(
			reinterpret_cast<const double*>(&c0),
			reinterpret_cast<const double*>(&c1),
			reinterpret_cast<const double*>(&c2),
			reinterpret_cast<const double*>(&c3));
	}


#endif


}
