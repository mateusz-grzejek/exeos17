
#ifndef __Exs_Math_Vector2_H__
#define __Exs_Math_Vector2_H__

#include "VectorBase.h"


namespace Exs
{


	template <typename T>
	struct Vector2 : public VectorBase<T, 2>
	{
	public:
		T x, y;

	public:
		Vector2()
		: x(static_cast<T>(0))
		, y(static_cast<T>(0))
		{ }

		Vector2(const Vector2<T>& origin)
		: x(origin.x)
		, y(origin.y)
		{ }

		template <typename U>
		Vector2(const Vector2<U>& vec2)
		: x(static_cast<T>(vec2.x))
		, y(static_cast<T>(vec2.y))
		{ }

		template <typename Tx, typename Ty>
		Vector2(Tx x, Ty y)
		: x(static_cast<T>(x))
		, y(static_cast<T>(y))
		{ }

		template <typename U>
		explicit Vector2(U scalar)
		: x(static_cast<T>(scalar))
		, y(static_cast<T>(scalar))
		{ }

		explicit Vector2(const Position& p)
		: x(static_cast<T>(p.x))
		, y(static_cast<T>(p.y))
		{ }

		explicit Vector2(const Size& s)
		: x(static_cast<T>(s.width))
		, y(static_cast<T>(s.height))
		{ }

		explicit Vector2(const T* data)
		: x(data[0])
		, y(data[1])
		{ }

		explicit Vector2(const UninitializedTag&)
		{ }

		Vector2<T>& operator=(const Vector2<T>& rhs)
		{
			this->x = rhs.x;
			this->y = rhs.y;

			return *this;
		}

		template <typename U>
		Vector2<T>& operator=(const Vector2<U>& rhs)
		{
			this->x = static_cast<T>(rhs.x);
			this->y = static_cast<T>(rhs.y);

			return *this;
		}

		template <typename U>
		Vector2<T>& operator=(U rhs)
		{
			this->x = static_cast<T>(rhs);
			this->y = static_cast<T>(rhs);

			return *this;
		}

		operator Position() const
		{
			return Position(static_cast<Int32>(this->x), static_cast<Int32>(this->y));
		}

		operator Size() const
		{
			ExsDebugAssert( (this->x >= 0) && (this->y >= 0) );
			return Size(static_cast<Uint32>(this->x), static_cast<Uint32>(this->y));
		}

		T* Data()
		{
			return &(this->x);
		}

		const T* Data() const
		{
			return &(this->x);
		}

		T& operator[](Size_t index)
		{
			ExsDebugAssert( index < 2 );
			return (&(this->x))[index];
		}

		const T& operator[](Size_t index) const
		{
			ExsDebugAssert( index < 2 );
			return (&(this->x))[index];
		}

		Vector2<T>& operator++()
		{
			++this->x;
			++this->y;

			return *this;
		}

		Vector2<T> operator++(int)
		{
			Vector2<T> result(*this);
			++(*this);

			return result;
		}

		Vector2<T>& operator--()
		{
			--this->x;
			--this->y;

			return *this;
		}

		Vector2<T> operator--(int)
		{
			Vector2<T> result(*this);
			--(*this);

			return result;
		}

		template <typename U>
		Vector2<T>& operator+=(const Vector2<U>& rhs)
		{
			this->x += static_cast<T>(rhs.x);
			this->y += static_cast<T>(rhs.y);

			return *this;
		}

		template <typename U>
		Vector2<T>& operator+=(U rhs)
		{
			this->x += static_cast<T>(rhs);
			this->y += static_cast<T>(rhs);

			return *this;
		}

		template <typename U>
		Vector2<T>& operator-=(const Vector2<U>& rhs)
		{
			this->x -= static_cast<T>(rhs.x);
			this->y -= static_cast<T>(rhs.y);

			return *this;
		}

		template <typename U>
		Vector2<T>& operator-=(U rhs)
		{
			this->x -= static_cast<T>(rhs);
			this->y -= static_cast<T>(rhs);

			return *this;
		}

		template <typename U>
		Vector2<T>& operator*=(const Vector2<U>& rhs)
		{
			this->x *= static_cast<T>(rhs.x);
			this->y *= static_cast<T>(rhs.y);

			return *this;
		}

		template <typename U>
		Vector2<T>& operator*=(U rhs)
		{
			this->x *= static_cast<T>(rhs);
			this->y *= static_cast<T>(rhs);

			return *this;
		}

		template <typename U>
		Vector2<T>& operator/=(const Vector2<U>& rhs)
		{
			this->x /= static_cast<T>(rhs.x);
			this->y /= static_cast<T>(rhs.y);

			return *this;
		}

		template <typename U>
		Vector2<T>& operator/=(U rhs)
		{
			this->x /= static_cast<T>(rhs);
			this->y /= static_cast<T>(rhs);

			return *this;
		}
	};

	
	template <typename T>
	inline Vector2<T> operator+(const Vector2<T>& lhs, const Vector2<T>& rhs)
	{
		return Vector2<T>(
			lhs.x + rhs.x,
			lhs.y + rhs.y);
	}
	
	template <typename T>
	inline Vector2<T> operator+(const Vector2<T>& lhs, T rhs)
	{
		return Vector2<T>(
			lhs.x + rhs,
			lhs.y + rhs);
	}

	template <typename T>
	inline Vector2<T> operator+(T lhs, const Vector2<T>& rhs)
	{
		return Vector2<T>(
			lhs + rhs.x,
			lhs + rhs.y);
	}
	
	template <typename T>
	inline Vector2<T> operator-(const Vector2<T>& lhs, const Vector2<T>& rhs)
	{
		return Vector2<T>(
			lhs.x - rhs.x,
			lhs.y - rhs.y);
	}

	template <typename T>
	inline Vector2<T> operator-(const Vector2<T>& lhs, T rhs)
	{
		return Vector2<T>(
			lhs.x - rhs,
			lhs.y - rhs);
	}
	
	template <typename T>
	inline Vector2<T> operator-(T lhs, const Vector2<T>& rhs)
	{
		return Vector2<T>(
			lhs - rhs.x,
			lhs - rhs.y);
	}

	template <typename T>
	inline Vector2<T> operator*(const Vector2<T>& lhs, const Vector2<T>& rhs)
	{
		return Vector2<T>(
			lhs.x * rhs.x,
			lhs.y * rhs.y);
	}

	template <typename T>
	inline Vector2<T> operator*(const Vector2<T>& lhs, T rhs)
	{
		return Vector2<T>(
			lhs.x * rhs,
			lhs.y * rhs);
	}
	
	template <typename T>
	inline Vector2<T> operator*(T lhs, const Vector2<T>& rhs)
	{
		return Vector2<T>(
			lhs * rhs.x,
			lhs * rhs.y);
	}
	
	template <typename T>
	inline Vector2<T> operator/(const Vector2<T>& lhs, const Vector2<T>& rhs)
	{
		return Vector2<T>(
			lhs.x / rhs.x,
			lhs.y / rhs.y);
	}

	template <typename T>
	inline Vector2<T> operator/(const Vector2<T>& lhs, T rhs)
	{
		return Vector2<T>(
			lhs.x / rhs,
			lhs.y / rhs);
	}
	
	template <typename T>
	inline Vector2<T> operator/(T lhs, const Vector2<T>& rhs)
	{
		return Vector2<T>(
			lhs / rhs.x,
			lhs / rhs.y);
	}

	template <typename T, typename U>
	inline Vector2<T> operator-(const Vector2<T>& rhs)
	{
		return Vector2<T>(
			-rhs.x,
			-rhs.y);
	}

	
	template <typename T>
	inline bool operator==(const Vector2<T>& lhs, const Vector2<T>& rhs)
	{
		return (lhs.x == rhs.x) && (lhs.y == rhs.y);
	}

	template <typename T>
	inline bool operator!=(const Vector2<T>& lhs, const Vector2<T>& rhs)
	{
		return (lhs.x != rhs.x) || (lhs.y != rhs.y);
	}


}


#endif /* __Exs_Math_Vector2_H__ */
