
#ifndef __Exs_Math_Vector3_H__
#define __Exs_Math_Vector3_H__

#include "Vector2.h"


namespace Exs
{


	template <typename T>
	struct Vector3 : public VectorBase<T, 3>
	{
	public:
		T x, y, z;

	public:
		Vector3()
		: x(static_cast<T>(0))
		, y(static_cast<T>(0))
		, z(static_cast<T>(0))
		{ }

		Vector3(const Vector3<T>& origin)
		: x(origin.x)
		, y(origin.y)
		, z(origin.z)
		{ }

		template <typename U>
		Vector3(const Vector3<U>& vec3)
		: x(static_cast<T>(vec3.x))
		, y(static_cast<T>(vec3.y))
		, z(static_cast<T>(vec3.z))
		{ }
		
		template <typename U, typename Tz>
		Vector3(const Vector2<U>& xy, Tz z)
		: x(static_cast<T>(xy.x))
		, y(static_cast<T>(xy.y))
		, z(static_cast<T>(z))
		{ }

		template <typename Tx, typename U>
		Vector3(Tx x, const Vector2<U>& yz)
		: x(static_cast<T>(x))
		, y(static_cast<T>(yz.x))
		, z(static_cast<T>(yz.y))
		{ }

		template <typename Tx, typename Ty, typename Tz>
		Vector3(Tx x, Ty y, Tz z)
		: x(static_cast<T>(x))
		, y(static_cast<T>(y))
		, z(static_cast<T>(z))
		{ }

		explicit Vector3(T scalar)
		: x(scalar)
		, y(scalar)
		, z(scalar)
		{ }

		explicit Vector3(const T* data)
		: x(data[0])
		, y(data[1])
		, z(data[2])
		{ }

		explicit Vector3(const UninitializedTag&)
		{ }

		Vector3<T>& operator=(const Vector3<T>& rhs)
		{
			this->x = rhs.x;
			this->y = rhs.y;
			this->z = rhs.z;

			return *this;
		}

		template <typename U>
		Vector3<T>& operator=(const Vector3<U>& rhs)
		{
			this->x = static_cast<T>(rhs.x);
			this->y = static_cast<T>(rhs.y);
			this->z = static_cast<T>(rhs.z);

			return *this;
		}

		template <typename U>
		Vector3<T>& operator=(U rhs)
		{
			this->x = static_cast<T>(rhs);
			this->y = static_cast<T>(rhs);
			this->z = static_cast<T>(rhs);

			return *this;
		}

		operator Vector2<T>() const
		{
			return Vector2<T>(this->x, this->y);
		}

		T* Data()
		{
			return &(this->x);
		}

		const T* Data() const
		{
			return &(this->x);
		}

		T& operator[](Size_t index)
		{
			ExsDebugAssert( index < 3 );
			return (&(this->x))[index];
		}

		const T& operator[](Size_t index) const
		{
			ExsDebugAssert( index < 3 );
			return (&(this->x))[index];
		}

		Vector3<T>& operator++()
		{
			++this->x;
			++this->y;
			++this->z;

			return *this;
		}

		Vector3<T> operator++(int)
		{
			Vector3<T> result(*this);
			++(*this);

			return result;
		}

		Vector3<T>& operator--()
		{
			--this->x;
			--this->y;
			--this->z;

			return *this;
		}

		Vector3<T> operator--(int)
		{
			Vector3<T> result(*this);
			--(*this);

			return result;
		}

		template <typename U>
		Vector3<T>& operator+=(const Vector3<U>& rhs)
		{
			this->x += static_cast<T>(rhs.x);
			this->y += static_cast<T>(rhs.y);
			this->z += static_cast<T>(rhs.z);

			return *this;
		}

		template <typename U>
		Vector3<T>& operator+=(U rhs)
		{
			this->x += static_cast<T>(rhs);
			this->y += static_cast<T>(rhs);
			this->z += static_cast<T>(rhs);

			return *this;
		}

		template <typename U>
		Vector3<T>& operator-=(const Vector3<U>& rhs)
		{
			this->x -= static_cast<T>(rhs.x);
			this->y -= static_cast<T>(rhs.y);
			this->z -= static_cast<T>(rhs.z);

			return *this;
		}

		template <typename U>
		Vector3<T>& operator-=(U rhs)
		{
			this->x -= static_cast<T>(rhs);
			this->y -= static_cast<T>(rhs);
			this->z -= static_cast<T>(rhs);

			return *this;
		}

		template <typename U>
		Vector3<T>& operator*=(const Vector3<U>& rhs)
		{
			this->x *= static_cast<T>(rhs.x);
			this->y *= static_cast<T>(rhs.y);
			this->z *= static_cast<T>(rhs.z);

			return *this;
		}

		template <typename U>
		Vector3<T>& operator*=(U rhs)
		{
			this->x *= static_cast<T>(rhs);
			this->y *= static_cast<T>(rhs);
			this->z *= static_cast<T>(rhs);

			return *this;
		}

		template <typename U>
		Vector3<T>& operator/=(const Vector3<U>& rhs)
		{
			this->x /= static_cast<T>(rhs.x);
			this->y /= static_cast<T>(rhs.y);
			this->z /= static_cast<T>(rhs.z);

			return *this;
		}

		template <typename U>
		Vector3<T>& operator/=(U rhs)
		{
			this->x /= static_cast<T>(rhs);
			this->y /= static_cast<T>(rhs);
			this->z /= static_cast<T>(rhs);

			return *this;
		}
	};

	
	template <typename T>
	inline Vector3<T> operator+(const Vector3<T>& lhs, const Vector3<T>& rhs)
	{
		return Vector3<T>(
			lhs.x + rhs.x,
			lhs.y + rhs.y,
			lhs.z + rhs.z);
	}

	template <typename T>
	inline Vector3<T> operator+(const Vector3<T>& lhs, T rhs)
	{
		return Vector3<T>(
			lhs.x + rhs,
			lhs.y + rhs,
			lhs.z + rhs);
	}

	template <typename T>
	inline Vector3<T> operator+(T lhs, const Vector3<T>& rhs)
	{
		return Vector3<T>(
			lhs + rhs.x,
			lhs + rhs.y,
			lhs + rhs.z);
	}

	template <typename T>
	inline Vector3<T> operator-(const Vector3<T>& lhs, const Vector3<T>& rhs)
	{
		return Vector3<T>(
			lhs.x - rhs.x,
			lhs.y - rhs.y,
			lhs.z - rhs.z);
	}

	template <typename T>
	inline Vector3<T> operator-(const Vector3<T>& lhs, T rhs)
	{
		return Vector3<T>(
			lhs.x - rhs,
			lhs.y - rhs,
			lhs.z - rhs);
	}

	template <typename T>
	inline Vector3<T> operator-(T lhs, const Vector3<T>& rhs)
	{
		return Vector3<T>(
			lhs - rhs.x,
			lhs - rhs.y,
			lhs - rhs.z);
	}

	template <typename T>
	inline Vector3<T> operator*(const Vector3<T>& lhs, const Vector3<T>& rhs)
	{
		return Vector3<T>(
			lhs.x * rhs.x,
			lhs.y * rhs.y,
			lhs.z * rhs.z);
	}
	
	template <typename T>
	inline Vector3<T> operator*(const Vector3<T>& lhs, T rhs)
	{
		return Vector3<T>(
			lhs.x * rhs,
			lhs.y * rhs,
			lhs.z * rhs);
	}

	template <typename T>
	inline Vector3<T> operator*(T lhs, const Vector3<T>& rhs)
	{
		return Vector3<T>(
			lhs * rhs.x,
			lhs * rhs.y,
			lhs * rhs.z);
	}

	template <typename T>
	inline Vector3<T> operator/(const Vector3<T>& lhs, const Vector3<T>& rhs)
	{
		return Vector3<T>(
			lhs.x / rhs.x,
			lhs.y / rhs.y,
			lhs.z / rhs.z);
	}

	template <typename T>
	inline Vector3<T> operator/(const Vector3<T>& lhs, T rhs)
	{
		return Vector3<T>(
			lhs.x / rhs,
			lhs.y / rhs,
			lhs.z / rhs);
	}

	template <typename T>
	inline Vector3<T> operator/(T lhs, const Vector3<T>& rhs)
	{
		return Vector3<T>(
			lhs / rhs.x,
			lhs / rhs.y,
			lhs / rhs.z);
	}

	template <typename T, typename U>
	inline Vector3<T> operator-(const Vector3<T>& rhs)
	{
		return Vector3<T>(-rhs.x, -rhs.y, -rhs.z);
	}


	template <typename T>
	inline bool operator==(const Vector3<T>& lhs, const Vector3<T>& rhs)
	{
		return (lhs.x == rhs.x) && (lhs.y == rhs.y) && (lhs.z == rhs.z);
	}

	template <typename T>
	inline bool operator!=(const Vector3<T>& lhs, const Vector3<T>& rhs)
	{
		return (lhs.x != rhs.x) || (lhs.y != rhs.y) || (lhs.z != rhs.z);
	}


}


#endif /* __Exs_Math_Vector3_H__ */
