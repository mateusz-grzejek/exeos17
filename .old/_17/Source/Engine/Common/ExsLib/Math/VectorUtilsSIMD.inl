
namespace Exs
{


#if ( EXS_SUPPORTED_EIS & EXS_EIS_SSE )

	template <>
	template <>
	ExsForceInline Vector4<float> Vector<Precision::Normal>::Inverse(const Vector4<float>& vec4)
	{
		const __m128 result = _mm_rcp_ps(vec4._vxd);
		return Vector4<float>(reinterpret_cast<const float*>(&result));
	}

	template <>
	template <>
	ExsForceInline Vector4<float> Vector<Precision::Normal>::Sqrt(const Vector4<float>& vec4)
	{
		const __m128 result = _mm_sqrt_ps(vec4._vxd);
		return Vector4<float>(reinterpret_cast<const float*>(&result));
	}

	template <>
	template <>
	ExsForceInline Vector4<float> Vector<Precision::Normal>::InvSqrt(const Vector4<float>& vec4)
	{
		const __m128 result = _mm_div_ps(_mm_set1_ps(1), _mm_sqrt_ps(vec4._vxd));
		return Vector4<float>(reinterpret_cast<const float*>(&result));

	}
	
	template <>
	template <>
	ExsForceInline Vector4<float> Vector<Precision::Normal>::InvSqrtApprox(const Vector4<float>& vec4)
	{
		const __m128 result = _mm_rsqrt_ps(vec4._vxd);
		return Vector4<float>(reinterpret_cast<const float*>(&result));

	}

	template <>
	template <>
	ExsForceInline float Vector<Precision::Normal>::Dot(const Vector4<float>& v1, const Vector4<float>& v2)
	{
		__m128 result = _mm_dp_ps4_s0(v1._vxd, v2._vxd);
		return _mm_cvtss_f32(result);
	}

	template <>
	template <>
	ExsForceInline Vector4<float> Vector<Precision::Normal>::Normalize(const Vector4<float>& vec4)
	{
		const __m128 inv_norm = _mm_sqrt_ps(_mm_dp_ps4(vec4._vxd, vec4._vxd));
		const __m128 result = _mm_div_ps(vec4._vxd, inv_norm);
		
		return Vector4<float>(reinterpret_cast<const float*>(&result));
	}

	template <>
	template <>
	ExsForceInline Vector4<float> Vector<Precision::Normal>::NormalizeApprox(const Vector4<float>& vec4)
	{
		const __m128 inv_norm = _mm_rsqrt_ps(_mm_dp_ps4(vec4._vxd, vec4._vxd));
		const __m128 result = _mm_mul_ps(vec4._vxd, inv_norm);
		
		return Vector4<float>(reinterpret_cast<const float*>(&result));
	}

	template <>
	template <>
	ExsForceInline float Vector<Precision::Normal>::Length(const Vector4<float>& vec4)
	{
		const __m128 result = _mm_sqrt_ss(_mm_dp_ps4_s0(vec4._vxd, vec4._vxd));
		return _mm_cvtss_f32(result);
	}

	template <>
	template <>
	ExsForceInline float Vector<Precision::Normal>::Distance(const Vector4<float>& v1, const Vector4<float>& v2)
	{
		const __m128 diff_vec = _mm_sub_ps(v2._vxd, v1._vxd);
		const __m128 result = _mm_sqrt_ss(_mm_dp_ps4_s0(diff_vec, diff_vec));

		return _mm_cvtss_f32(result);
	}

#endif


#if ( EXS_SUPPORTED_EIS & EXS_EIS_AVX )

	template <>
	template <>
	ExsForceInline Vector4<double> Vector<Precision::High>::Inverse(const Vector4<double>& vec4)
	{
		const __m256d result = _mm256_div_pd(_mm256_set1_pd(1), vec4._vxd);
		return Vector4<double>(reinterpret_cast<const double*>(&result));
	}

	template <>
	template <>
	ExsForceInline Vector4<double> Vector<Precision::High>::Sqrt(const Vector4<double>& vec4)
	{
		const __m256d result = _mm256_sqrt_pd(vec4._vxd);
		return Vector4<double>(reinterpret_cast<const double*>(&result));
	}

	template <>
	template <>
	ExsForceInline Vector4<double> Vector<Precision::High>::InvSqrt(const Vector4<double>& vec4)
	{
		const __m256d result = _mm256_div_pd(_mm256_set1_pd(1), _mm256_sqrt_pd(vec4._vxd));
		return Vector4<double>(reinterpret_cast<const double*>(&result));

	}

	template <>
	template <>
	ExsForceInline Vector4<double> Vector<Precision::High>::InvSqrtApprox(const Vector4<double>& vec4)
	{
		return InvSqrt(vec4);
	}

	template <>
	template <>
	ExsForceInline double Vector<Precision::High>::Dot(const Vector4<double>& v1, const Vector4<double>& v2)
	{
		const __m256d result = _mm256_dp_pd4_s0(v1._vxd, v2._vxd);
		return _mm_cvtsd_f64(_mm256_castpd256_pd128(result));
	}

	template <>
	template <>
	ExsForceInline Vector4<double> Vector<Precision::High>::Normalize(const Vector4<double>& vec4)
	{
		const __m256d inv_norm = _mm256_sqrt_pd(_mm256_dp_pd4(vec4._vxd, vec4._vxd));
		const __m256d result = _mm256_div_pd(vec4._vxd, inv_norm);
		
		return Vector4<double>(reinterpret_cast<const double*>(&result));
	}

	template <>
	template <>
	ExsForceInline Vector4<double> Vector<Precision::High>::NormalizeApprox(const Vector4<double>& vec4)
	{
		return Normalize(vec4);
	}

	template <>
	template <>
	ExsForceInline double Vector<Precision::High>::Length(const Vector4<double>& vec4)
	{
		const __m256d result = _mm256_sqrt_pd(_mm256_dp_pd4_s0(vec4._vxd, vec4._vxd));
		
		return _mm_cvtsd_f64(_mm256_castpd256_pd128(result));
	}

	template <>
	template <>
	ExsForceInline double Vector<Precision::High>::Distance(const Vector4<double>& v1, const Vector4<double>& v2)
	{
		const __m256d diff_vec = _mm256_sub_pd(v2._vxd, v1._vxd);
		const __m256d result = _mm256_sqrt_pd(_mm256_dp_pd4_s0(diff_vec, diff_vec));
		
		return _mm_cvtsd_f64(_mm256_castpd256_pd128(result));
	}

#endif


}
