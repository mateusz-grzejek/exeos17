
#ifndef __Exs_Math_MathBase_H__
#define __Exs_Math_MathBase_H__

#include <Exs/Core/Prerequisites.h>


// [D]ouble [P]recision [C]onstants

#define EXS_MATH_DPC_PI       3.14159265358979323846
#define EXS_MATH_DPC_PI_2     1.57079632679489661923
#define EXS_MATH_DPC_PI_4     0.78539816339744830961
#define EXS_MATH_DPC_E        2.71828182845904523536
#define EXS_MATH_DPC_LOG2_E   1.44269504088896340736
#define EXS_MATH_DPC_LOG10_E  0.43429448190325182765
#define EXS_MATH_DPC_LN_2     0.69314718055994530941
#define EXS_MATH_DPC_LN_10    2.30258509299404568402
#define EXS_MATH_DPC_1_PI     0.31830988618379067153
#define EXS_MATH_DPC_2_PI     0.63661977236758134307
#define EXS_MATH_DPC_SQRT_2   1.41421356237309504880
#define EXS_MATH_DPC_SQRT_PI  1.77245385090551602729
#define EXS_MATH_DPC_DTR_1    0.01745329251994329576   // 1 degree in radians
#define EXS_MATH_DPC_DTR_15   0.26179938779914943653   // 15 degrees in radians
#define EXS_MATH_DPC_DTR_30   0.52359877559829887307   // 30 degrees in radians
#define EXS_MATH_DPC_DTR_45   0.78539816339744830961   // 45 degrees in radians
#define EXS_MATH_DPC_DTR_60   1.04719755119659774615   // 60 degrees in radians
#define EXS_MATH_DPC_DTR_90   1.57079632679489661923   // 90 degrees in radians
#define EXS_MATH_DPC_DTR_120  2.09439510239319549230   // 120 degrees in radians
#define EXS_MATH_DPC_DTR_180  3.14159265358979323846   // 180 degrees in radians
#define EXS_MATH_DPC_DTR_360  6.28318530717958647692   // 360 degrees in radians
#define EXS_MATH_DPC_RTD_1    57.29577951308232087679  // 1 radian in degrees
#define EXS_MATH_DPC_RTD_2    114.59155902616464175359 // 2 radians in degrees
#define EXS_MATH_DPC_RTD_3    171.88733853924696263039 // 3 radians in degrees


// [S]ingle [P]recision [C]onstants

#define EXS_MATH_SPC_PI       3.1415926536f
#define EXS_MATH_SPC_PI_2     1.5707963268f
#define EXS_MATH_SPC_PI_4     0.7853981634f
#define EXS_MATH_SPC_E        2.7182818285f
#define EXS_MATH_SPC_LOG2_E   1.4426950409f
#define EXS_MATH_SPC_LOG10_E  0.4342944819f
#define EXS_MATH_SPC_LN_2     0.6931471806f
#define EXS_MATH_SPC_LN_10    2.3025850930f
#define EXS_MATH_SPC_1_PI     0.3183098862f
#define EXS_MATH_SPC_2_PI     0.6366197724f
#define EXS_MATH_SPC_SQRT_2   1.4142135624f
#define EXS_MATH_SPC_SQRT_PI  1.7724538509f
#define EXS_MATH_SPC_DTR_1    0.0174532925f   // 1 degree in radians
#define EXS_MATH_SPC_DTR_15   0.2617993878f   // 15 degrees in radians
#define EXS_MATH_SPC_DTR_30   0.5235987756f   // 30 degrees in radians
#define EXS_MATH_SPC_DTR_45   0.7853981634f   // 45 degrees in radians
#define EXS_MATH_SPC_DTR_60   1.0471975512f   // 60 degrees in radians
#define EXS_MATH_SPC_DTR_90   1.5707963268f   // 90 degrees in radians
#define EXS_MATH_SPC_DTR_120  2.0943951024f   // 120 degrees in radians
#define EXS_MATH_SPC_DTR_180  3.1415926536f   // 180 degrees in radians
#define EXS_MATH_SPC_DTR_360  6.2831853072f   // 360 degrees in radians
#define EXS_MATH_SPC_RTD_1    57.2957795131f  // 1 radian in degrees
#define EXS_MATH_SPC_RTD_2    114.5915590262f // 2 radians in degrees
#define EXS_MATH_SPC_RTD_3    171.8873385392f // 3 radians in degrees


#define EXS_MATH_DEGREES_TO_RADIANS(degrees) ((degrees) * EXS_MATH_SPC_DTR_1)
#define EXS_MATH_RADIANS_TO_DEGREES(radians) ((radians) * EXS_MATH_SPC_RTD_1)


namespace Exs
{


	typedef float Scalar_t;
	typedef double Hi_prec_scalar_t;


	enum class Precision : unsigned int
	{
		Normal,
		High,

	#if ( EXS_CONFIG_BASE_USE_DOUBLE_PRECISION )
		Default = High
	#else
		Default = Normal
	#endif
	};


	template <Precision>
	struct ScalarPrecisionType
	{
		typedef Scalar_t Type;
	};

	template <>
	struct ScalarPrecisionType<Precision::High>
	{
		typedef Hi_prec_scalar_t Type;
	};


	template <typename Arg_t>
	struct SqrtResultType
	{
		typedef typename std::enable_if<std::is_integral<Arg_t>::value, double>::type Type;
	};

	template <>
	struct SqrtResultType<float>
	{
		typedef float Type;
	};

	template <>
	struct SqrtResultType<double>
	{
		typedef double Type;
	};


	template <>
	struct SqrtResultType<long double>
	{
		typedef long double Type;
	};


	template <typename T>
	inline typename SqrtResultType<T>::Type Sqrt(T value)
	{
		return sqrt(static_cast<typename SqrtResultType<T>::Type>(value));
	}


	template <typename T>
	inline typename SqrtResultType<T>::Type InvSqrt(T value)
	{
		return static_cast<typename SqrtResultType<T>::Type>(1) / Sqrt(value);
	}


	template <typename T>
	inline T Clamp(T value, T min, T max)
	{
		return stdx::get_max_of(stdx::get_min_of(value, max), min);
	}


}


#endif /* __Exs_Math_MathBase_H__ */
