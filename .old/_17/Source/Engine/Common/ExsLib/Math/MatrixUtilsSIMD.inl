
namespace Exs
{


#if ( EXS_MATH_USE_VX128F )

	
	template <>
	template <>
	ExsForceInline Matrix4x4<float> Matrix<Precision::Normal>::Transpose(const Matrix4x4<float>& mat4)
	{
		const __m128& mc0 = mat4[0]._vxd; // [ x0, y0, z0, w0 ]
		const __m128& mc1 = mat4[1]._vxd; // [ x1, y1, z1, w1 ]
		const __m128& mc2 = mat4[2]._vxd; // [ x2, y2, z2, w2 ]
		const __m128& mc3 = mat4[3]._vxd; // [ x3, y3, z3, w3 ]
		
		const __m128 tmp_0 = _mm_shuffle_ps(mc0, mc1, 0x44); // [ x0, y0, x1, y1 ]
		const __m128 tmp_1 = _mm_shuffle_ps(mc2, mc3, 0x44); // [ x2, y2, x3, y3 ]
		const __m128 tmp_2 = _mm_shuffle_ps(mc0, mc1, 0xEE); // [ z0, w0, z1, w1 ]
		const __m128 tmp_3 = _mm_shuffle_ps(mc2, mc3, 0xEE); // [ z2, w2, z3, w3 ]
		
		const __m128 c0 = _mm_shuffle_ps(tmp_0, tmp_1, 0x88); // [ x0, x1, x2, x3 ]
		const __m128 c1 = _mm_shuffle_ps(tmp_0, tmp_1, 0xDD); // [ y0, y1, y2, y3 ]
		const __m128 c2 = _mm_shuffle_ps(tmp_2, tmp_3, 0x88); // [ z0, z1, z2, z3 ]
		const __m128 c3 = _mm_shuffle_ps(tmp_2, tmp_3, 0xDD); // [ w0, w1, w2, w3 ]

		return Matrix4x4<float>(
			reinterpret_cast<const float*>(&c0),
			reinterpret_cast<const float*>(&c1),
			reinterpret_cast<const float*>(&c2),
			reinterpret_cast<const float*>(&c3));
	}


	template <>
	template <>
	ExsForceInline Matrix4x4<float> Matrix<Precision::Normal>::Rotation(float axisX, float axisY, float axisZ, float angle)
	{
		const __m128 axis = _mm_set_ps(0, axisZ, axisY, axisX);
		
		const __m128 inv_norm = _mm_sqrt_ps(_mm_dp_ps4(axis, axis));
		const __m128 axis_norm = _mm_div_ps(axis, inv_norm);

		const float angle_cos = cos(angle);
		const float angle_sin = sin(angle);

		const float one_minus_cos = 1.0f - angle_cos;
			
		const __m128 axmc = _mm_mul_ps(axis_norm, _mm_set1_ps(one_minus_cos));
		const __m128 axms = _mm_mul_ps(axis_norm, _mm_set1_ps(angle_sin));
		
		const float* axmc_f = reinterpret_cast<const float*>(&axmc);
		const float* axms_f = reinterpret_cast<const float*>(&axms);
		
		const __m128 tmp_0 = _mm_mul_ps(axis_norm, _mm_set1_ps(axmc_f[0]));
		const __m128 tmp_1 = _mm_mul_ps(axis_norm, _mm_set1_ps(axmc_f[1]));
		const __m128 tmp_2 = _mm_mul_ps(axis_norm, _mm_set1_ps(axmc_f[2]));

		const __m128 col_0 = _mm_add_ps(tmp_0, _mm_set_ps(0, -axms_f[1], axms_f[2], angle_cos));
		const __m128 col_1 = _mm_add_ps(tmp_1, _mm_set_ps(0, axms_f[0], angle_cos, -axms_f[2]));
		const __m128 col_2 = _mm_add_ps(tmp_2, _mm_set_ps(0, angle_cos, -axms_f[0], axms_f[1]));
		const __m128 col_3 = _mm_set_ps(1, 0, 0, 0);

		return Matrix4x4<float>(
			reinterpret_cast<const float*>(&col_0),
			reinterpret_cast<const float*>(&col_1),
			reinterpret_cast<const float*>(&col_2),
			reinterpret_cast<const float*>(&col_3));
	}


#endif


#if ( EXS_MATH_USE_VX256D )

	
	template <>
	template <>
	ExsForceInline Matrix4x4<double> Matrix<Precision::High>::Transpose(const Matrix4x4<double>& mat4)
	{
		const __m256d& mc0 = mat4[0]._vxd; // [ x0, y0, z0, w0 ]
		const __m256d& mc1 = mat4[1]._vxd; // [ x1, y1, z1, w1 ]
		const __m256d& mc2 = mat4[2]._vxd; // [ x2, y2, z2, w2 ]
		const __m256d& mc3 = mat4[3]._vxd; // [ x3, y3, z3, w3 ]
		
		const __m128d x0y0 = _mm256_extractf128_pd(mc0, 0);
		const __m128d x1y1 = _mm256_extractf128_pd(mc1, 0);
		const __m128d x2y2 = _mm256_extractf128_pd(mc2, 0);
		const __m128d x3y3 = _mm256_extractf128_pd(mc3, 0);
		
		const __m128d z0w0 = _mm256_extractf128_pd(mc0, 1);
		const __m128d z1w1 = _mm256_extractf128_pd(mc1, 1);
		const __m128d z2w2 = _mm256_extractf128_pd(mc2, 1);
		const __m128d z3w3 = _mm256_extractf128_pd(mc3, 1);
		
		const __m256d c0 = _mm256_set_m128d(_mm_shuffle_pd(x2y2, x3y3, 0), _mm_shuffle_pd(x0y0, x1y1, 0));
		const __m256d c1 = _mm256_set_m128d(_mm_shuffle_pd(x2y2, x3y3, 3), _mm_shuffle_pd(x0y0, x1y1, 3));
		const __m256d c2 = _mm256_set_m128d(_mm_shuffle_pd(z2w2, z3w3, 0), _mm_shuffle_pd(z0w0, z1w1, 0));
		const __m256d c3 = _mm256_set_m128d(_mm_shuffle_pd(z2w2, z3w3, 3), _mm_shuffle_pd(z0w0, z1w1, 3));

		return Matrix4x4<double>(
			reinterpret_cast<const double*>(&c0),
			reinterpret_cast<const double*>(&c1),
			reinterpret_cast<const double*>(&c2),
			reinterpret_cast<const double*>(&c3));
	}


	template <>
	template <>
	ExsForceInline Matrix4x4<double> Matrix<Precision::High>::Rotation(double axisX, double axisY, double axisZ, double angle)
	{
		const __m256d axis = _mm256_set_pd(0, axisZ, axisY, axisX);
		
		const __m256d inv_norm = _mm256_sqrt_pd(_mm256_dp_pd4(axis, axis));
		const __m256d axis_norm = _mm256_div_pd(axis, inv_norm);

		const double angle_cos = cos(angle);
		const double angle_sin = sin(angle);

		const double one_minus_cos = 1.0 - angle_cos;
			
		const __m256d axmc = _mm256_mul_pd(axis_norm, _mm256_set1_pd(one_minus_cos));
		const __m256d axms = _mm256_mul_pd(axis_norm, _mm256_set1_pd(angle_sin));
		
		const double* axmc_f = reinterpret_cast<const double*>(&axmc);
		const double* axms_f = reinterpret_cast<const double*>(&axms);
		
		const __m256d tmp_0 = _mm256_mul_pd(axis_norm, _mm256_set1_pd(axmc_f[0]));
		const __m256d tmp_1 = _mm256_mul_pd(axis_norm, _mm256_set1_pd(axmc_f[1]));
		const __m256d tmp_2 = _mm256_mul_pd(axis_norm, _mm256_set1_pd(axmc_f[2]));

		const __m256d col_0 = _mm256_add_pd(tmp_0, _mm256_set_pd(0, -axms_f[1], axms_f[2], angle_cos));
		const __m256d col_1 = _mm256_add_pd(tmp_1, _mm256_set_pd(0, axms_f[0], angle_cos, -axms_f[2]));
		const __m256d col_2 = _mm256_add_pd(tmp_2, _mm256_set_pd(0, angle_cos, -axms_f[0], axms_f[1]));
		const __m256d col_3 = _mm256_set_pd(1, 0, 0, 0);

		return Matrix4x4<double>(
			reinterpret_cast<const double*>(&col_0),
			reinterpret_cast<const double*>(&col_1),
			reinterpret_cast<const double*>(&col_2),
			reinterpret_cast<const double*>(&col_3));
	}


#endif


}
