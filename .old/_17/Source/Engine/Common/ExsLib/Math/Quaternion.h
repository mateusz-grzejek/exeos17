
#ifndef __Exs_Math_Quaternion_H__
#define __Exs_Math_Quaternion_H__

#include "Vector3.h"
#include "Vector4.h"
#include "Matrix4.h"


namespace Exs
{


	template <typename T>
	struct Quaternion
	{
	};


	typedef Quaternion<ScalarPrecisionType<Precision::Default>::Type> Quat;


}


#endif /* __Exs_Math_Quaternion_H__ */
