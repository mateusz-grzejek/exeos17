
#ifndef __Exs_Math_Matrix3_H__
#define __Exs_Math_Matrix3_H__

#include "MatrixBase.h"
#include "VectorUtils.h"


namespace Exs
{


	template <typename T>
	struct Matrix3x3 : public MatrixBase<T, 3, 3>
	{
	protected:
		Vector3<T> _c0, _c1, _c2;

	public:
		Matrix3x3()
		: _c0(1, 0, 0)
		, _c1(0, 1, 0)
		, _c2(0, 0, 1)
		{ }

		Matrix3x3(const Matrix3x3<T>& origin)
		: _c0(origin._c0)
		, _c1(origin._c1)
		, _c2(origin._c2)
		{ }

		template <typename U>
		Matrix3x3(const Matrix3x3<U>& mat3)
		: _c0(mat3[0])
		, _c1(mat3[1])
		, _c2(mat3[2])
		{ }

		Matrix3x3(const Vector3<T>& c0, const Vector3<T>& c1, const Vector3<T>& c2)
		: _c0(c0)
		, _c1(c1)
		, _c2(c2)
		{ }

		template <typename C0, typename C1, typename C2>
		Matrix3x3(const Vector3<C0>& c0, const Vector3<C1>& c1, const Vector3<C2>& c2)
		: _c0(c0)
		, _c1(c1)
		, _c2(c2)
		{ }

		Matrix3x3(	T x0, T y0, T z0,
					T x1, T y1, T z1,
					T x2, T y2, T z2)
		: _c0(x0, y0, z0)
		, _c1(x1, y1, z1)
		, _c2(x2, y2, z2)
		{ }

		template <	typename Tx0, typename Ty0, typename Tz0,
					typename Tx1, typename Ty1, typename Tz1,
					typename Tx2, typename Ty2, typename Tz2>
		Matrix3x3(	Tx0 x0, Ty0 y0, Tz0 z0,
					Tx1 x1, Ty1 y1, Tz1 z1,
					Tx2 x2, Ty2 y2, Tz2 z2)
		: _c0(static_cast<T>(x0), y0, z0)
		, _c1(static_cast<T>(x1), y1, z1)
		, _c2(static_cast<T>(x2), y2, z2)
		{ }

		template <typename U>
		Matrix3x3(U scalar)
		: _c0(static_cast<T>(scalar), 0, 0)
		, _c1(0, static_cast<T>(scalar), 0)
		, _c2(0, 0, static_cast<T>(scalar))
		{ }

		Matrix3x3(const T* col0, const T* col1, const T* col2)
		: _c0(col0)
		, _c1(col1)
		, _c2(col2)
		{ }

		explicit Matrix3x3(const UninitializedTag&)
		{ }

		Matrix3x3<T>& operator=(const Matrix3x3<T>& rhs)
		{
			this->_c0 = rhs._c0;
			this->_c1 = rhs._c1;
			this->_c2 = rhs._c2;

			return *this;
		}

		template <typename U>
		Matrix3x3<T>& operator=(const Matrix3x3<U>& rhs)
		{
			this->_c0 = rhs[0];
			this->_c1 = rhs[1];
			this->_c2 = rhs[2];

			return *this;
		}

		T* Data()
		{
			return this->_c0.Data();
		}

		const T* Data() const
		{
			return this->_c0.Data();
		}

		Vector3<T>& operator[](Size_t index)
		{
			ExsDebugAssert( index < 4 );
			return (&(this->_c0))[index];
		}

		const Vector3<T>& operator[](Size_t index) const
		{
			ExsDebugAssert( index < 4 );
			return (&(this->_c0))[index];
		}

		template <typename U>
		Matrix3x3<T>& operator+=(const Matrix3x3<U>& rhs)
		{
			this->_c0 += rhs[0];
			this->_c1 += rhs[1];
			this->_c2 += rhs[2];

			return *this;
		}

		template <typename U>
		Matrix3x3<T>& operator+=(U rhs)
		{
			this->_c0 += rhs;
			this->_c1 += rhs;
			this->_c2 += rhs;

			return *this;
		}

		template <typename U>
		Matrix3x3<T>& operator-=(const Matrix3x3<U>& rhs)
		{
			this->_c0 -= rhs[0];
			this->_c1 -= rhs[1];
			this->_c2 -= rhs[2];

			return *this;
		}

		template <typename U>
		Matrix3x3<T>& operator-=(U rhs)
		{
			this->_c0 -= rhs;
			this->_c1 -= rhs;
			this->_c2 -= rhs;

			return *this;
		}

		template <typename U>
		Matrix3x3<T>& operator*=(const Matrix3x3<U>& rhs)
		{
			const Vector3<T>& my_c0 = this->_c0;
			const Vector3<T>& my_c1 = this->_c1;
			const Vector3<T>& my_c2 = this->_c2;
			
			const Vector3<T>& rc0 = rhs[0];
			const Vector3<T>& rc1 = rhs[1];
			const Vector3<T>& rc2 = rhs[2];

			this->_c0 = my_c0 * rc0.x + my_c1 * rc0.y + my_c2 * rc0.z;
			this->_c1 = my_c0 * rc1.x + my_c1 * rc1.y + my_c2 * rc1.z;
			this->_c2 = my_c0 * rc2.x + my_c1 * rc2.y + my_c2 * rc2.z;

			return *this;
		}

		template <typename U>
		Matrix3x3<T>& operator*=(U rhs)
		{
			this->_c0 *= rhs;
			this->_c1 *= rhs;
			this->_c2 *= rhs;

			return *this;
		}

		template <typename U>
		Matrix3x3<T>& operator/=(U rhs)
		{
			this->_c0 /= rhs;
			this->_c1 /= rhs;
			this->_c2 /= rhs;

			return *this;
		}
	};

	
	template <typename T>
	inline Matrix3x3<T> operator+(const Matrix3x3<T>& lhs, const Matrix3x3<T>& rhs)
	{
		return Matrix3x3<T>(
			lhs[0] + rhs[0],
			lhs[1] + rhs[1],
			lhs[2] + rhs[2]);
	}

	template <typename T>
	inline Matrix3x3<T> operator+(const Matrix3x3<T>& lhs, T rhs)
	{
		return Matrix3x3<T>(
			lhs[0] + rhs,
			lhs[1] + rhs,
			lhs[2] + rhs);
	}

	template <typename T>
	inline Matrix3x3<T> operator+(T lhs, const Matrix3x3<T>& rhs)
	{
		return Matrix3x3<T>(
			lhs + rhs[0],
			lhs + rhs[1],
			lhs + rhs[2]);
	}

	template <typename T>
	inline Matrix3x3<T> operator-(const Matrix3x3<T>& lhs, const Matrix3x3<T>& rhs)
	{
		return Matrix3x3<T>(
			lhs[0] - rhs[0],
			lhs[1] - rhs[1],
			lhs[2] - rhs[2]);
	}

	template <typename T>
	inline Matrix3x3<T> operator-(const Matrix3x3<T>& lhs, T rhs)
	{
		return Matrix3x3<T>(
			lhs[0] - rhs,
			lhs[1] - rhs,
			lhs[2] - rhs);
	}

	template <typename T>
	inline Matrix3x3<T> operator-(T lhs, const Matrix3x3<T>& rhs)
	{
		return Matrix3x3<T>(
			lhs - rhs[0],
			lhs - rhs[1],
			lhs - rhs[2]);
	}

	template <typename T>
	inline Matrix3x3<T> operator*(const Matrix3x3<T>& lhs, const Matrix3x3<T>& rhs)
	{
		const Vector3<T>& lc0 = lhs[0];
		const Vector3<T>& lc1 = lhs[1];
		const Vector3<T>& lc2 = lhs[2];

		const Vector3<T>& rc0 = rhs[0];
		const Vector3<T>& rc1 = rhs[1];
		const Vector3<T>& rc2 = rhs[2];

		return Matrix3x3<T>(
			lc0 * rc0.x + lc1 * rc0.y + lc2 * rc0.z,
			lc0 * rc1.x + lc1 * rc1.y + lc2 * rc1.z,
			lc0 * rc2.x + lc1 * rc2.y + lc2 * rc2.z);
	}

	template <typename T>
	inline Vector3<T> operator*(const Matrix3x3<T>& lhs, const Vector3<T>& rhs)
	{
		const Vector3<T>& c0 = lhs[0];
		const Vector3<T>& c1 = lhs[1];
		const Vector3<T>& c2 = lhs[2];

		return Vector3<T>(
			Vector<>::template Dot(Vector3<T>(c0[0], c1[0], c2[0]), rhs),
			Vector<>::template Dot(Vector3<T>(c0[1], c1[1], c2[1]), rhs),
			Vector<>::template Dot(Vector3<T>(c0[2], c1[2], c2[2]), rhs));
	}

	template <typename T>
	inline Vector3<T> operator*(const Vector3<T>& lhs, const Matrix3x3<T>& rhs)
	{
		return Vector3<T>(
			Vector<>::template Dot(lhs, rhs[0]),
			Vector<>::template Dot(lhs, rhs[1]),
			Vector<>::template Dot(lhs, rhs[2]));
	}

	template <typename T>
	inline Matrix3x3<T> operator*(const Matrix3x3<T>& lhs, T rhs)
	{
		return Matrix3x3<T>(
			lhs[0] * rhs,
			lhs[1] * rhs,
			lhs[2] * rhs);
	}

	template <typename T>
	inline Matrix3x3<T> operator*(T lhs, const Matrix3x3<T>& rhs)
	{
		return Matrix3x3<T>(
			lhs * rhs[0],
			lhs * rhs[1],
			lhs * rhs[2]);
	}

	template <typename T>
	inline Matrix3x3<T> operator/(const Matrix3x3<T>& lhs, T rhs)
	{
		return Matrix3x3<T>(
			lhs[0] / rhs,
			lhs[1] / rhs,
			lhs[2] / rhs);
	}

	template <typename T>
	inline Matrix3x3<T> operator/(T lhs, const Matrix3x3<T>& rhs)
	{
		return Matrix3x3<T>(
			lhs / rhs[0],
			lhs / rhs[1],
			lhs / rhs[2]);
	}

	template <typename T, typename U>
	inline Matrix3x3<T> operator-(const Matrix3x3<T>& rhs)
	{
		return Matrix3x3<T>(-rhs[0], -rhs[1], -rhs[2]);
	}

	
	template <typename T>
	inline bool operator==(const Matrix3x3<T>& lhs, const Matrix3x3<T>& rhs)
	{
		return (lhs[0] == rhs[0]) && (lhs[1] == rhs[1]) && (lhs[2] == rhs[2]);
	}

	template <typename T>
	inline bool operator!=(const Matrix3x3<T>& lhs, const Matrix3x3<T>& rhs)
	{
		return (lhs[0] != rhs[0]) || (lhs[1] != rhs[1]) || (lhs[2] != rhs[2]);
	}

	
	typedef Matrix3x3<float>     Matrix3F;
	typedef Matrix3x3<double>    Matrix3D;


}


#endif /* __Exs_Math_Matrix3_H__ */
