
#ifndef __Exs_Math_Color_H__
#define __Exs_Math_Color_H__

#include "MathBase.h"


namespace Exs
{


	enum class ColorU32 : Uint32
	{
		Black = EXS_MAKEU32B(0x00, 0x00, 0x00, 0xFF),
		White = EXS_MAKEU32B(0xFF, 0xFF, 0xFF, 0xFF),
	};


	template <typename Component_t>
	struct RGBData
	{
		Component_t  red;
		Component_t  green;
		Component_t  blue;
	};

	
	template <typename Component_t>
	struct RGBAData : public RGBData<Component_t>
	{
		Component_t alpha;
	};


	union Color
	{
	public:
		Uint32             value;
		RGBAData<Uint8>    rgba;

	public:
		Color()
		: value(0)
		{ }

		Color(const Color& origin)
		: value(origin.value)
		{ }

		Color(ColorU32 cu32)
		: value(static_cast<Uint32>(cu32))
		{ }

		explicit Color(Uint32 rgba)
		: value(rgba)
		{ }

		explicit Color(const Uint8 (&rgba)[4])
		{
			this->rgba.red = rgba[0];
			this->rgba.green = rgba[1];
			this->rgba.blue = rgba[2];
			this->rgba.alpha = rgba[3];
		}

		Color(Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha = 255)
		{
			this->rgba.red = red;
			this->rgba.green = green;
			this->rgba.blue = blue;
			this->rgba.alpha = alpha;
		}

		Color& operator=(const Color& rhs)
		{
			this->value = rhs.value;
			return *this;
		}

		Color& operator=(Uint32 rhs)
		{
			this->value = rhs;
			return *this;
		}

		void Swap(Color& other)
		{
			std::swap(this->value, other.value);
		}
	};
	

	inline bool operator==(const Color& lhs, const Color& rhs)
	{
		return lhs.value == rhs.value;
	}

	inline bool operator!=(const Color& lhs, const Color& rhs)
	{
		return lhs.value != rhs.value;
	}

	
	inline void swap(Color& left, Color& right)
	{
		left.Swap(right);
	}


}


#endif /* __Exs_Math_Color_H__ */
