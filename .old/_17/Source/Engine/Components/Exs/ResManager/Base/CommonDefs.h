
#ifndef __Exs_ResManager_CommonDefs_H__
#define __Exs_ResManager_CommonDefs_H__


namespace Exs
{

	
	typedef Enum ResourceType;


	enum : ResourceType
	{
		ResourceType_Image = 0x003401,

		ResourceType_Mesh = 0x003402
	};


	enum : TraceCategoryID
	{
		TRC_ResManager = 0x7400D0,
		TRC_ResManager_FontSystem = 0x7400D1
	};

	
	EXS_TRACE_SET_CATEGORY_NAME(TRC_ResManager, "Importer");
	EXS_TRACE_SET_CATEGORY_NAME(TRC_ResManager_FontSystem, "Importer:Fonts");


	template <ResourceType>
	struct ResourceTypeProperties;


	// Registers information about plugin type.
	// - pluginTypeTag: enum constant representing plugin type
	// - managerType: type (class) of manager handling plugins of registered type
	// - pluginType: type (class) of plugins
	#define EXS_ResManager_REGISTER_RESOURCE_TYPE(resourceTypeTag, loaderType) \
	class loaderType; \
	class loaderType##Object; \
	template <> struct ResourceTypeProperties<resourceTypeTag> \
	{ \
		typedef loaderType LoaderType; \
		typedef loaderType##Object LoaderObjectType; \
	};

	EXS_ResManager_REGISTER_RESOURCE_TYPE(ResourceType_Image, ImageLoader);
	EXS_ResManager_REGISTER_RESOURCE_TYPE(ResourceType_Mesh, MeshLoader);


}


#endif /* __Exs_ResManager_CommonDefs_H__ */
