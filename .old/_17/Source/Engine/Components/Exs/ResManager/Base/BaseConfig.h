
#ifndef __Exs_ResManager_BaseConfig_H__
#define __Exs_ResManager_BaseConfig_H__

#include <Exs/Core/Plugins/PluginBase.h>
#include <ExsRenderSystem/Prerequisites.h>
#include <ExsMath/Vector4.h>
#include <ExsMath/Matrix4.h>
#include <stdx/data_array.h>


#if ( EXS_BUILD_MODULE_RESMANAGER )
#  define EXS_LIBAPI_RESMANAGER       EXS_MODULE_EXPORT
#  define EXS_LIBCLASS_RESMANAGER     EXS_MODULE_EXPORT
#  define EXS_LIBOBJECT_RESMANAGER    extern EXS_MODULE_EXPORT
#else
#  define EXS_LIBAPI_RESMANAGER       EXS_MODULE_IMPORT
#  define EXS_LIBCLASS_RESMANAGER     EXS_MODULE_IMPORT
#  define EXS_LIBOBJECT_RESMANAGER    extern EXS_MODULE_IMPORT
#endif


#endif /* __Exs_ResManager_BaseConfig_H__ */
