
#ifndef __Exs_ResManager_FontBase_H__
#define __Exs_ResManager_FontBase_H__

#include "../Prerequisites.h"


namespace Exs
{


	class Font;


	namespace Config
	{
		enum : Uint32
		{
			FNT_Max_Font_Layers_Num = 64
		};
	}


	enum class FontType : Enum
	{
		Bitmap,

		Distance_Field,

		Free_Type
	};


	struct FontKerningInfo
	{
		CodePoint charLeft;

		CodePoint charRight;

		Int32 kerning;
	};


}


#endif /* __Exs_ResManager_FontBase_H__ */
