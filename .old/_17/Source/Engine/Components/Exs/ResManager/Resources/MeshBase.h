
#ifndef __Exs_ResManager_MeshBase_H__
#define __Exs_ResManager_MeshBase_H__

#include "../Prerequisites.h"


namespace Exs
{


	enum class VertexTexCoordsFormat : Size_t
	{
		Coords_1 = 1,
		Multi_Coords_2 = 2,
		Multi_Coords_3 = 3,
		Multi_Coords_4 = 4
	};


	template <Size_t Tex_coords_num>
	struct VertexTexCoords
	{
		typedef Vector2F Type[Tex_coords_num];
	};

	
	template <VertexTexCoordsFormat TCFormat = VertexTexCoordsFormat::Coords_1>
	struct Vertex3D
	{
		typedef Vertex3D<TCFormat> MyType;
		typedef typename VertexTexCoords<static_cast<Size_t>(TCFormat)>::Type TexCoordsType;

		Vector3F       position;
		Vector3F       normal;
		TexCoordsType  texCoords;
		Color          color;

		enum : Size_t
		{
			Offset_Position = 0,
			Offset_Normal = sizeof(Vector3F),
			Offset_Tex_Coords = Offset_Normal + sizeof(Vector3F),
			Offset_Color = Offset_Tex_Coords + sizeof(TexCoordsType),
			Stride = Offset_Color + sizeof(Color)
		};
	};


	typedef Vertex3D<> Vertex3DDefault;


}


#endif /* __Exs_ResManager_MeshBase_H__ */
