
#ifndef __Exs_ResManager_PixelFormatConverter_H__
#define __Exs_ResManager_PixelFormatConverter_H__

#include "../Resources/ImageBase.h"


namespace Exs
{

	
	enum class PixelConversionMode : Enum
	{
		Expand_Alpha,

		Expand_Gray_To_RGB,

		Expand_Gray_To_RGBA,

		Expand_Gray_With_Alpha_To_RGBA,

		Pass_Through_RGB,

		Pass_Through_RGBA,

		Shrink_RGB,

		Shrink_RGBA,

		Swap_Channels_RGB,

		Swap_Channels_RGBA,
	};
	

	template <ImagePixelFormat>
	struct ImagePixelFormatTraits;

	template <>
	struct ImagePixelFormatTraits<ImagePixelFormat::Alpha>
	{
		static const Size_t channelsNum = 1;
	};

	template <>
	struct ImagePixelFormatTraits<ImagePixelFormat::Color_BGR>
	{
		static const Size_t channelsNum = 3;
	};

	template <>
	struct ImagePixelFormatTraits<ImagePixelFormat::Color_BGRA>
	{
		static const Size_t channelsNum = 4;
	};

	template <>
	struct ImagePixelFormatTraits<ImagePixelFormat::Color_RGB>
	{
		static const Size_t channelsNum = 3;
	};

	template <>
	struct ImagePixelFormatTraits<ImagePixelFormat::Color_RGBA>
	{
		static const Size_t channelsNum = 4;
	};

	template <>
	struct ImagePixelFormatTraits<ImagePixelFormat::Gray>
	{
		static const Size_t channelsNum = 1;
	};

	template <>
	struct ImagePixelFormatTraits<ImagePixelFormat::Gray_Alpha>
	{
		static const Size_t channelsNum = 2;
	};
	

	template <ImagePixelFormat Input, ImagePixelFormat Output>
	struct GetPixelConversionMode;

	template <>
	struct GetPixelConversionMode<ImagePixelFormat::Alpha, ImagePixelFormat::Color_BGRA>
	{
		static const PixelConversionMode conversionMode = PixelConversionMode::Expand_Alpha;
	};

	template <>
	struct GetPixelConversionMode<ImagePixelFormat::Gray, ImagePixelFormat::Color_BGR>
	{
		static const PixelConversionMode conversionMode = PixelConversionMode::Expand_Gray_To_RGB;
	};

	template <>
	struct GetPixelConversionMode<ImagePixelFormat::Gray, ImagePixelFormat::Color_BGRA>
	{
		static const PixelConversionMode conversionMode = PixelConversionMode::Expand_Gray_To_RGBA;
	};

	template <>
	struct GetPixelConversionMode<ImagePixelFormat::Gray, ImagePixelFormat::Color_RGB>
	{
		static const PixelConversionMode conversionMode = PixelConversionMode::Expand_Gray_To_RGB;
	};

	template <>
	struct GetPixelConversionMode<ImagePixelFormat::Gray, ImagePixelFormat::Color_RGBA>
	{
		static const PixelConversionMode conversionMode = PixelConversionMode::Expand_Gray_To_RGBA;
	};

	template <>
	struct GetPixelConversionMode<ImagePixelFormat::Gray_Alpha, ImagePixelFormat::Color_BGRA>
	{
		static const PixelConversionMode conversionMode = PixelConversionMode::Expand_Gray_With_Alpha_To_RGBA;
	};

	template <>
	struct GetPixelConversionMode<ImagePixelFormat::Gray_Alpha, ImagePixelFormat::Color_RGBA>
	{
		static const PixelConversionMode conversionMode = PixelConversionMode::Expand_Gray_With_Alpha_To_RGBA;
	};

	template <>
	struct GetPixelConversionMode<ImagePixelFormat::Color_BGR, ImagePixelFormat::Color_BGR>
	{
		static const PixelConversionMode conversionMode = PixelConversionMode::Pass_Through_RGB;
	};

	template <>
	struct GetPixelConversionMode<ImagePixelFormat::Color_RGB, ImagePixelFormat::Color_RGB>
	{
		static const PixelConversionMode conversionMode = PixelConversionMode::Pass_Through_RGB;
	};

	template <>
	struct GetPixelConversionMode<ImagePixelFormat::Color_BGRA, ImagePixelFormat::Color_BGRA>
	{
		static const PixelConversionMode conversionMode = PixelConversionMode::Pass_Through_RGBA;
	};

	template <>
	struct GetPixelConversionMode<ImagePixelFormat::Color_RGBA, ImagePixelFormat::Color_RGBA>
	{
		static const PixelConversionMode conversionMode = PixelConversionMode::Pass_Through_RGBA;
	};

	template <>
	struct GetPixelConversionMode<ImagePixelFormat::Color_BGR, ImagePixelFormat::Color_RGB>
	{
		static const PixelConversionMode conversionMode = PixelConversionMode::Swap_Channels_RGB;
	};

	template <>
	struct GetPixelConversionMode<ImagePixelFormat::Color_RGB, ImagePixelFormat::Color_BGR>
	{
		static const PixelConversionMode conversionMode = PixelConversionMode::Swap_Channels_RGB;
	};

	template <>
	struct GetPixelConversionMode<ImagePixelFormat::Color_BGRA, ImagePixelFormat::Color_RGBA>
	{
		static const PixelConversionMode conversionMode = PixelConversionMode::Swap_Channels_RGBA;
	};

	template <>
	struct GetPixelConversionMode<ImagePixelFormat::Color_RGBA, ImagePixelFormat::Color_BGRA>
	{
		static const PixelConversionMode conversionMode = PixelConversionMode::Swap_Channels_RGBA;
	};

	template <>
	struct GetPixelConversionMode<ImagePixelFormat::Color_BGR, ImagePixelFormat::Gray>
	{
		static const PixelConversionMode conversionMode = PixelConversionMode::Shrink_RGB;
	};

	template <>
	struct GetPixelConversionMode<ImagePixelFormat::Color_BGRA, ImagePixelFormat::Gray>
	{
		static const PixelConversionMode conversionMode = PixelConversionMode::Shrink_RGBA;
	};

	template <>
	struct GetPixelConversionMode<ImagePixelFormat::Color_RGB, ImagePixelFormat::Gray>
	{
		static const PixelConversionMode conversionMode = PixelConversionMode::Shrink_RGB;
	};

	template <>
	struct GetPixelConversionMode<ImagePixelFormat::Color_RGBA, ImagePixelFormat::Gray>
	{
		static const PixelConversionMode conversionMode = PixelConversionMode::Shrink_RGBA;
	};
	

	template <typename In, typename Out>
	struct PixelTypeConv
	{
		Out operator()(In value)
		{
			return (value * stdx::limits<Out>::max_value) / stdx::limits<In>::max_value;
		}
	};

	template <typename T>
	struct PixelTypeConv<T, T>
	{
		T operator()(T value)
		{
			return value;
		}
	};

	template <typename In>
	struct PixelTypeConv<In, float>
	{
		float operator()(In value)
		{
			return (static_cast<float>(value) / static_cast<float>(stdx::limits<In>::max_value));
		}
	};

	template <typename Out>
	struct PixelTypeConv<float, Out>
	{
		Out operator()(float value)
		{
			return static_cast<Out>(value * static_cast<float>(stdx::limits<Out>::max_value));
		}
	};


	template <PixelConversionMode, typename In, typename Out>
	struct PixelFormatConv;

	template <typename In, typename Out>
	struct PixelFormatConv<PixelConversionMode::Expand_Alpha, In, Out>
	{
		void operator()(const In* input, Out* output) const
		{
			output[0] = output[1] = output[2] = stdx::limits<Out>::max_value;
			output[3] = PixelTypeConv<In, Out>()(input[0]);
		}
	};

	template <typename In, typename Out>
	struct PixelFormatConv<PixelConversionMode::Expand_Gray_To_RGB, In, Out>
	{
		void operator()(const In* input, Out* output) const
		{
			output[0] = output[1] = output[2] = PixelTypeConv<In, Out>()(input[0]);
		}
	};

	template <typename In, typename Out>
	struct PixelFormatConv<PixelConversionMode::Expand_Gray_To_RGBA, In, Out>
	{
		void operator()(const In* input, Out* output) const
		{
			output[0] = output[1] = output[2] = PixelTypeConv<In, Out>()(input[0]);
			output[3] = PixelTypeConv<float, Out>()((input[0] > static_cast<In>(0)) ? 1.0f : 0.0f);
		}
	};

	template <typename In, typename Out>
	struct PixelFormatConv<PixelConversionMode::Expand_Gray_With_Alpha_To_RGBA, In, Out>
	{
		void operator()(const In* input, Out* output) const
		{
			output[0] = output[1] = output[2] = PixelTypeConv<In, Out>()(input[0]);
			output[3] = PixelTypeConv<In, Out>()(input[1]);
		}
	};
	
	template <typename In, typename Out>
	struct PixelFormatConv<PixelConversionMode::Pass_Through_RGB, In, Out>
	{
		void operator()(const In* input, Out* output) const
		{
			output[0] = PixelTypeConv<In, Out>()(input[0]);
			output[1] = PixelTypeConv<In, Out>()(input[1]);
			output[2] = PixelTypeConv<In, Out>()(input[2]);
		}
	};
	
	template <typename In, typename Out>
	struct PixelFormatConv<PixelConversionMode::Pass_Through_RGBA, In, Out>
	{
		void operator()(const In* input, Out* output) const
		{
			output[0] = PixelTypeConv<In, Out>()(input[0]);
			output[1] = PixelTypeConv<In, Out>()(input[1]);
			output[2] = PixelTypeConv<In, Out>()(input[2]);
			output[3] = PixelTypeConv<In, Out>()(input[3]);
		}
	};
	
	template <typename In, typename Out>
	struct PixelFormatConv<PixelConversionMode::Swap_Channels_RGB, In, Out>
	{
		void operator()(const In* input, Out* output) const
		{
			output[0] = PixelTypeConv<In, Out>()(input[2]);
			output[1] = PixelTypeConv<In, Out>()(input[1]);
			output[2] = PixelTypeConv<In, Out>()(input[0]);
		}
	};

	template <typename In, typename Out>
	struct PixelFormatConv<PixelConversionMode::Swap_Channels_RGBA, In, Out>
	{
		void operator()(const In* input, Out* output) const
		{
			output[0] = PixelTypeConv<In, Out>()(input[2]);
			output[1] = PixelTypeConv<In, Out>()(input[1]);
			output[2] = PixelTypeConv<In, Out>()(input[0]);
			output[3] = PixelTypeConv<In, Out>()(input[3]);
		}
	};

	template <typename In, typename Out>
	struct PixelFormatConv<PixelConversionMode::Shrink_RGB, In, Out>
	{
		void operator()(const In* input, Out* output) const
		{
			output[0] = PixelTypeConv<In, Out>()((input[0] + input[1] + input[2]) / 3);
		}
	};

	template <typename In, typename Out>
	struct PixelFormatConv<PixelConversionMode::Shrink_RGBA, In, Out>
	{
		void operator()(const In* input, Out* output) const
		{
			output[0] = PixelTypeConv<In, Out>()(input[3]);
		}
	};


	template <ImagePixelFormat Input, ImagePixelFormat Output>
	class PixelFormatConverter
	{
	public:
		template <typename In, typename Out>
		static void Convert(const In* input, Size_t inputSizeInBytes, Out* output)
		{
			const Size_t inputPixelChannelsNum = ImagePixelFormatTraits<Input>::channelsNum;

			const Size_t outputPixelChannelsNum = ImagePixelFormatTraits<Output>::channelsNum;

			const auto conversionMode = GetPixelConversionMode<Input, Output>::conversionMode;

			// Type of functor used to convert single pixel data.
			typedef PixelFormatConv<conversionMode, In, Out> FormatConv;

			// Data is specified in bytes. Compute number of pixels to convert.
			const Size_t inputPixelsNum = inputSizeInBytes / (sizeof(In) * inputPixelChannelsNum);

			for (Size_t p = 0; p < inputPixelsNum; ++p)
			{
				FormatConv()(input, output);
				input += inputPixelChannelsNum;
				output += outputPixelChannelsNum;
			}
		}
	};


	template <>
	class PixelFormatConverter<ImagePixelFormat::Mono, ImagePixelFormat::Color_RGB>
	{
	public:
		static void Convert(const Byte* input, Size_t inputSizeInBytes, Uint32 bitsPerByte, Byte* output)
		{
			ExsDebugAssert( (bitsPerByte > 0) && (bitsPerByte <= 8) );

			for (Size_t p = 0; p < inputSizeInBytes; ++p)
			{
				Byte inputValue = *input;

				for (Uint32 b = 0; b < bitsPerByte; ++b)
				{
					Byte gray = ((inputValue >> (7 - b)) & 0x1) * 255;
					PixelFormatConv<PixelConversionMode::Expand_Gray_To_RGB, Byte, Byte>()(&gray, output);
					output += 4;
				}
				
				++input;
			}
		}
	};


	template <>
	class PixelFormatConverter<ImagePixelFormat::Mono, ImagePixelFormat::Color_RGBA>
	{
	public:
		static void Convert(const Byte* input, Size_t inputSizeInBytes, Uint32 bitsPerByte, Byte* output)
		{
			ExsDebugAssert( (bitsPerByte > 0) && (bitsPerByte <= 8) );

			for (Size_t p = 0; p < inputSizeInBytes; ++p)
			{
				Byte inputValue = *input;

				for (Uint32 b = 0; b < bitsPerByte; ++b)
				{
					Byte gray = ((inputValue >> (7 - b)) & 0x1) * 255;
					PixelFormatConv<PixelConversionMode::Expand_Gray_To_RGBA, Byte, Byte>()(&gray, output);
					output += 4;
				}
				
				++input;
			}
		}
	};


	template <>
	class PixelFormatConverter<ImagePixelFormat::Mono, ImagePixelFormat::Gray>
	{
	public:
		static void Convert(const Byte* input, Size_t inputSizeInBytes, Uint32 bitsPerByte, Byte* output)
		{
			ExsDebugAssert( (bitsPerByte > 0) && (bitsPerByte <= 8) );

			for (Size_t p = 0; p < inputSizeInBytes; ++p)
			{
				Byte inputValue = *input;

				for (Uint32 b = 0; b < bitsPerByte; ++b)
				{
					*output = ((inputValue >> (7 - b)) & 0x1) * 255;
					++output;
				}
				
				++input;
			}
		}
	};


	template <>
	class PixelFormatConverter<ImagePixelFormat::Mono, ImagePixelFormat::Color_BGR>
	{
	public:
		static void Convert(const Byte* input, Size_t inputSizeInBytes, Uint32 bitsPerByte, Byte* output)
		{
			PixelFormatConverter<ImagePixelFormat::Mono, ImagePixelFormat::Color_RGB>::Convert(input, inputSizeInBytes, bitsPerByte, output);
		}
	};


	template <>
	class PixelFormatConverter<ImagePixelFormat::Mono, ImagePixelFormat::Color_BGRA>
	{
	public:
		static void Convert(const Byte* input, Size_t inputSizeInBytes, Uint32 bitsPerByte, Byte* output)
		{
			PixelFormatConverter<ImagePixelFormat::Mono, ImagePixelFormat::Color_RGBA>::Convert(input, inputSizeInBytes, bitsPerByte, output);
		}
	};
}


#endif /* __Exs_ResManager_PixelFormatConverter_H__ */
