
#ifndef __Exs_ResManager_Font_H__
#define __Exs_ResManager_Font_H__

#include "FontGlyph.h"
#include <unordered_map>
#include <stdx/sorted_array.h>

#undef CreateFont


namespace Exs
{

	
	class Font;
	class FontLayer;
	class RenderSystem;

	ExsDeclareRSClassObjHandle(ShaderResourceView);
	ExsDeclareRSClassObjHandle(Texture2DArray);

	
	///<summary>
	///</summary>
	class EXS_LIBCLASS_RESMANAGER FontLayer
	{
		friend class Font;

	public:
		typedef std::vector<CodePoint> GlyphArray;

	protected:
		Font*       _font;
		Uint32      _layerIndex;
		Vector2U32  _dimensions;
		GlyphArray  _glyphs;

	public:
		FontLayer(Font* font, Size_t layerIndex, const Vector2U32& dimensions);
		
		///<summary>
		///</summary>
		void Reset();
		
		///<summary>
		///</summary>
		const Vector2U32& GetDimensions() const;
		
		///<summary>
		///</summary>
		const GlyphArray& GetGlyphs() const;
		
		///<summary>
		///</summary>
		Uint32 GetLayerIndex() const;

	protected:
		// Called when layer is being reset.
		virtual void OnLayerReset();

	friendapi:
		// Adds glyph to this layer.
		void AddGlyph(CodePoint cp);
	};
	

	inline const Vector2U32& FontLayer::GetDimensions() const
	{
		return this->_dimensions;
	}

	inline const FontLayer::GlyphArray& FontLayer::GetGlyphs() const
	{
		return this->_glyphs;
	}
	
	inline Uint32 FontLayer::GetLayerIndex() const
	{
		return this->_layerIndex;
	}

	inline void FontLayer::AddGlyph(CodePoint cp)
	{
		this->_glyphs.push_back(cp);
	}

	
	///<summary>
	///</summary>
	class EXS_LIBCLASS_RESMANAGER Font
	{
		friend class FontLayer;

		EXS_DECLARE_NONCOPYABLE(Font);

	protected:
		struct GlyphData
		{
			CodePoint codePoint;

			Uint32 layerIndex;

			FontGlyph glyph;
		};

		struct FontKerningInfoCmp
		{
			bool operator()(const FontKerningInfo& lhs, const FontKerningInfo& rhs) const
			{
				return (lhs.charLeft < rhs.charLeft) || ((lhs.charLeft == rhs.charLeft) && (lhs.charRight < rhs.charRight));
			}
			
			bool operator()(const FontKerningInfo& lhs, const std::pair<CodePoint, CodePoint>& rhs) const
			{
				return (lhs.charLeft < rhs.first) || ((lhs.charLeft == rhs.first) && (lhs.charRight < rhs.second));
			}
		};

		struct FontKerningInfoEq
		{
			bool operator()(const FontKerningInfo& lhs, const FontKerningInfo& rhs) const
			{
				return (lhs.charLeft == rhs.charLeft) && (lhs.charRight == rhs.charRight);
			}
			
			bool operator()(const FontKerningInfo& lhs, const std::pair<CodePoint, CodePoint>& rhs) const
			{
				return (lhs.charLeft == rhs.first) && (lhs.charRight == rhs.second);
			}
		};

		typedef std::vector< std::unique_ptr<FontLayer> > LayerArray;
		typedef std::unordered_map<CodePoint, GlyphData> GlyphArray;
		typedef stdx::sorted_array<FontKerningInfo, FontKerningInfoCmp> KerningArray;

	protected:
		FontType      _fontType; // Type of this font.
		Uint32        _fontSize; // Font size, in 72dpi points.
		Uint32        _fontLineHeight;
		std::string   _name;
		Size_t        _maxLayersNum;
		GlyphArray    _glyphs;
		KerningArray  _kernings;
		LayerArray    _layers;

	public:
		Font(FontType type, Uint32 fontSize, Uint32 fontLineHeight, const std::string& name, Size_t maxLayersNum);

		virtual ~Font();
		
		///<summary>
		/// Returns glyph for specified code point. Returns NULL if an error is encountered
		/// or glyph could not be found in the glyph cache.
		///</summary>
		const FontGlyph* GetGlyph(CodePoint codePoint) const;
		
		///<summary>
		/// Returns glyph for specified code point. Returns NULL if an error is encountered.
		/// If glyph could not be found in the glyph cache, font may attempt to load the reuqested
		/// glyph on-the-fly (this applies only to dynamic fonts). If glyph could not be loaded
		/// whatsoever, NULL is returned.
		///</summary>
		const FontGlyph* GetGlyph(CodePoint codePoint);
		
		///<summary>
		/// Returns glyph metrics for specified code point. Returns NULL if an error is encountered
		/// or glyph could not be found in the glyph cache.
		///</summary>
		const FontGlyphMetrics* GetGlyphMetrics(CodePoint codePoint) const;
		
		///<summary>
		/// Returns glyph metrics for specified code point. Returns NULL if an error is encountered.
		/// If glyph could not be found in the glyph cache, font may attempt to load the reuqested
		/// glyph on-the-fly (this applies only to dynamic fonts). If glyph could not be loaded
		/// whatsoever, NULL is returned.
		///</summary>
		const FontGlyphMetrics* GetGlyphMetrics(CodePoint codePoint);
		
		///<summary>
		/// Returns kerning value for specified pair of code points.
		///</summary>
		Int32 GetKerning(CodePoint left, CodePoint right) const;
		
		///<summary>
		/// Returns kerning value for specified pair of code points. If kerning could not be found
		/// in the kerning cache, font may attempt to load the reuqested kerning on-the-fly (applies
		/// only to dynamic fonts).
		///</summary>
		Int32 GetKerning(CodePoint left, CodePoint right);

		Uint32 GetLineHeight() const;

	protected:
		// Adds glyph to the specified layer. Returns pointer to the inserted glyph.
		FontGlyph* AddGlyph(CodePoint codePoint, Uint32 layerIndex, FontLayer* layerPtr);
		
		// Adds new layer of specified type.
		template <class Layer_t, class... Args>
		Layer_t* AddLayer(Args&&... args);

	friendapi:
		//
		void ResetLayer(FontLayer* layer);

	private:
		//
		virtual const FontGlyph* _LoadGlyph(CodePoint codePoint);

		//
		virtual const FontGlyphMetrics* _LoadGlyphMetrics(CodePoint codePoint);

		//
		virtual Int32 _LoadKerning(CodePoint left, CodePoint right);

		//
		void _EraseGlyph(CodePoint codePoint);

		//
		const FontGlyph* _FindGlyph(CodePoint codePoint) const;

		//
		const FontGlyphMetrics* _FindGlyphMetrics(CodePoint codePoint) const;

		//
		const Int32* _FindKerning(CodePoint left, CodePoint right) const;
	};


	inline const FontGlyph* Font::GetGlyph(CodePoint codePoint) const
	{
		return this->_FindGlyph(codePoint);
	}

	inline const FontGlyph* Font::GetGlyph(CodePoint codePoint)
	{
		if (const auto* glyph = this->_FindGlyph(codePoint))
			return glyph;

		return this->_LoadGlyph(codePoint);
	}

	inline const FontGlyphMetrics* Font::GetGlyphMetrics(CodePoint codePoint) const
	{
		return this->_FindGlyphMetrics(codePoint);
	}

	inline const FontGlyphMetrics* Font::GetGlyphMetrics(CodePoint codePoint)
	{
		if (const auto* glyphMetrics = this->_FindGlyphMetrics(codePoint))
			return glyphMetrics;

		return this->_LoadGlyphMetrics(codePoint);
	}

	inline Int32 Font::GetKerning(CodePoint left, CodePoint right) const
	{
		const auto* kerningPtr = this->_FindKerning(left, right);
		return (kerningPtr != nullptr) ? *kerningPtr : 0;
	}

	inline Int32 Font::GetKerning(CodePoint left, CodePoint right)
	{
		if (const auto* kerning = this->_FindKerning(left, right))
			return *kerning;
		
		return this->_LoadKerning(left, right);
	}

	inline Uint32 Font::GetLineHeight() const
	{
		return this->_fontLineHeight;
	}

	template <class Layer_t, class... Args>
	inline Layer_t* Font::AddLayer(Args&&... args)
	{
		Size_t currentLayersNum = this->_layers.size();
		ExsDebugAssert( currentLayersNum < this->_maxLayersNum );

		auto layer = std::make_unique<Layer_t>(this, currentLayersNum, std::forward<Args>(args)...);
		Layer_t* layerPtr = layer.get();
		this->_layers.push_back(std::move(layer));

		return layerPtr;
	}

	inline const FontGlyph* Font::_FindGlyph(CodePoint codePoint) const
	{
		auto glyphRef = this->_glyphs.find(codePoint);
		return (glyphRef != this->_glyphs.end()) ? &(glyphRef->second.glyph) : nullptr;
	}
	
	inline const FontGlyphMetrics* Font::_FindGlyphMetrics(CodePoint codePoint) const
	{
		auto glyphRef = this->_glyphs.find(codePoint);
		return (glyphRef != this->_glyphs.end()) ? &(glyphRef->second.glyph.metrics) : nullptr;
	}
	
	inline const Int32* Font::_FindKerning(CodePoint left, CodePoint right) const
	{
		auto kerningRef = this->_kernings.find(std::pair<CodePoint, CodePoint>(left, right), FontKerningInfoEq());
		return (kerningRef != this->_kernings.end()) ? &(kerningRef->kerning) : nullptr;
	}


}


#endif /* __Exs_ResManager_Font_H__ */
