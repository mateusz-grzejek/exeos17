
#ifndef __Exs_ResManager_ImageBase_H__
#define __Exs_ResManager_ImageBase_H__

#include "../Prerequisites.h"


namespace Exs
{


	enum class ImagePixelFormat : Enum
	{
		// Alpha channel.
		Alpha,

		// 
		Color_BGR,
		
		// 
		Color_BGRA,
		
		// 
		Color_RGB,
		
		// 
		Color_RGBA,

		// Gray color.
		Gray,

		// Gray color with alpha channel.
		Gray_Alpha,

		//
		Mono
	};


	struct ImageDataInfo
	{
		Uint32 width;
		Uint32 height;
		Uint32 rowPitch;
		Uint32 sizeInBytes;
		Uint16 channelSize;
		Uint16 channelsNum;
		Uint16 pixelSize;
		ImagePixelFormat pixelFormat;
	};


}


#endif /* __Exs_ResManager_ImageBase_H__ */
