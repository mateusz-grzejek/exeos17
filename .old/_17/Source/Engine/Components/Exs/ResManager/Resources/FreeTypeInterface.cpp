
#include <Exs/Core/File.h>
#include <ExsResManager/Resources/FreeTypeInterface.h>


namespace Exs
{


	FreeTypeLoader::FreeTypeLoader(FT_Library ftLibrary, FT_Face ftFace, FTFaceData&& faceData, Uint32 fontSize)
	: _ftLibrary(ftLibrary)
	, _ftFace(ftFace)
	, _ftFaceData(std::move(faceData))
	, _fontSize(fontSize)
	, _lineHeight(ftFace->height)
	{ }


	FreeTypeLoader::~FreeTypeLoader()
	{
		for (auto& g : this->_glyphCache)
		{
			FT_Done_Glyph(g.second.glyph);
			this->_glyphCache.clear();
		}

		if (this->_ftFace != nullptr)
		{
			FT_Done_Face(this->_ftFace);
			this->_ftFace = nullptr;
		}
		
		if (this->_ftLibrary != nullptr)
		{
			FT_Done_FreeType(this->_ftLibrary);
			this->_ftLibrary = nullptr;
		}
	}

	
	const FTGlyphData* FreeTypeLoader::LoadGlyph(CodePoint codePoint)
	{
		if (this->_ftFace == nullptr)
			return nullptr;

		// Fetch glyph info object. If NULL, the glyph for specified code point could not be loaded.
		auto* glyphInfo = this->_GetGlyphInfo(codePoint);

		if (glyphInfo == nullptr)
			return nullptr;

		if (glyphInfo->data.image == nullptr)
		{
			if (glyphInfo->glyph->format != FT_GLYPH_FORMAT_BITMAP)
			{
				if (FT_Glyph_To_Bitmap(&(glyphInfo->glyph), FT_RENDER_MODE_NORMAL, nullptr, 1) != FT_Err_Ok)
				{
					//?
					return nullptr;
				}
			}

			glyphInfo->data.image = reinterpret_cast<FT_BitmapGlyph>(glyphInfo->glyph);
		}

		return &(glyphInfo->data);
	}

	
	const FT_Glyph_Metrics* FreeTypeLoader::LoadGlyphMetrics(CodePoint codePoint)
	{
		if (this->_ftFace == nullptr)
			return nullptr;
		
		// Fetch glyph info object. If NULL, the glyph for specified code point could not be loaded.
		auto* glyphInfo = this->_GetGlyphInfo(codePoint);

		if (glyphInfo == nullptr)
			return nullptr;

		return &(glyphInfo->data.metrics);
	}


	Int32 FreeTypeLoader::LoadKerning(CodePoint left, CodePoint right)
	{
		FT_UInt leftIndex = FT_Get_Char_Index(this->_ftFace, left);
		FT_UInt rightIndex = FT_Get_Char_Index(this->_ftFace, right);

		FT_Vector kerningVector;
		kerningVector.x = 0;
		kerningVector.y = 0;

		if (FT_Get_Kerning(this->_ftFace, leftIndex, rightIndex, FT_KERNING_DEFAULT, &kerningVector) != FT_Err_Ok)
			return 0;

		return kerningVector.x;
	}


	FreeTypeLoader::GlyphInfo* FreeTypeLoader::_GetGlyphInfo(CodePoint codePoint)
	{
		// Fetch glyph data from the cache - it may be already loaded.
		auto glyphInfoRef = this->_glyphCache.find(codePoint);

		if (glyphInfoRef != this->_glyphCache.end())
		{
			// If glyph info for given codePoint is in the cache, it means, that this codePoint
			// have been loaded. If glyph is not NULL, return it. If glyph is empty, an error has
			// occured before, so do not try to load it again.

			return (glyphInfoRef->second.glyph != nullptr) ? &(glyphInfoRef->second) : nullptr;
		}

		// Insert new info object to the cache to indicate, that glyph was already fetched.
		glyphInfoRef = this->_glyphCache.emplace().first;

		// Try to actually load glyph data from FreeType.
		if (!this->_InternalLoad(codePoint, &(glyphInfoRef->second)))
			return nullptr;

		return &(glyphInfoRef->second);
	}


	bool FreeTypeLoader::_InternalLoad(CodePoint codePoint, GlyphInfo* glyphInfo)
	{
		// Translate code point to index within currently loaded face.
		FT_UInt glyphIndex = FT_Get_Char_Index(this->_ftFace, codePoint);

		if (glyphIndex == 0)
			return false;

		if (FT_Load_Glyph(this->_ftFace, glyphIndex, FT_LOAD_DEFAULT) != FT_Err_Ok)
			return false;

		if (FT_Get_Glyph(this->_ftFace->glyph, &(glyphInfo->glyph)) != FT_Err_Ok)
			return false;

		glyphInfo->data.image = nullptr;
		glyphInfo->data.metrics = this->_ftFace->glyph->metrics;

		return true;
	}




	std::unique_ptr<FreeTypeLoader> CreateFreeTypeLoader(FTFaceData&& faceData, Uint32 fontSize, Uint32 faceIndex)
	{
		FT_Library ftLibrary = nullptr;
		FT_Face ftFace = nullptr;

		if (FT_Init_FreeType(&ftLibrary) != FT_Err_Ok)
		{
			//?
			return nullptr;
		}

		const FT_Byte* faceDataPtr = reinterpret_cast<const FT_Byte*>(faceData.data_ptr());
		FT_Long faceDataSize = truncate_cast<FT_Long>(faceData.size_in_bytes());

		if (FT_New_Memory_Face(ftLibrary, faceDataPtr, faceDataSize, faceIndex, &ftFace) != FT_Err_Ok)
		{
			//?
			return nullptr;
		}
		
		if (FT_Set_Char_Size(ftFace, 0, fontSize * 64, 0, 0) != FT_Err_Ok)
		{
			//?
			return nullptr;
		}
		
		auto freeTypeLoader = std::make_unique<FreeTypeLoader>(ftLibrary, ftFace, std::move(faceData), fontSize);
		return freeTypeLoader;
	}


}
