
#include <ExsResManager/Importers/ImageLoader.h>
#include <ExsResManager/Importers/ResourceLoaderPlugin.h>


namespace Exs
{


	ImageLoader::ImageLoader(ResourceLoaderPlugin* plugin, const char* name, const ImageLoaderObjectCreateProc& loaderObjectCreateProc)
	: ResourceLoader(ResourceType_Image, name)
	, _plugin(plugin)
	, _loaderObjectCreateProc(loaderObjectCreateProc)
	{ }


	ImageLoader::~ImageLoader()
	{ }


	ImageLoaderObjectRefPtr ImageLoader::CreateLoaderObject()
	{
		const auto& loaderPluginHandle = this->_plugin->GetHandle();
		auto imageLoaderObject = this->_loaderObjectCreateProc(this, loaderPluginHandle);
		return imageLoaderObject;
	}


	void ImageLoader::OnLoaderObjectCreate(ImageLoader* loader)
	{
		this->_activeLoaderObjectsNum.fetch_add(1, std::memory_order_release);
	}


	void ImageLoader::OnLoaderObjectDestroy(ImageLoader* loader)
	{
		this->_activeLoaderObjectsNum.fetch_sub(1, std::memory_order_release);
	}




	ImageLoaderObject::ImageLoaderObject(ImageLoader* loader, const PluginHandle& pluginHandle)
	: ResourceLoaderObject(pluginHandle)
	, _loader(loader)
	{ }


	ImageLoaderObject::~ImageLoaderObject()
	{ }


}
