
#include <ExsResManager/Importers/ResourceLoader.h>
#include <stdx/string_utils.h>


namespace Exs
{


	ResourceLoader::ResourceLoader(ResourceType resourceType, const char* name)
	: _resourceType(resourceType)
	, _name(name)
	{ }
	

	ResourceLoader::~ResourceLoader()
	{ }
	

	void ResourceLoader::RegisterFileFormat(const ResourceFileFormatSpecification& formatSpecification)
	{
		ResourceFileFormatInfo fileFormatInfo;
		fileFormatInfo.resourceType = this->_resourceType;
		fileFormatInfo.name = formatSpecification.name;
		fileFormatInfo.mimeType = formatSpecification.mimeType;

		stdx::split_string(
			formatSpecification.supportedExtensions, ',',
			[&fileFormatInfo](const char* str, Size_t length) -> void {
				fileFormatInfo.extensions.push_back(std::string(str, length));
			});

		this->_fileFormatList.push_back(std::move(fileFormatInfo));
	}


}
