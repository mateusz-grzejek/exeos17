
#include <ExsRenderSystem/GraphicsDriver.h>
#include <ExsRenderSystem/RenderSystem.h>


namespace Exs
{


	GraphicsDriver::GraphicsDriver(const GraphicsDriverCapabilitiesDesc& capabilitiesDesc)
	: _capabilities(capabilitiesDesc)
	, _mainRSThreadState(nullptr)
	{
		ExsTraceInfo(TRC_RenderSystem, "GraphicsDriver: Init (%p).", this);
	}


	GraphicsDriver::~GraphicsDriver()
	{
		ExsTraceInfo(TRC_RenderSystem, "GraphicsDriver: Release.");
	}


	bool GraphicsDriver::AcquireMainThreadState()
	{
		ExsDebugAssert( this->_renderSystem );
		ExsDebugAssert( this->_mainRSThreadState == nullptr );

		if (RSThreadState* mainRSThreadState = this->_renderSystem->AcquireMainThreadState(nullptr))
		{
			if (mainRSThreadState->Attach())
			{
				this->_mainRSThreadState = mainRSThreadState;
				return true;
			}
		}

		return false;
	}


	void GraphicsDriver::ReleaseMainThreadState()
	{
		ExsDebugAssert( this->_mainRSThreadState != nullptr );

		this->_mainRSThreadState->Detach();
		this->_renderSystem->ReleaseMainThreadState(this->_mainRSThreadState);
		this->_mainRSThreadState = nullptr;
	}


	void GraphicsDriver::ReleaseActiveObjects()
	{
		ExsDebugAssert( this->_mainRSThreadState != nullptr );

		this->_mainRSThreadState->Reset();
		this->_renderSystem->ReleaseObjects();
	}


}
