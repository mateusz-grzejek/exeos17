
#ifndef __Exs_RenderSystem_RSSwapChain_H__
#define __Exs_RenderSystem_RSSwapChain_H__

#include "Prerequisites.h"


namespace Exs
{


	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM RSSwapChain
	{
	public:
		RSSwapChain();
		virtual ~RSSwapChain();

		///<summary>
		///</summary>
		virtual Result Present(Enum flags) = 0;
	};


}


#endif /* __Exs_RenderSystem_RSSwapChain_H__ */
