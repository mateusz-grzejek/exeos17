
#include <ExsRenderSystem/Resource/Texture.h>
#include <ExsRenderSystem/Resource/TextureStorage.h>


namespace Exs
{


	Texture::Texture(RenderSystem* renderSystem, RSBaseObjectID baseObjectID, TextureType type, GraphicDataFormat format)
	: RSResource(renderSystem, baseObjectID, 0, RSResourceType::Texture, 0)
	, _textureType(type)
	, _texFormat(format)
	{ }


	Texture::~Texture()
	{ }


	void Texture::OnBindStorage(RSResourceStorage* storage)
	{
		RSResource::OnBindStorage(storage);
		this->_textureStorage = dbgsafe_ptr_cast<TextureStorage*>(storage);
		this->_dimensions = this->_textureStorage->GetTextureDimensions();
	}



	
	Texture2D::Texture2D(RenderSystem* renderSystem, RSBaseObjectID baseObjectID, GraphicDataFormat format)
	: Texture(renderSystem, baseObjectID, TextureType::Tex_2D, format)
	{ }


	Texture2D::~Texture2D()
	{ }


	Result Texture2D::Update(const TextureData2DDesc& dataDesc)
	{
		TextureRange updateRange;
		updateRange.offset.x = 0;
		updateRange.offset.y = 0;
		updateRange.size.x = this->_dimensions.size.x;
		updateRange.size.y = this->_dimensions.size.y;

		return this->_textureStorage->UpdateTexture2D(updateRange, dataDesc);
	}


	Result Texture2D::Update(const TextureRange& range, const TextureData2DDesc& dataDesc)
	{
		return this->_textureStorage->UpdateTexture2D(range, dataDesc);
	}



	
	Texture2DArray::Texture2DArray(RenderSystem* renderSystem, RSBaseObjectID baseObjectID, GraphicDataFormat format)
	: Texture(renderSystem, baseObjectID, TextureType::Tex_2D_Array, format)
	{ }


	Texture2DArray::~Texture2DArray()
	{ }


	Result Texture2DArray::UpdateSubtexture(Uint32 arrayIndex, const TextureData2DDesc& dataDesc)
	{
		if (arrayIndex >= this->_dimensions.arraySize)
			return ExsResult(RSC_Err_Out_Of_Range);

		TextureRange updateRange;
		updateRange.offset.x = 0;
		updateRange.offset.y = 0;
		updateRange.size.x = this->_dimensions.size.x;
		updateRange.size.y = this->_dimensions.size.y;

		return this->_textureStorage->UpdateTexture2DArraySubtexture(arrayIndex, updateRange, dataDesc);
	}


	Result Texture2DArray::UpdateSubtexture(Uint32 arrayIndex, const TextureRange& range, const TextureData2DDesc& dataDesc)
	{
		if (arrayIndex >= this->_dimensions.arraySize)
			return ExsResult(RSC_Err_Out_Of_Range);

		return this->_textureStorage->UpdateTexture2DArraySubtexture(arrayIndex, range, dataDesc);
	}

	

	
	Texture2DMultisample::Texture2DMultisample(RenderSystem* renderSystem, RSBaseObjectID baseObjectID, GraphicDataFormat format)
	: Texture(renderSystem, baseObjectID, TextureType::Tex_2D_Multisample, format)
	{ }


	Texture2DMultisample::~Texture2DMultisample()
	{ }

	

	
	Texture3D::Texture3D(RenderSystem* renderSystem, RSBaseObjectID baseObjectID, GraphicDataFormat format)
	: Texture(renderSystem, baseObjectID, TextureType::Tex_3D, format)
	{ }


	Texture3D::~Texture3D()
	{ }


	Result Texture3D::Update(const TextureData3DDesc& dataDesc)
	{
		return ExsResult(RSC_Success);
	}


	Result Texture3D::Update(const TextureRange& range, const TextureData3DDesc& dataDesc)
	{
		return ExsResult(RSC_Success);
	}


	Result Texture3D::UpdateLayer(Uint32 layerIndex, const TextureData2DDesc& dataDesc)
	{
		return ExsResult(RSC_Success);
	}


	Result Texture3D::UpdateLayer(Uint32 layerIndex, const TextureRange& range, const TextureData2DDesc& dataDesc)
	{
		return ExsResult(RSC_Success);
	}




	TextureCubeMap::TextureCubeMap(RenderSystem* renderSystem, RSBaseObjectID baseObjectID, GraphicDataFormat format)
	: Texture(renderSystem, baseObjectID, TextureType::Tex_Cube_Map, format)
	{ }


	TextureCubeMap::~TextureCubeMap()
	{ }


	Result TextureCubeMap::UpdateFace(CubeMapFace face, const TextureData2DDesc& dataDesc)
	{
		return ExsResult(RSC_Success);
	}


	Result TextureCubeMap::UpdateFace(CubeMapFace face, const TextureRange& range, const TextureData2DDesc& dataDesc)
	{
		return ExsResult(RSC_Success);
	}


}
