
#include <ExsRenderSystem/Resource/GPUBufferStorage.h>


namespace Exs
{


	GPUBufferStorage::GPUBufferStorage(const RSResourceMemoryInfo& memoryInfo, Size_t size)
	: RSResourceStorage(memoryInfo)
	, _size(size)
	{ }


	GPUBufferStorage::~GPUBufferStorage()
	{ }


}
