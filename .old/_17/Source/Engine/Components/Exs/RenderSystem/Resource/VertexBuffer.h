
#ifndef __Exs_RenderSystem_VertexBuffer_H__
#define __Exs_RenderSystem_VertexBuffer_H__

#include "GeometryBuffer.h"


namespace Exs
{


	class EXS_LIBCLASS_RENDERSYSTEM VertexBuffer : public GeometryBuffer
	{
		EXS_DECLARE_NONCOPYABLE(VertexBuffer);

		friend class RenderSystem;

	public:
		VertexBuffer(RenderSystem* renderSystem, RSBaseObjectID baseObjectID);
		virtual ~VertexBuffer();
	};


}


#endif /* __Exs_RenderSystem_VertexBuffer_H__ */
