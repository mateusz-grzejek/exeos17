
#ifndef __Exs_RenderSystem_GPUBufferBase_H__
#define __Exs_RenderSystem_GPUBufferBase_H__

#include "../Prerequisites.h"


namespace Exs
{


	class GPUBuffer;
	class GPUBufferStorage;


	///<summary>
	///</summary>
	enum class GPUBufferType : Enum
	{
		Constant_Buffer,

		Index_Buffer,

		Vertex_Buffer
	};

	
	///<summary>
	///</summary>
	enum class GPUBufferUpdateMode : Enum
	{
		Append = static_cast<Enum>(RSResourceMapMode::Write_No_Overwrite),
	
		Discard = static_cast<Enum>(RSResourceMapMode::Write_Invalidate_All),
	
		Modify = static_cast<Enum>(RSResourceMapMode::Write)
	};


	struct GPUBufferDataDesc
	{
		const void* dataPtr = nullptr;

		Size_t dataSize = 0;
	};

	
	///<summary>
	///</summary>
	struct GPUBufferCreateInfo
	{
		Size_t sizeInBytes;
		
		stdx::mask<RSResourceUsageFlags> usageFlags;
	};


	///<summary>
	///</summary>
	struct ConstantBufferCreateInfo : public GPUBufferCreateInfo
	{
	};
	

	///<summary>
	///</summary>
	struct IndexBufferCreateInfo : public GPUBufferCreateInfo
	{
		RSMemoryAccess memoryAccess;

		GPUBufferDataDesc initDataDesc;
	};
	

	///<summary>
	///</summary>
	struct ShaderResourceBufferCreateInfo : public GPUBufferCreateInfo
	{
		RSMemoryAccess memoryAccess;

		GPUBufferDataDesc initDataDesc;
	};
	

	///<summary>
	///</summary>
	struct VertexBufferCreateInfo : public GPUBufferCreateInfo
	{
		RSMemoryAccess memoryAccess;

		GPUBufferDataDesc initDataDesc;
	};


}


#endif /* __Exs_RenderSystem_GPUBufferBase_H__ */
