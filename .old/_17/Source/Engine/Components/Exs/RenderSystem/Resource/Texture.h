
#ifndef __Exs_RenderSystem_Texture_H__
#define __Exs_RenderSystem_Texture_H__

#include "TextureBase.h"


namespace Exs
{


	class TextureStorage;


	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM Texture : public RSResource
	{
		EXS_DECLARE_NONCOPYABLE(Texture);

	protected:
		TextureType        _textureType;
		GraphicDataFormat  _texFormat;
		TextureDimensions  _dimensions;
		TextureStorage*    _textureStorage;

	public:
		Texture(RenderSystem* renderSystem, RSBaseObjectID baseObjectID, TextureType type, GraphicDataFormat format);
		virtual ~Texture();

		///<summary>
		///</summary>
		TextureType GetTextureType() const;

		///<summary>
		///</summary>
		GraphicDataFormat GetTextureFormat() const;

		///<summary>
		///</summary>
		const TextureDimensions& GetDimensions() const;

	protected:
		virtual void OnBindStorage(RSResourceStorage* storage);
	};


	inline TextureType Texture::GetTextureType() const
	{
		return this->_textureType;
	}

	inline GraphicDataFormat Texture::GetTextureFormat() const
	{
		return this->_texFormat;
	}

	inline const TextureDimensions& Texture::GetDimensions() const
	{
		return this->_dimensions;
	}


	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM Texture2D : public Texture
	{
		EXS_DECLARE_NONCOPYABLE(Texture2D);

	public:
		Texture2D(RenderSystem* renderSystem, RSBaseObjectID baseObjectID, GraphicDataFormat format);
		virtual ~Texture2D();
		
		Result Update(const TextureData2DDesc& dataDesc);
		Result Update(const TextureRange& range, const TextureData2DDesc& dataDesc);
	};


	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM Texture2DArray : public Texture
	{
		EXS_DECLARE_NONCOPYABLE(Texture2DArray);

	public:
		Texture2DArray(RenderSystem* renderSystem, RSBaseObjectID baseObjectID, GraphicDataFormat format);
		virtual ~Texture2DArray();
		
		Result UpdateSubtexture(Uint32 arrayIndex, const TextureData2DDesc& dataDesc);
		Result UpdateSubtexture(Uint32 arrayIndex, const TextureRange& range, const TextureData2DDesc& dataDesc);
	};


	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM Texture2DMultisample : public Texture
	{
		EXS_DECLARE_NONCOPYABLE(Texture2DMultisample);

	public:
		Texture2DMultisample(RenderSystem* renderSystem, RSBaseObjectID baseObjectID, GraphicDataFormat format);
		virtual ~Texture2DMultisample();
	};

	
	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM Texture3D : public Texture
	{
		EXS_DECLARE_NONCOPYABLE(Texture3D);

	public:
		Texture3D(RenderSystem* renderSystem, RSBaseObjectID baseObjectID, GraphicDataFormat format);
		virtual ~Texture3D();
		
		Result Update(const TextureData3DDesc& dataDesc);
		Result Update(const TextureRange& range, const TextureData3DDesc& dataDesc);
		Result UpdateLayer(Uint32 layerIndex, const TextureData2DDesc& dataDesc);
		Result UpdateLayer(Uint32 layerIndex, const TextureRange& range, const TextureData2DDesc& dataDesc);
	};


	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM TextureCubeMap : public Texture
	{
		EXS_DECLARE_NONCOPYABLE(TextureCubeMap);

	public:
		TextureCubeMap(RenderSystem* renderSystem, RSBaseObjectID baseObjectID, GraphicDataFormat format);
		virtual ~TextureCubeMap();
		
		Result UpdateFace(CubeMapFace face, const TextureData2DDesc& dataDesc);
		Result UpdateFace(CubeMapFace face, const TextureRange& range, const TextureData2DDesc& dataDesc);
	};


}


#endif /* __Exs_RenderSystem_Texture_H__ */
