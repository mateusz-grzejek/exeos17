
#include <ExsRenderSystem/Resource/TextureStorage.h>


namespace Exs
{


	TextureStorage::TextureStorage(const RSResourceMemoryInfo& memoryInfo,
		const TextureDimensions& dimensions,
		GraphicDataFormat textureFormat)
	: RSResourceStorage(memoryInfo)
	, _texDimensions(dimensions)
	, _texFormat(textureFormat)
	{ }
	

	TextureStorage::~TextureStorage()
	{ }


}
