
#ifndef __Exs_RenderSystem_RenderTarget_H__
#define __Exs_RenderSystem_RenderTarget_H__

#include "../Prerequisites.h"


namespace Exs
{


	ExsDeclareRSClassObjHandle(RenderTargetView);


	struct RenderTargetDesc
	{

	};


	class EXS_LIBCLASS_RENDERSYSTEM RenderTarget : public RSResource
	{
	private:
		typedef std::array<RenderTargetView, Config::RS_OM_Max_Render_Targets_Num> pp;
	};


}


#endif /* __Exs_RenderSystem_RenderTarget_H__ */
