
#ifndef __Exs_RenderSystem_IndexBuffer_H__
#define __Exs_RenderSystem_IndexBuffer_H__

#include "GeometryBuffer.h"


namespace Exs
{


	class EXS_LIBCLASS_RENDERSYSTEM IndexBuffer : public GeometryBuffer
	{
		EXS_DECLARE_NONCOPYABLE(IndexBuffer);

		friend class RenderSystem;

	public:
		IndexBuffer(RenderSystem* renderSystem, RSBaseObjectID baseObjectID);
		virtual ~IndexBuffer();
	};


}


#endif /* __Exs_RenderSystem_IndexBuffer_H__ */
