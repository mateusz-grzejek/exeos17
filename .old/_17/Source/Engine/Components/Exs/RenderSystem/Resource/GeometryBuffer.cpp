
#include <ExsRenderSystem/Resource/GeometryBuffer.h>
#include <ExsRenderSystem/Resource/GPUBufferStorage.h>


namespace Exs
{


	GeometryBuffer::GeometryBuffer(RenderSystem* renderSystem, RSBaseObjectID baseObjectID, GeometryBufferType bufferType)
	: GPUBuffer(renderSystem, baseObjectID, static_cast<GPUBufferType>(bufferType))
	{ }


	GeometryBuffer::~GeometryBuffer()
	{ }


	bool GeometryBuffer::BeginDynamicUpdate(GPUBufferUpdateMode updateMode)
	{
		if (this->_bufferStorage->IsMapped())
		{
			ExsRuntimeInterrupt();
			return false;
		}

		RSResourceMapMode mapMode = static_cast<RSResourceMapMode>(updateMode);
		bool mapResult = this->_storage->MapMemory(mapMode, &(this->_mappedMemoryInfo));

		return mapResult;
	}


	bool GeometryBuffer::BeginDynamicUpdate(const RSMemoryRange& range, GPUBufferUpdateMode updateMode)
	{
		if (this->_bufferStorage->IsMapped())
		{
			ExsRuntimeInterrupt();
			return false;
		}

		RSResourceMapMode mapMode = static_cast<RSResourceMapMode>(updateMode);
		bool mapResult = this->_storage->MapMemory(range, mapMode, &(this->_mappedMemoryInfo));

		return mapResult;
	}


	void GeometryBuffer::EndDynamicUpdate()
	{
		if (!this->_bufferStorage->IsMapped())
		{
			ExsRuntimeInterrupt();
			return;
		}

		this->_storage->UnmapMemory();
		this->_mappedMemoryInfo.Reset();
	}


	void GeometryBuffer::Update(const RSMemoryRange& range, const void* data, Size_t dataSize, bool autoFlush)
	{
		ExsDebugAssert( range.offset + range.length <= this->_size );
		dataSize = stdx::get_min_of(dataSize, range.length);

		if (dataSize == 0)
			return;
		
		if (this->_mappedMemoryInfo.ptr != nullptr)
		{
			if (!this->_mappedMemoryInfo.access.is_set(AccessMode_Write))
				ExsExceptionThrowEx(EXC_Invalid_Operation, "");

			const auto& mappedRange = this->_mappedMemoryInfo.range;
			ExsDebugAssert( range.offset >= mappedRange.offset );
			ExsDebugAssert( range.offset + range.length <= mappedRange.offset + mappedRange.length );
			
			// Note, that range.offset is the offset from the beginning of the buffer, while mapped ptr always
			// points to the beginning of the mapped range. Because of that, we must calculate proper ptr offset.
			Size_t mappedPtrOffset = range.offset - mappedRange.offset;

			ExsCopyMemory(this->_mappedMemoryInfo.ptr + mappedPtrOffset, data, dataSize);

			if (autoFlush)
				this->_bufferStorage->FlushMappedRange(range);
		}
		else
		{
			this->_bufferStorage->CopySubdata(range.offset, range.length, data, dataSize);
		}
	}


	void GeometryBuffer::CopySubdata(const RSMemoryRange& range, const void* data, Size_t dataSize)
	{
		ExsDebugAssert( range.offset + range.length <= this->_size );

		if (this->_mappedMemoryInfo.ptr != nullptr)
			ExsExceptionThrowEx(EXC_Invalid_Operation, "");
		
		dataSize = stdx::get_min_of(dataSize, range.length);

		if (dataSize == 0)
			return;
		
		this->_bufferStorage->CopySubdata(range.offset, range.length, data, dataSize);
	}


}
