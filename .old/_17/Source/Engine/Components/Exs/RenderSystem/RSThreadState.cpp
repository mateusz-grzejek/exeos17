
#include <ExsRenderSystem/RenderSystem.h>
#include <ExsRenderSystem/RSContextCommandQueue.h>
#include <ExsRenderSystem/RSContextStateController.h>
#include <ExsRenderSystem/RSThread.h>
#include <ExsRenderSystem/RSThreadState.h>
#include <ExsRenderSystem/RSThreadManager.h>
#include <Exs/Core/Threading/ThreadAccessGuards.h>


namespace Exs
{


	RSThreadState::RSThreadState( RenderSystem* renderSystem,
	                              RSThreadManager* rsThreadManager,
	                              RSThreadType rsThreadType,
	                              RSThreadIndex rsThreadIndex)
	: _renderSystem(renderSystem)
	, _rsThreadManager(rsThreadManager)
	, _rsThreadType(rsThreadType)
	, _rsThreadIndex(rsThreadIndex)
	{ }


	RSThreadState::~RSThreadState()
	{ }


	bool RSThreadState::Attach()
	{
		if (this->_rsThreadObject == nullptr)
			ExsExceptionThrowEx(EXC_Invalid_Operation, "Thread object is not set!");

		ExsDeclareRestrictedThreadAccess( this->_rsThreadObject->GetUID() );

		if (!RSThreadManager::BindThreadState(this))
			return false;

		this->OnAttach();
		return true;
	}


	void RSThreadState::Detach()
	{
		if (this->_rsThreadObject == nullptr)
			ExsExceptionThrowEx(EXC_Invalid_Operation, "Thread object is not set!");

		ExsDeclareRestrictedThreadAccess( this->_rsThreadObject->GetUID() );

		this->OnDetach();
		RSThreadManager::UnbindThreadState(this);
	}


	void RSThreadState::Reset()
	{
		this->_rsContextCommandQueue->Reset();
		this->_rsContextStateController->Reset();
		this->_renderSystem->Cleanup();
	}


	void RSThreadState::SetThreadObject(RSThread* threadObject)
	{
		if (threadObject != nullptr)
		{
			ExsDebugAssert( this->_rsThreadObject == nullptr );
			threadObject->SetStateFlags(RSThreadState_Reserved, true);
		}
		else
		{
			ExsDebugAssert( this->_rsThreadObject != nullptr );
			this->_rsThreadObject->SetStateFlags(RSThreadState_Reserved, false);
		}

		this->_rsThreadObject = threadObject;
	}


	void RSThreadState::OnAttach()
	{ }


	void RSThreadState::OnDetach()
	{ }


}
