
#ifndef __Exs_RenderSystem_VertexArrayStateObject_H__
#define __Exs_RenderSystem_VertexArrayStateObject_H__

#include "VertexStreamState.h"


namespace Exs
{

	
	///<summary>
	///</summary>
	struct VertexArrayStateObjectCreateInfo
	{
		//
		VertexStreamConfiguration vertexStreamConfiguration;

		//
		VertexStreamStateDescriptorHandle vertexStreamStateDescriptor;
	};

	
	///<summary>
	///</summary>
	struct VertexArrayStateObjectInitDesc
	{
		//
		VertexStreamStateDescriptorHandle vertexStreamStateDescriptor;
	};


	///<summary>
	/// State object used to store vertex array state. Vertex array state contains all settings and
	/// properties which describe how input vertices/indices data are fetched by input vertex streams.
	///</summary>
	class VertexArrayStateObject : public RSStateObject
	{
	private:
		VertexStreamStateDescriptorHandle    _vertexStreamStateDescriptor; // Descriptor with vertex stream state.

	public:
		///<param name="renderSystem"> Pointer to the RenderSystem instance. </param>
		///<param name="baseObjectID"> Unique ID assigned to this state object. </param>
		///<param name="initDesc"> Initialization data containing handles to sub-descriptors. </param>
		VertexArrayStateObject( RenderSystem* renderSystem,
		                        RSBaseObjectID baseObjectID,
		                        const VertexArrayStateObjectInitDesc& initDesc)
		: RSStateObject(renderSystem, baseObjectID, 0, RSStateObjectType::Vertex_Array)
		, _vertexStreamStateDescriptor(initDesc.vertexStreamStateDescriptor)
		{ }

		///<summary>
		/// Returns pointer to the VertexStreamStateDescriptor. This pointer is never NULL.
		///</summary>
		VertexStreamStateDescriptor* GetVertexStreamStateDescriptor() const
		{
			return this->_vertexStreamStateDescriptor.get();
		}
	};


}


#endif /* __Exs_RenderSystem_VertexArrayStateObject_H__ */
