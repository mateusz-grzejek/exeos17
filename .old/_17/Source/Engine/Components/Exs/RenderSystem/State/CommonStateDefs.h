
#ifndef __Exs_RenderSystem_CommonStateDefs_H__
#define __Exs_RenderSystem_CommonStateDefs_H__

#include "../Prerequisites.h"


namespace Exs
{


	struct BlendConfiguration;
	struct DepthStencilConfiguration;
	struct GraphicsShaderConfiguration;
	struct InputLayoutConfiguration;
	struct RasterizerConfiguration;
	struct VertexStreamConfiguration;

	ExsDeclareRSClassObjHandle(BlendStateDescriptor);
	ExsDeclareRSClassObjHandle(DepthStencilStateDescriptor);
	ExsDeclareRSClassObjHandle(GraphicsShaderStateDescriptor);
	ExsDeclareRSClassObjHandle(InputLayoutStateDescriptor);
	ExsDeclareRSClassObjHandle(RasterizerStateDescriptor);
	ExsDeclareRSClassObjHandle(VertexStreamStateDescriptor);


	template <RSStateDescriptorType>
	struct RSStateDescriptorTypeProperties;


	#define EXS_RS_STATE_DESCRIPTOR_TYPE_REGISTER(typeTag, descriptorClassType) \
	template <> struct RSStateDescriptorTypeProperties<typeTag> \
	{ \
		static const RSStateDescriptorType typeTagValue = typeTag; \
		typedef descriptorClassType Type; \
		typedef descriptorClassType##Handle Handle; \
	};

	
	EXS_RS_STATE_DESCRIPTOR_TYPE_REGISTER(RSStateDescriptorType::Blend, BlendStateDescriptor);
	EXS_RS_STATE_DESCRIPTOR_TYPE_REGISTER(RSStateDescriptorType::Depth_Stencil, DepthStencilStateDescriptor);
	EXS_RS_STATE_DESCRIPTOR_TYPE_REGISTER(RSStateDescriptorType::Graphics_Shader, GraphicsShaderStateDescriptor);
	EXS_RS_STATE_DESCRIPTOR_TYPE_REGISTER(RSStateDescriptorType::Input_Layout, InputLayoutStateDescriptor);
	EXS_RS_STATE_DESCRIPTOR_TYPE_REGISTER(RSStateDescriptorType::Rasterizer, RasterizerStateDescriptor);
	EXS_RS_STATE_DESCRIPTOR_TYPE_REGISTER(RSStateDescriptorType::Vertex_Stream, VertexStreamStateDescriptor);



	struct RSStateConfig
	{
		EXS_LIBAPI_RENDERSYSTEM static const BlendConfiguration& GetDefaultBlendConfiguration();
		EXS_LIBAPI_RENDERSYSTEM static const DepthStencilConfiguration& GetDefaultDepthStencilConfiguration();
		EXS_LIBAPI_RENDERSYSTEM static const RasterizerConfiguration& GetDefaultRasterizerConfiguration();
	};


};


#endif /* __Exs_RenderSystem_CommonStateDefs_H__ */
