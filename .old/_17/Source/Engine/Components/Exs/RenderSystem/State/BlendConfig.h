
#ifndef __Exs_RenderSystem_BlendConfig_H__
#define __Exs_RenderSystem_BlendConfig_H__

#include "CommonStateDefs.h"


namespace Exs
{


	///<summary>
	///</summary>
	enum class BlendEquation : Enum
	{
		//
		Add,

		//
		Min,

		//
		Max,

		//
		Subtract,

		//
		Reverse_Subtract
	};


	///<summary>
	///</summary>
	enum class BlendFactor : Enum
	{
		//
		Zero,

		//
		One,

		///
		Constant_Blend_Factor,

		//
		Inv_Constant_Blend_Factor,

		//
		Dest_Alpha,

		//
		Dest_Color,

		//
		Src_Alpha,

		//
		Src_Color,

		//
		Src_Alpha_Saturate,

		//
		Inv_Src_Alpha,

		//
		Inv_Src_Color,

		//
		Inv_Dest_Alpha,

		//
		Inv_Dest_Color
	};

	
	///<summary>
	///</summary>
	struct BlendConfiguration
	{
		struct Settings
		{
			struct Component
			{
				BlendEquation equation;

				BlendFactor srcFactor;

				BlendFactor tgtFactor;
			};

			Component colorComponent;

			Component alphaComponent;

			Color constantFactor;
		};

		ActiveState state;

		Settings settings;
	};


}


#endif /* __Exs_RenderSystem_BlendConfig_H__ */
