
#ifndef __Exs_RenderSystem_GraphicsShaderState_H__
#define __Exs_RenderSystem_GraphicsShaderState_H__

#include "GraphicsShaderConfig.h"


namespace Exs
{


	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM GraphicsShaderStateDescriptor : public RSStateDescriptor
	{
		EXS_DECLARE_NONCOPYABLE(GraphicsShaderStateDescriptor);

	protected:
		GraphicsShaderConfiguration    _configuration;

	public:
		///<param name="renderSystem"> Pointer to the RenderSystem instance. </param>
		///<param name="baseObjectID"> Unique ID assigned to this descriptor. </param>
		///<param name="descriptorPID">  </param>
		///<param name="configuration">  </param>
		GraphicsShaderStateDescriptor( RenderSystem* renderSystem,
		                               RSBaseObjectID baseObjectID,
		                               RSStateDescriptorPID descriptorPID,
		                               const GraphicsShaderConfiguration& configuration)
		: RSStateDescriptor(renderSystem, baseObjectID, 0, RSStateDescriptorType::Graphics_Shader, descriptorPID)
		, _configuration(configuration)
		{ }

		const GraphicsShaderConfiguration& GetConfiguration() const
		{
			return this->_configuration;
		}

		static bool ValidateConfiguration(const GraphicsShaderConfiguration& configuration)
		{
			return true;
		}
	};


}


#endif /* __Exs_RenderSystem_GraphicsShaderState_H__ */
