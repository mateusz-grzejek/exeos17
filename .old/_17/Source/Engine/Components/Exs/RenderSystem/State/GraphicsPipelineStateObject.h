
#ifndef __Exs_RenderSystem_GraphicsPipelineStateObject_H__
#define __Exs_RenderSystem_GraphicsPipelineStateObject_H__

#include "BlendState.h"
#include "DepthStencilState.h"
#include "GraphicsShaderState.h"
#include "RasterizerState.h"
#include "InputLayoutState.h"


namespace Exs
{


	///<summary>
	///</summary>
	struct GraphicsPipelineStateObjectCreateInfo
	{
		// Configuration of blend stage. Used only if blendStateDescriptor is non-NULL.
		BlendConfiguration blendConfiguration;

		// Handle of blend state descriptor to use. If set to NULL, new descriptor is created using blendConfiguration.
		BlendStateDescriptorHandle blendStateDescriptor;

		// Configuration of depth-stencil stage. Used only if depthStencilStateDescriptor is non-NULL.
		DepthStencilConfiguration depthStencilConfiguration;

		// Handle of depth-stencil state descriptor to use. If set to NULL, new descriptor is created using depthStencilConfiguration.
		DepthStencilStateDescriptorHandle depthStencilStateDescriptor;

		// Configuration of graphics shader stage. Used only if graphicsShaderStateDescriptor is non-NULL.
		GraphicsShaderConfiguration graphicsShaderConfiguration;

		// Handle of graphics shader state descriptor to use. If set to NULL, new descriptor is created using graphicsShaderConfiguration.
		GraphicsShaderStateDescriptorHandle graphicsShaderStateDescriptor;

		// Configuration of rasterizer stage. Used only if rasterizerStateDescriptor is non-NULL.
		RasterizerConfiguration rasterizerConfiguration;

		// Handle of rasterizer state descriptor to use. If set to NULL, new descriptor is created using rasterizerConfiguration.
		RasterizerStateDescriptorHandle rasterizerStateDescriptor;

		// Configuration of vertex input stage. Used only if inputLayoutStateDescriptor is non-NULL.
		InputLayoutConfiguration inputLayoutConfiguration;

		// Handle of vertex input state descriptor to use. If set to NULL, new descriptor is created using inputLayoutConfiguration.
		InputLayoutStateDescriptorHandle inputLayoutStateDescriptor;
	};


	///<summary>
	///</summary>
	struct GraphicsPipelineStateObjectInitDesc
	{
		// Handle of blend state descriptor to use. Cannot be NULL.
		BlendStateDescriptorHandle blendStateDescriptor;

		// Handle of depth-stencil state descriptor to use. Cannot be NULL.
		DepthStencilStateDescriptorHandle depthStencilStateDescriptor;

		// Handle of graphics shader state descriptor to use. Cannot be NULL.
		GraphicsShaderStateDescriptorHandle graphicsShaderStateDescriptor;

		// Handle of rasterizer state descriptor to use. Cannot be NULL.
		RasterizerStateDescriptorHandle rasterizerStateDescriptor;

		// Handle of vertex input state descriptor to use. Cannot be NULL.
		InputLayoutStateDescriptorHandle inputLayoutStateDescriptor;
	};


	///<summary>
	///</summary>
	class GraphicsPipelineStateObject : public RSStateObject
	{
	private:
		BlendStateDescriptorHandle             _blendStateDescriptor; // Descriptor with blend configuration.
		DepthStencilStateDescriptorHandle      _depthStencilStateDescriptor; // Descriptor with dpeth-stencil configuration.
		GraphicsShaderStateDescriptorHandle    _graphicsShaderStateDescriptor; // Descriptor with graphics shaders.
		InputLayoutStateDescriptorHandle       _inputLayoutStateDescriptor; // Descriptor with input layout configuration.
		RasterizerStateDescriptorHandle        _rasterizerStateDescriptor; // Descriptor with rasterizer configuration.

	public:
		///<param name="renderSystem"> Pointer to the RenderSystem instance. </param>
		///<param name="baseObjectID"> Unique ID assigned to this state object. </param>
		///<param name="initDesc">  </param>
		GraphicsPipelineStateObject( RenderSystem* renderSystem,
		                             RSBaseObjectID baseObjectID,
		                             const GraphicsPipelineStateObjectInitDesc& initDesc)
		: RSStateObject(renderSystem, baseObjectID, 0, RSStateObjectType::Graphics_Pipeline)
		, _blendStateDescriptor(initDesc.blendStateDescriptor)
		, _depthStencilStateDescriptor(initDesc.depthStencilStateDescriptor)
		, _graphicsShaderStateDescriptor(initDesc.graphicsShaderStateDescriptor)
		, _inputLayoutStateDescriptor(initDesc.inputLayoutStateDescriptor)
		, _rasterizerStateDescriptor(initDesc.rasterizerStateDescriptor)
		{ }

		///<summary>
		///</summary>
		BlendStateDescriptor* GetBlendStateDescriptor() const
		{
			return this->_blendStateDescriptor.get();
		}

		///<summary>
		///</summary>
		DepthStencilStateDescriptor* GetDepthStencilStateDescriptor() const
		{
			return this->_depthStencilStateDescriptor.get();
		}

		///<summary>
		///</summary>
		GraphicsShaderStateDescriptor* GetGraphicsShaderStateDescriptor() const
		{
			return this->_graphicsShaderStateDescriptor.get();
		}

		///<summary>
		///</summary>
		InputLayoutStateDescriptor* GetInputLayoutStateDescriptor() const
		{
			return this->_inputLayoutStateDescriptor.get();
		}

		///<summary>
		///</summary>
		RasterizerStateDescriptor* GetRasterizerStateDescriptor() const
		{
			return this->_rasterizerStateDescriptor.get();
		}
	};


}


#endif /* __Exs_RenderSystem_GraphicsPipelineStateObject_H__ */
