
#ifndef __Exs_RenderSystem_BindingCacheTable_H__
#define __Exs_RenderSystem_BindingCacheTable_H__

#include "../Prerequisites.h"


namespace Exs
{
	
	
	ExsDeclareRSClassObjHandle(ConstantBuffer);
	ExsDeclareRSClassObjHandle(Sampler);
	ExsDeclareRSClassObjHandle(ShaderResourceView);


	template <class Handle, Size_t Size>
	class BindingCacheTable
	{
	public:
		struct Binding
		{
		public:
			Handle object;
			stdx::mask<ShaderStageFlags> stageAccess;

		public:
			Binding()
			: object(nullptr)
			, stageAccess(ShaderStage_None)
			{ }
		};

		typedef std::array<Binding, Size> BindingArray;

	private:
		BindingArray  _bindings;
		Uint32        _activeBindingsNum;

	public:
		BindingCacheTable(const BindingArray& bindings, Uint32 activeBindingsNum)
		: _bindings(bindings)
		, _activeBindingsNum(activeBindingsNum)
		{ }

		virtual ~BindingCacheTable()
		{ }
		
		///<summary>
		///</summary>
		template <class T>
		T* As()
		{
			T* otherPtr = dbgsafe_ptr_cast<T*>(this);
			return otherPtr;
		}
		
		///<summary>
		///</summary>
		template <class T>
		const T* As() const
		{
			T* otherPtr = dbgsafe_ptr_cast<T*>(this);
			return otherPtr;
		}

		Uint32 GetActiveBindingsNum() const
		{
			return this->_activeBindingsNum;
		}
	};


	///<summary>
	///</summary>
	class ConstantBufferBindingCacheTable : public BindingCacheTable<ConstantBufferHandle, Config::RS_IA_Max_Constant_Buffers_Num>
	{
	public:
		ConstantBufferBindingCacheTable(const BindingArray& bindings, Uint32 activeBindingsNum)
		: BindingCacheTable(bindings, activeBindingsNum)
		{ }
	};

	
	///<summary>
	///</summary>
	class SamplerBindingCacheTable : public BindingCacheTable<SamplerHandle, Config::RS_IA_Max_Samplers_Num>
	{
	public:
		SamplerBindingCacheTable(const BindingArray& bindings, Uint32 activeBindingsNum)
		: BindingCacheTable(bindings, activeBindingsNum)
		{ }
	};


	///<summary>
	///</summary>
	class ShaderResourceBindingCacheTable : public BindingCacheTable<ShaderResourceViewHandle, Config::RS_IA_Max_Shader_Resources_Num>
	{
	public:
		ShaderResourceBindingCacheTable(const BindingArray& bindings, Uint32 activeBindingsNum)
		: BindingCacheTable(bindings, activeBindingsNum)
		{ }
	};


}


#endif /* __Exs_RenderSystem_BindingCacheTable_H__ */

