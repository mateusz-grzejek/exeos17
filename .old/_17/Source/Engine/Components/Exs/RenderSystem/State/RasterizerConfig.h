
#ifndef __Exs_RenderSystem_RasterizerConfig_H__
#define __Exs_RenderSystem_RasterizerConfig_H__

#include "CommonStateDefs.h"


namespace Exs
{


	///<summary>
	///</summary>
	enum class CullMode : Enum
	{
		//
		Back,

		//
		Front,

		//
		Disabled,

		//
		Default = Back
	};


	///<summary>
	/// Represents rendering modes used by rasterizer to draw primitives.
	///</summary>
	enum class FillMode : Enum
	{
		// All pixels belonging to polygons are drawn using output from the pixel shader stage.
		Solid,

		// Only vertices and boundary edges are drawn. Interior of polygons is not part of the output.
		Wireframe,

		//
		Default = Solid
	};


	///<summary>
	///</summary>
	enum class VerticesOrder : Enum
	{
		//
		Clockwise,

		//
		Counter_Clockwise,

		//
		Default = Counter_Clockwise
	};


	///<summary>
	///</summary>
	struct RasterizerConfiguration
	{
		struct Settings
		{
			CullMode faceCullMode;

			VerticesOrder frontFaceOrder;

			FillMode triangleFillMode;
		};

		ActiveState scissorTestState;

		Settings settings;
	};


}


#endif /* __Exs_RenderSystem_RasterizerConfig_H__ */
