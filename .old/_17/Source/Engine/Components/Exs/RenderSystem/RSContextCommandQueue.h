
#ifndef __Exs_RenderSystem_RSContextCommandQueue_H__
#define __Exs_RenderSystem_RSContextCommandQueue_H__

#include "Prerequisites.h"


namespace Exs
{


	class RSContextStateController;
	class RSThreadState;
	
	ExsDeclareRSClassObjHandle(ConstantBuffer);
	ExsDeclareRSClassObjHandle(Sampler);
	ExsDeclareRSClassObjHandle(ShaderResourceView);
	ExsDeclareRSClassObjHandle(ConstantBufferBindingCacheTable);
	ExsDeclareRSClassObjHandle(SamplerBindingCacheTable);
	ExsDeclareRSClassObjHandle(ShaderResourceBindingCacheTable);


	enum RenderTargetBufferFlags : Enum
	{
		RenderTargetBuffer_Color = 0x0001,
		RenderTargetBuffer_Depth = 0x0002,
		RenderTargetBuffer_Stencil = 0x0004,
		RenderTargetBuffer_All = 0xFFFF
	};


	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM RSContextCommandQueue
	{
	protected:
		RenderSystem*                _renderSystem;
		RSThreadState*               _rsThreadState;
		RSContextStateController*    _rsContextStateController;

	public:
		RSContextCommandQueue(RenderSystem* renderSystem, RSThreadState* rsThreadState, RSContextStateController* rsContextStateController);
		virtual ~RSContextCommandQueue();
		
		virtual void SetConstantBuffer(Uint32 cbufferIndex, stdx::mask<ShaderStageFlags> stageAccess, const ConstantBufferHandle& constantBuffer) = 0;
		virtual void SetConstantBufferBindingCacheTable(const ConstantBufferBindingCacheTableHandle& cbufferBindingCacheTable) = 0;
		
		virtual void SetSampler(Uint32 resourceIndex, stdx::mask<ShaderStageFlags> stageAccess, const SamplerHandle& sampler) = 0;
		virtual void SetSamplerBindingCacheTable(const SamplerBindingCacheTableHandle& samplerBindingCacheTable) = 0;
		
		virtual void SetShaderResource(Uint32 resourceIndex, stdx::mask<ShaderStageFlags> stageAccess, const ShaderResourceViewHandle& resourceView) = 0;
		virtual void SetShaderResourceBindingCacheTable(const ShaderResourceBindingCacheTableHandle& shaderResourceBindingCacheTable) = 0;

		virtual void Clear(stdx::mask<RenderTargetBufferFlags> bufferMask) = 0;
		virtual void ClearColorBuffer() = 0;

		virtual void Draw(Uint verticesOffset, Size_t verticesCount) = 0;
		virtual void DrawIndexed(Uint verticesOffset, Size_t verticesCount, IndexBufferDataType indicesType) = 0;

		virtual void Present() = 0;

		virtual void Reset();

		bool SetGraphicsPipelineState(const GraphicsPipelineStateObjectHandle& graphicsPipelineStateObject);
		bool SetVertexArrayState(const VertexArrayStateObjectHandle& vertexArrayStateObject);

		bool SetClearColor(const Color* color);
		bool SetDepthRange(const DepthRange* depthRange);
		bool SetScissorRect(const RectU32* scissorRect);
		bool SetViewport(const Viewport* viewport);

		RSContextStateController* GetContextStateController() const;
	};


	inline RSContextStateController* RSContextCommandQueue::GetContextStateController() const
	{
		return this->_rsContextStateController;
	}


}


#endif /* __Exs_RenderSystem_RSContextCommandQueue_H__ */
