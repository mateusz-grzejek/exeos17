
#ifndef __Exs_RenderSystem_VideoMode_H__
#define __Exs_RenderSystem_VideoMode_H__

#include "Prerequisites.h"


namespace Exs
{

	
	///<summary>
	///</summary>
	enum class VideoAspectRatio : Enum
	{
		// 4:3 aspect - 1024x768, 800x600, etc.
		R_4_3 = 0xDF0403,

		// 5:3 aspect - 1280x768.
		R_5_3 = 0xDF0503,

		// 5:4 aspect - 1280x1024, 2560x2048, etc.
		R_5_4 = 0xDF0504,

		// 16:9 aspect - 1280x720, 1920x1080, etc.
		R_16_9 = 0xDF1609,

		// 16:10 aspect - 1280x800, 1680x1050, etc.
		R_16_10 = 0xDF1610,

		// Other aspects, not listed above.
		Other = 0xDF0101
	};


	enum VideoModeFlags : Enum
	{
		VideoMode_Default = 0,

		VideoMode_Fullscreen = 0x8000
	};

	
	///<summary>
	///</summary>
	struct DisplayResolution
	{
		// Width of the display, in pixels.
		Uint32 width;
		
		// Height of the display, in pixels.
		Uint32 height;

		// Aspect ratio of the display.
		VideoAspectRatio apectRatio;

		// Aspect ratio multiplier: width / height.
		float apectRatioMultiplier;
	};

	
	///<summary>
	///</summary>
	struct VideoModeConfig
	{
		// Display resolution used by this video mode.
		DisplayResolution displayResolution;

		// Bit depth of color format used by this video mode.
		Uint16 formatDepth;

		// Refresh rate used by this video mode, in Hz.
		Uint16 refreshRate;
	};


}


#endif /* __Exs_RenderSystem_VideoMode_H__ */
