
#include <ExsRenderSystem/RSContextStateController.h>
#include <ExsRenderSystem/RSThreadState.h>
#include <ExsRenderSystem/State/GraphicsPipelineStateObject.h>
#include <ExsRenderSystem/State/VertexArrayStateObject.h>


namespace Exs
{


	RSContextStateController::RSContextStateController(RenderSystem* renderSystem, RSThreadState* rsThreadState)
	: _renderSystem(renderSystem)
	, _rsThreadState(rsThreadState)
	{ }


	RSContextStateController::~RSContextStateController()
	{ }


	void RSContextStateController::Reset()
	{
		this->_graphicsPipelineStateObject = nullptr;
		this->_vertexArrayStateObject = nullptr;
	}


	bool RSContextStateController::SetGraphicsPipelineState(const GraphicsPipelineStateObjectHandle& graphicsPipelineStateObject)
	{
		if (this->_graphicsPipelineStateObject != graphicsPipelineStateObject)
		{
			auto* blendStateDescriptor = graphicsPipelineStateObject->GetBlendStateDescriptor();
			if (blendStateDescriptor != this->_stateDescriptors.blend)
			{
				this->UpdateBlendState(blendStateDescriptor);
				this->_updateMask.set(RSStateUpdate_Descriptor_Blend);
				this->_stateDescriptors.blend = blendStateDescriptor;
			}

			auto* depthStencilStateDescriptor = graphicsPipelineStateObject->GetDepthStencilStateDescriptor();
			if (depthStencilStateDescriptor != this->_stateDescriptors.depthStencil)
			{
				this->UpdateDepthStencilState(depthStencilStateDescriptor);
				this->_updateMask.set(RSStateUpdate_Descriptor_Depth_Stencil);
				this->_stateDescriptors.depthStencil = depthStencilStateDescriptor;
			}

			auto* rasterizerStateDescriptor = graphicsPipelineStateObject->GetRasterizerStateDescriptor();
			if (rasterizerStateDescriptor != this->_stateDescriptors.rasterizer)
			{
				this->UpdateRasterizerState(rasterizerStateDescriptor);
				this->_updateMask.set(RSStateUpdate_Descriptor_Rasterizer);
				this->_stateDescriptors.rasterizer = rasterizerStateDescriptor;
			}

			auto* graphicsShaderStateDescriptor = graphicsPipelineStateObject->GetGraphicsShaderStateDescriptor();
			if (graphicsShaderStateDescriptor != this->_stateDescriptors.graphicsShader)
			{
				this->_updateMask.set(RSStateUpdate_Descriptor_Graphics_Shader);
				this->_stateDescriptors.graphicsShader = graphicsShaderStateDescriptor;
			}

			auto* inputLayoutStateDescriptor = graphicsPipelineStateObject->GetInputLayoutStateDescriptor();
			if (inputLayoutStateDescriptor != this->_stateDescriptors.inputLayout)
			{
				this->_updateMask.set(RSStateUpdate_Descriptor_Input_Layout);
				this->_stateDescriptors.inputLayout = inputLayoutStateDescriptor;
			}

			this->_graphicsPipelineStateObject = graphicsPipelineStateObject;
			return true;
		}

		return false;
	}


	bool RSContextStateController::SetVertexArrayState(const VertexArrayStateObjectHandle& vertexArrayStateObject)
	{
		if (this->_vertexArrayStateObject != vertexArrayStateObject)
		{
			auto* vertexStreamStateDescriptor = vertexArrayStateObject->GetVertexStreamStateDescriptor();
			if (vertexStreamStateDescriptor != this->_stateDescriptors.vertexStream)
			{
				this->_updateMask.set(RSStateUpdate_Descriptor_Vertex_Stream);
				this->_stateDescriptors.vertexStream = vertexStreamStateDescriptor;
			}

			this->_vertexArrayStateObject = vertexArrayStateObject;
			return true;
		}

		return false;
	}


	bool RSContextStateController::ApplyInternalRenderState()
	{
		return this->_updateMask != 0;
	}

	
}
