
#ifndef __Exs_RenderSystem_RSResourceView_H__
#define __Exs_RenderSystem_RSResourceView_H__


namespace Exs
{


	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM RSResourceView : public RSBaseObject
	{
		EXS_DECLARE_NONCOPYABLE(RSResourceView);

	protected:
		RSResourceViewType  _resourceViewType;

	public:
		///<param name="renderSystem"> Pointer to the RenderSystem instance. </param>
		///<param name="baseObjectID"> Unique ID assigned to this view. </param>
		///<param name="initialState"> Initial state that is set for this view. </param>
		RSResourceView( RenderSystem* renderSystem,
		                RSBaseObjectID baseObjectID,
		                Enum initialState,
		                RSResourceViewType resourceViewType);

		virtual ~RSResourceView();

		///<summary>
		///</summary>
		RSResourceViewType GetResourceViewType() const;
	};
	

	inline RSResourceViewType RSResourceView::GetResourceViewType() const
	{
		return this->_resourceViewType;
	}


}


#endif /* __Exs_RenderSystem_RSResourceView_H__ */
