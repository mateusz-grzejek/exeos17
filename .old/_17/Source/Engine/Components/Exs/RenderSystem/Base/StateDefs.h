
#ifndef __Exs_RenderSystem_StateDefs_H__
#define __Exs_RenderSystem_StateDefs_H__


namespace Exs
{


	ExsDeclareRSClassObjHandle(GraphicsPipelineStateObject);
	ExsDeclareRSClassObjHandle(VertexArrayStateObject);


	


	namespace Config
	{
		enum : Size_t
		{
			RS_IA_Max_Constant_Buffers_Num = 16,

			RS_IA_Max_Samplers_Num = 16,

			RS_IA_Max_Shader_Resources_Num = 64,
		};
	}


}


#endif /* __Exs_RenderSystem_StateDefs_H__ */
