
#ifndef __Exs_RenderSystem_CommonTypes_H__
#define __Exs_RenderSystem_CommonTypes_H__


namespace Exs
{


	struct DataDesc
	{
		const void* ptr;

		Size_t size;
	};


	///<summary>
	///</summary>
	struct DepthRange
	{
	public:
		float min;
		float max;

	public:
		DepthRange()
		: min(0.0f)
		, max(1.0f)
		{ }

		DepthRange(float min, float max)
		: min(min)
		, max(max)
		{ }

		bool operator==(const DepthRange& rhs) const
		{
			return (this->min == rhs.min) && (this->max == rhs.max);
		}

		bool operator!=(const DepthRange& rhs) const
		{
			return (this->min != rhs.min) || (this->max != rhs.max);
		}
	};


	///<summary>
	///</summary>
	struct Viewport
	{
	public:
		Vector2I32  origin; // Origin point of the viewport.
		Vector2I32  size; // Size of the viewport.

	public:
		Viewport()
		: origin(0, 0)
		, size(0, 0)
		{ }
		
		template <typename Tr>
		Viewport(const RectBase<Tr>& rect)
		: origin(static_cast<Int32>(rect.position.x), static_cast<Int32>(rect.position.y))
		, size(static_cast<Int32>(rect.size.x), static_cast<Int32>(rect.size.y))
		{ }

		template <typename Tv, typename Tw>
		Viewport(const Vector2<Tv>& origin, const Vector2<Tw>& size)
		: origin(static_cast<Int32>(origin.x), static_cast<Int32>(origin.y))
		, size(static_cast<Int32>(size.x), static_cast<Int32>(size.y))
		{ }

		template <typename Tx, typename Ty, typename Tw, typename Th>
		Viewport(Tx originX, Ty originY, Tw width, Th height)
		: origin(static_cast<Int32>(originX), static_cast<Int32>(originY))
		, size(static_cast<Int32>(width), static_cast<Int32>(height))
		{ }

		operator RectI32() const
		{
			return RectI32(this->origin, this->size);
		}

		bool operator==(const Viewport& rhs) const
		{
			return (this->origin == rhs.origin) && (this->size == rhs.size);
		}

		bool operator!=(const Viewport& rhs) const
		{
			return (this->origin != rhs.origin) || (this->size != rhs.size);
		}
	};


}


#endif /* __Exs_RenderSystem_CommonTypes_H__ */
