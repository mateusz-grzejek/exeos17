
#include <ExsRenderSystem/RenderSystem.h>


namespace Exs
{


	RSResourceView::RSResourceView( RenderSystem* renderSystem,
	                                RSBaseObjectID baseObjectID,
	                                Enum initialState,
	                                RSResourceViewType resourceViewType)
	: RSBaseObject(renderSystem, RSBaseObjectType::Resource_View, baseObjectID, initialState)
	, _resourceViewType(resourceViewType)
	{ }
	

	RSResourceView::~RSResourceView()
	{
		this->_renderSystem->OnResourceViewDestroy(this->GetBaseObjectID(), this);
	}


}
