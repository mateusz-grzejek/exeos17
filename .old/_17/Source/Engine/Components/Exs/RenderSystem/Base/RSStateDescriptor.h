
#ifndef __Exs_RenderSystem_RSStateDescriptor_H__
#define __Exs_RenderSystem_RSStateDescriptor_H__


namespace Exs
{


	// Type used to represent persistent (RS-unique and immutable) IDs of state descriptor objects.
	typedef Uint64 RSStateDescriptorPID;


	enum : RSStateDescriptorPID
	{
		// Represents invalid desriptor PID. Descriptor with this PID set has not been initialized yet.
		Invalid_RSStateDescriptor_PID = stdx::limits<RSStateDescriptorPID>::max_value
	};


	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM RSStateDescriptor : public RSBaseObject
	{
		EXS_DECLARE_NONCOPYABLE(RSStateDescriptor);

	protected:
		RSStateDescriptorType   _stateDescriptorType;
		RSStateDescriptorPID    _descriptorPID;

	public:
		///<param name="renderSystem"> Pointer to the RenderSystem instance. </param>
		///<param name="baseObjectID"> Unique ID assigned to this descriptor. </param>
		///<param name="initialState"> Initial state that is set for this descriptor. </param>
		///<param name="stateDescriptorType">  </param>
		///<param name="descriptorPID">  </param>
		RSStateDescriptor( RenderSystem* renderSystem,
		                   RSBaseObjectID baseObjectID,
		                   Enum initialState,
		                   RSStateDescriptorType stateDescriptorType,
		                   RSStateDescriptorPID descriptorPID);

		virtual ~RSStateDescriptor();
		
		///<summary>
		///</summary>
		RSStateDescriptorType GetStateDescriptorType() const;
		
		///<summary>
		///</summary>
		RSStateDescriptorPID GetDescriptorPID() const;
	};
	

	inline RSStateDescriptorType RSStateDescriptor::GetStateDescriptorType() const
	{
		return this->_stateDescriptorType;
	}

	inline RSStateDescriptorPID RSStateDescriptor::GetDescriptorPID() const
	{
		return this->_descriptorPID;
	}


}


#endif /* __Exs_RenderSystem_RSStateDescriptor_H__ */
