
#ifndef __Exs_RenderSystem_RenderSystem_H__
#define __Exs_RenderSystem_RenderSystem_H__

#include "RSObjectFactory.h"
#include "RSObjectManager.h"
#include "RSStateDescriptorCache.h"
#include "RSThreadState.h"
#include "State/CommonStateDefs.h"


namespace Exs
{


	class RSContextStateController;
	class RSStateDescriptorCache;
	class RSThreadState;
	
	struct ConstantBufferCreateInfo;
	struct IndexBufferCreateInfo;
	struct SamplerCreateInfo;
	struct ShaderCreateInfo;
	struct Texture2DCreateInfo;
	struct Texture2DArrayCreateInfo;
	struct Texture3DCreateInfo;
	struct VertexBufferCreateInfo;

	struct GraphicsPipelineStateObjectCreateInfo;
	struct VertexArrayStateObjectCreateInfo;
	
	struct ConstantBufferBinding;
	struct SamplerBinding;
	struct ShaderResourceBinding;

	ExsDeclareRSClassObjHandle(ConstantBuffer);
	ExsDeclareRSClassObjHandle(IndexBuffer);
	ExsDeclareRSClassObjHandle(Sampler);
	ExsDeclareRSClassObjHandle(Shader);
	ExsDeclareRSClassObjHandle(ShaderResourceView);
	ExsDeclareRSClassObjHandle(Texture2D);
	ExsDeclareRSClassObjHandle(Texture3D);
	ExsDeclareRSClassObjHandle(Texture2DArray);
	ExsDeclareRSClassObjHandle(VertexBuffer);

	ExsDeclareRSClassObjHandle(GraphicsPipelineStateObject);
	ExsDeclareRSClassObjHandle(VertexArrayStateObject);
	
	ExsDeclareRSClassObjHandle(ConstantBufferBindingCacheTable);
	ExsDeclareRSClassObjHandle(SamplerBindingCacheTable);
	ExsDeclareRSClassObjHandle(ShaderResourceBindingCacheTable);


	///<summary>
	///</summary>
	class EXS_LIBCLASS_RENDERSYSTEM RenderSystem
	{
		EXS_DECLARE_NONCOPYABLE(RenderSystem);
		
		friend class GraphicsDriver;
		friend class RSMiscObject;
		friend class RSResource;
		friend class RSResourceView;
		friend class RSStateDescriptor;
		friend class RSStateObject;
		friend class RSThreadManager;

	protected:
		GraphicsDriver*                    _graphicDriver; // GraphicsDriver this RS is created by.
		RSMemorySystem*                   _memorySystem;
		RSObjectFactory                   _rsObjectFactory; // Object factory used by this RS.
		RSObjectManager                   _rsObjectManager; // Object manager used by this RS.
		RSStateDescriptorCache            _rsStateDescriptorCache; // State descriptor cache used by this RS.
		std::unique_ptr<RSThreadManager>  _rsThreadManager; // State descriptor cache used by this RS.

	public:
		RenderSystem(GraphicsDriver* graphicDriver, RSMemorySystem* memorySystem);
		virtual ~RenderSystem();

		RSThreadState* AcquireWorkerThreadState(RSThread* threadObject = nullptr);

		void ReleaseWorkerThreadState(RSThreadState* threadState);

		virtual void Cleanup();

		virtual void ReleaseObjects();
		
		virtual SamplerHandle CreateSampler(const SamplerCreateInfo& createInfo) = 0;
		virtual ShaderHandle CreateShader(const ShaderCreateInfo& createInfo) = 0;
		
		virtual ConstantBufferBindingCacheTableHandle CreateConstantBufferBindingCacheTable(const ConstantBufferBinding* bindings, Uint32 bindingsNum) = 0;
		virtual SamplerBindingCacheTableHandle CreateSamplerBindingCacheTable(const SamplerBinding* bindings, Uint32 bindingsNum) = 0;
		virtual ShaderResourceBindingCacheTableHandle CreateShaderResourceBindingCacheTable(const ShaderResourceBinding* bindings, Uint32 bindingsNum) = 0;

		virtual ConstantBufferHandle CreateConstantBuffer(const ConstantBufferCreateInfo& createInfo) = 0;
		virtual IndexBufferHandle CreateIndexBuffer(const IndexBufferCreateInfo& createInfo) = 0;
		virtual VertexBufferHandle CreateVertexBuffer(const VertexBufferCreateInfo& createInfo) = 0;
		
		virtual Texture2DHandle CreateTexture2D(const Texture2DCreateInfo& createInfo) = 0;
		virtual Texture3DHandle CreateTexture3D(const Texture3DCreateInfo& createInfo) = 0;
		virtual Texture2DArrayHandle CreateTexture2DArray(const Texture2DArrayCreateInfo& createInfo) = 0;
		
		virtual ShaderResourceViewHandle CreateTextureShaderResourceView(const Texture2DHandle& texture2D) = 0;
		virtual ShaderResourceViewHandle CreateTextureShaderResourceView(const Texture2DArrayHandle& texture2DArray) = 0;

		virtual BlendStateDescriptorHandle CreateBlendStateDescriptor(const BlendConfiguration& configuration) = 0;
		virtual DepthStencilStateDescriptorHandle CreateDepthStencilStateDescriptor(const DepthStencilConfiguration& configuration) = 0;
		virtual GraphicsShaderStateDescriptorHandle CreateGraphicsShaderStateDescriptor(const GraphicsShaderConfiguration& configuration) = 0;
		virtual InputLayoutStateDescriptorHandle CreateInputLayoutStateDescriptor( const InputLayoutConfiguration& configuration,
		                                                                           GraphicsShaderStateDescriptorHandle vsSignatureDescriptor) = 0;
		virtual RasterizerStateDescriptorHandle CreateRasterizerStateDescriptor(const RasterizerConfiguration& configuration) = 0;
		virtual VertexStreamStateDescriptorHandle CreateVertexStreamStateDescriptor(const VertexStreamConfiguration& configuration) = 0;

		GraphicsPipelineStateObjectHandle CreateGraphicsPipelineStateObject(const GraphicsPipelineStateObjectCreateInfo& createInfo);
		VertexArrayStateObjectHandle CreateVertexArrayStateObject(const VertexArrayStateObjectCreateInfo& createInfo);

		RSMemorySystem* GetMemorySystem() const;

	friendapi:
		// Initializes state used by RS threads. This state contains all objects and controllers defined
		// in per-thread basis (context state controller, command queue, etc.).
		virtual std::unique_ptr<RSThreadState> InitializeRSThreadState(RSThreadIndex threadIndex, RSThreadType threadType) = 0;

		//
		RSThreadState* AcquireMainThreadState(RSThread* threadObject = nullptr);

		//
		void ReleaseMainThreadState(RSThreadState* threadState);

		// Releases misc object and removes it from the index. Call is forwarded to the internal manager.
		// This methos is used directly by the RSMiscObject class.
		void OnMiscObjectDestroy(RSBaseObjectID miscObjectID, RSMiscObject* miscObject);

		// Releases resource and removes it from the index. Call is forwarded to the internal manager.
		// This methos is used directly by the RSResource class.
		void OnResourceDestroy(RSBaseObjectID resourceID, RSResource* resource);

		// Releases resource view and removes it from the index. Call is forwarded to the internal manager.
		// This methos is used directly by the RSResourceView class.
		void OnResourceViewDestroy(RSBaseObjectID resourceViewID, RSResourceView* resourceView);
		
		// Releases state descriptor and removes it from the index. Call is forwarded to the internal manager.
		// This methos is used directly by the RSStateDescriptor class.
		void OnStateDescriptorDestroy(RSBaseObjectID stateDescriptorID, RSStateDescriptor* stateDescriptor);
		
		// Releases state object and removes it from the index. Call is forwarded to the internal manager.
		// This methos is used directly by the RSStateObject class.
		void OnStateObjectDestroy(RSBaseObjectID stateObjectID, RSStateObject* stateObject);

	protected:
		///<summary>
		/// Creates new resource of specified type and returns handle to this resource. Request is forwarded
		/// to internal manager which also adds the resource to the global RS index.
		///</summary>
		template <class Misc_object_t, class Render_system_t, class... Args>
		RSHandle<Misc_object_t> NewMiscObject(Render_system_t* renderSystem, Args&&... args);

		///<summary>
		/// Creates new resource of specified type and returns handle to this resource. Request is forwarded
		/// to internal manager which also adds the resource to the global RS index.
		///</summary>
		template <class Resource_t, class Render_system_t, class... Args>
		RSHandle<Resource_t> NewResource(Render_system_t* renderSystem, Args&&... args);
		
		///<summary>
		/// Creates new resource view of specified type and returns handle to this view. Request is forwarded
		/// to internal manager which also adds the resource view to the global RS index.
		///</summary>
		template <class Resource_view_t, class Render_system_t, class... Args>
		RSHandle<Resource_view_t> NewResourceView(Render_system_t* renderSystem, Args&&... args);
		
		///<summary>
		/// Creates new state descriptor of specified type and returns handle to this descriptor. Request is forwarded
		/// to internal manager which also adds the descriptor to the global RS index.
		///</summary>
		template <class State_descriptor_t, class Render_system_t, class... Args>
		RSHandle<State_descriptor_t> NewStateDescriptor(Render_system_t* renderSystem, Args&&... args);
		
		///<summary>
		/// Creates new state object of specified type and returns handle to this object. Request is forwarded
		/// to internal manager which also adds the object to the global RS index.
		///</summary>
		template <class State_object_t, class Render_system_t, class... Args>
		RSHandle<State_object_t> NewStateObject(Render_system_t* renderSystem, Args&&... args);

	};


	inline RSMemorySystem* RenderSystem::GetMemorySystem() const
	{
		return this->_memorySystem;
	}

	inline void RenderSystem::OnMiscObjectDestroy(RSBaseObjectID miscObjectID, RSMiscObject* miscObject)
	{
		this->_rsObjectManager.OnMiscObjectDestroy(miscObjectID, miscObject);
	}

	inline void RenderSystem::OnResourceDestroy(RSBaseObjectID resourceID, RSResource* resource)
	{
		this->_rsObjectManager.OnResourceDestroy(resourceID, resource);
	}

	inline void RenderSystem::OnResourceViewDestroy(RSBaseObjectID resourceViewID, RSResourceView* resourceView)
	{
		this->_rsObjectManager.OnResourceViewDestroy(resourceViewID, resourceView);
	}

	inline void RenderSystem::OnStateDescriptorDestroy(RSBaseObjectID stateDescriptorID, RSStateDescriptor* stateDescriptor)
	{
		this->_rsObjectManager.OnStateDescriptorDestroy(stateDescriptorID, stateDescriptor);
	}

	inline void RenderSystem::OnStateObjectDestroy(RSBaseObjectID stateObjectID, RSStateObject* stateObject)
	{
		this->_rsObjectManager.OnStateObjectDestroy(stateObjectID, stateObject);
	}

	template <class Misc_object_t, class Render_system_t, class... Args>
	inline RSHandle<Misc_object_t> RenderSystem::NewMiscObject(Render_system_t* renderSystem, Args&&... args)
	{
		return this->_rsObjectManager.NewMiscObject<Misc_object_t>(renderSystem, std::forward<Args>(args)...);
	}

	template <class Resource_t, class Render_system_t, class... Args>
	inline RSHandle<Resource_t> RenderSystem::NewResource(Render_system_t* renderSystem, Args&&... args)
	{
		return this->_rsObjectManager.NewResource<Resource_t>(renderSystem, std::forward<Args>(args)...);
	}

	template <class Resource_view_t, class Render_system_t, class... Args>
	inline RSHandle<Resource_view_t> RenderSystem::NewResourceView(Render_system_t* renderSystem, Args&&... args)
	{
		return this->_rsObjectManager.NewResourceView<Resource_view_t>(renderSystem, std::forward<Args>(args)...);
	}

	template <class State_descriptor_t, class Render_system_t, class... Args>
	inline RSHandle<State_descriptor_t> RenderSystem::NewStateDescriptor(Render_system_t* renderSystem, Args&&... args)
	{
		auto stateDescriptor = this->_rsObjectManager.NewStateDescriptor<State_descriptor_t>(renderSystem, std::forward<Args>(args)...);
		this->_rsStateDescriptorCache.RegisterDescriptor(stateDescriptor);
		return stateDescriptor;
	}

	template <class State_object_t, class Render_system_t, class... Args>
	inline RSHandle<State_object_t> RenderSystem::NewStateObject(Render_system_t* renderSystem, Args&&... args)
	{
		return this->_rsObjectManager.NewStateObject<State_object_t>(renderSystem, std::forward<Args>(args)...);
	}

}


#endif /* __Exs_RenderSystem_RenderSystem_H__ */
