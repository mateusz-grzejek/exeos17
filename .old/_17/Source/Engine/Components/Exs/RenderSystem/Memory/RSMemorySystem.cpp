
#include <ExsRenderSystem/Memory/RSMemorySystem.h>
#include <ExsRenderSystem/Memory/DeviceMemory.h>


namespace Exs
{


	RSMemorySystem::RSMemorySystem(RenderSystem* renderSystem)
	: _renderSystem(renderSystem)
	{ }


	RSMemorySystem::~RSMemorySystem()
	{ }


	Result RSMemorySystem::Reset()
	{
		return ExsResult(RSC_Success);
	}


	RSMemoryHeapProperties RSMemorySystem::GetHeapRequirements(const RSResourceMemoryAllocationDesc& allocationDesc)
	{
		RSMemoryHeapProperties heapProperties;
		heapProperties.flags = 0;

		if (allocationDesc.allocationTarget == RSMemoryAllocationTarget::Buffer)
		{
			heapProperties.flags.set(RSMemoryHeapProperty_Resource_Type_Only_Buffers);
		}
		else if (allocationDesc.allocationTarget == RSMemoryAllocationTarget::Texture)
		{
			if (!allocationDesc.targetInfo.texture.isRTDSTexture)
				heapProperties.flags.set(RSMemoryHeapProperty_Resource_Type_Only_Non_RTDS_Textures);
			else
				heapProperties.flags.set(RSMemoryHeapProperty_Resource_Type_Only_RTDS_Textures);

			if (allocationDesc.targetInfo.texture.isMSAAEnabled)
				heapProperties.flags.set(RSMemoryHeapProperty_Ext_MSAA_Compatible);
		}
		
		if (allocationDesc.resourceUsageFlags.is_set(RSResourceUsage_Dynamic))
			heapProperties.flags.set(RSMemoryHeapProperty_Usage_Dynamic);
		else
			heapProperties.flags.set(RSMemoryHeapProperty_Usage_Default);

		return heapProperties;
	}


	bool RSMemorySystem::CheckHeapCompatibility(const RSMemoryHeapProperties& heapProperties, const RSMemoryHeapProperties& requiredProperties)
	{
		if (!heapProperties.flags.is_set(requiredProperties.flags & RSMemoryHeapProperty_Resource_Type))
			return false;

		if (!heapProperties.flags.is_set(requiredProperties.flags & RSMemoryHeapProperty_Ext))
			return false;

		if (!heapProperties.flags.is_set(requiredProperties.flags & RSMemoryHeapProperty_Usage))
			return false;

		return true;
	}


	// Uint32 RSMemorySystem::CheckPoolCompatibility(const RSMemoryPoolConfig& poolConfig, const RSMemoryAllocationDesc& allocationDesc)
	// {
	// 	stdx::mask<RSMemoryCompatibilityFlags> compatibilityMask = RSMemoryCompatibility_Resource_Category_Mask;
	// 
	// 	stdx::mask<AccessModeFlags> accessModeFlags = static_cast<AccessModeFlags>(allocationDesc.access);
	// 	if (accessModeFlags.is_set(AccessMode_Write) && !poolConfig.memoryAccessFlags.is_set(AccessMode_Write))
	// 		return 0;
	// 	if (accessModeFlags.is_set(AccessMode_Read) && !poolConfig.memoryAccessFlags.is_set(AccessMode_Read))
	// 		return 0;
	// 
	// 	Uint32 alignmentValue = static_cast<Uint32>(allocationDesc.alignment);
	// 	if (poolConfig.memoryAlignment < alignmentValue)
	// 		return 0;
	// 
	// 	Uint32 resourceCategoryMaskValue = allocationDesc.resourceCategoryMask;
	// 	if (!poolConfig.resourceCategoryMask.is_set(resourceCategoryMaskValue))
	// 		return 0;
	// 
	// 	if (poolConfig.memoryAccessFlags == accessModeFlags)
	// 		compatibilityMask.set(RSMemoryCompatibility_Access_Perfect);
	// 	else
	// 		compatibilityMask.set(RSMemoryCompatibility_Access_More_Relaxed);
	// 
	// 	if (poolConfig.memoryAlignment == alignmentValue)
	// 		compatibilityMask.set(RSMemoryCompatibility_Alignment_Perfect);
	// 	else
	// 		compatibilityMask.set(RSMemoryCompatibility_Alignment_Greater);
	// 	
	// 	compatibilityMask.unset(~(poolConfig.resourceCategoryMask & resourceCategoryMaskValue));
	// 
	// 	return compatibilityMask;
	// }


	/*DeviceMemoryBlock* RSMemorySystem::_AllocateHeapMemory(RS_memory_size_t memSize, const DeviceMemoryConfiguration& configuration)
	{
		Array<DeviceMemory*> deviceMemoryList = this->_GetCompatibleDeviceMemory(configuration);
		DeviceMemoryBlock* memoryBlock = nullptr;

		for (Size_t n = 0; n < deviceMemoryList.Size(); ++n)
		{
			DeviceMemory* deviceMemory = deviceMemoryList[n];

			// Check if memory has enough free space for this allocation.
			if (deviceMemory->GetFreeMemorySize() < memSize)
				continue;

			// Lock memory. Physical device memory can be accessed from multiple threads.
			ExsEnterCriticalSection(deviceMemory->GetLock());
			{
				if (deviceMemory->GetFreeMemorySize() >= memSize)
					memoryBlock = deviceMemory->AllocateMemoryBlock(memSize);
			}
			ExsLeaveCriticalSection();

			if (memoryBlock != nullptr)
				break;
		}

		return memoryBlock;
	}


	Array<DeviceMemory*> RSMemorySystem::_GetCompatibleDeviceMemory(const DeviceMemoryConfiguration& configuration)
	{
		Array<DeviceMemory*> heapList { };

		for (Size_t n = 0; n < this->_deviceMemoryObjects.Size(); ++n)
		{
			auto& deviceMemory = this->_deviceMemoryObjects[n];
			auto& memoryConfig = deviceMemory->GetConfiguration();

			if (memoryConfig.memoryAccess.cpua < configuration.memoryAccess.cpua)
				continue;

			if (memoryConfig.memoryAccess.gpua < configuration.memoryAccess.gpua)
				continue;

			if (memoryConfig.memoryProperties < configuration.memoryProperties)
				continue;

			if (memoryConfig.baseAlignment > configuration.baseAlignment)
				continue;

			if (memoryConfig.minAllocSize > configuration.minAllocSize)
				continue;

			heapList.PushBack(deviceMemory.GetPtr());
		}

		return heapList;
	}


	RSMemoryHeap* RSMemorySystem::_SelectHeapForPool(RSMemoryPoolDescription& poolDesc)
	{
		return nullptr;
	}


	RSMemoryPool* RSMemorySystem::_CreatePool(RSMemoryPoolDescription& poolDesc)
	{
		RSMemoryHeap* sourceHeap = poolDesc.sourceHeap;
		ExsDebugAssert( sourceHeap != nullptr );

		RSMemoryHeapBlock heapBlock { };
		Result allocResult = sourceHeap->AllocateMemoryBlock(truncate_cast<RS_memory_size_t>(poolDesc.size), &heapBlock);

		if (allocResult != RSC_Success)
			return nullptr;

		auto memoryPool = MakeSharedObject<RSMemoryPool>(sourceHeap, &heapBlock, 0, poolDesc.size);
		RSMemoryPool* memoryPoolPtr = memoryPool.GetPtr();

		Result regResult = sourceHeap->RegisterPool(0, memoryPoolPtr, &heapBlock);

		if (regResult != RSC_Success)
			return nullptr;

		this->_rsPools.PushBack(memoryPool);

		return memoryPoolPtr;
	}*/


}
