
#include <ExsRenderSystem/GraphicsDriverPlugin.h>


namespace Exs
{


	GraphicsDriverPlugin::GraphicsDriverPlugin(GraphicsDriverPluginManager* manager)
	: Plugin(manager)
	{ }


	GraphicsDriverPlugin::~GraphicsDriverPlugin()
	{ }




	GraphicsDriverPluginManager::GraphicsDriverPluginManager(PluginSystem* pluginSystem)
	: PluginManager(pluginSystem, PluginType_GraphicsDriver, "GraphicsDriverPluginManager")
	{ }


	GraphicsDriverPluginManager::~GraphicsDriverPluginManager()
	{ }


	void GraphicsDriverPluginManager::OnRegisterPlugin(Plugin* plugin)
	{ }


	void GraphicsDriverPluginManager::OnUnregisterPlugin(Plugin* plugin)
	{ }


}
