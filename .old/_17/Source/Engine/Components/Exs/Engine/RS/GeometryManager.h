
#ifndef __Exs_Engine_GeometryManager_H__
#define __Exs_Engine_GeometryManager_H__

#include "GeometryBase.h"


namespace Exs
{

	
	ExsDeclareRSClassObjHandle(IndexBuffer);
	ExsDeclareRSClassObjHandle(VertexBuffer);
	ExsDeclareRSClassObjHandle(VertexArrayStateObject);

	
	///<summary>
	/// This class provides common functionality used to implement geometry management. Every manager
	/// contains one or more storages for vertices and an optional index buffer storage (for indexed
	/// geometry data). Geometry manager also holds VertexArrayStateObject created for its internal
	/// buffer layout.
	///</summary>
	///<remarks>
	/// Buffers used to store the geometry may be either bound (possibly within a certain subrange)
	/// or new buffers can be created (see GeometryStorage for details). Creation of those buffers
	/// is the task of concrete manager sub-class. For example, every Base2DRectGeometryManager owns
	/// at most two buffers - required vertex buffer and an optional index buffer.
	///</remarks>
	class EXS_LIBCLASS_ENGINE GeometryManager
	{
	protected:
		// Used in child classes to store basic info about a single geometry buffer.
		struct GeometryBufferState
		{
			GeometryStorage* bufferStorage = nullptr;

			Uint32 elementSize = 0;

			Uint32 elementsNum = 0;

			Uint32 elementsNumLimit = 0;
		};
		
		typedef std::vector<GeometryBufferState> VertexBufferStateArray;

	private:
		struct IndexBufferRefInfo
		{
			// Handle to the index buffer.
			IndexBufferHandle buffer;

			// Size of a single index in the buffer.
			Size_t elementSize;

			// Storage which manages owned subrange of the buffer.
			std::unique_ptr<GeometryStorage> storage;
		};

		struct VertexBufferRefInfo
		{
			// Handle to the vertex buffer.
			VertexBufferHandle buffer;

			// Size of a single vertex in the buffer.
			Size_t elementSize;

			// Stream index which uses the buffer.
			Uint32 index;

			// Storage which manages owned subrange of the buffer.
			std::unique_ptr<GeometryStorage> storage;
		};
		
		typedef std::vector<VertexBufferRefInfo> VertexBufferRefInfoArray;

	private:
		RenderSystem*                 _renderSystem;
		IndexBufferRefInfo            _ibRefInfo;
		VertexBufferRefInfoArray      _vbRefInfoArray;
		VertexArrayStateObjectHandle  _vertexArrayStateObject;

	public:
		GeometryManager(RenderSystem* renderSystem, Uint32 vbnumHint = 0);
		~GeometryManager();
		
		///<summary>
		///</summary>
		void BeginUpdate(GeometryUpdateMode updateMode);
		
		///<summary>
		///</summary>
		void EndUpdate();
		
		///<summary>
		///</summary>
		void FlushUpdateCache();

		const VertexArrayStateObjectHandle& GetVertexArrayStateObject() const;

		Size_t GetVertexBuffersNum() const;

		bool HasIndexBuffer() const;

	protected:
		// Creates storage object for specified index buffer and binds it to this manager.
		bool BindIndexBuffer(const IndexBufferHandle& indexBuffer, const RSMemoryRange& bindRange, IndexBufferDataType indexDataType, GeometryBufferState* state);
		
		// Creates storage object for specified vertex buffer and binds it to this manager at IAVS index 'index'.
		bool BindVertexBuffer(Uint32 index, const VertexBufferHandle& vertexBuffer, const RSMemoryRange& bindRange, Size_t vertexSize, GeometryBufferState* state);
		
		// Creates new index buffer and storage object for it and binds it to this manager.
		bool CreateIndexBuffer(const IndexBufferCreateInfo& createInfo, Size_t bufferElementCapacity, IndexBufferDataType indexDataType, GeometryBufferState* state);
		
		// Creates new vertex buffer and storage object for it and binds it to this manager at IAVS index 'index'.
		bool CreateVertexBuffer(Uint32 index, const VertexBufferCreateInfo& createInfo, Size_t bufferElementCapacity, Size_t vertexSize, GeometryBufferState* state);

		// Initializes VASO for bound buffers. After this is done, further buffers may not be bound.
		void CreateVertexArrayStateObject();

	private:
		//
		void _InitializeIndexBuffer(const IndexBufferHandle& indexBuffer, const RSMemoryRange& bindRange, IndexBufferDataType indexDataType, GeometryBufferState* state);

		//
		void _InitializeVertexBuffer(Uint32 index, const VertexBufferHandle& vertexBuffer, const RSMemoryRange& bindRange, Size_t vertexSize, GeometryBufferState* state);

		//
		bool _IsVertexBufferPresent(Uint32 index) const;
	};
	

	inline const VertexArrayStateObjectHandle& GeometryManager::GetVertexArrayStateObject() const
	{
		return this->_vertexArrayStateObject;
	}

	inline Size_t GeometryManager::GetVertexBuffersNum() const
	{
		return this->_vbRefInfoArray.size();
	}
	
	inline bool GeometryManager::HasIndexBuffer() const
	{
		return this->_ibRefInfo.buffer ? true : false;
	}


}


#endif /* __Exs_Engine_GeometryManager_H__ */
