
#ifndef __Exs_Engine_GeometryStorage_H__
#define __Exs_Engine_GeometryStorage_H__

#include "GeometryData.h"
#include <stdx/memory_buffer.h>


namespace Exs
{


	class GeometryStorageUpdateCache
	{
	private:
		struct UpdateBatch
		{
			// Offset from the start of the target buffer.
			Uint bufferOffset;

			// Offset from the beginning of the cache.
			Uint cacheOffset;

			// Size of the data.
			Size_t dataSize;
		};

		typedef stdx::dynamic_memory_buffer<Byte> DataBuffer;
		typedef std::vector<UpdateBatch> UpdateList;

	private:
		DataBuffer    _dataBuffer;
		Size_t        _dataBufferReservedSize;
		UpdateList    _updateList;

	public:
		GeometryStorageUpdateCache()
		: _dataBufferReservedSize(0)
		{ }

		template <class Result_t>
		Result_t* Reserve(Size_t dataSize, Uint bufferOffset)
		{
			if (this->_dataBufferReservedSize + dataSize > this->_dataBuffer.size())
				this->_dataBuffer.expand(this->_dataBufferReservedSize + dataSize, 1.8f);

			Byte* basePtr = this->_dataBuffer.data_offset_ptr(this->_dataBufferReservedSize);
			this->_updateList.push_back(UpdateBatch { bufferOffset, this->_dataBufferReservedSize, dataSize });
			this->_dataBufferReservedSize += dataSize;

			return reinterpret_cast<Result_t*>(basePtr);
		}

		void Reset()
		{
			this->_updateList.clear();
			this->_dataBufferReservedSize = 0;
		}

		const Byte* GetBatchMemory(const UpdateBatch& batch) const
		{
			return this->_dataBuffer.data_offset_ptr(batch.cacheOffset);
		}

		const UpdateList& GetUpdateList() const
		{
			return this->_updateList;
		}

		bool IsEmpty() const
		{
			return this->_updateList.empty();
		}
	};

	
	///<summary>
	/// Provides common functionality for buffer storage management. Every storage object owns a subrange
	/// of a geometry buffer (vertex or index buffer), specified at the creation time. It uses this space
	/// to allocate memory for geometry data. Storage objects are also responsible for optimized geometry
	/// updates by providing explicit control of update modes, begin/end update blocks and internal cache
	/// for non-mappable buffers.
	///</summary>
	class EXS_LIBCLASS_ENGINE GeometryStorage
	{
	protected:
		// Represents internal state of a buffer.
		struct BufferState
		{
			// Buffer object.
			GeometryBufferHandle buffer;

			// Range within the buffer used by this geometry.
			RSMemoryRange bufferRange;

			// Size of the buffer currently in use.
			Size_t reservedSpace = 0;
		};

	protected:
		RenderSystem*                 _renderSystem;
		GeometryBufferHandle          _buffer;
		RSMemoryRange                 _bufferRange;
		Size_t                        _reservedStorageSize;
		GeometryStorageUpdateCache    _updateCache;
		GeometryUpdateMode            _updateMode;

	public:
		GeometryStorage(RenderSystem* renderSystem, const GeometryBufferHandle& buffer, const RSMemoryRange& bufferRange);
		virtual ~GeometryStorage();
		
		///<summary>
		/// Allocates space within the managed buffer range and saves it in 'reservedMemory'. Returns true on success. If there
		/// is not enough memory, false is returned. If 'reservedMemory' is NULL, the result indicates whether reservation would
		/// succeeded, but no memory is allocated.
		///</summary>
		bool Allocate(Size_t dataSize, GeometryMemory* reservedMemory);
		
		///<summary>
		/// Indicates the beginning of multiple update commands. Allows storage to take additional steps, which may significantly
		/// improve update performance (for example, by mapping owned range of a mappable buffer). If another BeginUpdate() call
		/// was made before without matching EndUpdate(), the behavior is undefined.
		///</summary>
		///
		///<param name="updateMode">
		/// Specifies the nature of the update process. Allowed values are: 'Append' (writing to an unused region of the storage),
		/// 'Modify' (writing to the region owned by an existing geometry) and 'Unspecified' (writing to an unspecified region).
		///</param>
		void BeginUpdate(GeometryUpdateMode updateMode);
		
		///<summary>
		/// Indicates the end of multiple update commands. This is a matching function for BeginUpdate() and must be called before
		/// next BeginUpdate(). If this function is not called before storage is used in rendering process, the behavior is undefined.
		///</summary>
		void EndUpdate();
		
		///<summary>
		/// Flushes cache containing dynamic updates to regions of the storage which were not mapped at the time of UpdateDynamic() call. 
		///</summary>
		void FlushDynamicUpdateCache();
		
		///<summary>
		/// Returns write-only pointer to the beginning of specified geometry memory.
		///</summary>
		///
		///<remarks>
		/// Note, that call to this function is perfectly valid even if BeginUpdate() has been called with 'Append' mode or not called
		/// at all. In such case, pointer to a temporary, cached memory is returned and all modifications are not visible until the next
		/// call to FlushDynamicUpdateCache().
		/// Performance warning: returned pointer should be used strictly as write-only pointer. Nothing forbids you from reading from it,
		/// but if specified range is mapped in write-only mode, it may cause serious performance issues.
		///</remarks>
		void* UpdateDynamic(const GeometryMemory& memory);
		
		///<summary>
		/// Returns write-only pointer to the beginning of a subrange within specified geometry memory.
		///</summary>
		///
		///<remarks>
		/// Note, that call to this function is perfectly valid even if BeginUpdate() has been called with 'Append' mode or not called
		/// at all. In such case, pointer to a temporary, cached memory is returned and any modifications are not visible until the next
		/// call to FlushDynamicUpdateCache().
		/// Performance warning: returned pointer should be used strictly as write-only pointer. Nothing forbids you from reading from it,
		/// but if specified range is mapped in write-only mode, it may cause serious performance issues.
		///</remarks>
		void* UpdateDynamic(const GeometryMemory& memory, const RSMemoryRange& subrange);
		
		///<summary>
		/// Updates specified geometry memory by sending data directly to the proper range of a geometry buffer.
		///</summary>
		void UpdateImmediate(const GeometryMemory& memory, const DataDesc& dataDesc);
		
		///<summary>
		/// Updates specified subrange of geometry memory by sending data directly to the proper range of a geometry buffer.
		///</summary>
		void UpdateImmediate(const GeometryMemory& memory, const RSMemoryRange& subrange, const DataDesc& dataDesc);

		bool CheckFreeSpace(Size_t dataSize) const;

		RenderSystem* GetRenderSystem() const;

		Size_t GetFreeSpace() const;
		
		bool IsMemoryMapped(const GeometryMemory& memory) const;
		
		bool IsMemoryMapped(const GeometryMemory& memory, const RSMemoryRange& subrange) const;

		bool IsInitialized() const;
	};


	inline void* GeometryStorage::UpdateDynamic(const GeometryMemory& memory)
	{
		return this->UpdateDynamic(memory, RSMemoryRange(0, memory.bufferRange.length));
	}

	inline void GeometryStorage::UpdateImmediate(const GeometryMemory& memory, const DataDesc& dataDesc)
	{
		return this->UpdateImmediate(memory, RSMemoryRange(0, memory.bufferRange.length), dataDesc);
	}

	inline bool GeometryStorage::IsMemoryMapped(const GeometryMemory& memory) const
	{
		return this->IsMemoryMapped(memory, RSMemoryRange(0, memory.bufferRange.length));
	}

	inline bool GeometryStorage::CheckFreeSpace(Size_t dataSize) const
	{
		return this->GetFreeSpace() >= dataSize;
	}

	inline RenderSystem* GeometryStorage::GetRenderSystem() const
	{
		return this->_renderSystem;
	}

	inline Size_t GeometryStorage::GetFreeSpace() const
	{
		return this->_bufferRange.length - this->_reservedStorageSize;
	}

	inline bool GeometryStorage::IsInitialized() const
	{
		return this->_buffer && (this->_bufferRange.length > 0);
	}


}


#endif /* __Exs_Engine_GeometryStorage_H__ */
