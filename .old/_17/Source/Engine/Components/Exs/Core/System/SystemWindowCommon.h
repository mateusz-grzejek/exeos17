
#ifndef __Exs_Core_SystemWindowCommon_H__
#define __Exs_Core_SystemWindowCommon_H__

#include "AppCommon.h"


namespace Exs
{


	ExsDeclareRefPtrClass(SystemWindow);

	
	///<summary>
	///</summary>
	enum class SystemWindowStyle : Enum
	{
		Fullscreen,

		Normal,

		Normal_No_Resize,

		Default = Normal_No_Resize,
	};


	///<summary>
	///</summary>
	struct SystemWindowFrameDesc
	{
		// Position of the window in pixels, (0,0) being a top-left corner.
		Position position;

		// Size of the window. It specifies either the size of the whole frame or client area only.
		Size size;

		// Title of the window, displayed on its title bar (it it has one!).
		std::string title;

		// Style of window's frame. Used only on Win32 and Linux platforms.
		SystemWindowStyle style = SystemWindowStyle::Default;
	};


	EXS_LIBAPI_CORE SystemWindowRefPtr CreateSystemWindow(AppGlobalStateRefHandle appGlobalState, const SystemWindowFrameDesc& frameDesc);


}


#endif /* __Exs_Core_SystemWindowCommon_H__ */
