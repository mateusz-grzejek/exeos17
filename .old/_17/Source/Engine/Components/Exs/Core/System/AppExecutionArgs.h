
#ifndef __Exs_Core_AppExecutionArgs_H__
#define __Exs_Core_AppExecutionArgs_H__

#include "../Prerequisites.h"
#include <stdx/assoc_array.h>
#include <stdx/variant.h>


namespace Exs
{


	namespace Internal
	{

		template <class T>
		struct AppExecArgDefaultParser;

		template <class T>
		struct AppExecArgHelper;

		template <class T>
		struct AppExecArgParser
		{
			typedef AppExecArgDefaultParser<T> Type;
		};

	};


	///<summary>
	///</summary>
	template <class T>
	struct AppStartupArgDef
	{
		const char* name;
		T defaultValue;
	};


	template <class... Types>
	class AppStartupArg
	{
	public:
		typedef stdx::variant<Types...> DataType;

	private:
		DataType _data;

	public:
		AppStartupArg( AppStartupArg&& ) = default;
		AppStartupArg& operator=( AppStartupArg&& ) = default;

		AppStartupArg( const Arg& ) = default;
		AppStartupArg& operator=( const Arg& ) = default;

		template <class T>
		AppStartupArg( T&& value )
			: _data( std::move( value ) )
		{ }

		template <class T>
		AppStartupArg( const T& value )
			: _data( value )
		{ }

		template <class T>
		T& As()
		{
			return this->_data.get<T>();
		}

		template <class T>
		const T& As() const
		{
			return this->_data.get<T>();
		}

		template <class T>
		void Set( T&& value )
		{
			return this->_data.set<T>( std::move( value ) );
		}

		template <class T>
		void Set( const T& value )
		{
			return this->_data.set<T>( value );
		}
	};


	template <class Arg>
	class AppStartupArgList
	{
	private:
		stdx::assoc_array<std::string, Arg> _internalArray;

	public:
		template <class T>
		Arg& Add( const char* name, T&& defaultValue = T() )
		{
			auto argRef = this->_internalArray.insert( name, std::move( defaultValue ) );
			return *argRef;
		}

		template <class T>
		Arg& Add( const char* name, const T& defaultValue )
		{
			auto argRef = this->_internalArray.insert( name, defaultValue );
			return *argRef;
		}

		template <class T>
		Arg& Add( AppStartupArgDef<T>&& argDef )
		{
			auto argRef = this->_internalArray.insert( argDef.name, std::move( argDef.defaultValue ) );
			return *argRef;
		}

		template <class T>

		Arg& Add( const AppStartupArgDef<T>& argDef )
		{
			auto argRef = this->_internalArray.insert( argDef.name, argDef.defaultValue );
			return *argRef;
		}

		template <class T>
		void Set( std::string&& name, const std::string& value )
		{
			auto argRef = this->_internalArray.insert( std::move( name ), std::forward<T>( defaultValue ) );
			return *argRef;
		}

		template <class T>
		void Set( const std::string& name, const T& defaultValue )
		{
			auto argRef = this->_internalArray.insert( name, std::forward<T>( defaultValue ) );
			return *argRef;
		}

		template <class T>
		T& Get( const char* name )
		{

		}

		template <class T>
		const T& Get( const char* name ) const;

		Size_t Size() const;

		bool IsSet( const char* name ) const;
	};


	template <class Arg>
	class ArgParser
	{
	private:
		typedef std::function<void( const std::string&, Arg& )> ParseCallback;

	public:
		template <class T>
		void AddArgDefinition( const ArgDefinition<T>& paramDef )
		{ }

		template <class T, class P, class... Args>
		void AddArgDefinition( const ArgDefinition<T>& paramDef, P parseFunc, Args&&... args )
		{ }
	};


	namespace Internal
	{

		template <>
		struct AppExecArgDefaultParser<bool>
		{
			bool operator()( const std::string& str ) const
			{
				if ( !str.empty() && ( ( str[0] == '1' ) || ( str == "true" ) || ( str == "True" ) || ( str == "TRUE" ) ) )
					return true;
				return false;
			}
		};

		template <>
		struct AppExecArgDefaultParser<Int32>
		{
			Int32 operator()( const std::string& str ) const
			{
				auto result = stdx::string_to_integer<Int32>( str );
				return result.first;
			}
		};

		template <>
		struct AppExecArgDefaultParser<Int64>
		{
			Int64 operator()( const std::string& str ) const
			{
				auto result = stdx::string_to_integer<Int64>( str );
				return result.first;
			}
		};

		template <>
		struct AppExecArgDefaultParser<std::string>
		{
			std::string operator()( const std::string& str ) const
			{
				return str;
			}
		};

		template <>
		struct AppExecArgDefaultParser<Position>
		{
			Position operator()( const std::string& str ) const
			{
			}
		};

		template <typename Val_t>
		struct AppExecArgDefaultParser< Range<Val_t> >
		{
			Range<Val_t> operator()( const std::string& str ) const
			{
				size_t start = ((str ) )
			}
		};

		template <>
		struct AppExecArgDefaultParser<Size>
		{
			Size operator()( const std::string& str ) const
			{ }
		};

		template <typename Val_t>
		struct AppExecArgDefaultParser< Version<Val_t> >
		{
			Version<Val_t> operator()( const std::string& str ) const
			{ }
		};

	}


}


#endif /* __Exs_Core_AppExecutionArgs_H__ */
