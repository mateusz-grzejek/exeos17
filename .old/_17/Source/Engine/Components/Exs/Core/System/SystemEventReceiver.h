
#ifndef __Exs_Core_SystemEventReceiver_H__
#define __Exs_Core_SystemEventReceiver_H__

#include "SystemEventBase.h"


namespace Exs
{


	class SystemEventDispatcher;


	enum SystemEventHandlerStateFlags : Enum
	{
		SystemEventHandlerState_Active = 0x0001
	};


	///<summary>
	///</summary>
	class EXS_LIBCLASS_CORE SystemEventReceiver
	{
		friend class SystemEventDispatcher;

	public:
		enum StateFlags : Enum
		{
			//
			State_Active = 0x0001,

			//
			State_Default = 0
		};

		enum HandlerStateFlags : Enum
		{
			//
			HandlerState_Active = 0x0001,

			//
			HandlerState_Default = HandlerState_Active
		};

		struct HandlerInfo
		{
			SystemEventHandler handler;
			stdx::mask<HandlerStateFlags> flags;
		};

		typedef std::array<HandlerInfo, System_Event_Codes_Num> EventHandlers;

	private:
		EventHandlers             _handlersArray;
		SystemEventDispatcher*    _dispatcher;
		stdx::mask<StateFlags>    _state;

	public:
		SystemEventReceiver();
		virtual ~SystemEventReceiver();

		bool PostEvent( const SystemEvent& event );

		void RegisterHandler( SystemEventCode eventCode, SystemEventHandler&& handler );
		void RegisterHandler( SystemEventCode eventCode, const SystemEventHandler& handler );

		void SetHandlerActiveState( SystemEventCode eventCode, bool active );

		const SystemEventHandler& GetHandlerByEventCode( SystemEventCode eventCode ) const;

		bool IsActive() const;
		bool IsHandlerRegistered( SystemEventCode eventCode ) const;

	friendapi:
		// Activates this receiver. This method is called by a dispatcher when a receiver object is set.
		bool Activate( SystemEventDispatcher* dispatcher );

		// Deactivates this receiver. This method is called by a dispatcher when a receiver object is replaced with another one.
		void Deactivate( SystemEventDispatcher* dispatcher );

	private:
		//
		virtual bool OnActivate( SystemEventDispatcher* dispatcher );

		//
		virtual void OnDeactivate( SystemEventDispatcher* dispatcher );
	};


	inline void SystemEventReceiver::SetHandlerActiveState( SystemEventCode eventCode, bool active )
	{
		auto& handlerInfo = this->_handlersArray[ExsEnumGetSystemEventIndex( eventCode )];
		active ? handlerInfo.flags.set( HandlerState_Active ) : handlerInfo.flags.unset( HandlerState_Active );
	}

	inline const SystemEventHandler& SystemEventReceiver::GetHandlerByEventCode( SystemEventCode eventCode ) const
	{
		auto& eventHandlerInfo = this->_handlersArray[ExsEnumGetSystemEventIndex( eventCode )];
		return eventHandlerInfo.handler;
	}

	inline bool SystemEventReceiver::IsActive() const
	{
		return this->_state.is_set( State_Active );
	}

	inline bool SystemEventReceiver::IsHandlerRegistered( SystemEventCode eventCode ) const
	{
		const auto& handler = this->GetHandlerByEventCode( eventCode );
		return handler ? true : false;
	}


}


#endif /* __Exs_Core_SystemEventReceiver_H__ */
