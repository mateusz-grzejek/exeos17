
#include <Exs/Core/System/AppCommon.h>


namespace Exs
{


	struct CoreAppSystemState::InitData
	{
		std::unique_ptr<AppObject> appObject;
		std::unique_ptr<SystemEventDispatcher> eventDispatcher;

		InitData() = default;
		InitData( InitData && ) = default;
	};


	CoreAppSystemState::CoreAppSystemState( InitData && initData )
		: _appObject(std::move(initData.appObject))
		, _eventDispatcher( std::move( initData.eventDispatcher ) )
		, appObject( _appObject.get() )
		, eventDispatcher( _eventDispatcher.get() )
		, systemEnvData( &_systemEnvData )
		, executionState( &_executionState )
	{ }


	CoreAppSystemState::~CoreAppSystemState() = default;




	SharedHandle<CoreAppSystemState> CoreAppSystemStateBuilder::CreateState()
	{

	}


	void CoreAppSystemStateBuilder::OnInitSystemEnvData( SystemEnvData* systemEnvData )
	{

	}


	std::unique_ptr<AppObject> CoreAppSystemStateBuilder::CreateAppObject()
	{

	}


	std::unique_ptr<SystemEventDispatcher> CoreAppSystemStateBuilder::CreateSystemEventDispatcher()
	{

	}


	void CoreAppSystemStateBuilder::InitializeSystemEnvData( SystemEnvData* systemEnvData )
	{

	}


}
