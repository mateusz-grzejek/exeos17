
#ifndef __Exs_Core_EvtSysApp_H__
#define __Exs_Core_EvtSysApp_H__

#include "SystemEvent.h"


namespace Exs
{

	
	///<summary>
	///</summary>
	class EvtSysAppCommand : public EvtSysApplication
	{
	public:
		EvtSysAppCommand(SystemEventCode code)
		: EvtSysApplication(code)
		{
			ExsDebugAssert( ExsEnumGetSystemEventCategory(code) == SystemEventCategory::App_Command );
		}
	};

	
	///<summary>
	///</summary>
	class EvtSysAppCommandInitialize : public EvtSysAppCommand
	{
	public:
		EvtSysAppCommandInitialize()
		: EvtSysAppCommand(SystemEventCode::App_Command_Initialize)
		{ }
	};

	
	///<summary>
	///</summary>
	class EvtSysAppCommandTerminate : public EvtSysAppCommand
	{
	public:
		EvtSysAppCommandTerminate()
		: EvtSysAppCommand(SystemEventCode::App_Command_Terminate)
		{ }
	};

	
	///<summary>
	///</summary>
	class EvtSysAppCommandFocusGained : public EvtSysAppCommand
	{
	public:
		EvtSysAppCommandFocusGained()
		: EvtSysAppCommand(SystemEventCode::App_Command_Focus_Gain)
		{ }
	};

	
	///<summary>
	///</summary>
	class EvtSysAppCommandFocusLost : public EvtSysAppCommand
	{
	public:
		EvtSysAppCommandFocusLost()
		: EvtSysAppCommand(SystemEventCode::App_Command_Focus_Lost)
		{ }
	};


}


#endif /* __Exs_Core_EvtSysApp_H__ */
