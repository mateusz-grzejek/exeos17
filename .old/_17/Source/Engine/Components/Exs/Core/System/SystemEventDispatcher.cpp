
#include <Exs/Core/System/SystemEventDispatcher.h>
#include <Exs/Core/System/SystemEventReceiver.h>


namespace Exs
{


	SystemEventDispatcher::SystemEventDispatcher()
	: _eventReceiver(nullptr)
	{ }


	SystemEventDispatcher::~SystemEventDispatcher()
	{ }


	bool SystemEventDispatcher::SetReceiver(SystemEventReceiver* eventReceiver, SystemEventReceiver** prevReceiver)
	{
		// This method should be always called via SystemEventDispatcher object. Make appropriate cast.
		SystemEventDispatcher* thisDispatcher = dbgsafe_ptr_cast<SystemEventDispatcher*>(this);

		// If receiver is not NULL, it must be able to properly activate itself.
		if ((eventReceiver != nullptr) && !eventReceiver->Activate(thisDispatcher))
			return false;

		if (this->_eventReceiver != nullptr)
			this->_eventReceiver->Deactivate(thisDispatcher);

		if (prevReceiver != nullptr)
			*prevReceiver = this->_eventReceiver;

		this->_eventReceiver = eventReceiver;

		return true;
	}


	bool SystemEventDispatcher::_InternalDispatch(const SystemEvent& event)
	{
		if (this->_eventReceiver == nullptr)
			return false;

		return this->_eventReceiver->PostEvent(event);
	}


}
