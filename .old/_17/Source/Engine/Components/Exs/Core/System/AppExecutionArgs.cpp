
#include <Exs/Core/System/AppExecutionArgs.h>


namespace Exs
{


	AppExecutionArgs::AppExecutionArgs()
	{ }


	AppExecutionArgs::~AppExecutionArgs()
	{ }


	bool AppExecutionArgs::AddArgDefinition(const char* name, AppExecutionArgType type)
	{
		if (type == AppExecutionArgType::Unknown)
			return false;

		auto argDataRef = this->_argsArray.find(name);
		if (argDataRef != this->_argsArray.end())
			return false;

		AppExecutionArgData argData { };
		argData.name = name;
		argData.type = type;

		this->_argsArray.insert(argData.name, std::move(argData));

		return true;
	}


	bool AppExecutionArgs::AddArgDefinition(const AppExecutionArgDef& argDef)
	{
		return this->AddArgDefinition(argDef.name, argDef.type);
	}


	void AppExecutionArgs::ParseArgsArray(const char** argv, Uint32 argc)
	{
		auto appArgsArrayView = BindArrayView(argv, argc);

		for (auto& appArgStr : appArgsArrayView)
		{
			Size_t appArgStrLen = (appArgStr != nullptr) ? std::char_traits<char>::length(appArgStr) : 0;

			if (appArgStrLen == 0)
				continue;

			if ((appArgStr[0] == '-') || (appArgStr[0] == '+'))
				++appArgStr;

			const char* separator = std::char_traits<char>::find(appArgStr, appArgStrLen, '=');
			Size_t separatorPos = (separator != nullptr) ? (separator - appArgStr) : appArgStrLen - 1;

			std::string argName { appArgStr, separatorPos };
			std::string argValue { separator + 1, appArgStrLen - (separatorPos + 1) };

			// Fetch arg data using [] operator. If registered, we will receive valid, initialized entry.
			// If not registered, it will be added automatically and will have type set to Unknown.
			auto& internalArgData = this->_argsArray[argName];

			switch(internalArgData.type)
			{
			case AppExecutionArgType::Boolean:
				{
					if (!argValue.empty() && (argValue[0] != '0') && (argValue[0] != 'f') && (argValue[0] != 'F'))
						internalArgData.content.boolean = true;
				}
				break;

			case AppExecutionArgType::String:
				{
					internalArgData.content.string = std::move(argValue);
				}
				break;

			case AppExecutionArgType::Value:
				{
					auto convResult = stdx::string_numeric_conv<char, Int32>::to_value(argValue);
					internalArgData.content.value = convResult.first;
				}
				break;

			case AppExecutionArgType::Unknown:
				{
					// Unknown type means, that arg definition was not specified. In this case, we treat argument as string literal.
					internalArgData.type = AppExecutionArgType::String;
					internalArgData.name = std::move(argName);
					internalArgData.content.string = std::move(argValue);
				}
			}
		}
	}


	void AppExecutionArgs::Reset()
	{
		this->_argsArray.clear();
	}


}
