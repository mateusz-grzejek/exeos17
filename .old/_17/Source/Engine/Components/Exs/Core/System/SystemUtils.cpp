
#include <Exs/Core/System/SystemUtils.h>


namespace Exs
{


	namespace System
	{


		FileHandle CreateFile(const char* path, FileCreateMode createMode, Enum accessMode)
		{
			EXS_UNUSED( accessMode );

			FILE* file = nullptr;

			if (createMode == FileCreateMode::Create_New)
			{
				file = fopen(path, "r");

				if (file)
				{
					fclose(file);
					return nullptr;
				}
			}

			file = fopen(path, "w+b");

			return file;
		}


		FileHandle OpenFile(const char* path, FileOpenMode openMode, Enum accessMode)
		{
			FILE* file = nullptr;
			stdx::mask<Enum> accessFlags = accessMode;

			if (openMode == FileOpenMode::Open_Existing)
			{
				if (accessFlags.is_set(AccessMode_Write))
					file = fopen(path, "r+b");
				else
					file = fopen(path, "rb");
			}
			else
			{
				file = fopen(path, "w+b");
			}

			return file;
		}


		FileHandle CreateTemporaryFile()
		{
			FILE* tmpFile = tmpfile();
			return tmpFile;
		}


		std::string GetTemporaryFilePath(const char* prefix)
		{
			std::string result { };
			char tmpName[L_tmpnam + 1];

			if (tmpnam(tmpName) != nullptr)
			{
				result += prefix;
				result += tmpName;
			}

			return result;
		}


		bool CheckExistence(const char* path)
		{
			int fileStatResult = -1;
			struct stat statInfo;
			fileStatResult = stat(path, &statInfo);

			return (fileStatResult == 0);
		}


		void CloseFile(FileHandle handle)
		{
			ExsDebugAssert( handle != nullptr );
			fclose(handle);
		}


		NativeDLLHandle LoadDynamicLibrary(const char* path)
		{
			NativeDLLHandle handle = nullptr;

		#if ( EXS_TARGET_SYSTEM & EXS_TARGET_PLATFORM_FLAG_WINDOWS )

			handle = LoadLibraryA(path);

		#elif ( EXS_TARGET_SYSTEM & EXS_TARGET_PLATFORM_FLAG_WINPHONE )

			auto wpath = StringUtils::ConvertAsciiToWide(path);
			handle = LoadPackagedLibrary(wpath.GetBuffer(), 0);

		#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_LINUX )

			handle = dlopen(path, RTLD_NOW | RTLD_GLOBAL);

			if (handle == nullptr)
			{
				if (char* dlerrorStr = dlerror())
					ExsTraceNotification(TRC_Core_System, "%s has failed to load. Error: %s.", path, dlerrorStr);
			}

		#endif

			return handle;
		}


		void* RetrieveDynamicLibrarySymbol(NativeDLLHandle handle, const char* symName)
		{
			void* symbolPtr = nullptr;

		#if ( EXS_TARGET_SYSTEM & (EXS_TARGET_PLATFORM_FLAG_WINDOWS | EXS_TARGET_PLATFORM_FLAG_WINPHONE) )

			symbolPtr = GetProcAddress(handle, symName);

		#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_LINUX )

			symbolPtr = dlsym(handle, symName);

		#endif

			return symbolPtr;
		}


		void UnloadDynamicLibrary(NativeDLLHandle handle)
		{
		#if ( EXS_TARGET_SYSTEM & (EXS_TARGET_PLATFORM_FLAG_WINDOWS | EXS_TARGET_PLATFORM_FLAG_WINPHONE) )

			FreeLibrary(handle);

		#elif ( EXS_TARGET_SYSTEM == EXS_TARGET_SYSTEM_LINUX )

			dlclose(handle);

		#endif
		}


	}


}
