

#ifndef __Exs_Core_SyncInternal_H__
#define __Exs_Core_SyncInternal_H__

#include "ConcurrentDataPool.h"
#include "SyncConditionVariable.h"


namespace Exs
{


	template <class Mutex>
	using SyncMutex = Mutex;


	///<summary>
	/// This class represents an internal context of execution used by SyncObjects. When thread Waits() on a SO,
	/// this SO acquires such context (this is SO-specific and depends on the max number of threads which can wait
	/// for the particular SO type) and uses it to control the synchronization process.
	///</summary>
	template <class Mutex>
	class SyncContext
	{
	private:
		enum StateFlags : Enum
		{
			//
			State_Active = 0x0001,

			// Indicates that sync was signaled normally.
			State_Notify = 0x0010,

			// Indicates that sync was interrupted.
			State_Interrupt = 0x0080,
		};

		// Internal condition variable.
		SyncConditionVariable<Mutex> _syncCv;

		// Internal flags
		stdx::atomic_mask<Enum> _internalFlags;

	public:
		// Thread synchronized with this context.
		Thread* thread = nullptr;

	public:
		//
		template <class Lock>
		void Wait( Lock& lock )
		{
			this->_syncCv.Wait( lock );
		}

		//
		template <class Lock, class Duration>
		std::cv_status WaitFor( Lock& lock, const Duration& duration )
		{
			return this->_syncCv.WaitFor( lock, duration );
		}

		//
		void NotifyThread()
		{
			this->_syncCv.NotifyOne();
		}

		// Clears internal state.
		void Reset()
		{
			this->_internalFlags.clear( std::memory_order_relaxed );
		}

		//
		void SetActive()
		{
			this->_internalFlags.set( State_Active, std::memory_order_relaxed );
		}

		// Sets flag which indicates that interruption has occured.
		void SetInterruptFlag()
		{
			this->_internalFlags.set( State_Interrupt, std::memory_order_relaxed );
		}

		// Sets flag which indicates that object was explicitly signaled.
		void SetNotifyFlag()
		{
			this->_internalFlags.set( State_Interrupt, std::memory_order_relaxed );
		}

		// Returns true if interrupt flag is set.
		bool IsActive() const
		{
			return this->_internalFlags.is_set( State_Active, std::memory_order_relaxed ) != 0;
		}

		// Returns true if interrupt flag is set.
		bool IsInterruptFlagSet() const
		{
			return this->_internalFlags.is_set( State_Interrupt, std::memory_order_relaxed ) != 0;
		}

		// Returns true if notify flag is set.
		bool IsNotifyFlagSet() const
		{
			return this->_internalFlags.is_set( State_Notify, std::memory_order_relaxed ) != 0;
		}
	};


	///<summary>
	/// Base class for sync objects. Sync object is a concept of synchronization primitve, which threads can wait on,
	/// also using timeouts and additional condition predicates. Sync object can be explicitly signaled, but also have
	/// the ability to interrupt waiting process of a single thread or even all threads waiting for a sync object.
	/// This base class provides only external client interface. Wait() functions are defined in concrete subclasses.
	///
	/// There are few types of sync objects:
	/// - Internal (InternalSyncObject): bound to a specific thread object, which is an owner
	///   of the SO. Only thread which is an owner can wait for its sync objects.
	/// - Unique (UniqueSyncObject): any thread can wait for a unique sync object, but only
	///   one at any given time. An attempt to wait on busy SO will result in WaitResult::Failure.
	/// - Shared (SharedSyncObject): sync object, which allows multiple threads to wait for
	///   it, even with different conditions and timeouts. Max number of threads which can wait
	///   at the same time is controlled by [CCRT_Shared_Sync_Object_Thread_Num_Limit] constant.
	///</summary>
	class SyncObject
	{
	public:
		SyncObject() = default;

		///<summary>
		///</summary>
		bool Interrupt( Thread* thread );

	private:
		virtual bool InternalNotify() = 0;
		virtual bool InternalBroadcast() = 0;
		virtual bool InternalInterrupt() = 0;
	};


	///<summary>
	/// Implements interface used to update thread state.
	///</summary>
	class ThreadSyncCommonInterface
	{
	protected:
		//
		static void UpdateThreadState( SyncObject*, Thread* )
		{ }

		//
		static void ResetThreadState( Thread* )
		{ }
	};


	///<summary>
	/// Interface provider for SharedSyncObject.
	///</summary>
	class CommonSyncObjectBase : public SyncObject
	{
	public:
		// Local constant for max number of threads that can wait for a shared sync object.
		static constexpr Size_t maxThreadsNum = Config::CCRT_Common_Sync_Object_Thread_Num_Limit;

	private:
		struct alignas(Config::HW_Destructive_Cache_Interference_Size) InternalSyncContext : public SyncContext<SystemMutex>
		{
			bool acquireFlag = false;
		};

	public:
		using MutexType = SystemMutex;
		using SyncContextType = InternalSyncContext;

		using ContextPool = std::array<SyncContextType, maxThreadsNum>;
		using ActiveContextQueue = std::deque<SyncContextType*>;

	private:
		ContextPool         _contextPool; // Pool of sync contexts.
		ActiveContextQueue  _activeContexts; // List of waiting threads.

	public:
		CommonSyncObjectBase();

		bool Notify( SystemMutexLock& lock );
		bool Broadcast( SystemMutexLock& lock );
		bool Interrupt( SystemMutexLock& lock );

	protected:
		SyncContextType* AcquireContext();
		void ReleaseContext( SyncContextType* syncContext );
		void OnWaitBegin( SyncContextType* syncContext );
		void OnWaitEnd( SyncContextType* syncContext );
	};


	///<summary>
	/// Interface provider for InternalSyncObjects. As ISO belongs to a specific thread and can be used only by
	/// that thread, its provider contains single wait context which is returned each time thread acquires context.
	///</summary>
	class InternalSyncObjectBase : public SyncObject
	{
	public:
		using MutexType = SystemMutex;
		using SyncContextType = SyncContext<SystemMutex>;

	private:
		Thread*          _owningThread;
		SyncContextType  _syncContext;

	public:
		InternalSyncObjectBase( Thread* owningThread );

		bool Notify( SystemMutexLock& lock );
		bool Broadcast( SystemMutexLock& lock );
		bool Interrupt( SystemMutexLock& lock );

	protected:
		SyncContextType* AcquireContext();
		void ReleaseContext( SyncContextType* syncContext );
		void OnWaitBegin( SyncContextType* syncContext );
		void OnWaitEnd( SyncContextType* syncContext );

	private:
		void _InitContext();
	};


	///<summary>
	/// Interface provider for UniqueSyncObjects. Contains single wait context which is returned each time thread
	/// acquires a context and a control flag used to 'reserve' this context for a single thread.
	///</summary>
	class UniqueSyncObjectBase : public SyncObject
	{
	public:
		using MutexType = SystemMutex;
		using SyncContextType = SyncContext<SystemMutex>;

	private:
		std::atomic_flag         _acquireFlag;
		Thread*                  _currentThread;
		SyncContext<SystemMutex> _syncContext;

	public:
		UniqueSyncObjectBase();

		bool Notify( SystemMutexLock& lock );
		bool Broadcast( SystemMutexLock& lock );
		bool Interrupt( SystemMutexLock& lock );

	protected:
		SyncContextType* AcquireContext();
		void ReleaseContext( SyncContextType* syncContext );
		void OnWaitBegin( SyncContextType* syncContext );
		void OnWaitEnd( SyncContextType* syncContext );
	};


	///<summary>
	/// Interface provider for SharedSyncObject.
	///</summary>
	class SharedSyncObjectBase : public SyncObject
	{
	public:
		// Local constant for max number of threads that can wait for a shared sync object.
		static constexpr Size_t maxThreadsNum = Config::CCRT_Common_Sync_Object_Thread_Num_Limit;

	private:
		struct alignas(Config::HW_Destructive_Cache_Interference_Size) InternalSyncContext : public SyncContext<SharedMutex>
		{
			bool reserveFlag;
		};

	public:
		using MutexType = SystemMutex;
		using SyncContextType = InternalSyncContext;

		using ContextPool = std::array<InternalSyncContext, maxThreadsNum>;
		using ActiveContextQueue = std::queue<size_t>;

	private:
		ContextPool         _contextPool; // Pool of sync contexts.
		ActiveContextQueue  _activeThreads; // List of waiting threads.

	public:
		SharedSyncObjectBase();

		bool Notify( std::unique_lock<SharedMutex>& lock );
		bool Broadcast( std::unique_lock<SharedMutex>& lock );
		bool Interrupt( std::unique_lock<SharedMutex>& lock );

	protected:
		SyncContextType* AcquireContext();
		void ReleaseContext( SyncContextType* syncContext );
		void OnWaitBegin( SyncContextType* syncContext );
		void OnWaitEnd( SyncContextType* syncContext );
	};


	///<summary>
	/// Implements core functionality of sync objects. This class is parametrized with SyncObjectBase type.
	/// It is a class, specific for a concrete SO subtype (internal/unique/shared) and provides two main functionalities:
	/// - context (SyncContext) management - example: iternal SO contains single conext which can be used only by
	///   the thread that owns this SO, while shared SO contains multiple context, possibly created on demand, which
	///   are assigned per waiting thread. This logic is exposed as a pair of AcquireContext()/ReleaseContext() methods.
	/// - 
	/// which is an interface specific to a concrete SO type. It provides implementation of context management,
	/// setting interrupt states or interaction with thread object states. With such design, all functionality is
	/// implemented here, resolved at compilation time and can be easily extended and/or modified. It is even
	/// possible for providers to use custom, extended wait context type (which is actually used in SharedSyncObject).
	///</summary>
	template <class SyncObjectBase>
	class SyncObjectCore : public SyncObjectBase, public ThreadSyncCommonInterface
	{
		EXS_DECLARE_NONCOPYABLE(SyncObjectCore);

	public:
		// Every provider must provide this typedef for context type it uses internally.
		using MutexType = typename SyncObjectBase::MutexType;

		// Every provider must provide this typedef for context type it uses internally.
		using SyncContextType = typename SyncObjectBase::SyncContextType;

	private:
		SyncMutex<MutexType> _syncMutex;

	public:
		template <typename... Args>
		SyncObjectCore(Args&&... args)
			: SyncObjectBase(std::forward<Args>(args)...)
		{ }

		//
		template <class Lock>
		WaitResult Wait(Lock& lock)
		{
			auto waitPredicate = [](SyncContext<MutexType>* syncContext) -> bool {
				// "Infinite" wait without predicate - can be either signaled or interrupted.
				return syncContext->IsNotifyFlagSet() || syncContext->IsInterruptFlagSet();
			};

			return this->InternalWait(lock, waitPredicate);
		}

		//
		template <class Lock, class Predicate>
		WaitResult Wait(Lock& lock, Predicate predicate)
		{
			auto waitPredicate = [&predicate](SyncContext<MutexType>* syncContext) -> bool {
				// "Infinite" wait with predicate - can be either signaled (predicate() == true) or interrupted.
				return (predicate() && syncContext->IsNotifyFlagSet()) || syncContext->IsInterruptFlagSet();
			};

			return this->InternalWait(lock, waitPredicate);
		}

		//
		template <class Lock, class Duration_t>
		WaitResult WaitFor(Lock& lock, const Duration_t& duration)
		{
			auto waitPredicate = [](SyncContext<MutexType>* syncContext) -> bool {
				// Timed wait without predicate - can be either signaled, interrupted or timeout can occur.
				return syncContext->IsNotifyFlagSet() || syncContext->IsInterruptFlagSet();
			};

			return this->InternalWaitFor(lock, duration, waitPredicate);
		}

		//
		template <class Lock, class Duration_t, class Predicate>
		WaitResult WaitFor(Lock& lock, const Duration_t& duration, Predicate predicate)
		{
			auto waitPredicate = [&predicate](SyncContext<MutexType>* syncContext) -> bool {
				// Timed wait with predicate - can be either signaled (predicate() == true), interrupted or timeout can occur.
				return (predicate() && syncContext->IsNotifyFlagSet()) || syncContext->IsInterruptFlagSet();
			};

			return this->InternalWaitFor(lock, duration, waitPredicate);
		}

		///
		SyncMutex<MutexType>& GetMutex()
		{
			return this->_mutex;
		}

	private:
		//
		template <class Lock, class Predicate>
		WaitResult InternalWait(Lock& lock, const Predicate& waitPredicate)
		{
			WaitResult waitResult = WaitResult::Failure;

			if (auto* syncContext = this->BeginWait())
			{
				try
				{
					while (!waitPredicate(syncContext))
					{
						//
						this->BeginWait(syncContext);

						//
						syncContext->Wait(lock);

						//
						this->EndWait(syncContext);
					}
				}
				catch (...)
				{
					;
				}

				waitResult = this->TranslateWaitResult(syncContext, std::cv_status::no_timeout);

				this->ReleaseContext(syncContext);
			}

			return waitResult;
		}

		//
		template <class Lock, class Duration_t, class Predicate>
		WaitResult InternalWaitFor(Lock& lock, const Duration_t& duration, const Predicate& waitPredicate)
		{
			WaitResult waitResult = WaitResult::Failure;

			if (auto* syncContext = this->BeginWait())
			{
				std::cv_status cvStatus = std::cv_status::no_timeout;

				try
				{
					while (!waitPredicate(syncContext) && (cvStatus != std::cv_status::timeout))
					{
						// This will mark thread as waiting and perform SO-specific "Enter Wait State" logic.
						this->BeginWait(syncContext);

						// Wait for specified timeout.
						cvStatus = syncContext->WaitFor(lock, duration);

						// Reset thread state.
						this->EndWait(syncContext);
					}
				}
				catch (...)
				{
					;
				}

				waitResult = this->TranslateWaitResult(syncContext, cvStatus);

				this->ReleaseContext(syncContext);
			}

			return waitResult;
		}

		// Internal method. Acquires context and updates thread state.
		void BeginWait(SyncContext<MutexType>* syncContext)
		{
			this->OnWaitBegin(syncContext);

			// Update thread state based on policy (TSUMode). For No_Update, this will expand to a no-op.
			// Otherwise, thread sync state is updated. Note, that this method will lock ThreadSyncState.
			// This will guarantee proper synchronization in case of an external interrupt attempt.
			UpdateThreadState(this, syncContext);
		}

		//
		void EndWait(SyncContext<MutexType>* syncContext, bool signaled)
		{
			// First, reset thread state to ensure it is updated ASAP (this will save a lot of unnecessary
			// work if someone would like to interrupt wait process that already entered post-wait phase).
			ResetThreadState(syncContext);

			this->OnWaitEnd(syncContext);
		}

		// Returns wait result based on current state and conditions.
		static WaitResult TranslateWaitResult(const SyncContext<MutexType>* syncContext, std::cv_status status)
		{
			auto waitResult = WaitResult::Spurious;

			if (status == std::cv_status::timeout)
			{
				waitResult = WaitResult::Timeout;
			}

			if (syncContext->IsInterruptFlagSet())
			{
				waitResult = WaitResult::Interrupt;
			}

			if (syncContext->IsNotifyFlagSet())
			{
				waitResult = WaitResult::Signaled;
			}

			return waitResult;
		}
	};


}


#endif /* __Exs_Core_SyncInternal_H__ */
