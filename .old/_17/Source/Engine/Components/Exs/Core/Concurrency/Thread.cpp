
#include <Exs/Core/Concurrency/Thread.h>
#include <Exs/Core/Concurrency/ThreadException.h>
#include <Exs/Core/Concurrency/ThreadSystemManager.h>
#include <Exs/Core/Concurrency/ThreadLocalStorage.h>
#include <Exs/Core/Concurrency/ThreadSharedApi.h>
#include <Exs/Core/Concurrency/SharedThreadSystemState.h>
#include <Exs/Core/Intercom/CoreMessageDispatcher.h>


namespace Exs
{


	static const ThreadProperties applicationThreadProperties
	{
		ThreadBaseType::Application,
		{
			/* isSynchronizedWithParent */ true,
			/* canCreateChildThreads */ true,
			/* canSendAndReceiveMessages */ true
		}
	};

	static const ThreadProperties backgroundThreadProperties
	{
		ThreadBaseType::Background,
		{
			/* isSynchronizedWithParent */ false,
			/* canCreateChildThreads */ false,
			/* canSendAndReceiveMessages */ true
		}
	};

	static const ThreadProperties temporaryThreadProperties
	{
		ThreadBaseType::Temporary,
		{
			/* isSynchronizedWithParent */ true,
			/* canCreateChildThreads */ false,
			/* canSendAndReceiveMessages */ false
		}
	};

	static const ThreadProperties workerThreadProperties
	{
		ThreadBaseType::Worker,
		{
			/* isSynchronizedWithParent */ true,
			/* canCreateChildThreads */ false,
			/* canSendAndReceiveMessages */ true
		}
	};




	Thread::Thread(SharedThreadSystemStateHandle tsSharedState)
	: _internalState(this)
	, _syncState(this)
	, _tsSharedState(tsSharedState)
	, _threadSystemManager(tsSharedState->threadSystemManager)
	, _coreInfo(nullptr)
	, _uid(Thread_UID_Invalid)
	, _systemThread(nullptr)
	, _tls(nullptr)
	{ }


	Thread::~Thread()
	{ }


	bool Thread::AddChildThread(Thread* thread)
	{
		ExsBeginCriticalSection(this->_internalState.GetLock());
		{
			return this->_internalState.AddChild(thread, thread->GetUID());
		}
		ExsEndCriticalSection();
	}


	void Thread::RemoveChildThread(Thread* thread)
	{
		ExsBeginCriticalSection(this->_internalState.GetLock());
		{
			this->_internalState.RemoveChild(thread, thread->GetUID());
		}
		ExsEndCriticalSection();
	}


	bool Thread::RegisterMessageController(bool activate)
	{
		// if (this->_messageController)
		// {
		// 	return false;
		// }
		// 
		// Result regResult = this->_messageDispatcher->RegisterController(this->_uid);

		return true;
	}


	void Thread::UnregisterMessageController()
	{

	}


	Result Thread::Start(const ThreadStartParameters& startParams)
	{
	}


	bool Thread::Register(const ThreadRegSpecification& regSpecification)
	{
		if (const auto* threadCoreInfo = this->_threadSystemManager->RegisterThread(this, &(this->_internalState), regSpecification))
		{
			this->_coreInfo = threadCoreInfo;
			this->_uid = threadCoreInfo->uid;
		}

		return this->_coreInfo != nullptr;

	}


	void Thread::Unregister()
	{
		if (this->_coreInfo != nullptr)
		{
			this->_threadSystemManager->UnregisterThread(this, this->_coreInfo);
			this->_coreInfo = nullptr;
		}
	}


	ThreadInitResult Thread::Initialize()
	{
		// Thread should be registered by now and contain pointer to a valid core info object.
		ExsDebugAssert( (this->_coreInfo != nullptr) && (this->_uid != Thread_UID_Invalid) );

		// Initialize TLS. If initialized successfuly, non-NULL pointer to the TLS of the current thread is returned.
		auto* threadLocalStorage = ThreadInternal::InitializeThreadLocalStorage(this->_coreInfo);

		// Save the TLS for the current thread object to properly release it in case of an error.
		this->_tls = threadLocalStorage;

		if (threadLocalStorage == nullptr)
		{
			return ThreadInitResult::Err_TLS_Init_Failed;
		}

		if (!this->InternalOnInit(threadLocalStorage))
		{
			// If there is an error during internal initialization, throw exception.
			return ThreadInitResult::Err_Internal_Init_Failed;
		}

		if (!this->OnInit(threadLocalStorage))
		{
			// If false is returned from this function, initialization should be aborted.
			return ThreadInitResult::Err_On_Init_Failed;;
		}

		return ThreadInitResult::No_Error;
	}

	
	void Thread::Release()
	{
		this->OnRelease(this->_tls);
		this->InternalOnRelease(this->_tls);

		ThreadInternal::ReleaseThreadLocalStorage(this->_tls);
	}


	bool Thread::OnInit(ThreadLocalStorage*)
	{
		return true;
	}


	void Thread::OnRelease(ThreadLocalStorage* tls) noexcept
	{ }


	void Thread::_ExecutionWrapper(Thread* thread)
	{
		if (!thread->Register({}))
		{
			return;
		}

		try
		{
			// Initialize thread (TLS, on-init callbacks, etc.). If this fails (returns code other than No_Error),
			// thread execution cannot proceed. Handle this by throwing ThreadInitializationErrorException.
			auto initResult = thread->Initialize();

			if (initResult != ThreadInitResult::No_Error)
			{
				// Throw exception. This special exception type will store the result and convert it to a readable message.
				ExsExceptionThrow(EXC_Thread_Initialization_Error, initResult);
			}

			// Execute thread entry point.
			thread->Entry();
		}
		// This is for exception that may be thrown when initialization fails.
		catch (const ThreadInitializationErrorException& exception)
		{
			(exception);
		}
		// This catches exception that mey be thrown from the thread's control logic if it is interrupted.
		catch (const ThreadInterruptException& exception)
		{
			(exception);
		}
		// Other exceptions (altough specialized catch() should be used when possible).
		catch (const Exception& exception)
		{
			(exception);
		}
		catch (...)
		{
			ExsDebugInterrupt();
		}

		thread->Release();
		thread->Unregister();
	}


	bool Thread::_ExternalInit(Thread* thread)
	{
		return true;
	}


	void Thread::_ExternalRelease(Thread* thread)
	{
	}




	ThreadBaseType ApplicationThread::GetBaseType() const
	{
		return ThreadBaseType::Application;
	}


	bool ApplicationThread::InternalOnInit(ThreadLocalStorage* tls)
	{
	}


	void ApplicationThread::InternalOnRelease(ThreadLocalStorage* tls)
	{
	}




	ThreadBaseType BackgroundThread::GetBaseType() const
	{
		return ThreadBaseType::Application;
	}


	bool BackgroundThread::InternalOnInit(ThreadLocalStorage* tls)
	{
	}


	void BackgroundThread::InternalOnRelease(ThreadLocalStorage* tls)
	{
	}




	ThreadBaseType TemporaryThread::GetBaseType() const
	{
		return ThreadBaseType::Application;
	}


	bool TemporaryThread::InternalOnInit(ThreadLocalStorage* tls)
	{
	}


	void TemporaryThread::InternalOnRelease(ThreadLocalStorage* tls)
	{
	}




	ThreadBaseType WorkerThread::GetBaseType() const
	{
		return ThreadBaseType::Application;
	}


	bool WorkerThread::InternalOnInit(ThreadLocalStorage* tls)
	{
	}


	void WorkerThread::InternalOnRelease(ThreadLocalStorage* tls)
	{
	}


}
