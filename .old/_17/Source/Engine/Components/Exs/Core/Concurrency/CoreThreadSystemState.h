
#ifndef __Exs_Core_SharedThreadSystemState_H__
#define __Exs_Core_SharedThreadSystemState_H__

#include "ConcrtCommon.h"


namespace Exs
{


	class CoreMessageDispatcher;
	class ThreadSystemManager;

	ExsDeclareSharedHandleTypeStruct( CoreThreadSystemState );


	///<summary>
	///</summary>
	struct CoreThreadSystemState : public ComponentSubsystemSharedState
	{
	public:
		struct InitData;

	private:
		const std::unique_ptr<ThreadSystemManager> _threadSystemManager;
		const std::unique_ptr<CoreMessageDispatcher> _coreMessageDispatcher;

	public:
		ThreadSystemManager* const threadSystemManager;
		CoreMessageDispatcher* const coreMessageDispatcher;

	public:
		CoreThreadSystemState( InitData && initData );
		~CoreThreadSystemState();
	};


	///<summary>
	///</summary>
	class CoreThreadSystemStateBuilder
	{
	public:
		///
		SharedHandle<CoreThreadSystemState> CreateState();

	private:
		///
		virtual std::unique_ptr<ThreadSystemManager> CreateThreadSystemManager();

		///
		virtual std::unique_ptr<CoreMessageDispatcher> CreateCoreMessageDispatcher();
	};


}


#endif /* __Exs_Core_SharedThreadSystemState_H__ */
