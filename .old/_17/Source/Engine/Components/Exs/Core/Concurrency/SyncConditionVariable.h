
#ifndef __Exs_Core_SyncConditionVariable_H__
#define __Exs_Core_SyncConditionVariable_H__

#include "SyncCommon.h"
#include <condition_variable>


namespace Exs
{


	///<summary>
	///</summary>
	template <class Cndvar>
	class SyncConditionVariableBase
	{
	protected:
		Cndvar _cndvar;

	public:
		SyncConditionVariableBase() = default;

		void NotifyAll()
		{
			this->_cndvar.notify_all();
		}

		void NotifyOne()
		{
			this->_cndvar.notify_one();
		}
	};


	///<summary>
	///</summary>
	template <class Mutex>
	class SyncConditionVariable : public SyncConditionVariableBase<std::condition_variable_any>
	{
	public:
		SyncConditionVariable() = default;

		template <class Lock>
		void Wait(Lock& lock)
		{
			this->_cndvar.wait(lock, predicate);
		}

		template <class Lock, class Duration>
		bool WaitFor(Lock& lock, const Duration& duration)
		{
			auto status = this->_cndvar.wait_for(lock, duration);
			return status == std::cv_status::no_timeout;
		}
	};


	///<summary>
	///</summary>
	template <>
	class SyncConditionVariable<SystemMutex> : public SyncConditionVariableBase<std::condition_variable>
	{
	public:
		SyncConditionVariable() = default;

		void Wait(SystemMutexLock& lock)
		{
			this->_cndvar.wait(lock);
		}

		template <class Duration>
		bool WaitFor(SystemMutexLock& lock, const Duration& duration)
		{
			auto status = this->_cndvar.wait_for(lock, duration);
			return status == std::cv_status::no_timeout;
		}
	};


	///<summary>
	///</summary>
	template <>
	class SyncConditionVariable<SharedMutex> : public SyncConditionVariableBase<std::condition_variable_any>
	{
	public:
		SyncConditionVariable() = default;

		void Wait( std::unique_lock<SharedMutex>& lock )
		{
			this->_cndvar.wait( lock );
		}

		void Wait( stdx::shared_lock<SharedMutex>& lock )
		{
			this->_cndvar.wait( lock );
		}

		template <class Duration>
		bool WaitFor( std::unique_lock<SharedMutex>& lock, const Duration& duration )
		{
			auto status = this->_cndvar.wait_for( lock, duration );
			return status == std::cv_status::no_timeout;
		}

		template <class Duration>
		bool WaitFor( stdx::shared_lock<SharedMutex>& lock, const Duration& duration)
		{
			auto status = this->_cndvar.wait_for(lock, duration);
			return status == std::cv_status::no_timeout;
		}
	};


}


#endif /* __Exs_Core_SyncConditionVariable_H__ */
