
#ifndef __Exs_Core_SyncLock_H__
#define __Exs_Core_SyncLock_H__

#include "SyncCommon.h"


namespace Exs
{


	class SyncMutexWrapper
	{
	private:
		LightMutex _accessMutex;
		SyncMutex _syncMutex;
		SyncMutexLock _syncMutexLock;
		std::atomic<bool> _lockFlag;

	public:
		SyncMutexWrapper()
		: _syncMutexLock(_syncMutex, std::defer_lock)
		{ }

		void Lock()
		{
			ExsBeginCriticalSection(this->_accessMutex);
			{
				this->_LockSyncMutex();
				this->_lockFlag.store(true, std::memory_order_release);
			}
			ExsEndCriticalSection();
		}

		bool TryLock()
		{
			ExsBeginCriticalSection(this->_accessMutex);
			{
				if (this->_TryLockSyncMutex())
				{
					this->_lockFlag.store(true, std::memory_order_release);
					return true;
				}
			}
			ExsEndCriticalSection();

			return false;
		}

		void Unlock()
		{
			ExsBeginCriticalSection(this->_accessMutex);
			{
				ExsDebugAssert( this->_syncMutexLock.owns_lock() );

				this->_lockFlag.store(false, std::memory_order_release);
				this->_UnlockSyncMutex();
			}
			ExsEndCriticalSection();
		}

		LightMutex& GetAccessLock()
		{
			return this->_accessMutex;
		}

		SyncMutexLock& GetSyncLock()
		{
			return this->_syncMutexLock;
		}

		bool CheckSyncLockState()
		{
			ExsBeginCriticalSection(this->_accessMutex);
			{
				return this->_syncMutexLock.owns_lock();
			}
			ExsEndCriticalSection();
		}

		bool IsLocked() const
		{
			return this->_lockFlag.load(std::memory_order_acquire);
		}

	private:
		void _LockSyncMutex()
		{
			try
			{
				this->_syncMutexLock.lock();
			}
			catch ( ... )
			{
				ExsDebugInterrupt();
			}
		}

		bool _TryLockSyncMutex()
		{
			try
			{
				bool syncLockAcquired = this->_syncMutexLock.try_lock();
				return syncLockAcquired;
			}
			catch ( ... )
			{
				ExsDebugInterrupt();
			}

			return false;
		}

		void _UnlockSyncMutex()
		{
			this->_syncMutexLock.unlock();
		}
	};


	class SyncMutexRefLock
	{
	private:
		SyncMutexLock*  _mutexLockPtr;
		bool            _lockedStatus;

	public:
		SyncMutexRefLock(SyncMutexLock& mutexLockRef)
		: _mutexLockPtr(nullptr)
		, _lockedStatus(false)
		{ }

		SyncMutexRefLock(SyncMutexLock& mutexLockRef)
		: _mutexLockPtr(&mutexLockRef)
		, _lockedStatus(false)
		{
			this->_mutexLockPtr->lock();
			this->_lockedStatus = true;
		}

		SyncMutexRefLock(SyncMutexLock& mutexLockRef, const std::defer_lock_t&)
		: _mutexLockPtr(&mutexLockRef)
		, _lockedStatus(false)
		{ }

		SyncMutexRefLock(SyncMutexLock& mutexLockRef, const std::try_to_lock_t&)
		: _mutexLockPtr(&mutexLockRef)
		, _lockedStatus(false)
		{
			this->_lockedStatus = this->_mutexLockPtr->try_lock();
		}

		~SyncMutexRefLock()
		{
			if (this->_lockedStatus)
			{
				this->_mutexLockPtr->unlock();
				this->_lockedStatus = false;
			}
		}

		operator bool() const
		{
			return this->owns_lock();
		}

		void lock()
		{
			this->_mutexLockPtr->lock();
			this->_lockedStatus = true;
		}

		bool try_lock()
		{
			this->_lockedStatus = this->_mutexLockPtr->try_lock();
			return this->_lockedStatus;
		}

		void unlock()
		{
			this->_mutexLockPtr->unlock();
			this->_lockedStatus = false;
		}

		SyncMutexLock& fetch_lock()
		{
			return *(this->_mutexLockPtr);
		}

		SyncMutexLock* release_lock()
		{
			auto* lockPtr = this->_mutexLockPtr;
			this->_mutexLockPtr = nullptr;
			this->_lockedStatus = false;
			return lockPtr;
		}

		bool owns_lock() const
		{
			return this->_lockedStatus ? this->_mutexLockPtr->owns_lock() : false;
		}
	};


#define ExsBeginSyncCriticalSection(lockGuardName, mutex) \
	do { SyncMutexRefLock lockGuardName { mutex }


}

#endif /* __Exs_Core_SyncLock_H__ */
