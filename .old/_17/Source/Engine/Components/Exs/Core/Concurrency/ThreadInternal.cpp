
#include <Exs/Core/Concurrency/Thread.h>
#include <Exs/Core/Concurrency/ThreadInternal.h>
#include <Exs/Core/Concurrency/ThreadLocalStorage.h>


namespace Exs
{


	ThreadUID ThreadInternal::GenerateThreadUID(Thread* thread, ThreadBaseType thrBaseType, Thread_index_t thrIndex, ThreadRefID refID)
	{
		ThreadUID result;
		result.internal.baseType = thrBaseType;
		result.internal.index = thrIndex;

		if (refID != 0)
		{
			// If non-zero ref id is specified by the user during thread's creation, use it.
			result.internal.refID = refID;
		}
		else
		{
			// Otherwise, use the address of the thread object as ref id.
			auto ptrValue = reinterpret_cast<Ptrval_t>(thread);

		#if ( EXS_TARGET_64 )
			// On 64-bit target, shift to remove lower 16 bits and then truncate to keep lower 32 bits only.
			result.internal.refID = truncate_cast<ThreadRefID>(ptrValue >> 16);
		#else
			// On 32-bit target, use all 32 bits of the address.
			result.internal.refID = truncate_cast<ThreadRefID>(ptrValue);
		#endif
		}

		return result;
	}

	std::pair<ThreadLocalStorage*, Uint> ThreadInternal::GetThreadLocalStorageUnchecked()
	{
		// Ensures, that each TLS is initialized only once.
		static EXS_ATTR_THREAD_LOCAL std::once_flag tlsInitFlag;

		// Actual TLS data, stored as thread-specific object.
		static EXS_ATTR_THREAD_LOCAL ThreadLocalStorage threadLocalStorage;

		// TLS is initialized by filling its whole memory with zeros.
		std::call_once(tlsInitFlag, [](ThreadLocalStorage* tls) -> void {
			TLSInterface::SetEmptyState(tls);
		}, &threadLocalStorage);

		// Increment fetch count and store it in variable to be returned.
		Uint fetchCount = ++threadLocalStorage.controlBlock.uncheckedFetchCount;

		// Return pointer to the TLS and value of the fetch counter.
		return std::pair<ThreadLocalStorage*, Uint>(&threadLocalStorage, fetchCount);
	}


	ThreadLocalStorage* ThreadInternal::InitializeThreadLocalStorage(const ThreadCoreInfo* threadCoreInfo)
	{
		// Fetch TLS via internal ("unchecked") method to initialize it.
		auto tlsInfo = GetThreadLocalStorageUnchecked();

		// Unchecked version of TLS getter also returns fetch counter. At this point, TLS should be
		// fetched for the very first time (fetch counter should be 1). Check this.
		if (tlsInfo.second != 1)
		{
			// If fetch counter is greater than 0, there may a logic error somewhere in code (the TLS cannot
			// be used before its initialization - which is performed here!).
			ExsTraceWarning(TRC_Core_Threading, "TLS fetch counter seems invalid (%u).", tlsInfo.second);
		}

		ThreadLocalStorageInitParams tlsInitParams{};
		tlsInitParams.threadCoreInfo = threadCoreInfo;

		if (!TLSInterface::Initialize(tlsInfo.first, tlsInitParams))
		{
			return nullptr;
		}

		return tlsInfo.first;
	}


	void ThreadInternal::ReleaseThreadLocalStorage(ThreadLocalStorage* tls)
	{
		if (tls != nullptr)
		{
			if (tls->internalState.releaseCallbackController != nullptr)
			{
				// Delete user data release list. This will call all registered callbacks
				// in reverse order of registration causing all user data to be released.
				delete tls->internalState.releaseCallbackController;
			}

			tls->internalState.releaseCallbackController = nullptr;
			tls->internalState.thread = nullptr;
			tls->internalState.threadUID = Thread_UID_Invalid;
			tls->controlBlock.validationKey = 0;
			tls = nullptr;
		}
	}


}
