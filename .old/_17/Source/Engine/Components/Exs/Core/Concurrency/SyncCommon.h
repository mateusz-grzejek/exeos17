
#ifndef __Exs_Core_SyncCommon_H__
#define __Exs_Core_SyncCommon_H__

#include "ConcrtCommon.h"


namespace Exs
{


	class SyncObject;
	class SyncRefLock;
	class Thread;
	class ThreadSyncState;


	enum class ThreadSyncStateUpdatePolicy : Enum
	{
		Update
	};


	///<summary>
	///</summary>
	enum class WaitResult : Enum
	{
		// SyncObject failed to wait. This can happen, for example, if a thread will try to wait for
		// UniqueSyncObject which is already busy or if limit of threads for SharedSyncObject is exceeded.
		Failure = RSC_Err_Failed,

		// Wait has been interrupted. This can happen if SyncObject is singaled with Abort() call or
		// if an explicit Interrupt() call is made for a specific thread object.
		Interrupt = RSC_Wait_Res_Interrupt,

		// Sync object has been signaled. If wait predicate
		Signaled = RSC_Success,

		//
		Spurious = RSC_None,

		//
		Timeout = RSC_Wait_Res_Timeout
	};


	namespace Config
	{

		enum : Size_t
		{
			// Max number of threads which can wait on a single shared sync object.
			CCRT_Common_Sync_Object_Thread_Num_Limit = 16
		};

	}


}

#endif /* __Exs_Core_SyncCommon_H__ */
