
#include <Exs/Core/Concurrency/Thread.h>
#include <Exs/Core/Concurrency/ThreadException.h>
#include <Exs/Core/Concurrency/ThreadRegistry.h>


namespace Exs
{


	ThreadUID ThreadRegistry::QueryThreadUID(ThreadRefID threadRefID) const
	{
		// Use explicit find to prevent creation of empty mappings to non-existing UIDs.
		// Altough it might be a possibility to consider, if ThreadUID objects were
		// initialized by default to Thread_UID_Invalid value.

		auto threadUIDRef = this->_refIDMap.find(threadRefID);

		if (threadUIDRef != this->_refIDMap.end())
		{
			// Return found UID. No additional checks are performed, because RefID --> UID
			// mapping is done during the registration phase, so here we can keep it simple.

			return threadUIDRef->value;
		}
		
		return ThreadUID(Thread_UID_Invalid);
	}


	bool ThreadRegistry::IsThreadRegistered(ThreadUID threadUID) const
	{
		// If this function turns to be used very intesively (altough it should not be),
		// an unordered_map<UID, {}> may be intruduced to increase search time. Keep it simple now, though.

		auto threadInfoRef = std::find_if(this->_threadList.begin(), this->_threadList.end(), [threadUID](const ThreadInfo& info) -> bool {
			return info.uid == threadUID;
		});

		return threadInfoRef != this->_threadList.end();
	}


	bool ThreadRegistry::IsThreadRegistered(ThreadRefID threadRefID) const
	{
		auto threadUID = this->QueryThreadUID(threadRefID);

		if (threadUID != Thread_UID_Invalid)
		{
			// If UID is still in the map, check if thread is on the reg list.
			bool result = this->IsThreadRegistered(threadUID);

			// This should never fail - id ref ID is still in the map, thread should also be on the list.
			ExsDebugAssert( result );

			return result;
		}

		return false;
	}


	bool ThreadRegistry::RegisterThread(Thread* thread, ThreadUID threadUID, const ThreadRegSpecification& regSpecification)
	{
		if (this->IsThreadRegistered(threadUID))
		{
			return false;
		}

		ThreadInfo threadInfo;
		threadInfo.thread = thread;
		threadInfo.uid = threadUID;
		threadInfo.name = regSpecification.name;

		this->_threadList.push_back(std::move(threadInfo));
		this->_refIDMap.insert(regSpecification.refID, threadUID);
		this->_registeredThreadsNum.fetch_add(1, std::memory_order_relaxed);

		return true;
	}


	bool ThreadRegistry::UnregisterThread(Thread* thread, ThreadUID threadUID)
	{
		auto threadInfoRef = std::find_if(this->_threadList.begin(), this->_threadList.end(), [threadUID](const ThreadInfo& info) -> bool {
			return info.uid == threadUID;
		});

		if ((threadInfoRef == this->_threadList.end()) || (threadInfoRef->thread == thread))
		{
			return false;
		}

		auto threadUIDRef = this->_refIDMap.find(threadUID.internal.refID);
		ExsDebugAssert( threadUIDRef != this->_refIDMap.end() );

		this->_threadList.erase(threadInfoRef);
		this->_refIDMap.erase(threadUIDRef);
		this->_registeredThreadsNum.fetch_sub(1, std::memory_order_relaxed);

		return true;
	}


}
