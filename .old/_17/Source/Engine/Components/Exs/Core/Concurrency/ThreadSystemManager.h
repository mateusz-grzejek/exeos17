
#ifndef __Exs_Core_ThreadSystemManager_H__
#define __Exs_Core_ThreadSystemManager_H__

#include "ConcurrentDataPool.h"
#include "ThreadActivityLog.h"
#include "ThreadRegistry.h"
#include "ThreadSyncContext.h"


namespace Exs
{


	class Thread;
	class ThreadInternalState;
	class ThreadRegistry;


	struct ThreadEnvironmentConfig
	{
		///
		Uint32 hardwareConcurrencyLevel = 0;
	};


	///<summary>
	/// ThreadSystemManager is the core management interface of thread system. It is responsible for registration
	/// of thread objects, stores activity log, sync index, registry and provides unified interface to access
	/// functionality of those components.
	///</summary>
	class EXS_LIBCLASS_CORE ThreadSystemManager
	{
		friend class Thread;

	private:
		struct alignas(Config::HW_Destructive_Cache_Interference_Size) ThreadPersistentData
		{
			ThreadCoreInfo coreInfo;
			ThreadInternalState* internalState = nullptr;
			Thread* thread = nullptr;
		};

		// Local constant for max allowed number of active threads in the system.
		static constexpr Size_t maxThreadsNum = Config::CCRT_Thr_Max_Active_Threads_Num;

		// Main 'index' which stores actual slots acquired by threads on registration.
		using ThreadPersistentDataIndex = ConcurrentDataPool<ThreadPersistentData, maxThreadsNum>;

	private:
		ThreadEnvironmentConfig         _environmentConfig; // Environment configuration of thread-specific things.
		ThreadActivityLog               _activityLog; // Activity log.
		ThreadRegistry                  _registry; // Registry. Controls thread registration process.
		ThreadPersistentDataIndex       _threadPersistentDataIndex; // Data index with thread persistent data.
		stdx::reg_ctrl_counter<size_t>  _regCounter; // Registration counter used to control the number of registered threads.

	public:
		ThreadSystemManager();
		~ThreadSystemManager();

		///
		ThreadUID QueryThreadUID(ThreadRefID threadRefID) const;

		///
		bool IsThreadRegistered(ThreadRefID threadRefID) const;

		///
		bool IsThreadRegistered(ThreadUID threadUID) const;

		///
		const ThreadEnvironmentConfig& GetEnvironmentConfig() const;

		///
		const ThreadActivityLog& GetActivityLog() const;

		///
		const ThreadRegistry& GetThreadRegistry() const;

		///
		Size_t GetActiveThreadsNum() const;

		///
		Size_t GetAvailableThreadsNum() const;

		///
		bool IsThreadAvailable() const;

	friendapi:
		//
		const ThreadCoreInfo* RegisterThread(Thread* thread, ThreadInternalState* internalState, const ThreadRegSpecification& regSpecification);

		//
		void UnregisterThread(Thread* thread, const ThreadCoreInfo* coreInfo);

	private:
		//
		bool _RegisterThreadInternal(Thread* thread, ThreadUID threadUID, const ThreadRegSpecification& regSpecification);

		//
		bool _UnregisterThreadInternal(Thread* thread, ThreadUID threadUID);

		//
		void _PostRegistrationEvent(ThreadActivityEventCode eventCode, ThreadUID threadUID);
	};


	inline const ThreadRegistry& ThreadSystemManager::GetThreadRegistry() const
	{
		return this->_registry;
	}

	inline Size_t ThreadSystemManager::GetActiveThreadsNum() const
	{
		return this->_regCounter.get_value();
	}

	inline Size_t ThreadSystemManager::GetAvailableThreadsNum() const
	{
		return maxThreadsNum - this->_regCounter.get_value();
	}

	inline bool ThreadSystemManager::IsThreadAvailable() const
	{
		return this->_regCounter.get_value() < maxThreadsNum;
	}


}


#endif /* __Exs_Core_ThreadSystemManager_H__ */
