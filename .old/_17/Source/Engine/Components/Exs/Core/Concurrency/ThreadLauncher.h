
#ifndef __Exs_Core_ThreadLauncher_H__
#define __Exs_Core_ThreadLauncher_H__

#include "Thread.h"


namespace Exs
{

	
	ExsDeclareSharedStateHandleType(CoreSubsystemState);

	
	///<summary>
	/// Contains data used to control the process of launching new threads.
	///</summary>
	struct ThreadLaunchInfo
	{
	};

	
	///<summary>
	///</summary>
	class ThreadLauncher
	{
	private:
		CoreSubsystemStateHandle  _commonState;

	public:
		ThreadLauncher(CoreSubsystemStateHandle commonState)
		: _commonState(commonState)
		{ }

		template <class Thr_t, class... Args>
		void Launch(const ThreadLaunchInfo& launchInfo, Args&&... args)
		{
		}

	};


}


#endif /* __Exs_Core_ThreadLauncher_H__ */
