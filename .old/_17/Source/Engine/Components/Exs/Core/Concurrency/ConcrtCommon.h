
#ifndef __Exs_Core_ConcrtCommon_H__
#define __Exs_Core_ConcrtCommon_H__

#include "../Prerequisites.h"


namespace Exs
{


	struct ThreadCoreInfo;

	class SyncObject;
	class Thread;

	ExsDeclareSharedHandleTypeStruct( CoreThreadSystemState );


	/// Opaque handle to thread objects.
	typedef const void* ThreadHandle;

	/// Stores index assigned to a thread in the thread registry.
	typedef Uint16 Thread_index_t;

	/// Represents unique ID of a thread object.
	typedef U64ID Thread_uid_value_t;


	/// Special values for ThreadUID.
	enum : Thread_uid_value_t
	{
		// Empty UID which has not been set. Used in addressing means 'all threads'.
		Thread_UID_None = 0,

		// Invalid UID. Represents uninitialized or invalidated thread.
		Thread_UID_Invalid = stdx::limits<Thread_uid_value_t>::max_value
	};


	enum : Thread_index_t
	{
		Thread_Index_Invalid = stdx::limits<Thread_index_t>::max_value,
	};


	namespace Config
	{

		enum : Size_t
		{
			//
			CCRT_Thr_Max_Active_Threads_Num = 32,

			//
			HW_Destructive_Cache_Interference_Size = EXS_HW_DESTRUCTIVE_CACHE_INTERFERENCE_SIZE
		};

	};

	
	///<summary>
	///</summary>
	enum class ThreadBaseType : Uint16
	{
		// Active, persistent thread which usually has application lifetime.
		// - Synchronized with parent thread: yes
		// - Can create child threads: yes
		// - Can send/receive messages: yes
		Application = 1,

		// Background thread, often reffered to as 'daemon' or detached thread.
		// - Synchronized with parent thread: no
		// - Can create child threads: no
		// - Can send/receive messages: yes
		Background,

		// Temporary, "fire-and-forget" thread. Alternative solution when thread pool cannot be used.
		// - Synchronized with parent thread: yes
		// - Can create child threads: no
		// - Can send/receive messages: no
		Temporary,

		// Worker thread, usually one of a group inside a thread pool.
		// - Synchronized with parent thread: yes
		// - Can create child threads: no
		// - Can send/receive messages: yes
		Worker,

		//
		Unknown = 0
	};


	///<summary>
	///</summary>
	struct ThreadProperties
	{
		struct Capabilities
		{
			bool isSynchronizedWithParent;
			bool canCreateChildThreads;
			bool canSendAndReceiveMessages;
		};

		ThreadBaseType baseType;
		Capabilities capabilities;
	};


	///<summary>
	///</summary>
	union ThreadUID
	{
	public:
		struct alignas(sizeof(Thread_uid_value_t)) InternalData
		{
			// Base type of thread - 16 bits.
			ThreadBaseType baseType;

			// Global thread index inside registry - 16 bits.
			Thread_index_t index;

			// Additional 'ref id', customizable on thread creation - 32 bits.
			ThreadRefID refID;
		};

		InternalData internal; // Internal data of the UID. Do not access directly!

		Thread_uid_value_t value; // Numerical value of the UID.

	public:
		explicit ThreadUID(Thread_uid_value_t uidValue = Thread_UID_Invalid)
		: value(uidValue)
		{ }

		ThreadUID(const ThreadUID& origin)
		: value(origin.value)
		{ }

		ThreadUID(const InternalData& internalData)
		: internal(internalData)
		{ }

		ThreadUID& operator=(const ThreadUID& rhs)
		{
			this->value = rhs.value;
			return *this;
		}

		ThreadUID& operator=(Thread_uid_value_t rhs)
		{
			this->value = rhs;
			return *this;
		}

		explicit operator Thread_uid_value_t() const
		{
			return this->value;
		}

		Thread_index_t GetIndex() const
		{
			return this->internal.index;
		}
	};


	static_assert(sizeof(ThreadUID) == sizeof(Thread_uid_value_t), "");


	inline bool operator==(const ThreadUID& lhs, const ThreadUID& rhs)
	{
		return lhs.value == rhs.value;
	}

	inline bool operator==(const ThreadUID& lhs, Thread_uid_value_t rhs)
	{
		return lhs.value == rhs;
	}

	inline bool operator==(Thread_uid_value_t lhs, const ThreadUID& rhs)
	{
		return lhs == rhs.value;
	}

	inline bool operator!=(const ThreadUID& lhs, const ThreadUID& rhs)
	{
		return lhs.value != rhs.value;
	}

	inline bool operator!=(const ThreadUID& lhs, Thread_uid_value_t rhs)
	{
		return lhs.value != rhs;
	}

	inline bool operator!=(Thread_uid_value_t lhs, const ThreadUID& rhs)
	{
		return lhs != rhs.value;
	}

	inline bool operator<(const ThreadUID& lhs, const ThreadUID& rhs)
	{
		return lhs.value < rhs.value;
	}

	inline bool operator<(const ThreadUID& lhs, Thread_uid_value_t rhs)
	{
		return lhs.value < rhs;
	}

	inline bool operator<(Thread_uid_value_t lhs, const ThreadUID& rhs)
	{
		return lhs < rhs.value;
	}

	inline bool operator<=(const ThreadUID& lhs, const ThreadUID& rhs)
	{
		return lhs.value <= rhs.value;
	}

	inline bool operator<=(const ThreadUID& lhs, Thread_uid_value_t rhs)
	{
		return lhs.value <= rhs;
	}

	inline bool operator<=(Thread_uid_value_t lhs, const ThreadUID& rhs)
	{
		return lhs <= rhs.value;
	}

	inline bool operator>(const ThreadUID& lhs, const ThreadUID& rhs)
	{
		return lhs.value > rhs.value;
	}

	inline bool operator>(const ThreadUID& lhs, Thread_uid_value_t rhs)
	{
		return lhs.value > rhs;
	}

	inline bool operator>(Thread_uid_value_t lhs, const ThreadUID& rhs)
	{
		return lhs > rhs.value;
	}

	inline bool operator>=(const ThreadUID& lhs, const ThreadUID& rhs)
	{
		return lhs.value >= rhs.value;
	}

	inline bool operator>=(const ThreadUID& lhs, Thread_uid_value_t rhs)
	{
		return lhs.value >= rhs;
	}

	inline bool operator>=(Thread_uid_value_t lhs, const ThreadUID& rhs)
	{
		return lhs >= rhs.value;
	}


};


#endif /* __Exs_Core_ConcrtCommon_H__ */
