
#ifndef __Exs_Core_ThreadInternal_H__
#define __Exs_Core_ThreadInternal_H__

#include "ConcrtCommon.h"


namespace Exs
{


	struct ThreadCoreInfo;
	struct ThreadLocalStorage;


	namespace Internal
	{

		enum : Uint32
		{
			THR_Local_Storage_Validation_Key = 0xABCDEF77
		};

	};


	enum class ThreadInitResult : Enum
	{
		No_Error,
		Err_Invalid_State,
		Err_Internal_Init_Failed,
		Err_On_Init_Failed,
		Err_TLS_Init_Failed
	};


	///<summary>
	///</summary>
	struct ThreadCoreInfo
	{
		// Unique ID assigned to the thread.
		ThreadUID uid;

		// Type of the thread.
		ThreadBaseType baseType = ThreadBaseType::Unknown;

		// Unique index assigned to the thread by the manager.
		Thread_index_t regIndex = Thread_Index_Invalid;

		//
		ThreadSyncContext* syncContext = nullptr;

		//
		Thread* thread = nullptr;
	};

	// ThreadCoreInfo objects should fit single cache line.
	static_assert(sizeof(ThreadCoreInfo) <= Config::HW_Destructive_Cache_Interference_Size, "");


	///<summary>
	///</summary>
	struct ThreadRegSpecification
	{
		// Text name assigned to the thread.
		const char* name = nullptr;

		// Reference ID: a user-defined, unique ID which the thread can be referenced with.
		ThreadRefID refID = 0;
	};


	///<summary>
	///</summary>
	class ThreadInternal
	{
	public:
		//
		static ThreadUID GenerateThreadUID(Thread* thread, ThreadBaseType thrBaseType, Thread_index_t thrIndex, ThreadRefID refID);

		//
		static std::pair<ThreadLocalStorage*, Uint> GetThreadLocalStorageUnchecked();

		//
		static ThreadLocalStorage* InitializeThreadLocalStorage(const ThreadCoreInfo* threadCoreInfo);

		//
		static void ReleaseThreadLocalStorage(ThreadLocalStorage* tls);
	};


}


#endif /* __Exs_Core_ThreadInternal_H__ */
