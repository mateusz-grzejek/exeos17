
#include <Exs/Core/Concurrency/ThreadLocalStorage.h>
#include <Exs/Core/Concurrency/ThreadInternal.h>
#include <Exs/Core/Internal/TLSReleaseCallbackController.h>


namespace Exs
{


	void TLSInterface::RegisterReleaseCallback(ThreadLocalStorage* tls, TLSReleaseCallback&& releaseCallback)
	{
		tls->internalState.releaseCallbackController->Register(std::move(releaseCallback));
	}


	void TLSInterface::RegisterReleaseCallback(ThreadLocalStorage* tls, const TLSReleaseCallback& releaseCallback)
	{
		tls->internalState.releaseCallbackController->Register(releaseCallback);
	}


	bool TLSInterface::SetEmptyState(ThreadLocalStorage* tls)
	{
		tls->controlBlock.validationKey = 0;
		tls->controlBlock.uncheckedFetchCount = 0;

		tls->internalState.threadUID = Thread_UID_Invalid;
		tls->internalState.thread = nullptr;
		tls->internalState.syncContext = nullptr;
		tls->internalState.releaseCallbackController = nullptr;

		ZeroMemory(&(tls->userDataArray[0]), sizeof(tls->userDataArray[0]) * tls->userDataArray.size());
	}


	bool TLSInterface::Initialize(ThreadLocalStorage* tls, const ThreadLocalStorageInitParams& initParams)
	{
		if (tls->controlBlock.validationKey != 0)
		{
			return false;
		}

		tls->controlBlock.validationKey = Internal::THR_Local_Storage_Validation_Key;

		tls->internalState.threadUID = initParams.threadCoreInfo->uid;
		tls->internalState.thread = initParams.threadCoreInfo->thread;
		tls->internalState.syncContext = initParams.threadCoreInfo->syncContext;
		tls->internalState.releaseCallbackController = new TLSReleaseCallbackController(tls);

		return true;
	}

	void TLSInterface::Release(ThreadLocalStorage* tls)
	{
		delete tls->internalState.releaseCallbackController;
		tls->internalState.releaseCallbackController = nullptr;

		ZeroMemory(&(tls->userDataArray[0]), sizeof(tls->userDataArray[0]) * tls->userDataArray.size());
	}


	bool TLSInterface::Validate(ThreadLocalStorage* tls)
	{
		if (tls->controlBlock.uncheckedFetchCount == 0)
		{
			return false;
		}

		if (tls->controlBlock.validationKey != Internal::THR_Local_Storage_Validation_Key)
		{
			return false;
		}

		return true;
	}


}
