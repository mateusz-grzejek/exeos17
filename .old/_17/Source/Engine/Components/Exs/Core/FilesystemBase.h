
#ifndef __Exs_Core_FilesystemBase_H__
#define __Exs_Core_FilesystemBase_H__

#include "Prerequisites.h"


namespace Exs
{


	class Directory;
	class File;

	typedef Int32 File_offset_t;
	typedef Uint32 File_size_t;


  #if ( EXS_CONFIG_BASE_USE_ANSI_FILE_IO )
	typedef FILE* FileHandle;
  #endif

	
	///<summary>
	///</summary>
	enum class FileCreateMode : Enum
	{
		//
		Create_Always = 0xF0B1,

		//
		Create_New
	};

	
	///<summary>
	///</summary>
	enum class FileOpenMode : Enum
	{
		//
		Open_Always = 0xF0C1,
		
		//
		Open_Existing
	};

	
	///<summary>
	///</summary>
	enum class FileRefPoint : Enum
	{
		//
		Start = SEEK_SET,

		//
		Current = SEEK_CUR,

		//
		End = SEEK_END
	};


	enum : File_offset_t
	{
		Invalid_File_Offset = -1
	};


	enum : File_size_t
	{
		Invalid_File_Size = stdx::limits<File_size_t>::max_value,

		Max_File_Size = stdx::limits<File_size_t>::max_value
	};


	///<summary>
	///</summary>
	class DirEntry
	{
	protected:
		Directory*    _parentDir;
	};


};


#endif /* __Exs_Core_FilesystemBase_H__ */
