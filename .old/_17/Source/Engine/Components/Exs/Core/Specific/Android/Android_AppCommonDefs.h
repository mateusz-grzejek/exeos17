
#ifndef __Exs_Core_AppCommonDefs_Android_H__
#define __Exs_Core_AppCommonDefs_Android_H__

#include <Exs/Core/Prerequisites.h>


namespace Exs
{


	enum AppMessages : Enum
	{
		AppMessage_Start,
		AppMessage_Stop,
		AppMessage_Pause,
		AppMessage_Resume,
		AppMessage_Destroy,
		AppMessage_Low_Memory,
		AppMessage_Configuration_Changed,
		AppMessage_Window_Focus_Changed,
		AppMessage_Native_Window_Created,
		AppMessage_Native_Window_Destroyed,
		AppMessage_Native_Window_Resized,
		AppMessage_Native_Window_Redraw_Needed,
		AppMessage_Input_Queue_Created,
		AppMessage_Input_Queue_Destroyed,
		AppMessage_Content_Rect_Changed,
		AppMessage_Save_Instance_State,
	};


}


#endif /* __Exs_Core_AppCommonDefs_Android_H__ */
