
#include <Exs/Core/AppEventLoop.h>
#include <Exs/Core/AppObject.h>
#include <android/input.h>
#include <android/looper.h>


namespace Exs
{


	AppEventLoop::AppEventLoop(AppObject* appObject, AppExecutionState* appExecutionState)
	: AppEventLoopBase(appObject, appExecutionState)
	{ }


	AppEventLoop::~AppEventLoop()
	{ }


	Result AppEventLoop::EnterSystemLoop(const UserCallback& loopUpdateCallback)
	{
		// SystemEventDispatcher* eventDispatcher = this->_appObject->GetSystemEventDispatcher();

		while (!this->_appExecutionState->IsStateSet(AppState_Terminate_Request))
		{
			int events;
			int eventTypeID;

			while (true)
			{
				if (this->_appExecutionState->IsStateSet(AppState_Idle))
					eventTypeID = ALooper_pollAll(-1, nullptr, &events, nullptr);
				else
					eventTypeID = ALooper_pollAll(0, nullptr, &events, nullptr);

				if (eventTypeID < 0)
					break;

				// if (evtSource != nullptr)
				// 	evtSource->process(androidApp, evtSource);
			}
		
			Result updateResult = loopUpdateCallback();

			if (updateResult == RSC_App_Main_Loop_Execution_Abort)
				break;
		
			CurrentThread::SleepFor(Milliseconds(16));
		}

		return RSC_Success;
	}


}
