
#include <Exs/Core/PlatformUtils.h>
#include <Exs/Core/System/SystemWindow.h>


namespace Exs
{


	bool SystemWindow::Destroy()
	{
		if (this->_handle == X11_Null_Window)
			return false;

		//XCloseDisplay(this->_systemEnvData->display);
		XDestroyWindow(this->_systemEnvData->display, this->_handle);
		this->_handle = X11_Null_Window;

		XFreeColormap(this->_systemEnvData->display, this->_windowData.colorMap);
		this->_windowData.colorMap = X11_Null_Drawable;

		return true;
	}


	bool SystemWindow::Show(bool show)
	{
		ExsDebugAssert( this->IsValid() );

		if (show)
		{
			XMapWindow(this->_systemEnvData->display, this->_handle);
			XFlush(this->_systemEnvData->display);
		}
		else
		{
			XFlush(this->_systemEnvData->display);
			XUnmapWindow(this->_systemEnvData->display, this->_handle);
		}

		return true;
	}


	bool SystemWindow::Resize(const Size& size)
	{
		ExsDebugAssert( this->IsValid() );

		int displayWidth = XDisplayWidth(this->_systemEnvData->display, this->_systemEnvData->screen);
		int displayHeight = XDisplayHeight(this->_systemEnvData->display, this->_systemEnvData->screen);
		Uint32 windowWidth = stdx::get_min_of(size.width, displayWidth);
		Uint32 windowHeight = stdx::get_min_of(size.height, displayHeight);

		XResizeWindow(this->_systemEnvData->display, this->_handle, windowWidth, windowHeight);

		return true;
	}


	bool SystemWindow::SetStyle(SystemWindowStyle style)
	{
		ExsDebugAssert( this->IsValid() );
		return false;
	}


	bool SystemWindow::SetTitle(const std::string& title)
	{
		ExsDebugAssert( this->IsValid() );
		return false;
	}


	bool SystemWindow::GetClientAreaSize(Size* size) const
	{
		ExsDebugAssert( this->IsValid() );

		Size windowSize { };
		XGetGeometry(this->_systemEnvData->display, this->_handle, 0, 0, 0, &(windowSize.width), &(windowSize.height), 0, 0);

		return true;
	}


	bool SystemWindow::GetSize(Size* size) const
	{
		return this->GetClientAreaSize(size);
	}




	SystemWindowRefPtr CreateSystemWindow(AppGlobalStateRefHandle appGlobalState, const SystemWindowFrameDesc& frameDesc)
	{
		ExsDebugAssert( appGlobalState );
		auto& systemEnvData = appGlobalState->systemEnvData;

		// Get default visual and depth for current screen.
		Visual* defaultVisual = XDefaultVisual(systemEnvData.display, systemEnvData.screen);
		int defaultDepth = XDefaultDepth(systemEnvData.display, systemEnvData.screen);

		Colormap colormap = XCreateColormap(systemEnvData.display, systemEnvData.rootWindow, defaultVisual, AllocNone);
		long eventMask = ExposureMask | StructureNotifyMask | KeyPressMask| KeyReleaseMask | ButtonPressMask | ButtonReleaseMask;

		XSetWindowAttributes windowSetAttribs { };
		windowSetAttribs.background_pixel = 0;
		windowSetAttribs.border_pixel = 0;
		windowSetAttribs.colormap = colormap;
		windowSetAttribs.event_mask = eventMask;
		unsigned long windowSetAttribsMask = CWBackPixel | CWBorderPixel | CWColormap | CWEventMask;

		Window windowHandle = XCreateWindow( systemEnvData.display,
		                                     systemEnvData.rootWindow,
		                                     frameDesc.position.x,
		                                     frameDesc.position.y,
		                                     frameDesc.size.width,
		                                     frameDesc.size.height,
		                                     0,
		                                     defaultDepth,
		                                     InputOutput,
		                                     defaultVisual,
		                                     windowSetAttribsMask,
		                                     &windowSetAttribs);

		if (windowHandle == X11C_None)
			return nullptr;

		Atom registeredWMProtocols[] = { systemEnvData.wmpDeleteWindow };
		Uint32 registeredWMProtocolsNum = 1;

		XSetWMProtocols(systemEnvData.display, windowHandle, registeredWMProtocols, registeredWMProtocolsNum);
		XStoreName(systemEnvData.display, windowHandle, frameDesc.title.c_str());

		X11SystemWindowData windowData { };
		windowData.colorMap = colormap;
		windowData.eventMask = eventMask;

		auto windowObject = std::make_shared<SystemWindow>(windowHandle, appGlobalState, windowData);
		return windowObject;
	}


}
