
#include <Exs/Core/SystemEventDispatcher.h>
#include <Exs/Core/SystemEventReceiver.h>
#include <Exs/Core/EvtSysAppCommand.h>
#include <Exs/Core/EvtSysInputKeyboard.h>
#include <Exs/Core/EvtSysInputMouse.h>
#include <Exs/Core/EvtSysInputTouch.h>
#include <Exs/Core/EvtSysWindowCommand.h>
#include <Exs/Core/EvtSysWindowNotification.h>


namespace Exs
{


	SystemEventDispatcher::SystemEventDispatcher(SystemEventReceiver* eventReceiver)
	: SystemEventDispatcherBase(eventReceiver)
	{ }


	SystemEventDispatcher::~SystemEventDispatcher()
	{ }


	void SystemEventDispatcher::DispatchEvent(const SystemEventDataAndroid* eventMessage)
	{
		TranslateAndDispatch(eventMessage, this->_eventReceiver);
	}


	void SystemEventDispatcher::TranslateAndDispatch(const SystemEventDataAndroid* eventMessage, SystemEventReceiver* eventReceiver)
	{
	}


}
