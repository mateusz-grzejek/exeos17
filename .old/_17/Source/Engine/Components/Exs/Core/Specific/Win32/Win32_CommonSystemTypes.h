
#ifndef __Exs_Core_CommonSystemTypes_Win32_H__
#define __Exs_Core_CommonSystemTypes_Win32_H__


namespace Exs
{

	
	///<summary>
	///</summary>
	struct Win32SystemEnvData
	{
		//
		HINSTANCE appInstance = nullptr;
	};

	
	///<summary>
	///</summary>
	struct Win32SystemEventInfo
	{
		//
		HWND windowHandle;

		//
		UINT messageCode;

		//
		LPARAM lParam;

		//
		WPARAM wParam;
	};


	///<summary>
	/// Represents Win32-specific data used by a system window.
	///</summary>
	struct Win32SystemWindowData
	{
		//
		ATOM windowClassID;

		//
		LPCSTR windowClassName;

		//
		HMODULE wndprocModule;
	};


}


#endif /* __Exs_Core_CommonSystemTypes_Win32_H__ */
