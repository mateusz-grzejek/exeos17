
#ifndef __Exs_Core_AppThread_Android_H__
#define __Exs_Core_AppThread_Android_H__

#include <Exs/Core/ActiveThread.h>


namespace Exs
{


	class AppObject;


	class AndroidAppThread : public ActiveThread
	{
	private:
		AppObject*    _appObject;

	public:
		AndroidAppThread( ThreadUID parentUID,
		                  ThreadCoreInfo* threadCoreInfo,
		                  SharedExecutionState* sharedExecutionState,
		                  AppObject* appObject);

		virtual ~AndroidAppThread();

	protected:
		virtual void Entry() override;
	};

	
}


#endif /* __Exs_Core_AppThread_Android_H__ */
