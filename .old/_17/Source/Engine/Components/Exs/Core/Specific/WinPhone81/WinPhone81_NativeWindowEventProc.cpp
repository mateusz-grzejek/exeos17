
#include <Exs/Core/NativeWindow.h>
#include <Exs/Core/EvtSystemAppCommand.h>
#include <Exs/Core/EvtSystemInputKeyboard.h>
#include <Exs/Core/EvtSystemInputMouse.h>
#include <Exs/Core/EvtSystemInputTouch.h>
#include <Exs/Core/EvtSystemWindowNotification.h>


namespace Exs
{


	bool NativeWindow::ProcessEvtCommon(UINT msgCode, WPARAM wParam, LPARAM lParam)
	{
		return true;
	}


	bool NativeWindow::ProcessEvtKeyboard(UINT msgCode, WPARAM wParam, LPARAM lParam)
	{
		return true;
	}


	bool NativeWindow::ProcessEvtMouse(UINT msgCode, WPARAM wParam, LPARAM lParam)
	{
		return true;
	}


}
