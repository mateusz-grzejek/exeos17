
#include <Exs/Core/System/AppEventLoop.h>


namespace Exs
{


	void Win32ProcessEventActive();
	void Win32ProcessEventIdle();


	void AppEventLoop::EnterSystemLoop(const AppInternalCallback& loopUpdateCallback)
	{
		while (!this->_appGlobalState->executionState.IsSet(AppExecutionState_Terminate_Request))
		{
			if (this->_appGlobalState->executionState.IsSet(AppExecutionState_Idle))
			{
				// Idle mode: blocks current thread until event is received.
				Win32ProcessEventIdle();
			}
			else
			{
				// Active mode: event is checked and if there is no event, continue.
				Win32ProcessEventActive();
			}

			if (!loopUpdateCallback())
				break;
		
			CurrentThread::SleepFor(Milliseconds(16));
		}
	}




	void Win32ProcessEventActive()
	{
		MSG win32Message = { 0 };
		while (PeekMessageA(&win32Message, nullptr, 0, 0, PM_REMOVE) != FALSE)
		{
			TranslateMessage(&win32Message);
			DispatchMessageA(&win32Message);
		}
	}


	void Win32ProcessEventIdle()
	{
		MSG win32Message = { 0 };
		if (GetMessageA(&win32Message, nullptr, 0, 0) != FALSE)
		{
			TranslateMessage(&win32Message);
			DispatchMessageA(&win32Message);
		}
	}


}
