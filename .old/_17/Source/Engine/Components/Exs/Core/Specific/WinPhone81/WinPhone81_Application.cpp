
#include <Exs/Core/Prerequisites.h>
#include <ExsEngine/Application.h>
#include <ExsEngine/CoreWindow.h>
#include <Exs/Core/NativeWindow.h>


namespace Exs
{


	Application::Application()
	: _appProcHandle(nullptr)
	{ }


	Application::~Application()
	{ }


	Result Application::CreateCoreWindow(const NativeWindowDesc& desc)
	{
		if (!this->IsInitialized())
			return RSC_Invalid_Operation;

		auto nativeWindow = MakeSharedObject<NativeWindow>();
		nativeWindow->SetEventsReceiver(this->_eventsReceiver.GetPtr());
		nativeWindow->CreateFrame(desc);
		nativeWindow->Show(true);

		this->_coreWindow = MakeSharedObject<CoreWindow>(*this, nativeWindow);

		return RSC_Success;
	}


	Result Application::InitSystemResources()
	{
		this->_appProcHandle = ::GetModuleHandleA(nullptr);
		return RSC_Success;
	}


	void Application::ReleaseSystemResources()
	{
		this->_coreWindow = nullptr;
		this->_appProcHandle = nullptr;
	}
	

	Result Application::RunSystemAppLoop()
	{
		MSG osMessage = { 0 };
		Result execResult = 0;

		while (!this->HasExitRequest())
		{
			if (PeekMessageA(&osMessage, nullptr, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&osMessage);
				DispatchMessageA(&osMessage);
			}

			execResult = this->OnUpdate();

			if (execResult != RSC_App_Execution_Continue)
			{
				ExsTraceNotification(TRC_System, "App quit received.");
				break;
			}

			CurrentThread::SleepFor(Milliseconds(33));
		}

		return execResult;
	}




	String AppBase::GetWorkingDirectory()
	{
		const Size_t cwdBufferSize = 1024;

		char cwdBuffer[cwdBufferSize];
		_getcwd(cwdBuffer, cwdBufferSize);

		return String(cwdBuffer);
	}


}
