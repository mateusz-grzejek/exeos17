
#include <Exs/Core/Prerequisites.h>
#include "Android_AppThread.h"


namespace Exs
{


	AndroidAppThread::AndroidAppThread( ThreadUID parentUID,
	                                    ThreadCoreInfo* threadCoreInfo,
	                                    SharedExecutionState* sharedExecutionState,
	                                    AppObject* appObject)
	: ActiveThread(parentUID, threadCoreInfo, sharedExecutionState, "AndroidAppThread")
	, _appObject(appObject)
	{ }
	

	AndroidAppThread::~AndroidAppThread()
	{ }


	void AndroidAppThread::Entry()
	{
	}


}
