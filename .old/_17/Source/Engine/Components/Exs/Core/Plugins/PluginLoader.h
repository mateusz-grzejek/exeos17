
#ifndef __Exs_Core_PluginLoader_H__
#define __Exs_Core_PluginLoader_H__

#include "PluginBase.h"


namespace Exs
{


	///<summary>
	/// Implements validation of plugin data during its loading and initialization. By default, no validation is performed.
	/// If certain plugin and/or group of plugins require additional checking of its internal structure, you should inherit
	/// this class, overload required function and specify pointer to its instance when calling <c>PluginSystem::LoadPlugin()</c>.
	///</summary>
	class EXS_LIBCLASS_CORE PluginValidator
	{
		friend class PluginLoader;
		
	protected:
		///<summary>
		/// Validates plugin base info. Failure causes loading process to be aborted.
		///</summary>
		///
		///<return><c>RSC_Success</c> on success, <c>RSC_Plugin_Validation_Failed</c> on failure. </return>
		virtual Result ValidateModuleBaseInfo(const PluginModuleBaseInfo* moduleBaseInfo);
		
		///<summary>
		/// Validates plugin interface. Failure causes loading process to be aborted.
		///</summary>
		///
		///<return><c>RSC_Success</c> on success, <c>RSC_Plugin_Validation_Failed</c> on failure. </return>
		virtual Result ValidateModuleInterface(const PluginModuleInterface* moduleInterface);
	};

	
	///<summary>
	///</summary>
	class EXS_LIBCLASS_CORE PluginLoader
	{
		EXS_DECLARE_NONCOPYABLE(PluginLoader);

		friend class PluginSystem;

	protected:
		PluginSystem*  _pluginSystem;
		Result         _lastLoadingResult;

	public:
		PluginLoader(PluginSystem* pluginSystem);
		virtual ~PluginLoader();
		
		///<summary>
		/// Initializes plugin identified by 'pluginBaseInfo' by calling its init procedure (exposed by the
		/// plugin DLL). If non-NULL validator is specified, loader first validates plugin data and interface.
		/// Plugin object which is returned is created by the init proc inside DLL of the plugin.
		///</summary>
		///<param name="pluginBaseInfo"> Core data of the plugin, loaded using LoadPlugin() method. </param>
		///<param name="manager"> Manager of plugins registered for this plugin type. </param>
		///<param name="validator"> Custom validator object, used to validate plugin data and interface (optional). </param>
		Plugin* Initialize(const PluginBaseInfo& pluginBaseInfo, PluginManager* manager, PluginValidator* validator);
		
		///<summary>
		/// Releases plugin by exetuing its release procedure (exposed by the plugin DLL).
		///</summary>
		///<param name="pluginBaseInfo"> Core data of the plugin, loaded using LoadPlugin() method. </param>
		///<param name="plugin"> Plugin object to be unloaded. </param>
		void Release(const PluginBaseInfo& pluginBaseInfo, Plugin* plugin);
		
		///<summary>
		///</summary>
		Result LoadPlugin(const std::string& pluginFile, PluginBaseInfo* pluginBaseInfo);
		
	private:
		// Called during initialization after plugin is successfuly initialized.
		virtual void OnPluginInitialize(const PluginBaseInfo& pluginBaseInfo, Plugin* plugin);

		// Called during unloading before plugin is release by its release procedure.
		virtual void OnPluginRelease(const PluginBaseInfo& pluginBaseInfo, Plugin* plugin);

		// Validates plugin components using provided validator object.
		Result Validate(const PluginModuleBaseInfo* moduleBaseInfo, const PluginModuleInterface* moduleInterface, PluginValidator* validator);
	};


}


#endif /* __Exs_Core_PluginLoader_H__ */
