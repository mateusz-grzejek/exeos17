
#ifndef __Exs_Core_PluginBase_H__
#define __Exs_Core_PluginBase_H__

#include <Exs/Common/HashCode.h>
#include <Exs/Core/System/DynamicLibrary.h>


// Name of global instance of PluginModuleInterface structure, exported by the plugin.
#define EXS_PLUGIN_MODULE_BASE_INFO_REF_NAME Exs_plugin_base_info

// Name of global instance of PluginModuleInterface structure, exported by the plugin.
// It contains public interface required by the plugin system.
#define EXS_PLUGIN_MODULE_INTERFACE_REF_NAME Exs_plugin_interface

//
#define EXS_PLUGIN_MODULE_BASE_INFO_REF_NAME_STR \
	EXS_MKSTR2(EXS_PLUGIN_MODULE_BASE_INFO_REF_NAME)

//
#define EXS_PLUGIN_MODULE_INTERFACE_REF_NAME_STR \
	EXS_MKSTR2(EXS_PLUGIN_MODULE_INTERFACE_REF_NAME)


namespace Exs
{


	class PluginLoader;
	class PluginManager;
	class PluginSystem;
	
	ExsDeclareRefPtrClass(Plugin);


	/// Internal, implementation-defined type used for unambiguous plugin identification.
	typedef Uint32 Plugin_id_value_t;

	// Internal, implementation-defined type used for unambiguous plugin identification.
	typedef Plugin_id_value_t PluginID;
	
	// Type used to store enum-like value which represents type of a plugin.
	typedef Enum Plugin_type_t;
	
	//
	typedef Plugin* (__cdecl *PluginOnInitProc)(const DynamicLibrary*, PluginManager*);
	
	//
	typedef void (__cdecl *PluginOnReleaseProc)(const DynamicLibrary*, Plugin*);


	/// Special values which PluginID may store.
	enum : Plugin_id_value_t
	{
		// Represents invalid plugin ID. If a plugin has this id set, it should be considered invalid and/or uninitialized.
		Plugin_ID_Invalid = stdx::limits<Plugin_id_value_t>::max_value
	};


	enum : Enum
	{
		RSC_Plugin_Err_Missing_Module_Data,
		RSC_Plugin_Manager_Not_Found,
		RSC_Plugin_Initialization_Failed
	};

	
	///<summary>
	///</summary>
	struct PluginMetadata
	{
	};

	
	///<summary>
	/// Contains basic information about a plugin and its module. It is exposed by the plugin's library and queried
	/// during loading process. All information stored in this structure can be assumed to be persistent (they are
	/// accessible as long as plugin is loaded) and read-only (an attempt to modify external instance of this structure
	/// results in Undefined Behavior).
	///</summary>
	struct PluginModuleBaseInfo
	{
		const char*     name; // Name of the plugin.
		const char*     description; // Short description of the plugin.
		const char*     keywords; // Keywords used to find the plugin (case insensitive).
		const char*     uuid; // UUID of the plugin.
		Plugin_type_t   type; // Type of the plugin supplied by the module.
		Uint32          archID; // ID of architecture, for which the plugin has been built.
		const char*     archName; // Name of architecture, for which the plugin has been built.
		const char*     versionStr; // String representation of the version of the plugin.
		PluginMetadata  metadata; // Pointer to metadata - internal data structure used for plugins validation.
	};

	
	///<summary>
	/// Contains interface specification of the plugin. This interface is consisted of a certain number of public functions
	/// which are used to perform certain operations on plugin's module.
	///</summary>
	struct PluginModuleInterface
	{
		PluginOnInitProc     onInitProc; // Init procedure, responsible for plugin creation, initialization and registration.
		PluginOnReleaseProc  onReleaseProc; // Release procedure, responsible for destroying plugin object and freeing resources.
	};

	
	///<summary>
	///</summary>
	struct PluginBaseInfo
	{
		DynamicLibrary                dllLibray; // Module (dynamic library) plugin is loaded from.
		const PluginModuleBaseInfo*   moduleBaseInfo; // Basic info about plugin (read-only pointer to data inside the DLL).
		const PluginModuleInterface*  moduleInterface; // Interface exposed by the plugin (read-only pointer to data inside the DLL).
	};


	template <PluginType>
	struct PluginTypeProperties;


	// Registers information about plugin type.
	// - pluginTypeTag: enum constant representing plugin type
	// - managerType: type (class) of manager handling plugins of registered type
	// - pluginType: type (class) of plugins
#define EXS_PLUGIN_REGISTER_PLUGIN_TYPE(pluginTypeTag, managerType, pluginType) \
	template <> struct PluginTypeProperties<pluginTypeTag> \
	{ \
		typedef managerType ManagerType; \
		typedef pluginType PluginType; \
	};

	
	///<summary>
	///</summary>
	class PluginHandle
	{
		friend class PluginSystem;

	private:
		Plugin*   _plugin;
		PluginID  _pluginID;

	public:
		PluginHandle()
		: _plugin(nullptr)
		, _pluginID(Plugin_ID_Invalid)
		{ }

		PluginHandle(PluginHandle&& source)
		: PluginHandle()
		{
			this->Swap(source);
		}

		PluginHandle(const PluginHandle& origin)
		: PluginHandle()
		{
			if ((origin._plugin != nullptr) && AddPluginActiveRef(origin._plugin))
			{
				this->_plugin = origin._plugin;
				this->_pluginID = origin._pluginID;
			}
		}
		
		PluginHandle(std::nullptr_t)
		: PluginHandle()
		{ }

		~PluginHandle()
		{
			this->Release();
		}

		PluginHandle& operator=(PluginHandle&& rhs)
		{
			if (this != &rhs)
				PluginHandle(std::move(rhs)).Swap(*this);
		
			return *this;
		}

		PluginHandle& operator=(const PluginHandle& rhs)
		{
			if (this != &rhs)
				PluginHandle(rhs).Swap(*this);
		
			return *this;
		}

		PluginHandle& operator=(std::nullptr_t)
		{
			this->Release();
			return *this;
		}

		operator bool() const
		{
			return this->_plugin != nullptr;
		}
		
		Plugin* operator->() const
		{
			ExsDebugAssert( this->_plugin != nullptr );
			return this->_plugin;
		}
		
		Plugin& operator*() const
		{
			ExsDebugAssert( this->_plugin != nullptr );
			return *(this->_plugin);
		}

		void Release()
		{
			if (this->_plugin != nullptr)
			{
				RemovePluginActiveRef(this->_plugin);
				this->_plugin = nullptr;
				this->_pluginID = Plugin_ID_Invalid;
			}
		}
		
		PluginID GetID() const
		{
			return this->_pluginID;
		}
		
		Plugin* GetPlugin() const
		{
			return this->_plugin;
		}
		
		void Swap(PluginHandle& other)
		{
			std::swap(this->_plugin, other._plugin);
			std::swap(this->_pluginID, other._pluginID);
		}

	private:
		PluginHandle(Plugin* plugin, PluginID pluginID)
		: _plugin(plugin)
		, _pluginID(pluginID)
		{ }
		
		static PluginHandle Create(Plugin* plugin, PluginID pluginID)
		{
			ExsDebugAssert( plugin != nullptr );
			ExsDebugAssert( GetPluginActiveRef(plugin) == 1 );
			return PluginHandle(plugin, pluginID);
		}
		
		/// Increments plugin use count if it is not zero already. Returns true on success or false otherwise.
		EXS_LIBAPI_CORE static bool AddPluginActiveRef(Plugin* plugin);

		/// Increments plugin use count if it is not zero already. Returns true on success or false otherwise.
		EXS_LIBAPI_CORE static void RemovePluginActiveRef(Plugin* plugin);

		/// Increments plugin use count if it is not zero already. Returns true on success or false otherwise.
		EXS_LIBAPI_CORE static Uint32 GetPluginActiveRef(Plugin* plugin);
	};


	inline void swap(PluginHandle& left, PluginHandle& right)
	{
		left.Swap(right);
	}
	

	namespace PluginUtils
	{
		
		/// Generates plugin ID using specified plugin name and optional 32-bit additional key.
		inline PluginID CreatePluginID(const char* pluginName, Uint32 extValue = 0)
		{
			auto nameHash = GetHashCode<DJB2>(pluginName);
			return static_cast<PluginID>(nameHash.value);
		}

	}


}


#endif /* __Exs_Core_PluginBase_H__ */
