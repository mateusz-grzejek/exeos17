
#ifndef __Exs_Core_CoreMessageBase_H__
#define __Exs_Core_CoreMessageBase_H__

#include "MessageBase.h"
#include "../Concurrency/ConcrtCommon.h"


#if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
#  define EXS_CORE_MESSAGE_ATTACH_UID 1
#else
#  define EXS_CORE_MESSAGE_ATTACH_UID 0
#endif


namespace Exs
{


	struct CoreMessageSendRequest;
	struct CoreAsyncMessageSendRequest;

	class CoreMessage;
	class CoreAsyncMessage;
	class CoreAsyncMessageResponse;
	class CoreAsyncMessageSharedResponseState;
	class CoreNotification;


  #if ( EXS_CORE_MESSAGE_ATTACH_UID )
	using CoreMessageBase = MessageBaseWithUID;
  #else
	using CoreMessageBase = MessageBase;
  #endif


	// Forward declaration of CoreAsyncMessageHandle.
	using CoreAsyncMessageHandle = SharedMessageHandle<CoreAsyncMessage, SharedMessageType::Ref_Counted>;

	// Forward declaration of CoreAsyncMessageResponseHandle.
	using CoreAsyncMessageResponseHandle = SharedMessageHandle<CoreAsyncMessageResponse, SharedMessageType::Ref_Counted>;

	// Forward declaration of CoreAsyncMessageHandle.
	template <class Message_t>
	using CoreAsyncMessageHandleEx = SharedMessageHandle<Message_t, SharedMessageType::Ref_Counted>;

	// Forward declaration of CoreAsyncMessageHandle.
	template <class Response_t>
	using CoreAsyncMessageResponseHandleEx = SharedMessageHandle<Response_t, SharedMessageType::Ref_Counted>;

	// Base queue type used to store async messages (via shared message handles).
	using CoreAsyncMessageQueue = MessageQueue<CoreAsyncMessage, CoreAsyncMessageHandle>;

	// Type of function which may be registered as a message handler.
	using CoreMessageHandlerFunction = std::function<void(const CoreMessage&)>;

	// Type of function which may be registered as a notification handler.
	using CoreNotificationHandlerFunction = std::function<void(const CoreNotification&)>;


	///<summary>
	///</summary>
	struct CoreMessageSendRequest
	{
		struct DestinationInfo
		{
			//
			U64ID controllerID = ID64_None;

			//
			ThreadRefID threadRefID;
		};

		DestinationInfo destination;
	};


	///<summary>
	///</summary>
	struct CoreAsyncMessageSendRequest : public CoreMessageSendRequest
	{
		struct ResponseRequestInfo
		{
			bool active;

			Milliseconds waitTimeout;
		};

		ResponseRequestInfo responseRequest;
	};


	///<summary>
	///</summary>
	struct CoreAsyncMessageSendResult
	{
		Result code;

		CoreAsyncMessageResponseHandle responseHandle;
	};


	///<summary>
	///</summary>
	class CoreMessageHandler
	{
	public:
		virtual void HandleMessage(const CoreMessage&) = 0;
	};


	///<summary>
	///</summary>
	class CoreNotificationHandler
	{
	public:
		virtual void HandleNotification(const CoreNotification&) = 0;
	};


};


#endif /* __Exs_Core_CoreMessageBase_H__ */
