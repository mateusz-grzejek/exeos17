
#include <Exs/Core/Intercom/CoreMessageReceiver.h>
#include <Exs/Core/Intercom/CoreMessageController.h>


namespace Exs
{


	CoreMessageReceiver::CoreMessageReceiver(CoreMessageController& controller)
	: _messageCotroller(&controller)
	, _syncObject(controller.GetThread())
	{ }


	void CoreMessageReceiver::PostMessage(const CoreAsyncMessageHandle& messageHandle)
	{
		this->_messageQueue.Push(messageHandle);
	}


	void CoreMessageReceiver::PostMessageAndNotify(const CoreAsyncMessageHandle& messageHandle)
	{
		this->_messageQueue.Push(messageHandle);
		this->_syncObject.NotifyOne();
	}


	void CoreMessageReceiver::Notify()
	{
		this->_syncObject.NotifyOne();
	}


	CoreAsyncMessageHandle CoreMessageReceiver::PeekMessage()
	{
		if (this->_messageQueue.IsEmpty())
		{
			return nullptr;
		}

		return this->_messageQueue.FetchUnchecked();
	}


	CoreAsyncMessageHandle CoreMessageReceiver::WaitForMessage()
	{
		if (this->_messageQueue.IsEmpty())
		{
			// We need to catch queue inside lambda to have access in wait predicate.
			auto& messageQueue = this->_messageQueue;
			
			// Wait until sync object is signaled.
			this->_syncObject.Wait([&messageQueue]() -> bool {
				return !messageQueue.IsEmpty();
			});

			// Note, that sync object may be signaled even if no message is inserted into the queue.
			// It can happen, for example, when Interrupt() or Abort() is called on the internal SO.

			if (this->_messageQueue.IsEmpty())
			{
				return nullptr;
			}
		}

		return this->_messageQueue.FetchUnchecked();
	}


	CoreAsyncMessageHandle CoreMessageReceiver::WaitForMessage(const Milliseconds& timeout)
	{
		if (this->_messageQueue.IsEmpty())
		{
			// We need to catch queue inside lambda to have access in wait predicate.
			auto& messageQueue = this->_messageQueue;

			// Wait until sync object is signaled.
			this->_syncObject.WaitFor(timeout, [&messageQueue]() -> bool {
				return !messageQueue.IsEmpty();
			});

			// Note, that sync object may be signaled even if no message is inserted into the queue.
			// It can happen, for example, when Interrupt() or Abort() is called on the internal SO.

			if (this->_messageQueue.IsEmpty())
			{
				return nullptr;
			}
		}

		return this->_messageQueue.FetchUnchecked();
	}


}
