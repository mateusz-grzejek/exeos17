
#ifndef __Exs_Core_CoreMessageAsync_H__
#define __Exs_Core_CoreMessageAsync_H__

#include "CoreMessageBase.h"
#include "../Concurrency/SyncObject.h"


namespace Exs
{


	class CoreAsyncMessageSharedResponseState;


	///
	static constexpr auto Core_message_system_sync_state_update_mode = ThreadSyncStateUpdateMode::No_Update;

	///
	using CoreMessageSystemSyncObject = InternalSyncObject<Core_message_system_sync_state_update_mode>;

	///
	using CoreMessageSystemLock = CoreMessageSystemSyncObject::LockType;


};


#endif /* __Exs_Core_CoreMessageAsync_H__ */
