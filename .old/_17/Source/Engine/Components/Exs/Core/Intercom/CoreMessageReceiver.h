
#ifndef __Exs_Core_CoreAsyncMessageReceiver_H__
#define __Exs_Core_CoreAsyncMessageReceiver_H__

#include "CoreMessageAsync.h"
#include "CoreMessageQueue.h"


namespace Exs
{


	class CoreMessageController;
	class SyncObject;


	class EXS_LIBCLASS_CORE CoreMessageReceiver
	{
		friend class CoreMessageController;
		friend class CoreMessageDispatcher;

	private:
		CoreMessageController*           _messageController;
		CoreMessageSystemSyncObject      _syncObject;
		CoreAsyncMessageInternalQueue    _messageQueue;

	public:
		CoreMessageReceiver(CoreMessageController& controller);

		///
		CoreMessageSystemLock& GetLock();

	friendapi: // Dispatcher
		///
		void PostMessage(const CoreAsyncMessageHandle& messageHandle);

		///
		void PostMessageAndNotify(const CoreAsyncMessageHandle& messageHandle);

		///
		void Notify();

	friendapi: // Controller
		///
		CoreAsyncMessageHandle PeekMessage();

		///
		CoreAsyncMessageHandle WaitForMessage();

		///
		CoreAsyncMessageHandle WaitForMessage(const Milliseconds& timeout);
	};


	inline CoreMessageSystemLock& CoreMessageReceiver::GetLock()
	{
		return this->_syncObject.GetLock();
	}


}


#endif /* __Exs_Core_CoreAsyncMessageReceiver_H__ */
