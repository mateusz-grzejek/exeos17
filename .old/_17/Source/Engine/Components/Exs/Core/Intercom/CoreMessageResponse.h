
#ifndef __Exs_Core_CoreMessageResponse_H__
#define __Exs_Core_CoreMessageResponse_H__

#include "CoreMessage.h"
#include "CoreMessageAsync.h"
#include "../Concurrency/LockedResource.h"


namespace Exs
{


	template <class Response_t>
	using CoreAsyncMessageLockedResponse = LockedResource<Response_t, SyncLock>;


	///<summary>
	///</summary>
	class CoreAsyncMessageResponse : public SharedMessageProxy<CoreMessage, SharedMessageType::Ref_Counted>
	{
	private:
		CoreAsyncMessage* _originalMessage;

	public:
		CoreAsyncMessageResponse(Message_code_t responseCode, CoreAsyncMessage* originalMessage)
			: SharedMessageProxy(responseCode)
			, _originalMessage(originalMessage)
		{ }
	};


	///<summary>
	///</summary>
	class CoreAsyncMessageSharedResponseState
	{
		friend class CoreMessageSender;

	private:
		CoreMessageSystemSyncObject*    _syncObject;
		std::atomic<CoreAsyncMessage*>  _respondedMessage;
		CoreAsyncMessageResponseHandle  _responseHandle;

	public:
		CoreAsyncMessageSharedResponseState(CoreMessageSystemSyncObject* syncObject);

		template <class Response_t = CoreAsyncMessageResponse>
		CoreAsyncMessageLockedResponse<Response_t> CreateResponse(CoreAsyncMessage& message)
		{
			SyncLock lock{ this->GetLock() };
			{
				if ((message.GetResponseState() == this) && (this->GetCurrentMessage() == &message))
				{
					this->_responseHandle = CreateSharedMessage<Response_t>();
				}
			}
		}

		template <class Response_t>
		void SetResponseReady(CoreAsyncMessageLockedResponse<Response_t>& response)
		{
		}

		void Reset();

		bool CheckRequestStatus(CoreAsyncMessage& message);

		CoreMessageSystemLock& GetLock();

		bool IsResponseSet() const;

		CoreAsyncMessageResponseHandle GetResponse() const;

	friendapi: // CoreMessageSender
			   // Sets message which is currently being responded.
		void SetCurrentMessage(CoreAsyncMessage& message);

		//
		CoreAsyncMessage* GetCurrentMessage() const;
	};


};


#endif /* __Exs_Core_CoreMessageResponse_H__ */
