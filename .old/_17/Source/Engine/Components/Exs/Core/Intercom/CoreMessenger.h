
#ifndef __Exs_Core_CoreMessenger_H__
#define __Exs_Core_CoreMessenger_H__

#include "CoreMessageQueue.h"
#include <stdx/assoc_array.h>


namespace Exs
{


	class CoreAsyncMessageExternalQueue;
	class CoreAsyncMessageInternalQueue;
	class CoreMessageController;
	class CoreMessageDispatcher;


	enum : U64ID
	{
		ID_Core_Message_Controller_Thread_Default = 0x777
	};


	///<summary>
	/// Implements common interface used to interact with core messaging system. Controller object is used in thread-local
	/// basis (only a thread which created the controller may use it), but there may be multiple controllers per thread.
	/// More than one controller may have the same id, but they must all be used by different threads.
	///</summary>
	class EXS_LIBCLASS_CORE CoreMessenger
	{
	public:
		enum class HandlerOverwritePolicy : Enum
		{
			Overwrite,
			No_Overwrite,
			Unspecified,
			Default = static_cast<Enum>(No_Overwrite)
		};

	private:
		struct HandlerState
		{ };

		using RegisteredHandlerList = stdx::assoc_array<Message_code_t, HandlerState>;

	private:
		CoreMessageController*         _controller; // Controller of the parent thread.
		CoreMessageDispatcher*         _msgDispatcher; // Message dispatcher.
		ThreadUID                      _threadUID;
		U64ID                          _id; // ID of the controller.
		CoreAsyncMessageExternalQueue  _messageQueue;
		RegisteredHandlerList          _registeredMessageHandlers;
		RegisteredHandlerList          _registeredNotificationHandlers;
		HandlerOverwritePolicy         _handlerOverwritePolicy;

	public:
		CoreMessenger(CoreMessageController& controller, U64ID id);

		CoreAsyncMessageHandle PeekNextMessage();

		Result SendAsyncMessage(ThreadRefID threadRefID, const CoreAsyncMessageHandle& messageHandle);
		Result SendAsyncMessage(ThreadRefID threadRefID, U64ID controllerID, const CoreAsyncMessageHandle& messageHandle);
		CoreAsyncMessageSendResult SendAsyncMessage(const CoreAsyncMessageSendRequest& request, const CoreAsyncMessageHandle& messageHandle);

		Result SendImmediateMessage(ThreadRefID threadRefID, const CoreMessage& message);
		Result SendImmediateMessage(ThreadRefID threadRefID, U64ID controllerID, const CoreMessage& message);
		Result SendImmediateMessage(const CoreMessageSendRequest& request, const CoreMessage& message);

		Result SendNotification(ThreadRefID threadRefID, const CoreNotification& notification);
		Result SendNotification(ThreadRefID threadRefID, U64ID controllerID, const CoreNotification& notification);
		Result SendNotification(const CoreMessageSendRequest& request, const CoreNotification& notification);

		void RegisterMessageHandler(Message_code_t messageCode, const CoreMessageHandlerFunction& handlerFunction, HandlerOverwritePolicy overwritePolicy = HandlerOverwritePolicy::Unspecified);
		void RegisterMessageHandler(Message_code_t messageCode, CoreMessageHandler* handler, HandlerOverwritePolicy overwritePolicy = HandlerOverwritePolicy::Unspecified);

		template <typename F, typename... Args>
		void RegisterMessageHandler(Message_code_t messageCode, F&& callback, Args&&... args, HandlerOverwritePolicy overwritePolicy = HandlerOverwritePolicy::Unspecified);

		void UnregisterMessageHandler(Message_code_t messageCode);
		void UnregisterAllMessageHandlers();

		void RegisterNotificationHandler(Message_code_t notificationCode, const CoreNotificationHandlerFunction& handlerFunction, HandlerOverwritePolicy overwritePolicy = HandlerOverwritePolicy::Unspecified);
		void RegisterNotificationHandler(Message_code_t notificationCode, CoreNotificationHandler* handler, HandlerOverwritePolicy overwritePolicy = HandlerOverwritePolicy::Unspecified);

		template <typename F, typename... Args>
		void RegisterNotificationHandler(Message_code_t notificationCode, F&& callback, Args&&... args, HandlerOverwritePolicy overwritePolicy = HandlerOverwritePolicy::Unspecified);

		void UnregisterNotificationHandler(Message_code_t notificationCode);
		void UnregisterAllNotificationHandlers();

		void SetHandlerOverwritePolicy(HandlerOverwritePolicy policy);

		HandlerOverwritePolicy GetHandlerOverwritePolicy() const;

		U64ID GetID() const;

		bool HasIncomingMessages() const;
	};


	inline Result CoreMessenger::SendAsyncMessage(ThreadRefID threadRefID, const CoreAsyncMessageHandle& messageHandle)
	{
		return this->SendAsyncMessage(threadRefID, ID64_None, messageHandle);
	}

	inline Result CoreMessenger::SendImmediateMessage(ThreadRefID threadRefID, const CoreMessage& message)
	{
		return this->SendImmediateMessage(threadRefID, ID64_None, message);
	}

	inline Result CoreMessenger::SendNotification(ThreadRefID threadRefID, const CoreNotification& notification)
	{
		return this->SendNotification(threadRefID, ID64_None, notification);
	}

	inline void CoreMessenger::RegisterMessageHandler(Message_code_t messageCode, CoreMessageHandler* handler, HandlerOverwritePolicy overwritePolicy)
	{
		this->RegisterMessageHandler(messageCode, std::bind([handler](const CoreMessage& message) -> void {
			return handler->HandleMessage(message);
		}, std::placeholders::_1), overwritePolicy);
	}

	template <typename F, typename... Args>
	inline void CoreMessenger::RegisterMessageHandler(Message_code_t messageCode, F&& callback, Args&&... args, HandlerOverwritePolicy overwritePolicy)
	{
		this->RegisterMessageHandler(messageCode, std::bind(std::forward<F>(callback), std::placeholders::_1, std::forward<Args>(args)...), overwritePolicy);
	}

	inline void CoreMessenger::RegisterNotificationHandler(Message_code_t messageCode, CoreNotificationHandler* handler, HandlerOverwritePolicy overwritePolicy)
	{
		this->RegisterNotificationHandler(messageCode, std::bind([handler](const CoreNotification& notification) -> void {
			return handler->HandleNotification(notification);
		}, std::placeholders::_1), overwritePolicy);
	}

	template <typename F, typename... Args>
	inline void CoreMessenger::RegisterNotificationHandler(Message_code_t notificationCode, F&& callback, Args&&... args, HandlerOverwritePolicy overwritePolicy)
	{
		this->RegisterNotificationHandler(notificationCode, std::bind(std::forward<F>(callback), std::placeholders::_1, std::forward<Args>(args)...), overwritePolicy);
	}

	inline CoreMessenger::HandlerOverwritePolicy CoreMessenger::GetHandlerOverwritePolicy() const
	{
		return this->_handlerOverwritePolicy;
	}

	inline U64ID CoreMessenger::GetID() const
	{
		return this->_id;
	}


}


#endif /* __Exs_Core_CoreMessenger_H__ */
