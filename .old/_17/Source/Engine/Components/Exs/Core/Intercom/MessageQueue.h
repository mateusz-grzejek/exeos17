
#ifndef __Exs_Core_MessageQueue_H__
#define __Exs_Core_MessageQueue_H__

#include "MessageBase.h"


namespace Exs
{


	///<summary>
	///</summary>
	template <class Message_t, class Handle_t>
	class MessageQueue
	{
	public:
		typedef MessageQueue<Message_t, Handle_t> MyType;
		typedef std::queue<Handle_t> InternalQueueType;

	private:
		InternalQueueType _messages; // Internal queue which stores enqueued messages.

	public:
		MessageQueue() = default;

		///<summary>
		///</summary>
		void Push(Handle_t&& message)
		{
			this->_messages.push(std::move(message));
		}

		///<summary>
		///</summary>
		void Push(const Handle_t& message)
		{
			this->_messages.push(message);
		}

		///<summary>
		///</summary>
		void Pop()
		{
			ExsDebugAssert( !this->IsEmpty() );
			this->_messages.pop();
		}

		///<summary>
		///</summary>
		Handle_t Fetch()
		{
			Handle_t message{};

			if (!this->IsEmpty())
			{
				message = this->_messages.front();
				this->_messages.pop();
			}

			return message;
		}

		///<summary>
		///</summary>
		bool Fetch(Handle_t* outputHandle)
		{
			bool result = false;

			if (!this->IsEmpty())
			{
				*outputHandle = this->_messages.front();
				this->_messages.pop();
				result = true;
			}

			return result;
		}

		///<summary>
		///</summary>
		Handle_t FetchUnchecked()
		{
			Handle_t message = this->_messages.front();
			this->_messages.pop();
			return message;
		}

		///<summary>
		///</summary>
		void FetchUnchecked(Handle_t* outputHandle)
		{
			*outputHandle = this->_messages.front();
			this->_messages.pop();
		}

		///<summary>
		///</summary>
		const Handle_t& Front() const
		{
			return this->_messages.front();
		}

		///<summary>
		///</summary>
		Size_t Size() const
		{
			return this->_messages.size();
		}

		///<summary>
		///</summary>
		bool IsEmpty() const
		{
			return this->_messages.empty();
		}

		///<summary>
		///</summary>
		void Swap(MyType& other)
		{
			std::swap(this->_messages, other._messages);
		}
	};


}


#endif /* __Exs_Core_ThrMessageQueue_H__ */
