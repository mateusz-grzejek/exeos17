
#include <Exs/Core/Intercom/CoreMessageDispatcher.h>
#include <Exs/Core/Intercom/CoreMessage.h>
#include <Exs/Core/Intercom/CoreMessageController.h>
#include <Exs/Core/Concurrency/ThreadSystemManager.h>


namespace Exs
{


	Result CoreMessageDispatcher::Dispatch(const CoreMessageDispatchRequest& request, const CoreAsyncMessageHandle& messageHandle)
	{
		auto* controllerState = this->_GetControllerState(request.destination.threadRefID);
	}


	Result CoreMessageDispatcher::Dispatch(const CoreMessageDispatchRequest& request, const CoreMessage& message)
	{
	}


	Result CoreMessageDispatcher::Dispatch(const CoreMessageDispatchRequest& request, const CoreNotification& notification)
	{
	}


	Result CoreMessageDispatcher::RegisterController(ThreadUID threadUID, CoreMessageController* controller)
	{
		auto controllerIndex = threadUID.GetIndex();
		if (controllerIndex == Thread_Index_Invalid)
		{
			return ExsResult(RSC_Err_Invalid_Value);
		}

		auto& controllerEntry = this->_controllers[controllerIndex];
		ExsBeginCriticalSection(controllerEntry.accessLock);
		{
			auto* receiver = controller->GetReceiver();
			ExsDebugAssert( receiver != nullptr );

			for (auto& suspendedMessage : controllerEntry.suspendedMessages)
			{
				receiver->PostMessage(suspendedMessage);
			}

			controllerEntry.suspendedMessages.clear();
			controllerEntry.suspendedMessages.shrink_to_fit();

			controllerEntry.controller = controller;
			controllerEntry.threadUID = threadUID;
			controllerEntry.receiver = receiver;
		}
		ExsEndCriticalSection();

		return ExsResult(RSC_Success);
	}


	void CoreMessageDispatcher::UnregisterController(ThreadUID threadUID, CoreMessageController* controller)
	{
	}


	Result CoreMessageDispatcher::RegisterMessageHandler(CoreMessenger* messenger, Message_code_t messageCode, const CoreMessageHandlerFunction& handlerFunction, bool overwrite)
	{
		ExsBeginSharedCriticalSectionWithUniqueAccess(this->_accessLock);
		{
			return this->RegisterMessageHandler(messenger, messageCode, handlerFunction, Tag::noLock, overwrite);
		}
		ExsEndCriticalSection();
	}


	Result CoreMessageDispatcher::RegisterMessageHandler(CoreMessenger* messenger, Message_code_t messageCode, const CoreMessageHandlerFunction& handlerFunction, const NoLockTag&, bool overwrite)
	{
		// Retrieve group for this code using []. If it has not been created so far, it will be default-constructed.
		auto& messageHandlerGroup = this->_messageHandlerSubscriptionGroups[messageCode];

		if (!this->_AddHandler(messageHandlerGroup, messenger, messageCode, handlerFunction, overwrite))
		{
			// If false is returned here it means, that handler for this code has been
			// already registered by this messenger and 'overwrite' was set to false.
			return ExsResult(RSC_Err_Already_Registered);
		}

		return ExsResult(RSC_Success);
	}


	Result CoreMessageDispatcher::RegisterNotificationHandler(CoreMessenger* messenger, Message_code_t notificationCode, const CoreNotificationHandlerFunction& handlerFunction, bool overwrite)
	{
		ExsBeginSharedCriticalSectionWithUniqueAccess(this->_accessLock);
		{
			return this->RegisterNotificationHandler(messenger, notificationCode, handlerFunction, Tag::noLock, overwrite);
		}
		ExsEndCriticalSection();
	}


	Result CoreMessageDispatcher::RegisterNotificationHandler(CoreMessenger* messenger, Message_code_t notificationCode, const CoreNotificationHandlerFunction& handlerFunction, const NoLockTag&, bool overwrite)
	{
		// Retrieve group for this code using []. If it has not been created so far, it will be default-constructed.
		auto& notificationHandlerGroup = this->_notificationHandlerSubscriptionGroups[notificationCode];

		if (!this->_AddHandler(notificationHandlerGroup, messenger, notificationCode, handlerFunction, overwrite))
		{
			// If false is returned here it means, that handler for this code has been
			// already registered by this messenger and 'overwrite' was set to false.
			return ExsResult(RSC_Err_Already_Registered);
		}

		return ExsResult(RSC_Success);
	}


	void CoreMessageDispatcher::UnregisterMessageHandler(CoreMessenger* messenger, Message_code_t messageCode)
	{
		ExsBeginSharedCriticalSectionWithUniqueAccess(this->_accessLock);
		{
			return this->UnregisterMessageHandler(messenger, messageCode, Tag::noLock);
		}
		ExsEndCriticalSection();
	}


	void CoreMessageDispatcher::UnregisterMessageHandler(CoreMessenger* messenger, Message_code_t messageCode, const NoLockTag&)
	{
		auto& messageHandlerGroup = this->_notificationHandlerSubscriptionGroups[messageCode];
		this->_RemoveHandler(messageHandlerGroup, messenger);
	}


	void CoreMessageDispatcher::UnregisterNotificationHandler(CoreMessenger* messenger, Message_code_t notificationCode)
	{
		ExsBeginSharedCriticalSectionWithUniqueAccess(this->_accessLock);
		{
			return this->UnregisterNotificationHandler(messenger, notificationCode, Tag::noLock);
		}
		ExsEndCriticalSection();
	}


	void CoreMessageDispatcher::UnregisterNotificationHandler(CoreMessenger* messenger, Message_code_t notificationCode, const NoLockTag&)
	{
		auto& notificationHandlerGroup = this->_notificationHandlerSubscriptionGroups[notificationCode];
		this->_RemoveHandler(notificationHandlerGroup, messenger);
	}


	Result CoreMessageDispatcher::_PostAsyncMessage(const CoreMessageDispatchRequest& request, const CoreAsyncMessageHandle& messageHandle)
	{

	}

	Result CoreMessageDispatcher::_HandleMessage(const CoreMessageDispatchRequest& request, Message_code_t messageCode, const CoreMessage& message)
	{
		ExsBeginSharedCriticalSectionWithSharedAccess(this->_accessLock);
		{
			auto& messageHandlerGroup = this->_messageHandlerSubscriptionGroups[messageCode];
			if (!messageHandlerGroup.handlers.empty())
			{
				for (auto& handlerInfo : messageHandlerGroup.handlers)
				{
					handlerInfo.handlerFunction(message);
				}
			}
		}
		ExsEndCriticalSection();
	}


	Result CoreMessageDispatcher::_HandleNotification(const CoreMessageDispatchRequest& request, Message_code_t notificationCode, const CoreNotification& notification)
	{
		ExsBeginSharedCriticalSectionWithSharedAccess(this->_accessLock);
		{
			auto& notificationHandlerGroup = this->_notificationHandlerSubscriptionGroups[notificationCode];
			if (!notificationHandlerGroup.handlers.empty())
			{
				for (auto& handlerInfo : notificationHandlerGroup.handlers)
				{
					handlerInfo.handlerFunction(notification);
				}
			}
		}
		ExsEndCriticalSection();
	}


	CoreMessageDispatcher::ControllerState* CoreMessageDispatcher::_GetControllerState(ThreadRefID threadRefID)
	{
		auto threadUID = this->_threadSystemManager->QueryThreadUID(threadRefID);
		if (threadUID == Thread_UID_Invalid)
		{
			return nullptr;
		}

		auto threadIndex = threadUID.GetIndex();

		ExsBeginSharedCriticalSectionWithUniqueAccess(this->_accessLock);
		{
			auto& controllerState = this->_controllers[threadIndex];
			return &controllerState;
		}
		ExsEndCriticalSection();
	}


	template <typename Handler>
	bool CoreMessageDispatcher::_AddHandler(HandlerSubscriptionGroup<Handler>& handlerGroup, CoreMessenger* messenger, Message_code_t code, const Handler& handler, bool overwrite)
	{
		// Default constructed group has messageCode set to zero. If it's not zero, it was already initialized
		// and may contain handler for this code. Check this and proceed accordingly to overwrite policy.
		if (handlerGroup.messageCode != 0)
		{
			if (!handlerGroup.handlers.empty())
			{
				auto handlerInfoRef = std::find_if(handlerGroup.handlers.begin(), handlerGroup.handlers.end(), [messenger](const auto& handlerInfo) -> bool {
					return handlerInfo.messenger == messenger;
				});

				if (handlerInfoRef != handlerGroup.handlers.end())
				{
					// If handler for this code has already been registered
					// by this messenger, check specified overwrite policy.

					if (overwrite)
					{
						// If allowed, overwrite current handler.
						handlerInfoRef->handlerFunction = handler;
					}

					// In both cases, there is nothing more to do, return.
					return overwrite;
				}
			}
		}
		else
		{
			// Set code for the group if it's a new one.
			handlerGroup.messageCode = code;
		}

		// Either group is empty or handler has not been registered yet. Insert new entry.
		handlerGroup.handlers.push_back({ messenger, handler });

		return true;
	}


	template <typename Handler>
	void CoreMessageDispatcher::_RemoveHandler(HandlerSubscriptionGroup<Handler>& handlerGroup, CoreMessenger* messenger)
	{
		if (!handlerGroup.handlers.empty())
		{
			auto handlerInfoRef = std::find_if(handlerGroup.handlers.begin(), handlerGroup.handlers.end(), [messenger](const auto& handlerInfo) -> bool {
				return handlerInfo.messenger == messenger;
			});

			if (handlerInfoRef != handlerGroup.handlers.end())
			{
				handlerGroup.handlers.erase(handlerInfoRef);
			}
		}
	}


	/*void CoreMessageDispatcher::UnregisterAllMessengerHandlers(CoreMessenger* messenger)
	{

	}


	bool CoreMessageDispatcher::RegisterNotificationSubscription( CoreMessageController* controller,
	                                                              Message_code_t notificationCode,
	                                                              const CoreNotificationHandlerFunction& handlerFunction,
	                                                              bool overwrite)
	{
		ExsBeginSharedCriticalSectionWithUniqueAccess(this->_accessLock);
		{
			auto& subscription = this->_notificationHandlerSubscriptionGroups[notificationCode];
			if (subscription.messageCode != 0)
			{
				auto handlerInfoRef = std::find_if(subscription.handlers.begin(), subscription.handlers.end(), [](const auto& handlerInfo) -> bool {
					handlerInfo.controller == controller;
				});

				if (handlerInfoRef != subscription.handlers.end())
				{
					if (overwrite)
					{
						handlerInfoRef->handlerFunction = handlerFunction;
					}

					return overwrite;
				}
			}
			else
			{
				subscription.messageCode = notificationCode;
			}
		}
		ExsEndCriticalSection();
	}


	void CoreMessageDispatcher::UnregisterNotificationSubscription( CoreMessageController* messageController,
	                                                                Message_code_t notificationCode)
	{
		ExsBeginSharedCriticalSectionWithUniqueAccess(this->_accessLock);
		{
			auto subscriptionListRef = this->_notificationClientSubscriptions.find(notificationCode);
			if (subscriptionListRef != this->_notificationClientSubscriptions.end())
			{
				auto& subscriptionClients = subscriptionListRef->value.clients;
				auto clientInfoRef = std::find_if(subscriptionClients.begin(), subscriptionClients.end(), [](const auto& clientInfo) -> bool {
					clientInfo.controller == controller;
				});

				if (clientInfoRef != subscriptionClients.end())
				{
					subscriptionClients.erase(clientInfoRef);
				}
			}
		}
		ExsEndCriticalSection();
	}


	void CoreMessageDispatcher::RemoveSubscriptions(CoreMessageController* messageController)
	{
		ExsBeginSharedCriticalSectionWithUniqueAccess(this->_accessLock);
		{
			for (auto& subscriptionList : this->_notificationClientSubscriptions)
			{
				auto& subscriptionClients = subscriptionList.value.clients;
				auto clientInfoRef = std::find_if(subscriptionClients.begin(), subscriptionClients.end(), [](const auto& clientInfo) -> bool {
					clientInfo.controller == controller;
				});

				if (clientInfoRef != subscriptionClients.end())
				{
					subscriptionClients.erase(clientInfoRef);
				}
			}
		}
		ExsEndCriticalSection();
	}*/


}
