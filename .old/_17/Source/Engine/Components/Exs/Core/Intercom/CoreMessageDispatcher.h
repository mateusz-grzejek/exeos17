
#ifndef __Exs_Core_CoreMessageDispatcher_H__
#define __Exs_Core_CoreMessageDispatcher_H__

#include "CoreMessageQueue.h"
#include <stdx/assoc_array.h>
#include <stdx/sorted_array.h>


namespace Exs
{


	class CoreMessageController;
	class CoreMessageReceiver;
	class CoreMessenger;
	class Thread;
	class ThreadSystemManager;


	struct CoreMessageDispatchRequest
	{
		struct SourceInfo
		{
			CoreMessageController* controller;
			ThreadUID threadUID;
		};

		using DestinationInfo = CoreMessageSendRequest::DestinationInfo;

		SourceInfo source;
		DestinationInfo destination;
	};

	
	///<summary>
	///</summary>
	class EXS_LIBCLASS_CORE CoreMessageDispatcher
	{
		friend class CoreMessageController;
		friend class CoreMessageSender;
		friend class Thread;

	private:
		// Represents group of handlers registsred for a single message/notification code.
		template <typename Handler>
		struct HandlerSubscriptionGroup
		{
			struct HandlerInfo
			{
				// Pointer to messenger which registered the handler.
				CoreMessenger* messenger;

				// Handler function.
				Handler handlerFunction;
			};

			// Code of message/notification.
			Message_code_t messageCode = 0;

			// List of handlers registsred for this code.
			std::vector<HandlerInfo> handlers;
		};

		//
		struct MessengerState
		{
			U32ID messengerGroupID;
			U64ID messengerID;
			CoreAsyncMessageExternalQueue* messageQueue;
		};

		typedef stdx::assoc_array<Message_code_t, MessageHandlerSubscriptionGroup> MessageHandlerSubscriptionGroupList;

		//
		struct ControllerState
		{
			LightMutex accessLock;
			ThreadUID threadUID;
			CoreMessageReceiver* receiver;
			CoreMessageController* controller;
			std::queue<CoreAsyncMessageHandle> suspendedMessages;
		};

		using MessageHandlerSubscriptionGroup = HandlerSubscriptionGroup<CoreMessageHandlerFunction>;
		using NotificationHandlerSubscriptionGroup = HandlerSubscriptionGroup<CoreNotificationHandlerFunction>;

		typedef std::array<ControllerState, Config::CCRT_Thr_Max_Active_Threads_Num> ControllerList;
		typedef stdx::assoc_array<Message_code_t, MessageHandlerSubscriptionGroup> MessageHandlerSubscriptionGroupList;
		typedef stdx::assoc_array<Message_code_t, NotificationHandlerSubscriptionGroup> NotificationHandlerSubscriptionGroupList;

	private:
		ThreadSystemManager*                       _threadSystemManager;
		SharedMutex                                 _accessLock;
		ControllerList                              _controllers;
		MessageHandlerSubscriptionGroupList         _messageHandlerSubscriptionGroups;
		NotificationHandlerSubscriptionGroupList    _notificationHandlerSubscriptionGroups;

	friendapi: // CoreMessageSender
		Result Dispatch(const CoreMessageDispatchRequest& request, const CoreAsyncMessageHandle& messageHandle);

		Result Dispatch(const CoreMessageDispatchRequest& request, const CoreMessage& message);

		Result Dispatch(const CoreMessageDispatchRequest& request, const CoreNotification& notification);

	friendapi: // Thread
		Result RegisterController(ThreadUID threadUID, CoreMessageController* controller);

		void UnregisterController(ThreadUID threadUID, CoreMessageController* controller);

	friendapi: // Controller
		void AddMessenger(ThreadUID threadUID, CoreMessenger* messenger, CoreAsyncMessageExternalQueue* messageQueue);
		
		void RemoveMessenger(ThreadUID threadUID, CoreMessenger* messenger);

		Result RegisterMessageHandler(CoreMessenger* messenger, Message_code_t messageCode, const CoreMessageHandlerFunction& handlerFunction, bool overwrite = true);

		Result RegisterMessageHandler(CoreMessenger* messenger, Message_code_t messageCode, const CoreMessageHandlerFunction& handlerFunction, const NoLockTag&, bool overwrite = true);

		Result RegisterNotificationHandler(CoreMessenger* messenger, Message_code_t notificationCode, const CoreNotificationHandlerFunction& handlerFunction, bool overwrite = true);

		Result RegisterNotificationHandler(CoreMessenger* messenger, Message_code_t notificationCode, const CoreNotificationHandlerFunction& handlerFunction, const NoLockTag&, bool overwrite = true);

		void UnregisterMessageHandler(CoreMessenger* messenger, Message_code_t messageCode);

		void UnregisterMessageHandler(CoreMessenger* messenger, Message_code_t messageCode, const NoLockTag&);

		void UnregisterNotificationHandler(CoreMessenger* messenger, Message_code_t notificationCode);

		void UnregisterNotificationHandler(CoreMessenger* messenger, Message_code_t notificationCode, const NoLockTag&);

	private:
		Result _PostAsyncMessage(const CoreMessageDispatchRequest& request, const CoreAsyncMessageHandle& messageHandle);

		Result _HandleMessage(const CoreMessageDispatchRequest& request, Message_code_t messageCode, const CoreMessage& message);

		Result _HandleNotification(const CoreMessageDispatchRequest& request, Message_code_t notificationCode, const CoreNotification& notification);

		ControllerState* _GetControllerState(ThreadRefID threadRefID);

		template <typename Handler>
		bool _AddHandler(HandlerSubscriptionGroup<Handler>& handlerGroup, CoreMessenger* messenger, Message_code_t code, const Handler& handler, bool overwrite);

		template <typename Handler>
		void _RemoveHandler(HandlerSubscriptionGroup<Handler>& handlerGroup, CoreMessenger* messenger);
	};


}


#endif /* __Exs_Core_CoreMessageDispatcher_H__ */
