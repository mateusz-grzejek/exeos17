
#ifndef __Exs_Core_MessageBase_H__
#define __Exs_Core_MessageBase_H__

#include "../Prerequisites.h"


#define EXS_MSG_MESSAGE_CODE_NCID_RESERVED 0x4700000000000000
#define EXS_MSG_MESSAGE_CODE_CLASS_MASK 0x0000FFFF00000000
#define EXS_MSG_MESSAGE_CODE_IID_MASK 0x00000000FFFF0000


namespace Exs
{


	class MessageBase;
	class MessageBaseWithUID;

	template <class Message_t, class Handle_t>
	class MessageQueue;

	/// Type used to store the value of a message code.
	typedef Uint64 Message_code_t;

	/// Type used to store the value of a message class.
	typedef Uint32 Message_class_t;

	/// Type used to store the value of a message UID.
	typedef Uint64 Message_uid_value_t;

	/// Type used to store the value of a message priority.
	typedef Int32 Message_priority_value_t;

	/// Implementation-dependent type which represents message UID (Unique ID). It is
	/// guaranteed to fulfill all requirements of UID-Compatible type (see #TODO: what?).
	typedef Message_uid_value_t MessageUID;


	namespace EnumDef
	{

		//
		constexpr Uint16 Message_Code_Control_Key = 0x4701;

		//
		inline constexpr Message_class_t DeclareMessageClass(Uint16 clsid)
		{
			return (static_cast<Message_class_t>(Message_Code_Control_Key) << 16) | static_cast<Message_class_t>(clsid);
		}

		//
		inline constexpr Message_code_t DeclareMessageCode(Message_class_t msgClass, Uint16 internalID, Uint16 ext = 0)
		{
			return (static_cast<Message_code_t>(msgClass) << 32) | (static_cast<Message_code_t>(internalID) << 16) | ext;
		}

		//
		inline constexpr Message_class_t GetMessageClassFromCode(Message_code_t messageCode)
		{
			return truncate_cast<Message_class_t>(messageCode >> 32);
		}

		//
		inline constexpr Uint16 GetMessageCodeControlKey(Message_code_t messageCode)
		{
			return truncate_cast<Uint16>(messageCode >> 48);
		}

		//
		inline constexpr bool ValidateMessageCode(Message_code_t messageCode)
		{
			return GetMessageCodeControlKey(messageCode) == Message_Code_Control_Key;
		}

	}

	
	//
	enum : Message_uid_value_t
	{
		/// @refitem UID_Empty
		Message_UID_Empty = 0,

		/// @refitem UID_Invalid
		Message_UID_Invalid = stdx::limits<Message_uid_value_t>::max_value
	};


	// Common message classes.
	enum : Message_class_t
	{
		// Message is an internal message, used to implement basic functionality of the engine.
		Message_Class_Internal = EnumDef::DeclareMessageClass(0xE888),

		// Message has unspecified class.
		Message_Class_None = EnumDef::DeclareMessageClass(0)
	};


	///<summary>
	///</summary>
	enum class MessagePriority : Message_priority_value_t
	{
		Low,
		Default,
		High
	};


	///<summary>
	///</summary>
	enum class SharedMessageType : Enum
	{
		// Ref-counted message without reference counter attached (shared ptr).
		// These messages may be safely used in multi-threaded scenarios.
		Ext_Shared,

		// Ref-counted message reference counter attached to a message (intrusive ptr).
		// These messages may be safely used in multi-threaded scenarios.
		Ref_Counted,

		// Messages with unspecified internal implementation, optimized for single-threaded usage.
		// These messages MUST NOT be used in multi-threaded scenarios.
		Thread_Local,

		// By default, we ref-counted messages with embedded reference counter.
		Default = Ref_Counted
	};


	namespace Internal
	{

		// Shared messages are used with handles, that implement some form of
		// reference counting. This enum represents supported types of counters.
		enum class SharedMessageCounterType : Enum
		{
			External,
			Internal
		};

		// Used to map ref type of messages to one of shared types.
		template <SharedMessageType>
		struct GetSharedMessageCounterType;

		// Defines underlying type of a handle. Handle may be implemented as either stdx::intrusive_ptr<>
		// (Type::Ref_Counted/Thread_Local), std::shared_ptr<> (Type::Shared) or plain pointer (Type::Custom).
		template <class Message_t, SharedMessageCounterType>
		struct SharedMessageHandleUnderlyingTypeImpl;

		// Implements create method for a specified message type and handle type. It uses the most efficient
		// and/or safe method of creation for each handle type (e.g. std::make_shared() for std::shared_ptr).
		template <class Message_t, SharedMessageCounterType>
		struct SharedMessageHandleCreateDispatcherImpl;

		//
		template <class Message_t, SharedMessageType SMT>
		using SharedMessageHandleUnderlyingType = SharedMessageHandleUnderlyingTypeImpl<Message_t, GetSharedMessageCounterType<SMT>::value>;

		//
		template <class Message_t, SharedMessageType SMT>
		using SharedMessageHandleCreateDispatcher = SharedMessageHandleCreateDispatcherImpl<Message_t, GetSharedMessageCounterType<SMT>::value>;

	};


	// Represents handle used to store a message of specified type. Message_internal_type may be specified explicitly
	// required e.g. in forward declaration of a handle type or left unspecified to use proper ref type from Message_t.
	template <class Message_t, SharedMessageType SMT = Message_t::sharedMessageType>
	using SharedMessageHandle = typename Internal::SharedMessageHandleUnderlyingType<Message_t, SMT>::Type;

	//
	template <class Message_t, SharedMessageType SMT = Message_t::sharedMessageType>
	using SharedMessageCreateDispatcher = Internal::SharedMessageHandleCreateDispatcher<Message_t, SMT>;


	// Declares handle type for specified type of message using definition of a message class. Full definition of a
	// message class (specified as first argument) is required for this macro to work.
	#define ExsMsgDeclareSharedMessageHandle(messageType) \
		class messageType; typedef SharedMessageHandle<messageType> messageType##SharedHandle;

	// Declares handle type for specified type of message using definition of a message class. Type of the message
	// is specified explicitly, so definition of the message class is not required.
	#define ExsMsgDeclareSharedMessageHandleExplicit(messageType, sharedMessageTypeTag) \
		class messageType; typedef SharedMessageHandle<messageType, sharedMessageTypeTag> messageType##Handle;




	namespace Internal
	{

		template <>
		struct GetSharedMessageCounterType<SharedMessageType::Ext_Shared>
		{
			static constexpr auto value = SharedMessageCounterType::External;
		};

		template <>
		struct GetSharedMessageCounterType<SharedMessageType::Ref_Counted>
		{
			static constexpr auto value = SharedMessageCounterType::Internal;
		};

		template <>
		struct GetSharedMessageCounterType<SharedMessageType::Thread_Local>
		{
			static constexpr auto value = SharedMessageCounterType::Internal;
		};

		template <class Message_t>
		struct SharedMessageHandleUnderlyingTypeImpl<Message_t, SharedMessageCounterType::External>
		{
			typedef std::shared_ptr<Message_t> Type;
		};

		template <class Message_t>
		struct SharedMessageHandleUnderlyingTypeImpl<Message_t, SharedMessageCounterType::Internal>
		{
			typedef stdx::intrusive_ptr<Message_t> Type;
		};

		// Messages used as shared ptrs should be created using dedicated std::make_shared.
		template <class Message_t>
		struct SharedMessageHandleCreateDispatcherImpl<Message_t, SharedMessageCounterType::External>
		{
			template <class... Args>
			static std::shared_ptr<Message_t> Create(Args&&... args)
			{
				return std::make_shared<Message_t>(std::forward<Args>(args)...);
			}
		};

		// Messages used as intrusive ptrs should be created using dedicated stdx::make_intrusive.
		template <class Message_t>
		struct SharedMessageHandleCreateDispatcherImpl<Message_t, SharedMessageCounterType::Internal>
		{
			template <class... Args>
			static stdx::intrusive_ptr<Message_t> Create(Args&&... args)
			{
				return stdx::make_instrusive<Message_t>(std::forward<Args>(args)...);
			}
		};

		// Ref counter base class for Ref_Counted messages. Inherits from proper counter defined in stdx library.
		template <stdx::ref_counter_type Ref_counter_type>
		class IntrusiveRefCounter : public stdx::ref_counted_base<typename stdx::ref_counter_class<Ref_counter_type>::type>
		{ };

		// Common base class for all shared message types.
		template <SharedMessageType>
		class SharedMessageBase;

		// Ref counted messages require additional thread-safe reference counter.
		template <>
		class SharedMessageBase<SharedMessageType::Ext_Shared>
		{
		public:
			static constexpr auto sharedMessageType = SharedMessageType::Ext_Shared;
		};

		// Ref counted messages require additional thread-safe reference counter.
		template <>
		class SharedMessageBase<SharedMessageType::Ref_Counted> : public IntrusiveRefCounter<stdx::ref_counter_type::atomic_sync>
		{
		public:
			static constexpr auto sharedMessageType = SharedMessageType::Ref_Counted;
		};

		// Thread local messages require additional reference counter (but without multi-threading safety).
		template <>
		class SharedMessageBase<SharedMessageType::Thread_Local> : public IntrusiveRefCounter<stdx::ref_counter_type::non_atomic>
		{
		public:
			static constexpr auto sharedMessageType = SharedMessageType::Thread_Local;
		};

		// Helper class which implements generation of global message UIDs.
		struct MessageUIDGen
		{
			// UID counter value is stored within UID on 32 bits (32 bits are for message class).
			static constexpr Message_uid_value_t maxUIDCounter = stdx::limits<Message_uid_value_t>::max_value >> 32;

			//
			static inline MessageUID GenerateGlobalMessageUID(Message_code_t messageCode)
			{
				static std::atomic<Message_uid_value_t> uidCounter = 0;

				// Message class, higher 32 bits of the UID.
				auto messageClass = EnumDef::GetMessageClassFromCode(messageCode);

				// Current value of the UID counter, lower 32 bits of the UID.
				auto uidNumValue = uidCounter.fetch_add(1, std::memory_order_relaxed);

				// Combine them to get the UID.
				return (static_cast<Message_uid_value_t>(messageClass) << 32) | (uidNumValue & maxUIDCounter);
			};
		};

	};


};


#endif /* __Exs_Core_MessageBase_H__ */
