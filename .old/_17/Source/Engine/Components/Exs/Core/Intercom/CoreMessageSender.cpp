
#include <Exs/Core/Intercom/CoreMessageSender.h>
#include <Exs/Core/Intercom/CoreMessageDispatcher.h>
#include <Exs/Core/Intercom/CoreMessageResponse.h>


namespace Exs
{


	CoreMessageSender::CoreMessageSender(CoreMessageController& controller, CoreMessageDispatcher& dispatcher, Thread& thread, ThreadUID threadUID)
	: _controller(&controller)
	, _dispatcher(&dispatcher)
	, _thread(&thread)
	, _threadUID(threadUID)
	, _syncObject(&thread)
	{ }


	CoreAsyncMessageSendResult CoreMessageSender::Send(const CoreAsyncMessageSendRequest& request, const CoreAsyncMessageHandle& messageHandle)
	{
		if (request.responseRequest.active)
		{
			return this->_DispatchAsyncWithResponseReuqest(request, messageHandle);
		}
		else
		{
			auto result = this->_DispatchAsync(request, messageHandle);
			return CoreAsyncMessageSendResult{ result, nullptr };
		}
	}


	Result CoreMessageSender::Send(const CoreMessageSendRequest& request, const CoreMessage& message)
	{
		return this->_InternalDispatch(request, message);
	}


	Result CoreMessageSender::Send(const CoreMessageSendRequest& request, const CoreNotification& notification)
	{
		return this->_InternalDispatch(request, notification);
	}


	Result CoreMessageSender::_DispatchAsync(const CoreAsyncMessageSendRequest& request, const CoreAsyncMessageHandle& messageHandle)
	{
		return this->_InternalDispatch(request, messageHandle);
	}


	CoreAsyncMessageSendResult CoreMessageSender::_DispatchAsyncWithResponseReuqest(const CoreAsyncMessageSendRequest& request, const CoreAsyncMessageHandle& messageHandle)
	{
		if (!this->_responseState)
		{
			this->_responseState = std::make_shared<CoreAsyncMessageSharedResponseState>(&(this->_syncObject));
		}

		CoreAsyncMessageSendResult sendResult{ExsResult(RSC_Err_Failed), nullptr};

		auto& responseState = *(this->_responseState);
		ExsBeginSyncCriticalSection(syncLock, responseState.GetLock());
		{
			// Save the response state inside the message being synchronized.
			messageHandle->SetResponseState(this->_responseState);
			
			// Dispatch message to the proper receiver.
			Result dispatchResult = this->_InternalDispatch(request, messageHandle);

			if (dispatchResult == RSC_Success)
			{
				//
				responseState.SetCurrentMessage(*messageHandle);

				//
				auto waitResult = this->_syncObject.WaitFor(syncLock, request.responseRequest.waitTimeout, [&responseState]() -> bool {
					return responseState.IsResponseSet();
				});

				if (responseState.IsResponseSet())
				{
					sendResult.code = ExsResult(RSC_Success);
					sendResult.responseHandle = responseState.GetResponse();
				}

				responseState.Reset();
			}
		}
		ExsEndCriticalSection();

		return sendResult;
	}


	template <class Message_t>
	Result CoreMessageSender::_InternalDispatch(const CoreMessageSendRequest& request, const Message_t& message)
	{
		CoreMessageDispatchRequest dispatchRequest;
		dispatchRequest.source.controller = this->_cotroller;
		dispatchRequest.source.threadUID = this->_threadUID;
		dispatchRequest.destination = request.destination;

		return this->_dispatcher->Dispatch(dispatchRequest, message);
	}


}
