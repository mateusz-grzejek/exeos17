
#ifndef __Exs_Core_CoreMessageController_H__
#define __Exs_Core_CoreMessageController_H__

#include "CoreMessage.h"
#include "CoreMessageReceiver.h"
#include "CoreMessageSender.h"


namespace Exs
{


	class CoreMessageDispatcher;
	class Thread;
	class ThreadSystemManager;


	///<summary>
	///</summary>
	class EXS_LIBCLASS_CORE CoreMessageController
	{
		friend class CoreMessenger;
		friend class CoreMessageDispatcher;
		friend class Thread;

	private:
		struct MessengerInfo
		{
			CoreMessenger* messenger;
		};

	private:
		CoreMessageDispatcher*  _coreMessageDispatcher;
		ThreadSystemManager*    _threadSystemManager;
		Thread*                 _thread;
		ThreadUID               _threadUID;
		CoreMessageReceiver     _receiver;
		CoreMessageSender       _sender;

	public:
		CoreMessageController(CoreMessageDispatcher& msgDispatcher, ThreadSystemManager& thrSystemManager, Thread& thread);

		///<summary>
		///</summary>
		Thread* GetThread() const;

		///<summary>
		///</summary>
		ThreadUID GetThreadUID() const;

	friendapi: // CoreMessageDispatcher
		CoreMessageReceiver* GetReceiver();

	friendapi: // Thread
		///
		CoreAsyncMessageSendResult SendAsyncMessage(const CoreAsyncMessageSendRequest& destination, const CoreAsyncMessageHandle& messageHandle);

		///
		Result SendImmediateMessage(const CoreMessageSendRequest& request, const CoreMessage& message);

		///
		Result SendNotification(const CoreMessageSendRequest& request, const CoreNotification& notification);

		/// Peeks message, if internal queue is not empty. Called in owner (receiver) thread.
		CoreAsyncMessageHandle PeekAsyncMessage();

		/// Peeks message or blocks until received if internal queue is empty. Called in owner (receiver) thread.
		CoreAsyncMessageHandle WaitForAsyncMessage();

		/// Peeks message or blocks with timeout if internal queue is empty. Called in owner (receiver) thread.
		CoreAsyncMessageHandle WaitForAsyncMessage(const Milliseconds& timeout);

	friendapi: // Messenger
		///
		void AddMessenger(CoreMessenger* messenger, CoreAsyncMessageExternalQueue* messageQueue);

		///
		void RemoveMessenger(CoreMessenger* messenger);

		///
		void RegisterMessageHandler(CoreMessenger* messenger, Message_code_t messageCode, const CoreMessageHandlerFunction& handlerFunction);

		///
		void UnregisterMessageHandler(CoreMessenger* messenger, Message_code_t messageCode);

		///
		void RegisterNotificationHandler(CoreMessenger* messenger, Message_code_t notificationCode, const CoreNotificationHandlerFunction& handlerFunction);

		///
		void UnregisterNotificationHandler(CoreMessenger* messenger, Message_code_t notificationCode);
	};


}


#endif /* __Exs_Core_CoreMessageController_H__ */
