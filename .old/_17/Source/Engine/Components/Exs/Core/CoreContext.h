
#ifndef __Exs_Core_CoreContext_H__
#define __Exs_Core_CoreContext_H__

#include "Concurrency/CoreThreadSystemState.h"
#include "System/AppCommon.h"


namespace Exs
{


	class CoreAppSystemStateBuilder;
	class CoreThreadSystemStateBuilder;


	///<summary>
	///</summary>
	struct CoreContextCreateInfo
	{
		struct AppSystemParameters
		{ };

		struct ThreadSystemParameters
		{ };

		//
		AppSystemParameters appSystemParameters;
		
		//
		CoreAppSystemStateBuilder* appSystemStateBuilder = nullptr;

		//
		ThreadSystemParameters threadSystemParameters;

		//
		CoreThreadSystemStateBuilder* threadSystemStateBuilder = nullptr;
	};


	///<summary>
	///</summary>
	class EXS_LIBCLASS_CORE CoreContext : public ComponentContext<SharedMutex>
	{
	private:
		SharedHandle<CoreAppSystemState> _appSystemState;

		SharedHandle<CoreThreadSystemState> _threadSystemState;

	public:
		// Public accessor used to access app system state object.
		CoreAppSystemState* const appSystemState;

		// Public accessor used to access thread system state object.
		CoreThreadSystemState* const threadSystemState;

	public:
		CoreContext();

		const SharedHandle<CoreAppSystemState>& GetAppSystemStateHandle() const;

		const SharedHandle<CoreThreadSystemState>& GetThreadSystemStateHandle() const;
	};


	SharedHandle<CoreContext> CreateCoreContext( const CoreContextCreateInfo& createInfo );


}


#endif /* __Exs_Core_CoreContext_H__ */
