
#ifndef __Exs_Core_Internal_TLSReleaseCallbackController_H__
#define __Exs_Core_Internal_TLSReleaseCallbackController_H__

#include "../Concurrency/ThreadLocalStorage.h"


namespace Exs
{


	class TLSReleaseCallbackController
	{
	private:
		struct ReleaseCallbackData
		{
			TLSReleaseCallback callback;
		};

		using ReleaseCallbackList = std::array<ReleaseCallbackData, Config::CCRT_TLS_Max_User_Release_Callbacks_Num>;

	private:
		ThreadLocalStorage*  _tls;
		ReleaseCallbackList  _callbackList;
		Size_t               _callbacksNum;

	public:
		TLSReleaseCallbackController(ThreadLocalStorage* tls)
		: _tls(tls)
		, _callbacksNum(0)
		{ }

		~TLSReleaseCallbackController()
		{
			this->_Release();
		}

		bool Register(TLSReleaseCallback&& callback)
		{
			if (callback)
			{
				if (auto* callbackData = this->_AllocEntry())
				{
					callbackData->callback = std::move(callback);
					return true;
				}
			}

			return false;
		}

		bool Register(const TLSReleaseCallback& callback)
		{
			if (callback)
			{
				if (auto* callbackData = this->_AllocEntry())
				{
					callbackData->callback = callback;
					return true;
				}
			}

			return false;
		}

	private:
		ReleaseCallbackData* _AllocEntry()
		{
			if (this->_callbacksNum == this->_callbackList.size())
			{
				return nullptr;
			}

			auto& entryRef = this->_callbackList[this->_callbacksNum];
			++this->_callbacksNum;

			return &entryRef;
		}

		void _Release()
		{
			std::for_each(
				this->_callbackList.rbegin(),
				this->_callbackList.rend(),
				[this](ReleaseCallbackData& callbackData) -> void {
					if (callbackData.callback)
					{
						callbackData.callback(this->_tls);
					}
			});
		}
	};


}


#endif /* __Exs_Core_Internal_TLSReleaseCallbackController_H__ */
