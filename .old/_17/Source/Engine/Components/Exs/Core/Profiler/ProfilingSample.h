
#ifndef __Exs_Core_ProfilingSample_H__
#define __Exs_Core_ProfilingSample_H__

#include "ProfilerBase.h"


namespace Exs
{


	class ProfilingSampleLabel;

	
	///<summary>
	///</summary>
	class ProfilingSample
	{
	public:
		struct Statistics
		{
			Size_t              executionCount = 0;
			Duration_value_t    maxExecutionTime = 0;
			Duration_value_t    minExecutionTime = 0;
			Duration_value_t    totalExecutionTime = 0;
		};

	private:
		ProfilerModule*          _module;
		ProfilingSampleID        _id;
		std::string              _name;
		SourceLocationInfo       _srcInfo;
		ProfilingSampleLabel*    _label;
		Statistics               _statistics;

	public:
		ProfilingSample(ProfilerModule* module)
		: _module(module)
		, _label(nullptr)
		{ }

		ProfilingSample(ProfilerModule* module, const std::string& name, const SourceLocationInfo& srcInfo, ProfilingSampleLabel* label)
		: _module(module)
		, _id(GetHashCode<ProfilingSampleID>(name))
		, _name(name)
		, _srcInfo(srcInfo)
		, _label(label)
		{ }

		Statistics& UpdateStatistics()
		{
			return this->_statistics;
		}
		
		const ProfilingSampleID& GetID() const
		{
			return this->_id;
		}
		
		const std::string& GetName() const
		{
			return this->_name;
		}
		
		const SourceLocationInfo& GetSrcInfo() const
		{
			return this->_srcInfo;
		}
		
		const Statistics& GetStatistics() const
		{
			return this->_statistics;
		}
	};

	
	///<summary>
	///</summary>
	class ProfilingSampleLabel
	{
	private:
		ProfilingSampleLabelID    _id;
		std::string               _name;

	public:
		ProfilingSampleLabel(const std::string& name)
		: _id(GetHashCode<ProfilingSampleLabelID>(name))
		, _name(name)
		{ }

		~ProfilingSampleLabel()
		{ }
		
		const ProfilingSampleLabelID& GetID() const
		{
			return this->_id;
		}

		const std::string& GetName() const
		{
			return this->_name;
		}
	};


}


#endif /* __Exs_Core_ProfilingSample_H__ */
