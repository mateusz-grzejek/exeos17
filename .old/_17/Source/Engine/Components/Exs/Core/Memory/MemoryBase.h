
#ifndef __Exs_Core_MemoryBase_H__
#define __Exs_Core_MemoryBase_H__

#include "../Prerequisites.h"



namespace Exs
{


	enum : Uint32
	{
		Memory_Base_Alignment = EXS_MEMORY_BASE_ALIGNMENT,

	#if ( EXS_CONFIG_BASE_ENABLE_EXTENDED_INSTRUCTION_SET )
		Memory_Internal_Alignment = 16
	#else
		Memory_Internal_Alignment = EXS_MEMORY_BASE_ALIGNMENT
	#endif
	};


	enum MemoryFlags : Uint16
	{
		MRF_Aligned		= 0x0040
	};


	struct MemoryAlignmentInfo
	{
		Uint16	alignment;
		Uint16	offset;
	};


	struct MemoryHeaderBase
	{
	};


	template <bool Aligned, Size_t Size, Uint16 Alignment>
	struct AlignedSize;


	template <Size_t Size, Uint16 Alignment>
	struct AlignedSize<true, Size, Alignment>
	{
		static const Size_t value = ExsCommonGetAlignedValue(Size, Alignment);
	};


	template <Size_t Size, Uint16 Alignment>
	struct AlignedSize<false, Size, Alignment>
	{
		static const Size_t value = Size;
	};



	inline void AlignMemoryAddress(Uint address, Uint16 alignment, Uint* alignedAddress, Uint16* alignOffset)
	{
		const Uint resultAddress = ExsCommonGetAlignedValue(address, alignment);
		const Uint16 resultOffset = truncate_cast<Uint16>(resultAddress - address);

		*alignedAddress = resultAddress;
		*alignOffset = resultOffset;
	}


}


#endif /* __Exs_Core_MemoryBase_H__ */
