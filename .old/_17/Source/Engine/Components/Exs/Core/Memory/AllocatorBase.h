
#ifndef __Exs_Core_AllocatorBase_H__
#define __Exs_Core_AllocatorBase_H__

#include "MemoryBase.h"


namespace Exs
{


	class Allocator;
	class InternalAllocator;
	class SystemAllocator;


  #if ( EXS_CONFIG_BASE_OVERRIDE_MEMORY_ALLOCATION )
	typedef InternalAllocator DefaultAllocator;
  #else
	typedef SystemAllocator DefaultAllocator;
  #endif


	typedef U32ID AllocatorID;


	enum : AllocatorID
	{
		Invalid_Allocator_ID = Invalid_U32ID
	};


	enum class AllocationSync : Enum
	{
		Default,
		Multithreaded
	};
	

	template <AllocationSync>
	struct AllocationLock;

	template <>
	struct AllocationLock<AllocationSync::Default>
	{
		typedef EmptyMutex Type;
	};

	template <>
	struct AllocationLock<AllocationSync::Multithreaded>
	{
		typedef LightMutex Type;
	};


	template <class T>
	struct FixedAllocTypeSize
	{
		static const Size_t Value =
			stdx::conditional_value<Size_t, sizeof(T) <= 16, 16, stdx::static_pow2_round<Size_t, sizeof(T)>::value>::value;
	};


}


#endif /* __Exs_Core_AllocatorBase_H__ */
