
#ifndef __Exs_Core_MemoryAllocationsStatistics_H__
#define __Exs_Core_MemoryAllocationsStatistics_H__

#include "AllocatorBase.h"


namespace Exs
{


	class MemoryAllocationsStatistics
	{
	public:
		struct StatInfo
		{
			Size_t	currentAllocationsNum = 0;
			Size_t	currentAllocationsSize = 0;
			Size_t	highestUsagePeek = 0;
			Size_t	maxAllocationSize = 0;
			Size_t	minAllocationSize = 0;
			Size_t	totalAllocationsNum = 0;
			Size_t	totalAllocationsSize = 0;
			Size_t	totalDeallocationsNum = 0;
			Size_t	totalDeallocationsSize = 0;
		};

	private:
		StatInfo    _statInfo;

	public:
		MemoryAllocationsStatistics() = default;

		void RegisterAllocation(Size_t allocSize)
		{
			ExsDebugAssert( allocSize > 0 );
		
			this->_statInfo.currentAllocationsNum += 1;
			this->_statInfo.currentAllocationsSize += allocSize;
		
			this->_statInfo.totalAllocationsNum += 1;
			this->_statInfo.totalAllocationsSize += allocSize;
	
			if (this->_statInfo.currentAllocationsSize > this->_statInfo.highestUsagePeek)
				this->_statInfo.highestUsagePeek = this->_statInfo.currentAllocationsSize;
	
			if (allocSize > this->_statInfo.maxAllocationSize)
				this->_statInfo.maxAllocationSize = allocSize;
	
			if ((allocSize < this->_statInfo.minAllocationSize) || (this->_statInfo.minAllocationSize == 0))
				this->_statInfo.minAllocationSize = allocSize;
		}

		void RegisterDeallocation(Size_t deallocSize)
		{
			ExsDebugAssert( deallocSize > 0 );
		
			this->_statInfo.currentAllocationsNum -= 1;
			this->_statInfo.currentAllocationsSize -= deallocSize;
		
			this->_statInfo.totalDeallocationsNum += 1;
			this->_statInfo.totalDeallocationsSize += deallocSize;
		}

		const StatInfo& GetStatisticsInfo() const
		{
			return this->_statInfo;
		}
	};


}


#endif /* __Exs_Core_MemoryAllocationsStatistics_H__ */
