
#ifndef __Exs_Core_CustomMemoryPolicy_H__
#define __Exs_Core_CustomMemoryPolicy_H__

#include "MemoryBase.h"


namespace Exs
{


	template <class Allocator_t>
	class CustomMemoryPolicy
	{
	public:
		static void* operator new(Size_t memSize)
		{
			return Allocator_t::Alloc(memSize);
		}

		static void* operator new[](Size_t memSize)
		{
			return Allocator_t::Alloc(memSize);
		}
		
		static void operator delete(void* memPtr) gnoexcept
		{
			return Allocator_t::Dealloc(memPtr);
		}

		static void operator delete[](void* memPtr) gnoexcept
		{
			return Allocator_t::Dealloc(memPtr);
		}
		
		static void* operator new(Size_t memSize, const SourceLocationInfo& srcInfo)
		{
			return Allocator_t::Alloc(memSize, srcInfo);
		}

		static void* operator new[](Size_t memSize, const SourceLocationInfo& srcInfo)
		{
			return Allocator_t::Alloc(memSize, srcInfo);
		}

		static void operator delete(void* memPtr, const SourceLocationInfo& srcInfo) gnoexcept
		{
			return Allocator_t::Dealloc(memPtr, srcInfo);
		}

		static void operator delete[](void* memPtr, const SourceLocationInfo& srcInfo) gnoexcept
		{
			return Allocator_t::Dealloc(memPtr, srcInfo);
		}
	};


}


#if ( EXS_CONFIG_BASE_TRACK_OPERATOR_NEW_ALLOCATIONS )
#  define new	new(EXS_CURRENT_SOURCE_INFO)
#else
#  define new	new
#endif


#endif /* __Exs_Core_CustomMemoryPolicy_H__ */
