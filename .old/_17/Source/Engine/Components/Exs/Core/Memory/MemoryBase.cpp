
#include <Exs/Core/Memory/MemoryBase.h>


#if ( EXS_CONFIG_INTERNAL_BUILD_GLOBAL_NEWDELETE_IN_CORE )

void* operator new(Size_t memSize)
{
#if ( EXS_CONFIG_BASE_OVERRIDE_MEMORY_ALLOCATION )
	return Exs::Internal::internalMalloc(memSize);
#else
	return Exs::Internal::systemMalloc(memSize);
#endif
}

void* operator new[](Size_t memSize)
{
#if ( EXS_CONFIG_BASE_OVERRIDE_MEMORY_ALLOCATION )
	return Exs::Internal::internalMalloc(memSize);
#else
	return Exs::Internal::systemMalloc(memSize);
#endif
}

void operator delete(void* memPtr) gnoexcept
{
#if ( EXS_CONFIG_BASE_OVERRIDE_MEMORY_ALLOCATION )
	Exs::Internal::internalFree(memPtr);
#else
	Exs::Internal::systemFree(memPtr);
#endif
}

void operator delete[](void* memPtr) gnoexcept
{
#if ( EXS_CONFIG_BASE_OVERRIDE_MEMORY_ALLOCATION )
	Exs::Internal::internalFree(memPtr);
#else
	Exs::Internal::systemFree(memPtr);
#endif
}

void* operator new(Size_t memSize, const Exs::SourceLocationInfo& srcInfo)
{
#if ( EXS_CONFIG_BASE_OVERRIDE_MEMORY_ALLOCATION )
	return Exs::Internal::internalMalloc(memSize, srcInfo);
#else
	return Exs::Internal::systemMalloc(memSize, srcInfo);
#endif
}

void* operator new[](Size_t memSize, const Exs::SourceLocationInfo& srcInfo)
{
#if ( EXS_CONFIG_BASE_OVERRIDE_MEMORY_ALLOCATION )
	return Exs::Internal::internalMalloc(memSize, srcInfo);
#else
	return Exs::Internal::systemMalloc(memSize, srcInfo);
#endif
}

void operator delete(void* memPtr, const Exs::SourceLocationInfo& srcInfo) gnoexcept
{
#if ( EXS_CONFIG_BASE_OVERRIDE_MEMORY_ALLOCATION )
	Exs::Internal::internalFree(memPtr, srcInfo);
#else
	Exs::Internal::systemFree(memPtr, srcInfo);
#endif
}

void operator delete[](void* memPtr, const Exs::SourceLocationInfo& srcInfo) gnoexcept
{
#if ( EXS_CONFIG_BASE_OVERRIDE_MEMORY_ALLOCATION )
	Exs::Internal::internalFree(memPtr, srcInfo);
#else
	Exs::Internal::systemFree(memPtr, srcInfo);
#endif
}

#endif
