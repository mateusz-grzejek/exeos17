
#ifndef __Exs_Core_DebugOutput_H__
#define __Exs_Core_DebugOutput_H__


namespace Exs
{


	typedef void (*DebugInfoOutputProc)(const char* text);
	typedef bool (*RuntimeAssertHandler)(const char* info);
	typedef bool (*RuntimeInterruptHandler)(const char* info);


	class DebugExecutionControl
	{
	public:
		EXS_LIBAPI_CORE static DebugInfoOutputProc SetDebugInfoOutputProc(DebugInfoOutputProc proc);
		EXS_LIBAPI_CORE static RuntimeAssertHandler SetRuntimeAssertHandler(RuntimeAssertHandler handler);
		EXS_LIBAPI_CORE static RuntimeInterruptHandler SetRuntimeInterruptHandler(RuntimeInterruptHandler handler);
		
		EXS_LIBAPI_CORE static DebugInfoOutputProc RestoreDefaultDebugInfoOutputProc();
		EXS_LIBAPI_CORE static RuntimeAssertHandler RestoreDefaultRuntimeAssertHandler();
		EXS_LIBAPI_CORE static RuntimeInterruptHandler RestoreDefaultRuntimeInterruptHandler();
		
		EXS_LIBAPI_CORE static DebugInfoOutputProc GetDebugInfoOutputProc();
		EXS_LIBAPI_CORE static RuntimeAssertHandler GetRuntimeAssertHandler();
		EXS_LIBAPI_CORE static RuntimeInterruptHandler GetRuntimeInterruptHandler();
		
	private:
		static void _PrintOutputDebug(const char* output);
		static void _PrintOutputDefault(const char* output);
		static void _DefaultDebugInfoOutputProc(const char* text);
		static bool _DefaultRuntimeAssertHandler(const char* info);
		static bool _DefaultRuntimeInterruptHandler(const char* info);
	};


	namespace Internal
	{

		EXS_LIBAPI_CORE void DebugInfoOutputImpl(const char* format, ...);
		EXS_LIBAPI_CORE bool RuntimeAssertImpl(const char* file, Int32 line, const char* conditionStr);
		EXS_LIBAPI_CORE bool RuntimeInterruptImpl(const char* file, Int32 line);

	}


}


#if ( EXS_CONFIG_BASE_ENABLE_DEBUG_INFO )
#  define ExsDebugInfo(format, ...) Internal::DebugOutputImpl(format, __VA_ARGS__)
#else
#  define ExsDebugInfo(format, ...)
#endif


#endif /* __Exs_Core_DebugOutput_H__ */
