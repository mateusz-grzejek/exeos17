
#ifndef __Exs_Core_CoreEnums_H__
#define __Exs_Core_CoreEnums_H__

namespace Exs
{


	///<summary>
	///</summary>
	enum AccessModeFlags : Enum
	{
		AccessMode_None = 0,
		AccessMode_Read = 0x0001,
		AccessMode_Write = 0x0002,
		AccessMode_Full = AccessMode_Read | AccessMode_Write
	};


}

#endif /* __Exs_Core_CoreEnums_H__ */
