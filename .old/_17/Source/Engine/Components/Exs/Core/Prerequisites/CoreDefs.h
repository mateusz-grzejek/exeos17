
#ifndef __Exs_Core_CoreDefs_H__
#define __Exs_Core_CoreDefs_H__


namespace Exs
{


	#define ExsDeclareRefPtrClass(type) \
		class type; typedef std::shared_ptr<type> type##RefPtr;

	#define ExsDeclareRefPtrStruct(type) \
		struct type; typedef std::shared_ptr<type> type##RefPtr;


	///
	typedef U32ID Thread_ref_id_value_t;

	///
	typedef Thread_ref_id_value_t ThreadRefID;


	/// Special values which ThreadUID may store.
	enum : Thread_ref_id_value_t
	{
		//
		Thread_Ref_ID_None = 0,

		//
		Thread_Ref_ID_Invalid = stdx::limits<Thread_ref_id_value_t>::max_value,

		//
		Thread_Ref_ID_Auto = Thread_Ref_ID_Invalid - 1
	};


	enum : Size_t
	{
		// Represents the largest value, that can be stored in native size type.
		Max_Size = stdx::limits<Size_t>::max_value,

		// Represents invalid length of sequence, collection, string or other measurable object. It usually means, that
		// such objects contains invalid/corrupted/forbidden value/state and thus, its length cannot be properly determined.
		//
		Invalid_Length = stdx::limits<Size_t>::max_value,

		//
		Invalid_Offset = stdx::limits<Uint>::max_value,

		// Represents invalid position in sequence, collection, string, etc. It is widely used in
		// various searching functions which return this value if requested element cannot be found.
		Invalid_Position = stdx::limits<Size_t>::max_value
	};


}


#endif /* __Exs_Core_CoreDefs_H__ */
