
#ifndef __Exs_Core_TraceOutput_H__
#define __Exs_Core_TraceOutput_H__

#include "Prerequisites.h"
#include <stdx/assoc_array.h>


#define EXS_TRACE_INFO_CONTENT_BASE_CATEGORY   0x0000F1
#define EXS_TRACE_INFO_CONTENT_BASE_TYPE       0x0000F2
#define EXS_TRACE_INFO_CONTENT_CURRENT_TIME    0x00F100
#define EXS_TRACE_INFO_CONTENT_THREAD_ADDRESS  0xF20000
#define EXS_TRACE_INFO_CONTENT_THREAD_UID      0xF40000

#define EXS_TRACE_INFO_MASK_BASE    0x0000F0
#define EXS_TRACE_INFO_MASK_CURRENT 0x00F000
#define EXS_TRACE_INFO_MASK_THREAD  0xF00000

#define EXS_TRACE_INFO_CONTENT_ALL         0xFFFF
#define EXS_TRACE_INFO_CONTENT_INIT_PHASE  EXS_TRACE_INFO_CONTENT_BASE_TYPE | EXS_TRACE_INFO_CONTENT_BASE_CATEGORY
#define EXS_TRACE_INFO_CONTENT_DEFAULT     EXS_TRACE_INFO_CONTENT_BASE_TYPE | EXS_TRACE_INFO_CONTENT_THREAD_UID


#if !defined( EXS_TRACE_CONFIG_ENABLE_INFO_CONTENT_CURRENT_TIME )
#  if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
#    define EXS_TRACE_CONFIG_ENABLE_INFO_CONTENT_CURRENT_TIME  1
#  else
#    define EXS_TRACE_CONFIG_ENABLE_INFO_CONTENT_CURRENT_TIME  0
#  endif
#endif

#if !defined( EXS_TRACE_CONFIG_ENABLE_INFO_CONTENT_CURRENT_THREAD )
#  if ( EXS_CONFIG_BASE_ENABLE_DEBUG )
#    define EXS_TRACE_CONFIG_ENABLE_INFO_CONTENT_CURRENT_THREAD  1
#  else
#    define EXS_TRACE_CONFIG_ENABLE_INFO_CONTENT_CURRENT_THREAD  0
#  endif
#endif


namespace Exs
{


	typedef std::function<void(const char*)> TraceOutputHandler;


	enum class TraceInfoContent : Enum
	{
		None = 0,
		Base_Category = EXS_TRACE_INFO_CONTENT_BASE_CATEGORY,
		Base_Type = EXS_TRACE_INFO_CONTENT_BASE_TYPE,
		Current_Time = EXS_TRACE_INFO_CONTENT_CURRENT_TIME,
		Thread = EXS_TRACE_INFO_MASK_THREAD,
		Thread_Address = EXS_TRACE_INFO_CONTENT_THREAD_ADDRESS,
		Thread_UID = EXS_TRACE_INFO_CONTENT_THREAD_UID,
		Default = EXS_TRACE_INFO_CONTENT_DEFAULT,
		All = EXS_TRACE_INFO_CONTENT_ALL
	};


	struct TraceOrigin
	{
		TraceType type;
		TraceCategoryID category;
		const char* typeName;
		const char* categoryName;
	};


	enum : Uint32
	{
		Default_Trace_Output_Thread_Name_Column_Width = 16
	};


	class EXS_LIBCLASS_CORE TraceOutputController : public Lockable<LightMutex, LockAccess::Relaxed>
	{
	private:
		struct CategoryInfo
		{
			bool                  active;
			TraceCategoryID       id;
			TraceOutputHandler    outputHandler;
		};

		typedef stdx::assoc_array<TraceCategoryID, CategoryInfo> Categories;

	private:
		stdx::mask<Enum>      _activeInfoContent;
		stdx::mask<Enum>      _activeTraceTypes;
		Categories            _categories;
		TraceOutputHandler    _commonOutputHandler;
		Uint32                _threadNameColumnWidth;

	public:
		TraceOutputController();
		~TraceOutputController();
		
		void PrintOutput(const TraceOrigin& traceOrigin, const char* format, va_list args);

		void EnableCategory(TraceCategoryID categoryID, bool enable);
		void EnableTraceType(TraceType type, bool enable);
;
		void SetCategoryOutputHandler(TraceCategoryID categoryID, const TraceOutputHandler& handler);
		void SetCommonOutputHandler(const TraceOutputHandler& handler);
		void SetInfoContentDisplayState(TraceInfoContent infoContent, bool enable);

		void SetThreadNameColumnWidth(Uint32 width);

		bool GetInfoContentDisplayState(TraceInfoContent infoContent) const;

		Uint32 GetThreadNameColumnWidth() const;

		bool IsCategoryEnabled(TraceCategoryID categoryID) const;
		bool IsTraceTypeEnabled(TraceType type) const;

	private:
		CategoryInfo* GetCategoryInfo(TraceCategoryID categoryID);
		const CategoryInfo* GetCategoryInfo(TraceCategoryID categoryID) const;

		static void DefaultOutputHandler(const char* output);
	};


	inline void TraceOutputController::EnableTraceType(TraceType type, bool enable)
	{
		Enum traceTypeValue = static_cast<Enum>(type);
		enable ? this->_activeTraceTypes.set(traceTypeValue) : this->_activeTraceTypes.unset(traceTypeValue);
	}

	inline void TraceOutputController::SetCommonOutputHandler(const TraceOutputHandler& handler)
	{
		this->_commonOutputHandler = handler;
	}

	inline void TraceOutputController::SetInfoContentDisplayState(TraceInfoContent infoContent, bool enable)
	{
		Enum traceInfoContentValue = static_cast<Enum>(infoContent);
		enable ? this->_activeInfoContent.set(traceInfoContentValue) : this->_activeInfoContent.unset(traceInfoContentValue);
	}

	inline void TraceOutputController::SetThreadNameColumnWidth(Uint32 width)
	{
		this->_threadNameColumnWidth = width;
	}

	inline bool TraceOutputController::GetInfoContentDisplayState(TraceInfoContent infoContent) const
	{
		Enum traceInfoContentValue = static_cast<Enum>(infoContent);
		return this->_activeInfoContent.is_set(traceInfoContentValue);
	}

	inline Uint32 TraceOutputController::GetThreadNameColumnWidth() const
	{
		return this->_threadNameColumnWidth;
	}

	inline bool TraceOutputController::IsCategoryEnabled(TraceCategoryID categoryID) const
	{
		const CategoryInfo* categoryInfo = this->GetCategoryInfo(categoryID);
		return !categoryInfo || categoryInfo->active;
	}

	inline bool TraceOutputController::IsTraceTypeEnabled(TraceType type) const
	{
		Enum traceTypeValue = static_cast<Enum>(type);
		return this->_activeTraceTypes.is_set(traceTypeValue);
	}

	inline TraceOutputController::CategoryInfo* TraceOutputController::GetCategoryInfo(TraceCategoryID categoryID)
	{
		auto categoryInfoRef = this->_categories.find(categoryID);
		return (categoryInfoRef != this->_categories.end()) ? &(categoryInfoRef->value) : nullptr;
	}

	inline const TraceOutputController::CategoryInfo* TraceOutputController::GetCategoryInfo(TraceCategoryID categoryID) const
	{
		auto categoryInfoRef = this->_categories.find(categoryID);
		return (categoryInfoRef != this->_categories.end()) ? &(categoryInfoRef->value) : nullptr;
	}


	EXS_LIBAPI_CORE TraceOutputController* GetTraceOutputController();


}


#endif /* __Exs_Core_TraceOutput_H__ */
