
#ifndef __Exs_ScriptSystem_TypeTraitsExt_H__
#define __Exs_ScriptSystem_TypeTraitsExt_H__

namespace Exs
{


	template <typename T>
	const char* GetTypeName()
	{
		static std::once_flag initFlag;
		static std::string typeName;

		std::call_once(initFlag, []() -> void {
			std::string rttiName { typeid(T).name() };
			std::replace(rttiName.begin(), rttiName.end(), ' ', '_');
			typeName = std::move(rttiName);
		});

		return typeName.c_str();
	}


}

#endif /* __Exs_ScriptSystem_TypeTraitsExt_H__ */
