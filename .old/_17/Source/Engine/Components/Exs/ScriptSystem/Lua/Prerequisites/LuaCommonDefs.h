
#pragma once

#ifndef __Exs_ScriptSystem_LuaCommonDefs_H__
#define __Exs_ScriptSystem_LuaCommonDefs_H__


namespace Exs
{
namespace Lua
{


	template <typename T>
	struct PointerWrapper
	{ };


	template <typename T>
	class ReferenceWrapper
	{ };


	template <typename V>
	struct ConstantDefition
	{
		const char* name;

		V value;
	};


	template <typename T>
	struct ConstantWrapper;

	template <>
	struct ConstantWrapper<lua_Integer>
	{
		static int Get(lua_State* lua)
		{
			int valueIndex = lua_upvalueindex(1);
			lua_Integer value = lua_tointeger(lua, valueIndex);
			lua_pushinteger(lua, value);
		}

		template <typename V>
		static void Create(lua_State* lua, V value)
		{
			lua_pushinteger(lua, value);
			lua_pushcclosure(lua, Get, 1);
		}
	};


	template <typename V, V* Ptr>
	struct StaticVariableWrapper
	{
		static int Get(lua_State* lua)
		{

		}
	};


#define ExsScriptStaticVariableWrapper(var) \
	StaticVariableWrapper<decltype(var), &(var)>::Get


	template <typename T>
	struct StaticWrapper;


	template <typename T>
	struct StaticWrapper;

	template <>
	struct StaticWrapper<lua_Integer>
	{
		static int Get(lua_State* lua)
		{
			int valueIndex = lua_upvalueindex(1);

			auto* valuePtr = (lua_Integer*)lua_topointer(lua, valueIndex);

			lua_pushinteger(lua, *valuePtr);

			return 1;
		}

		template <typename V>
		static void Create(lua_State* lua, V* valPtr)
		{
			lua_pushlightuserdata(lua, valPtr);

			lua_pushcclosure(lua, Get, 1);
		}
	};


	struct luaEnumReg
	{
		const char* name;
		lua_Integer value;
	};


#define ExsScriptEnumItem(enumName, itemName) \
	{ #itemName }


	template <typename T>
	struct EnumWrapper;

	template <>
	struct EnumWrapper<Enum>
	{
		static int Get(lua_State* lua)
		{
			int valueIndex = lua_upvalueindex(1);

			auto* valuePtr = (lua_Integer*)lua_topointer(lua, valueIndex);

			lua_pushinteger(lua, *valuePtr);

			return 1;
		}

		template <typename V, size_t N>
		static void Create(lua_State* lua, const luaEnumReg (&enums)[N])
		{
			lua_pushlightuserdata(lua, valPtr);

			lua_pushcclosure(lua, Get, 1);
		}
	};


	void LuaDumpStack(lua_State* luaState)
	{
		int top = lua_gettop(luaState);

		if (top == 0)
			printf("Stack empty.");
		else
			printf("Stack size = %u: ", top);

		for (int i = 1; i <= top; i++)
		{
			int t = lua_type(luaState, -i);

			switch (t)
			{
			case LUA_TSTRING:
				printf("string (\"%s\")", lua_tostring(luaState, -i));
				break;

			case LUA_TBOOLEAN:
				printf("bool (%s)", lua_toboolean(luaState, -i) ? "true" : "false");
				break;

			case LUA_TNUMBER:
				printf("number (%g)", lua_tonumber(luaState, -i));
				break;

			default:
				printf("%s", lua_typename(luaState, t));
				break;
			}

			printf("  ");
		}

		printf("\n");
	}


}
}


#endif /* __Exs_ScriptSystem_LuaCommonDefs_H__ */
