
#ifndef __Exs_ScriptSystem_LuaCommonExtApi_H__
#define __Exs_ScriptSystem_LuaCommonExtApi_H__


namespace Exs
{
namespace Lua
{


	inline void* luaE_checkuserdata(lua_State* luaState, int idx, const char* name)
	{
		void* result = nullptr;

		if (void* userdata = lua_touserdata(luaState, idx))
		{
			if (lua_getmetatable(luaState, idx))
			{
				luaL_getmetatable(luaState, name);

				if (lua_rawequal(luaState, -1, -2))
				{
					result = userdata;
				}

				lua_pop(luaState, 2);
			}
		}

		return result;
	}


	template <typename T>
	inline void* luaE_checkuserdata(lua_State* luaState, int idx)
	{
		void* result = nullptr;

		if (void* userdata = lua_touserdata(luaState, idx))
		{
			if (lua_getmetatable(luaState, idx))
			{
				luaL_getmetatable(luaState, TypeMetaInfo<T>::GetClsMetatableName());

				if (lua_rawequal(luaState, -1, -2))
				{
					result = userdata;
				}

				lua_pop(luaState, 2);
			}
		}

		return result;
	}


}
}


#endif /* __Exs_ScriptSystem_LuaCommonExtApi_H__ */
