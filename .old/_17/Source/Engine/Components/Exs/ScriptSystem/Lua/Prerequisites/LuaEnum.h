
#ifndef __Exs_ScriptSystem_LuaEnum_H__
#define __Exs_ScriptSystem_LuaEnum_H__


namespace Exs
{
namespace Lua
{


	template <typename E>
	struct EnumDef
	{
		struct Enumerator
		{
			const char* eid;
			E value;
		};

		const char* name;
		std::initializer_list<Enumerator> valueList;
	};


	template <typename E>
	int EnumNewValueWrapper(lua_State* luaState)
	{
		printf("[Error] Cannot modify enum values!\n");
		return 0;
	}


	template <typename E>
	int EnumAccessWrapper(lua_State* luaState)
	{
		int enumeratorsTableIdx = lua_upvalueindex(1);

		lua_rawget(luaState, enumeratorsTableIdx);

		if (lua_isnil(luaState, -1))
		{
			lua_pop(luaState, 1);
			return 0;
		}

		return 1;
	}


	template <typename E>
	void CreateEnumAccessWrapper(lua_State* luaState, const EnumDef<E>& enumDef)
	{
		lua_newtable(luaState);

		for (auto& enumerator : enumDef.valueList)
		{
			lua_pushstring(luaState, enumerator.eid);
			lua_pushinteger(luaState, static_cast<lua_Integer>(enumerator.value));
			lua_rawset(luaState, -3);
		}

		lua_pushcclosure(luaState, EnumAccessWrapper<E>, 1);
	}


	template <typename E>
	void CreateEnumMetatable(lua_State* luaState, const EnumDef<E>& enumDef)
	{

		luaL_newmetatable(luaState, Lua::TypeMetaInfo<E>::GetClsMetatableName());
		// Stack: [metatable]

		luaL_newmetatable(luaState, Lua::TypeMetaInfo<E>::GetInternalMetatableName());
		// Stack: [submetatable, metatable]

		CreateEnumAccessWrapper<E>(luaState, enumDef);
		// Stack: [func, submetatable, metatable]

		lua_setfield(luaState, -2, "__index"); // submetatable.__index = func
		// Stack: [submetatable, metatable]

		lua_pushcfunction(luaState, EnumNewValueWrapper<E>);
		// Stack: [func, submetatable, metatable]

		lua_setfield(luaState, -2, "__newindex"); // submetatable.__index = func
		// Stack: [submetatable, metatable]

		lua_setmetatable(luaState, -2);
		// Stack: [metatable]

		lua_setglobal(luaState, enumDef.name);
	}


}
}


#endif /* __Exs_ScriptSystem_LuaEnum_H__ */
