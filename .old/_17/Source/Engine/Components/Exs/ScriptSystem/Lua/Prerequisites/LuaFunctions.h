
#ifndef __Exs_ScriptSystem_LuaFunctions_H__
#define __Exs_ScriptSystem_LuaFunctions_H__


namespace Exs
{
namespace Lua
{


	struct FunctionResultPushWrapper
	{
		template <typename T>
		void PushRef(lua_State* luaState, T& ref)
		{

		}
	};


	template <typename T>
	inline typename DataValueType<T>::Type GetFunctionArgument(lua_State* luaState, size_t argsNum, int argIdx)
	{
		int luaArgIndex = -static_cast<int>(argsNum) + argIdx;
		return LuaInterface::GetValue<T>(luaState, luaArgIndex);
	}


	template <typename... Args>
	struct FunctionArgsWrapper
	{
		static const size_t argsNum = sizeof...(Args);

		typedef typename IntegerSequenceGenerator<argsNum>::Type SeqType;

		template <int argIdx>
		struct ArgType
		{
			typedef typename std::tuple_element< argIdx, std::tuple<Args...> >::type Type;
		};

		template <Int... Seq>
		static std::tuple<Args...> GetArgs(lua_State* luaState, IntegerSequence<Seq...> sequence)
		{
			return std::tuple<Args...>(GetFunctionArgument<Args>(luaState, argsNum, Seq)...);
		}
	};


	template <typename T>
	struct MemberFunctionBase
	{
		static T* GetThis(lua_State* luaState)
		{
			return Stack::GetThisPtr<T>(luaState, 1);
		}
	};


	template <class T, class R, class... Args>
	struct MemberFunction : public MemberFunctionBase<T>
	{
		static R Execute(R(T::*funcPtr)(void), T* objPtr)
		{
			return (objPtr->*(funcPtr))();
		}

		static R Execute(R(T::*funcPtr)(void) const, const T* objPtr)
		{
			return (objPtr->*(funcPtr))();
		}

		template <Int... Seq>
		static R Execute(R(T::*funcPtr)(Args...), T* objPtr, const std::tuple<Args...>& args, IntegerSequence<Seq...>)
		{
			return (objPtr->*(funcPtr))(std::get<Seq>(args)...);
		}

		template <Int... Seq>
		static R Execute(R(T::*funcPtr)(Args...) const, const T* objPtr, const std::tuple<Args...>& args, IntegerSequence<Seq...>)
		{
			return (objPtr->*(funcPtr))(std::get<Seq>(args)...);
		}
	};

	template <class T, class... Args>
	struct MemberFunction<T, void, Args...> : public MemberFunctionBase<T>
	{
		static void Execute(void(T::*funcPtr)(void), T* objPtr)
		{
			(objPtr->*(funcPtr))();
		}

		static void Execute(void(T::*funcPtr)(void) const, const T* objPtr)
		{
			(objPtr->*(funcPtr))();
		}

		template <Int... Seq>
		static void Execute(void(T::*funcPtr)(Args...), T* objPtr, const std::tuple<Args...>& args, IntegerSequence<Seq...>)
		{
			(objPtr->*(funcPtr))(std::get<Seq>(args)...);
		}

		template <Int... Seq>
		static void Execute(void(T::*funcPtr)(Args...) const, const T* objPtr, const std::tuple<Args...>& args, IntegerSequence<Seq...>)
		{
			(objPtr->*(funcPtr))(std::get<Seq>(args)...);
		}
	};


	template <class R, class... Args>
	struct StaticFunction
	{
		static R Execute(R(*funcPtr)(void))
		{
			return funcPtr();
		}

		template <Int... Seq>
		static R Execute(R(*funcPtr)(Args...), const std::tuple<Args...>& args, IntegerSequence<Seq...>)
		{
			return (funcPtr)(std::get<Seq>(args)...);
		}
	};

	template <class... Args>
	struct StaticFunction<void, Args...>
	{
		static void Execute(void(*funcPtr)(void))
		{
			return funcPtr();
		}

		template <Int... Seq>
		static void Execute(void(*funcPtr)(Args...), const std::tuple<Args...>& args, IntegerSequence<Seq...>)
		{
			return (funcPtr)(std::get<Seq>(args)...);
		}
	};


	template <RefObjectTransferPolicy, class Func>
	struct Function;

	template <RefObjectTransferPolicy TP, class T>
	struct Function<TP, void(T::*)(void)> : public MemberFunction<T, void>
	{
		static int Call(lua_State* luaState, void(T::*funcPtr)())
		{
			T* thisPtr = GetThis(luaState);
			Execute(funcPtr, thisPtr);
			return 0;
		}
	};
	
	template <RefObjectTransferPolicy TP, class T>
	struct Function<TP, void(T::*)(void) const> : public MemberFunction<const T, void>
	{
		static int Call(lua_State* luaState, void(T::*funcPtr)())
		{
			const T* thisPtr = GetThis(luaState);
			Execute(funcPtr, thisPtr);
			return 0;
		}
	};

	template <RefObjectTransferPolicy TP, class T, class... Args>
	struct Function<TP, void(T::*)(Args...)> : public MemberFunction<T, void, Args...>
	{
		typedef FunctionArgsWrapper<Args...> ArgsWrapper;
		typedef typename ArgsWrapper::SeqType ArgsSequence;

		static const Size_t argsNum = ArgsWrapper::argsNum;

		static int Call(lua_State* luaState, void(T::*funcPtr)(Args...))
		{
			T* thisPtr = GetThis(luaState);
			auto argsTuple = ArgsWrapper::GetArgs(luaState, ArgsSequence());
			Execute(funcPtr, thisPtr, argsTuple, ArgsSequence());
			return 0;
		}
	};

	template <RefObjectTransferPolicy TP, class T, class... Args>
	struct Function<TP, void(T::*)(Args...) const> : public MemberFunction<const T, void, Args...>
	{
		typedef FunctionArgsWrapper<Args...> ArgsWrapper;
		typedef typename ArgsWrapper::SeqType ArgsSequence;

		static const Size_t argsNum = ArgsWrapper::argsNum;

		static int Call(lua_State* luaState, void(T::*funcPtr)(Args...) const)
		{
			const T* thisPtr = GetThis(luaState);
			auto argsTuple = ArgsWrapper::GetArgs(luaState, ArgsSequence());
			Execute(funcPtr, thisPtr, argsTuple, ArgsSequence());
			return 0;
		}
	};

	template <RefObjectTransferPolicy TP, class T, class R, class... Args>
	struct Function<TP, R(T::*)(Args...)> : public MemberFunction<T, R, Args...>
	{
		typedef FunctionArgsWrapper<Args...> ArgsWrapper;
		typedef typename ArgsWrapper::SeqType ArgsSequence;

		static const Size_t argsNum = ArgsWrapper::argsNum;

		static int Call(lua_State* luaState, R(T::*funcPtr)(Args...))
		{
			T* thisPtr = GetThis(luaState);
			auto argsTuple = ArgsWrapper::GetArgs(luaState, ArgsSequence());
			LuaInterface::PushValue<TP>(luaState, Execute(funcPtr, thisPtr, argsTuple, ArgsSequence()));
			return 1;
		}
	};

	template <RefObjectTransferPolicy TP, class T, class R, class... Args>
	struct Function<TP, R(T::*)(Args...) const> : public MemberFunction<const T, R, Args...>
	{
		typedef FunctionArgsWrapper<Args...> ArgsWrapper;
		typedef typename ArgsWrapper::SeqType ArgsSequence;

		static const Size_t argsNum = ArgsWrapper::argsNum;

		static int Call(lua_State* luaState, R(T::*funcPtr)(Args...) const)
		{
			const T* thisPtr = GetThis(luaState);
			auto argsTuple = ArgsWrapper::GetArgs(luaState, ArgsSequence());
			LuaInterface::PushValue<TP>(luaState, Execute(funcPtr, thisPtr, argsTuple, ArgsSequence()));
			return 1;
		}
	};

	// template <RefObjectTransferPolicy TP, class T, class R, class... Args>
	// struct Function<TP, R&(T::*)(Args...)> : public MemberFunction<T, R&, Args...>
	// {
	// 	typedef FunctionArgsWrapper<Args...> ArgsWrapper;
	// 	typedef typename ArgsWrapper::SeqType ArgsSequence;
	// 
	// 	static const Size_t argsNum = ArgsWrapper::argsNum;
	// 
	// 	static int Call(lua_State* luaState, R&(T::*funcPtr)(Args...))
	// 	{
	// 		T* thisPtr = GetThis(luaState);
	// 		auto argsTuple = ArgsWrapper::GetArgs(luaState, ArgsSequence());
	// 		Stack::PushRef(luaState, Execute(funcPtr, thisPtr, argsTuple, ArgsSequence()));
	// 		return 1;
	// 	}
	// };

	template <RefObjectTransferPolicy TP>
	struct Function<TP, void(void)> : public StaticFunction<void>
	{
		static int Call(lua_State* luaState, void(*funcPtr)(void))
		{
			Execute(funcPtr);
			return 0;
		}
	};

	template <RefObjectTransferPolicy TP, class R>
	struct Function<TP, R(void)> : public StaticFunction<R>
	{
		static int Call(lua_State* luaState, R(*funcPtr)(void))
		{
			LuaInterface::PushValue<TP>(luaState, Execute(funcPtr));
			return 1;
		}
	};

	template <RefObjectTransferPolicy TP, class... Args>
	struct Function<TP, void(Args...)> : public StaticFunction<void, Args...>
	{
		typedef FunctionArgsWrapper<Args...> ArgsWrapper;
		typedef typename ArgsWrapper::SeqType ArgsSequence;

		static const Size_t argsNum = ArgsWrapper::argsNum;

		static int Call(lua_State* luaState, void(*funcPtr)(Args...))
		{
			auto argsTuple = ArgsWrapper::GetArgs(luaState, ArgsSequence());
			Execute(funcPtr, argsTuple, ArgsSequence());
			return 0;
		}
	};

	template <RefObjectTransferPolicy TP, class R, class... Args>
	struct Function<TP, R(Args...)> : public StaticFunction<R, Args...>
	{
		typedef FunctionArgsWrapper<Args...> ArgsWrapper;
		typedef typename ArgsWrapper::SeqType ArgsSequence;

		static const Size_t argsNum = ArgsWrapper::argsNum;

		static int Call(lua_State* luaState, R(*funcPtr)(Args...))
		{
			auto argsTuple = ArgsWrapper::GetArgs(luaState, ArgsSequence());
			LuaInterface::PushValue<TP>(luaState, Execute(funcPtr, argsTuple, ArgsSequence()));
			return 1;
		}
	};


	template <RefObjectTransferPolicy TP, typename F, F Func>
	struct MemberFunctionWrapper
	{
		static int Call(lua_State* luaState)
		{
			auto res = Function<TP, F>::Call(luaState, Func);
			return res;
		}
	};


	template <class T, class... Args>
	struct Constructor
	{
		typedef FunctionArgsWrapper<Args...> ArgsWrapper;
		typedef typename ArgsWrapper::SeqType ArgsSequence;

		static const Size_t argsNum = ArgsWrapper::argsNum;

		static void Ctor(lua_State* luaState, Args&&... args)
		{
			auto* userdata = CreateUserdataObject<T>(luaState);
			userdata->Construct(std::forward<Args>(args)...);
		}

		template <Int... Seq>
		static void Execute(lua_State* luaState, const std::tuple<Args...>& args, IntegerSequence<Seq...>)
		{
			Ctor(luaState, std::get<Seq>(args)...);
		}

		static int Call(lua_State* luaState)
		{
			LuaDumpStack(luaState);
			auto argsTuple = ArgsWrapper::GetArgs(luaState, ArgsSequence());
			Execute(luaState, argsTuple, ArgsSequence());
			return 1;
		}
	};

	template <typename T, typename... Args>
	struct ConstructorWrapper
	{
		static int Call(lua_State* luaState)
		{
			auto res = Constructor<T, Args...>::Call(luaState);
			return res;
		}
	};


	template <typename F>
	struct ConstMemberFunctionType;

	template <typename T, typename R, typename... Args>
	struct ConstMemberFunctionType<R(T::*)(Args...) const>
	{
		typedef R(T::*Type)(Args...) const;
	};


	template <typename F>
	struct NonConstMemberFunctionType;

	template <typename T, typename R, typename... Args>
	struct NonConstMemberFunctionType<R(T::*)(Args...)>
	{
		typedef R(T::*Type)(Args...);
	};


}
}


#define ExsClassConstMemberFunctionExt(function, policy) \
	::Exs::Lua::MemberFunctionWrapper<policy, typename ::Exs::Lua::ConstMemberFunctionType<decltype(function)>::Type, function>::Call

#define ExsClassNonConstMemberFunctionExt(function, policy) \
	::Exs::Lua::MemberFunctionWrapper<policy, typename ::Exs::Lua::NonConstMemberFunctionType<decltype(function)>::Type, function>::Call

#define ExsClassConstMemberFunction(function) \
	ExsClassConstMemberFunctionExt(function, ::Exs::Lua::RefObjectTransferPolicy::Default)

#define ExsClassNonConstMemberFunction(function) \
	ExsClassNonConstMemberFunctionExt(function, ::Exs::Lua::RefObjectTransferPolicy::Default)

#define ExsClassConstructor(type, ...) \
	::Exs::Lua::ConstructorWrapper<type, __VA_ARGS__>::Call

#define ExsClassConstructor(type, ...) \
	::Exs::Lua::ConstructorWrapper<type, __VA_ARGS__>::Call


#endif /* __Exs_ScriptSystem_LuaFunctions_H__ */
