
#pragma once

#ifndef __Exs_ScriptSystem_LuaVariables_H__
#define __Exs_ScriptSystem_LuaVariables_H__

#include "LuaPrerequisites.h"


namespace Exs
{
namespace Lua
{


	namespace Wrap
	{


		template <class V>
		struct ClassNonStaticVariable;

		template <class T, class V>
		struct ClassNonStaticVariable<V T::*>
		{
			static void Get(lua_State* luaState, V T::* varPtr)
			{
				if (auto* thisPtr = CheckUserdata<T>::Get(luaState, 1))
				{
					V& memberRef = thisPtr->*(varPtr);
					PushUserdata<V>::Push(luaState, memberRef);
				}
			}

			static void set(lua_State* luaState, V T::* varPtr)
			{
				// Stack: [value, userdata, ...]

				T* this_ptr = *(T **)luaL_checkudata(luaState, 1, QueryMetaTableName<T>());
				auto value = luaL_checknumber(luaState, 2);
				this_ptr->*(varPtr) = static_cast<V>(value);
			}
		};


	}
	

}
}


#endif /* __Exs_ScriptSystem_LuaVariables_H__ */
