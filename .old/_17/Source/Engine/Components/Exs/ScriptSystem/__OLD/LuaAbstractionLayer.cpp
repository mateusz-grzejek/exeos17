
#include "Precompiled.h"

#undef ExsTraceInfo
#undef ExsTraceError
#define ExsTraceInfo(...)
#define ExsTraceError(...)


namespace Exs
{


	void Lua::OpenLibrary(lua_State* lua, LuaCallback loadCallback)
	{
		loadCallback(lua);
		lua_pop(lua, 1);
	}


	bool Lua::QueryMetatable(lua_State* lua, const char *metatableName)
	{
		int pushed_value_type = luaL_getmetatable(lua, metatableName);

		if(pushed_value_type != LUA_TTABLE)
		{
			if(pushed_value_type == LUA_TNIL)
			{
				ExsTraceInfo(TRC_Script_System, "Metatable {%s} not found.", metatableName);
			}
			else
			{
				const char* type_name = lua_typename(lua, pushed_value_type);
				ExsTraceInfo(TRC_Script_System, "{%s} is not a metatable. Found %s.", metatableName, type_name);
			}

			lua_pop(lua, 1);

			return false;
		}

		return true;
	}


	void Lua::TypeError(lua_State* lua, int index, const char *metatableName)
	{
		ExsTraceError(TRC_Script_System, "Type error (%s expected).", metatableName);
	}


	void* Lua::NewObject(lua_State* lua, Size_t size, const char* metatableName)
	{
		bool metatable_ok = QueryMetatable(lua, metatableName);

		if(metatable_ok)
		{
			// Stack: [..., metatable]

			void* data_ptr = lua_newuserdata(lua, size);
			// Stack: [..., metatable, userdata]

			lua_insert(lua, -2);
			// Stack: [..., userdata, metatable]
			
			lua_setmetatable(lua, -2);
			// Stack: [..., userdata]

			lua_pop(lua, 1);
			// Stack: [...]

			return data_ptr;
		}

		return nullptr;
	}


	void Lua::DumpStack(lua_State* lua)
	{
		int top = lua_gettop(lua);
		
		if(top == 0)
			printf("Stack empty.");
		else
			printf("Stack size = %u: ", top);
		
		for(int i = 1; i <= top; i++)
		{
			int t = lua_type(lua, i);
			
			switch (t)
			{
			case LUA_TSTRING:
				printf("string (\"%s\")", lua_tostring(lua, i));
				break;
			
			case LUA_TBOOLEAN:
				printf("bool (%s)", lua_toboolean(lua, i) ? "true" : "false");
				break;
			
			case LUA_TNUMBER:
				printf("number (%g)", lua_tonumber(lua, i));
				break;
			
			default:
				printf("%s", lua_typename(lua, t));
				break;
			}
			
			printf("  ");
		}
		
		printf("\n");
	}


	void Lua::Pop(lua_State* lua, Int count)
	{
		lua_pop(lua, count);
	}


	bool Lua::GetBoolean(lua_State* lua, int index)
	{
		int value = lua_toboolean(lua, index);
		return (value != 0) ? true : false;
	}


	lua_Integer Lua::GetInteger(lua_State* lua, int index)
	{
		int is_integer = 0;
		lua_Integer value = lua_tointegerx(lua, index, &is_integer);

		if(is_integer == 0)
		{
			//... THROW
			ExsTraceError(TRC_Script_System, "Type error.");
		}

		return value;
	}


	lua_Number Lua::GetFloatingPoint(lua_State* lua, int index)
	{
		int is_number = 0;
		lua_Number value = lua_tonumberx(lua, index, &is_number);

		if(is_number == 0)
		{
			//... THROW
			ExsTraceError(TRC_Script_System, "Type error.");
		}

		return value;
	}


	void* Lua::GetPointer(lua_State* lua, int index)
	{
		void* ptr = lua_touserdata(lua, index);

		if(ptr == nullptr)
		{
			//... THROW
			ExsTraceError(TRC_Script_System, "Type error.");
		}

		return ptr;
	}


	const char* Lua::GetString(lua_State* lua, int index)
	{
		size_t str_length = Invalid_Length;
		const char* str = lua_tolstring(lua, index, &str_length);

		if(str == nullptr)
		{
			//... THROW
			ExsTraceError(TRC_Script_System, "Type error.");
		}

		return str;
	}


	void* Lua::GetUserData(lua_State* lua, int index, const char *metatableName)
	{
		void* user_data = lua_touserdata(lua, index);

		if(user_data != nullptr)
		{
			if(lua_getmetatable(lua, index) != 0)
			{
				luaL_getmetatable(lua, metatableName);

				if(lua_rawequal(lua, -1, -2) == 0)
				{
					ExsTraceError(TRC_Script_System, "Invalid user data: metatable mismatch.");
					user_data = nullptr;
				}

				lua_pop(lua, 2);
			}
			else
			{
				ExsTraceError(TRC_Script_System, "Invalid user data: metatable missing.");
				user_data = nullptr;
			}
		}

		return user_data;
	}


	void Lua::PushBoolean(lua_State* lua, bool value)
	{
		lua_pushboolean(lua, value ? 1 : 0);
	}


	void Lua::PushInteger(lua_State* lua, lua_Integer value)
	{
		lua_pushinteger(lua, value);
	}


	void Lua::PushFloatingPoint(lua_State* lua, lua_Number value)
	{
		lua_pushnumber(lua, value);
	}


	void Lua::PushPointer(lua_State* lua, void* ptr)
	{
		lua_pushlightuserdata(lua, ptr);
	}


	void Lua::PushString(lua_State* lua, const char* str)
	{
		lua_pushstring(lua, str);
	}


	bool Lua::PushUserData(lua_State* lua, void* data, const char* metatableName)
	{
		bool metatable_ok = QueryMetatable(lua, metatableName);

		if(metatable_ok)
		{
			lua_pushlightuserdata(lua, data);

			lua_insert(lua, -2);
			lua_setmetatable(lua, -2);
			luaL_ref(0, 0);
			return true;
		}

		lua_pushnil(lua);

		return false;
	}


}
