
#pragma once

#ifndef __Exs_ScriptSystem_SpecBase_H__
#define __Exs_ScriptSystem_SpecBase_H__

#include "Prerequisites.h"


namespace Exs
{

	enum class VariableAccess : Enum
	{
		Read_Only,
		Read_Write
	};


	struct FunctionSpec
	{
		const char*		name;
		lua_CFunction	wrapper;
	};

	struct MemberVariableSpec
	{
		const char*		name;
		VariableAccess	access;
		lua_CFunction	handler_get;
		lua_CFunction	handler_set;
	};

	struct StaticVariableSpec
	{
		const char*		name;
		VariableAccess	access;
		void*			ptr;
	};


	class EnumSpecification
	{
	public:
		struct Enumerator
		{
			const char*		name;
			Uint64			value;
		};

		typedef std::vector<Enumerator> Enumerators;

	public:
		EnumSpecification();
	};


}


#endif /* __Exs_ScriptSystem_SpecBase_H__ */
