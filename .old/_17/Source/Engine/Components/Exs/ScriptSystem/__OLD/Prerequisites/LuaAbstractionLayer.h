
#pragma once

#ifndef __Exs_ScriptSystem_LuaAbstractionLayer_H__
#define __Exs_ScriptSystem_LuaAbstractionLayer_H__

#undef EXS_LIBAPI_SCRIPTSYSTEM
#define EXS_LIBAPI_SCRIPTSYSTEM

namespace Exs
{


	namespace Lua
	{


		///<summary>
		/// Opens standard LUA library, allowing for automatic removal from the stack (default).
		///</summary>
		EXS_LIBAPI_SCRIPTSYSTEM void OpenLibrary(lua_State* lua, LuaCallback loadCallback);
		
		///<summary>
		/// Queries metatable with given name. Returns true if it has been found or false otherwise.
		///</summary>
		EXS_LIBAPI_SCRIPTSYSTEM bool QueryMetatable(lua_State* lua, const char* metatableName);

		///<summary>
		/// Raises type error caused by value at index <c>index</c>.
		///</summary>
		EXS_LIBAPI_SCRIPTSYSTEM void TypeError(lua_State* lua, int index, const char* expectedStr);
		
		///<summary>
		/// Allocates new userdata of specified size and sets metatable of given name.
		///</summary>
		EXS_LIBAPI_SCRIPTSYSTEM void* NewObject(lua_State* lua, Size_t size, const char* metatableName);
		
		///<summary>
		/// Prints on the screen all values currently residing on the stack.
		///</summary>
		EXS_LIBAPI_SCRIPTSYSTEM void DumpStack(lua_State* lua);
		
		///<summary>
		/// Removes specified number of values from the top of the stack.
		///</summary>
		EXS_LIBAPI_SCRIPTSYSTEM void Pop(lua_State* lua, Int count);
		
		///<summary>
		/// Retrieves value from the top of the stack as boolean. Performs basic type checking. Does not remove value from the stack.
		///</summary>
		EXS_LIBAPI_SCRIPTSYSTEM bool GetBoolean(lua_State* lua, int index);
		
		///<summary>
		/// Retrieves value from the top of the stack as integral number. Performs basic type checking. Does not remove value from the stack.
		///</summary>
		EXS_LIBAPI_SCRIPTSYSTEM lua_Integer GetInteger(lua_State* lua, int index);
		
		///<summary>
		/// Retrieves value from the top of the stack as floating point number. Performs basic type checking. Does not remove value from the stack.
		///</summary>
		EXS_LIBAPI_SCRIPTSYSTEM lua_Number GetFloatingPoint(lua_State* lua, int index);
		
		///<summary>
		/// Retrieves value from the top of the stack as raw pointer. Performs basic type checking. Does not remove value from the stack.
		///</summary>
		EXS_LIBAPI_SCRIPTSYSTEM void* GetPointer(lua_State* lua, int index);
		
		///<summary>
		/// Retrieves value from the top of the stack as string literal. Performs basic type checking. Does not remove value from the stack.
		///</summary>
		EXS_LIBAPI_SCRIPTSYSTEM const char* GetString(lua_State* lua, int index);
		
		///<summary>
		/// Retrieves value from the top of the stack as user data. Performs basic type checking. Does not remove value from the stack.
		///</summary>
		EXS_LIBAPI_SCRIPTSYSTEM void* GetUserData(lua_State* lua, int index, const char* metatableName);
		
		///<summary>
		/// Pushes boolean value to the top of the stack.
		///</summary>
		EXS_LIBAPI_SCRIPTSYSTEM void PushBoolean(lua_State* lua, bool value);
		
		///<summary>
		/// Pushes integral number to the top of the stack.
		///</summary>
		EXS_LIBAPI_SCRIPTSYSTEM void PushInteger(lua_State* lua, lua_Integer value);
		
		///<summary>
		/// Pushes floating point number to the top of the stack.
		///</summary>
		EXS_LIBAPI_SCRIPTSYSTEM void PushFloatingPoint(lua_State* lua, lua_Number value);
		
		///<summary>
		/// Pushes raw pointer to the top of the stack.
		///</summary>
		EXS_LIBAPI_SCRIPTSYSTEM void PushPointer(lua_State* lua, void* ptr);
		
		///<summary>
		/// Pushes string literal to the top of the stack.
		///</summary>
		EXS_LIBAPI_SCRIPTSYSTEM void PushString(lua_State* lua, const char* str);
		
		///<summary>
		/// Pushes user data to the top of the stack.
		///</summary>
		EXS_LIBAPI_SCRIPTSYSTEM bool PushUserData(lua_State* lua, void* data, const char* metatableName);

		
		///<summary>
		///</summary>
		template <class T>
		inline typename LuaDataType<T>::Type GetValue(lua_State* lua, Int index)
		{
			typedef typename LuaDataType<T>::Type ResultType;

			int argument_index = static_cast<int>(index);
			void* argument_ptr = GetUserData(lua, argument_index, GetTypeMetatableName<T>());

			return reinterpret_cast<ResultType>(argument_ptr);
		}

		template <>
		inline typename LuaDataType<void*>::Type GetValue<void*>(lua_State* lua, Int index)
		{
			auto result = GetPointer(lua, static_cast<int>(index));
			return static_cast<void*>(result);
		}

		template <>
		inline typename LuaDataType<const char*>::Type GetValue<const char*>(lua_State* lua, Int index)
		{
			auto result = GetString(lua, static_cast<int>(index));
			return static_cast<const char*>(result);
		}

		template <>
		inline typename LuaDataType<bool>::Type GetValue<bool>(lua_State* lua, Int index)
		{
			auto result = GetBoolean(lua, static_cast<int>(index));
			return static_cast<bool>(result);
		}

		template <>
		inline typename LuaDataType<Int32>::Type GetValue<Int32>(lua_State* lua, Int index)
		{
			auto result = GetInteger(lua, static_cast<int>(index));
			return truncate_cast<Int32>(result);
		}

		template <>
		inline typename LuaDataType<Uint32>::Type GetValue<Uint32>(lua_State* lua, Int index)
		{
			auto result = GetInteger(lua, static_cast<int>(index));
			return truncate_cast<Uint32>(result);
		}

		template <>
		inline typename LuaDataType<Int64>::Type GetValue<Int64>(lua_State* lua, Int index)
		{
			auto result = GetInteger(lua, static_cast<int>(index));
			return static_cast<Int64>(result);
		}

		template <>
		inline typename LuaDataType<Uint64>::Type GetValue<Uint64>(lua_State* lua, Int index)
		{
			auto result = GetInteger(lua, static_cast<int>(index));
			return static_cast<Uint64>(result);
		}

		template <>
		inline typename LuaDataType<float>::Type GetValue<float>(lua_State* lua, Int index)
		{
			auto result = GetFloatingPoint(lua, static_cast<int>(index));
			return truncate_cast<float>(result);
		}

		template <>
		inline typename LuaDataType<double>::Type GetValue<double>(lua_State* lua, Int index)
		{
			auto result = GetFloatingPoint(lua, static_cast<int>(index));
			return static_cast<double>(result);
		}
		
		
		///<summary>
		///</summary>
		template <class T>
		inline auto PopValue(lua_State* lua, Int index) -> decltype(GetValue<T>(lua, index))
		{
			auto result = GetValue<T>(lua, index);
			Pop(lua, 1);

			return result;
		}
		
		
		///<summary>
		///</summary>
		template <class R>
		inline void PushValue(lua_State* lua, typename LuaDataType<R>::Type value)
		{
			void* user_data_ptr = reinterpret_cast<void*>(value);
			PushUserData(lua, user_data_ptr, GetTypeMetatableName<R>());
		}
		
		template <>
		inline void PushValue<void*>(lua_State* lua, LuaDataType<void*>::Type value)
		{
			PushPointer(lua, value);
		}
		
		template <>
		inline void PushValue<const char*>(lua_State* lua, LuaDataType<const char*>::Type value)
		{
			PushString(lua, value);
		}
		
		template <>
		inline void PushValue<bool>(lua_State* lua, LuaDataType<bool>::Type value)
		{
			PushBoolean(lua, value);
		}
		
		template <>
		inline void PushValue<Int32>(lua_State* lua, LuaDataType<Int32>::Type value)
		{
			PushInteger(lua, value);
		}
		
		template <>
		inline void PushValue<Uint32>(lua_State* lua, LuaDataType<Uint32>::Type value)
		{
			PushInteger(lua, value);
		}
		
		template <>
		inline void PushValue<Int64>(lua_State* lua, LuaDataType<Int64>::Type value)
		{
			PushInteger(lua, value);
		}
		
		template <>
		inline void PushValue<Uint64>(lua_State* lua, LuaDataType<Uint64>::Type value)
		{
			PushInteger(lua, value);
		}
		
		template <>
		inline void PushValue<float>(lua_State* lua, LuaDataType<float>::Type value)
		{
			PushFloatingPoint(lua, value);
		}
		
		template <>
		inline void PushValue<double>(lua_State* lua, LuaDataType<double>::Type value)
		{
			PushFloatingPoint(lua, value);
		}


	}


}


#endif /* __Exs_ScriptSystem_LuaAbstractionLayer_H__ */
