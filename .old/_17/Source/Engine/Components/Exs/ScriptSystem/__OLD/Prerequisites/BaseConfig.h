
#pragma once

#ifndef __Exs_ScriptSystem_BaseConfig_H__
#define __Exs_ScriptSystem_BaseConfig_H__

#include <Exs/Common/HashCode.h>
#include <Exs/Core/Prerequisites.h>

extern "C"
{
	#include <Lua/lua.h>
	#include <Lua/lualib.h>
	#include <Lua/lauxlib.h>
}


#define EXS_SCRIPT_ENGINE_NAME LUA_VERSION


#if ( _EXS_BUILD_COMPONENT_SCRIPTSYSTEM )
#
#	define EXS_LIBAPI_SCRIPTSYSTEM		EXS_ATTR_DLL_EXPORT
#	define EXS_LIBCLASS_SCRIPTSYSTEM	EXS_ATTR_DLL_EXPORT
#	define EXS_LIBOBJECT_SCRIPTSYSTEM	extern EXS_ATTR_DLL_EXPORT
#
#else
#
#	define EXS_LIBAPI_SCRIPTSYSTEM		EXS_ATTR_DLL_IMPORT
#	define EXS_LIBCLASS_SCRIPTSYSTEM	EXS_ATTR_DLL_IMPORT
#	define EXS_LIBOBJECT_SCRIPTSYSTEM	extern EXS_ATTR_DLL_IMPORT
#
#endif

void LuaDumpStack(lua_State* lua)
{
	int top = lua_gettop(lua);

	if (top == 0)
		printf("Stack empty.");
	else
		printf("Stack size = %u: ", top);

	for (int i = 1; i <= top; i++)
	{
		int t = lua_type(lua, -i);

		switch (t)
		{
		case LUA_TSTRING:
			printf("string (\"%s\")", lua_tostring(lua, -i));
			break;

		case LUA_TBOOLEAN:
			printf("bool (%s)", lua_toboolean(lua, -i) ? "true" : "false");
			break;

		case LUA_TNUMBER:
			printf("number (%g)", lua_tonumber(lua, -i));
			break;

		default:
			printf("%s", lua_typename(lua, t));
			break;
		}

		printf("  ");
	}

	printf("\n");
}

#endif /* __Exs_ScriptSystem_BaseConfig_H__ */
