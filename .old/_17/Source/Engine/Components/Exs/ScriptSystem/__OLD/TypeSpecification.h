
#pragma once

#ifndef __Exs_ScriptSystem_TypeSpecification_H__
#define __Exs_ScriptSystem_TypeSpecification_H__

#include "SpecBase.h"


namespace Exs
{


	class TypeSpecification
	{
	public:
		typedef std::vector<EnumSpecification> Enums;
		typedef std::vector<FunctionSpec> MetaFunctions;
		typedef std::vector<FunctionSpec> MemberFunctions;
		typedef std::vector<FunctionSpec> StaticFunctions;
		typedef std::vector<MemberVariableSpec> MemberVariables;
		typedef std::vector<StaticVariableSpec> StaticVariables;
		typedef std::vector<TypeSpecification> NestedTypes;
	};


}


#endif /* __Exs_ScriptSystem_TypeSpecification_H__ */
