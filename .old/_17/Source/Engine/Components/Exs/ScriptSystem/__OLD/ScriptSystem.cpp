
#include "Precompiled.h"
#include <Exs/ScriptSystem/__OLD/ScriptSystem.h>
#include <Exs/ScriptSystem/__OLD/ScriptEngineInterface.h>
#include <Exs/ScriptSystem/__OLD/CompiledScript.h>


namespace Exs
{


	ScriptSystem::ScriptSystem()
	{
		ScriptEngineInterface(&(this->_state)).Initialize();
	}


	ScriptSystem::~ScriptSystem()
	{
		ScriptEngineInterface(&(this->_state)).Release();
	}


	void ScriptSystem::RunGC()
	{
	}


	CompiledScript* ScriptSystem::CreateScript(const char* name, const Byte* binary, Size_t length)
	{
		CompiledScript* script = rcnew CompiledScript(name, binary, length);
		return script;
	}


	CompiledScript* ScriptSystem::CompileScript(const char* name, const void* source, Size_t length)
	{
		const char* source_str = reinterpret_cast<const char*>(source);

		DynamicDataArray<Byte> binary { };

		Result compilation_result =
			ScriptEngineInterface(&(this->_state)).CompileSource(name, source_str, length, binary);

		if(compilation_result != RSC_Success)
			return nullptr;
		
		CompiledScript* script = rcnew CompiledScript(name, std::move(binary));

		return script;
	}


	Result ScriptSystem::Execute(CompiledScript* script)
	{
		if(script == nullptr)
			ExsThrowExceptionEx(EXC_Null_Pointer, "Script is null.");

		const char* name = script->GetNameStr();
		const Byte* binary = script->GetBinary();
		Size_t binary_length = script->GetBinaryLength();

		Result execution_result =
			ScriptEngineInterface(&(this->_state)).ExecuteCompiledScript(name, binary, binary_length);

		return execution_result;
	}


	Result ScriptSystem::Parse(const String& command, const char* name)
	{
		if(command.IsEmpty())
			return RSC_Err_Failed;

		const char* source = command.GetBuffer();
		Size_t source_length = command.Length();
		
		Result execution_result =
			ScriptEngineInterface(&(this->_state)).ExecuteTextScript(name, source, source_length);

		return execution_result;
	}


	void ScriptSystem::RegisterLib(const char* name, const luaL_Reg* reg)
	{
		ScriptEngineInterface(&(this->_state)).RegisterLib(name, reg);
	}


	void ScriptSystem::RegisterType(const char* libName, const char* metaName, const luaL_Reg* lib, const luaL_Reg* meta)
	{
		ScriptEngineInterface(&(this->_state)).RegisterType(libName, metaName, lib, meta);
	}


}
