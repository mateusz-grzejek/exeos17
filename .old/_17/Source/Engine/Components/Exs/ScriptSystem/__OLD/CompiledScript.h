
#pragma once

#ifndef __Exs_ScriptSystem_Script_H__
#define __Exs_ScriptSystem_Script_H__

#include "Prerequisites.h"


namespace Exs
{


	class CompiledScript
	{
	};


	typedef std::shared_ptr<CompiledScript> CompiledScriptRefPtr;


}


#endif /* __Exs_ScriptSystem_Script_H__ */
