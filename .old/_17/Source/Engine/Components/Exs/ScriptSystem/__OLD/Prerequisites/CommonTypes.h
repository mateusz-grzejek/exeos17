
#pragma once

#ifndef __Exs_ScriptSystem_CommonTypes_H__
#define __Exs_ScriptSystem_CommonTypes_H__


namespace Exs
{


	struct ScriptEngineState
	{
		lua_State* lua_state = nullptr;
	};


}


#endif /* __Exs_ScriptSystem_CommonTypes_H__ */
