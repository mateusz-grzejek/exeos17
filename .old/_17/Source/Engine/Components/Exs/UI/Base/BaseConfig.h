
#ifndef __Exs_UI_BaseConfig_H__
#define __Exs_UI_BaseConfig_H__

#include <ExsEngine/Prerequisites.h>


#if ( EXS_BUILD_MODULE_UI )
#  define EXS_LIBAPI_UI       EXS_MODULE_EXPORT
#  define EXS_LIBCLASS_UI     EXS_MODULE_EXPORT
#  define EXS_LIBOBJECT_UI    extern EXS_MODULE_EXPORT
#else
#  define EXS_LIBAPI_UI       EXS_MODULE_IMPORT
#  define EXS_LIBCLASS_UI     EXS_MODULE_IMPORT
#  define EXS_LIBOBJECT_UI    extern EXS_MODULE_IMPORT
#endif


#endif /* __Exs_UI_BaseConfig_H__ */
