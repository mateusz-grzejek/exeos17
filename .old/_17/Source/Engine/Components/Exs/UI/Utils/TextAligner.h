
#ifndef __Exs_UI_TextAligner_H__
#define __Exs_UI_TextAligner_H__

#include "../Prerequisites.h"


namespace Exs
{


	struct FontGlyphMetrics;

	class Font;


	enum class TextAlignmentMode : Enum
	{
		Left,

		Right,

		Center,

		Default = Left
	};

	
	///<summary>
	///</summary>
	struct TextChar
	{
		// Layer index within a font.
		CodePoint codePoint;

		// Computed position of a character.
		RectI64 rect;
	};
	

	///<summary>
	///</summary>
	struct TextLine
	{
		typedef std::vector<TextChar> TextCharArray;

		TextCharArray chars;

		Uint64 width = 0;
	};

	
	///<summary>
	///</summary>
	struct AlignedTextLine : public TextLine
	{
		// Extra space which should be added to the position of each glyph.
		// May be 
		Uint64 extraCharSpace = 0;
	};

	
	///<summary>
	///</summary>
	struct AlignedText
	{
		typedef std::vector<AlignedTextLine> AlignedLineArray;

		AlignedLineArray lines;
	};


	struct TextAlignmentSettings
	{
		TextAlignmentMode alignmentMode;

		Uint32 lineWidth;

		Vector4U32 margin;

		Uint16 tabSize = 4;

		bool enableMultiline = true;
	};

	
	///<summary>
	///</summary>
	class TextAligner
	{
	private:
		Font*                    _font; // Font used to align text.
		Uint32                   _fontLineHeight; // Height of a single line of text (fetched from the Font).
		const FontGlyphMetrics*  _cmSpaceMetrics; // Cached metrics of a space (0x20) character (fetched from the Font).
		TextAlignmentSettings    _settings;
		AlignedText              _alignedText; // Aligned text data.
		Size_t                   _currentLineIndex; // Index of the current line of text (starting from 0).
		AlignedTextLine*         _currentLine; // Pointer to the current line.
		Range<Uint64>            _charPosXRange; // Range of possible x position of a characters ([0;lineWidth-1] if no padding is specified)
		Vector2U64               _currentOffset; // Current x offset of chars position.
		
	public:
		TextAligner(Font& font, const TextAlignmentSettings& settings);
		
		///<summary>
		///</summary>
		void Align(const std::string& text);
		
		///<summary>
		///</summary>
		void Append(const std::string& text);
		
		///<summary>
		///</summary>
		void Reset();
		
		///<summary>
		///</summary>
		void SetFont(Font& font);
		
		///<summary>
		///</summary>
		void UpdateSettings(const TextAlignmentSettings& settings);

	private:
		//
		Uint64 _GetLineFreeSpace() const;

		//
		bool _CheckLineSpace(Uint64 width) const;

		//
		void _ReserveLineSpace(Uint64 width);
		
		//
		void _NextLine();
		
		//
		bool _Append(const std::string& text);
		
		//
		void _ProcessSpace(Size_t count);
		
		//
		bool _ProcessSpecialChar(char ch);

		//
		std::pair<Size_t, Uint64> _ProcessSubtext(const std::string& text, Size_t start, std::vector<TextChar>* charData);

		//
		void _ReparseAsNextLine(std::vector<TextChar>& charData);
	};


	inline Uint64 TextAligner::_GetLineFreeSpace() const
	{
		return (this->_charPosXRange.max > this->_currentOffset.x) ? (this->_charPosXRange.max - this->_currentOffset.x) : 0;
	}

	inline bool TextAligner::_CheckLineSpace(Uint64 width) const
	{
		Uint64 freeSpace = this->_GetLineFreeSpace();
		return width <= freeSpace;
	}


}


#endif /* __Exs_UI_TextAligner_H__ */
