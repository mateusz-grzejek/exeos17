
#ifndef __Exs_UI_Text_H__
#define __Exs_UI_Text_H__

#include "../UIControl.h"


namespace Exs
{


	class Font;


	// Represents single, pre-processed character of a text.
	struct TextCharData
	{
		// Layer index within a font.
		Int32 bitmapLayer;

		// Computed position of a character.
		RectI64 rect;

		// Source texture coordinates.
		RectF texc;
	};


	//
	struct TextProperties
	{
		// Base offset of position, in pixel units.
		Vector3F baseOffset;

		// Color of the text.
		Uint32 color;

		// Font used by the text.
		Font* font;
	};


	namespace TextUtils
	{

		std::vector<TextCharData> GenerateTextCharData(const std::string& text, const TextProperties& textProperties);

	}


	class EXS_LIBCLASS_UI TextLabel : public UIControl
	{
	private:
		Font*               _font;
		Uint32              _maxLength;
		std::string         _text;
		Handle<UIGeometry>  _geometry;

	public:
		TextLabel(UIControl* parent, UILayer* layer, Font& font, Uint32 maxLength, const std::string& text, const Vector2U32& position, const Color& color = ColorU32::White);
		
		void SetText(const std::string& text);

		const std::string& GetText() const;

	protected:
		virtual bool OnRenderBegin() override;
		virtual void OnRenderEnd() override;

	private:
		void _UpdateGeometry();
	};


	inline void TextLabel::SetText(const std::string& text)
	{
		this->_text = text;
		this->_state.set(UIControlState_Update_Geometry);
	}
	
	inline const std::string& TextLabel::GetText() const
	{
		return this->_text;
	}


}


#endif /* __Exs_UI_Text_H__ */
