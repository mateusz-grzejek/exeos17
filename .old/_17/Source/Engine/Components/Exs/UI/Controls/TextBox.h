
#ifndef __Exs_UI_UITextBox_H__
#define __Exs_UI_UITextBox_H__

#include "TextBase.h"
#include "../UIComponent.h"


namespace Exs
{


	struct TextBoxDimensions
	{
		Uint32 maxLineLength;

		Uint32 linesNum;
	};


	class EXS_LIBCLASS_UI UITextBox : public UIComponent
	{
	private:
		Font*  _font;
		RectF  _boundingRect;

	public:
		template <typename Pv_t, typename Sv_t>
		UITextBox(UILayer* layer, UIControl* parent, const Vector2<Pv_t>& position, const Vector2<Sv_t>& size, Font* font, const Color& color = ColorU32::White);

		template <typename Pv_t, typename Sv_t>
		UITextBox(UILayer* layer, UIControl* parent, UIAnchorPoint anchorPoint, const Vector2<Pv_t>& position, const Vector2<Sv_t>& size, Font* font, const Color& color);


	};


}


#endif /* __Exs_UI_UITextBox_H__ */
