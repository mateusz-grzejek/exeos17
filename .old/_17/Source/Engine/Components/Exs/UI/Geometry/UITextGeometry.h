
#ifndef __Exs_UI_UITextGeometry_H__
#define __Exs_UI_UITextGeometry_H__

#include "UIGeometry.h"


namespace Exs
{

	
	///<summary>
	///</summary>
	struct UITextGeometryBatch
	{
		//
		Uint verticesBaseOffset;
	
		//
		Uint32 verticesNum;

		//
		ShaderResourceViewHandle sourceTexture;
	};

	
	///<summary>
	///</summary>
	struct UITextGeometryVertex
	{
		// 3D position: 3 x sizeof(float) = 12 bytes.
		Vector3F position;

		// 3D texture coord: 3 x sizeof(float) = 12 bytes.
		Vector3F texCoord;
	
		// Constant color: sizeof(Uint32) = 4 bytes.
		Uint32 fixedColor;

		// Padding value: sizeof(Uint32) = 4 bytes.
		Uint32 _padding;
	};

	
	///<summary>
	///</summary>
	struct UITextGeometry : public Base2DRectGeometry
	{
		// Text which was used to generate geometry. Used to enable minimal updates.
		std::string text;
	};

	
	///<summary>
	///</summary>
	class UITextGeometryComposer
	{
	public:
		static void ComposeTextGeometry();

	private:
		static void FillTextCharRectIndexed(const TextCharData& charData, /*const TextProperties& textProperties,*/ UITextGeometryVertex* vertices);

		static void FillTextCharRectNonIndexed(const TextCharData& charData, /*const TextProperties& textProperties,*/ UITextGeometryVertex* vertices);
	};


}


#endif /* __Exs_UI_UITextGeometry_H__ */
