
#include <ExsUI/UILayer.h>


namespace Exs
{


	UILayer::UILayer(UIGeometryManager* geometryManager, UILayer* prev, Int32 depth)
	: _geometryManager(geometryManager)
	, _prev(prev)
	, _depth(depth)
	{ }


	UILayer::~UILayer()
	{ }

	
	UILayer* UILayer::NextLayer()
	{
		if (!this->_next)
			this->_next = std::make_unique<UILayer>(this->_geometryManager, this, this->_depth);

		return this->_next.get();
	}


}
