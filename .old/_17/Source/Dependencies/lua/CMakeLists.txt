cmake_minimum_required(VERSION 3.6)
project(lua)

file(GLOB SRC_FILES
    "*.h"
    "*.c")

set(CMCFG_EXS_Dependency_lua_SRC_FILES ${SRC_FILES} PARENT_SCOPE)
