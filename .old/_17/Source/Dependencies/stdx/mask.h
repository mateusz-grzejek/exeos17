
#ifndef __STDX_MASK_H__
#define __STDX_MASK_H__

#include "type_traits.h"


namespace stdx
{


	template <typename _Tp>
	class mask
	{
		static_assert((std::is_integral<_Tp>::value || std::is_enum<_Tp>::value) && !std::is_same<_Tp, bool>::value,
			"Atomic masks are only valid for integer and enum types (but not a bool type)!");

	public:
		typedef mask<_Tp> my_type;
		typedef typename uint_type_by_size<sizeof( _Tp )>::type value_type;

	private:
		value_type  _value;

	public:
		mask()
			: _value( 0 )
		{ }

		template <typename _En>
		mask( _En value )
			: _value( static_cast<value_type>(value) )
		{ }

		template <typename _En>
		my_type& operator=( _En value )
		{
			this->_value = static_cast<value_type>(value);
			return *this;
		}

		operator value_type() const
		{
			return static_cast<value_type>(this->_value);
		}

		template <typename _En>
		void store( _En mask )
		{
			this->_value = static_cast<value_type>(mask);
		}

		template <typename _En>
		void set( _En mask )
		{
			this->_value |= static_cast<value_type>(mask);
		}

		template <typename _En>
		void toggle( _En mask )
		{
			this->_value ^= static_cast<value_type>(mask);
		}

		template <typename _En>
		void unset( _En mask )
		{
			this->_value &= ~static_cast<value_type>(mask);
		}

		void invert()
		{
			this->_value = ~(this->_value);
		}

		template <typename _En>
		bool test_and_set( _En mask )
		{
			if ((this->_value & static_cast<value_type>(mask)) != 0)
				return false;

			this->_value |= static_cast<value_type>(mask);
			return true;
		}

		template <typename _En>
		bool test_and_unset( _En mask )
		{
			if ((this->_value & static_cast<value_type>(mask)) != static_cast<value_type>(mask))
				return false;

			this->_value &= ~static_cast<value_type>(mask);
			return true;
		}

		void clear()
		{
			this->_value = 0;
		}

		value_type get() const
		{
			return this->_value;
		}

		template <typename _En>
		value_type test( _En mask ) const
		{
			return this->_value & static_cast<value_type>(mask);
		}

		template <typename _En>
		bool is_set( _En mask ) const
		{
			return (this->_value & static_cast<value_type>(mask)) == static_cast<value_type>(mask);
		}

		template <typename _En>
		void operator|=( _En mask )
		{
			this->_value |= static_cast<value_type>(mask);
		}

		template <typename _En>
		void operator&=( _En mask )
		{
			this->_value &= static_cast<value_type>(mask);
		}

		template <typename _En>
		void operator^=( _En mask )
		{
			this->_value ^= static_cast<value_type>(mask);
		}

		void operator<<=( size_t
			shift )
		{
			this->_value <<= shift;
		}

		void operator>>=( size_t shift )
		{
			this->_value <<= shift;
		}

		value_type operator~() const
		{
			return ~(this->_value);
		}

		template <typename _En>
		value_type operator|( _En mask ) const
		{
			return this->_value | static_cast<value_type>(mask);
		}

		template <typename _En>
		value_type operator&( _En mask ) const
		{
			return this->_value & static_cast<value_type>(mask);
		}

		template <typename _En>
		value_type operator^( _En mask ) const
		{
			return this->_value ^ static_cast<value_type>(mask);
		}

		value_type operator<<( size_t shift ) const
		{
			return this->_value << shift;
		}

		value_type operator>>( size_t shift ) const
		{
			return this->_value >> shift;
		}
	};


}


#endif /* __STDX_MASK_H__*/
