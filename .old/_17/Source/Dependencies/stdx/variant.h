
#ifndef __STDX_VARIANT_H__
#define __STDX_VARIANT_H__

#include "static_algo.h"


namespace stdx
{


	//
	typedef size_t type_index_t;

	//
	constexpr type_index_t invalid_type_index = 0;


	template <typename T>
	struct variant_type_index
	{
		static const type_index_t value;
	};


	template <typename T>
	const type_index_t variant_type_index<T>::value = typeid(T).hash_code();


	template <typename... Types>
	struct variant_storage
	{
		static constexpr size_t alignment = static_max_alignment_of<Types...>::value;
		static constexpr size_t size = static_max_size_of<Types...>::value;

		typedef typename std::aligned_storage<size, alignment>::type type;
	};


	template <typename... Types>
	struct variant_helper;


	template <typename F, typename... Rest>
	struct variant_helper<F, Rest...>
	{
		template <typename... Args>
		static void construct( type_index_t tpIndex, void* storage, Args&&... args )
		{
			if (tpIndex == variant_type_index<F>::value)
			{
				new (reinterpret_cast<F*>(storage)) F( std::forward<Args>( args )... );
			}
			else
			{
				variant_helper<Rest...>::construct( tpIndex, storage, std::forward<Args>( args )... );
			}
		}

		static void construct_default( void* storage )
		{
			if (std::is_default_constructible<F>::value)
			{
				new (reinterpret_cast<F*>(storage)) F();
			}
			else
			{
				variant_helper<Rest...>::construct_default( storage );
			}
		}

		static void copy( type_index_t tpIndex, void* storage, void* origin )
		{
			if (tpIndex == variant_type_index<F>::value)
			{
				new (reinterpret_cast<F*>(storage)) F( *(reinterpret_cast<F*>(origin)) );
			}
			else
			{
				variant_helper<Rest...>::copy( tpIndex, origin, storage );
			}
		}

		static void move( type_index_t tpIndex, void* storage, void* source )
		{
			if (tpIndex == variant_type_index<F>::value)
			{
				new (reinterpret_cast<F*>(storage)) F( std::move( *(reinterpret_cast<F*>(source)) ) );
			}
			else
			{
				variant_helper<Rest...>::move( tpIndex, source, storage );
			}
		}

		static void destroy( type_index_t tpIndex, void* storage )
		{
			if (tpIndex == variant_type_index<F>::value)
			{
				reinterpret_cast<F*>(storage)->~F();
			}
			else
			{
				variant_helper<Rest...>::destroy( tpIndex, storage );
			}
		}

		static type_index_t construct_default_if_possible( void* storage )
		{
			if (std::is_default_constructible<F>::value)
			{
				new (reinterpret_cast<F*>(storage)) F();
				return variant_type_index<F>::value;
			}
			else
			{
				variant_helper<Rest...>::construct_default_if_possible( storage );
			}
		}
	};


	template <>
	struct variant_helper<>
	{
		template <typename... Args>
		static void construct( type_index_t tpIndex, void* storage, Args&&... args )
		{

		}

		static void construct_default( void* storage )
		{

		}

		static void copy( type_index_t tpIndex, void* storage, void* origin )
		{

		}

		static void move( type_index_t tpIndex, void* storage, void* source )
		{

		}

		static void destroy( type_index_t tpIndex, void* storage )
		{

		}

		static type_index_t construct_default_if_possible( void* storage )
		{
			// This method should do nothing if there is no default-constructible type available.
			return invalid_type_index;
		}
	};


	template <typename... Types>
	class variant
	{
	public:
		static constexpr size_t storage_alignment = variant_storage<Types...>::alignment;
		static constexpr size_t storage_size = variant_storage<Types...>::size;
		\
			typedef variant<Types...> my_type;

		typedef typename variant_storage<Types...>::type storage_type;

	private:
		storage_type  _storage;
		type_index_t  _typeIndex;

	public:
		variant()
		{
			this->_typeIndex = variant_helper<Types...>::construct_default_if_possible( &(this->_storage) );
		}

		variant( variant<Types...>&& source )
			: _typeIndex( source._typeIndex )
		{
			variant_helper<Types...>::move( this->_typeIndex, &(this->_storage), &(source._storage) );
		}

		variant( const variant<Types...>& origin )
			: _typeIndex( origin._typeIndex )
		{
			variant_helper<Types...>::copy( this->_typeIndex, &(this->_storage), &(origin._storage) );
		}

		template <typename T>
		variant( T&& value )
			: _typeIndex( variant_type_index<T>::value )
		{
			variant_helper<Types...>::construct( this->_typeIndex, &(this->_storage), std::move( value ) );
		}

		template <typename T>
		variant( const T& value )
			: _typeIndex( variant_type_index<T>::value )
		{
			variant_helper<Types...>::construct( this->_typeIndex, &(this->_storage), value );
		}

		~variant()
		{
			this->_release();
		}

		variant& operator=( my_type&& rhs )
		{
			if (this != &rhs)
				my_type( std::move( rhs ) ).swap( *this );

			return *this;
		}

		variant& operator=( const my_type& rhs )
		{
			if (this != &rhs)
				my_type( rhs ).swap( *this );

			return *this;
		}

		template <typename T>
		variant& operator=( T&& rhs )
		{
			this->_reinit<T>( std::move( rhs ) );
			return *this;
		}

		template <typename T>
		variant& operator=( const T& rhs )
		{
			this->_reinit<T>( rhs );
			return *this;
		}

		template <typename T>
		void set( T&& value )
		{
			this->_reinit<T>( std::move( value ) );
		}

		template <typename T>
		void set( const T& value )
		{
			this->_reinit<T>( value );
		}

		template <typename T, typename... Args>
		void emplace( Args&&... args )
		{
			this->_reinit<T>( std::forward<Args>( args )... );
		}

		template <typename T>
		bool check_type() const
		{
			return this->_check<T>();
		}

		template <typename T>
		T& get() const
		{
			this->_validate<T>();
			return *(reinterpret_cast<T*>(&(this->_storage)));
		}

	private:
		template <typename T, typename... Args>
		void _reinit( Args&&... args )
		{
			variant_helper<Types...>::destroy( _typeIndex, &(this->_storage) );
			variant_helper<Types...>::construct( variant_type_index<T>::value, &(this->_storage), std::forward<Args>( args )... );
			this->_typeIndex = variant_type_index<T>::value;
		}

		template <typename T>
		bool _check() const
		{
			return this->_typeIndex == variant_type_index<T>::value;
		}

		template <typename T>
		void _validate() const
		{
			if (this->_typeIndex != variant_type_index<T>::value)
			{
				throw 0;
			}
		}

		void _release()
		{
			variant_helper<Types...>::destroy( this->_typeIndex, &(this->_storage) );
			this->_typeIndex = invalid_type_index;
		}
	};


}


#endif /* __STDX_VARIANT_H__ */
