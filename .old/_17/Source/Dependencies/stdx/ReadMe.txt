
---------------------------------------------------------------------------------------
--------------------------------- STDX Library ReadMe ---------------------------------
---------------------------------------------------------------------------------------

STDX (STD eXtension library) is a small, header-only library created for the Exeos Engine.
It is a bunch of common utilities used during the development process.

Library is provided as-is, without any warranty of any kind. It is still in development
phase and may contain bugs. However, it is used very intensively and constantly fixed.

Copyright (c) 2017-2018 Mateusz Grzejek
