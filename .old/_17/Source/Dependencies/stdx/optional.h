
#ifndef __STDX_OPTIONAL_H__
#define __STDX_OPTIONAL_H__

#include "static_algo.h"


namespace stdx
{


	template <typename T>
	class optional
	{
	public:
		static constexpr size_t storage_alignment = alignof(T);
		static constexpr size_t storage_size = sizeof( T );

		typedef T value_type;
		typedef optional<T> my_type;

		typedef typename std::aligned_storage<storage_size, storage_alignment>::type storage_type;

	private:
		storage_type  _storage;
		T*            _valuePtr;

	public:
		optional()
			: _valuePtr( nullptr )
		{ }

		template <typename T>
		optional( T&& value )
		{
			this->_init( std::move( value ) );
		}

		template <typename T>
		optional( const T& value )
		{
			this->_init( value );
		}

		template <typename... Args>
		optional( const in_place_construct_tag&, Args&&... args )
		{
			this->_init( std::forward<Args>( args )... );
		}

		~optional()
		{
			this->_release();
		}

		optional& operator=( my_type&& rhs )
		{
			if (this != &rhs)
				my_type( std::move( rhs ) ).swap( *this );

			return *this;
		}

		optional& operator=( const my_type& rhs )
		{
			if (this != &rhs)
				my_type( rhs ).swap( *this );

			return *this;
		}

		optional& operator=( T&& rhs )
		{
			this->_reinit( std::move( rhs ) );
			return *this;
		}

		optional& operator=( const T& rhs )
		{
			this->_reinit( rhs );
			return *this;
		}

		operator bool() const
		{
			return this->_check();
		}

		T& operator*() const
		{
			this->_validate();
			return *(this->_valuePtr);
		}

		T* operator->() const
		{
			this->_validate();
			return this->_valuePtr;
		}

		void set( T&& value )
		{
			this->_reinit( std::move( value ) );
		}

		void set( const T& value )
		{
			this->_reinit( value );
		}

		template <typename... Args>
		void emplace( Args&&... args )
		{
			this->_reinit( std::forward<Args>( args )... );
		}

		void reset()
		{
			this->_release();
		}

		bool empty() const
		{
			return !this->_check();
		}

		const T& value() const
		{
			this->_validate();
			return *(this->_valuePtr);
		}

		const T& value_or( const T& def ) const
		{
			return this->_check() ? *(this->_valuePtr) : def;
		}

		void swap( my_type& other )
		{
		}

	private:
		template <typename... Args>
		void _init( Args&&... args )
		{
			this->_valuePtr = reinterpret_cast<T*>(&(this->_storage));
			new (this->_valuePtr) T( std::forward<Args>( args )... );
		}

		template <typename... Args>
		void _reinit( Args&&... args )
		{
			if (this->_valuePtr != nullptr)
			{
				this->_valuePtr->~T();
			}
			else
			{
				this->_valuePtr = reinterpret_cast<T*>(&(this->_storage));
			}

			new (this->_valuePtr) T( std::forward<Args>( args )... );
		}

		bool _check() const
		{
			return this->_valuePtr != nullptr;
		}

		void _validate() const
		{
			if (this->_valuePtr == nullptr)
			{
				throw 0;
			}
		}

		void _release()
		{
			if (this->_valuePtr != nullptr)
			{
				this->_valuePtr->~T();
				this->_valuePtr = nullptr;
			}
		}
	};


}


#endif /* __STDX_OPTIONAL_H__ */
