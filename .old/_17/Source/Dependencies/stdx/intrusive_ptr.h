
#ifndef __STDX_INTRUSIVE_PTR_H__
#define __STDX_INTRUSIVE_PTR_H__

#include "ref_counter.h"
#include <functional>


namespace stdx
{


	template <class T>
	struct array_delete
	{
		void operator()( T* ptr ) const
		{
			delete[] ptr;
		}
	};


	template <class T>
	struct custom_delete
	{
		std::function<void( T* )> deleter;

		template <class F, class... Args>
		custom_delete( F&& f, Args&&... args )
			: deleter( std::bind( std::forward<F>( f ), std::forward<Args>( args )... ) )
		{ }

		void operator()( T* ptr ) const
		{
			this->deleter( ptr );
		}
	};


	template <class T>
	struct default_delete
	{
		void operator()( T* ptr ) const
		{
			delete ptr;
		}
	};


	template <class T>
	struct empty_delete
	{
		void operator()( T* ptr ) const
		{
		}
	};


	template <class Counter = ref_counter>
	class ref_counted_base
	{
		friend struct ref_counted_interface;

	public:
		typedef ref_counted_base<Counter> my_type;
		typedef Counter counter_type;

	private:
		counter_type  _counter;

	public:
		ref_counted_base( const ref_counted_base& ) = delete;
		ref_counted_base& operator=( const ref_counted_base& ) = delete;

		ref_counted_base() = default;

		ref_counter_value_t get_refs_num()
		{
			return this->_counter.get_value();
		}

	private:
		ref_counter_value_t add_ref()
		{
			return this->_counter.increment();
		}

		ref_counter_value_t release_ref()
		{
			return this->_counter.decrement();
		}
	};


	struct ref_counted_interface
	{
		template <class Counter>
		static ref_counter_value_t add_ref_default( ref_counted_base<Counter>* refCountedBase )
		{
			return refCountedBase->add_ref();
		}

		template <class Counter>
		static ref_counter_value_t release_ref_default( ref_counted_base<Counter>* refCountedBase )
		{
			return refCountedBase->release_ref();
		}
	};


	template <class T>
	struct intrusive_ptr_default_destroy
	{
		void operator()( T* ptr ) const
		{
			delete ptr;
		}
	};


	template <class T>
	struct intrusive_ptr_traits
	{
		static ref_counter_value_t add_ref( T* objPtr )
		{
			return ref_counted_interface::add_ref_default( objPtr );
		}

		static ref_counter_value_t release_ref( T* objPtr )
		{
			return ref_counted_interface::release_ref_default( objPtr );
		}
	};


	template < class T, class Deleter = intrusive_ptr_default_destroy<T> >
	class intrusive_ptr
	{
		template <class Tx, class Dx>
		friend class intrusive_ptr;

	public:
		typedef intrusive_ptr<T> my_type;
		typedef intrusive_ptr_traits<T> traits_ref_support;

	private:
		T*  _ptr;

	public:
		intrusive_ptr()
			: _ptr( nullptr )
		{ }

		intrusive_ptr( my_type&& source )
			: _ptr( nullptr )
		{
			this->swap( source );
		}

		intrusive_ptr( const my_type& origin )
			: _ptr( nullptr )
		{
			this->_set_no_rel( origin._ptr );
		}

		template <class Cx>
		intrusive_ptr( intrusive_ptr<Cx>&& other )
			: _ptr( static_cast<T*>(other._ptr) )
		{
			other._ptr = nullptr;
		}

		template <class Cx>
		intrusive_ptr( const intrusive_ptr<Cx>& other )
			: _ptr( nullptr )
		{
			this->_set_no_rel( other._ptr );
		}

		intrusive_ptr( std::nullptr_t )
			: _ptr( nullptr )
		{ }

		explicit intrusive_ptr( T* ptr )
			: _ptr( ptr )
		{ }

		template <class Cx>
		explicit intrusive_ptr( Cx* ptr )
			: _ptr( static_cast<T*>(ptr) )
		{ }

		~intrusive_ptr()
		{
			this->_release();
		}

		my_type& operator=( my_type&& rhs )
		{
			if (this != &rhs)
				my_type( std::move( rhs ) ).swap( *this );

			return *this;
		}

		my_type& operator=( const my_type& rhs )
		{
			if (this != &rhs)
				my_type( rhs ).swap( *this );

			return *this;
		}

		template <class Cx>
		my_type& operator=( intrusive_ptr<Cx>&& rhs )
		{
			my_type( std::move( rhs ) ).swap( *this );
			return *this;
		}

		template <class Cx>
		my_type& operator=( const intrusive_ptr<Cx>& rhs )
		{
			my_type( rhs ).swap( *this );
			return *this;
		}

		my_type& operator=( std::nullptr_t )
		{
			this->_release();
			return *this;
		}

		my_type& operator=( T* rhs )
		{
			my_type( rhs ).swap( *this );
			return *this;
		}

		template <class Cx>
		my_type& operator=( Cx* rhs )
		{
			my_type( rhs ).swap( *this );
			return *this;
		}

		explicit operator bool() const
		{
			return this->_ptr != nullptr;
		}

		explicit operator T*() const
		{
			return this->_ptr;
		}

		T& operator*() const
		{
			assert( this->_ptr != nullptr );
			return *(this->_ptr);
		}

		T* operator->() const
		{
			assert( this->_ptr != nullptr );
			return this->_ptr;
		}

		T* get() const
		{
			return this->_ptr;
		}

		void reset( T* newPtr = nullptr )
		{
			this->_set( newPtr );
		}

		void swap( my_type& other )
		{
			std::swap( this->_ptr, other._ptr );
		}

	private:
		void _release()
		{
			if (this->_ptr != nullptr)
			{
				_release_ptr( this->_ptr );
				this->_ptr = nullptr;
			}
		}

		void _set_no_rel( T* ptr )
		{
			if (ptr != nullptr)
				traits_ref_support::add_ref( ptr );

			this->_ptr = ptr;
		}

		template <class Cx>
		void _set_no_rel( Cx* ptr )
		{
			this->_set_no_rel( static_cast<T*>(ptr) );
		}

		void _set( T* ptr )
		{
			if (ptr != this->_ptr)
			{
				T* prevPtr = this->_ptr;
				this->_set_no_rel( ptr );

				if (prevPtr != nullptr)
					_release_ptr( prevPtr );
			}
		}

		template <class Cx>
		void _set( Cx* ptr )
		{
			this->_set( static_cast<T*>(ptr) );
		}

		static void _release_ptr( T* ptr )
		{
			ref_counter_value_t remainingRefs = traits_ref_support::release_ref( ptr );

			if (remainingRefs == 0)
				Deleter()(ptr);
		}
	};


	template <class Tx, class Ty, class Delx, class Dely>
	inline bool operator==( const intrusive_ptr<Tx, Delx>& lhs, const intrusive_ptr<Ty, Dely>& rhs )
	{
		return lhs.get() == rhs.get();
	}

	template <class Tx, class Ty, class Delx, class Dely>
	inline bool operator!=( const intrusive_ptr<Tx, Delx>& lhs, const intrusive_ptr<Ty, Dely>& rhs )
	{
		return lhs.get() != rhs.get();
	}

	template <class Tx, class Ty, class Delx, class Dely>
	inline bool operator>( const intrusive_ptr<Tx, Delx>& lhs, const intrusive_ptr<Ty, Dely>& rhs )
	{
		return lhs.get() > rhs.get();
	}

	template <class Tx, class Ty, class Delx, class Dely>
	inline bool operator>=( const intrusive_ptr<Tx, Delx>& lhs, const intrusive_ptr<Ty, Dely>& rhs )
	{
		return lhs.get() >= rhs.get();
	}

	template <class Tx, class Ty, class Delx, class Dely>
	inline bool operator<( const intrusive_ptr<Tx, Delx>& lhs, const intrusive_ptr<Ty, Dely>& rhs )
	{
		return lhs.get() < rhs.get();
	}

	template <class Tx, class Ty, class Delx, class Dely>
	inline bool operator<=( const intrusive_ptr<Tx, Delx>& lhs, const intrusive_ptr<Ty, Dely>& rhs )
	{
		return lhs.get() <= rhs.get();
	}


	template <class T, class Deleter>
	void swap( intrusive_ptr<T, Deleter>& left, const intrusive_ptr<T, Deleter>& right )
	{
		left.swap( right );
	}


	template <class T, class... Args>
	inline intrusive_ptr<T> make_instrusive( Args&&... args )
	{
		return intrusive_ptr<T>( new T( std::forward<Args>( args )... ) );
	}


}


#endif /* __STDX_INTRUSIVE_PTR_H__ */
