
#ifndef __STDX_ALGO_H__
#define __STDX_ALGO_H__

#include "common.h"


namespace stdx
{


	template <typename _Tp>
	inline _Tp gcd( _Tp a, typename std::enable_if<std::is_integral<_Tp>::value, _Tp>::type b )
	{
		a = std::abs( a );
		b = std::abs( b );

		while (b != 0)
		{
			const _Tp temporary = b;
			b = a % b;
			a = temporary;
		}

		return a;
	}


}


#endif /* __STDX_ALGO_H__ */

