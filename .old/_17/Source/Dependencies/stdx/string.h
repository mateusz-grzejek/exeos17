
#ifndef __STDX_STRING_H__
#define __STDX_STRING_H__

#include "common.h"
#include <string>


namespace stdx
{


	template <typename _Char, typename _Rt>
	struct char_numeric_conv;


	template <>
	struct char_numeric_conv<char, int32_t>
	{
		static std::pair<int32_t, char*> to_value( const char* str, numeric_base base = numeric_base::decimal )
		{
			char* endPtr = 0; int32_t value = std::strtol( str, &endPtr, static_cast<int>(base) );
			return std::pair<int32_t, char*>( value, endPtr );
		};
	};

	template <>
	struct char_numeric_conv<char, uint32_t>
	{
		static std::pair<uint32_t, char*> to_value( const char* str, numeric_base base = numeric_base::decimal )
		{
			char* endPtr = 0; uint32_t value = std::strtoul( str, &endPtr, static_cast<int>(base) );
			return std::pair<uint32_t, char*>( value, endPtr );
		};
	};

	template <>
	struct char_numeric_conv<char, int64_t>
	{
		static std::pair<int64_t, char*> to_value( const char* str, numeric_base base = numeric_base::decimal )
		{
			char* endPtr = 0; int64_t value = std::strtoll( str, &endPtr, static_cast<int>(base) );
			return std::pair<int64_t, char*>( value, endPtr );
		};
	};

	template <>
	struct char_numeric_conv<char, uint64_t>
	{
		static std::pair<uint64_t, char*> to_value( const char* str, numeric_base base = numeric_base::decimal )
		{
			char* endPtr = 0; uint64_t value = std::strtoull( str, &endPtr, static_cast<int>(base) );
			return std::pair<uint64_t, char*>( value, endPtr );
		};
	};

	template <>
	struct char_numeric_conv<char, float>
	{
		static std::pair<float, char*> to_value( const char* str, numeric_base = numeric_base::decimal )
		{
			char* endPtr = 0; float value = std::strtof( str, &endPtr );
			return std::pair<float, char*>( value, endPtr );
		};
	};

	template <>
	struct char_numeric_conv<char, double>
	{
		static std::pair<double, char*> to_value( const char* str, numeric_base = numeric_base::decimal )
		{
			char* endPtr = 0; double value = std::strtod( str, &endPtr );
			return std::pair<double, char*>( value, endPtr );
		};
	};

	template <>
	struct char_numeric_conv<char, long double>
	{
		static std::pair<long double, char*> to_value( const char* str, numeric_base = numeric_base::decimal )
		{
			char* endPtr = 0; long double value = std::strtold( str, &endPtr );
			return std::pair<long double, char*>( value, endPtr );
		};
	};

	template <>
	struct char_numeric_conv<wchar_t, int32_t>
	{
		static std::pair<int32_t, wchar_t*> to_value( const wchar_t* str, numeric_base base = numeric_base::decimal )
		{
			wchar_t* endPtr = 0; int32_t value = std::wcstol( str, &endPtr, static_cast<int>(base) );
			return std::pair<int32_t, wchar_t*>( value, endPtr );
		};
	};

	template <>
	struct char_numeric_conv<wchar_t, uint32_t>
	{
		static std::pair<uint32_t, wchar_t*> to_value( const wchar_t* str, numeric_base base = numeric_base::decimal )
		{
			wchar_t* endPtr = 0; uint32_t value = std::wcstoul( str, &endPtr, static_cast<int>(base) );
			return std::pair<uint32_t, wchar_t*>( value, endPtr );
		};
	};

	template <>
	struct char_numeric_conv<wchar_t, int64_t>
	{
		static std::pair<int64_t, wchar_t*> to_value( const wchar_t* str, numeric_base base = numeric_base::decimal )
		{
			wchar_t* endPtr = 0; int64_t value = std::wcstoll( str, &endPtr, static_cast<int>(base) );
			return std::pair<int64_t, wchar_t*>( value, endPtr );
		};
	};

	template <>
	struct char_numeric_conv<wchar_t, uint64_t>
	{
		static std::pair<uint64_t, wchar_t*> to_value( const wchar_t* str, numeric_base base = numeric_base::decimal )
		{
			wchar_t* endPtr = 0; uint64_t value = std::wcstoull( str, &endPtr, static_cast<int>(base) );
			return std::pair<uint64_t, wchar_t*>( value, endPtr );
		};
	};

	template <>
	struct char_numeric_conv<wchar_t, float>
	{
		static std::pair<float, wchar_t*> to_value( const wchar_t* str, numeric_base = numeric_base::decimal )
		{
			wchar_t* endPtr = 0; float value = std::wcstof( str, &endPtr );
			return std::pair<float, wchar_t*>( value, endPtr );
		};
	};

	template <>
	struct char_numeric_conv<wchar_t, double>
	{
		static std::pair<double, wchar_t*> to_value( const wchar_t* str, numeric_base = numeric_base::decimal )
		{
			wchar_t* endPtr = 0; double value = std::wcstod( str, &endPtr );
			return std::pair<double, wchar_t*>( value, endPtr );
		};
	};

	template <>
	struct char_numeric_conv<wchar_t, long double>
	{
		static std::pair<long double, wchar_t*> to_value( const wchar_t* str, numeric_base = numeric_base::decimal )
		{
			wchar_t* endPtr = 0; long double value = std::wcstold( str, &endPtr );
			return std::pair<long double, wchar_t*>( value, endPtr );
		};
	};


	template <typename _Char, typename _Rt>
	struct string_numeric_conv;


	template <typename _Char>
	struct string_numeric_conv<_Char, int32_t>
	{
		static std::pair<int32_t, size_t> to_value( const std::basic_string<_Char>& str, numeric_base base = numeric_base::decimal )
		{
			size_t processedCount = 0; int32_t value = std::stol( str, &processedCount, static_cast<int>(base) );
			return std::pair<int32_t, size_t>( value, processedCount );
		};
	};

	template <typename _Char>
	struct string_numeric_conv<_Char, uint32_t>
	{
		static std::pair<uint32_t, size_t> to_value( const std::basic_string<_Char>& str, numeric_base base = numeric_base::decimal )
		{
			size_t processedCount = 0; uint32_t value = std::stoul( str, &processedCount, static_cast<int>(base) );
			return std::pair<uint32_t, size_t>( value, processedCount );
		};
	};

	template <typename _Char>
	struct string_numeric_conv<_Char, int64_t>
	{
		static std::pair<int64_t, size_t> to_value( const std::basic_string<_Char>& str, numeric_base base = numeric_base::decimal )
		{
			size_t processedCount = 0; int64_t value = std::stoll( str, &processedCount, static_cast<int>(base) );
			return std::pair<int64_t, size_t>( value, processedCount );
		};
	};

	template <typename _Char>
	struct string_numeric_conv<_Char, uint64_t>
	{
		static std::pair<uint64_t, size_t> to_value( const std::basic_string<_Char>& str, numeric_base base = numeric_base::decimal )
		{
			size_t processedCount = 0; uint64_t value = std::stoull( str, &processedCount, static_cast<int>(base) );
			return std::pair<uint64_t, size_t>( value, processedCount );
		};
	};

	template <typename _Char>
	struct string_numeric_conv<_Char, float>
	{
		static std::pair<float, size_t> to_value( const std::basic_string<_Char>& str, numeric_base = numeric_base::decimal )
		{
			size_t processedCount = 0; float value = std::stof( str, &processedCount );
			return std::pair<float, size_t>( value, processedCount );
		};
	};

	template <typename _Char>
	struct string_numeric_conv<_Char, double>
	{
		static std::pair<double, size_t> to_value( const std::basic_string<_Char>& str, numeric_base = numeric_base::decimal )
		{
			size_t processedCount = 0; double value = std::stod( str, &processedCount );
			return std::pair<double, size_t>( value, processedCount );
		};
	};

	template <typename _Char>
	struct string_numeric_conv<_Char, long double>
	{
		static std::pair<long double, size_t> to_value( const std::basic_string<_Char>& str, numeric_base = numeric_base::decimal )
		{
			size_t processedCount = 0; long double value = std::stold( str, &processedCount );
			return std::pair<long double, size_t>( value, processedCount );
		};
	};


	template <typename _Char>
	struct string_source_conv;


	template <>
	struct string_source_conv<char>
	{
		template <class _Tp>
		std::basic_string<char> to_string( const _Tp& value )
		{
			return std::to_string( value );
		};
	};

	template <>
	struct string_source_conv<wchar_t>
	{
		template <class _Tp>
		std::basic_string<wchar_t> to_string( const _Tp& value )
		{
			return std::to_wstring( value );
		};
	};


	template <typename _Char>
	struct string_conv
	{
		template <typename _Rt>
		static std::pair<_Rt, char*> to_integer( const char* str, numeric_base base = numeric_base::decimal )
		{
			return char_numeric_conv<char, _Rt>::to_value( str, base );
		}

		template <typename _Rt>
		static std::pair<_Rt, size_t> to_integer( const std::basic_string<char>& str, numeric_base base = numeric_base::decimal )
		{
			return string_numeric_conv<char, _Rt>::to_value( str, base );
		}

		template <typename _Rt>
		static std::pair<_Rt, char*> to_real( const char* str )
		{
			return char_numeric_conv<char, _Rt>::to_value( str );
		}

		template <typename _Rt>
		static std::pair<_Rt, size_t> to_real( const std::basic_string<char>& str )
		{
			return string_numeric_conv<char, _Rt>::to_value( str );
		}

		template <typename _Rt>
		static std::pair<_Rt, size_t> to_value( const std::basic_string<char>& str, numeric_base base = numeric_base::decimal )
		{
			return string_numeric_conv<char, _Rt>::to_value( str, base );
		}

		template <class _Tp>
		std::basic_string<_Char> to_string( const _Tp& value )
		{
			return string_source_conv<_Char>::to_string( value );
		}
	};


	template <typename _Rt, typename _Char>
	inline std::pair<_Rt, _Char*> string_to_integer( const _Char* str, numeric_base base = numeric_base::decimal )
	{
		return string_conv<_Char>::template to_integer<_Rt>( str, base );
	}

	template <typename _Rt, typename _Char>
	inline std::pair<_Rt, size_t> string_to_integer( const std::basic_string<_Char>& str, numeric_base base = numeric_base::decimal )
	{
		return string_conv<_Char>::template to_integer<_Rt>( str, base );
	}

	template <typename _Rt, typename _Char>
	inline std::pair<_Rt, _Char*> string_to_real( const _Char* str )
	{
		return string_conv<_Char>::template to_real<_Rt>( str );
	}

	template <typename _Rt, typename _Char>
	inline std::pair<_Rt, size_t> string_to_real( const std::basic_string<_Char>& str )
	{
		return string_conv<_Char>::template to_real<_Rt>( str );
	}

	template <typename _Rt, typename _Char>
	inline std::pair<_Rt, _Char*> string_to_value( const _Char* str, numeric_base base = numeric_base::decimal )
	{
		return string_conv<_Char>::template to_value<_Rt>( str, base );
	}

	template <typename _Rt, typename _Char>
	inline std::pair<_Rt, size_t> string_to_value( const std::basic_string<_Char>& str, numeric_base base = numeric_base::decimal )
	{
		return string_conv<_Char>::template to_value<_Rt>( str, base );
	}

	template <typename _Char, typename _Tp>
	inline std::basic_string<_Char> to_string( const _Tp& value )
	{
		return string_conv<_Char>::template to_string( value );
	}


}


#endif /* __STDX_STRING_H__ */
