
#ifndef __STDX_SPIN_LOCK_H__
#define __STDX_SPIN_LOCK_H__

#include "thr_common.h"
#include <atomic>


namespace stdx
{


	class spin_lock
	{
	private:
		std::atomic_flag _flag;

	public:
		spin_lock()
			: _flag{ ATOMIC_FLAG_INIT }
		{ }

		///<summary>
		///</summary>
		void lock()
		{
			for ( auto spinCounter = 0; !this->_flag.test_and_set( std::memory_order_acq_rel ); ++spinCounter )
			{
				thr_yield_spinlock( spinCounter );
			}
		}

		///<summary>
		///</summary>
		bool try_lock()
		{
			return !this->_flag.test_and_set( std::memory_order_acq_rel );
		}

		///<summary>
		///</summary>
		void unlock()
		{
			this->_flag.clear( std::memory_order_release );
		}
	};


}


#endif /* __STDX_SPIN_LOCK_H__ */
