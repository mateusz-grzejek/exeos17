
#ifndef __STDX_ATOMIC_MASK_H__
#define __STDX_ATOMIC_MASK_H__

#include "type_traits.h"
#include <atomic>


namespace stdx
{


	template <typename _Tp>
	class atomic_mask
	{
		static_assert((std::is_integral<_Tp>::value || std::is_enum<_Tp>::value) && !std::is_same<_Tp, bool>::value,
			"Atomic Masks are only valid for integer and enum types (but not a bool type)!");

	public:
		typedef atomic_mask<_Tp> my_type;
		typedef typename uint_type_by_size<sizeof( _Tp )>::type value_type;

	private:
		std::atomic<value_type>  _value;

	public:
		atomic_mask()
			: _value( 0 )
		{ }

		template <typename _En>
		atomic_mask( _En mask )
			: _value( static_cast<value_type>(mask) )
		{ }

		template <typename _En>
		my_type& operator=( _En mask )
		{
			this->_value.store( static_cast<value_type>(mask), std::memory_order_relaxed );
			return *this;
		}

		operator value_type() const
		{
			return this->get();
		}

		template <typename _En>
		void store( _En mask )
		{
			this->_value.store( static_cast<value_type>(mask), std::memory_order_relaxed );
		}

		template <typename _En>
		void store( _En mask, std::memory_order order )
		{
			this->_value.store( static_cast<value_type>(mask), order );
		}

		template <typename _En>
		void set( _En mask )
		{
			this->_value.fetch_or( static_cast<value_type>(mask), std::memory_order_relaxed );
		}

		template <typename _En>
		void set( _En mask, std::memory_order order )
		{
			this->_value.fetch_or( static_cast<value_type>(mask), order );
		}

		template <typename _En>
		void toggle( _En mask )
		{
			this->_value.fetch_xor( static_cast<value_type>(mask), std::memory_order_relaxed );
		}

		template <typename _En>
		void toggle( _En mask, std::memory_order order )
		{
			this->_value.fetch_xor( static_cast<value_type>(mask), order );
		}

		template <typename _En>
		void unset( _En mask )
		{
			this->_value.fetch_and( ~static_cast<value_type>(mask), std::memory_order_relaxed );
		}

		template <typename _En>
		void unset( _En mask, std::memory_order order )
		{
			this->_value.fetch_and( ~static_cast<value_type>(mask), order );
		}

		void clear()
		{
			this->_value.store( 0, std::memory_order_relaxed );
		}

		void clear( std::memory_order order )
		{
			this->_value.store( 0, order );
		}

		template <typename _En>
		bool test_and_set( _En mask )
		{
			value_type current = this->_value.load( std::memory_order_relaxed );

			while (true)
			{
				if ((current & static_cast<value_type>(mask)) != 0)
					return false;

				if (this->_value.compare_exchange_strong( current, current | static_cast<value_type>(mask), std::memory_order_acq_rel, std::memory_order_relaxed ))
					return true;
			}
		}

		template <typename _En>
		bool test_and_unset( _En mask )
		{
			value_type current = this->_value.load( std::memory_order_relaxed );

			while (true)
			{
				if ((current & static_cast<value_type>(mask)) != static_cast<value_type>(mask))
					return false;

				if (this->_value.compare_exchange_strong( current, current & ~static_cast<value_type>(mask), std::memory_order_acq_rel, std::memory_order_relaxed ))
					return true;
			}
		}

		value_type get() const
		{
			return this->_value.load( std::memory_order_relaxed );
		}

		value_type get( std::memory_order order ) const
		{
			return this->_value.load( order );
		}

		template <typename _En>
		value_type test( _En mask ) const
		{
			return this->get() & static_cast<value_type>(mask);
		}

		template <typename _En>
		value_type test( _En mask, std::memory_order order ) const
		{
			return this->get( order ) & static_cast<value_type>(mask);
		}

		template <typename _En>
		bool is_set( _En mask ) const
		{
			return (this->get() & static_cast<value_type>(mask)) == static_cast<value_type>(mask);
		}

		template <typename _En>
		bool is_set( _En mask, std::memory_order order ) const
		{
			return (this->get( order ) & static_cast<value_type>(mask)) == static_cast<value_type>(mask);
		}

		template <typename _En>
		void operator|=( _En mask )
		{
			this->_value.fetch_or( static_cast<value_type>(mask), std::memory_order_relaxed );
		}

		template <typename _En>
		void operator&=( _En mask )
		{
			this->_value.fetch_and( static_cast<value_type>(mask), std::memory_order_relaxed );
		}

		template <typename _En>
		void operator^=( _En mask )
		{
			this->_value.fetch_xor( static_cast<value_type>(mask), std::memory_order_relaxed );
		}

		value_type operator~() const
		{
			return ~(this->get());
		}

		template <typename _En>
		value_type operator|( _En mask ) const
		{
			return this->get() | static_cast<value_type>(mask);
		}

		template <typename _En>
		value_type operator&( _En mask ) const
		{
			return this->get() & static_cast<value_type>(mask);
		}

		template <typename _En>
		value_type operator^( _En mask ) const
		{
			return this->get() ^ static_cast<value_type>(mask);
		}

		value_type operator<<( size_t shift ) const
		{
			return this->get() << shift;
		}

		value_type operator>>( size_t shift ) const
		{
			return this->get() >> shift;
		}
	};


}


#endif /* __STDX_ATOMIC_MASK_H__*/
