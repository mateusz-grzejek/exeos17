
#ifndef __STDX_REG_CTRL_COUNTER_H__
#define __STDX_REG_CTRL_COUNTER_H__

#include "thr_common.h"
#include <atomic>


namespace stdx
{


	template <typename Tval = size_t, Tval maxValue = stdx::limits<Tval>::max_value>
	class reg_ctrl_counter
	{
	private:
		std::atomic<Tval>  _value;

	public:
		reg_ctrl_counter( const reg_ctrl_counter& ) = delete;
		reg_ctrl_counter& operator=( const reg_ctrl_counter& ) = delete;

		reg_ctrl_counter()
			: _value( static_cast<Tval>(0) )
		{ }

		Tval increment()
		{
			Tval currentVal = this->_value.load( std::memory_order_relaxed );
			for ( auto spinCounter = 0; ; ++spinCounter )
			{
				if (currentVal == maxValue)
					return maxValue;

				if (this->_value.compare_exchange_weak( currentVal, currentVal + 1, std::memory_order_acq_rel, std::memory_order_relaxed ))
					return currentVal;

				thr_yield_counter( spinCounter );
			}
		}

		Tval increment_cnz()
		{
			Tval currentVal = this->_value.load( std::memory_order_relaxed );
			for ( auto spinCounter = 0; ; ++spinCounter )
			{
				if (currentVal == 0)
					return 0;

				if (currentVal == maxValue)
					return maxValue;

				if (this->_value.compare_exchange_weak( currentVal, currentVal + 1, std::memory_order_acq_rel, std::memory_order_relaxed ))
					return currentVal;

				thr_yield_counter( spinCounter );
			}
		}

		void decrement()
		{
			this->_value.fetch_sub( 1, std::memory_order_release );
		}

		void reset()
		{
			this->_value.store( 0, std::memory_order_release );
		}

		Tval get_value() const
		{
			return this->_value.load( std::memory_order_relaxed );
		}

		Tval load_value() const
		{
			return this->_value.load( std::memory_order_acquire );
		}

		Tval max_value() const
		{
			return maxValue;
		}
	};


}


#endif /* __STDX_REG_CTRL_COUNTER_H__ */

