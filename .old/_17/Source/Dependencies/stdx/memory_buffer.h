
#ifndef __STDX_MEMORY_BUFFER_H__
#define __STDX_MEMORY_BUFFER_H__

#include "common.h"


namespace stdx
{


	template <class _T>
	struct memory_buffer_traits
	{
		typedef _T data_type;
		typedef _T* pointer_type;
	};


	template <class _T>
	class memory_buffer
	{
	public:
		typedef memory_buffer<_T> my_type;
		typedef memory_buffer_traits<_T> traits_type;

		typedef typename traits_type::data_type data_type;
		typedef typename traits_type::pointer_type pointer_type;

	protected:
		data_type*    _memory;
		size_t        _size;

	public:
		memory_buffer( const memory_buffer& ) = delete;
		memory_buffer& operator=( const memory_buffer& ) = delete;

		memory_buffer()
			: _memory( nullptr )
			, _size( 0 )
		{ }

		memory_buffer( data_type* memory, size_t size )
			: _memory( memory )
			, _size( size )
		{ }

		virtual ~memory_buffer()
		{ }

		data_type& operator[]( size_t pos )
		{
			assert( pos < this->_size );
			return this->_memory[pos];
		}

		const data_type& operator[]( size_t pos ) const
		{
			assert( pos < this->_size );
			return this->_memory[pos];
		}

		data_type* data_ptr()
		{
			assert( this->_size > 0 );
			return this->_memory;
		}

		const data_type* data_ptr() const
		{
			assert( this->_size > 0 );
			return this->_memory;
		}

		data_type* data_offset_ptr( size_t offset )
		{
			assert( (this->_size > 0) && (offset <= this->_size) );
			return this->_memory + offset;
		}

		const data_type* data_offset_ptr( size_t offset ) const
		{
			assert( (this->_size > 0) && (offset <= this->_size) );
			return this->_memory + offset;
		}

		size_t fill_bytes( byte value, size_t fillSize = max_size, size_t byteOffset = 0 )
		{
			size_t myTotalSize = this->size_in_bytes();
			if (byteOffset >= myTotalSize)
				return 0;

			fillSize = std::min( fillSize, myTotalSize - byteOffset );
			if (fillSize > 0)
			{
				void* memory = reinterpret_cast<byte*>(this->_memory) + byteOffset;
				memset( memory, value, fillSize );
			}

			return fillSize;
		}

		size_t fill_zero( size_t fillSize = max_size, size_t byteOffset = 0 )
		{
			return this->fill_bytes( 0, fillSize, byteOffset );
		}

		size_t set_bytes( const void* data, size_t dataSize, size_t setOffset = 0 )
		{
			size_t myTotalSize = this->size_in_bytes();
			if ((dataSize == 0) || (setOffset >= myTotalSize))
				return 0;

			size_t copySize = std::min( dataSize, myTotalSize - setOffset );
			if (copySize > 0)
			{
				void* memory = reinterpret_cast<Byte*>(this->_memory) + setOffset;
				memcpy( memory, data, copySize );
			}

			return copySize;
		}

		size_t copy( void* memory, size_t memSize, size_t copySize, size_t byteOffset = 0 ) const
		{
			size_t myTotalSize = this->size_in_bytes();
			if ((memSize == 0) || (copySize == 0) || (byteOffset >= myTotalSize))
				return 0;

			copySize = std::min( memSize, copySize );
			copySize = std::min( copySize, myTotalSize - byteOffset );
			if (copySize > 0)
			{
				void* source = reinterpret_cast<Byte*>(this->_memory) + byteOffset;
				memcpy( memory, source, copySize );
			}

			return copySize;
		}

		size_t size() const
		{
			return this->_size;
		}

		size_t size_in_bytes() const
		{
			return sizeof( data_type ) * this->_size;
		}

		bool empty() const
		{
			return !this->_memory || (this->_size == 0);
		}
	};


	template <class _T, class _Alloc>
	struct dynamic_memory_buffer_traits : public memory_buffer_traits<_T>
	{
		typedef typename memory_buffer_traits<_T>::data_type data_type;
		typedef typename memory_buffer_traits<_T>::pointer_type pointer_type;

		typedef _Alloc allocator_type;
	};


	///<summary>
	///</summary>
	template < class _T,
		class _Alloc = std::allocator<_T> >
		class dynamic_memory_buffer : public memory_buffer<_T>
	{
		dynamic_memory_buffer( const dynamic_memory_buffer& ) = delete;
		dynamic_memory_buffer& operator=( const dynamic_memory_buffer& ) = delete;

	public:
		typedef memory_buffer<_T> base_type;
		typedef dynamic_memory_buffer<_T, _Alloc> my_type;
		typedef dynamic_memory_buffer_traits<_T, _Alloc> traits_type;

		typedef typename traits_type::data_type data_type;
		typedef typename traits_type::pointer_type pointer_type;
		typedef typename traits_type::allocator_type allocator_type;

	private:
		allocator_type  _allocator;

	public:
		dynamic_memory_buffer()
			: base_type()
		{ }

		dynamic_memory_buffer( my_type&& source )
			: base_type()
		{
			this->swap( source );
		}

		explicit dynamic_memory_buffer( const allocator_type& allocator )
			: base_type()
			, _allocator( allocator )
		{ }

		explicit dynamic_memory_buffer( size_t capacity, const allocator_type& allocator = allocator_type() )
			: base_type()
			, _allocator( allocator )
		{
			if (capacity > 0)
				this->resize( capacity );
		}

		virtual ~dynamic_memory_buffer()
		{
			this->release();
		}

		my_type& operator=( my_type&& rhs )
		{
			if (this != &rhs)
				my_type( std::move( rhs ) ).swap( *this );

			return *this;
		}

		void resize( size_t size )
		{
			if (size == this->_size)
				return;

			if (size == 0)
			{
				this->release();
			}
			else if (size > this->_size)
			{
				data_type* newMemory = this->_allocator.allocate( size );
				memcpy( newMemory, this->_memory, sizeof( data_type ) * this->_size );
				this->_allocator.deallocate( this->_memory, this->_size );
				this->_memory = newMemory;
				this->_size = size;
			}
		}

		size_t expand( size_t minSize = 0, float factor = 1.6f )
		{
			if ((minSize > 0) && (minSize <= this->_size))
				return this->_size;

			size_t newSize = std::max( static_cast<size_t>(this->_size * factor), minSize );
			this->resize( newSize );

			return newSize;
		}

		void release()
		{
			if (this->_memory != nullptr)
			{
				this->_allocator.deallocate( this->_memory, this->_size );
				this->_memory = nullptr;
				this->_size = 0;
			}
		}

		void swap( my_type& other )
		{
			std::swap( this->_memory, other._memory );
			std::swap( this->_size, other._size );
			std::swap( this->_allocator, other._allocator );
		}
	};


	template <class _T, size_t _S>
	struct fixed_memory_buffer_traits : public memory_buffer_traits<_T>
	{
		typedef typename memory_buffer_traits<_T>::data_type data_type;
		typedef typename memory_buffer_traits<_T>::pointer_type pointer_type;

		static const size_t size = _S;
	};


	template <class _T, size_t _S>
	class fixed_memory_buffer : public memory_buffer<_T>
	{
		fixed_memory_buffer( fixed_memory_buffer&& ) = delete;
		fixed_memory_buffer( const fixed_memory_buffer& ) = delete;
		fixed_memory_buffer& operator=( fixed_memory_buffer&& ) = delete;
		fixed_memory_buffer& operator=( const fixed_memory_buffer& ) = delete;

	public:
		typedef memory_buffer<_T> base_type;
		typedef fixed_memory_buffer<_T, _S> my_type;
		typedef fixed_memory_buffer_traits<_T, _S> traits_type;

		typedef typename traits_type::data_type data_type;
		typedef typename traits_type::pointer_type pointer_type;

		static const size_t byteSize = traits_type::size * sizeof( data_type );
		static const size_t unitSize = traits_type::size;

	private:
		byte  _storage[byteSize];

	public:
		fixed_memory_buffer()
			: base_type( reinterpret_cast<data_type*>(this->_storage), unitSize )
		{ }

		virtual ~fixed_memory_buffer()
		{ }
	};


}


#endif /* __STDX_MEMORY_BUFFER_H__ */
