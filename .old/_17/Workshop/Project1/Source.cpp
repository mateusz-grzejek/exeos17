//
//#include <Exs/ScriptSystem/Prerequisites.h>
//#include <Exs/ScriptSystem/Lua/LuaPrerequisites.h>
//
//using namespace Exs;
//
//
//class ExData2
//{
//};
//
//
//class ExData
//{
//public:
//	static lua_Integer counter;
//
//public:
//	int value = 0;
//
//public:
//	ExData(int value = 77)
//	: value(value)
//	{ }
//
//	virtual ~ExData()
//	{
//		printf("DTORED!\n");
//	}
//
//	ExData& GetSelf()
//	{
//		return *this;
//	}
//
//	int& GetValRef()
//	{
//		return this->value;
//	}
//
//	int* GetValPtr()
//	{
//		return &(this->value);
//	}
//
//	void SetRef(int& ref, int value)
//	{
//		ref = value;
//	}
//
//	void Exec()
//	{
//		printf("Exec()!\n");
//	}
//
//	void SetValue(int value)
//	{
//		this->value = value;
//	}
//
//	int GetValue() const
//	{
//		return this->value;
//	}
//};
//
//lua_Integer ExData::counter = 666;
//
//struct AA
//{
//	virtual ~AA()
//	{
//		printf("~AA()\n");
//	}
//};
//
//struct BB : public AA
//{
//	virtual ~BB()
//	{
//		printf("~BB()\n");
//	}
//};
//
//
//template <typename T>
//int L_mtx_Ctor(lua_State* luaState)
//{
//	auto top = lua_gettop(luaState);
//
//	auto* userdata = Lua::CreateUserdataObject<T>(luaState);
//
//	userdata->Construct();
//
//	return 1;
//}
//
//int L_ExData_Dtor(lua_State* lua)
//{
//	// ExData* exdata = *(ExData **)luaL_checkudata(lua, 1, Lua::TypeMetaInfo<ExData>::GetClsMetatableName());
//	// delete exdata;
//
//	Lua::UserdataReleaseProcWrapper<ExData>::Release(lua);
//	return 0;
//}
//
//int L_ExData_SetValue(lua_State* lua)
//{
//	Lua::LuaDumpStack(lua);
//	ExData* exdata = *(ExData **)luaL_checkudata(lua, 1, Lua::TypeMetaInfo<ExData>::GetClsMetatableName());
//	auto value = luaL_checknumber(lua, 2);
//	exdata->SetValue(truncate_cast<int>(value));
//	return 0;
//}
//
//int L_ExData_GetValue(lua_State* lua)
//{
//	ExData* exdata = *(ExData **)luaL_checkudata(lua, 1, Lua::TypeMetaInfo<ExData>::GetClsMetatableName());
//	lua_pushinteger(lua, exdata->GetValue());
//	return 1;
//}
//
//
//template <class Variable_type>
//struct NonStaticMemberWrapper;
//
//
//template <class T, class V>
//struct NonStaticMemberWrapper<V T::*>
//{
//	static void get(lua_State* luaState, V T::* varPtr)
//	{
//		// Stack: [..., userdata]
//		T* this_ptr = Lua::Stack::GetThisPtr<T>(luaState, 1);
//		auto value = this_ptr->*(varPtr);
//		lua_pushinteger(luaState, value);
//	}
//
//	static void set(lua_State* luaState, V T::* varPtr)
//	{
//		// Stack: [..., userdata, value]
//
//		T* this_ptr = Lua::Stack::GetThisPtr<T>(luaState, 1);
//		auto value = luaL_checknumber(luaState, 2);
//		this_ptr->*(varPtr) = static_cast<V>(value);
//	}
//};
//
//template <class Var_type, Var_type Var_ptr>
//struct MemberVariableGetterWrapper
//{
//	static int func(lua_State* lua)
//	{
//		NonStaticMemberWrapper<Var_type>::get(lua, Var_ptr);
//		return 1;
//	}
//};
//
//
//template <class Var_type, Var_type Var_ptr>
//struct MemberVariableSetterWrapper
//{
//	static int func(lua_State* lua)
//	{
//		NonStaticMemberWrapper<Var_type>::set(lua, Var_ptr);
//		return 0;
//	}
//};
//
//
//#define ExsScriptMemberVariableGetter(variable) \
//	MemberVariableGetterWrapper<decltype(variable), variable>::func
//
//#define ExsScriptMemberVariableSetter(variable) \
//	MemberVariableSetterWrapper<decltype(variable), variable>::func
//
//
//template <typename T, T* refPtr>
//struct StaticMemberWrapper
//{
//	static int func(lua_State* lua)
//	{
//		T ** udata = (T **)lua_newuserdata(lua, sizeof(T*));
//		*udata = refPtr;
//		luaL_getmetatable(lua, Lua::TypeMetaInfo<ExData>::GetClsMetatableName());
//		lua_setmetatable(lua, -2);
//
//		return 1;
//	}
//};
//
//template <int* refPtr>
//struct StaticMemberWrapper<int, refPtr>
//{
//	static int func(lua_State* lua)
//	{
//		lua_pushinteger(lua, *refPtr);
//		return 1;
//	}
//};
//
//template <typename T, T Value>
//struct ConstantValueWrapper;
//
//template <lua_Integer Value>
//struct ConstantValueWrapper<lua_Integer, Value>
//{
//	static int func(lua_State* lua)
//	{
//		lua_pushinteger(lua, Value);
//		return 1;
//	}
//};
//
//
//#define ExsScriptConstantValueWrapper(variable) \
//	ConstantValueWrapper<decltype(variable), variable>::func
//
//#define ExsScriptEnumeratorWrapper(enumVal) \
//	ConstantValueWrapper<std::underlying_type<decltype(enumVal)>::type, enumVal>::func
//
//struct constantReg
//{
//	const char* name;
//	lua_Integer* valuePtr;
//};
//
//template <size_t N>
//void initStaticSubarray(lua_State* lua, const char* name, const constantReg(&consts)[N])
//{
//	lua_newtable(lua);
//	// Stack: [table, metatable]
//
//	for (auto& reg : consts)
//	{
//		if (reg.name == nullptr)
//		{
//			break;
//		}
//
//		Lua::StaticWrapper<lua_Integer>::Create(lua, reg.valuePtr);
//
//		lua_setfield(lua, -2, reg.name);
//		// Stack: [table, metatable]
//	}
//
//	lua_setfield(lua, -2, name);
//	// Stack: [metatable]
//}
//
//int st__index(lua_State* lua)
//{
//	const char* indexedName = luaL_checkstring(lua, -1);
//	
//	lua_pushstring(lua, "__static_vars_ro");
//	lua_rawget(lua, -3);
//
//	if (lua_isnil(lua, -1))
//	{
//		lua_pop(lua, 1);
//		return 0;
//	}
//	else
//	{
//		lua_pushstring(lua, indexedName);
//		lua_rawget(lua, -2);
//	
//		if (!lua_isnil(lua, -1))
//		{
//			lua_call(lua, 0, 1);
//			return 1;
//		}
//	
//	}
//
//	return 0;
//}
//
//template <typename T>
//void regStdType(lua_State* lua, const char* tpname)
//{
//	luaL_newmetatable(lua, Lua::TypeMetaInfo<T>::GetClsMetatableName());
//	// Stack: [metatable]
//
//	lua_setglobal(lua, tpname);
//}
//
//#define RegStdType(lua, type) regStdType<type>(lua, #type)
//
//template <typename T>
//int SelfRefWrapper(lua_State* luaState)
//{
//	auto* thisptr = Lua::Stack::GetThisPtr<T>(luaState, 1);
//}
//
//enum class Color : Enum
//{
//	Black = 0x00000000,
//	Red = 0xFF0000FF,
//	Green = 0x00FF00FF,
//	Blue = 0x0000FFFF,
//	White = 0xFFFFFFFF
//};
//
//void regColorEnum(lua_State* luaState)
//{
//	Lua::EnumDef<Color> enumDef {
//		"Color",
//		{
//			{ "Black", Color::Black },
//			{ "Red", Color::Red },
//			{ "Green", Color::Green },
//			{ "Blue", Color::Blue },
//			{ "White", Color::White }
//		}
//	};
//
//	Lua::CreateEnumMetatable(luaState, enumDef);
//}
//
//void regExData(lua_State* lua)
//{
//	luaL_Reg const m_funcs[] =
//	{
//		{ "SetValue", ExsClassNonConstMemberFunction(&ExData::SetValue) },
//		{ "GetValue", ExsClassConstMemberFunction(&ExData::GetValue) },
//		{ "GetValRef", ExsClassNonConstMemberFunction(&ExData::GetValRef) },
//		{ "GetSelf", ExsClassNonConstMemberFunction(&ExData::GetSelf) },
//		{ "SetRef", ExsClassNonConstMemberFunction(&ExData::SetRef) },
//		{ "Exec", ExsClassNonConstMemberFunction(&ExData::Exec) },
//		{ nullptr, nullptr }
//	};
//	
//	luaL_Reg const m_getters[] =
//	{
//		{ "value", ExsScriptMemberVariableGetter(&ExData::value) },
//		{ nullptr, nullptr }
//	};
//
//	luaL_Reg const m_setters[] =
//	{
//		{ "value", ExsScriptMemberVariableSetter(&ExData::value) },
//		{ nullptr, nullptr }
//	};
//
//	const Lua::luaEnumReg c_enums[] =
//	{
//		{ "White", 0xFFFFFFFF },
//		{ "Black", 0x00000000 },
//		{ nullptr, 0 }
//	};
//
//	const constantReg s_getters[] =
//	{
//		{ "counter", &(ExData::counter) },
//		{ nullptr, nullptr }
//	};
//
//	const constantReg s_setters[] =
//	{
//		{ "counter", &(ExData::counter) },
//		{ nullptr, nullptr }
//	};
//
//	luaL_newmetatable(lua, Lua::TypeMetaInfo<ExData>::GetClsMetatableName());
//	// Stack: [metatable]
//
//	luaL_setfuncs(lua, m_funcs, 0);
//	// Stack: [metatable]
//
//	lua_pushcfunction(lua, Lua::LuaClsNonStaticIndex<ExData>);
//	// Stack: [func, metatable]
//
//	lua_setfield(lua, -2, "__index"); // metatable.__index = func
//	// Stack: [metatable]
//
//	lua_pushcfunction(lua, Lua::LuaClsNonStaticNewIndex<ExData>);
//	// Stack: [func, metatable]
//
//	lua_setfield(lua, -2, "__newindex"); // metatable.__newindex = func
//	// Stack: [metatable]
//
//	lua_pushcfunction(lua, L_ExData_Dtor);
//	// Stack: [dtor, metatable]
//
//	lua_setfield(lua, -2, "__gc"); // metatable.__gc = dtor
//	// Stack: [metatable]
//	
//	// Lua::CreateNonStaticIndexMetamethod(lua, "__member_vars_ro", m_getters);
//	// Lua::CreateNonStaticIndexMetamethod(lua, "__member_vars_rw", m_setters);
//	initStaticSubarray(lua, "__static_vars_ro", s_getters);
//
//	// lua_newtable(lua);
//	// // Stack: [enumindex, metatable]
//	// 
//	// initEnumSubarray(lua, "Color", c_enums);
//	// 
//	// lua_setfield(lua, -2, "__enumindex");
//	// // Stack: [metatable]
//
//	luaL_newmetatable(lua, Lua::TypeMetaInfo<ExData>::GetInternalMetatableName());
//	// Stack: [submetatable, metatable]
//
//	lua_pushcfunction(lua, ExsClassConstructor(ExData));
//	// Stack: [ctor, submetatable, metatable]
//
//	lua_setfield(lua, -2, "__call"); // submetatable.__call = ctor
//	// Stack: [submetatable, metatable]
//
//	lua_pushcfunction(lua, st__index);
//	// Stack: [func, submetatable, metatable]
//
//	lua_setfield(lua, -2, "__index"); // submetatable.__index = func
//	// Stack: [submetatable, metatable]
//
//	lua_setmetatable(lua, -2);
//	// Stack: [metatable]
//
//	lua_setglobal(lua, "ExData");
//}
//
//
//int main()
//{
//	lua_State * lua = luaL_newstate();
//	luaL_openlibs(lua);
//	
//	RegStdType(lua, int);
//
//	regExData(lua);
//	regColorEnum(lua);
//
//	Lua::LuaDumpStack(lua);
//
//	if (luaL_dofile(lua, "../../../Assets/Scripts/Lua/sample.lua"))
//	{
//		printf("%s\n", lua_tostring(lua, -1));
//	}
//	
//	lua_gc(lua, LUA_GCCOLLECT, 0);
//
//	lua_close(lua);
//
//	return 0;
//}

#include <chrono>
#include <condition_variable>
#include <thread>

int main()
{
	using Cv = std::condition_variable;

	constexpr size_t cvSize = sizeof(Cv);
	constexpr size_t cvNum = 16386;

	Cv* cvPtr = (Cv*)malloc(cvSize * cvNum);

	auto t0 = std::chrono::high_resolution_clock::now();
	for ( size_t i = 0; i < cvNum; ++i )
	{
		new (cvPtr++) Cv();
	}
	auto t1 = std::chrono::high_resolution_clock::now();

	auto diff = t1 - t0;
	auto diffMilli = std::chrono::duration_cast<std::chrono::microseconds>(diff);

	printf("Time: %u\n", diffMilli.count());
	printf("HWConcrt: %u\n", std::thread::hardware_concurrency());

	return 0;
}
